<?php

session_start();
if (isset($_POST['submit_without_captcha'])) {
    /* if Service Enter */
    if (isset($_POST['enquire_for'])) {
        $enquire_for = $_POST['enquire_for'];
    }
    else {
        $enquire_for = 'Hair Transplant';
    }
    /* if page URL available */
    if (isset($_POST['page_url'])) {
        $page_url = $_POST['page_url'];
    }
    else {
        $page_url = '';
    }



    /* if service not available */
//   print_r($_POST);

    if ($_POST['Name'] == '' || $_POST['Mobile'] == '' || $_POST['Email'] == '' || $_POST['City'] == '' || $_POST['Country'] == '') {
        $out = array('status' => 'failed', 'reasdon' => 'empty_filelds');
    }
    else {
        $url      = 'http://intelligible.co.in/get_lead/old_leads';
        $fields   = array(
            'name'        => $_POST['Name'],
            'mobile'      => $_POST['Mobile'],
            'email'       => $_POST['Email'],
            'city'        => $_POST['City'],
            'country'     => $_POST['Country'],
            'lp_url'      => $_POST['lp_url'],
            'return_url'  => $_POST['return_url'],
            'lead_source' => $_POST['lead_source'],
            'service'     => $enquire_for,
            'page_url'    => $page_url,
            'submit_lead' => 'submit'
        );
        $postvars = '';
        $sep      = '';
        foreach ($fields as $key => $value) {
            $postvars.= $sep . urlencode($key) . '=' . urlencode($value);
            $sep = '&';
        }
        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $out    = array('status' => 'success');
    }
    echo json_encode($out);
}
else if (isset($_POST['submit'])) {

    if (isset($_POST['enquire_for'])) {
        $enquire_for = $_POST['enquire_for'];
    }
    else {
        $enquire_for = 'Hair Transplant';
    }

    /* if page URL available */
    if (isset($_POST['page_url'])) {
        $page_url = $_POST['page_url'];
    }
    else {
        $page_url = '';
    }
    // code for check server side validation
    if (empty($_SESSION['captcha_code']) || strcasecmp($_SESSION['captcha_code'], $_POST['captcha_code']) != 0) {
        $last_url = $_SERVER['HTTP_REFERER'] . '?error=wrong_captcha';
        header("location:$last_url");
        //echo 'wrong_captcha';
    }
    else {
        $url    = 'http://intelligible.co.in/get_lead/old_leads';
        $fields = array(
            'name'        => $_POST['Name'],
            'mobile'      => $_POST['Mobile'],
            'email'       => $_POST['Email'],
            'city'        => $_POST['City'],
            'country'     => $_POST['Country'],
            'lp_url'      => $_POST['lp_url'],
            'return_url'  => $_POST['return_url'],
            'lead_source' => $_POST['lead_source'],
            'service'     => $enquire_for,
            'page_url'    => $page_url,
            'submit_lead' => 'submit'
        );

        $postvars = '';
        $sep      = '';
        foreach ($fields as $key => $value) {
            $postvars.= $sep . urlencode($key) . '=' . urlencode($value);
            $sep = '&';
        }
        $ch         = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result     = curl_exec($ch);
        $retrun_url = $_POST['return_url'];
        header("location:$retrun_url");

        curl_close($ch);
    }
}

?>
