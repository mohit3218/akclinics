<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

//define('WP_HOME','http://localhost/');
//define('WP_SITEURL','http://localhost/akc_wp/');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_akclinics');

/** MySQL database username */
//define('DB_USER', 'orgsite_akclinic');
define('DB_USER', 'root');

/** MySQL database password */
//define('DB_PASSWORD', "akclinics@123_");
define('DB_PASSWORD', "");

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z&H(:He.9-$K ,3rPqUv:W<HGM6E$Ng6x.PQX>XaP<g!NqIu^ Civ`uT>byDMqnH');
define('SECURE_AUTH_KEY',  '^S)</,zI)~PVz%~J[C^:TxlN+Fw{iHfMJf=`i.2$qWe!:.4z4dV78ZV[l=%N0;r9');
define('LOGGED_IN_KEY',    'JCGq<y|`#!PT*E:KJWg_{::B?)]u(81jfKMzKL(AI9+ c4qUcZ*y8V/@4-[klrN?');
define('NONCE_KEY',        'T+=fXs|`<|g~u=#T{x$o>3s5kZMa*y-ajy~^VKz9|$D~Vc]6WQS/tJe;N`5E =vg');
define('AUTH_SALT',        'RqL36(e=ldl!ncjIAB#ORC3~lQMdJ!3)eOK9hl}T]L$EcbO|hbil)hU>A918YtJ>');
define('SECURE_AUTH_SALT', 'feb.3&@nbrFNYjl~i#.Tg2:`[y^;yx [n7]]|l3*JM=M_Lxy#P2Pug}yO<L.rvHd');
define('LOGGED_IN_SALT',   'l;J[E+>OAKt`WR/KNQdP2^$]|%Lb6Muan-M`gNU^IeWO_(a0V56Ckw#I`~L.FjuC');
define('NONCE_SALT',       'HGv6-//C[z(|Z%IM]Klyu iU{zA$dn+G0BI 3otdW:RgGaOV#>:Ujd]pdTpmnb?Y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'akc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
