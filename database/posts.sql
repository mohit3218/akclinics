-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2018 at 10:33 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ak_clinics`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) NOT NULL,
  `categories_id` int(10) NOT NULL,
  `title` varchar(5000) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(5000) NOT NULL,
  `post_url` varchar(5000) NOT NULL,
  `post_status` int(1) NOT NULL COMMENT '0 - Uncategorized, 1 - Private, 2 -  Public ',
  `post_date` datetime DEFAULT NULL,
  `document_title` varchar(5000) NOT NULL,
  `meta_description` varchar(6500) NOT NULL,
  `meta_keywords` varchar(5000) NOT NULL,
  `canonical_url` varchar(2000) NOT NULL,
  `no_index` tinyint(1) DEFAULT NULL,
  `no_follow` tinyint(1) DEFAULT NULL,
  `no_archive` tinyint(1) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` int(10) NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

ALTER TABLE `posts` ADD COLUMN `slug_id` INT(10) NOT NULL AFTER `title`;

ALTER TABLE `posts` ADD COLUMN `is_cron` TINYINT(1) NOT NULL AFTER `is_active`;