ALTER TABLE `categories` ADD `document_title` VARCHAR(5000) NOT NULL AFTER `name`, 
ADD `meta_description` VARCHAR(5000) NOT NULL AFTER `document_title`, 
ADD `meta_keywords` VARCHAR(500) NOT NULL AFTER `meta_description`, 
ADD `no_index` TINYINT(1) NOT NULL AFTER `meta_keywords`, 
ADD `no_follow` TINYINT(1) NOT NULL AFTER `no_index`, 
ADD `no_archive` TINYINT(1) NOT NULL AFTER `no_follow`;

ALTER TABLE `posts` ADD `og_title` VARCHAR(3000) NOT NULL AFTER `canonical_url`, ADD `og_description` VARCHAR(3000) NOT NULL AFTER `og_title`, ADD `og_image` TEXT NOT NULL AFTER `og_description`;

ALTER TABLE `posts` ADD `interlink_title` VARCHAR(2000) NOT NULL AFTER `is_active`, ADD `interlink_content` VARCHAR(5000) NOT NULL AFTER `interlink_title`, ADD `interlink_image` VARCHAR(2500) NOT NULL AFTER `interlink_content`;

ALTER TABLE `pages` ADD `canonical_url` VARCHAR(2500) NOT NULL AFTER `og_image`;

ALTER TABLE `posts` ADD `interlink_title` VARCHAR(2000) NOT NULL AFTER `is_active`, ADD `interlink_content` VARCHAR(5000) NOT NULL AFTER `interlink_title`, ADD `interlink_image` VARCHAR(2500) NOT NULL AFTER `interlink_content`;