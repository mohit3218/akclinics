-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ak_clinics
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin','Administrator'),(2,'members','General User');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts`
--

LOCK TABLES `login_attempts` WRITE;
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(5000) NOT NULL,
  `slug_id` int(10) NOT NULL,
  `meta_title` varchar(5000) NOT NULL,
  `meta_keywords` varchar(500) NOT NULL,
  `meta_description` text NOT NULL,
  `og_title` varchar(5000) NOT NULL,
  `og_description` varchar(5000) NOT NULL,
  `og_image` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` int(10) NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'Best Hair Transplant, Dermatology, Cosmetic Surgery in India',1,'Best Hair Transplant, Dermatology, Cosmetic Surgery in India','home-page','AK Clinics offers Best Hair Transplantation, Hair Loss Treatment, Dermatology & Cosmetic Surgery in Ludhiana, New Delhi, Bangalore India at nominal cost. The clinics are lead by internationally renowned surgeon Dr Kapil Dua, best hair transplant surgeon in India.','Result of Hair Transplant','Ak Clinics one of the leading Clinic in India with assurance of best result.Successful Hair Transplant of 5240 grafts done with a good result.','https://cdn.akclinics.org/wp-content/uploads/2016/11/contact-us.jpg',1,0,'2018-10-08 06:26:34',1,'2018-10-09 11:15:50',1),(2,'Hair Transplant - Best Hair Transplantation Clinic in India',2,'Hair Transplant - Best Hair Transplantation Clinic in India','','isit AK Clinics, one stop destination of people who are looking for the best hair transplant clinic and get your hair transplant surgery done by expert surgeons.','Hair Transplant - Types, Advantages & Historical Evolution','AK Clinics is a premiere hair transplant clinic offering  top-class service and lifelong results of hair transplantation.','https://cdn.akclinics.org/wp-content/uploads/2015/06/about-Hair-Transplant-Surgery.png',1,0,'2018-10-09 12:49:32',1,NULL,0),(3,'Hair Transplant Cost in India, Cost of Hair Transplant in Delhi, Ludhiana, Bangalore',3,'Hair Transplant Cost in India, Cost of Hair Transplant in Delhi, Ludhiana, Bangalore','Hair Transplant Cost in India, Hair Transplant Cost','Hair Transplant Cost - AK Clinics offering Hair Transplant Surgery in very genuine and affordable cost in India (Delhi, Ludhiana and Bangalore) - Get Free Price Quote for Hair Transplant.','','','',1,0,'2018-10-16 10:16:42',1,NULL,0),(4,'FUE Hair Transplant - FUE Hair Transplant Cost, Method &amp; Procedure in India',4,'FUE Hair Transplant - FUE Hair Transplant Cost, Method &amp; Procedure in India','FUE Hair Transplant','FUE Hair Transplant - Our reputation for the best FUE Hair Transplant has come to us through hard work and a dedication. AK Clinics pioneered the technique in India in 2007.','FUE Hair Transplant -Advanced methods of Hair Transplantation','FUE or follicular unit extraction is one of the most modern and advanced methods of hair transplantation that does not require any stitches.','https://cdn.akclinics.org/wp-content/uploads/2016/09/fue-ht-post.png',1,0,'2018-10-16 10:18:58',1,NULL,0),(5,'FUT Hair Transplant, FUT Hair Transplant in India - Procedure, Scar &amp; Recovery Time',5,'FUT Hair Transplant, FUT Hair Transplant in India - Procedure, Scar &amp; Recovery Time','FUT Hair Transplant, FUT, Strip Hair Transplant','FUT Hair Transplant - In FUT Hair Transplant, an entire strip of hair is removed from the scalp’s donor area &amp; dissection done under a microscope, area from where strip is taken is stitched','FUT Hair Transplant Procedure and Method','In FUT Hair Transplant, an entire strip of hair is removed from the scalp’s donor area & dissection done under a microscope, area from where strip is taken is stitched','https://cdn.akclinics.org/wp-content/uploads/2016/06/fut-img.png',1,0,'2018-10-16 10:23:39',1,NULL,0),(6,'Hair Transplant Technique in India, Different Procedures and Methods',6,'Hair Transplant Technique in India, Different Procedures and Methods','Hair Transplant Technique, India, Procedures, Methods, FUE, FUT','At AK Clinics, we constantly endeavour to bring the latest hair transplant techniques and technologies, and we opte different procedure to the people who have placed their trust in us.','Methods of Hair Transplant','Hair loss has been around for a really long time, but methods of transplanting hair are actually a little more recent. While the very first hair transplant procedures were conducted in the 1890s.','https://cdn.akclinics.org/wp-content/uploads/2016/08/hair-transplant-techniques.png',1,0,'2018-10-16 10:26:13',1,NULL,0),(7,'Facial Hair Transplant, Beard &amp; Moustache Hair Transplant in India',7,'Facial Hair Transplant, Beard &amp; Moustache Hair Transplant in India','','Complete Facial Hair Transplant Solution by India&#039;s most trusted and experienced surgeon Dr Kapil Dua in india','','','',1,0,'2018-10-16 10:29:52',1,NULL,0),(8,'Beard Transplant in India, Facial Hair Transplant and Cost',8,'Beard Transplant in India, Facial Hair Transplant and Cost','','Complete Beard Transplant and Facial Hair Transplant Solution by India&#039;s most trusted and experienced surgeon Dr Kapil Dua at minimal cost.','Facial Hair Transplant','Before you decide to undergo a beard transplant, you need to understand its anatomy. The formation of the beard starts during the puberty and it continues to grow and develop till the mid-thirties.','https://cdn.akclinics.org/wp-content/uploads/2016/09/beard-hair-tramsplant.png',1,0,'2018-10-16 10:32:17',1,NULL,0),(9,'Bio-FUE Hair Transplant - a Trademark Technique of AK Clinics',9,'Bio-FUE Hair Transplant - a Trademark Technique of AK Clinics','Bio-FUE','AK Clinics started Bio FUE which is followed by many clinics in India. We use the most sophisticated machinery &amp; hygienic methodology for it.','BIO-FUE Hair Transplant - An AK Clinics trademark','AK Clinics started Bio FUE Hair Transplant, which is followed by many clinics in India. We use the most sophisticated machinery & hygienic methodology for it.','https://cdn.akclinics.org/wp-content/uploads/2015/06/bio-fue-result.png',1,0,'2018-10-16 10:37:04',1,NULL,0),(10,'Body Hair Transplant (BHT) in India, Body to Head Hair Transplant',10,'Body Hair Transplant (BHT) in India, Body to Head Hair Transplant','Body Hair Transplant','Now you can have fuller looking eyebrows or even a stylish beard, with the help of our body hair transplant services.','','','',1,0,'2018-10-16 10:39:34',1,NULL,0),(11,'Know all About Revision Hair Transplant',11,'Know all About Revision Hair Transplant','','With the advancement of Hair Restoration and specifically Hair Transplant, there is ever increasing demand of Revision Hair Transplant','Revision Hair Transplant Method','Increasing demand of Revision Hair Transplant. Hair Transplant has come a long way in establishing itself as a technique of Hair Restoration','https://cdn.akclinics.org/wp-content/uploads/2016/08/revision-hair-transplant.png',1,0,'2018-10-16 10:42:12',1,NULL,0),(12,'Hair Transplant - Post-Op Care, What to Expect',12,'Hair Transplant - Post-Op Care, What to Expect','','Post operative care and instructions for hair transplant surgery - General instructions for FUE, FUT, Body hair transplant &amp; Female hair transplant.','Post Operative Care after Hair Transplant','Here are hair transplant surgery post op precautions for better healing and good results.','https://cdn.akclinics.org/wp-content/uploads/2016/12/post-op-care-1.png',1,0,'2018-10-16 10:46:29',1,NULL,0),(13,'Hair Loss Problem, Hair Fall Solution and Treatment',13,'Hair Loss Problem, Hair Fall Solution and Treatment','Hair Loss, Hair fall, hair loss causes','Hair loss problem can become the thing of the past, if you are looking for Hair fall solution, AK Clinics offers you the best and advanced hair fall treatments.','Causes of Hair Loss & Treatment','Most of people are suffering from hair fall ,but they are not aware about proper medical treatment.Get Free Online consultation.','https://cdn.akclinics.org/wp-content/uploads/2016/11/hl-prob.png',1,0,'2018-10-16 11:09:51',1,NULL,0),(14,'Best Hair Restoration Surgery and Transplants Clinics in India',14,'Best Hair Restoration Surgery and Transplants Clinics in India','hair loss treatment, hair restoration clinic in india','AK Clinics offers end to end services for Hair restoration. Starting with diagnosis, medical treatment, Hair Gain Therapy, Wigs, patches to Hair Transplant.','','','',1,0,'2018-10-16 11:11:18',1,NULL,0),(15,'Hair Gain Therapy, PRP Hair Treatment in Delhi, Ludhiana - India',15,'Hair Gain Therapy, PRP Hair Treatment in Delhi, Ludhiana - India','','AK Clinics&#039; Hair Gain Therapy is a combination of PRP Therapy, Low-powered (cold) laser (LLLT) which stops hair loss &amp; accelerates hair growth in India.','What is hair gain therapy?','PRP is one of the best to control hair loss without any surgical pain.','https://cdn.akclinics.org/wp-content/uploads/2016/11/hair-gain-thrpy.png',1,0,'2018-10-16 11:12:34',1,NULL,0),(16,'PRP Therapy, PRP Hair Treatment in Delhi, Ludhiana &amp; Bangalore',16,'PRP Therapy, PRP Hair Treatment in Delhi, Ludhiana &amp; Bangalore','prp, prp hair treatment, prp hair loss treatment, prp in delhi','PRP (Platelet Rich Plasma) Therapy is an advance &amp; innovative hair loss treatment. AK Clinics provide PRP treatment in Delhi, Ludhiana or Bangalore at affordable cost.','Benefits of Platelet Rich Plasma (PRP) Therapy','Know about the benefits of advanced Therapy to stop hair loss that shows impressive results even in the increase of hair caliber.','https://cdn.akclinics.org/wp-content/uploads/2017/07/PRPx.png',1,0,'2018-10-16 11:14:17',1,NULL,0),(17,'Artifical Hair Restoration, Hair Patches &amp; Extensions at Ludhiana &amp; Delhi',17,'Artifical Hair Restoration, Hair Patches &amp; Extensions at Ludhiana &amp; Delhi','','AK Clinics offers artificial hair restoration with custom hair patches, wigs and extensions  at Ludhiana and New Delhi at an affordable cost.','Hair Patches & Extention','Hair Restoration is a excellent options to cover hair loss, without any surgery and over the past few years, we have gained somewhat an expertise in the same.','https://cdn.akclinics.org/wp-content/uploads/2016/09/artificial-hair-restoration.png',1,0,'2018-10-16 11:15:46',1,NULL,0),(18,'Best Scalp Micropigmentation - AK Clinics India',18,'Best Scalp Micropigmentation - AK Clinics India','','AK Clinics offers Scalp micropigmentation in India with only FDA Approved material. An  alternate styling option for diffused hair loss or Hair Transplant.','','','',1,0,'2018-10-16 11:17:34',1,NULL,0),(19,'Mesotherapy Treatment for Hair loss',19,'Mesotherapy Treatment for Hair loss','','Having spent years perfecting the correct combination of therapy and drugs, we can now promise you a full head of hair.','Mesotherapy Treatment for Hair Regrowth','Mesotherapy is a painless treatmnent for hair fall. It helps in regrowth of your hair and reduces baldness.','https://cdn.akclinics.org/wp-content/uploads/2016/08/mesotheraphy.png',1,0,'2018-10-16 11:18:51',1,NULL,0),(20,'AK Clinics - India&#039;s Leading Cosmetic Surgery Clinics',20,'AK Clinics - India&#039;s Leading Cosmetic Surgery Clinics','','Over the years, we have helped several people not only gain their hair back, but also their confidence and self-esteem.','','','',1,0,'2018-10-16 11:20:24',1,NULL,0),(21,'Hair Transplant Training',21,'Hair Transplant Training','Training','We have spent years practicing and perfecting our science, which is why we are so confident that we can bring your hair back.','','','',1,0,'2018-10-16 11:21:28',1,NULL,0),(22,'Best Cosmetic Surgery, Plastic Surgery Clinics in Delhi, India',22,'Best Cosmetic Surgery, Plastic Surgery Clinics in Delhi, India','Best Cosmetic Surgery, Plastic Surgery','AK Clinics specializes in various cosmetic plastic surgeries like, Liposuction, Vitiligo surgery and Breast Augmentation by renowned plastic surgeons at Delhi.','','','',1,0,'2018-10-17 11:49:24',1,NULL,0),(23,'Rhinoplasty, Nose Reshaping Surgery in Delhi, India',23,'Rhinoplasty, Nose Reshaping Surgery in Delhi, India','Rhinoplasty, Nose surgery','Get your nose surgery by India&#039;s most trusted and experienced Rhinoplasty surgeons at AK Clinics, New Delhi, India','Shape your nose with Rhinoplasty','Get your nose surgery and shape it by India\\\'s most trusted and experienced Rhinoplasty surgeons at AK Clinics, New Delhi, Ludhiana center(India)','https://cdn.akclinics.org/wp-content/uploads/2016/09/rhino-post.jpg',1,0,'2018-10-17 11:51:15',1,NULL,0),(24,'Blepharoplasty Surgery, Eyelid Surgery in India',24,'Blepharoplasty Surgery, Eyelid Surgery in India','Blepharoplasty, eyelid surgery in India','AK Clinics is a prestigious cosmetic surgery centre for successful blepharoplasty. Its basically eyelid surgery for the aesthetic correction of both eyelids','Blepharoplasty – the Eyelid Surgery','The procedure done to reconstruct or repair the eyelids is known as blepharoplasty or eyelid surgery.','https://cdn.akclinics.org/wp-content/uploads/2016/08/know-about-blepharoplasty.png',1,0,'2018-10-17 11:56:31',1,NULL,0),(25,'Gynecomastia Surgery, Male Breast Reduction Clinics in Delhi, India',25,'Gynecomastia Surgery, Male Breast Reduction Clinics in Delhi, India','Gynaecomastia','Gynecomastia is enlargement of breast in males. Our state-of-art cosmetic surgery for male breast reduction help patients in Ludhiana and Delhi, India.','Gynecomestia - Male Breast Reduction Surgery','Enlargement of breast in human male is called Gynecomastia because of feminine type looks of breast.','https://cdn.akclinics.org/wp-content/uploads/2016/07/gynecomestia-male-breast-reduction-surgery.png',1,0,'2018-10-17 11:59:00',1,NULL,0),(26,'Vitiligo, White Patched in Skin Treatment in Delhi, Ludhiana &amp; Bangalore - India',26,'Vitiligo, White Patched in Skin Treatment in Delhi, Ludhiana &amp; Bangalore - India','Vitiligo treatment','Vitiligo (leucoderma) Treatment at AK Clinics includes evaluation, its cure and white skin to normal skin color. Facilities - PUVA Therapy &amp; surgical methods. Visit our clinic in Delhi, Ludhiana or Bangalore Now!','White Patches Treatment with Minimally Procedure','Vitiligo is a continual & long-term skin problem that produces patches of white depigmentation. We’ve full-time team of doctors at Delhi, Ludhiana (India) clinics.','https://cdn.akclinics.org/wp-content/uploads/2016/08/viti.png',1,0,'2018-10-17 12:00:31',1,NULL,0),(27,'Abdominoplasty - Tummy Tuck Treatment in India',27,'Abdominoplasty - Tummy Tuck Treatment in India','','Abdominoplasty is a procedure to remove excess fat and skin from the stomach area. The best way to get a flat stomach is Tummy Tuck Treatment.','Remove excess Fat & Skin from Stomach area','Abdominoplasty is a procedure to remove excess fat and skin from the stomach area. The best way to get a flat stomach is Tummy Tuck Treatment.','https://cdn.akclinics.org/wp-content/uploads/2016/08/abdominoplasty-1.png',1,0,'2018-10-17 12:02:06',1,NULL,0),(28,'Fat Reduction, Liposuction Surgery and Cost in Delhi, India',28,'Fat Reduction, Liposuction Surgery and Cost in Delhi, India','Liposuction Surgery','Liposuction is cosmetic surgery that involves the removal of excess of unwanted fat. AK Clinics is renowned for this fat reduction or lipsuction surgery.','Know all about Liposuction - Fat Reduction Surgery','Liposuction surgery removes excess of unwanted fat from various body parts with the help of special medical instruments and cosmetic surgery.','https://cdn.akclinics.org/wp-content/uploads/2016/09/liposuction.png',1,0,'2018-10-17 12:04:07',1,NULL,0),(29,'Breast Augmentation Surgery, Breast Enlargement in Delhi, India',29,'Breast Augmentation Surgery, Breast Enlargement in Delhi, India','','AK Clinics provides best &amp; confidential counseling service in Ludhiana, Delhi for women considering Breast Augmentation Surgery for increasing the breast size.','Breast Augmentation Surgery','Every women wants to look the best and in today\\\'s world it is possible with the advanced cosmetic produres.','https://cdn.akclinics.org/wp-content/uploads/2016/08/breast-augmentaion-surgery.png',1,0,'2018-10-17 12:06:11',1,NULL,0),(30,'Breast Reduction Surgery, Breast Size Reduction in Delhi, India',30,'Breast Reduction Surgery, Breast Size Reduction in Delhi, India','','AK Clinics offers the best Breast Reduction surgical treatment at affordable cost in Ludhiana, Scarless breast reduction surgery from experienced surgeon.','','','',1,0,'2018-10-17 12:07:04',1,NULL,0),(31,'Hymenoplasty, Labiaplasty, Vaginoplasty Ludhiana, Delhi, India',31,'Hymenoplasty, Labiaplasty, Vaginoplasty Ludhiana, Delhi, India','','AK Clinics&#039; plastics surgeons are experienced &amp; known in India for vaginal rejuvenation, So visit us today for Virginity restoration or Virginal tightening.','','','',1,0,'2018-10-17 12:08:12',1,NULL,0),(32,'Male Genital Surgery in Delhi and Ludhiana - India',32,'Male Genital Surgery in Delhi and Ludhiana - India','','AK Clinics offers the male genital surgery which Includes Penis Enlargement,Testicular implants and even excretory functions of sperms can all be treated.','','','',1,0,'2018-10-17 12:09:02',1,NULL,0),(33,'Skin Treatment, Skin Care Clinic &amp; Skin Specialist in Delhi, Ludhiana',33,'Skin Treatment, Skin Care Clinic &amp; Skin Specialist in Delhi, Ludhiana','','AK Clinics offers a range of cosmetological solutions, which will allow you to look young and fresh for a longer time','Know about all cosmetology procedures at AK Clinics','AK Clinics offers a range of cosmetological solutions, which will allow you to look young and fresh for a longer time','https://cdn.akclinics.org/wp-content/uploads/2016/07/22-cosmetology.png',1,0,'2018-10-17 12:39:09',1,NULL,0),(34,'Laser Hair Removal in Delhi &amp; Ludhiana, Full Body Hair Reduction Cost',34,'Laser Hair Removal in Delhi &amp; Ludhiana, Full Body Hair Reduction Cost','','AK Clinics offer best Laser hair removal in Delhi for men and women at affordable cost. Know more about Full Body Laser Hair Removal treatment like face, underarms, hands, legs and bikini line etc.','Diode Laser Treatment for Hair Reduction','Laser Treatment is advanced procedures for unwanted hair removal. We have lastet treatment for laser hair removal in Delih & Ludhiana (India).','https://cdn.akclinics.org/wp-content/uploads/2016/07/laser-hair-removal-treatment.png',1,0,'2018-10-17 12:40:55',1,NULL,0),(35,'Non-surgical Ultrasonic Liposuction - Excess Fat Removal',35,'Non-surgical Ultrasonic Liposuction - Excess Fat Removal','','AK Clinics are one of the leaders in offering the Non-surgical Ultrasonic Liposuction treatment to get rid from excess weight or fat.','','','',1,0,'2018-10-17 12:41:48',1,NULL,0),(36,'Skin Polishing Precedure, Benefit &amp; Treatment in Delhi, Ludhiana - India',36,'Skin Polishing Precedure, Benefit &amp; Treatment in Delhi, Ludhiana - India','','Microdermabrasion or skin polishing at AK Clinics (Ludhiana, Delhi) offers the ultimate peeling solution at affordable cost which suitable for every skin type, makes the skin look fresh  and healthy','Get youthful look by Skin Polishing service','Skin polishing is a modern and upgrade way doing facial, which gives you better result as compare to parlour.','https://cdn.akclinics.org/wp-content/uploads/2016/08/skin-polishing.png',1,0,'2018-10-17 12:42:49',1,NULL,0),(37,'Stretch Marks Remove &amp; Laser Treatment in Ludhiana, Delhi India',37,'Stretch Marks Remove &amp; Laser Treatment in Ludhiana, Delhi India','Stretch Marks Treatment','Get your Laser surgery done at AK Clinics (Ludhiana-Delhi) the best treatment for Stretch marks. Dermabrasion is another treatments to cure stretch marks.','Easy way to remove stretch marks.','Becoming a mother is perhaps one of the most beautiful feelings,this is one of the most memorable times of their lives.once the baby is out in the world, women are plagued with what many of them call, the reminder – stretch marks!','https://cdn.akclinics.org/wp-content/uploads/2016/07/strech-marks-treatment.png',1,0,'2018-10-17 12:43:59',1,NULL,0),(38,'Laser Photo Facial, Photo Facial Treatment, Skin Clinics',38,'Laser Photo Facial, Photo Facial Treatment, Skin Clinics','','Laser Photo Facial at AK Clinics, New Delhi and Ludhiana offers  light-based skin resurfacing treatment to treat brown spots and pigmentation broken capillaries.','Get Clean and Glowing Skin by Laser Photo Facial','Photo Facial is done in easy steps which helps in repairing broken capillaries and remove brown spots from your face.','https://cdn.akclinics.org/wp-content/uploads/2016/08/laser-photo-facial.png',1,0,'2018-10-17 12:45:27',1,NULL,0),(39,'Tattoo Removal Clinics, Tattoo Removal in Delhi, Ludhiana &amp; Bangalore',39,'Tattoo Removal Clinics, Tattoo Removal in Delhi, Ludhiana &amp; Bangalore','Tattoo Removal','Visit India’s leading Laser Tattoo clinic in Delhi, Ludhiana or Bangalore for Permanent tattoo removal or Laser Tattoo Removal service at an affordable cost.','','','',1,0,'2018-10-17 12:46:22',1,NULL,0),(40,'Ultherapy or HIFU Treatment, Ulthera Skin Tightening - Delhi, Ludhiana',40,'Ultherapy or HIFU Treatment, Ulthera Skin Tightening - Delhi, Ludhiana','','Ultherapy treatment Ulthera&trade; is non-surgical skin tightening treatment that uses HIFU - High Intensity Focused Ultrasound. Non Invasive Skin Tightening.','Ulthera Skin Tightening Therapy','Ultherapy is a non-invasive skin tightening treatment to look younger that uses HIFU - High Intensity Focused Ultrasound.','https://cdn.akclinics.org/wp-content/uploads/2017/06/Ultherapy.png',1,0,'2018-10-17 12:47:36',1,NULL,0),(41,'Chemical Peeling Treatment in Delhi, India | Chemical Peeling Cost',41,'Chemical Peeling Treatment in Delhi, India | Chemical Peeling Cost','','Chemical peels are ideal treatment for various skin condition like Acne, Enlarged pores, Blackheads, Pigmentation etc. Visit Ak Clinics for Chemical Peeling Treatment at affordable cost.','Chemical Peels - Ideal solution to treat signs of skin damage.','Chemical peels are not only used to treat problems like acne, pigmentation but also for giving a smooth the texture to the skin','https://cdn.akclinics.org/wp-content/uploads/2016/09/healthy-skin.png',1,0,'2018-10-17 12:50:07',1,NULL,0),(42,'Acne Scar Laser Treatment Clinic in Delhi, Ludhiana | Acne Treatment Cost',42,'Acne Scar Laser Treatment Clinic in Delhi, Ludhiana | Acne Treatment Cost','','Acne Scar Treatment - AK Clinics offers laser treatment for acne scar at affordable cost in Delhi, Ludhiana and Bangalore. Know more about Acne Scars, its type &amp; solutions etc.','Best Place for Acne Treatment','AK Clinics there are many treatment modalities available for Acne & Acne Scars.','https://cdn.akclinics.org/wp-content/uploads/2015/06/acne.png',1,0,'2018-10-17 12:52:19',1,NULL,0),(43,'Pigmentation on Face, Skin Treatment &amp; Remove Pigment in Ludhiana, Delhi',43,'Pigmentation on Face, Skin Treatment &amp; Remove Pigment in Ludhiana, Delhi','','Pigmentation and Melasma on Face and Skin Treatment is a growing problem and AK Clinics with its best doctors and lasers provide permanent solution at Ludhiana &amp; New Delhi.','Hyper-Pigmentation','Excessive Melanin causes Hyper-Pigmentation which can treatment in many ways','https://cdn.akclinics.org/wp-content/uploads/2016/09/skin-pigmentation.png',1,0,'2018-10-17 12:54:40',1,NULL,0),(44,'Laser Vaginal Tightening &amp; Rejuvenation in Delhi, Ludhiana - India',44,'Laser Vaginal Tightening &amp; Rejuvenation in Delhi, Ludhiana - India','','AK Clinics offers the solution for vaginal tightening with the non invasive, painless Laser Vaginal Rejuvenation Procedure ( Vaginoplasty ) at very much affordable cost.','Laser Vaginal Tightening at Affordable Cost','','https://cdn.akclinics.org/wp-content/uploads/2016/08/laservaginalrejuvenation.png',1,0,'2018-10-17 12:56:08',1,NULL,0),(45,'Anti Ageing Skin Treatments &amp; Wrinkle Treatment Clinics in Delhi, Ludhiana - India',45,'Anti Ageing Skin Treatments &amp; Wrinkle Treatment Clinics in Delhi, Ludhiana - India','','AK Clinics, best clinics in Delhi, India offer complete range of anti ageing solutions and treatment for wrinkles, face tightening, crow feet etc.','Look Younger with Anti-Ageing Treatment.','Anti-Ageing Treatment is best way to remove wrinkle and fine line from your face. At AK Clinics we offer all kind of advance treatments for this.','https://cdn.akclinics.org/wp-content/uploads/2016/08/anti-aging.png',1,0,'2018-10-17 12:57:13',1,NULL,0),(46,'ULTRAcel Skin Tightening Treatment - Non Surgical Skin &amp; Lifting Treatment in Delhi, Ludhiana &amp; Bangalore',46,'ULTRAcel Skin Tightening Treatment - Non Surgical Skin &amp; Lifting Treatment in Delhi, Ludhiana &amp; Bangalore','','Are you afraid of undergoing surgical process for skin tightening? Don&#039;t worry. Here is the most ultra modern technique- ULTRAcel, a non-surgical skin tightening &amp; skin uplifting procedure in just 3 steps.','ULTRAcel - A Revolutionary Non Surgical Face Lift & Skin Rejuvenation Treatment','Get soft, tight & uplifted face in just 3 simple steps. Yes, you heard it right! Now you can flaunt your flawless lifted skin just like in your 30s and that too without makeup.','https://cdn.akclinics.org/wp-content/uploads/2018/06/ultracel.png',1,0,'2018-10-17 12:58:22',1,NULL,0),(47,'Hair Transplant Results Before After, Review and Testimonial : India',47,'Hair Transplant Results Before After, Review and Testimonial : India','Results','Hair transplant results, review, video, testimonial and before-after photos : Results of different patients at AK Clinics - Hair Transplant Center','Hair Transplant with EMI Option','Hair Transplant is becoming more and more affordable day by day. Now we have tied up with some finance companies that provide finance for cosmetic procedures. This is a 0% interest loans.','https://cdn.akclinics.org/wp-content/uploads/2016/10/emi-option.png',1,0,'2018-10-17 03:01:03',1,NULL,0),(48,'Hair Transplant Videos, Before After Result Videos in India',48,'Hair Transplant Videos, Before After Result Videos in India','','Hair Transplant is a very easy but difficult to master surgery. These patient videos are a testimony to the quality offered at AK Clinics','','','',1,0,'2018-10-17 03:01:48',1,NULL,0),(49,'Cosmetic Surgeries Results - Before and After',49,'Cosmetic Surgeries Results - Before and After','','Cosmetics surgery before and after results of different patients at AK Clinics.','','','',1,0,'2018-10-17 03:02:33',1,NULL,0),(50,'PRP Therapy Before and After Results',50,'PRP Therapy Before and After Results','','Platelet Rich plasma (PRP) or Hair Gain therapy the best medical treatment for hair loss, Check out the awewome result photos of PRP Therapy by AK Clinics.','','','',1,0,'2018-10-17 03:03:14',1,NULL,0),(51,'Blog',1,'Blog','','AK Clinics\\\'s share their knowledge on hair transplant, hair loss, hair related issues, skin care, case studies and cosmetics procedures for skin enhancement.','','','',1,0,'2018-10-17 03:04:11',1,'2018-10-17 03:25:21',1),(52,'Locations',52,'Locations','AK Clinics Locations','AK clinics locations of all branches.','','','',1,0,'2018-10-17 03:54:14',1,NULL,0),(53,'Hair Transplant in Ludhiana, FUE &amp; FUT Hair Transplant in Ludhiana',53,'Hair Transplant in Ludhiana, FUE &amp; FUT Hair Transplant in Ludhiana','Hair Transplant in Ludhiana','Hair Transplant Ludhiana - AK Clinics is Best Hair Transplant Clinic in Ludhiana at affordable cost with renowned Hair Transplant surgeon across India','','','',1,0,'2018-10-17 03:55:04',1,NULL,0),(54,'Hair Transplant in Delhi, FUE &amp; FUT Hair Transplant Delhi',54,'Hair Transplant in Delhi, FUE &amp; FUT Hair Transplant Delhi','hair transplant in delhi, hair transplant clinic delhi, hair transplant cost in delhi','AK Clinics, offer world-class FUE, FUT Hair Transplant and Hair Loss treatment service in Delhi at affordable cost. Visit our clinic for more detail.','','','',1,0,'2018-10-17 03:56:15',1,NULL,0),(55,'Hair Transplant in Bangalore | Best Hair Transplant clinic in Bangalore | Hair Replacement in Bangalore',55,'Hair Transplant in Bangalore | Best Hair Transplant clinic in Bangalore | Hair Replacement in Bangalore','hair transplant in bangalore, hair transplant clinic in bangalore, best hair transplant clinic in bangalore','Best Hair Transplant in Bangalore - AK Clinics is a leading Hair Loss Treatment and Hair Transplant Clinic in Bangalore that is known for providing best transplant service in Bangalore with outstanding results at affordable cost. Know more about the best hair transplant clinic in Bangalore.','','','',1,0,'2018-10-17 03:57:23',1,NULL,0),(56,'Contact Us',56,'Contact Us','','Call us, email or have a live chat – we are here to help you!','Get free online consultation','AK Clinics offers Best Hair Transplantation, Hair Loss Treatment, Dermatology &amp; Cosmetic Surgery in Ludhiana, New Delhi, Bangalore, Chennai India at nominal cost. The clinics are lead by internationally renowned surgeon Dr Kapil Dua, best hair transplant surgeon in India.','https://cdn.akclinics.org/wp-content/uploads/2016/11/contact-us.jpg',1,0,'2018-10-17 03:58:35',1,NULL,0);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slugs`
--

DROP TABLE IF EXISTS `slugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slugs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(5000) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` int(10) NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slugs`
--

LOCK TABLES `slugs` WRITE;
/*!40000 ALTER TABLE `slugs` DISABLE KEYS */;
INSERT INTO `slugs` VALUES (1,'home',1,0,'2018-10-08 04:34:34',1,'0000-00-00 00:00:00',0),(2,'hair-transplant',1,0,'2018-10-09 12:48:00',1,NULL,0),(3,'hair-transplant-cost',1,0,'2018-10-16 09:57:32',1,NULL,0),(4,'fue-hair-transplant',1,0,'2018-10-16 09:57:54',1,NULL,0),(5,'fut-strip-hair-transplant',1,0,'2018-10-16 09:58:08',1,NULL,0),(6,'hair-transplant-techniques',1,0,'2018-10-16 09:58:43',1,NULL,0),(7,'facial-hair-transplant',1,0,'2018-10-16 09:58:56',1,NULL,0),(8,'beard-transplant',1,0,'2018-10-16 09:59:13',1,NULL,0),(9,'bio-fue',1,0,'2018-10-16 09:59:27',1,NULL,0),(10,'body-hair-transplant',1,0,'2018-10-16 09:59:42',1,NULL,0),(11,'revision-hair-transplant',1,0,'2018-10-16 10:00:03',1,NULL,0),(12,'post-operative-care',1,0,'2018-10-16 10:00:24',1,NULL,0),(13,'hair-loss',1,0,'2018-10-16 10:06:11',1,NULL,0),(14,'hair-restoration',1,0,'2018-10-16 10:06:23',1,NULL,0),(15,'hair-gain-therapy',1,0,'2018-10-16 10:09:00',1,NULL,0),(16,'prp-treatment',1,0,'2018-10-16 10:09:12',1,NULL,0),(17,'artificial-hair-restoration',1,0,'2018-10-16 10:09:24',1,NULL,0),(18,'scalp-micro-pigmentation-india',1,0,'2018-10-16 10:09:45',1,NULL,0),(19,'mesotherapy',1,0,'2018-10-16 10:10:01',1,NULL,0),(20,'about-us',1,0,'2018-10-16 10:10:24',1,NULL,0),(21,'hair-transplant-training',1,0,'2018-10-16 10:10:34',1,NULL,0),(22,'cosmetic-surgery',1,0,'2018-10-17 11:37:46',1,NULL,0),(23,'rhinoplasty',1,0,'2018-10-17 11:37:59',1,NULL,0),(24,'blepharoplasty',1,0,'2018-10-17 11:38:09',1,NULL,0),(25,'gynecomastia',1,0,'2018-10-17 11:38:57',1,NULL,0),(26,'vitiligo-treatment',1,0,'2018-10-17 11:39:16',1,NULL,0),(27,'abdominoplasty',1,0,'2018-10-17 11:39:33',1,NULL,0),(28,'liposuction',1,0,'2018-10-17 11:40:08',1,NULL,0),(29,'breast-augmentation',1,0,'2018-10-17 11:40:25',1,NULL,0),(30,'breast-reduction',1,0,'2018-10-17 11:40:46',1,NULL,0),(31,'cosmetic-gynaecology',1,0,'2018-10-17 11:41:08',1,NULL,0),(32,'male-genital-surgery',1,0,'2018-10-17 11:41:26',1,NULL,0),(33,'cosmetology',1,0,'2018-10-17 12:30:38',1,NULL,0),(34,'laser-hair-removal-for-men-and-women',1,0,'2018-10-17 12:30:54',1,NULL,0),(35,'non-surgical-ultrasonic-liposuction',1,0,'2018-10-17 12:33:17',1,NULL,0),(36,'skin-polishing',1,0,'2018-10-17 12:33:30',1,NULL,0),(37,'stretch-marks-treatment',1,0,'2018-10-17 12:33:41',1,NULL,0),(38,'laser-photo-facial',1,0,'2018-10-17 12:34:03',1,NULL,0),(39,'laser-tattoo-removal',1,0,'2018-10-17 12:34:16',1,NULL,0),(40,'ultherapy-treatment',1,0,'2018-10-17 12:34:36',1,NULL,0),(41,'chemical-peels',1,0,'2018-10-17 12:34:55',1,NULL,0),(42,'acne-treatment',1,0,'2018-10-17 12:35:13',1,NULL,0),(43,'pigmentation-treatment',1,0,'2018-10-17 12:35:40',1,NULL,0),(44,'laser-vaginal-rejuvenation',1,0,'2018-10-17 12:36:00',1,NULL,0),(45,'anti-ageing-treatments',1,0,'2018-10-17 12:36:20',1,NULL,0),(46,'ultracel-skin-tightening-treatment',1,0,'2018-10-17 12:36:35',1,NULL,0),(47,'results',1,0,'2018-10-17 02:58:29',1,NULL,0),(48,'videos',1,0,'2018-10-17 02:58:39',1,NULL,0),(49,'cosmetic-surgery-results',1,0,'2018-10-17 02:58:49',1,NULL,0),(50,'prp-therapy',1,0,'2018-10-17 02:59:03',1,NULL,0),(51,'blog',1,0,'2018-10-17 02:59:28',1,NULL,0),(52,'locations',1,0,'2018-10-17 03:51:41',1,NULL,0),(53,'hair-transplant-ludhiana',1,0,'2018-10-17 03:51:50',1,NULL,0),(54,'hair-transplant-delhi',1,0,'2018-10-17 03:51:58',1,NULL,0),(55,'best-hair-transplant-clinic',1,0,'2018-10-17 03:52:06',1,NULL,0),(56,'contact-us',1,0,'2018-10-17 03:52:20',1,NULL,0);
/*!40000 ALTER TABLE `slugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'127.0.0.1','administrator','$2y$08$hi1qXTUAz/9iyPTVOOCDCO7Sggbg1z.4h8kJ7fxTLmpyWBAoCGsLO','','admin@akclinics.com','',NULL,NULL,'cIraPwpn1Fb3FR5J1dQBrO',1268889823,1539756432,1,'Super','Admin','AK Clinics','1234567891');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (14,1,1),(15,1,2);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-17 16:02:58
