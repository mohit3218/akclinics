<?php
//* Template Name: Front Page
/**
 * Front Page * 
 * @package Ak clinics 
 * @author jatin khatri <dev1@akclinics.com> 
 * @copyright Copyright (c) 2011, AK Clinics 
 * @license http://opensource.org/licenses/gpl-2.0.php GNU Public License * */
// set full width layout
//add_filter(‘genesis_pre_get_option_site_layout ’, ‘__genesis_return_full_width_content’);
?>
<?php
// GENESIS HOMEPAGE TEMPLATE //
// FORCE FULL WIDTH LAYOUT
add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');
// Homepage CUSTOM LOOP
remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'my_custom_page');

function my_custom_page() {
   ?>
   <div class="header-section content content-center valign-center services" id="header-section">
      <div class="container">
         <div class="row ">
            <div class="col-md-6"><img src="<?= get_site_url(); ?>/wp-content/themes/AKclinics/images/result-page-1.png" alt=""></div>
            <div class="col-md-6">
               <h2>Celebrate your life with AK Clinics</h2>
               <h4> Absolutely Natural Results</h4>
               <p>Our consistent results are result of our constant endeqvour to bring excellence in every step in Hair Restoration. That is the purpose of our exsitence!!</p>
               <button class="btn btn-warning right-btn">Result</button>  <button class="btn btn-warning btn-lg right-btn">Free Online Consultation</button> 
            </div>
         </div>
      </div>
   </div>
   <section class="container-fluid mgr-50">
      <div class="row">
         <div class="container">
            <div class="heading">
               <h1>AK Clinics Pioneer Hair Transplant Clinic in India</h1>
               <p>This is not mere statement but a religion with us. Our team strives very hard to meet up with ever expanding needs & wishes of the patients. AK Clinics is amongst the fastest growing Cosmetic Surgery Clinics having served thousands patients every year in our various branches. We’ve been working very hard to achieve excellence in the services we provide especially Hair Transplant . This is the reason all our efforts & standards are at par or better than the best in the world.</p>
            </div>
         </div>
      </div>
   </section>
   <section class="container padding-top">
      <div class="col-lg-12 row-pad">
         <div class="features-tabs">
            <ul>
               <li class="content-1 active"><a style="margin-left:0px;" href="#"><span>Hair Loss</span></a></li>
               <li class="content-2"><a href="#"><span>Hair Transplant</span></a></li>
               <li class="content-3"><a href="#"><span>Hair Restoration</span></a></li>
               <li class="content-4"><a href="#"><span>Why Hair Transplant</span></a></li>
               <li class="content-5"><a href="#"><span>5 Common Mistake</span></a></li>
            </ul>
         </div>
         <div class="features-content">
            <ul>
               <li class="content-1 active">
                  <div class="left">
                     <h3>Hair Loss</h3>
                     <img src="<?= get_site_url(); ?>/wp-content/themes/AKclinics/images/hair-loss.jpg" class="img-responsive" />
                     <a class="btn-green-medium" href="<?= get_site_url(); ?>/hair-loss/">Learn More</a> </div>
                  <div class="right">
                     <p>Growth and development of hair is a cyclical process. After attaining the full maturity considering its length and thickness the hair falls out naturally and is replaced by the growth of new one and the process goes on. If follicles fall out at tremendous rate is called as&nbsp;<b>Alopecia</b>&nbsp;in Medical Terminology. This may result in complete or incomplete baldness which can be patchy, irregular or regular showing certain specific pattern. Pattern and frequency of loss is typically different in&nbsp;<a href="https://akclinics.org/hair-loss/hair-loss-men/"><b>men</b></a>&nbsp;and&nbsp;<a href="https://akclinics.org/hair-loss/hair-loss-in-women/"><b>women</b></a>. There are many factors responsible and differ in all individuals:</p>
                     <p>At&nbsp;<b>AK Clinics</b>, our specialists after thorough diagnosis of&nbsp;<b>root cause of hair loss</b>&nbsp;&amp; suggest a treatment plan</p> </div>
               </li>
               <li class="content-2">
                  <div class="left">
                     <h3><strong>Hair Transplant</strong> overview</h3>
                     <img src="<?= get_site_url(); ?>/wp-content/themes/AKclinics/images/hair-transplant.jpg" class="img-responsive" />
                     <a class="btn-green-medium" href="<?= get_site_url(); ?>/hair-transplant/">Learn More</a> </div>
                  <div class="right">
                     <p>It is the ultimate and permanent treatment for baldness. In the process, the grafts are taken from backside of head called as donor are and then transplanted to bald part of scalp. This surgical procedure is performed by super specialists with great care at right angle, correct distance and orientation for proper and permanent growth with a natural look. This&nbsp;<b>hair restoration surgery</b>&nbsp;is highly effective for both&nbsp;<a href="https://akclinics.org/hair-transplant/hair-transplant-in-men/"><b>men</b></a>&nbsp;and&nbsp;<b>women</b>&nbsp;and the patient can go home same day without any problem or complication.</p>
                     <p>At AK Clinics our world renowned surgeons perform the&nbsp;<a href="https://akclinics.org/hair-transplant/"><b>procedure</b></a>&nbsp;in a very systematic way by using state-of-art- technology which enhances aesthetic value and look.</p>
                  </div>
               </li>
               <li class="content-3">
                  <div class="left">
                     <h3><strong>Hair Restoration </strong>Treatment</h3>
                      <img src="<?= get_site_url(); ?>/wp-content/themes/AKclinics/images/hair-restoration.jpg" class="img-responsive" />
                     <a class="btn-green-medium" href="<?= get_site_url(); ?>/hair-restoration/">Learn More</a></div>
                  <div class="right">
                     <p>It is an overall comprehensive approach adopted by skilled and specialist Doctors after a thorough diagnosis and the requirement of the patient suffering from the baldness. During the treatment, our professionals follow multifarious and systematic strategy to control and reduce the baldness which generally differs from one patient to other.</p>
                     <p class="orange_text"><em><strong>Hair Restoration Treatment basically involves following four practices:</strong></em></p>

                     <ul class="list-unstyled margin-bottom-20">
                        <li class="icon-front-page"><strong>Medicinal Treatment:</strong><ol><li class="icon-front-page1">There are many types of medicines used. Medicines like Finasteride specifically for males, Minoxidil, Dutasteride, Corticosteroids injection etc. Some are used for growth of follicles whereas some other are used to prevent the androgenetic hormone.</li></ol></li>
                     </ul>

                     <ul class="list-unstyled margin-bottom-20">
                        <li class="icon-front-page"><strong>Non-surgical Treatment: This treatment includes PRP treatment, SMP treatment etc.</strong>
                           <ol><li class="icon-front-page1">In PRP treatment, a small quantity of blood is removed and then the highly concentrated PRP is injected into the scalp. PRP is very much rich in growth factors which are helpful in growing new follicles.</li>
                              <li class="icon-front-page1">SMP or Scalp Micro Pigmentation treatment; where natural pigments are used at the epidermal level to replicate the natural appearance of real follicles. This is not only beneficial for baldness but also for hiding scars of scalp.</li>
                              <li class="icon-front-page1">Low level of laser therapy increases blood circulation and prevent clogging in follicles which ultimately promotes hair growth.</li>
                           </ol>
                        </li>


                     </ul>
                     <ul class="list-unstyled margin-bottom-20">
                        <li class="icon-front-page"><strong>Surgical Treatment:</strong>
                           <ol>
                              <li class="icon-front-page1">Treatment includes Hair Transplant procedure such as FUT (Follicular Unit Transplant) and FUE (Follicular Unit Extraction).</li>

                           </ol>

                        </li>

                     </ul>

                     <ul class="list-unstyled margin-bottom-20">
                        <li class="icon-front-page"><strong>Artificial Treatment:</strong>

                           <ol><li class="icon-front-page1">Patches, wig etc are artificial treatments used to hide baldness.</li>
                              <li class="icon-front-page1">This treatment also involves implanting synthetic fibers into scalp by using local anesthesia (synthetic fibers- made up of acrylics or human hair)</li></ol>
                        </li>

                     </ul>

                  </div>
               </li>
               <li class="content-4">
                  <div class="left">
                     <h3>Why <strong>Hair Transplant</strong></h3>
                    <img src="<?= get_site_url(); ?>/wp-content/themes/AKclinics/images/why-hair-transplant.jpg" class="img-responsive" />
                    <a class="btn-green-medium"  href="<?= get_site_url(); ?>/get-free-consultation/">Get Free Consultation</a> </div>
                  <div class="right">
 It is a very natural question that everybody likes to ask his/her doctor that why one needs a surgical procedure and our <a href="https://akclinics.org/our-team/">doctors</a> explain each and every condition required to perform this minor surgical procedure. This procedure involves removal of grafts from donor site and implanting in the recipient site where these grafts grow into new follicles.

                    <strong>Here are some reasons why <a href="https://akclinics.org/hair-transplant/">surgical restoration</a>is the recommended option:</strong>

                     <ul class="list-unstyled margin-bottom-20">
                       <li class="icon-chk">Only known permanent treatment of baldness.</li>
                       <li class="icon-chk">100% safe minor surgical procedure though it requires lot of expertise to get natural results.</li>
                       <li class="icon-chk">Virtually a pain-free procedure done under local anaesthesia.</li>
                        <li>You can style or color your tresses the way you always wished as they grow naturally.</li>
                     </ul>
                  </div>
               </li>
               <li class="content-5">
                  <div class="left">
                     <h3><strong>5</strong>5 Common Mistakes</h3>
                       <img src="<?= get_site_url(); ?>/wp-content/themes/AKclinics/images/mistake.jpg" class="img-responsive" />
                     <a class="btn-green-medium"  href="<?= get_site_url(); ?>/get-free-consultation/">Get Free Consultation</a> </div>
                  <div class="right"><p><strong class="orange_text">All Surgeons are the same</strong><br>
                       <em>Reality:</em> Hair Transplant is an easy to do but very difficult to master surgery, so, patients often deal with unaesthetic results at the hands of inexperienced surgeons.<br>
                       <strong class="orange_text">Everywhere results are aesthetically same</strong><br>
                        <em>Reality:</em> the softness, angulation, depth, direction are in the hands of the surgeon. only a experienced surgeon can give same results in all patients<br>
                        <strong>Unlimited Grafts are possible with FUE </strong><br>
                       <em> Reality:</em> In one sitting maximum 3500-4200 follicles are safe for the patient.<br>
                        <strong>Always look for the cheapest option</strong><br>
                        <em>Reality:</em> There are always some inherent costs for safety, hygiene, experience, systems & trained team. There has to be a compromise for lowering costs dramatically.<br>
                         <strong>Just extraction & implantation is what matters</strong><br>
                        <em>Reality:</em> Right from start of surgery there more than 50 steps and intra-steps for great results eg painless anesthesia, graft storage & handling, FTR to give great results.</p>            
                  </div>
               </li>
            </ul>
         </div>
      </div>
   </section>
   <div class="message-block content content-center valign-center services" id="message-block">
      <div class="container">
         <div class="row">
            <h2 class="center-txt main-title">Our Services</h2>
            <div class="col-lg-4"> <span class="img-circle pic1-services"></span>
               <h2>Hair Restoration</h2>
               <p>Growth and development of hair is a cyclical process. After attaining the full maturity considering its length and thickness the hair </p>
               <p><a role="button" href="<?= get_site_url(); ?>/hair-restoration/" class="btn btn-warning">View details »</a></p>
            </div>
            <div class="col-lg-4"> <span class="img-circle pic2-services"></span>
               <h2>Cosmetic Surgery</h2>
               <p>Growth and development of hair is a cyclical process. After attaining the full maturity considering its length and thickness the hair </p>
               <p><a role="button" href="<?= get_site_url(); ?>/cosmetic-surgery/" class="btn btn-warning">View details »</a></p>
            </div>
            <div class="col-lg-4"> <span class="img-circle pic3-services"></span>
               <h2>Cosmetology</h2>
               <p>Growth and development of hair is a cyclical process. After attaining the full maturity considering its length and thickness the hair </p>
               <p><a role="button" href="<?= get_site_url(); ?>/cosmetology/" " class="btn btn-warning">View details »</a></p>
            </div>
         </div>
      </div>
   </div>
   <section class="container">
      <h2 class="center-txt margin-bottom-60">Our Hair Transplant Testimonials</h2>

      <div class="col-lg-5">
         <div id="owl-demo" class="owl-carousel">
            <div class="item">
               <figure class=""> <a href="#"> <img width="96" height="96" class="bdr" alt="#" src="<?= get_site_url(); ?>/wp-content/themes/AKclinics/images/raj-n.jpg" /> </a> </figure>
               <div itemprop="review" itemscope itemtype="http://schema.org/Review">
                  <h3  itemprop="author">Raj Jaswal</h3>
                  <p itemprop="reviewBody">I started facing hair loss at the age of 26 but it was not very noticeable. But at 29 the hair loss progressed very quickly and crown almost became empty. And further my hair line receded quickly. Now at the age of 31 I started looking very old. I tried many medicines like homeopathic etc etc but noting seem to work. Then I started searching for my options and finally decided for a hair transplant.</p>
                  <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                     <meta itemprop="worstRating" content = "1"/>
                     <meta itemprop="ratingValue" content="5">
                     <meta itemprop="bestRating" content="5">
                  </div>
               </div> 

            </div>
            <div class="item">
               <figure class=""> <a href="#"> <img width="96" height="96" class="bdr" alt="#" src="<?= get_site_url(); ?>/wp-content/themes/AKclinics/images/supal.jpg" /> </a> </figure>
               <div itemprop="review" itemscope itemtype="http://schema.org/Review">
                  <h3  itemprop="author">Vikalp Gaur</h3>
                  <p itemprop="reviewBody">I can see good results post transplant. Overall good. As suggested, I may go for further transplant for crown area. I am currently taking 6 months cyclical therapy again to stop hair fall. Then will take the decision accordingly. I can say I would prefer someone for A K Clinic. As it is giving person the way to live more happily.</p>
                  <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                     <meta itemprop="worstRating" content = "1"/>
                     <meta itemprop="ratingValue" content="5">
                     <meta itemprop="bestRating" content="5">
                  </div>
               </div>  
            </div>
            <div class="item">
               <figure class=""> <a href="#"> <img width="96" height="96" class="bdr" alt="#" src="<?= get_site_url(); ?>/wp-content/themes/AKclinics/images/avtor.jpg" /> </a> </figure>
               <div itemprop="review" itemscope itemtype="http://schema.org/Review">
                  <h3  itemprop="author">Rahul Bhatia</h3>
                  <p itemprop="reviewBody">had a hair transplant at AK clinic under care of Doc. KAPIL DUA...And after treatment it has been 3 month now and I have very much inproved growing hair. He is best surgeon ever seen and very experienced in this art. They also provided very good atmosphere while surgery. With his experience he gave very confident to his patients while surgery.</p>
                  <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                     <meta itemprop="worstRating" content = "1"/>
                     <meta itemprop="ratingValue" content="5">
                     <meta itemprop="bestRating" content="5">
                  </div>
               </div>

            </div>
            <div class="item">
               <figure class=""> <a href="#"> <img width="96" height="96" class="bdr" alt="#" src="<?= get_site_url(); ?>/wp-content/themes/AKclinics/images/avtor.jpg" /> </a> </figure>
               <div itemprop="review" itemscope itemtype="http://schema.org/Review">
                  <meta itemprop="datePublished" content="2011-03-25">
                  <h3  itemprop="author">Suresh</h3>
                  <p itemprop="reviewBody">After talking to Dr Kapil I have decided to go for a hair transplant surgery which turned out to be very good. Total of 6000 grafts were transplanted. The density was medium density. The procedure was FUE without any scar. The procedure was by done both Dr Kapil and Dr Aman. I strongly recommend anyone who is looking for a FUE surgery in India.</p>
                  <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                     <meta itemprop="worstRating" content = "1"/>
                     <meta itemprop="ratingValue" content="5">
                     <meta itemprop="bestRating" content="5">
                  </div>
               </div>
            </div>


         </div>
      </div>
      <div class="col-lg-5 col-md-offset-1">
         <a href="<?= get_site_url(); ?>/results/videos/" title="Hair Testimonials Transplant Videos link">
            <img src="<?= get_site_url(); ?>/wp-content/themes/AKclinics/images/video.jpg" alt="Hair Testimonials Transplant Videos">
         </a>
      </div>
   </div>
   </section>
   <section class="result-block content content-center valign-center services " id="result-block">
      <div class="container-fluid">
         <div class="row">
            <h2 class="center-txt main-title"><a href="#">Why Our Results Are Awesome</a></h2>
            <div class="col-lg-2"> <i class="fa fa-lightbulb-o fa-4x"></i>
               <h6>Experienced Surgeons</h6>
            </div>
            <div class="col-lg-2"><i class="fa fa-trash-o fa-4x"></i>
               <h6>Minimum Graft Wastage</h6>
            </div>
            <div class="col-lg-2"><i class="fa fa-th fa-4x"></i>
               <h6>Minimum out-of-body-time</h6>
            </div>
            <div class="col-lg-2"><i class="fa fa-times-circle fa-4x"></i>
               <h6>Grafts given by Counting</h6>
            </div>
            <div class="col-lg-2"> <i class="fa fa-user-md fa-4x"></i>
               <h6>Ultra Hygienic<br>
                  OTs</h6>
            </div>
            <div class="col-lg-2"><i class="fa fa-user fa-4x"></i>
               <h6>Experienced counselors</h6>
            </div>
            <button class="btn btn-warning btn-lg margin-top-80 ">Free Online Consulation</button>
         </div>
      </div>
   </section>
   <section class="container-fluid">
      <div class="container">
         <h2 class="center-txt main-title">Latest News</h2>
         <div class="row margin-bottom-20"><?php
            global $paged;
            $args = ( array(
                'numberposts' => 4,
                'offset' => 0,
                'category' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'post',
                'post_status' => 'publish',
            ));
            $myposts = get_posts($args);
            //  print_r($myposts);
            foreach ($myposts as $post) : setup_postdata($post);
               // echo 'hello';
               $pos_id = $post->ID;
               $permalink = get_the_permalink($pos_id);
               $post_title = get_the_title($pos_id);
               $image_id = get_post_thumbnail_id($pos_id);
               $image_url = wp_get_attachment_image_src($image_id, 'large', true);
               $imgpath = $image_url[0];
               $string = get_the_content($pos_id);
               $author = get_the_author($pos_id);
               $string = strip_tags($string);

               if (strlen($string) > 100) {

                  // truncate string
                  $string = substr($string, 0, 100);
                  // make sure it ends in a word so assassinate doesn't become ass...
                  //$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="'.$permalink.'">Read More</a>'; 
               }
               $string;
               $author;

               echo '<div class="col-md-3 col-sm-6">
        <div class="thumbnails thumbnail-style thumbnail-kenburn">
          <div class="thumbnail-img">
            <div class="overflow-hidden"> <img alt="" src="' . $imgpath . '" class="img-responsive" /> </div>
            <a href="' . $permalink . '" class="btn-more hover-effect">+</a> </div>
          <div class="caption">
            <h3><a href="' . $permalink . '" class="hover-effect">' . $post_title . '</a></h3>
            <p>' . $string . '</p>
          </div>
          <a href="' . $permalink . '" class="btn-more-location hover-effect"><span class="fa fa-map-marker fa-2x mgr-rht"></span>' . $author . '</a> </div>
      </div>';
            endforeach;
            wp_reset_postdata();
            //enesis_custom_loop( $args );
            ?>
         </div>
      </div>
   </section>
   <script type="application/ld+json">
      {
      "@context": "http://schema.org",
      "@type": "WebSite",
      "url": "https://akclinics.org/",
      "name":"AK clinics",
      "potentialAction": {
      "@type": "SearchAction",
      "target": "https://akclinics.org/search/?q={search_term_string}",
      "query-input": "required name=search_term_string"
      }
      }
   </script>
   <?php
}
?>
<?php genesis(); ?>
