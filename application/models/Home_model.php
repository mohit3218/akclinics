<?php

class Home_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    //get slug ID by current URl Slug name
    public function get_slug_id_by_name($slug_name)
    {
		$this->db->cache_on();
        $this->db->select(array('id', 'name'));
        $this->db->where('name',$slug_name);
        $query = $this->db->get('slugs');
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $slugs_arr = array('id' => $row->id, 'name' => $row->name);
            }
        } 
        else 
        {
            $slugs_arr = FALSE;
        }
        return $slugs_arr;
    }
    
    //get post by slug ID 
    public function get_post_by_slug_id($slug_id)
    {
        $query = $this->db->get_where('posts', array('slug_id' => $slug_id));
        return $query->row_array();
    }
    
    //get post by slug ID 
    public function get_post_blogs($conditions = NULL, $limit = NULL, $cat_ids = array())
    {
        $this->db->cache_on();
        $this->db->select(array('post.id', 'post.title' , 'post.content', 'post.image', 'post.image_alt', 'post.post_url',
                            'post.interlink_title','post.interlink_content','post.interlink_image',
                            'post.post_status', 'post.post_date', 'post.post_author', 'post.is_cron', 'pc.cat_id'));
        $this->db->from('posts as post');
        $this->db->join('posts_cat_ids as pc', 'pc.post_id = post.id', 'left'); 
        $this->db->where($conditions);
        if(!empty($cat_ids))
        {
            $this->db->where_in('pc.cat_id', $cat_ids);
        }
        $this->db->limit($limit);
        $this->db->order_by('DATE(post.post_date)', 'DESC');
        $this->db->group_by('pc.post_id');
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $post_blogs[] = array(
                    'id' => $row->id, 
                    'interlink_title' => $row->interlink_title,
                    'interlink_content' => $row->interlink_content,
                    'interlink_image' => $row->interlink_image,
                    'title' => $row->title,
                    'categories_id' => $row->cat_id,
                    'content' => $row->content,
                    'image' => $row->image,
                    'image_alt' => $row->image_alt,
                    'post_url' => $row->post_url,
                    'post_status' => $row->post_status,
                    'post_date' => $row->post_date,
                    'is_cron' => $row->is_cron
                );
            }
        }
        else 
        {
            $post_blogs = FALSE;
        }
        return $post_blogs;
    }
    
    public function get_blogs_data($conditions = NULL, $limit = NULL, $offset= NULL)
    {
        $this->db->cache_on();
        $this->db->select(array('post.id', 'post.title' , 'post.content', 'post.image', 'post.image_alt', 'post.post_url',
                            'post.interlink_title','post.interlink_content','post.interlink_image',
                            'post.post_status', 'post.post_date', 'post.is_cron', 'pc.cat_id'));
        $this->db->from('posts as post');
        $this->db->join('posts_cat_ids as pc', 'pc.post_id = post.id', 'left'); 
        $this->db->limit($limit, $offset);
        $this->db->where($conditions);
        $this->db->order_by('DATE(post.post_date)', 'DESC');
        $this->db->group_by('pc.post_id');
        $query = $this->db->get();
        
        $post_blogs = array();
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $post_blogs[] = array(
                    'id' => $row->id, 
                    'interlink_title' => $row->interlink_title,
                    'interlink_content' => $row->interlink_content,
                    'interlink_image' => $row->interlink_image,
                    'title' => $row->title,
                    'categories_id' => $row->cat_id,
                    'content' => $row->content,
                    'image' => $row->image,
					'image_alt' => $row->image_alt,
                    'post_url' => $row->post_url,
                    'post_status' => $row->post_status,
                    'post_date' => $row->post_date,
                    'is_cron' => $row->is_cron
                );
            }
        }
        
        return $post_blogs;
    }
    
    //get post url and post ID
    public function get_post_url_and_id()
    {
        $this->db->cache_on();
        $this->db->select(array('id','post_url','post_date'));
        $this->db->where('is_cron', 1);
        $query = $this->db->get('posts');
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = array(
                    'id' => $row->id, 
                    'post_url' => $row->post_url,
                    'post_date' => $row->post_date
                );
            }
        }
        else 
        {
            $data = FALSE;
        }
        return $data;
    }
    
    function get_posts_list_url_id() 
    {
        $this->db->cache_on();
        $this->db->select('posts.id, posts.post_url');
        $this->db->where('is_deleted', 0 );
        $this->db->from('posts');
        $query = $this->db->get();
        foreach ($query->result_array() as $key => $data)
        {
            $slug = substr($data['post_url'], strpos($data['post_url'], "blog") + 5);
            $slug_name = trim($slug,'/');
            $list[$data['id']] = $slug_name;
        }
        return $list;
    }
    
        //get slug ID by current URl Slug name
    public function get_post_slug_id_by_name($slug_name)
    {
        $this->db->cache_on();
        $this->db->select(array('id', 'name'));
        $this->db->where('name',$slug_name);
        $query = $this->db->get('post_slugs');
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $slugs_arr = array('id' => $row->id, 'name' => $row->name);
            }
        } 
        else 
        {
            $slugs_arr = FALSE;
        }
        return $slugs_arr;
    }
    
    //get post by slug ID 
    public function get_post_by_post_slug_id($slug_id)
    {
        $this->db->cache_on();
        $query = $this->db->get_where('posts', array('slug_id' => $slug_id));
        return $query->row_array();
    }
    
    //get cat ID by current URl Slug name
    public function get_blog_cat_slug_by_name($slug_name)
    {
        $this->db->cache_on();
        $this->db->select(array('id', 'slug'));
        $this->db->where('slug',$slug_name);
        $query = $this->db->get('categories');
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $cat_slugs_arr = array('id' => $row->id, 'slug' => $row->slug);
            }
        } 
        else 
        {
            $cat_slugs_arr = FALSE;
        }
        return $cat_slugs_arr;
    }
    
    //get all google reviews all locations
    public function get_google_review_record()
    {
        $this->db->cache_on();
        $this->db->select(array('id', 'name', 'rating', 'review', 'is_active'));
        $query = $this->db->get('google_review_ratings');
        return $query->result_array();
    }
	
	function get_users_list() 
    {
		$this->db->cache_on();
        $this->db->select('users.id, users.first_name, users.last_name');
        $this->db->from('users');
        $query = $this->db->get();
        foreach ($query->result_array() as $key => $data)
        {
            $user_list[$data['id']] = $data['first_name'] . ' ' .  $data['last_name'];
        }
        return $user_list;
    }
    
    public function getSimilarPost()
    {
        $slud_data = $this->get_post_slug_id_by_name($this->uri->segment(2));
        $query = $this->db->get_where('posts_cat_ids',array('post_id' => $slud_data['id']));
        
        foreach($query->result() as $row)
        {
            $cat_id = $row->cat_id; 
        }
        
        $cat_post_records = $this->get_post_by_cat_id($cat_id);
        return $cat_post_records;
    }
    
    //get post by cat ID 
    public function get_post_by_cat_id($cat_id)
    {
        $this->db->cache_on();
        $this->db->select(array('post.id', 'post.title' , 'post.post_url','post.post_status','pc.cat_id'));
        $this->db->from('posts as post');
        $this->db->join('posts_cat_ids as pc', 'pc.post_id = post.id', 'left'); 
        $this->db->limit(6);
        $this->db->where('post.post_status = 2');
        $this->db->where('pc.cat_id = ' . $cat_id);
        $this->db->order_by('DATE(post.post_date)', 'DESC');
        $this->db->group_by('pc.post_id');
        $query = $this->db->get();
        
        $post_blogs_by_cat = [];
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $post_blogs_by_cat[] = array(
                    'id' => $row->id, 
                    'title' => $row->title,
                    'post_url' => $row->post_url,
                    'post_status' => $row->post_status
                );
            }
        }
        else 
        {
            $post_blogs_by_cat;
        }
        return $post_blogs_by_cat;
    }
}
