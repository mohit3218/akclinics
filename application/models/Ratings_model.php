<?php

class Ratings_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function ratings_view_search_coundtiotn($s_data)
    {
        $whr = NULL;

        $name = $s_data['name'];
        $is_active = $s_data['is_active'];
        
        if ($name != '')
        {
            $whr[] = array('field' => 'name', 'field_type' => 'like', 'value' => $name);
        }
        if ($is_active != '')
        {
            $whr[] = array('field' => 'is_active', 'field_type' => 'single', 'value' => $is_active);
        }

        return $whr;
    }
    
    function count_ratings($where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        $query = $this->db->get('google_review_ratings');
        
        return $query->num_rows();
    }
    
    function get_review_ratings_data($limit, $start, $search, $where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        
        $start = max(0, ( $start - 1 ) * $limit);
        $this->db->limit($limit, $start);
        $this->db->where('is_deleted', 0);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('google_review_ratings');
        
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $out[] = array(
                    'id' => $row->id,
                    'name' => $row->name,
                    'rating' => $row->rating,
                    'review' => $row->review,
                    'is_active' => $row->is_active
                );
            }
        }
        else
        {
            $out = [];
        }
        return $out;
    }
    
    public function get_review_ratings()
    {
        $this->db->where('is_deleted', 0);
        $query = $this->db->get('google_review_ratings');
        return $query->result_array();
    }

    public function get_review_ratings_by_id($id = 0)
    {
        if ($id === 0)
        {
            $query = $this->db->get('google_review_ratings');
            return $query->result_array();
        }

        $query = $this->db->get_where('google_review_ratings', array('id' => $id));
        return $query->row_array();
    }

    public function add_review_ratings($id = 0)
    {
        $data = array();
         
        if ($id == 0)
        {
            $data = array(
                'name' => $this->input->post('name'),
                'rating' => $this->input->post('rating'),
                'review' => $this->input->post('review'),
                'created_on' => date('Y-m-d h:i:s'),
                'created_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            return $this->db->insert('google_review_ratings', $data);
        }
        else
        {
            
            $data = array(
                'name' => $this->input->post('name'),
                'rating' => $this->input->post('rating'),
                'review' => $this->input->post('review'),
                'updated_on' => date('Y-m-d h:i:s'),
                'updated_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            $this->db->where('id', $id);
            return $this->db->update('google_review_ratings', $data);
        }
    }

    public function delete_review_ratings($id)
    {
        /*$this->db->where('pages.slug_id', $id);
        $this->db->where('pages.is_deleted', 0);
        $query = $this->db->get('pages');
        $count_record = $query->num_rows();
        
        if($count_record > 0)
        {
            return false;
        }
        else
        {*/
            $this->db->where('id', $id);
            $data = array('is_deleted' => 1);
            return $this->db->update('google_review_ratings', $data);
        //}
    }
    
    function get_review_ratings_arr() 
    {
        $this->db->select(array('id', 'name'));
        $query = $this->db->get('google_review_ratings');
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $review_ratings_arr[] = array('id' => $row->id, 'name' => $row->name);
            }
        } 
        else 
        {
            $review_ratings_arr[] = FALSE;
        }
        return $review_ratings_arr;
    }
    
    function get_review_ratings_list() 
    {
        $this->db->select('grr.id, grr.name');
        $this->db->where('grr.is_deleted', 0 );
        $this->db->from('google_review_ratings as grr')->order_by('grr.name','asc');
        $query = $this->db->get();
        foreach ($query->result_array() as $key => $data)
        {
            $list[$data['id']] = $data['name'];
        }
        return $list;
    }
}
