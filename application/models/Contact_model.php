<?php

class Contact_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function contact_view_search_coundtiotn($s_data)
    {
        $whr = NULL;

        $name = $s_data['name'];
        $is_active = $s_data['is_active'];
        
        if ($name != '')
        {
            $whr[] = array('field' => 'name', 'field_type' => 'like', 'value' => $name);
        }
        if ($is_active != '')
        {
            $whr[] = array('field' => 'is_active', 'field_type' => 'single', 'value' => $is_active);
        }

        return $whr;
    }
    
    function count_contact($where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        $query = $this->db->get('contacts');
        
        return $query->num_rows();
    }
    
    function get_contact_data($limit, $start, $search, $where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        
        $start = max(0, ( $start - 1 ) * $limit);
        $this->db->limit($limit, $start);
        $this->db->where('is_deleted', 0);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('contacts');
        
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $out[] = array(
                    'id' => $row->id,
                    'name' => $row->name,
                    'email' => $row->email,
                    'enquire_types' => $row->enquire_types,
                    'phone' => $row->phone,
                    'city' => $row->city,
                    'country' => $row->country,
                    'is_active' => $row->is_active
                );
            }
        }
        else
        {
            $out = [];
        }
        return $out;
    }
    
    public function get_contact()
    {
        $this->db->where('is_deleted', 0);
        $query = $this->db->get('contacts');
        return $query->result_array();
    }

    public function get_contact_by_id($id = 0)
    {
        if ($id === 0)
        {
            $query = $this->db->get('contacts');
            return $query->result_array();
        }

        $query = $this->db->get_where('contacts', array('id' => $id));
        return $query->row_array();
    }

    public function add_contact($id = 0)
    {
        $this->load->helper('url');

        $data = array();
         
        if ($id == 0)
        {
            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'enquire_types' => $this->input->post('enquire_types'),
                'city' => $this->input->post('city'),
                'country' => $this->input->post('country'),
                'created_on' => date('Y-m-d h:i:s'),
                'created_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            return $this->db->insert('contacts', $data);
        }
        else
        {
            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'enquire_types' => $this->input->post('enquire_types'),
                'city' => $this->input->post('city'),
                'country' => $this->input->post('country'),
                'updated_on' => date('Y-m-d h:i:s'),
                'updated_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            $this->db->where('id', $id);
            return $this->db->update('contacts', $data);
        }
    }

    public function delete_contact($id)
    {
        /*$this->db->where('pages.slug_id', $id);
        $this->db->where('pages.is_deleted', 0);
        $query = $this->db->get('pages');
        $count_record = $query->num_rows();
        
        if($count_record > 0)
        {
            return false;
        }
        else
        {*/
            $this->db->where('id', $id);
            $data = array('is_deleted' => 1);
            return $this->db->update('contacts', $data);
        //}
    }
    
    function get_contact_arr() 
    {
        $this->db->select(array('id', 'name'));
        $query = $this->db->get('contacts');
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $contact_arr[] = array('id' => $row->id, 'name' => $row->name);
            }
        } 
        else 
        {
            $contact_arr[] = FALSE;
        }
        return $contact_arr;
    }
    
    function get_contact_list() 
    {
        $this->db->select('contacts.id, contacts.name');
        $this->db->where('is_deleted', 0 );
        $this->db->from('contacts')->order_by('contacts.name','asc');
        $query = $this->db->get();
        foreach ($query->result_array() as $key => $data)
        {
            $list[$data['id']] = $data['name'];
        }
        return $list;
    }

}
