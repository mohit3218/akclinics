<?php

class Post_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function posts_view_search_coundtiotn($s_data)
    {
        $whr = NULL;

        $title = $s_data['title'];
        //$categories_id = $s_data['categories_id'];
        $post_sts = $s_data['post_status'];
        $is_active = $s_data['is_active'];
        
        if ($title != '')
        {
            $whr[] = array('field' => 'title', 'field_type' => 'like', 'value' => $title);
        }
        /*if ($categories_id != '')
        {
            $whr[] = array('field' => 'categories_id', 'field_type' => 'single', 'value' => $categories_id);
        }*/
        if ($post_sts != '')
        {
            $whr[] = array('field' => 'post_status', 'field_type' => 'single', 'value' => $post_sts);
        }
        if ($is_active != '')
        {
            $whr[] = array('field' => 'is_active', 'field_type' => 'single', 'value' => $is_active);
        }

        return $whr;
    }
    
    function count_posts($where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        $query = $this->db->get('posts');
        
        return $query->num_rows();
    }
    
    function get_posts_data($limit, $start, $search, $where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        $this->db->select(array('post.id', 'post.slug_id', 'post.title' , 'post.content', 'post.image', 'post.image_alt', 'post.post_url',
                        'post.post_status', 'post.post_author', 'post.post_date', 'post.document_title', 'post.meta_description', 
                        'post.meta_keywords','post.canonical_url','post.no_index','post.no_follow','post.no_archive',
                        'post.interlink_title','post.interlink_content','post.interlink_image','post.created_on','post.is_active','pc.cat_id'));
        
        $this->db->from('posts as post');
        $this->db->join('posts_cat_ids as pc', 'pc.post_id = post.id', 'left'); 
        $start = max(0, ( $start - 1 ) * $limit);
        $this->db->limit($limit, $start);
        $this->db->where('post.is_deleted', 0);
        $this->db->order_by('DATE(post.post_date)', 'DESC');
        $this->db->group_by('pc.post_id');
        $query = $this->db->get();
        
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $out[] = array(
                    'id' => $row->id,
                    'cat_id' => $row->cat_id,
                    'slug_id' => $row->slug_id,
                    'title' => $row->title,
                    'content' => $row->content,
                    'image' => $row->image,
                    'image_alt' => $row->image_alt,
                    'post_url' => $row->post_url,
                    'post_author' => $row->post_author,
                    'post_status' => $row->post_status,
                    'post_date' => $row->post_date,
                    'document_title' => $row->document_title,
                    'meta_description' => $row->meta_description,
                    'meta_keywords' => $row->meta_keywords,
                    'canonical_url' => $row->canonical_url,
                    'no_index' => $row->no_index,
                    'no_follow' => $row->no_follow,
                    'no_archive' => $row->no_archive,
                    'interlink_title' => $row->interlink_title,
                    'interlink_content' => $row->interlink_content,
                    'interlink_image' => $row->interlink_image,
                    'created_on' => $row->created_on,
                    'is_active' => $row->is_active
                );
            }
        }
        else
        {
            $out = [];
        }
        return $out;
    }
    
    public function get_posts()
    {
        $this->db->where('is_deleted', 0);
        $query = $this->db->get('posts');
        return $query->result_array();
    }

    public function get_posts_by_id($id = 0)
    {
        if ($id === 0)
        {
            $query = $this->db->get('posts');
            return $query->result_array();
        }

        $query = $this->db->get_where('posts', array('id' => $id));
        return $query->row_array();
    }

    public function add_posts($id = 0)
    {
        $this->load->helper('url');

        $data = array();
         
        if ($id == 0)
        {
            $image_file = $interlink_image= "";
            //Featuread Image Upload
            if($_FILES['image']['name'] != "")
            {
                $image_file = $this->uploadPdfFaq($_FILES);

                if($image_file == FALSE)
                    $image_file = "";
            }
            //Interlinking Image Upload
            if(isset($_FILES['interlink_image']['name']))
            {
                if($_FILES['interlink_image']['name'] != "")
                {
                    $interlink_image = $this->uploadInterLinkImgae($_FILES);

                    if($interlink_image == FALSE)
                        $interlink_image = "";
                }
            }
            
            $no_index = $no_follow = $no_archive = 0;
            if(isset($_POST['no_index']) && $_POST['no_index'] == 'on'){$no_index  = 1;}
            if(isset($_POST['no_follow']) && $_POST['no_follow'] == 'on'){$no_follow  = 1;}
            if(isset($_POST['no_archive']) && $_POST['no_archive'] == 'on'){$no_archive  = 1;}
            
            $data = array(
                'title' => $this->input->post('title'),
                'content' => trim($this->input->post('content')),
                'image' => $image_file,
                'image_alt' => $this->input->post('image_alt'),
                'post_url' => $this->input->post('post_url') . '/',
                'post_author' => $this->input->post('post_author'),
                'post_status' => $this->input->post('post_status'),
                'post_date' => date('Y-m-d h:i:s'),
                'document_title' => $this->input->post('document_title'),
                'meta_description' => $this->input->post('meta_description'),
                'meta_keywords' => $this->input->post('meta_keywords'),
                'canonical_url' => $this->input->post('canonical_url'),
                'no_index' => $no_index,
                'no_follow' => $no_follow,
                'no_archive' => $no_archive,
                'og_title' => $this->input->post('og_title'),
                'og_description' => $this->input->post('og_description'),
                'og_image' => $this->input->post('og_image'),
                'interlink_title' => $this->input->post('interlink_title'),
                'interlink_content' => $this->input->post('interlink_content'),
                'interlink_image' => $interlink_image,
                'created_on' => date('Y-m-d h:i:s'),
                'created_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            $slug_data = array(
                'name' => $this->input->post('slug_name'),
                'created_on' => date('Y-m-d h:i:s'),
                'created_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 
            
            //Post Data insert
            $this->db->insert('posts', $data);
            //Get Post Insert ID
            $post_inserted_id = $this->db->insert_id();
             
            // Insert post categories id
            $cat_ids = $this->input->post('categories_id');
            
            $post_cat_data = [];
            foreach ($cat_ids as $k => $cat_id)
            {
                $post_cat_data['post_id'] = $post_inserted_id;
                $post_cat_data['cat_id'] = $cat_id;
                
                $this->db->insert('posts_cat_ids', $post_cat_data);
            }
            
            //Slug Data Inset
            $this->db->insert('post_slugs', $slug_data);
            //Get Slug Insert ID
            $slug_inserted_id = $this->db->insert_id();

            //Updated Slug Id Inserted Post
            $update_post_data = array('slug_id' => $slug_inserted_id);
            $this->db->where('id', $post_inserted_id);
            $this->db->update('posts', $update_post_data);
            $this->db->trans_complete(); # Completing transaction

            if ($this->db->trans_status() === FALSE)
            {
                # Something went wrong.
                $this->db->trans_rollback();
                return FALSE;
            }
            else
            {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
                return TRUE;
            }
        }
        else
        {
            $image_file = $interlink_image = FALSE;
            if($_FILES['image']['name'] != "")
                $image_file = $this->uploadPdfFaq($_FILES);
            
            
            //Interlinking Image Upload
            if(isset($_FILES['interlink_image']['name']))
            {
                if($_FILES['interlink_image']['name'] != "")
                $interlink_image = $this->uploadInterLinkImgae($_FILES);
            }
            $no_index = $no_follow = $no_archive = 0;
            if(isset($_POST['no_index']) && $_POST['no_index'] == 'on'){$no_index  = 1;}
            if(isset($_POST['no_follow']) && $_POST['no_follow'] == 'on'){$no_follow  = 1;}
            if(isset($_POST['no_archive']) && $_POST['no_archive'] == 'on'){$no_archive  = 1;}
            $post_date_update = date('Y-m-d', strtotime($_POST['post_date'])) . ' '. date("h:i:s");
            
            $data = array(
                'title' => $this->input->post('title'),
                'slug_id' => $this->input->post('slug_id'),
                'content' => $this->input->post('content'),
                'post_url' => $this->input->post('post_url'),
                'post_author' => $this->input->post('post_author'),
                'post_status' => $this->input->post('post_status'),
                'post_date' => $post_date_update,
				'image_alt' => $this->input->post('image_alt'),
                'document_title' => $this->input->post('document_title'),
                'meta_description' => $this->input->post('meta_description'),
                'meta_keywords' => $this->input->post('meta_keywords'),
                'canonical_url' => $this->input->post('canonical_url'),
                'no_index' => $no_index,
                'no_follow' => $no_follow,
                'no_archive' => $no_archive,
                'og_title' => $this->input->post('og_title'),
                'og_description' => $this->input->post('og_description'),
                'og_image' => $this->input->post('og_image'),
                'interlink_title' => $this->input->post('interlink_title'),
                'interlink_content' => $this->input->post('interlink_content'),
                'updated_on' => date('Y-m-d h:i:s'),
                'updated_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            if($image_file != FALSE)
            {
                $data['image'] = $image_file;
                if(trim($this->input->post('old_file_name')) != "")
                {
                    $old_file_image = FCPATH.$this->input->post('old_file_name');
                    if(file_exists($old_file_image))
                        unlink($old_file_image);
                }
            }
            
            if($interlink_image != FALSE)
            {
                $data['interlink_image'] = $interlink_image;
                if(trim($this->input->post('old_inter_file_name')) != "")
                {
                    $old_inetr_file_image = FCPATH.$this->input->post('old_inter_file_name');
                    if(file_exists($old_inetr_file_image))
                        unlink($old_inetr_file_image);
                }
            }
            
            if(!empty($this->input->post('slug_name')))
            {
                $update_slug_data = array(
                    'name' => $this->input->post('slug_name')
                );
            }
            else
            {
                $slug_id_edit = $this->input->post('slug_id');
                $slug_query = $this->db->get_where('post_slugs', array('id' => $slug_id_edit));
                $slud_data = $slug_query->row_array();
                $update_slug_data = array(
                    'name' => $slud_data['name']
                );
            }
            
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 
            
            //Post Data Update
            $this->db->where('id', $id);
            //Update Post data
            $this->db->update('posts', $data);
            
            // Update post categories id
            $cat_ids = $this->input->post('categories_id');
            
            $post_cat_data = [];
            foreach ($cat_ids as $k => $cat_id)
            {
                $post_cat_data['cat_id'] = $cat_id;
                $post_cat_data['post_id'] = $id;
                
                $this->db->where('post_id', $id);
                $this->db->delete('posts_cat_ids');
                $this->db->insert('posts_cat_ids', $post_cat_data);
            }
             
            //Slug Data update
            $this->db->where('id', $this->input->post('slug_id'));
            $this->db->update('post_slugs', $update_slug_data);

            $this->db->trans_complete(); # Completing transaction

            if ($this->db->trans_status() === FALSE)
            {
                # Something went wrong.
                $this->db->trans_rollback();
                return FALSE;
            }
            else
            {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
                return TRUE;
            }
        }
    }

    public function delete_posts($id)
    {
        /*$this->db->where('pages.slug_id', $id);
        $this->db->where('pages.is_deleted', 0);
        $query = $this->db->get('pages');
        $count_record = $query->num_rows();
        
        if($count_record > 0)
        {
            return false;
        }
        else
        {*/
            $this->db->where('id', $id);
            $data = array('is_deleted' => 1);
            return $this->db->update('posts', $data);
        //}
    }
    
    function get_posts_arr() 
    {
        $this->db->select(array('id', 'name'));
        $query = $this->db->get('posts');
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $posts_arr[] = array('id' => $row->id, 'title' => $row->title);
            }
        } 
        else 
        {
            $posts_arr[] = FALSE;
        }
        return $posts_arr;
    }
    
    function get_posts_list() 
    {
        $this->db->select('posts.id, posts.title');
        $this->db->where('is_deleted', 0 );
        $this->db->from('posts')->order_by('posts.title','asc');
        $query = $this->db->get();
        foreach ($query->result_array() as $key => $data)
        {
            $list[$data['id']] = $data['title'];
        }
        return $list;
    }
    
    function uploadPdfFaq($imageFile)
    {
        if($imageFile['image']['name'] != "")
        {
            $files = $imageFile;

            $file_name = time()."_".basename($imageFile['image']['name']);
            $file_type = $imageFile['image']['type'];
            $file_tmp_name = $imageFile['image']['tmp_name'];
            $file_error = $imageFile['image']['error'];
            $file_size  = $imageFile['image']['size'];

            $target_dir  = FCPATH."assets/template/uploads/posts/";
            $target_file = $target_dir . $file_name;
            $file_path   = "assets/template/uploads/posts/".$file_name;

            if(move_uploaded_file($file_tmp_name, $target_file)) 
            {
                return $file_path;
            }
            else
            {
                return FALSE;
            }
        }
    }
    
    function uploadInterLinkImgae($imageFile)
    {
        if($imageFile['interlink_image']['name'] != "")
        {
            $files = $imageFile;

            $file_name = 'inter_'.time()."_".basename($imageFile['interlink_image']['name']);
            $file_type = $imageFile['interlink_image']['type'];
            $file_tmp_name = $imageFile['interlink_image']['tmp_name'];
            $file_error = $imageFile['interlink_image']['error'];
            $file_size  = $imageFile['interlink_image']['size'];

            $target_dir  = FCPATH."assets/template/uploads/posts/";
            $target_file = $target_dir . $file_name;
            $file_path   = "assets/template/uploads/posts/".$file_name;

            if(move_uploaded_file($file_tmp_name, $target_file)) 
            {
                return $file_path;
            }
            else
            {
                return FALSE;
            }
        }
    }
    
}