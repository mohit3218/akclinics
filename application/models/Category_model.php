<?php

class Category_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function categories_view_search_coundtiotn($s_data)
    {
        $whr = NULL;

        $name = $s_data['name'];
        $is_active = $s_data['is_active'];
        
        if ($name != '')
        {
            $whr[] = array('field' => 'name', 'field_type' => 'like', 'value' => $name);
        }
        if ($is_active != '')
        {
            $whr[] = array('field' => 'is_active', 'field_type' => 'single', 'value' => $is_active);
        }

        return $whr;
    }
    
    function count_categories($where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        $query = $this->db->get('categories');
        
        return $query->num_rows();
    }
    
    function get_categories_data($limit, $start, $search, $where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        
        $start = max(0, ( $start - 1 ) * $limit);
        $this->db->limit($limit, $start);
        $this->db->where('is_deleted', 0);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('categories');
        
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $out[] = array(
                    'id' => $row->id,
                    'name' => $row->name,
                    'slug' => $row->slug,
                    'document_title' => $row->document_title,
                    'meta_description' => $row->meta_description,
                    'meta_keywords' => $row->meta_keywords,
                    'is_active' => $row->is_active
                );
            }
        }
        else
        {
            $out = [];
        }
        return $out;
    }
    
    public function get_categories()
    {
        $this->db->where('is_deleted', 0);
        $query = $this->db->get('categories');
        return $query->result_array();
    }

    public function get_categories_by_id($id = 0)
    {
        if ($id === 0)
        {
            $query = $this->db->get('categories');
            return $query->result_array();
        }

        $query = $this->db->get_where('categories', array('id' => $id));
        return $query->row_array();
    }

    public function add_categories($id = 0)
    {
        $this->load->helper('url');

        $slug = url_title($this->input->post('name'), 'dash', TRUE);

        $data = array();
         
        if ($id == 0)
        {
            $no_index = $no_follow = $no_archive = 0;
            if(isset($_POST['no_index']) && $_POST['no_index'] == 'on'){$no_index  = 1;}
            if(isset($_POST['no_follow']) && $_POST['no_follow'] == 'on'){$no_follow  = 1;}
            if(isset($_POST['no_archive']) && $_POST['no_archive'] == 'on'){$no_archive  = 1;}
            
            $data = array(
                'name' => $this->input->post('name'),
                'slug' => $this->input->post('slug'),
                'document_title' => $this->input->post('document_title'),
                'meta_description' => $this->input->post('meta_description'),
                'meta_keywords' => $this->input->post('meta_keywords'),
                'no_index' => $no_index,
                'no_follow' => $no_follow,
                'no_archive' => $no_archive,
                'created_on' => date('Y-m-d h:i:s'),
                'created_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            return $this->db->insert('categories', $data);
        }
        else
        {
            $no_index = $no_follow = $no_archive = 0;
            if(isset($_POST['no_index']) && $_POST['no_index'] == 'on'){$no_index  = 1;}
            if(isset($_POST['no_follow']) && $_POST['no_follow'] == 'on'){$no_follow  = 1;}
            if(isset($_POST['no_archive']) && $_POST['no_archive'] == 'on'){$no_archive  = 1;}
            
            $data = array(
                'name' => $this->input->post('name'),
                'slug' => $this->input->post('slug'),
                'document_title' => $this->input->post('document_title'),
                'meta_description' => $this->input->post('meta_description'),
                'meta_keywords' => $this->input->post('meta_keywords'),
                'no_index' => $no_index,
                'no_follow' => $no_follow,
                'no_archive' => $no_archive,
                'updated_on' => date('Y-m-d h:i:s'),
                'updated_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            $this->db->where('id', $id);
            return $this->db->update('categories', $data);
        }
    }

    public function delete_categories($id)
    {
        /*$this->db->where('pages.slug_id', $id);
        $this->db->where('pages.is_deleted', 0);
        $query = $this->db->get('pages');
        $count_record = $query->num_rows();
        
        if($count_record > 0)
        {
            return false;
        }
        else
        {*/
            $this->db->where('id', $id);
            $data = array('is_deleted' => 1);
            return $this->db->update('categories', $data);
        //}
    }
    
    function get_categories_arr() 
    {
        $this->db->select(array('id', 'name'));
        $query = $this->db->get('categories');
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $categories_arr[] = array('id' => $row->id, 'name' => $row->name);
            }
        } 
        else 
        {
            $categories_arr[] = FALSE;
        }
        return $categories_arr;
    }
    
    function get_categories_list() 
    {
        $this->db->select('categories.id, categories.name');
        $this->db->where('is_deleted', 0 );
        $this->db->from('categories')->order_by('categories.name','asc');
        $query = $this->db->get();
        foreach ($query->result_array() as $key => $data)
        {
            $list[$data['id']] = $data['name'];
        }
        return $list;
    }
    
    public function get_categories_meta_data($id)
    {
        $query = $this->db->get_where('categories', array('id' => $id));
        return $query->row_array();
    }

}
