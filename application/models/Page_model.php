<?php

class Page_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function pages_view_search_coundtiotn($s_data)
    {
        $whr = NULL;

        $slug_id = $s_data['slug_id'];
        $name = $s_data['name'];
        $is_active = $s_data['is_active'];
        
        if ($slug_id != '')
        {
            $whr[] = array('field' => 'slug_id', 'field_type' => 'single', 'value' => $slug_id);
        }
        if ($name != '')
        {
            $whr[] = array('field' => 'name', 'field_type' => 'like', 'value' => $name);
        }
        if ($is_active != '')
        {
            $whr[] = array('field' => 'is_active', 'field_type' => 'single', 'value' => $is_active);
        }

        return $whr;
    }
    
    function count_pages($where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        $query = $this->db->get('pages');
        
        return $query->num_rows();
    }
    
    function get_pages($limit, $start, $search, $where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        
        $start = max(0, ( $start - 1 ) * $limit);
        $this->db->limit($limit, $start);
        $this->db->where('is_deleted', 0);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('pages');
        
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $out[] = array(
                    'id' => $row->id,
                    'name' => $row->name,
                    'slug_id' => $row->slug_id,
                    'meta_title' => $row->meta_title,
                    'meta_keywords' => $row->meta_keywords,
                    'meta_description' => $row->meta_description,
                    'og_title' => $row->og_title,
                    'og_description' => $row->og_description,
                    'og_image' => $row->og_image,
                    'canonical_url' => $row->canonical_url,
                    'is_active' => $row->is_active
                );
            }
        }
        else
        {
            $out = [];
        }
        return $out;
    }
    
    /*public function get_pages()
    {
        $query = $this->db->get('pages');
        return $query->result_array();
    }*/

    public function get_page_by_id($id = 0)
    {
        if ($id === 0)
        {
            $query = $this->db->get('pages');
            return $query->result_array();
        }

        $query = $this->db->get_where('pages', array('id' => $id));
        return $query->row_array();
    }

    public function add_page($id = 0)
    {
        $this->load->helper('url');

        $slug = url_title($this->input->post('name'), 'dash', TRUE);

        $data = array();
         
        if ($id == 0)
        {
            $data = array(
                'name' => $this->input->post('name'),
                'slug_id' => $this->input->post('slug_id'),
                'meta_title' => $this->input->post('meta_title'),
                'meta_keywords' => $this->input->post('meta_keywords'),
                'meta_description' => $this->input->post('meta_description'),
                'og_title' => $this->input->post('og_title'),
                'og_description' => $this->input->post('og_description'),
                'og_image' => $this->input->post('og_image'),
                'canonical_url' => $this->input->post('canonical_url'),
                'created_on' => date('Y-m-d h:i:s'),
                'created_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            return $this->db->insert('pages', $data);
        }
        else
        {
            $data = array(
                'name' => $this->input->post('name'),
                'slug_id' => $this->input->post('slug_id'),
                'meta_title' => $this->input->post('meta_title'),
                'meta_keywords' => $this->input->post('meta_keywords'),
                'meta_description' => $this->input->post('meta_description'),
                'og_title' => $this->input->post('og_title'),
                'og_description' => $this->input->post('og_description'),
                'og_image' => $this->input->post('og_image'),
                'canonical_url' => $this->input->post('canonical_url'),
                'updated_on' => date('Y-m-d h:i:s'),
                'updated_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            $this->db->where('id', $id);
            return $this->db->update('pages', $data);
        }
    }

    public function delete_page($id)
    {
        $this->db->where('id', $id);
        $data = array('is_deleted' => 1);
        return $this->db->update('pages', $data);
    }
    
    
    public function get_page_list_id($id)
    {
        $query = $this->db->get_where('pages', array('id' => $id));
        return $query->row_array();
    }

}
