<?php

class Slug_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function slugs_view_search_coundtiotn($s_data)
    {
        $whr = NULL;

        $name = $s_data['name'];
        $is_active = $s_data['is_active'];
        
        if ($name != '')
        {
            $whr[] = array('field' => 'name', 'field_type' => 'like', 'value' => $name);
        }
        if ($is_active != '')
        {
            $whr[] = array('field' => 'is_active', 'field_type' => 'single', 'value' => $is_active);
        }

        return $whr;
    }
    
    function count_slugs($where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        $query = $this->db->get('slugs');
        
        return $query->num_rows();
    }
    
    function get_slugs_data($limit, $start, $search, $where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        
        $start = max(0, ( $start - 1 ) * $limit);
        $this->db->limit($limit, $start);
        $this->db->where('is_deleted', 0);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('slugs');
        
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $out[] = array(
                    'id' => $row->id,
                    'name' => $row->name,
                    'is_active' => $row->is_active
                );
            }
        }
        else
        {
            $out = [];
        }
        return $out;
    }
    
    public function get_slugs()
    {
        $this->db->where('is_deleted', 0);
        $query = $this->db->get('slugs');
        return $query->result_array();
    }

    public function get_slug_by_id($id = 0)
    {
        if ($id === 0)
        {
            $query = $this->db->get('slugs');
            return $query->result_array();
        }

        $query = $this->db->get_where('slugs', array('id' => $id));
        return $query->row_array();
    }

    public function add_slug($id = 0)
    {
        $this->load->helper('url');

        $slug = url_title($this->input->post('name'), 'dash', TRUE);

        $data = array();
         
        if ($id == 0)
        {
            $data = array(
                'name' => $this->input->post('name'),
                'created_on' => date('Y-m-d h:i:s'),
                'created_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            return $this->db->insert('slugs', $data);
        }
        else
        {
            $data = array(
                'name' => $this->input->post('name'),
                'updated_on' => date('Y-m-d h:i:s'),
                'updated_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            $this->db->where('id', $id);
            return $this->db->update('slugs', $data);
        }
    }

    public function delete_slug($id)
    {
        $this->db->where('pages.slug_id', $id);
        $this->db->where('pages.is_deleted', 0);
        $query = $this->db->get('pages');
        $count_record = $query->num_rows();
        
        if($count_record > 0)
        {
            return false;
        }
        else
        {
            $this->db->where('id', $id);
            $data = array('is_deleted' => 1);
            return $this->db->update('slugs', $data);
        }
    }
    
    function get_slugs_arr() 
    {
        $this->db->select(array('id', 'name'));
        $query = $this->db->get('slugs');
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $slugs_arr[] = array('id' => $row->id, 'name' => $row->name);
            }
        } 
        else 
        {
            $slugs_arr[] = FALSE;
        }
        return $slugs_arr;
    }
    
    function get_slugs_list() 
    {
        $this->db->select('slugs.id, slugs.name');
        //$this->db->where('is_deleted', 0 );
        $this->db->from('slugs')->order_by('slugs.name','asc');
        $query = $this->db->get();
        foreach ($query->result_array() as $key => $data)
        {
            $list[$data['id']] = $data['name'];
        }
        return $list;
    }

}
