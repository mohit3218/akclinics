<?php

class Common_functions extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }
    
    function get_all_slugs_arr() 
    {
        $this->db->select(array('id', 'name'));
        $query = $this->db->get('slugs');
        
        if ($query->num_rows > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $slugs_arr[] = array('id' => $row->id, 'name' => $row->name);
            }
        } 
        else 
        {
            $slugs_arr[] = FALSE;
        }
        return $slugs_arr;
    }

    function get_where_conditions($whr)
    {
        foreach ($whr as $where)
        {
            $where['field_type'];
            if ($where['field_type'] == 'single')
            {
                $this->db->where($where['field'], $where['value']);
            }
            
            if ($where['field_type'] == 'like')
            {
                $this->db->like($where['field'], $where['value']);
            }
            
            if ($where['field_type'] == 'or_and')
            {
                $or_and_condition = $where['or_and_condition'];
                $this->db->where($or_and_condition);
            }
        }
    }
    
    public function user_list_arr()
    {
        $this->db->select(array('id', 'first_name', 'last_name'));
        $query = $this->db->get('users');
        
        $users_arr = [];
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row) 
            {
                $users_arr[] = array('id' => $row->id, 'name' => $row->first_name . ' ' . $row->last_name);
            }
        }
        else 
        {
            $users_arr;
        }
        
        return $users_arr;
    }
    
    public function users_list()
    {
        $this->db->select('users.id, users.first_name, users.last_name');
        $query = $this->db->get('users');
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result_array() as $key => $data) 
            {
                $users_list[$data['id']] = $data['first_name'] . ' ' .  $data['last_name'];
            }
        } 
        
        return $users_list;
    }
}
