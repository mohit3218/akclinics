<?php

class Blog_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function blogs_view_search_coundtiotn($s_data)
    {
        $whr = NULL;

        $title = $s_data['title'];
        $categories_id = $s_data['categories_id'];
        $post_blog_sts = $s_data['blog_status'];
        $is_active = $s_data['is_active'];
        
        if ($title != '')
        {
            $whr[] = array('field' => 'title', 'field_type' => 'like', 'value' => $title);
        }
        if ($categories_id != '')
        {
            $whr[] = array('field' => 'categories_id', 'field_type' => 'single', 'value' => $categories_id);
        }
        if ($post_blog_sts != '')
        {
            $whr[] = array('field' => 'blog_status', 'field_type' => 'single', 'value' => $post_blog_sts);
        }
        if ($is_active != '')
        {
            $whr[] = array('field' => 'is_active', 'field_type' => 'single', 'value' => $is_active);
        }

        return $whr;
    }
    
    function count_blogs($where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        $query = $this->db->get('blogs');
        
        return $query->num_rows();
    }
    
    function get_blogs_data($limit, $start, $search, $where)
    {
        if ($where != '')
        {
            $this->common_functions->get_where_conditions($where);
        }
        
        $start = max(0, ( $start - 1 ) * $limit);
        $this->db->limit($limit, $start);
        $this->db->where('is_deleted', 0);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('blogs');
        
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $out[] = array(
                    'id' => $row->id,
                    'categories_id' => $row->categories_id,
                    'title' => $row->title,
                    'content' => $row->content,
                    'image' => $row->image,
                    'blog_url' => $row->blog_url,
                    'blog_status' => $row->blog_status,
                    'blog_post_date' => $row->blog_post_date,
                    'created_on' => $row->created_on,
                    'is_active' => $row->is_active,
                    'is_cron' => $row->is_cron
                );
            }
        }
        else
        {
            $out = [];
        }
        return $out;
    }
    
    public function get_blogs()
    {
        $this->db->where('is_deleted', 0);
        $query = $this->db->get('blogs');
        return $query->result_array();
    }

    public function get_blogs_by_id($id = 0)
    {
        if ($id === 0)
        {
            $query = $this->db->get('blogs');
            return $query->result_array();
        }

        $query = $this->db->get_where('blogs', array('id' => $id));
        return $query->row_array();
    }

    public function add_blogs($id = 0)
    {
        $this->load->helper('url');

        $data = array();
         
        if ($id == 0)
        {
            $image_file = "";
            if($_FILES['image']['name'] != "")
            {
                $image_file = $this->uploadPdfFaq($_FILES, $this->input->post('categories_id'));

                if($image_file == FALSE)
                    $image_file = "";
            }
        
            $data = array(
                'title' => $this->input->post('title'),
                'categories_id' => $this->input->post('categories_id'),
                'content' => $this->input->post('content'),
                'image' => $image_file,
                'blog_url' => $this->input->post('blog_url'),
                'blog_status' => $this->input->post('blog_status'),
                'blog_post_date' => $this->input->post('blog_post_date'),
                'created_on' => date('Y-m-d h:i:s'),
                'created_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            return $this->db->insert('blogs', $data);
        }
        else
        {
            $image_file = FALSE;
            if($_FILES['image']['name'] != "")
                $image_file = $this->uploadPdfFaq($_FILES, $this->input->post('categories_id'));

            if(isset($data['status']))
            {
                $data['status'] = "Active";
            }
            else
            {
                $data['status'] = 'InActive';
            }
            
        
            $data = array(
                'title' => $this->input->post('title'),
                'categories_id' => $this->input->post('categories_id'),
                'content' => $this->input->post('content'),
                'blog_url' => $this->input->post('blog_url'),
                'blog_status' => $this->input->post('blog_status'),
                'blog_post_date' => $this->input->post('blog_post_date'),
                'updated_on' => date('Y-m-d h:i:s'),
                'updated_by' => $this->ion_auth->user()->row()->id,
                'is_active' => 1
            );
            
            if($image_file != FALSE)
            {
                $data['image'] = $image_file;
                if(trim($this->input->post('old_file_name')) != "")
                {
                    $old_file_image = FCPATH.$this->input->post('old_file_name');
                    if(file_exists($old_file_image))
                        unlink($old_file_image);
                }
            }
            
            $this->db->where('id', $id);
            return $this->db->update('blogs', $data);
        }
    }

    public function delete_blogs($id)
    {
        /*$this->db->where('pages.slug_id', $id);
        $this->db->where('pages.is_deleted', 0);
        $query = $this->db->get('pages');
        $count_record = $query->num_rows();
        
        if($count_record > 0)
        {
            return false;
        }
        else
        {*/
            $this->db->where('id', $id);
            $data = array('is_deleted' => 1);
            return $this->db->update('blogs', $data);
        //}
    }
    
    function get_blogs_arr() 
    {
        $this->db->select(array('id', 'name'));
        $query = $this->db->get('blogs');
        
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $blogs_arr[] = array('id' => $row->id, 'title' => $row->title);
            }
        } 
        else 
        {
            $blogs_arr[] = FALSE;
        }
        return $blogs_arr;
    }
    
    function get_blogs_list() 
    {
        $this->db->select('blogs.id, blogs.title');
        $this->db->where('is_deleted', 0 );
        $this->db->from('blogs')->order_by('blogs.title','asc');
        $query = $this->db->get();
        foreach ($query->result_array() as $key => $data)
        {
            $list[$data['id']] = $data['title'];
        }
        return $list;
    }
    
    function uploadPdfFaq($imageFile, $categories_id)
    {
        if($imageFile['image']['name'] != "")
        {
            $files = $imageFile;

            $file_name = $categories_id.'_'.time()."_".basename($imageFile['image']['name']);
            $file_type = $imageFile['image']['type'];
            $file_tmp_name = $imageFile['image']['tmp_name'];
            $file_error = $imageFile['image']['error'];
            $file_size  = $imageFile['image']['size'];

            $target_dir  = FCPATH."assets/template/uploads/blogs/";
            $target_file = $target_dir . $file_name;
            $file_path   = "assets/template/uploads/blogs/".$file_name;

            if(move_uploaded_file($file_tmp_name, $target_file)) 
            {
                return $file_path;
            }
            else
            {
                return FALSE;
            }
        }
    }

}
