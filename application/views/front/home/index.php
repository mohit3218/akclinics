<link rel="preload" href="<?php echo cdn('assets/template/frontend/'); ?>css/easy-responsive-tabs-min.css" as="style" onload="this.onload = null;this.rel = 'stylesheet'">
<noscript><link rel="stylesheet" href="<?php echo cdn('assets/template/frontend/'); ?>css/easy-responsive-tabs-min.css"></noscript>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner">
		<div class="carousel-item active"> 
			<img class="img-fluid" src="<?php echo cdn('assets/template/frontend/img/'); ?>slide-1.jpg" alt="">
			<div class="container">
				<div class="carousel-caption">
					<p class="b-h4"><span class="white-line"> _______</span>India's leading Hair Transplant Clinics <span class="white-line"> _______</span></p>
					<p class="b-h1">Great Results at Affordable Cost </p>
					<p class="b-h3"><img src="<?= cdn('assets/template/frontend/images/'); ?>Trust-Quality-Results.png" alt="India's best Hair & Skin Clinic" class="img-fluid img-resize"></p>
					<div class="row">
						<div class="col-md-6 col-12">
							<p class="text-h3"><i class="fa fa-check fa-1x" aria-hidden="true"></i>Hassle Free Experience</p>
						</div>
						<div class="col-md-6 col-12">
							<p class="text-h3"><i class="fa fa-check fa-1x" aria-hidden="true"></i>Absolutely Natural Results</p>
						</div>
					</div>
					<a class="btn  btn-outline text-center ptop" href="<?= base_url(); ?>contact-us/" target="_blank">Free Online Consultation</a> </div>
			</div>
		</div>
	</div>
</div>
<div class="orange-area bg-black">
	<div class="container">
		<div class="row">
			<div class="col-md-9 animated fadeInLeft">
				<p class="mg-0 text-left text-white banner-font-small ctr"> Founded in the year 2007, AK Clinics is lead by able leadership of a team that understands that every patient is important and deserves care. Call us for any questions that you may have </p>
			</div>
			<div class="col-md-3 text-center animated fadeInRight"> <a class="btn-outline mt-1">+91 977 9944 207</a> </div>
		</div>
	</div>
</div>
<div class="service-area ptb-3"> 
	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container text-center animated fadeInUp delay-2s">
			<h1 class="black-text animated fadeInLeft delay-1s">India's Leading Hair, Skin & Cosmetic Surgery Clinic</h1>
			<h3 class="orange-text animated fadeInRight delay-1s">New Delhi, Bangalore, Ludhiana</h3>
			<p class="ptb-1 animated fadeInUp delay-1s">This is not mere statement but a religion with us. Our team strives very hard to meet the ever expanding needs & wishes of the patients. AK Clinics is amongst the fastest growing Cosmetic Surgery Clinics having served 1000s patients every year in our various branches. We work very hard to achieve excellence in the services especially Hair Transplant. This is the reason all our standards are at par or better than the best in the world.</p>
		</div>
		<div class="container-fluid animated fadeInUp delay-2s ">
			<div class="regular slider">
				<div>
				  <div class="card mb-3 box-shadow">
						<img data-src="<?php echo cdn('assets/template/frontend/'); ?>img/service-4.png" class="card-img-top img-fluid lazy" alt="Hair Transplant">
						<div class="card-body text-center"> <strong>Hair Transplant</strong>
							<p class="card-text"> World’s best hair transplant surgeons who performed more than 2000+ successful surgeries with outstanding results. </p>
							<div class="d-flex justify-content-between align-items-center card-box-btn">
								<div class="btn-group"> <a href="<?= base_url(); ?>hair-transplant/" target="_blank" class="btn btn-sm btn-outline"> Read More <i class="icofont-long-arrow-right"></i> </a> </div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<div class="card mb-3 box-shadow"> 
						<img class="card-img-top img-fluid lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/service-1.png"  alt="Hair Restoration">
						<div class="card-body  text-center"> <strong>Hair Restoration</strong>
							<p class="card-text"> AK Clinics offers the complete services ranging from medical treat to surgical treatment to non-surgical treatment. </p>
							<div class="d-flex justify-content-between align-items-center card-box-btn">
								<div class="btn-group"> <a href="<?= base_url(); ?>hair-restoration/" target="_blank" class="btn btn-sm btn-outline"> Read More <i class="icofont-long-arrow-right"></i> </a> </div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<div class="card mb-3 box-shadow"> 
						<img class="card-img-top img-fluid lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/service-3.png"  alt="Cosmetic Surgery">
						<div class="card-body  text-center"> <strong>Cosmetic Surgery</strong>
							<p class="card-text"> We understand privacy & concerns of ever patient planning a cosmetic surgery. We guide the patient step by step to make the journey easy. </p>
							<div class="d-flex justify-content-between align-items-center card-box-btn">
								<div class="btn-group"> <a href="<?= base_url(); ?>cosmetic-surgery/" target="_blank" class="btn btn-sm btn-outline"> Read More <i class="icofont-long-arrow-right"></i></a> </div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<div class="card mb-3 box-shadow"> 
						<img class="card-img-top img-fluid lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/service-2.png"  alt="Cosmetology">
						<div class="card-body  text-center"> <strong>Cosmetology</strong>
							<p class="card-text"> Our doctors are patient friendly and suggest a treatment plans which give results and are pocket friendly. </p>
							<div class="d-flex justify-content-between align-items-center card-box-btn">
								<div class="btn-group"> <a href="<?= base_url(); ?>cosmetology/" target="_blank" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i> </a> </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- START THE FEATURETTES --> 
</div>
<div class="service-area pb-3">
	<div class="container ">
		<div class="row">
			<div class="col-md-8 animated fadeInLeft delay-2s">
				<div class="section-title-1 text-left">
					<h2 class="black-text">AK Clinics - Center for world class procedures</h2>
					 
					<p><em><strong>“AK Clinics was founded with the goal of transformation of looks of the entire human race. The purpose of existence of AK Clinics is to provide excellent treatments at an affordable cost.”</strong></em><br />
This is not mere statement but a religion with us. Our team strives very hard to meet up with ever expanding needs & wishes of the patients. AK Clinics is amongst the fastest growing cosmetic surgery clinics having served thousands patients every year in our various branches. We’ve been working very hard to achieve excellence in the services we provide especially hair transplant. This is the reason all our efforts & standards are at par or better than the best in the world.</p>
				</div>
			</div>
			<div class="col-md-4 animated fadeInRight delay-2s">
				<div class="cta-content marbot40 pt-3">
					<ul class="side-menu list-group media">
						<li class="list-group-item"><a href="<?= base_url(); ?>results/videos/"><i class="fa fa-eye fa-1x" aria-hidden="true"></i><span class="media-body">Videos</span></a></li>
						<li class="list-group-item"><a href="<?= base_url(); ?>results/"><i class="fa fa-user-circle fa-1x"></i><span class="media-body">Before & After Results</span></a></li>
						<li class="list-group-item"><a href="<?= base_url(); ?>about-us/our-team/"><i class="fa fa-users fa-1x"></i><span class="media-body">Our Team</span></a></li>
						<li class="list-group-item"><a href="<?= base_url(); ?>contact-us/"><i class="fa fa-times-circle-o fa-1x"></i><span class="media-body">Free Consultation</span></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="service-area bg-light-gray ptb-3 animated fadeInRight delay-1s">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-6 animated fadeInLeft delay-2s">
				<h3 class="font-weight-bold">Watch our Results</h3>
				<p>Our results are testimony to the quality service delivery of our doctors & team. All our hair transplant results, hairless results, cosmetic surgery results or skin treatment results are purely due to standardised practices that we follow at all clinics.</p>
				<p> It is medically not possible to get 100% results but we strive very hard to remain as close as possible. </p>
				<div class=" ptb-1"><a class="btn-gradient" data-scroll="" href="<?= base_url() ?>results/" role="button" target="_blank">Our Results</a></div>
			</div>
			<div class="col-12 col-lg-6 video-part animated fadeInRight delay-3s"> 
				<a href="https://www.youtube.com/watch?v=RWKZS72xz3Y" target="_blank" class="media-lightbox">
					<div class="embed-responsive embed-responsive-16by9" role="img" aria-label="hair transplant results">
						<div class="embed-responsive-item element-cover-bg bg-cover"> <span class="play-wrapper"> <i class="fa fa-play-circle-o fa-4x"></i> </span> </div>
					</div>
				</a> 
			</div>
		</div>
	</div>
</div>
<div class="divider parallax layer-overlay overlay-black animated fadeIn delay-5s" data-bg-img="<?php echo cdn('assets/template/frontend/'); ?>img/testimonial.jpg" style="background-image: url(&quot;<?php echo cdn('assets/template/frontend/'); ?>img/testimonial.jpg&quot;); ">
	<div class="container p-3">
		<div class="row">
			<div class="col-md-12 text-center text-white">
				<h2>Testimonials</h2>
			</div>
			<div class="col-md-12 text-center text-white">
				<div id="carouselExampleIndicators" class="carousel slide text-white" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
					</ol>
					<div class="carousel-inner mt-4  text-white">
						<div class="carousel-item text-center  text-white item-index">
							<div class="img-box p-1 border rounded-circle m-auto"> 
								<img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>raj-n.jpg" alt="Raj Jaswal"> 
							</div>
							<h5 class="mt-4"><strong class=" text-uppercase">Raj Jaswal</strong></h5>
							<!--<h6 class="text-white">Web Developer</h6>-->
							<p class="m-0 pt-2 text-white"> I started facing hair loss at the age of 26 but it was not very noticeable. But at 29 
								the hair loss progressed very quickly and crown almost became empty. And further my 
								hair line receded quickly. Now at the age of 31 I started looking very old. 
								I tried many medicines like homeopathic etc etc but nothing seems to work. 
								Then I started searching for my options and finally decided for a hair transplant. </p>
						</div>
						<div class="carousel-item text-center item-index">
							<div class="img-box p-1 border rounded-circle m-auto"> 
								<img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>supal.jpg " alt="Vikalp Gaur"> 
							</div>
							<h5 class="mt-4 "><strong class="text-uppercase">Vikalp Gaur</strong></h5>
							<!--<h6 class="text-white">Web Designer</h6>-->
							<p class="m-0 pt-2 text-white"> I can see good results post-transplant. Overall good. As suggested, I may go for 
								further transplant for crown area. I am currently taking 6 months cyclical therapy 
								again to stop hair fall. Then will take the decision accordingly. I can say I would 
								prefer someone for A K Clinic. As it is giving person the way to live more happily. </p>
						</div>
						<div class="carousel-item text-center active item-index">
							<div class="img-box p-1 border rounded-circle m-auto"> 
								<img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="Rahul Bhatia"> 
							</div>
							<h5 class="mt-4"><strong class="text-uppercase">Rahul Bhatia</strong></h5>
							<!--<h6 class="text-white ">Seo Analyst</h6>-->
							<p class="m-0 pt-2 text-white"> Had a hair transplant at AK clinic under care of Doc. KAPIL DUA.And after treatment it 
								has been 3 month now and I have very much inproved growing hair. He is best surgeon 
								ever seen and very experienced in this art. They also provided very good atmosphere 
								while surgery. With his experience he gave very confident to his patients while surgery. </p>
						</div>
						<div class="carousel-item text-center item-index">
							<div class="img-box p-1 border rounded-circle m-auto"> 
								<img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="Suresh"> 
							</div>
							<h5 class="mt-4"><strong class="text-uppercase">Suresh</strong></h5>
							<!--<h6 class="text-white ">Seo Analyst</h6>-->
							<p class="m-0 pt-2 text-white"> After talking to Dr Kapil I have decided to go for a hair transplant surgery which turned
								out to be very good. Total of 6000 grafts were transplanted. The density was medium 
								density. The procedure was FUE without any scar. The procedure was by done both Dr 
								Kapil and Dr Aman. I strongly recommend anyone who is looking for a FUE surgery in India. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('front/partials/blog_block'); ?>
<?php $this->load->view('front/partials/location_block'); ?>
<script defer="defer" src="<?php echo cdn('assets/template/frontend/'); ?>js/easy-responsive-tabs.min.js"></script> 
<script>
	$(document).ready(function () {
		$('#horizontalTab').easyResponsiveTabs({
			type: 'default',
			width: 'auto',
			fit: true,
			closed: 'accordion',
			activate: function (event) {
				var $tab = $(this);
				var $info = $('#tabInfo');
				var $name = $('span', $info);
				$name.text($tab.text());
				$info.show();
			}
		});
	});
</script>