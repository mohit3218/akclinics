<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>bangalore/"> <span itemprop="name">Best Skin Care Clinic in Bangalore</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>bangalore/laser-acne-scar-removal-treatment/"> <span itemprop="name">Laser Acne Scar Removal Treatment</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area">
    <div class="container ptb-3 ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Acne Scar Laser Treatment</h1>
                </div>
                <p>There would be a rare few people in this world, who, at one time or the other, have not been troubled by acne. Pimples have been around for teenagers since time immemorial and while for most people the acne disappears when they reach adulthood, there are some people who are troubled by it, even after. While most people will ignore their acne, there are those who will be truly worried by it – they will opt for all kinds of creams and lotions to get rid of it, but the fact of the matter is that for most people, acne is a huge problem.
                    If you are looking to find the best acne treatment in Bangalore, then there is only one place that you need to go to – AK Clinics. We offer some of the best acne treatment services and we can help you not only remove the acne, but also make sure that it does not return.</p>
                <br />
                <h4>What is acne?</h4>
                <p>Our skin has pores, and when dirt, grime and oil from our daily routine clogs those pores, they lead to acne. If your skin is prone to acne, then your pores will get clogged with dead skin cells, even faster than normal ones. Healthy pores normally shed a single layer of dead skin cells a day, but if your skin is prone to acne, then the pores could be shedding up to five layers of dead skin each day! Because of this excessive shedding, the pores are unable to clear themselves out, leading to build up.</p>
            </div>
<div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>bangalore/best-hair-transplant-clinic/">Best Hair transplant Clinic</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/hair-loss-treatment/">Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-acne-scar-removal-treatment/">Laser Acne Scar Removal Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-hair-removal-for-men-and-women/">Laser Hair Removal For Men and Women</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-skin-treatment/">Laser Skin Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/prp-hair-loss-treatment/">PRP Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/scalp-micro-pigmentation/">Scalp Micro Pigmentation</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/skin-whitening-lightening-treatment/">Skin Whitening Lightening Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/stretch-marks-removal-treatment/">Stretch Marks Removal Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area bg-light-gray ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12  text-center">
                <div class="section-title">
                    <h1 class="black-text">What are the Different Types of Acne?</h2><p>Before treating acne, you need to understand the type – when you come to AK Clinics for acne treatment in Bangalore, our team of doctors will make sure that they take the time to diagnose the type of acne that is afflicting you and the plan of action will be based on that. There are basically two types of acne – inflammatory and non-inflammatory. Here are the main types of acne:</p>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row  text-center">
                <div class="col-md-4 ">
                    <h4>Blackheads</h4>
                    <p> When pores become clogged because of the accumulation of dead skin cells and sebum, it leads to the formation of blackheads. The top of the pore remains open, but the rest of the pore is clogged, which is why it looks black. </p>
                </div>
                <div class="col-md-4">
                    <h4>Whiteheads</h4>
                    <p>Just like blackheads, these are also formed when the pores get clogged because of dead skin cells and sebum. However, the top of the pore is closed, which is why it looks white. Whiteheads are a lot more difficult to treat, because the pores are closed, unlike in the blackheads. </p>
                </div>
                <div class="col-md-4">
                    <h4>Papules</h4>
                    <p>Pustules are formed when the walls surrounding the pores break down, and this is normally caused due to massive inflammation. Pustules are normally filled with pus and look like a red bump with a whitish or yellow head. </p>
                </div>
                <div class="clr"></div>
                <div class="col-md-4">
                    <h4>Papules </h4>
                    <p>Like pustules, papules are also formed when the wall surrounding the pores break down. This leads to the clogging of the pores and is often pinkish in colour. </p>
                </div>
                <div class="col-md-4">
                    <h4>Nodules</h4>
                    <p>When the clogged pores start to grow larger and the irritation increases, it becomes a nodule. Nodules are also deeper inside the skin and are much tougher to treat. </p>
                </div>
                <div class="col-md-4">
                    <h4>Cysts</h4>
                    <p>A cyst is formed when bacteria also accumulates in the pores, along with the dead skin cells and sebum. These cysts are much deeper than even nodules and can be extremely painful to the touch. Cysts are the most severe form of acne and can lead to severe scarring. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">How Does Acne Occur?</h2>
                    <p>When you walk into AK Clinics for acne scar treatment in Bangalore, our first step will be to educate you about how acne occurs. Although we will be handling everything associated with the treatment, we do find it helpful to ensure that our patients understand what is happening to their skin. Here is how acne is formed: </p></div>
            </div>
        </div>
        <div class="col-md-12 text-center">
            <div class="row">
                <div class="col-md-4">
                    <figure><img data-src="<?= cdn('assets/template/uploads/') ?>2015/06/acne-occurs-1.png" alt="Clogging of pore" title="Clogging of pore" width="159" height="159" class="aligncenter size-full wp-image-12437 lazy"></figure>
                    <p>Skin cells are always in a state of regeneration and when old skin cells die, new ones will replace them. However, for skins that are prone to acne, these dead skin cells are not removed, leading to the clogging of the pores. </p>
                </div>
                <div class="col-md-4 ptb-1">
                    <figure><img data-src="<?= cdn('assets/template/uploads/') ?>2015/06/acne-occurs-2.png" alt="Microcomedone" title="Microcomedone" width="159" height="159" class="aligncenter size-full wp-image-12437 lazy"></figure>
                    <p>When the dead skin cells start to build up inside the pores, the cells become sticky and are unable to escape out. The sticky cells become almost like a plug inside the pore. </p>
                </div>
                <div class="col-md-4 ptb-1">
                    <figure><img data-src="<?= cdn('assets/template/uploads/') ?>2015/06/acne-occurs-3.png" alt="Formation of Acne" title="Formation of Acne" width="159" height="159" class="aligncenter size-full wp-image-12437 lazy"></figure>
                    <p>When there are hormonal changes in the body, there is a higher level of oil production in the body and this leads to even more dead skin shedding. </p>
                </div>
                <div class="ptb-1"></div>
                <div class="col-md-4 ptb-1">
                    <figure><img data-src="<?= cdn('assets/template/uploads/') ?>2015/06/acne-occurs-4.png" alt="Blocked pore" title="Blocked pore" width="159" height="159" class="aligncenter size-full wp-image-12437 lazy"></figure>
                    <p>The oil in the skin works are the nutritional factor for the bacteria, which starts to grow exponentially, leading to completely blocked pores and inflammation. </p>
                </div>
                <div class="col-md-4 ptb-1">
                    <figure><img data-src="<?= cdn('assets/template/uploads/') ?>2015/06/acne-occurs-5.png" alt="Pimple or acne Scar" title="Pimple or acne Scar" width="159" height="159" class="aligncenter size-full wp-image-12437 lazy" srcset="<?= cdn('assets/template/uploads/') ?>2015/06/acne-occurs-5.png 159w, <?= cdn('assets/template/uploads/') ?>2015/06/acne-occurs-5-150x150.png 150w" sizes="(max-width: 159px) 100vw, 159px"></figure>
                    <p>Once the inflammation process is complete, there is the appearance of the bump, which is often filled with pus, and is known as a pimple or acne. </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray  ">
    <div class="container ">
        <div class="row">
            <div class="col-md-12  text-center">
                <div class="section-title">
                    <h2 class="black-text">What Causes Acne?</h2>
                    <p>There are several myths associated with acne, such as it appears only on the face and that if you eat too much of oily food, you will get more acne. The first thing that you need to understand is that acne can affect any part of your body, including your back, chest, neck, arms or even your buttocks. Understanding what causes acne is the first step to treating it and at AK Clinics, we consider this one of the most important steps for people who come to us for acne scar treatment in Bangalore.<br>
                        These are some of the main reasons that cause acne:
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-12  text-center">
            <div class="row">
                <div class="col-md-4">
                    <h4>Dirt and pollution </h4>
                    <p>There is no denying that the world that we live in is filled with dirt and pollution today and the minute we step out of our home, we and are skin are subjected to it. The dust, dirt and grime blocks the pores, while the gases and chemicals that are floating in the atmosphere can inflame the pores even further. The combination leads to the formation of acne. </p>
                </div>
                <div class="col-md-4">
                    <h4>Stress</h4>
                    <p>Although stress does not cause acne directly, it can definitely aggravate the same. When your body is stressed, the capillaries in the body tend to expand and this increases the blood flow. Because of the increased blood flow, your skin will look red and as if it is inflamed. Stress can also lead to increased androgen levels, which in turn will make the sebaceous glands secrete more sebum, leading to acne. </p>
                </div>
                <div class="col-md-4">
                    <h4>Cosmetics </h4>
                    <p>It is important to remember that most cosmetic products are made using some chemicals, and when you slather chemicals on your skin, you are inviting trouble. Cosmetics can clog the pores and prohibit skin from breathing properly. </p>
                </div>
                <div class="clr"></div>
                <div class="col-md-4">
                    <h4>Hormonal imbalance </h4>
                    <p>There are certain types of acne, such as Acne Vulgaris, which are caused by hormonal imbalances in the body. When there is a rise in the levels of androgen, the oil glands start producing more sebum, which leads to the rupturing of the skin cells. The bacteria that causes acne then blocks the pores, creating acne. </p>
                </div>
                <div class="col-md-4">
                    <h4>Medications </h4>
                    <p>Certain medicines, especially ones that contain steroids, bromides and iodides can bring upon acne or worsen existing acne. If you are taking anticonvulsant drugs, oral contraceptive pills or medicines for disorders like PCOS, you could be more prone to acne. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 ">
    <div class="container ">
        <div class="row">
            <div class="col-md-12  text-center">
                <div class="section-title">
                    <h1 class="black-text">How is Acne Treated?</h1>
                    <p>When you come to AK Clinics Bangalore, we ensure that we provide customised service to each 
                        client, because each person is different – each person’s skin is different, the cause for 
                        their acne will be different, which is why the same treatment will not work for everyone. 
                        We take the time to examine each person, find out what could be causing their acne, find out
                        more about their skin type and then finalise a course of treatment, which would be the best
                        option for them.Here are some of the most commonly used treatments for acne:
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-12  text-center ptb-1"> 
            <div class="row">
                <div class="col-md-3">
                    <h4>Combination therapy </h4>
                    <p>If your acne has just started, you are in luck, because our dermatologist might just prescribe some topical lotions and creams and perhaps a few antibiotics. The topical solutions will have a small concentration of isotretinoin, while the antibiotics could include erythromycin, doxycycline and azithromycin. However, if the acne is on the severe side, then you might be prescribed a stronger concentration of isotretinoin. </p>
                </div>
                <div class="col-md-3">
                    <h4>Chemical peels </h4>
                    <p>The basic idea behind chemical peels is that it removed the outer layer of the skin, which is normally the dead skin layer. When this layer of the skin is removed under a controlled method, there is a proper exfoliation and if there are any superficial lesions on the skin, they are removed as well. Once the exfoliation has been done, the new layers of skin start to develop again and this skin will be much healthier and fresher looking. Trichloroacetic acid Peel and Salicylic Acid peel are the best for acne. </p>
                </div>
                <div class="col-md-3">
                    <h4>Microdermabrasion</h4>
                    <p>This is one of the most popular non-invasive procedures for acne treatment. In this procedure, a specialised equipment, which has crystals on the tip is used to buff the dead skin cells away. The skin texture is resurfaced and all the dead pores are flushed out. Whether you have uneven skin tone, age spots or acne, microdermabrasion is the way to go. The procedure helps with the stimulation of new cells, making the skin glow and making it look clear and clean. </p>
                </div>
                <div class="col-md-3">
                    <h4>Laser resurfacing </h4>
                    <p>We are one of the leaders when it comes to providing laser treatment for acne scars in Bangalore and this is one of the best methods to remove acne and acne scars. A device that emits high energy amplified light is used to vaporise the outer layer of the skin, revealing healthy new skin, that lies just beneath. The laser can also kill the bacteria which can lead to acne. The most popular lasers used for acne treatment include CO2, pulsed dye or erbium YAG lasers. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3  bg-light-gray  ">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle ">What happens before, during and after the acne treatment?</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">What happens before, during and after the acne treatment?</h3>
                    <div class="col-md-12">

                        <figure class="pull-left p-2 ">
                        <img data-src="<?= cdn('assets/template/uploads/') ?>2015/06/before-acne.png" alt="Before Acne Scar Treatment" title="Before Acne Scar Treatment" width="130" height="130" class="aligncenter size-large wp-image-12444 lazy"></figure>
                        <h4>Before the Acne Treatment</h4>
                        <p>The first step is the consultation, wherein our dermatologist will examine your skin carefully, taking notes on all the problems. This will allow them to chart out the most optimum course of treatment for you. This is also a great time to clarify all doubts and queries. You should also give all your medical information to the doctors, such as medication you might be on, any allergies that you might have and of course, any previous surgeries that you might have had. </p>
                        <hr />
                        <figure class="pull-left p-2 ">
                        <img data-src="<?= cdn('assets/template/uploads/') ?>2015/06/during-acne.png" alt="Best Acne Treatment in Bangalore" title="Best Acne Treatment in Bangalore" width="130" height="130" class="aligncenter size-large wp-image-12445 lazy"></figure>
                        <h4>During the Treatment</h4>
                        <p>Depending on which procedure you are undergoing, you will be given the appropriate protective gear, such as protective eye shields. Whether you are undergoing a laser acne scar removal session or a microdermabrasion, a numbing gel will be applied to the skin to ensure that you feel no pain. The duration and the intensity of the procedure will be dependent on what your doctor has decided for you. With almost all the procedures, the time taken from start to finish will be anywhere between 30 to 60 minutes. </p>
                        <hr />
                        <figure class="pull-left p-2 ">
                        <img data-src="<?= cdn('assets/template/uploads/') ?>2015/06/after-acne.png" alt="Laser Acne Scar Removal Cost in Bangalore" title="Laser Acne Scar Removal Cost in Bangalore" width="130" height="130" class="aligncenter size-full wp-image-12443 lazy"></figure>
                        <h4>After the treatment </h4>
                        <p>Once the procedure has been completely, our dermatologist will apply some topical lotion or cream to soothe the skin. In the days to come, you will need to keep a tab on what you eat and how you treat your skin. You will be given clear instructions on how to wash your face and take care of your skin. You will be asked to drink a lot of water and make sure you have proper sun protection, every time you step out of the house. In certain cases, you might be prescribed some form of oral or topical medication. </p>
                    </div>
                </div>

                <div class="tab">
                    <button class="tab-toggle">What are the risks or side effects of acne treatment?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What are the risks or side effects of acne treatment?</h3>
                    <p>If you are getting your treatments done at AK Clinics Bangalore, you have little to worry about, because we one of the best in the industry. We use the most modern equipment and methods to help our patients and our constant aim is to ensure that they face no discomfort and pain. However, there are always some risks and side effects associated with medical procedures, and some of the possible side effects of acne treatment include:</p>
                    <ul>
                        <li>Dryness and skin irritation are common, but tend to disappear on its own within a day or two.</li>
                        <li>There could be a burning sensation or a peeling of skin, but these are also common and nothing to worry about. </li>
                        <li>If you are on medications, you might feel slightly lightheaded, but the feeling should pass soon enough. </li>
                        <li>If you have been using birth control pills to control your acne, you could notice a spike in your blood pressure. </li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">FAQs</button>
                </div>
                <div class="content">
                    <h3 class="m-3">FAQs</h3>

                    <div class="col-md-12 pt-1">
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Will Eating Fried Foods Or Chocolate Cause Acne?</a> </div>
                                <div id="collapseOne" class="collapse" data-parent="#accordion">
                                    <div class="card-body">There is no one food group that could cause acne – there are people who believe that if you eat too much chocolate or milk products or oily food, you will get acne, but there is no scientific proof to support the same. However, your overall diet will have an important role to play, because you need to make sure that you have a healthy diet. Studies have shown that people whose diet is loaded with sugar and simply carbohydrates are more prone to acne, as opposed to those who have more fresh fruits and vegetables in their daily diet.</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Can Stress Lead To Acne?</a> </div>
                                <div id="collapseThree" class="collapse" data-parent="#accordion">
                                    <div class="card-body">Directly no, but indirectly yes. When you are under too much stress, your body will produce a lore more androgen, which in turn will lead to the secretion of a lot more sebum than regular and this can lead to acne.</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">Is There A Difference Between A Dark Spot And An Acne Scar?</a> </div>
                                <div id="collapsefive" class="collapse" data-parent="#accordion">
                                    <div class="card-body">Yes, there is a difference – a dark spot is the pigmentation that arises after the inflammation from the acne healing process. These are not scars, which means that over time, they will heal and disappear. However, scars that have been caused by acne will take a long time to heal and at times, might only lighten, but not disappear completely.</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsese">How Soon Will I See Clear Skin?</a> </div>
                                <div id="collapsese" class="collapse" data-parent="#accordion">
                                    <div class="card-body">It is important for you to understand that there is no miracle cure for acne, which will clear it all out overnight. Any treatment will take time to start showing results and you need to stay strong on the course being prescribed to you. However, with our treatments, you should start seeing results in a few months.</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsenine">Will I Face Any Downtime?</a> </div>
                                <div id="collapsenine" class="collapse " data-parent="#accordion">
                                    <div class="card-body">Most of the acne treatment procedures are what can be called lunchtime procedures, which means that you should be able to return to work within a few hours. However, certain post procedure effects such as redness, tenderness to the touch and swelling might be visible. These should subside on their own or with the help of an ice pack, within a few hours.</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseelev">Will My Acne Return After The Treatment Schedule?</a> </div>
                                <div id="collapseelev" class="collapse" data-parent="#accordion">
                                    <div class="card-body">If you have skin that is prone to acne, there is always the chance that it might return. If there are hormonal changes in your body, there is a chance of your acne returning. This is why it is imperative that you complete the course of treatment, as this will minimize the chances of recurrence.</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsethi">How Much Will The Procedure Cost?</a> </div>
                                <div id="collapsethi" class="collapse" data-parent="#accordion">
                                    <div class="card-body">There is no fixed cost for the acne removal procedure, because no two procedures will be the same. For instance, if you want the acne treatment cost in Bangalore AK Clinics, it will be dependent on a host of factors, including how severe your acne is, how long it has been troubling you, which procedure is being considered for you and how many sittings will be needed.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/services_bangalore'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
