<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>bangalore/"> <span itemprop="name">Best Skin Care Clinic in Bangalore</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>bangalore/pigmentation-treatment/"> <span itemprop="name">Pigmentation Treatment</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
                <!-- Breadcrumbs /--> 
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title">
                    <h2 class="black-text text-center">Pigmentation Treatment in Bangalore</h2>
                </div>
                <p>So many of us, go to great lengths to take care of our skin – we wash it with the best of face washes, apply expensive skin care creams, go for regular facials and massages and ensure that we eat healthy, to keep our skin nourished from within. However, there are times, when things go wrong from within the body and that starts to show on the skin, and in such cases, we might need something more than home remedies.<br />
                    Pigmentation is one such problem that affects more people than we could imagine, and in most cases, people ignore the symptoms. While pigmentation is not a dangerous condition, it needs medical attention at the right time. Fortunately, with the best pigmentation treatment in Bangalore, you can now get rid of all symptoms of pigmentation forever.</p>
                <h4>What is Pigmentation?</h4>
                <p>Pigments are naturally occurring elements in the skin and they are what give our skin the natural colour – but when there is an imbalance in these pigments, it leads to conditions known as hyperpigmentation or hypopigmentation. Hyperpigmentation is caused when there is an excessive production of melanin in the body and this leads to dark spots all over the skin. Hyperpigmentation can affect any part of the body, including the face, arms and legs. A proper diagnosis is important, because in many cases, this pigmentation could be the pointer towards some other medical condition and also because hyper pigmentation treatment in Bangalore can be started only after the diagnosis has been made.</p>
                <h4>Causes of dark spots on your skin</h4>

                <figure><img class="img-fluid lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>pigmentation-cause.png" alt="Causes of Pigmentation and its treatment" title="Causes of Pigmentation and its treatment"></figure></div><div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>bangalore/best-hair-transplant-clinic/">Best Hair transplant Clinic</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/hair-loss-treatment/">Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-acne-scar-removal-treatment/">Laser Acne Scar Removal Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-hair-removal-for-men-and-women/">Laser Hair Removal For Men and Women</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-skin-treatment/">Laser Skin Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/prp-hair-loss-treatment/">PRP Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/scalp-micro-pigmentation/">Scalp Micro Pigmentation</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/skin-whitening-lightening-treatment/">Skin Whitening Lightening Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/stretch-marks-removal-treatment/">Stretch Marks Removal Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray  ">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle ">What are the types of hyperpigmentation?</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">What are the types of hyperpigmentation?</h3>
                    <p>While there are several types of hyperpigmentation, the most common types include:</p>
                    <div class="col-md-12">
                        <h4>Sunspots </h4>

                        <p>Also known as solar lentigines, sun spots are normally caused because of excessive exposure to sunlight. These spots will be most visible on those parts of the body, which are exposed to sunlight, such as face, hands and legs. </p>
                        <h4>Melasma </h4>

                        <p>This type of hyperpigmentation is often caused because of hormonal activity in your body – when certain hormones are not functioning properly, it could lead to hyperpigmentation. The most common appearance is during pregnancy and menopause, because there is a change in hormones during these times. </p>
                        <h4>Post inflammatory </h4>

                        <p>This is the type of hyperpigmentation that is caused because of an injury to the skin. </p>
                    </div>
                </div>

                <div class="tab">
                    <button class="tab-toggle">What are the causes for hyperpigmentation?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What are the causes for hyperpigmentation?</h3>
                    <p>To plan a proper course of pigmentation treatment in Bangalore, it is important that the cause is diagnosed first. Here are the most common causes, there are a few basic causes for hyperpigmentation:</p>
                    <ul>
                        <li>Excessive exposure to sunlight</li>
                        <li>Excessive production of melanin in the body</li>
                        <li>Certain medicines such as those used for chemotherapy</li>
                        <li>Change in hormonal levels due to conditions such as pregnancy or menopause</li>
                        <li>Medical conditions such as Addison’s disease, which can affect the adrenal glands</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Why you should consider getting pigmentation treatment?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Why you should consider getting pigmentation treatment?</h3>
                    <p>If you are diagnosed with hyperpigmentation, then it is important that you get treatment for the same as soon as possible. After just a few sessions, the pigments will start to fade, and your skin will start to return to its normal colour and tone. More importantly, if you start your treatment on time, you can ensure that the condition does not spread, because the treatments aims at nipping the very root.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 ">
    <div class="container">
        <div class="col-md-12">
            <div class="row text-center">
                <div class="section-title">
                    <h1 class="black-text">What are the methods of diagnosing hyperpigmentation?</h2><p class="text-center">The main methods of diagnosing the condition, before starting pigmentation treatment clinic in Bangalore are:</p>
                </div>

            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row text-center">
                <div class="col-md-4"> <strong>Wood’s Lamp </strong>
                    <p>This device emits a UV light, which helps determine the type of hyperpigmentation. This, most preferred method of diagnosing hyperpigmentation, is normally done in a dark room and can help determine not only the type of pigmentation, but also the extent of the condition. </p>
                </div>
                <div class="col-md-4"> <h4>Magnifying Lamp </h4>

                    <p>This too is an extremely popular method of diagnosing the condition – when the skin is examined under this lamp, the pigmentation becomes evident and easy to determine. The benefit of this lamp is that it can help determine several other skin conditions too. </p>
                </div>
                <div class="col-md-4"> <h4>Skin biopsy </h4>

                    <p>In the rare situations that the lamps are not able to gauge the type of pigmentation, a skin biopsy might be needed. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container ">
        <div class="col-md-12">
            <div class="row text-center">
                <div class="section-title">
                    <h1 class="black-text">What is the procedure for pigmentation treatment?</h1>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row text-center">
                <div class="col-md-4" itemtype="https://schema.org/MedicalTherapy" itemscope="" itemprop="possibleTreatment"> <strong>Before the procedure </strong>
                    <p>Before the course of treatment can be started, you will be given a thorough skin examination, because the treatment can be decided only after diagnosing the extent and the cause. During this examination, your medical history will also be taken, including your genetic history and details of any medication that you might be taking. The examination will include UV studies, because it is important to understand whether over exposure to sunlight has caused the pigmentation. The medical history will help the doctor understand whether any hormonal imbalances or other medical conditions such as Cushing’s disease or Addison’s disease are causing the condition. It would also be wise to tell the doctor about any surgeries or skin care procedure that you might have undergone in the past. </p>
                </div>
                <div class="col-md-4"> <strong>During the procedure </strong>
                    <p>For your skin pigmentation laser treatment in Bangalore, you will be taken to the treatment room, where a specialised laser will be used on your skin. The intense light of the laser will penetrate through your skin to the pigments in the skin, where it will be absorbed. The energy that is created by the laser will break up the cells of the pigment and the resultant particles will be cleared out by the body’s natural immune system. There could be a little bleeding, crusting or blistering on the skin but this is completely natural and will disappear after a little while. It is important to understand that the pigments will not fade away immediately – some time and a few more sessions will be needed for proper results to become visible.</p>
                </div>
                <div class="col-md-4" itemtype="https://schema.org/MedicalTherapy" itemscope="" itemprop="possibleTreatment"> <strong>After the procedure </strong>
                    <p>Immediately after the procedure, your skin will feel tender and there might be some bleeding. You will be given proper instructions on how to take care of your skin and at the best clinic for pigmentation treatment in Bangalore, all these instructions will be given to you in a printed form. You will be given medications to help with the pain as well as for the healing process and you will be asked to not step out into the sun for a few days. </p>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Am I Safe During The Procedure?</a> </div>
                        <div id="collapseOne" class="collapse show" data-parent="#accordion">
                            <div class="card-body">Absolutely. We use only the most modern lasers, which will ensure the best results with the minimal amount of discomfort. You will be given protective eye wear so that the bright light does not hurt your eyes.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Will The Effects Last A While?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">You need to remember that the results will not become visible in the first session – you will need to get through a minimum of 2-3 sessions to start seeing the results. And once the results become visible, they will last a long time.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Will It Hurt?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">Lasers are not known to hurt, but there could be a little discomfort, which is temporary. Chemical peels will cause a little more discomfort, as there could be some itching and irritation. But these are not known to last for more than a few hours.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">How Much Will The Procedure Cost?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">Pigmentation treatment cost in Bangalore will be dependent on several factors including the type of pigmentation, the extent and the choice of procedure. The number of sessions you require will also affect the costing.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" id="testimonails">
                <div class="col-md-12 text-center">
                    <h2>What are the possible risks?</h2>
                    <p>When the procedure is done by professionals, there is little to be worried about, but as is with any medical procedure, there are a few risks involved:</p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                            <li>There could be a little bleeding, which should subside in a few hours. </li>
                            <li>The skin could start to look a little grey, but this too is normal, and your skin will return to its normal colour soon. </li>
                            <li>The pigments might not fade at all and you might have to look at other methods of treatment. </li>
                            <li>There could be uneven colouring all over the treated area.</li>
                            <li>The blistering and crusting is extreme, and it does not fall off easily.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-light-gray">
<?php $this->load->view('front/partials/services_bangalore'); ?></div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>