<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-left m-2 text-dark" >
        <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
            <meta itemprop="position" content="1" />
          </li>
          <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>bangalore/"> <span itemprop="name">Best Skin Care Clinic in Bangalore</span></a>
            <meta itemprop="position" content="2" />
          </li>
        </ul>
        <!-- Breadcrumbs /--> 
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="section-title-1 text-left">
          <h1 class="black-text">AK Clinics Bangalore</h1>
          <p>There are clinics that will offer you a handful of services, and then there is the best hair and skin clinic in Bangalore, where you can find every solution that your hair & skin might have been looking for. Whether you want your skin to look younger or to feel healthier or you are facing hair loss or baldness, AK Clinics in Bangalore is the best Hair & Skin clinic Bangalore that you will want to come to.</p>
        </div>
      </div>
      <div class="col-md-6">
        <div class="section-title text-center">
          <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2018/06/IMG_20180406_153634.jpg" class="img-fluid rounded lazy" alt="Best Dermatologist in Bangalore for Skin" title="Skin Care Clinic in Bangalore" style="width:80%;"></figure>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title text-center">
          <h2 class="black-text">Skin Treatment Services</h2>
          <p>The skin is the largest organ of the human body, but many a times, we neglect the same, because we are so busy in our daily lives. We subject our skin to harsh sunlight and harsher chemicals in the name of skin care products. Our diets are also such that there is not enough nutrition for the skin to look and feel healthy. Soon, you will notice that your skin is feeling tired and signs of aging will become visible, so much earlier than they are supposed to start showing. You will notice wrinkles, fine lines and a skin that is tired and blemished. 
            At AK Clinics, we will have the best skin care specialist in Bangalore, ready to assist you on your journey to the kind of skin you had always dreamt about. 
            We offer a range of cosmetology procedures including:</p>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="kd-logo ptb-1">
        <div class="regular slider">
          <div>
            <div class="card mb-3 box-shadow"> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/06/anti-ageing.png" class="img-fluid lazy" alt="Best Dermatologist in Bangalore for Skin" title="Anti-Ageing">
              <div class="card-body text-center"> <strong>Anti Ageing</strong>
                <p class="card-text addReadMore showlesscontent" >When age starts to catch up, your skin will start showing the same, and at AK Clinics, we offer you a range of anti ageing treatments. While ageing is inevitable, there are procedures which can help keep the signs of ageing away for a little longer. Some of the most popular treatments offered at our clinic include Botox, dermal fillers and facial volumizers. We also provide Vampire lifts, fractional laser, fractional microneedle High-Intensity Focused Ultrasound or HUFA and monopolar RF. All these treatments and therapies are designed to reduce fine lines, tighten your skin from within and give you a younger look. With age, you will notice that that extra weight starts to show itself everywhere including your chin. With our FatX procedure, we can take away that double chin of yours, taking away a few years too. We invite you to talk to our skin specialist in Bangalore, and choose a procedure that works best for you.</p>
              </div>
            </div>
          </div>
          <div>
            <div class="card mb-3 box-shadow"> <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/uploads/'); ?>2018/06/cosmetology-procedures.png"  alt="Skin Care Clinic in Bangalore" title="Cosmetology Procedures"/>
              <div class="card-body  text-center"> <strong>Cosmetology Procedures</strong>
                <p class="card-text addReadMore showlesscontent" >If it is not age that is the worrying factor for you, there are other cosmetology procedures that are offered at AK Clinics, Bangalore. We can ensure that your skin looks smooth with our skin polishing and laser photo facials. We can also use laser to remove acne, scars and pigmentation related problems. If it is acne that has been bothering you, we can help you with acne as well as acne scar treatments. With our chemical peels, you can say goodbye to skin that constantly looks tired and has marks and blemishes. At AK Clinics Bangalore, we also offer a range of medi-facials, which are designed to work like the normal facial, but have a much more profound effect on your skin. For people with oily skin, we offer the carbon peel, which is known to work really well, while our revolutionary OxyGeneo facial is like a super facial, because it gently exfoliates the skin, naturally oxygenating it and rejuvenating it from deep within. Our dual laser toning procedure can give your skin the even tone that you had always wanted.</p>
              </div>
            </div>
          </div>
          <div>
            <div class="card mb-3 box-shadow"> <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/uploads/'); ?>2018/06/laser-hair-removal.png"  alt="Good Skin Specialist in Bangalore" title="Laser Hair Removal">
              <div class="card-body  text-center"> <a href="<?= base_url(); ?>bangalore/laser-hair-removal-for-men-and-women/" target="_blank"><strong>Laser Hair Removal</strong></a>
                <p class="card-text addReadMore showlesscontent" >With our laser hair removal procedure, you can bid farewell to unwanted hair forever. Over the course of a few sessions, you will notice that you no longer have to invest time in shaving, waxing or using hair removal creams, because no matter which part of the body it is, laser hair removal can clean it!</p>
              </div>
            </div>
          </div>
          <div>
            <div class="card mb-3 box-shadow"> <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/uploads/'); ?>2018/06/Pigmentation-Treatment.png" alt="Best Skin Care Clinic in Bangalore" title="Pigmentation Treatment" />
              <div class="card-body  text-center"><a href="<?= base_url();?>bangalore/pigmentation-treatment/" target="_blank"> <strong>Pigmentation Treatment</strong></a>
                <p class="card-text addReadMore showlesscontent" >Whether its sun spots or freckles, age spots or proper hyperpigmentation, at AK Clinics, we have a solution for all such blemishes and spots that might be marring your skin. With our modern and revolutionary pigmentation treatments, you can be sure that you will have skin that is truly clean and clear. With our laser pigmentation treatments, you should be able to see clearer skin within 2 to 3 sessions.</p>
              </div>
            </div>
          </div>
          <div>
            <div class="card mb-3 box-shadow"> <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/uploads/'); ?>2018/06/acne-scar.png"  alt="Dermatologist in Indiranagar Bangalore" title="Acne & Scar Treatment">
              <div class="card-body  text-center"><a href="<?= base_url(); ?>bangalore/laser-acne-scar-removal-treatment/" target="_blank"> <strong>Acne & Scar Treatment</strong></a>
                <p class="card-text addReadMore showlesscontent" >Acne is something that has troubled almost every human being, at some point or the other, but when acne starts to trouble you into your adult years and refuses to let go, it is time to head to the experts at AK Clinics Bangalore. We offer a range of acne and acne scar treatments, which will help reduce and remove acne as well as the scars that accompany it. Our treatments include chemical peels, microdermabrasion and laser resurfacing.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title text-center">
          <h2 class="black-text">Hair Restoration Services</h2>
          <p>At AK Clinics, not only will you find the best dermatologist in Bangalore for skin, you will also receive the best advice in terms of which procedure you should choose, how to prepare for the same and how to take care once the procedure has been completed. We also understand the concerns that our patients might have, which is why we keep their privacy extremely high on our priority list. </p>
          <p>While you can find the best dermatologist in Bangalore at AK Clinics, you can also find some of the best hair restoration and transplant solutions here too. Hair loss is a far more common problem than most people would think – while losing a few hair every day is normal, if the hair that is falling is not being replaced, you have a hair loss problem. Understanding the reason for your hair loss would be the first step to treating the same. At our clinic, we can assure you that you will be a given a thorough examination and then the right course of treatment. Your hair loss, which could be because of anything ranging from genetics to bad food habits, will be categorised, before any medical advice is given to you. There is quite the chance that you might not have</p>
        </div>
      </div>
    </div>
    <div class="col-md-12 ptb-1">
      <div class="tabs">
        <div class="tab">
          <button class="tab-toggle ">Hair restoration</button>
        </div>
        <div class="content ">
          <h3 class="m-3"> Hair restoration</h3>
          <p>If your hair loss has just started, then you could look at restoration procedures, such as medication. Minoxidil and Finasteride are approved by FDA and have shown promising results when used regularly. Therapies such PRP (platelet rich plasma) allows doctors to take blood from your body, convert it into a potent serum and inject it back into your scalp, allowing better hair growth. SMP (scalp micropigmentation) is like a tattoo for your head – thousands of tiny dots will create the illusion of a full head of hair. In addition, we also offer low level laser therapy, which will improve the blood circulation on the scalp, reduce clogging in the hair follicles and ultimately better hair growth.</p>
        </div>
        <div class="tab">
          <button class="tab-toggle">Hair transplant in Bangalore</button>
        </div>
        <div class="content">
          <h3 class="m-3"><a href="<?= base_url(); ?>bangalore/best-hair-transplant-clinic/" target="_blank">Hair transplant in Bangalore</a></h3>
          <p>In cases, where the hair loss is extensive and the only way out is a transplant, we at AK Clinics are the leaders and pioneers in the field. Whether you need an FUT or strip surgery or are looking at the FUE method, we are the best people for you. We have extremely experienced surgeons as well as trained counsellors, who will be able to assist you throughout. While our counsellors will ensure that they walk you through the entire procedure and help you with all your doubts and queries, the surgeons will make sure that there is minimum graft wastage, the grafts are harvested and implanted with a strict count and the time spent by each graft, outside the body, is minimal. Our operation theatres are state of the art and we maintain the highest levels of hygiene.</p>
        </div>
        <div class="tab">
          <button class="tab-toggle">Hair loss treatment in Bangalore</button>
        </div>
        <div class="content">
          <h3 class="m-3"><a href="<?= base_url(); ?>bangalore/hair-loss-treatment/" target="_blank">Hair loss treatment in Bangalore</a></h3>
          <p>In cases, where the hair loss is extensive and the only way out is a transplant, we at AK Clinics are the leaders and pioneers in the field. Whether you need an FUT or strip surgery or are looking at the FUE method, we are the best people for you. In FUT, an entire strip of hair is removed from the scalp (specifically, the donor area) and then the hair follicles are individually removed and implanted into the recipient area. The area from where the strip has been removed is closed and all that is left over is a linear scar, which will get hidden underneath the new hair growth. In FUE, the hair follicles are individually removed via a micro-punching method and implanted immediately. The only scars that are left are miniscule dots, which are hidden completely under the new hair growth.</p>
        </div>
        <div class="tab">
          <button class="tab-toggle">SMP treatment in Bangalore</button>
        </div>
        <div class="content">
          <h3 class="m-3"><a href="<?= base_url(); ?>bangalore/scalp-micro-pigmentation/" target="_blank">SMP treatment in Bangalore</a></h3>
          <p>For many people, getting a hair transplant is a scary thought, because they are worried about the surgery aspect of it. In cases, where the hair loss has only just started, there are other options which can be explored and SMP or scalp micropigmentation is one of them. Imagine hundreds of little dots being tattooed onto your scalp – that is the basic concept behind SMP. Matching the colour of your natural hair, dots will be tattooed onto your scalp, giving the impression of a full head of hair.</p>
        </div>
        <div class="tab">
          <button class="tab-toggle">PRP Treatment in Bangalore</button>
        </div>
        <div class="content">
          <h3 class="m-3"><a href="<?= base_url(); ?>bangalore/prp-hair-loss-treatment/" target="_blank">PRP Treatment in Bangalore</a></h3>
          <p>Perhaps one of the most ground-breaking and effective methods of re-growing hair naturally is PRP. Platelet rich plasma therapy is a revolutionary method, which utilises blood from your own body to nourish your body. Blood is extracted from your body and the plasma is separated, enriched with growth nutrients and then injected back into the scalp. The potent serum works from within the body to rejuvenate the hair follicles and promote new hair growth.</p>
        </div>
      </div>
      <p class="ptb-3">We have extremely experienced surgeons as well as trained counsellors, who will be able to assist you throughout. While our counsellors will ensure that they walk you through the entire procedure and help you with all your doubts and queries, the surgeons will make sure that there is minimum graft wastage, the grafts are harvested and implanted with a strict count and the time spent by each graft, outside the body, is minimal. Our surgeons will also make sure that only the required number of grafts are extracted from the carefully measured out donor area and the natural hairline is maintained at all time. As a matter of fact, when your new hair starts to grow in, you will not be able to recognise which the implanted hair and which the natural ones are.</p>
    </div>
  </div>
</div>
<div class="service-area ptb-3   bg-light-gray ">
  <div class="container">
    <div class="col-md-12">
      <div class="row">
        <div class="section-title text-center">
          <h2 class="black-text">Cosmetology Treatments</h2>
          <p>In addition to the cosmetology procedures and offering you the services of the best skin specialists in Bangalore, we also bring to you a range of cosmetic surgeries:</p>
        </div>
      </div>
    </div>
    <div class="col-md-12 ptb-1">
      <div class="row">
        <div class="col-md-6">
          <ul>
            <li ><strong>Gynecomastia - </strong>A surgery to remove male breasts or man boobs, as they are commonly referred to as</li>
            <li><strong>Vitiligo surgery –</strong> A procedure to remove white patches that might be apparent on various parts of your body</li>
            <li><strong>Augmentation surgery –</strong> A procedure to increase the size of breasts</li>
            <li><strong>Blepharoplasty –</strong> A procedure that allows you to reshape your eyelids to improve the structure of your face or improve vision</li>
          </ul>
        </div>
        <div class="col-md-6">
          <ul>
            <li><strong>Liposuction –</strong> A procedure that allows us to remove cellulite and fat deposits from almost any part of the body, without invasive surgery</li>
            <li><strong>Breast reduction –</strong> A procedure opted for by women who might have extremely large breasts, as this procedure will reduce the size</li>
            <li><strong>Rhinoplasty –</strong> A procedure that allows you to reshape your nose or improve breathing </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title text-center">
          <h2 class="black-text">What are the machines and equipment being used?</h2>
          <p>One task that our team at AK Clinics has set for itself, is to ensure that we are constantly updated with all the new machines that are arriving on the scene and after understanding how they work, we make sure that they become available for our clients. <strong>Here are just some of the machines that are on offer to our clients in Bangalore:</strong></p>
        </div>
      </div>
    </div>
  </div>
  <div class="container ptb-1">
    <div class="col-md-12 regular slider">
      <div>
        <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/Alma-Q.png" alt="Skin Specialist Doctor in Bangalore" width="200" height="200" class="img-circle lazy" title="	
Alma Q" /></figure>
        <h4><span itemprop="award">Alma Q </span></h4>
        <p>This is one of the best and most modern machines, when it comes to permanent tattoo removal. The light of this machines works in such a manner that it removes the colour pigmentation of the tattoo, without damaging the surrounding skin cells and tissue. </p>
      </div>
      <div>
        <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/Lumenis-Duet.png" alt="Skin Care Specialist in Bangalore" width="200" height="200" class="img-circle lazy" title="Lumenis Duet Lightsheer"/></figure>
        <h4><span itemprop="award">Lumenis Duet Lightsheer </span></h4>
        <p>The Lightsheer Duet by Lumenis is one of the most effective machines when it comes to permanent hair removal. Without using any gels or anaesthetics, the machines can easily remove hair from the entire back or both legs in less than 20 minutes. </p>
      </div>
      <div>
        <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/ultracel.png" alt="Best Dermatologist in Bangalore for Skin" title="Ultracel" width="200" height="200" class="rounded lazy" /></figure>
        <h4><span itemprop="award">Ultracel </span></h4>
        <p>Whether you want to address the fine lines on your face or you want your skin to look smoother and have a natural glow, then Ultracel is the way to go for you. This totally non-invasive procedure can give you the type of effect, you would imagine possible only with a surgical facelift. </p>
      </div>
      <div>
        <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/oxygeneo.png" alt="Skin Care Clinic in Bangalore" title="OxyGeneo" width="200" height="200" class="rounded lazy" /></figure>
        <h4><span itemprop="award">OxyGeneo </span> </h4>
        <p>This is being renamed as a miracle facial, because its provides three benefits simultaneously. While gently exfoliating all the dead skin cells off your skin, the procedure oxygenates and hydrates your skin, nourishing it from within. The deep facial will rejuvenate your skin, giving you results, after the very first procedure. </p>
      </div>
      <div>
        <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/six-step-therapy.png" alt="Good Skin Specialist in Bangalore" title="Six Step Therapy Machine" width="200" height="200" class="rounded lazy" /></figure>
        <h4><span itemprop="award">Six step therapy machine </span> </h4>
        <p>Perhaps one of the single most important machines, because the six step therapy machine can handle six procedures, including Oxygenating your skin, microdermabrasion, radiofrequency treatment, Mask Light Face Treatment, give your skin an air massage and also allow our doctors to do an Electro-poration.</p>
      </div>
      <div>
        <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/hydra.png" alt="Best Skin Care Clinic in Bangalore" title="Hydra Facial" width="200" height="200" class="rounded lazy" /></figure>
        <h4><span itemprop="award">Hydra facial </span> </h4>
        <p>The hydra-facial is becoming one of the most popular facials with women of all ages, because it not only exfoliates the dead skin revealing healthier skin underneath, it also extracts dirt from the pores, hydrating the skin in the process. The final step is the protection of the skin, via a layer of peptides and antioxidants. </p>
      </div>
      <div>
        <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/Microdermabrasion.png" alt="Best Skin Doctor in Bangalore" title="Microdermabrasion" width="200" height="200"  class="rounded lazy" /></figure>
        <h4><span itemprop="award">Microdermabrasion </span> </h4>
        <p>Imagine the dead skin layer being ‘sanded’ away to reveal younger and healthier looking skin! That is pretty much what we can achieve with our microdermabrasion machine – we will gently scrub away the top layer of skin, clearing away all the dirt and dead skin, revealing the glowing, healthy skin that was always there, right underneath! </p>
      </div>
    </div>
  </div>
</div>
<div class="service-area">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12 bg-img cover-bg sm-height-550px xs-height-350px " data-background="<?= cdn('assets/template/frontend/img/'); ?>slide1.png" style="background-image:url(<?= cdn('assets/template/frontend/img/'); ?>slide1.png);"></div>
      <div class="col-md-6 col-sm-12 col-xs-12 bg-dark pd-60 pl-sm10 pr-sm10">
        <div class="cent">
          <div class="section-head">
            <h4 class="text-muted">What Infrastructure is Available</h4>
          </div>
          <p class="pb-20 text-white">At AK Clinics, we have an infrastructure that is at par with any international clinic – from the interiors of the operation theatres to the equipment we utilise! We take pride in the fact that each of our treatment and procedure rooms is equipped to handle all our clients’ needs. All our rooms are extremely clean and all our procedures are done with hygiene being the utmost priority. After each procedure, the rooms are cleaned and sanitised and all the equipment is sterilised for the next procedure. We realise that our clients should be offered nothing but the best, which is why we constantly upgrade all our machines and equipment and at any given point of time, you can be sure that your treatment will be done using state of the art and truly modern equipment. </p>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('front/partials/services_bangalore'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
<style>
    a.slide-over{ opacity:0.8; background:#ccc;}
    a.slide-over:hover{ opacity:1;}
    .addReadMore.showlesscontent .SecSec,.addReadMore.showlesscontent .readLess {display: none;}
    .addReadMore.showmorecontent .readMore {display: none;}
    .addReadMore .readMore,.addReadMore .readLess {margin-left: 2px;cursor: pointer;}
    .addReadMoreWrapTxt.showmorecontent .SecSec,.addReadMoreWrapTxt.showmorecontent .readLess {display: block;}
</style>
<script>
  

    $(document).on('ready', function () {
        $(".regular").slick({
            dots: true,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
</script> 
