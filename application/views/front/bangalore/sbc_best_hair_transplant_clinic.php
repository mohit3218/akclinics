<?php $this->load->view('front/partials/main_hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>bangalore/"> <span itemprop="name">Best Skin Care Clinic in Bangalore</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>bangalore/best-hair-transplant-clinic/"> <span itemprop="name">Best Hair Transplant Clinic</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1">
                    <h1 class="black-text text-left">Hair Restoration and Transplantation in Bangalore</h1>
                </div>
                <p>Hair loss is a problem that does not look at geographical boundaries; it can happen to anyone. As a matter of fact, hair loss is something that happens with everyone, it’s just that the magnitude of the loss varies for each person. And depending on the magnitude of the hair loss, the solution needs to be identified. For years, the options for people suffering from hair loss were severely limited, and even in cities like Bangalore, there was not much too choose from.</p>
                <p class="pt-1">If you are someone who is facing extensive hair loss and are trying to locate the best option for hair transplant in Bangalore, then AK Clinics would be your best choice. Over the years, we have not only perfected a range of hair restoration methods, our team of doctors are highly experienced and will offer you the best advice, allowing you to make the most informed decisions. Hair Transplant in Bangalore is not going to cut your pockets if you are at AK Clinics. We provide you treatment at affordable cost with EMI option.</p>
                <h4>We offer our clients a range of services including:</h4>
                <ul class="ptb-1">
                    <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT hair transplant</a> – FUT hair transplant is used in situations, where larger areas need to be covered.. We will ensure that the scars from the strip surgery are carefully hidden underneath the new growth areas.</li>
                    <li><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/"> FUE hair transplant</a> – FUE is a more modern procedure, in which the hair is extracted in tiny clusters. Each cluster will not have more than 2-5 follicles and the grafts are then implanted into the recipient area.</li>
                    <li><a href="<?= base_url(); ?>hair-transplant/bio-fue/">Bio FUE</a> &amp; <a href="<?= base_url(); ?>hair-transplant/bio-dht/" target="_blank">Bio DHT</a> – Both are AKClinics’s trademark technique. On both methods the results have been truly incredible.</li>
                    <li ><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial hair transplant</a> – We can help restore hair on other parts of the body, such as beards, moustaches, eyebrows and eyelashes</li>
                </ul>
                <h4>There are certain factors that will cost you for Hair Transplant and these include:</h4>
                <ul class="ptb-1">
                    <li>The actual area of the scalp that will be undergoing the transplant</li>
                    <li>How many hair follicles will have to be extracted and implanted</li>
                    <li >How many sessions you will have to undergo</li>
                    <li>If there are any skin or hair related conditions or if your scalp has any pre-existing conditions</li>
                    <li >The actual part of the body where the transplant will take place</li>
                    <li>In case, money is a cause of concern for you, we also offer EMI facilities and that too with 0% interest!</li>
                </ul>
                <p>We also offer the hair transplant payment in EMI with 0% Interest <a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Get the Cost table with per graft</a></p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>bangalore/hair-loss-treatment/">Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-acne-scar-removal-treatment/">Laser Acne Scar Removal Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-hair-removal-for-men-and-women/">Laser Hair Removal For Men and Women</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-permanent-tattoo-removal/">Laser Permanent Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-skin-treatment/">Laser Skin Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/prp-hair-loss-treatment/">PRP Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/scalp-micro-pigmentation/">Scalp Micro Pigmentation</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/skin-whitening-lightening-treatment/">Skin Whitening Lightening Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/stretch-marks-removal-treatment/">Stretch Marks Removal Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12 text-center">
            <div class="row">
                <div class=" section-title ">
                    <h2 class="black-text">Why AK Clinics</h2>  <p> You are still wondering why you should come to AK Clinics, here are a few reasons:</p>
                </div>

            </div>
        </div>
        <div class="col-md-12 text-center ptb-1">
            <div class="row">
                <div class="col-md-4 aligncenter"> <h4>Educated team</h4>
                    <p> We understand that people coming to us, will have innumerable questions and queries, which is why our team is ever ready to answer all of them. Our team will provide you all the necessary assistance in making an informed decision. Our doctors will also examine you in detail, in order to assess the actual extent of your hair loss, and post this, they will be able to offer you the most appropriate course of treatment.</p>
                </div>
                <div class="col-md-4 aligncenter"> <h4>Completely natural look</h4>
                    <p>When you are getting a hair transplant, you do not want people to know about it and we at AK Clinics, understand the same, which is why we offer you a completely natural look. This is possible for us because the hair that will be used, would have been harvested from your own body. What’s more, once the hair has grown out, you will be able to cut, colour or even straighten them!</p>
                </div>
                <div class="col-md-4 aligncenter"> <h4>Pain-free procedure</h4>
                    <p>We are aware that most people are not too comfortable with the concept of pain, which is why we have developed advanced methods of anaesthesia and sedation. This not only ensures minimal pain to the patient, but also a much faster recovery. We actually encourage our patients to read, chat with our team or perhaps even catch a movie, all while the procedure is going on.</p>
                </div>
                <div class="clr magr-top-30"></div>
                <div class="col-md-4 aligncenter"> <h4>High graft yield</h4>
                    <p>We are proud to be one of the few clinics in the nation, which can claim to have Follicular Transection Rate that is well below 5% in FUE. This has been possible, only because we take incredible measures to ensure the health of the follicles. In addition, we also take care to harvest only from the permanent zones and our constant attempt is to keep the grafts out of the body, for a minimal amount of time.</p>
                </div>
                <div class="col-md-4 aligncenter"> <h4>Perfect hygiene</h4>
                    <p>For us, hygiene is of utmost significance, and we ensure that our team adheres to the strictest rules of the same. While our staff follows strict codes of hygiene, we ensure that the entire clinic is disinfected and sanitised regularly. In addition, all the equipment is sterilised before and after each procedure.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12 text-center">
            <div class="row">
                <div class=" section-title ">
                    <h2 class="section black-text">Hair Transplant Method</h2>
                </div>
                <p> The initially hair transplant were based around removing an entire patch of the scalp and then removing the grafts from piece of scalp. The improved version of that procedure is follicular unit transplant or FUT. But more modern method of hair transplant is known as FUE or follicular unit extraction.</p>
            </div>
        </div>
        <div class="col-md-12 text-center">
            <div class="row">
                <div class="col-lg-6"> <h4 class="white">FUE Hair Transplant</h4>
                    <p> This is a more modern and advanced method of hair transplant and this is a procedure that the doctors at AK Clinics hold an expertise in. Hair will be extracted in tiny grafts, via micro punches and each graft will not have more than 2-5 follicles. These grafts will be implanted into the recipient area, ensuring that is minimal discomfort to the patient. With FUE, the scarring is also much less than regular FUT procedure, which is probably why it is fast becoming the most preferred method of hair transplant.</p>
                </div>
                <div class="col-lg-6"> <h4 class="white">FUT Hair Transplant</h4>
                    <p>This is one of the original methods of hair transplant and has been used by hair transplant surgeons, all over the world. In this method, a small portion of the scalp is removed from an area that has been identified beforehand, and it will be from this section, that the grafts will be harvested. Once the grafts have been taken out, they will be implanted into the recipient site and the donor site will be carefully sutured back. if the procedure has been done well, there will be nothing more than a linear scar, which will get hidden under the new growth of hair.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12 text-center"><div class="row">
                <div class=" section-title ">
                    <h2 class="black-text">FUE and FUT Comparison</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center"><div class="row table-responsive">
            <table class="table table-hover" border="0" cellspacing="0" cellpadding="0">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col">FUE</th>
                        <th scope="col">FUT</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><strong>1.</strong></td>
                        <td><strong>Method</strong></td>
                        <td>Extraction of individual follicular units through small punches. No Sutures</td>
                        <td>Scalpel excision of donor strip with suturing</td>
                    </tr>
                    <tr>
                        <td><strong>2.</strong></td>
                        <td><strong>Scarring</strong></td>
                        <td>Small scars are formed which are inconspicuous and get merged very well most of the times but not always especially if overdone</td>
                        <td>Linear Scar that may be visible if hair is trimmed /shaved</td>
                    </tr>
                    <tr>
                        <td><strong>3.</strong></td>
                        <td><strong>Donor Source</strong></td>
                        <td>Scalp and Body</td>
                        <td>Back of Scalp</td>
                    </tr>
                    <tr>
                        <td><strong>4.</strong></td>
                        <td><strong>Quantity of Hair</strong></td>
                        <td>Large session difficult (2000 – 2500 graft)</td>
                        <td>Larger session (Upto 3000 graft possible)</td>
                    </tr>
                    <tr>
                        <td><strong>5.</strong></td>
                        <td><strong>Natural Result</strong></td>
                        <td>Yes, Permanent</td>
                        <td>Yes, Permanent</td>
                    </tr>
                    <tr>
                        <td><strong>6.</strong></td>
                        <td><strong>Recovery</strong></td>
                        <td>Back to routine activity within few days</td>
                        <td>Suturing used healing over 10 days</td>
                    </tr>
                    <tr>
                        <td><strong>7.</strong></td>
                        <td><strong>Cost</strong></td>
                        <td>Moderate</td>
                        <td>Slightely Less</td>
                    </tr>
                    <tr>
                        <td><strong>8.</strong></td>
                        <td><strong>Time Take</strong></td>
                        <td>Moderate</td>
                        <td>Slightely Less</td>
                    </tr>
                    <tr>
                        <td><strong>9.</strong></td>
                        <td><strong>Pain</strong></td>
                        <td>Not Painless. But anesthesia is given in such a way that it feels painless to the patient</td>
                        <td>Not Painless. But anesthesia is given in such a way that it feels painless to the patient</td>
                    </tr>
                    <tr>
                        <td><strong>10.</strong></td>
                        <td><strong>Preferred</strong></td>
                        <td>Hair restoration of short hair/small number of graft or for patients that have undergone several STRIP procedures and their scalp has become too tight</td>
                        <td>Grade of baldness is higher and the donor area is not very good, strip technique is usually preferred</td>
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 ">
    <div class="container">
        <div class="col-md-12 text-center">
            <div class="row">
                <div class=" section-title ">
                    <h2 class="black-text">Type of Hair Transplant</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center ">
            <div class="row">  
                <div class="col-lg-3">
                    <h4>FUE Hair Transplant</h4>
                    <p>FUE is a more modern procedure, in which the hair is extracted in tiny clusters. Each cluster will not have more than 2-5 follicles and the grafts are then implanted into the recipient area. The process is used to ascertain the most natural results</p>
                    <div class="col-md-12 content-center center"><a class="" href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/" target="_blank">Read More</a></div>
                </div>
                <div class="col-lg-3">
                    <h4>FUT Hair Transplant</h4>
                    <p>FUT is one of the original methods of hair transplant and is used in situations, where larger areas need to be covered. A small section of the scalp is removed from the donor area, and grafts are harvested from the same and transplanted into the recipient site</p>
                    <div class="col-md-12 content-center center"><a class="" href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/" target="_blank">Read More</a></div>
                </div>
                <div class="col-lg-3">
                    <h4>Bio-FUE<sup style="position: relative; font-size: 10px; top: -18px;">TM</sup></h4>
                    <p>This new method has been created by our in-house team and in this procedure platelet cells from the body are used to create a special serum and this serum is then injected back into the scalp. The aim of the serum is to enhance hair growth naturally</p>
                    <div class="col-md-12 content-center center"><a class="" href="<?= base_url(); ?>hair-transplant/bio-fue/" target="_blank">Read More</a></div>
                </div>
                <div class="col-lg-3">
                    <h4>Bio-DHT<sup style="position: relative; font-size: 10px; top: -18px;">TM</sup></h4>
                    <p>This procedure also has been developed by our own team and in this process, hair that has been extracted, spends a minimal amount of time outside the body. The extracted hair is then transplanted, immediately because this helps ensure the health of the hair.</p>
                    <div class="col-md-12 content-center center"><a class="" href="<?= base_url(); ?>hair-transplant/bio-fue/" target="_blank">Read More</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/services_bangalore'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>