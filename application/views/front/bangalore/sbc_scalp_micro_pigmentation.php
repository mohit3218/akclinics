<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>bangalore/"> <span itemprop="name">Best Skin Care Clinic in Bangalore</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>bangalore/scalp-micro-pigmentation/"> <span itemprop="name">Scalp Micro Pigmentation</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Scalp Micropigmentation – An Alternative To A Hair Transplant</h1>
                </div>
                <p>Losing hair is never an easy prospect to accept, and while most of us have some amount of hair loss on a daily basis, new hair not growing in its stead is what leads to the condition known as hair loss. If the hair loss is extreme, then a hair transplant might be the only way out, but with several new methods breaking ground each day in the medical world, there are actually other options in hand these days. If you walk into AK Clinics, the best scalp micropigmentation clinic in Bangalore, you will come to know that these days, there are several other methods of covering up your hair loss, and a transplant might be required only in the most extreme scenario. SMP in Bangalore is now kicking up as awareness is readily to the masses.<br />
                    In the past few years, scalp micropigmentation has become extremely popular, because this is a non-surgical procedure that allows you to create an illusion of having a head full of hair. Imagine getting several dots tattooed on your scalp – that is the basic tenet on which scalp micropigmentation (SMP) works on. Essentially there is no surgery – all you have to do is sit through a tattoo session and you can walk out with the look of a lot more hair, almost immediately.</p>
            </div><div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>bangalore/best-hair-transplant-clinic/">Best Hair transplant Clinic</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/hair-loss-treatment/">Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-acne-scar-removal-treatment/">Laser Acne Scar Removal Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-hair-removal-for-men-and-women/">Laser Hair Removal For Men and Women</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/laser-skin-treatment/">Laser Skin Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/prp-hair-loss-treatment/">PRP Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/scalp-micro-pigmentation/">Scalp Micro Pigmentation</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/skin-whitening-lightening-treatment/">Skin Whitening Lightening Treatment</a></li>
                        <li><a href="<?= base_url(); ?>bangalore/stretch-marks-removal-treatment/">Stretch Marks Removal Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">What exactly is SMP?</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">What exactly is SMP?</h3>
                    <p>SMP is a technique by which a series of tiny dots are created all over the scalp, 
                        concentrating on the areas where the hair loss is most visible. A non-toxic ink is used and 
                        the colour will be matched to your exact hair colour, to make the final look natural. 
                        The tiny dots will mimic tiny hair, as if you had a really short cropped hair cut. 
                        This procedure is also a great way to cover scars, which might have been caused by infection 
                        or any trauma or FUT hair transplant. The best part about SMP is that it is a cost effective 
                        method to cover your entire head, offering it a really well groomed look. In addition, 
                        with the services from the best clinic for SMP in Bangalore, you can get treatment, 
                        irrespective of your age, hair or skin colour and type of hair loss.
                    </p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">How does SMP work?</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">How does SMP work?</h3>
                    <p>The average number of hair per centimetre square on the scalp is 200, but when you start to lose your hair, your scalp will start becoming visible, a lot faster than you can imagine. Hair loss is a lot more than actually losing hair – it is about losing a sense of confidence in yourself and by a process such as SMP, not only can you gain an illusion of having a full head of hair, you can also gain back a lot of your confidence. One of the biggest advantages of scalp micropigmentation in Bangalore is that you could have diffused thinning or you could have complete bald spots – this procedure can handle both.</p>
                    <h4>Here is how SMP works:</h4>
                    <ul class="ptb-1 text-left">
                        <li>It can give the illusion of a head full of hair, as if you had a cropped haircut</li>
                        <li>It can create the look of a full frontal, side or rear hairline</li>
                        <li>It can restore hairlines on partially or fully bald heads</li>
                        <li>It can help camouflage all levels of alopecia</li>
                        <li>It can help camouflage scars that could have risen because of infection, surgery or trauma or FUT hair transplant</li>
                        <li>It can help camouflage scars, birthmarks and burns</li>
                        <li>It can help boost the effects of a hair transplant, by creating a fuller look</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Who can consider getting SMP?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Who can consider getting SMP?</h3>
                    <ul class="ptb-1 text-left">
                        <li>When the grade of baldness is extremely high and there is not enough of a donor area</li>
                        <li>When there have been multiple surgical sessions, but the patient wants more covering, without another surgery</li>
                        <li>When the patient is not an ideal candidate for a hair transplant</li>
                        <li>When patients want a way to cover scars from a previous surgery</li>
                        <li>When the alopecia has been caused by a systemic disease</li>
                        <li>When the patient is bald because of an immunological problem, such as liver or kidney related problems</li>
                        <li>When the patient wants to restore facial hair, but is not too keen on surgical methods</li>
                        <li>When there have been multiple transplant sessions, but the result is still nowhere near what was desired</li>
                        <li>When there is diffused thinning</li>
                        <li>When the patient does not want the surgical option</li>
                        <li>When the alopecia is extensive such as alopecia universalis or alopecia totalis</li>
                        <li>When the alopecia is caused by cancer treatment</li>
                        <li>When the patient has genetic alopecia</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle ">What is the procedure for revision or reversal?</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">What is the procedure for revision or reversal?</h3>
                    <p>With time, the ink of the tattoo will start to fade, which is when you will have to return for touch up. In addition, if you have baldness, chances are that new bald spots will keep appearing and you will need to cover those too. However, there are some people who might want a reversal, because with age, they might want a new hairline. The revision could include a changed hairline, changed side hair shape, addition of side burns and so on. Because the pigment being used is on a superficial level, the removal is also much easier, as opposed to a regular tattoo.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle ">How much will SMP cost?</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">How much will SMP cost?</h3>
                    <p>The cost of the scalp micropigmentation procedure will be dependent on a few factors:</p>
                    <ul class="ptb-1 text-left">
                        <li>When there have been multiple transplant sessions, but the result is still nowhere near what was desired</li>
                        <li>When there is diffused thinning</li>
                        <li>When the patient does not want the surgical option</li>
                        <li>When the alopecia is extensive such as alopecia universalis or alopecia totalis</li>
                        <li>When the alopecia is caused by cancer treatment</li>
                        <li>When the patient has genetic alopecia</li>
                    </ul>
                    <p>While no clinic will be able to give you a precise amount right at the beginning, most of the good clinics will give you a fair idea of how much it will cost. The final billing might vary by about 5-10%.</p>
                </div>

                <div class="tab">
                    <button class="tab-toggle ">Can SMP be done in tandem with a hair transplant?</button>
                </div>
                <div class="content active">
                    <h3 class="m-3">Can SMP be done in tandem with a hair transplant?</h3>
                    <p>There are several cases where a hair transplant is not able to provide the kind of density that the patient was looking for. This could be because of an insufficient donor area or it could be because the recipient area is too large. In such situations, combining a hair transplant with scalp micropigmentation is a great idea, because with this procedure, the desired density can be achieved with ease. And because the hair will already be short cropped post the transplant, the scalp micropigmentation will only look all the more natural, along with it.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle ">Frequently Ask Questions</button>
                </div>
                <div class="content active">
                    <h3 class="m-3">Frequently Ask Questions</h3>
                    <div id="accordion" class="mt-4">
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Is SMP Permanent?</a> </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                <div class="card-body">No. as is the case with any tattoo like procedure, the colour will start to fade over time and if you want to maintain the procedure, you will need to go back for revision procedures.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Can SMP Be Reversed?</a> </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">Yes. You can reverse the pigmented spots on your scalp with the help of laser treatment.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Is SMP Painful?</a> </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body"> The needles used for the procedure are extremely thin, which is why the pain is minimal. But at AK Clinics, we will offer you the options of local anaesthesia or sedation, should you require the same. Normally, a numbing gel will be applied to the scalp, post which the pain and discomfort should be minimal.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Is SMP Expensive?</a> </div>
                            <div id="collapsefour" class="collapse " data-parent="#accordion">
                                <div class="card-body">The scalp micropigmentation cost in Bangalore is actually much lesser than a hair transplant, and because the results are visible almost instantaneously, a lot of people prefer this procedure.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/services_bangalore'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>