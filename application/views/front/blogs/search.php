<style>
.col-md-12.header-search {padding: 0px !important;margin-top: 10px;}
.header-search .search-form input.search-input[type=text] {font-size: 18px;height: 46px;line-height: 1.33333;padding: 10px 35px;background-color: #fff;border: 1px solid #ccc;border-radius: 22px;box-shadow: 0 1px 1px rgba(0,0,0,.075) inset;color: #555;display: block;transition: border-color .15s 0 ease-in-out, box-shadow .15s 0 ease-in-out;margin: 0px auto;width: 95%;opacity: 0.5;}
.searchsubmit.search-submit {display: none;}
</style>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>search"> <span itemprop="name">Search</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
                <!-- Breadcrumbs /--> 
            </div>
        </div>
    </div>
</div>
<div class="container-fluid pt-1">
<div class="col-md-12 header-search">
	<form method="get" class="searchform search-form" action="<?= base_url(); ?>search/" >
		<input type="text" value="Google Search ..." name="q" class="s search-input" onfocus="if (this.value == 'Google Search ...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Google Search ...';}" />
		<input type="submit" class="searchsubmit search-submit" value="Search" />
	</form>    
</div>
	</div>
<script>
  (function() {
    var cx = '000938023301683131145:uke9tgdaaf8';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:searchresults-only></gcse:searchresults-only>
