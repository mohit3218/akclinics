<link type="text/css" rel="stylesheet" href="<?= cdn('assets/template/frontend/social-share/') ?>jssocials.css" />
<link type="text/css" rel="stylesheet" href="<?= cdn('assets/template/frontend/social-share/') ?>jssocials-theme-classic.css" />
<script type="text/javascript" src="<?= cdn('assets/template/frontend/social-share/') ?>jssocials.min.js"></script>
<style>.h3, h3{width: 100%; padding: 5px 0;}.col-md-8, .col-md-4, .col-md-5, .col-md-7, .col-md-9, .col-md-12 li {line-height: 1.7;font-weight: 400;font-size: 18px;line-height: 1.7rem;}h4 {font-weight: 500!important;margin: 15px 0;width: 100%;}.list-inline, .list-unstyled {padding-left: 0;list-style: none;}.margin-bottom-20 {margin-bottom: 20px !important;}.list-unstyled.margin-bottom-20 li {display: block;}li.icon-chk::before, li.icon-front-page::before, li.icon-front-page:before, ol li.icon-chk-1::before, ol li.icon-front-page1::before {content: "\f00c";font-family: fontawesome;line-height: 24px;margin-right: 10px;position:relative;}.panel-group {padding-top: 21px;margin-bottom: 20px;}.panel-group .panel {border-radius: 4px;margin-bottom: 0;overflow: visible;}.panel-group .panel+.panel {margin-top: 5px;}div::after, div::before {content: " ";display: table;}*, :after, :before {box-sizing: border-box;}*, :after, :before, input[type=checkbox], input[type=radio] {-webkit-box-sizing: border-box;-moz-box-sizing: border-box;}.panel-default>.panel-heading {background: rgba(0,0,0,0);border: none;padding: 0;color: #333;}.content h4 {font-size: 18px;}.panel-title {border: 1px solid #ccc;border-radius: 3px;color: inherit;margin-bottom: 5px;margin-top: 0;padding: 6px;text-transform: capitalize;font-size: 16px !important;}.panel-default.accordion-toggle {top: 10px;}.blog-left-side-box {border: 2px solid #dedede;}.left-sidebar-links h2{background: #ff9000 none repeat scroll 0 0 !IMPORTANT;color: #fff;font-size: 14px;margin-top: 0;text-align: center; line-height:normal;padding: 10px;}.blog-left-side-box h2 {background: #ff9000 none repeat scroll 0 0;color: #fff;font-size: 16px;margin-top: 0;text-align: center;padding: 10px;}.blog-left-side-box p.blg-btn {margin: 0;}.blog-left-side-box h2 {background: #ff9000 none repeat scroll 0 0 !IMPORTANT;color: #fff;font-size: 14px;margin-top: 0;text-align: center; line-height:normal;padding: 10px;}.blog-left-side-box P{padding: 10PX;line-height:normal;Text-align: center;}
    .left-sidebar-links ul li img{ width:70% !important; text-align:center !important;}div{line-height:1.7 !important;font-weight:400 !important;font-size:16px !important}img.aligncenter{max-width:100%;height: auto;}/*strong{clear:both;width:100% !important;display:block !important;}*/.size-full{width:85%;margin:0px auto;clear: both;display: block;margin-bottom: 10px;text-align:center;}.social-buttons.share {list-style-type: none;margin: 0;padding: 0;}</style>
<?php
$curret_url_slug = $this->uri->segment(2);
?> 
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark">
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"><span itemprop="name">Home</span></a> <i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>blog"> <span itemprop="name">Blog</span></a> <i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> 
                        <a itemprop="item" href="<?= base_url(); ?>blog/<?= $slug_name ?>"> <span itemprop="name"><?= ucwords(str_replace("-", " ", $curret_url_slug)) ?></span></a>
                        <meta itemprop="position" content="3" />
                    </li> </ul>
            </div> 
        </div>
    </div>
</div>
<div class="service-area">
    <div class="container-fluid">
        <div class="row ptb-3"> 
            <div class="col-md-1">
                <div class="ak-social-icons" style="position: absolute;">
                    <div id="shareRoundIcons"></div>
                </div>
            </div>
            <div class="col-md-7 text-left text-dark" itemscope="itemscope" itemtype="http://schema.org/blog"> 
                <h1 class="pt-1"><?= $title; ?></h1>
                    <div itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost" itemid="<?= base_url(uri_string()) . '/' ?>">
                    <header class="entry-header">
                        <p class="entry-meta">
                            <time class="entry-time" itemprop="datePublished" datetime="<?= date("Y-m-d H:i:s", strtotime($post_date)); ?>"><?= date("F d,Y", strtotime($post_date)); ?></time> By 
                            <span class="entry-author" itemprop="author" itemscope="itemscope" itemtype="http://schema.org/Person">
                                <!--<a href="http://akclinics.co.in/blog/author/akclinics/" class="entry-author-link" itemprop="url" rel="author">-->
                                    <span class="entry-author-name" itemprop="name"><?= $post_author; ?></span>
                                <!--</a>-->
                            </span> 
                            <span class="entry-comments-link">
                            <a id="discuss-comment-count-a" href="" data-disqus-identifier=""></a>
                        </span> 
                        </p>
                    </header>
                
                    <div>
                        <meta itemprop="headline" content="<?= $title; ?>">
                        <div itemscope="" itemprop="image" itemtype="http://schema.org/imageObject">
                            <meta itemprop="height" content="300">
                            <meta itemprop="width" content="300">
                             <?php if (!empty($record['image'])) { ?>
                            <meta itemprop="url" content="<?php echo cdn($blog_data['image']); ?>">
                             <?php } else { ?>
                            <meta itemprop="url" content="<?php echo cdn('assets/template/frontend/'); ?>img/ak-blog.png">
                             <?php } ?>
                        </div>
                        <div itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">
                            <div itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
                              <meta itemprop="url" content="https://cdn.akclinics.org/wp-content/themes/AKclinics/images/logo-footer.png">
                              <meta itemprop="width" content="99">
                              <meta itemprop="height" content="44">
                            </div>
                            <meta itemprop="name" content="AK Clinics">
                        </div>
                        <meta itemprop="dateModified" content="<?= date("F d,Y", strtotime($post_date)); ?>">
                        <?= html_entity_decode($blog_data['content']); ?>
                    </div>
                </div>
                <br /><br />
                <div class="related-posts"><h3>Related Posts</h3>
                    <ul class="related-posts">   
                        <?php  foreach($similar_post as $record) : if($record['id'] != $blog_data['id']) : ;?>
                       <li><a href="<?= $record['post_url']; ?>" title="<?= $record['title']; ?>"><?= $record['title']; ?></a></li>   
                       <?php endif; endforeach; ?>
                    </ul>
                </div>
				<br /><br /><br />
				<div id="disqus_thread"></div>
            </div>
            <div class="col-md-3 text-center text-dark" > 
                <div class="left-sidebar-links">
                    <h2>Membership &amp; Recommendations</h2>
                    <ul class="text-center" style="list-style:none;" >
                        <li><img src="<?= cdn('assets/template/uploads/'); ?>2015/06/ishrs-gold.png" class="img-fluid "></li>
                        <li><img src="<?= cdn('assets/template/uploads/'); ?>2015/06/ishrs-fellow.png" class="img-fluid"></li>
                        <li><img src="<?= cdn('assets/template/uploads/'); ?>2015/06/ABHRS_logo.png" class="img-responsive"></li>
                        <li><img src="<?= cdn('assets/template/uploads/'); ?>2015/06/ahrs.png" class="img-fluid"></li>
                        <li><img src="<?= cdn('assets/template/uploads/'); ?>2015/06/realself.png" class="img-fluid"></li>
                        <li><img src="<?= cdn('assets/template/uploads/'); ?>2015/06/htn.png" class="img-fluid"></li>
                    </ul>
                </div>
                <div class="blog-left-side-box">
                    <h2>Hair Transplant Testimonials</h2>
                    <iframe src="https://www.youtube.com/embed/zgI1wFC23hg?rel=0&autoplay=true" controls="0" frameborder="0" allowfullscreen="" style="width:100%;" id="_dytid_8861"></iframe>
                    <p class="blg-btn">
                        <a href="<?= base_url(); ?>results/videos/" target="_blank">Read More</a>
                    </p>
                </div>
                <div class="blog-left-side-box">
                    <h2>Hair Transplant Photos</h2><figure>
                        <img src="<?= cdn('assets/template/frontend/images/'); ?>before-after.png" class="img-fluid"></figure>
                    <p>Before &amp; after photos of more than 50 of our patients.</p>
                    <p class="blg-btn"><a href="<?= base_url(); ?>results/" target="_blank">Read More</a></p>
                </div>
            </div> 
        </div> 
    </div>
</div>
<script>
function genericSocialShare(url){
    window.open(url,'sharer','toolbar=0,status=0,width=648,height=395');
    return true;
}    
var disqus_config = function () {
    this.page.url = 'https://akclinics.org/blog/<?= $slug_name; ?>#disqus_thread';
    console.log( this.page.url);
    $('#discuss-comment-count-a').attr('href', this.page.url);
    this.page.identifier = '<?= "https://akclinics.org/" . uri_string(); ?>/430';
    $('#discuss-comment-count-a').attr('data-disqus-identifier', this.page.identifier);
    this.page.title = '<?= $slug_name; ?>';
};

(function() {
var d = document, s = d.createElement('script');
s.src = 'https://akclinic.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>lease enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<script>
var slug = '<?= $slug_name ?>';
$("#shareRoundIcons").jsSocials({
    url: "<?= base_url(); ?>blog/<?= $slug_name ?>/",
    text: '<?= $title; ?>',
    showCount: true,
    showCount: "inside",
    showLabel: false,
    shareIn: "popup",
    shares: [
        "facebook",
        "linkedin",
        { share: "pinterest", label: "Pin this" },
        "twitter",
        "googleplus",
        "whatsapp"
    ]
});
</script> 