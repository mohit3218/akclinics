<style>
.blog-h2{font-size:20px;color:#333;line-height:32px;font-weight:400;padding:10px 5px;}
.side-menu .list-group-item {  margin-bottom: -2px;}
.col-md-4.header-search {padding: 0px !important;}
.header-search .search-form input.search-input[type=text] {font-size: 18px;height: 46px;line-height: 1.33333;padding: 10px 35px;background-color: #fff;border: 1px solid #ccc;border-radius: 4px;box-shadow: 0 1px 1px rgba(0,0,0,.075) inset;color: #555;display: block;transition: border-color .15s 0 ease-in-out, box-shadow .15s 0 ease-in-out;margin-bottom: 10px;}
.searchsubmit.search-submit {display: none;}
@media (min-width:320px) and (max-width:640px){.card {min-height: auto!important;}}
</style>
<?php
$curret_url_slug = $this->uri->segment(2);
$cat_slug_data = $this->home_model->get_blog_cat_slug_by_name($curret_url_slug);
$cat_id = '';
$active = '';
if(!empty($cat_slug_data))
{
    $cat_id = $cat_slug_data['id'];
    if($curret_url_slug == $cat_slug_data['slug'])
    {
       $active = 'active';
    }
}
$useragent = $_SERVER['HTTP_USER_AGENT'];
?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>blog"> <span itemprop="name">Blog</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid ptb-3">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="card-columns pt-3 blogs">
                    <?php foreach ($data['all_blog'] as $k => $record) : ?>
                       <?php $class = 'all';
                           if($record['categories_id'] == CI_PATIRNTS_ASK){$class ='hdpe';}
                           if($record['categories_id'] == CI_SUCCESS_STORIES){$class ='sprinkle';}
                           if($record['categories_id'] == CI_CASE_STUDY){$class ='spray';                }
                           if($record['categories_id'] == CI_HAIR_TRANSPLANT_ARTICLES){$class ='irrigation';}
                           if($record['categories_id'] == CI_NEWS_AND_EVENTS){$class ='news-and-events';}
                           if($record['categories_id'] == CI_HAIR_LOSS_ARTICLES){$class ='hair-loss-articles';}
                           if($record['categories_id'] == CI_HAIR_LOSS_TREATMENT_ARTICLES){$class ='hair-loss-treatment-articles';}
                           if($record['categories_id'] == CI_MEDICAL_TREATMENT_ARTICLES){$class ='medical-treatment-articles';}
                           if($record['categories_id'] == CI_COSMETIC_SURGERY_ARTICLES){$class ='cosmetic-surgery-articles';}
                           if($record['categories_id'] == CI_STYLESHALA){$class ='styleshala';}
                       ?>
                        <?php if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) { ?>
                        <a class="blog-btn" href="<?= $record['post_url']; ?>amp" target="_blank">
                        <?php } else { ?>
                        <a class="blog-btn" href="<?= $record['post_url']; ?>" target="_blank">
                        <?php } ?>
                            <div class="card <?php echo $class; ?>"> 
                                <div class="card-content">
                                    <div class="card-body">
                                    <?php if (!empty($record['image'])) { ?>
                                        <img src="<?php echo cdn($record['image']); ?>" class="card-img" alt="<?= $record['image_alt']; ?>">
                                            <?php } else { ?>
                                                <img src="<?php echo cdn('assets/template/frontend/'); ?>img/ak-blog.png" class="card-img" alt="AK Clinics">
                                            <?php } ?>
                                            <h2 class="card-title blog-h2"><?= $record['title']; ?></h2>
                                      </div>
                                </div>
                            </div>
                        </a>
                       <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php if (!preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) { ?>
            <div class="col-md-3">
                <div class="col-md-4 header-search">
                    <form method="get" class="searchform search-form" action="<?= base_url(); ?>search/" >
                        <input type="text" value="Google Search ..." name="q" class="s search-input" onfocus="if (this.value == 'Google Search ...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Google Search ...';}" />
                        <input type="submit" class="searchsubmit search-submit" value="Search" />
                    </form>    
                </div>
                <div class="cta-content marbot40 pt-3">  
                    <ul class="side-menu list-group media blog-item-nav" role="tablist">
                        <li class="list-group-item"> 
                            <a class=" filter-button cate_id" id="Patients-Ask" data-id="patients-ask" data-toggle="pill" href="<?= base_url(); ?>blog/patients-ask" role="tab" aria-controls="Cars" data-filter="hdpe" ari-selected="false">Patients Ask?</a> 
                        </li>
                        <li class="list-group-item">
                            <a class=" filter-button cate_id" id="Success-Stories" data-id="success-stories" data-toggle="pill" href="<?= base_url(); ?>blog/success-stories" role="tab" aria-controls="Forest" data-filter="sprinkle" aria-selected="false">Success Stories</a> 
                        </li>
                       <li class="list-group-item">
                            <a class=" filter-button cate_id" id="Case-Study" data-id="case-study" data-toggle="pill" href="<?= base_url(); ?>blog/case-study" role="tab" aria-controls="Forest" data-filter="spray" aria-selected="false">Case Study</a>
                        </li>
                        <li class="list-group-item">
                            <a class=" filter-button cate_id" id="Hair-Transplant-Articles" data-id="hair-transplant-articles" data-toggle="pill" href="<?= base_url(); ?>blog/hair-transplant-articles" role="tab" aria-controls="Forest" data-filter="irrigation" aria-selected="false">Hair Transplant Articles</a>
                        </li>
                         <li class="list-group-item"> 
                            <a class=" filter-button cate_id" id="News-And-Events" data-id="news-and-events" data-toggle="pill" href="<?= base_url(); ?>blog/news-and-events" role="tab" aria-controls="Forest" data-filter="news-and-events" aria-selected="false">News And Events</a>
                        </li>
                          <li class="list-group-item">
                            <a class=" filter-button cate_id" id="Hair-Loss-Articles" data-id="hair-loss-articles" data-toggle="pill" href="<?= base_url(); ?>blog/hair-loss-articles" role="tab" aria-controls="Forest" data-filter="hair-loss-articles" aria-selected="false">Hair Loss Articles</a>
                        </li>
                         <li class="list-group-item"> 
                            <a class=" filter-button cate_id" id="Hair-Loss-Treatment-Articles" data-id="hair-loss-treatments"  data-toggle="pill" href="<?= base_url(); ?>blog/hair-loss-treatments" role="tab" aria-controls="Forest" data-filter="hair-loss-treatment-articles" aria-selected="false">Hair Loss Treatment Articles</a>
                        </li>
                     <li class="list-group-item">
                            <a class=" filter-button cate_id " id="Medical-Treatment-Articles" data-id="medical-treatment-articles" data-toggle="pill" href="<?= base_url(); ?>blog/medical-treatment-articles" role="tab" aria-controls="Forest" data-filter="medical-treatment-articles" aria-selected="false">Medical Treatment Articles</a>
                        </li>
                        <li class="list-group-item">
                            <a class=" filter-button cate_id" id="Cosmetic-Surgery-Articles" data-id="cosmetic-surgery-articles" data-toggle="pill" href="<?= base_url(); ?>blog/Cosmetic-Surgery-Articles" role="tab" aria-controls="Forest" data-filter="cosmetic-surgery-articles" aria-selected="false">Cosmetic Surgery Articles</a>
                        </li>
                      <li class="list-group-item">
                            <a class=" filter-button cate_id" id="Styleshala" data-id="styleshala" data-toggle="pill" href="<?= base_url(); ?>blog/styleshala" role="tab" aria-controls="Forest" data-filter="styleshala" aria-selected="false">Styleshala</a>
                        </li>
                    </ul>                       
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="load-more" pageNo="<?php echo $pageNo ?>" style="display: none; position: relative;left: 50%; padding-bottom: 10px">
            <img src="<?php echo cdn('assets/template/frontend/img/loader.gif'); ?>"/> 
        </div>
    </div>
</div>
<?php 
$heignt_scroll = 950;
if (!preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) 
{ 
    $heignt_scroll = 400;
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        $(window).unbind('scroll');
        var pageNo = $('.load-more').attr('pageNo');
        
        var cat_id = '<?php echo $cat_id; ?>';
        $('.cate_id').on('click', function(){
            cat_id = $(this).data('id');
            window.location.replace( '<?= base_url(); ?>' + 'blog/' +cat_id);
            $('.load-more-data').hide();
            pageNo = 0;
            loadMoreData(pageNo,cat_id);
        });
        
        $(window).scroll(function ()
        {
            if ($(window).scrollTop() + $(window).height() + 20 >= $(document).height() - '<?= $heignt_scroll; ?>')
            {
                pageNo = parseInt(pageNo) + 1;
                loadMoreData(pageNo,cat_id);
            }
        });
        function loadMoreData(pageNo,cat_id) {
            
            var lastID = $('.load-more-data').attr('lastID');
            if(lastID == 0)
            {
                $('.load-more-data').show();
                return false;
            }
            
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>' + 'home/loadMoreData',
                data: {id:pageNo,cat_id:cat_id},
                beforeSend: function () {
                    $('.load-more').show();
                },
                success: function (html1) {
                    setTimeout(function () {
                        $('.load-more').hide();
                    }, 1000);
                    $('.blogs').append(html1);
                }
            });
        }
    });
    
    $(document).ready(function () {
        $(".filter-button").click(function () {
            var value = $(this).attr('data-filter');
            if (value == "all")
            {
                $('.filter').show('1000');
            } else
            {
                $(".filter").not('.' + value).hide('3000');
                $('.filter').filter('.' + value).show('3000');

            }
        });

        if ($(".filter-button").removeClass("active"))
        {
            $(this).removeClass("active");
        }
        $(this).addClass("active");
    });
</script>
