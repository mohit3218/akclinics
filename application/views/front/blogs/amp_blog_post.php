<?php
$action = $this->uri->segment(1);
$curret_url_slug = $this->uri->segment(2);
?>
<div class="amp-wp-article-content">
    <h1><?= ucwords(str_replace("-", " ", $title)) ?></h1>
    <?= html_entity_decode($blog_data['content']); ?>
</div>