<?php
if (!empty($data['all_blog']) && (isset($cat_id) || $pageNo != 0)) { foreach ($data['all_blog'] as $k => $record) {
$class = 'all';
if($record['categories_id'] == CI_PATIRNTS_ASK){$class ='hdpe';}
if($record['categories_id'] == CI_SUCCESS_STORIES){$class ='sprinkle';}
if($record['categories_id'] == CI_CASE_STUDY){$class ='spray';                }
if($record['categories_id'] == CI_HAIR_TRANSPLANT_ARTICLES){$class ='irrigation';}
if($record['categories_id'] == CI_NEWS_AND_EVENTS){$class ='news-and-events';}
if($record['categories_id'] == CI_HAIR_LOSS_ARTICLES){$class ='hair-loss-articles';}
if($record['categories_id'] == CI_HAIR_LOSS_TREATMENT_ARTICLES){$class ='hair-loss-treatment-articles';}
if($record['categories_id'] == CI_MEDICAL_TREATMENT_ARTICLES){$class ='medical-treatment-articles';}
if($record['categories_id'] == CI_COSMETIC_SURGERY_ARTICLES){$class ='cosmetic-surgery-articles';}
if($record['categories_id'] == CI_STYLESHALA){$class ='styleshala';}
?>
<a class="blog-btn" href="<?= $record['post_url']; ?>" target="_blank">
    <div class="card <?php echo $class; ?>"> 
        <div class="card-content">
            <div class="card-body">
                <?php if (!empty($record['image'])) { ?>
                    <img data-src="<?php echo cdn($record['image']); ?>" class="card-img lazy" alt="<?= $record['image_alt']; ?>">
                <?php } else { ?>
                    <img data-src="<?php echo cdn('assets/template/frontend/'); ?>img/ak-blog.png" class="card-img lazy" alt="AK Clinics">
                <?php } ?>
                <h2 class="card-title blog-h2"><?= $record['title']; ?></h2>
            </div>
        </div>
    </div>
</a>
<?php } } else {
if ($total_records == 0) {
    ?>
    <div class="clearfix"></div>
    <div class="load-more-data" lastID="<?= $total_records ?>">
        No More Blogs..
    </div>
<?php } } ?>
<script type="text/javascript">
    (function () {
        function logElementEvent(eventName, element) {
            console.log(Date.now(), eventName, element.getAttribute("data-src"));
        }
        ll = new LazyLoad({
            elements_selector: '.lazy',
            load_delay: 300,
            threshold: 0,
            callback_enter: function (element) {
                logElementEvent("ENTERED", element);
            },
            callback_load: function (element) {
                logElementEvent("LOADED", element);
            },
            callback_set: function (element) {
                logElementEvent("SET", element);
            },
            callback_error: function (element) {
                logElementEvent("ERROR", element);
                element.src = "https://placeholdit.imgix.net/~text?txtsize=21&txt=Fallback%20image&w=220&h=280";
            }
        });
    }());
</script>