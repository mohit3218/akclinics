<style>
    .background-one {background: #f0ebd8;opacity: 0.9}
    .custom-padding {padding-top: 60px !important;padding-right: 40px !important;padding-bottom: 0px !important;padding-left: 0px !important;}
    a.slide-over {opacity: 0.8;background: #ccc;}  a.slide-over:hover {opacity: 1;}    .card-mh{min-height: 340px;}
    .slider .card{    min-height: 310px;}

    @media (min-width:320px) and (max-width:640px)
    {.carousel-caption {z-index:10;width: 100%;height: 200px;}}
</style>
<?php $this->load->view('front/partials/hair_loss_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-loss/"> <span itemprop="name">Hair Loss</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-tex">Hair Fall Treatment</h1>
                </div>
                <h4>What is Hair Loss ?</h4>
                <p>Losing hair is common but losing hair in excess is a problem. Hair is considered as the crowning glory and everyone dream of healthy, thick, soft, shiny and lustrous hair. Hair loss is thought to affect around 70% of men and 40% of women. It obviously hits the confidence of a person and in many cases, affects work and social life as well. Each day, our scalp sheds approximately 100 hair per day normally. But anything more than is number may be an alarm for hair loss beginning.</p>
                <p>The cause and types of hair loss can vary, so evaluating and treating hair loss is an important part of primary care. The most common cause of the Hair loss or baldness is a genetic trait. Due to advances in medical sciences, we have come to learn that the hair loss genes are actually passed down from both sides of the family and affect both men and women. There are many patients who contact us for hair loss solution but before that let us understand how hair loss progresses.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>hair-loss/hair-loss-men/">Hair Loss Men</a></li>
                        <li><a href="<?= base_url(); ?>hair-loss/hair-loss-women/">Hair Loss Women</a></li>
                        <li><a href="<?= base_url(); ?>hair-loss-treatment-men-women/">Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>prp-treatment/">PRP Therapy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container ">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h2 class="black-tex">What are the stages of Hair Loss?</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row text-center">
                <div class="col-md-4 ">
                    <figure><img class="rounded border lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/11/Anagen-Phase.png" alt="Anagen Phase"></figure>
                    <h4 class="pt-1">Anagen Phase</h4>
                    <p>This phase is considered as the longest phase of the growth cycle or active growth cycle. The life span of this cycle is usually 2-8 years, during this time period, the hair follicle is active and actively produces new dermal papillae cells, resulting in the hair growth at a rate of an approximately one-half inch per month. However, this time period of active growth cycle may vary from patient to patient.</p>
                </div>
                <div class="col-md-4 ">
                    <figure><img class="rounded border lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/11/Catagen-Phase.png" alt="Catagen Phase"></figure>
                    <h4 class="pt-1">Catagen Phase</h4>
                    <p>The catagen phase is considered as a short transitional phase that only lasts for about 2-3 weeks. The first sign of the catagen phase is the cessation of melanin production in the hair bulb. During this phase, hair follicle detaches from the bulb slowly which leads to hair shaft shedding and the hair follicle enters into a dormant period. Eventually, the hair follicle is 1/6th of its original length at the end of this phase.</p>
                </div>
                <div class="col-md-4 ">
                    <figure><img class="rounded border lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/11/Telogen-Phase.png" alt="Teloge Phase"></figure>
                    <h4 class="pt-1">Telogen Phase</h4>
                    <p>The life span of this phase is 3 months when the hair follicle “rests” before converting into the anagen phase again. Due to unhealthy conditions or stress more and more hair start entering Telogen Phase permanently. This span is known as ‘Telogen Effluvium’. It is necessary to nourish your scalp with the healthy diet, hair vitamins, amino acids etc during this phase.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs" style="min-height: 97px;">
                <div class="tab">
                    <button class="tab-toggle">Main causes of hair loss?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What are the main causes of hair loss?</h3>
                    <p>As mentioned before, losing a few hair a day is normal, but when you start noticing hair everywhere, including your towel, pillow and shoulders, you know that it is snowballing into something much more serious or moving you towards baldness treatment. Balding can be a hereditary condition or it could be an indication of certain types of illnesses. Here are some of the most common causes of hair loss:</p>
                    <ul>
                        <li>Hypothyroidism</li>
                        <li>Chemical injuries such as acid burns or physical trauma such as an accident or surgery</li>
                        <li>Certain genetic disorders</li>
                        <li>Infectious diseases such as herpes, leprosy and tuberculosis</li>
                        <li>Skin diseases such as Lichen Planus and DLE</li>
                    </ul>
                    <div class="col-md-6">
                        <strong><a href="<?= base_url(); ?>hair-loss/hair-loss-women/">Hair loss in women</a></strong>
                        <figure class="center"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/11/hair-loss-women.png" class="img-thumbnail lazy" alt="women hair loss pattern"></figure>
                        <p>Hair loss in women is generally of three types and is summed up as- Androgenetic alopecia, Telogen effluvium &amp; Non-pattern hair loss. Androgenetic alopecia is the most common reason for hair loss and pattern is difficult to predict as compared to men. Telogen effluvium can be due to medications, stress etc. There are other reasons too for hair loss in women like hormonal imbalance which fall into the non pattern hair loss category.</p>
                    </div>
                    <div class="col-md-6">
                        <strong><a href="<?= base_url(); ?>hair-loss/hair-loss-men/">Hair loss in men</a></strong>
                        <figure class="center"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/11/hair-loss-men.png" alt="men hair loss pattern" class="img-thumbnail lazy" /></figure>
                        <p>In men, receding hair line is the most common pattern of hair loss. Men, who start losing hair at an early stage, tend to develop the more extensive baldness. In male pattern baldness, hair loss mostly occurs in the crown area or in the frontal region. The main signs of the male pattern baldness are; vertex thinning (losing hair from the top of the head), thinning crown, receding hair line around the temples. (Hairline looks like M shape).</p>
                    </div>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Types of Hair Loss in Men & Women</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What are Types of Hair Loss in Men & Women</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Androgenetic Alopecia</h4>
                            <p>It is commonly known as Male pattern baldness. Signs of Androgenetic Alopecia include hair loss which;</p>
                            <ul>
                                <li>Starts from sides &amp; progress towards back of scalp</li>
                                <li>Horseshoe shaped bald area</li>
                                <li>May also affect other body areas too</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h4>Alopecia Areata</h4>
                            <p>It is characterized by sudden bald patches. Signs of Alopecia Areata include patchy hair loss which</p>
                            <ul>
                                <li>Circular in shape</li>
                                <li>Can appear anywhere on the head</li>
                                <li>May also affect other body areas too</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h4>Alopecia Universalis</h4>
                            <p>It is the rarest and extreme alopecia. Signs of Alopecia Universalis include hair loss which;</p>
                            <ul>
                                <li>Total hair loss on scalp &amp; body</li>
                                <li>Damages &amp; distorts the nails</li>
                                <li>May also causes genetic mutations</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h4>Telogen Effluvium</h4>
                            <p>It is usually caused by stress or shock and takes 3 months. Signs of Telogen Effluvium include hair loss which;</p>
                            <ul>
                                <li>Increased shedding of hair</li>
                                <li>Hair thinning</li>
                                <li>Bald patches</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h4>Traction Alopecia</h4>
                            <p>It occurs due to excessive strain on hair shaft. Signs of Traction Alopecia include;</p>
                            <ul>
                                <li>Hair loss around hairline &amp; temples</li>
                                <li>Patchy hair loss</li>
                                <li>Fluffy or frizzy hair around receding hairline</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h4>Trichotillomania</h4>
                            <p>It is the self inflicted hair loss due to pulling. Signs of Trichotillomania include hair loss which;</p>
                            <ul>
                                <li>Tug on the hair</li>
                                <li>Twisted hair</li>
                                <li>Pulled Hair </li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h4>Alopecia Cicatrisata</h4>
                            <p>It is also known as pseudopelade. Signs of Alopecia Cicatrisata include patchy hair loss which;</p>
                            <ul>
                                <li>Well defined patches</li>
                                <li>Bald patches among surviving hair</li>
                                <li>Long lasting condition</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h4>Follicular Degeneration Syndrome</h4>
                            <p>It is a form of scarring alopecia. Signs of FDG include;</p>
                            <ul>
                                <li>Diffuse hair loss patches</li>
                                <li>Hair loss from scalp vertex</li>
                                <li>Area of hair loss expands gradually with the time</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h4>Alopecia Totalis</h4>
                            <p>It is an autoimmune condition. Signs of Alopecia Totalis include;</p>
                            <ul>
                                <li>Total hair loss on the scalp</li>
                                <li>Other facial hair like eyebrows, eyelashes also fall out</li>
                                <li>Nails become brittle, thin and</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Hair loss assessment at AK Clinics</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Hair loss assessment at AK Clinics</h3>
                    <p>Hair diagnostic tools in our trichology consultation comprise of a thorough medical history, physical examination and hair analysis test. Always talk freely with the consultant and describe properly about your problems as it will help the physician to prepare a best treatment plan for you.</p>
                    <div class="col-md-6">
                        <h4>Medical History</h4>
                        <p>To determine the cause of your hair loss, physician may ask about the:</p>
                        <ul>
                            <li>Your family history of hair loss</li>
                            <li>Hair characteristics including hair thinning, noticeable hair loss pattern</li>
                            <li>Your hair styling habits</li>
                            <li>If you have encounter any severe illness</li>
                            <li>If you are on any medications</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h4>Photographic Evaluation</h4>
                        <p>Physician may also ask to bring pictures of your head for the reference so that he can make assessment of your previous hair loss pattern and also for the future reference. He may ask you to bring pictures of:</p>
                        <ul>
                            <li>Frontal region</li>
                            <li>Hair line</li>
                            <li>Back of the scalp</li>
                            <li>Vertex or crown area </li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <h4>Hair Analysis Test</h4>
                        <p>At our clinic, we use the advanced software and devices for the hair analysis test which will provide the correct assessment of your hair condition and hair loss. The software’s we use for the analysis are:</p>
                        <ul class="text-left list-unstyled">
                            <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>Caslite: </strong>Caslite is the computerized hair scalp analysis to identify the cause of hair loss. It is the world’s most advanced scalp &amp; hair diagnosis software which is trusted and tested by the dermatologists around the worldwide. It helps to keep the record of patients with images, do Norwood classification and also calculate the hair density, growth and thickness along with many other features. </li>
                            <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>Hair Check: </strong>Hair check is other advanced software used by our physicians. Hair check examines the hair and scalp with high magnification and illumination. With the magnified high resolution views of scalp and hair shafts, balding areas are easily identified. It also shows the dramatic and detailed vision of the hair and scalp on the screen to capture pictures and to record video. </li>
                            <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>DermLite: </strong>DermLite is another hand held dermoscope device attached with the digital camera. It is valuable in evaluating the structural abnormalities of the hair. It helps to capture the images in the polarized mode. DermLite is used to calculate the hair density, hair diameter, hair growth rate and anagen/telogen ratio.</li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <h4>Scalp Biopsy if required in case of any disease</h4>
                        <p>The scalp biopsy is performed when your hair loss cause is not identified with the above mentioned methods. It gives the exact number of hair follicles in the specific area and for the assessment of their hair growth cycle. Scalp biopsy is considered as the important tool in diagnosing the female pattern hair loss.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container ">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h2 class="black-text">Treatment for Hair Loss at AK Clinics</h2>
                    <p>The most common hair loss problem in men & women is Androgenetic Alopecia which is mostly observed in patients. But there are also other reasons behind the hair loss which varies from person to person and therefore the treatment should be different too. After the detailed diagnosis of your hair loss, our physicians then led through a best and tailored treatment plan accordingly. Your treatment plan may include;</p>
                </div>
            </div>
        </div>
        <div class="regular slider pt-1">
            <div>
                <div class="card mb-3 box-shadow">
                    <div class="card-body text-center"> <strong>Cyclic Therapy</strong>
                        <p class="card-text card-mh">In cyclic therapy, our physician can prescribes you the topical, oral or other supplements required for the hair growth. These are given in the combinations to avoid the nutrient-nutrient interaction and to enhance the absorption. In therapy, physician may prescribe minoxidil, finasteride, biotin, multivitamins, zinc or other hair growth supplements.</p>
                    </div>
                </div>
            </div>
            <div>
                <div class="card mb-3 box-shadow">
                    <div class="card-body  text-center"> <strong>Hair Gain Therapy</strong>
                        <p class="card-text card-mh">At our clinic, we offer hair gain or restoration therapies for the patients who do not have excessive hair fall. These therapies promote the hair growth and encourage slow hair follicles to grow at a better rate. These therapies include ‘Low Level Laser Therapy’ & ‘Bio therapy’. Both the therapies accelerate the hair growth rate.</p>
                    </div>
                </div>
            </div>
            <div>
                <div class="card mb-3 box-shadow">
                    <div class="card-body  text-center"> <strong>Hair Gain Max</strong>
                        <p class="card-text card-mh">In hair gain max therapy, a combination of ‘Mesotherapy, Low Level Laser Therapy & Biotherapy’ is given. The combination of these therapies acts a booster for hair growth in which infusion of vitamins, amino acids along with the growth factors, plasma and low level of laser light is given which accelerates the hair growth with improved texture.</p>
                    </div>
                </div>
            </div>
            <div>
                <div class="card mb-3 box-shadow">
                    <div class="card-body  text-center"> <strong>Stand Alone Therapies</strong>
                        <p class="card-text card-mh">Biotherapy, Low Level Laser Therapy or Mesotherapy are stand alone therapies. In Biotherapy, growth factors are infused into the scalp while in Low-Level Laser therapy, low powered cold laser is passed to the hair roots which accelerate the hair growth. In Mesotherapy, micro injections are used to deliver the medication to the demarcated parts of the scalp resulting in the nourishment & hair growth.</p>
                    </div>
                </div>
            </div>
            <div>
                <div class="card mb-3 box-shadow">
                    <div class="card-body  text-center"> <strong>Surgical Methods</strong>
                        <p class="card-text card-mh">There are surgical methods if your hair loss has settled and you have bald patches. The two of the most commonly used surgical methods are FUT or follicular unit transplant or FUE or follicular unit extraction. We have our own trademarked procedure i.e. Bio FUE which is an advanced version of FUE which results in faster healing and better natural results.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="embed-responsive embed-responsive-21by9 ht">
                    <iframe src="https://www.youtube.com/embed/e5VevJbRSa0?rel=1" class="embed-responsive-item"></iframe>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 bg-dark pd-60 pl-sm10 pr-sm10">
                <div class="cent">
                    <div class="section-head">
                        <h4 class="text-white">Why choose AK Clinics?</h4>
                    </div>
                    <p class="text-white">AK Clinics is India’s leading & pioneer Hair restoration and aesthetic dermatology center. We have the experience of more than 2500 procedures and provide the highest quality of results at affordable cost at our exclusive centers across India, with the highly trained team of doctors. We strive to provide the best results. Apart from this we have;</p>
                    <ul class="text-left list-unstyled text-white">
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Experienced Surgeons</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Experienced Counselors</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Trained &amp; friendly technicians &amp; staff</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Ultra hygienic OTs</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Natural Results</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Best Guidance about the treatment</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <a href="<?= base_url(); ?>hair-transplant/post-operative-care/" class="text-white">Proper post op care instructions</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="section-title text-center">
                    <h2 class="black-text text-center">Frequently Ask Questions</h2>
                    <p>We strive to provide highest quality at affordable cost with hygiene and imported equipments</p>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id="accordion" class="mt-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Why Does Hair Fall?</a> </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                <div class="card-body">Hair loss is an extremely common occurrence and can affect anyone in their lives. The common causes of hair loss can be stress, hormonal imbalance, menopause, certain medications etc. One of the major causes of hair fall is DHT (dihydrotestosterone) hormone. The hormone testosterone gets converted into dihydrotestosterone which causes miniaturization of the hair follicles, resulting into hair fall.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">How Does Hair Loss Differ In Men And Women?</a> </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body">Both men and women can experience hair loss and is triggered by hormones & genetics. As compared to women, hair loss often occurs faster in men. In men, hair loss can occur near the temples and appear as “M” shaped hair line. While in women, diffuse thinning at the top of the head which shows the part line. Diffuse thinning can occur in early 20s & 30s, due to hormonal changes, aging, thyroid or anemia.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">What Is Telogen Effluvium?</a> </div>
                            <div id="collapsefive" class="collapse" data-parent="#accordion">
                                <div class="card-body">Telogen effluvium is a condition that can be easily confused with genetic female hair loss if it is misdiagnosed. Telogen effluvium, is a condition where hair is pushed into its resting phase and then several months later is shed. This can be caused by several medications and many different medical conditions. Fortunately, telogen effluvium is usually reversible.</div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo"> What Is Alopecia Areata?</a> </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">Alopecia areata causes patches of hair to fall out in a number of dissimilar places and in most instances causes’ baldness. Alopecia areata makes the body attack its anagen follicles which stops the hair growth. Alopecia areata is one of the most common causes of hair loss.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">How Can Diet Cause Hair Loss?</a> </div>
                            <div id="collapsefour" class="collapse " data-parent="#accordion">
                                <div class="card-body">When our body is deprived of essential nutrients, it may lead to hair fall. Diets lacking in iron and protein will affect hair growth because these are the nutrients required by the follicles to grow hair. Excessive consumption of alcohol and smoking a lot can also be a trigger for hair loss.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsesix">What Is The Best Treatment For Hair Fall?</a> </div>
                            <div id="collapsesix" class="collapse" data-parent="#accordion">
                                <div class="card-body">There are various treatments for hair fall. Consult with the hair restoration experts about the exact cause of your loss. He or she will suggest you whether you should try topical medications for hair fall or you should try hair restoration therapies.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
<script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type": "MedicalCondition",
    "alternateName": "androgenic alopecia",
    "associatedAnatomy": {
    "@type": "AnatomicalStructure",
    "name": "hair"
    },
    "cause": [
    {
    "@type": "MedicalCause",
    "name": "Hypothyroidism"
    },
    {
    "@type": "MedicalCause",
    "name": "abudance of male hormone DHT in hair follice"
    },
    {
    "@type": "MedicalCause",
    "name": "Chemical injuries"
    },
    {
    "@type": "MedicalCause",
    "name": "Genetic Disorders"
    },
    {
    "@type": "MedicalCause",
    "name": "Infectious Disease"
    }
    ],

    "differentialDiagnosis": {
    "@type": "DDxElement",
    "diagnosis": {
    "@type": "MedicalCondition",
    "name": "hair loss"
    },
    "distinguishingSign": [
    {
    "@type": "MedicalSymptom",
    "name": "baldness"
    },
    {
    "@type": "MedicalSymptom",
    "name": "thinning hair"
    }
    ]
    },
    "name": "baldness",
    "possibleTreatment": [
    {
    "@type": "MedicalTherapy",
    "name": "Hair Transplant"
    },
    {
    "@type": "MedicalTherapy",
    "name": "PRP"
    }

    ],
    "riskFactor": [
    {
    "@type": "MedicalRiskFactor",
    "name": "Age"
    },
    {
    "@type": "MedicalRiskFactor",
    "name": "hormones"
    },
    {
    "@type": "MedicalRiskFactor",
    "name": "stress"
    }
    ],

    "signOrSymptom": [
    {
    "@type": "MedicalSymptom",
    "name": "thinning hair"
    }
    ]
    }
</script>
<script type="text/javascript">
    $(document).on('ready', function () {
        $(".regular").slick({
            dots: true,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
</script>


