<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-loss/"> <span itemprop="name">Hair Loss</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-loss/hair-loss-men/"> <span itemprop="name">Hair Loss Men</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Hair Loss In Men</h1
                    >
                </div>
                <h4>What is Hair loss in men?</h4>
                <p>There are many people who are aware of the fact that losing up to a 100 hair a day is normal; however, there would also be many who would not be aware of when this normal hair loss turns into a more serious condition. As a matter of fact, many men suddenly notice that there is a huge bald patch on the top of their head.</p>
                
                <p>Hair loss is actually a much more common problem than most people would think – but diagnosing the type of hair loss is imperative and this is something that only an experienced professional can do. There are causes to be analysed and types to be looked into, before a course of action can be decided.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-loss/">Hair Loss</a></li>
                        <li><a href="<?= base_url(); ?>hair-loss/hair-loss-women/">Hair Loss Women</a></li>
                        <li><a href="<?= base_url(); ?>hair-loss-treatment-men-women/">Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>prp-treatment/">PRP Therapy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 bg-img cover-bg sm-height-550px xs-height-350px " data-background="<?php echo cdn('assets/template/uploads/'); ?>2015/06/hair-loss-steps.png" style="background-image:url(<?php echo cdn('assets/template/uploads/'); ?>2015/06//hair-loss-steps.png);" title="Hair Loss Steps "></div>
            <div class="col-md-6 col-sm-12 col-xs-12 bg-light-gray pd-60 pl-sm10 pr-sm10">
                <div class="cent">
                    <div class="section-head">
                        <h4 class="white-text">Some of the most common causes of hair loss in men</h4>
                    </div>
                    <ul>
                        <li>Improper lifestyle, which includes unhealthy eating, stress and improper hair washing techniques</li>
                        <li>Imbalance of thyroid </li>
                        <li>Physical trauma such as an accident or surgery or chemical injuries such as acid burns </li>
                        <li>Medical treatments such as chemotherapy or radiation</li>
                        <li>Genetic disorders </li>
                        <li>Infectious diseases such as herpes, leprosy and tuberculosis</li>
                        <li>Skin diseases such as Lichen Planers and DLE</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class=" col-md-6">
                <div class="col-md-12 text-center">
                    <h2>Hair Loss in Men</h2>
                </div>
                <figure class="text-center"><img class="img-fluid lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2014/09/infographic-hair-loss-.jpg" alt="Types of Hair Loss in Men" style="width:80%" /></figure>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>Types of Hair Loss in Men</h2>
                </div>
                <div class="row">
                    <ul>
                        <li><em><strong>Androgenetic Alopecia</strong></em> –Commonly known as male pattern baldness, androgenetic alopecia is quite widespread. The balding will start in the form of thinning around the temples and the same will slowly progress towards the back of the head. Eventually, there will be a bald patch on the top of the head. The condition is often genetic and the hair follicles are often sensitive to dihydrotestosterone or DHT. The falling out of hair will be gradual and the chances of new hair growing will be minimal. </li>
                        <li><em><strong><a href="<?= base_url(); ?>blog/alopecia-areata/">Alopecia Areata</a></strong></em> – This is an autoimmune condition, which can cause small patches where hair is missing or it could even lead to the complete loss of body hair. There are three further subdivisions in this condition – alopecia monolocularis, alopecia totalis and alopecia universalis, each of which presents differently. </li>
                        <li><em><strong>Cicatricial or Scarring Alopecia</strong></em> – This is actually a serious condition, and also quite rare. The hair follicles are destroyed from within and since the condition is not easy to detect, it is often diagnosed a little too late, leaving little scope for hair regrowth. </li>
                        <li><em><strong>Traction Alopecia</strong></em> – Pulled back hair might seem like a good hairstyle, but over a period of time, such hair styles will make the hair follicles weak. This in turn will make the hair more susceptible to breakage and if the habit continues, the hair loss might become permanent. </li>
                        <li><em><strong>Trichotillomania</strong></em> – A medical condition, wherein which the person has an uncontrollable urge to pull out one’s own hair. When the person is under stress or is nervous or anxious, he will find solace in pulling out hair, leading to bald patches.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">How Does Hair Loss Progresses in Men</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row">
                
                <div class="col-md-6">
                    <h3 class="center-txt">The process of diagnosis</h3>
                    <p>It is crucial that a thorough physical examination be done and the history of the patient is looked into. Certain lab tests might be recommended, in order to gain a complete and comprehensive patient assessment. These tests could include: </p>
                    <ul>
                        <li>Iron, total iron-binding capacity and transferrin saturation, which will allow the measurement of the exact amount of iron deficiency and detect the presence of telogen effluvium</li>
                        <li>Thyrotropin levels</li>
                        <li>A biopsy and histology might be required to detect any infections </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <h3 class="">There are several methods of treating hair loss: </h3>
                    <ul>
                        <li><em><strong>Non-surgical methods:</strong></em> If the hair loss is not too extensive, nonsurgical methods would be a good idea. These would include hair pieces, extensions, patches, weaves, Mesotherapy or topical therapies such as <a href="<?= base_url(); ?>blog/minoxidil-uses-dosage-precautions-and-side-effects/">Minoxidil</a> or <a href="<?= base_url(); ?>blog/finasteride-dosage-uses-side-effects-for-hair-loss/">Finasteride</a>. </li>
                        <li><em><strong>Hair transplant: </strong></em>If the condition is male pattern baldness, wherein a part of the scalp suffers complete or near complete hair loss, then hair transplant would be a good idea. There is the option of either follicular unit transplantation or extraction. These days, <a href="<?= base_url(); ?>hair-transplant/">hair transplants</a> can be done without much human intervention, in the manner of automated hair transplant too. </li>
                    </ul>
                </div>
                <p class="clr">At AK Clinics, we do all this and more. Walk into one of our clinics and an expert will diagnose your exact type of hair loss. After this, not only will you provide advice on the most appropriate solution, but also get it done for you! </p>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>