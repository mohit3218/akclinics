<style>
    .icontent {margin: 30px 50px 50px;}
    icontent h2 {font-size: 28px;font-weight: 100;letter-spacing: -1px;margin: 0 0 20px;}
    .icontent h5 {color: #f99c1c;font-size: 22px;font-weight: 400;letter-spacing: -0.5px;margin: 20px 0 8px;}
    .icontent p {color: #666;font-size: 15px;line-height: 22px;margin: 0 0 10px;text-align: justify;}
    .fbox {background-color: #fff;border: 1px solid #ddd;border-radius: 10px;box-shadow: 0 1px 1px #ccc;margin-bottom: 10px;padding: 12px 20px 20px;}
    .line {border-top: 1px dashed #ccc;margin-bottom: 15px;margin-top: 21px;width: 100%;}
    .f_row {margin: 0 0 7px;overflow: hidden;}
    .label {color: #666;float: left;font-size: 12px;padding: 5px 0 4px;width: 20%;}
    .labelform {color: #666;float: right;font-size: 12px;width: 78%;}
    .ftsnew {color: #666;font-family: Calibri;padding: 3px;width: 160px;}
    .g_baldness {float: left;margin-bottom: 10px;text-align: center;width: 20%;}
</style>
<div class="grid-main">
    <div style="float:right; margin: 5px 10px 0 0px"><div class="addthis_sharing_toolbox"></div></div>
</div>
<div class="mar25"></div>
<div class="icontent">
    <h5>Free Online Hair Restoration Consult</h5>
    <p> 
        To consult with one of the quality physicians recommended below form, please provide the following information. 
    </p>
    <form name="vc_form" method="post" action="">
        <div class="fbox">
            <p><strong>PERSONAL INFORMATION</strong></p>
            <div class="line"></div>
            <br>
            <div class="f_row">
                <div class="label">Your Name</div>
                <div class="labelform">
                    <input class="ftsnew" type="text" name="vc_name" id="name">
                </div>
            </div>
            <div class="f_row">
                <div class="label">Your Age</div>
                <div class="labelform"><input class="ftsnews" type="text" name="vc_age" id="name"><span style="font-size:12px;">Years Old</span></div>
            </div>
            <div class="f_row">
                <div class="label">Gender</div>
                <div class="labelform">
                    <select class="ftsnew" name="vc_gender" id="gender">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="fbox">
            <p><strong>GRADE OF BALDNESS FOR MALE</strong></p>
            <div class="line"></div>
            <br>
            <div class="f_row">
                <div class="label">Choose Your Grade</div>
                <div class="labelform">
                    <div class="g_baldness">
                        <label>
                            <img class="lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>gradeball1.jpg" width="64" height="40" alt="grade ball"><br>
                            2
                            <input type="radio" name="b_gr_val" id="male_grade" value="2">
                        </label>
                    </div>
                    <div class="g_baldness">
                        <label>
                            <img class="lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>gradeball4.jpg" width="64" height="40" alt="grade ball"><br>
                            2A
                            <input type="radio" name="b_gr_val" id="male_grade" value="2A">
                        </label>
                    </div>
                    <div class="g_baldness">
                        <label>
                            <img class="lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>gradeball3.jpg" width="64" height="40" alt="grade ball"><br>
                            3
                            <input type="radio" name="b_gr_val" id="male_grade" value="3">
                        </label>
                    </div>
                    <div class="g_baldness">
                        <label>
                            <img class="lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>gradeball2.jpg" width="64" height="40" alt="grade ball"><br>
                            3A
                            <input type="radio" name="b_gr_val" id="male_grade" value="3A">
                        </label>
                    </div>
                    <div class="g_baldness">
                        <label>
                            <img class="lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>gradeball5.jpg" width="64" height="40" alt="grade ball"><br>
                            3V
                            <input type="radio" name="b_gr_val" id="male_grade" value="3V">
                        </label>
                    </div>
                    <div class="g_baldness">
                        <label> 
                            <img class="lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>gradeball6.jpg" width="64" height="40" alt="grade ball"><br>
                            4
                            <input type="radio" name="b_gr_val" id="male_grade" value="4">
                        </label>
                    </div>
                    <div class="g_baldness">
                        <label>
                            <img class="lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>gradeball7.jpg" width="64" height="40" alt="grade ball"><br>
                            4V
                            <input type="radio" name="b_gr_val" id="male_grade" value="4V">
                        </label>
                    </div>
                    <div class="g_baldness">
                        <label> 
                            <img class="lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>gradeball11.jpg" width="64" height="40" alt="grade ball"><br>
                            5
                            <input type="radio" name="b_gr_val" id="male_grade" value="5">
                        </label>
                    </div>
                    <div class="g_baldness">
                        <label> 
                            <img class="lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>gradeball9.jpg" width="64" height="40" alt="grade ball"><br>
                            5A
                            <input type="radio" name="b_gr_val" id="male_grade" value="5A">
                        </label>
                    </div>
                    <div class="g_baldness">
                        <label> 
                            <img class="lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>gradeball10.jpg" width="64" height="40" alt="grade ball"><br>
                            5V
                            <input type="radio" name="b_gr_val" id="male_grade" value="5V">
                        </label>
                    </div>
                    <div class="g_baldness">
                        <label> 
                            <img class="lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>gradeball11.jpg" width="64" height="40" alt="grade ball"><br>
                            6
                            <input type="radio" name="b_gr_val" id="male_grade" value="6">
                        </label>
                    </div>
                    <div class="g_baldness">
                        <label> 
                            <img class="lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>gradeball12.jpg" width="64" height="40" alt="grade ball"><br>
                            7
                            <input type="radio" name="b_gr_val" id="male_grade" value="7">
                        </label>
                    </div>
                </div>
            </div>
            <br>
            <p><strong>Have you used the following treatments? </strong></p>
            <div class="f_row">
                <div class="label">Rogaine</div>
                <div class="labelform">
                    <label><input type="radio" value="Used Rogaine treatment in past" id="rogaine" name="rogaine"> Past</label>
                    <label><input type="radio" value="Using Rogaine treatment" id="rogaine" name="rogaine"> Present</label>
                    <label><input type="radio" value="Never Rogaine treatment" id="rogaine" name="rogaine"> Never</label>
                </div>
            </div>
            <div class="f_row">
                <div class="label">Propecia </div>
                <div class="labelform">
                    <label><input type="radio" name="propecia" id="propecia" value="Used Propecia treatment in past">Past</label>
                    <label><input type="radio" name="propecia" id="propecia" value="Using Propecia treatment">Present</label>
                    <label><input type="radio" name="propecia" id="propecia" value="Never Propecia treatment">Never</label>
                </div>
            </div>
            <br>
            <p>For how long have you been on any medication, if at all? </p>
            <div>
                <input class="ftsnew" type="text" name="vc_medication" id="name">
                <span style="font-size:12px;">(NA) if never.</span></div>
            <br>
            <h3>Your questions or concerns</h3>
            <div style="margin-bottom:12px;">
                <textarea class="ftsnewa" name="vc_questions" id="questions" cols="45" rows="2"></textarea>
            </div>
            <div class="f_row">
                <div class="label">Email Id:</div>
                <div class="labelform">
                    <input class="ftsnew" type="text" name="vc_email" id="name">
                </div>
            </div>
            <div class="f_row">
                <div class="label">Mobile No. :</div>
                <div class="labelform">
                    <input class="ftsnew" type="text" name="vc_mobile" id="name">
                </div>
            </div>
            <div class="line"></div>
            <br>
            <div> I want to be on the private newsletter list.
                <input type="checkbox" name="newsletterreceive" id="newsletter-receive" value="yes">
            </div>
            <div align="right">
                <input class="btn btn-warning ftssb" type="submit" name="submit_vc" value="Submit Now">
            </div>
        </div>
    </form>
    <div class="split"></div>
</div>