<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/"> <span itemprop="name">Our Team</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/dr-madhu-kuthial/"> <span itemprop="name">Dr Madhu Kuthial</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="content ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="text-left col-manage-section">
                <h2>Doctors Profile</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 dr-figure">
                <figure> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/dr-madhu-1.jpg" class="img-fluid"> </figure>
                <div class="doctors-social-icons text-center"> 
                    <a href="https://www.twitter.com/akclinics" target="_blank" class="icons"><i class="icofont-twitter  icofont-1x"></i></a> 
                    <a href="https://plus.google.com/+Akclinics" target="_blank" class="icons google"><i class="icofont-google-plus  icofont-1x"></i></a> 
                    <a href="https://www.linkedin.com/company/ak-clinics/" target="_blank" class="icons in"><i class="icofont-linkedin  icofont-1x"></i></a> 
                </div>
            </div>
            <div class="col-sm-8 doctors-details-section">
                <h1>Dr Madhu Kuthial</h1>
                <article class="dr-des">
                    <span>Qualification</span>
                    <span>MBBS, DVDL (Skin & VD)</span> 
                    <span>Consultant Dermatologist & Hair Transplant Surgeon</span></article>
                <p class="set-para">Dr Madhu kuthial, Dermatologist at AK Clinics has more than 4 years of experience in the field of Aesthetic Dermatology, Dermatology pathology and laser hair removal. She has worked as a consultant dermatologist in NSH Panchkula.</p>
                <span class="dr-des"><em>RESEARCH PROJECTS</em></span>
                <p class="set-para">Role of glutathione, superoxide dismutase and serum copper levels in Melasma</p>
                <p class="set-para">She has worked as a consultant dermatologist in NSH, Panchkula and NMH, Punjab. She has experience in clinical and cosmetic dermatology such as botox, fillers, threads, all kinds of hair removal, pigmentary and scar removal lasers, chemical peels, microdermabrasion and PRP for hair gain, facial rejuvenation and acne scar treatment. Dr. Madhu has attended workshops in various national conferences such as ACSICON, PIGMENTARYCON and DERMACON and is a member of Indian association of dermatologists. She has presented papers and posters at various regional and national conferences</p>

            </div>
        </div>
    </div>
</div>

<div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg" style="background-image: url(&quot;<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg&quot;); ">
    <div class="container pt-0 pb-0">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 sm-height-auto">
                    <div class="p-30 bg-deep-transparent " style="height:500px; overflow-y:scroll;"> and 
                        <h4 class="title mt-0 line-bottom line-height-2 text-black-222">Journal Publication & <span class="text-theme-colored">Poster Presentations</span></h4>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Role of serum copper levels and oxidative stress in melasma</h5>
                            </div>
                        </div>


                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Vulvo-vaginal ano- gingival syndrome: another variant of Mucosal Lichen Planus</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">
                                    Comparative efficacy of cellular grafting versus tissue grafting technique in stable vitiligo</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Safety and efficacy of DCP therapy in pemphigus patients: an observational study in tertiary care centre</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Comparative efficacy of vitamin c with Dermaroller versus PRP with Dermaroller in post acne scars</h5>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/meet_the_team_section'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>

