<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/"> <span itemprop="name">Our Team</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/dr-shraddha-uprety/"> <span itemprop="name">Dr Shraddha Uprety</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="content ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="text-left col-manage-section">
                <h2>Doctors Profile</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 dr-figure">
                <figure> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/dr-shraddha-1.png" class="img-fluid" alt="Dermatologist & Hair Transplant Surgeon at AK Clinics"> </figure>
                <div class="doctors-social-icons text-center"> 
                    <a href="https://www.twitter.com/akclinics" target="_blank" class="icons"><i class="fa fa-twitter fa-1x"></i></a> 
                    <a href="https://plus.google.com/+Akclinics" target="_blank" class="icons google"><i class="fa fa-google-plus fa-1x"></i></a> 
                    <a href="https://www.linkedin.com/company/ak-clinics/" target="_blank" class="icons in"><i class="fa fa-linkedin fa-1x"></i></a> 
                </div>
            </div>
            <div class="col-sm-8 doctors-details-section">
                <h1>Dr. Shraddha Uprety</h1>
                <article class="dr-des">
                    <span>Qualification</span>
                    <span>MBBS, MD</span>
                    <span>Dermatologist & Hair Transplant Surgeon</span> </article>
                <p class="set-para">Dr. Shraddha Uprety, Dermatologist at AK Clinics has more than 5 years of experience in the field of Aesthetic Dermatology, dermato-surgeries and hair restoration. She completed her M.D Dermatology, Venereology and Leprology from the India’s one of the top institute PGIMER (Post Graduate Institute of Medical Education and Research) and M.B.B.S from Koirala Institute of Health Science (BPKIHS), Nepal.</p>
                <span class="dr-des"><em>  Reserch Projects</em></span>
                <p class="set-para">A pilot randomized double blind study to assess the safety and efficacy of rituximab versus combination of rituximab and intravenous cyclophosphamide in the treatment of refractory pemphigus (PGIMER).</p>
                <p class="set-para">Over the course of these years, she has learned and mastered the art of dermatology care in the term of laser hair removal with diode and excels in other cosmetic procedures including facial peels, Botox, PRP therapy. She has worked with the leading dermatologists in India and has been into various International conferences. She has participated in all types of dermatological workshops of British Association of Dermatology, DERMACON by Indian Association of Dermatology, European Association of Dermato-Venereology.</p>

            </div>
        </div>
    </div>
</div>

<div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg" style="background-image: url(&quot;<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg&quot;); ">
    <div class="container pt-0 pb-0">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 sm-height-auto">
                    <div class="p-30 bg-deep-transparent " style="height:500px; overflow-y:scroll;">
                        <h4 class="title mt-0 line-bottom line-height-2 text-black-222">Textbook <span class="text-theme-colored">Publication</span></h4>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Clinical Diagnosis of Leprosy. International Textbook of Leprosy.</h5>
                            </div>
                        </div>

                        <h4 class="title mt-0 line-bottom line-height-2 text-black-222">Journal <span class="text-theme-colored">Publications</span></h4>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Itolizumab, a novel anti-CD6 monoclonal antibody: a safe and efficacious biologic agent for management of psoriasis.</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">
                                    Follicular pemphigus: an observation in patients treated with steroids.</h5>
                            </div>
                        </div>


                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Hypopigmented patches on a young man.</h5>
                            </div>
                        </div>

                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Young female with multiple pedunculated scalp tumors.</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/meet_the_team_section'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>

