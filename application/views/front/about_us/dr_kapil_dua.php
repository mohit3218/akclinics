<style>
.dr-des {
    font-size: 14px;
    font-style: normal;     padding: 20px;
    background: #efefef;
}

.doctors-details-section {
	padding:0!important;
    /* padding-left: 40px!important; */
}
@media (min-width:320px) and (max-width:640px){

.doctors-details-section{ padding:10px !important; text-align:center;}
	
	
	}
</style>

<div class="header-banner-content-area light-orange">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-left m-2 text-dark" >
        <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
            <meta itemprop="position" content="1" />
          </li>
          <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/"> <span itemprop="name">Our Team</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
            <meta itemprop="position" content="2" />
          </li>
          <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/dr-kapil-dua/"> <span itemprop="name">Dr Kapil Dua</span></a>
            <meta itemprop="position" content="3" />
          </li>
        </ul>
        <!-- Breadcrumbs /--> 
      </div>
    </div>
  </div>
</div>
<div class="content ptb-3">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 dr-figure">
        <figure> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/dr-kapil-dua-1.png" class="img-fluid" alt="Dr Kapil Dua - Best Hair Restoration Surgeon in India"> </figure>
        <div class="doctors-social-icons text-center"> <a href="https://twitter.com/drkapildua" target="_blank" class="icons"><i class="fa fa-twitter fa-1x"></i></a> <a href="https://plus.google.com/105305311373999416206" target="_blank" class="icons google"><i class="fa fa-google-plus fa-1x"></i></a> <a href="https://in.linkedin.com/in/drkapildua" target="_blank" class="icons in"><i class="fa fa-linkedin fa-1x"></i></a> <a href="<?= base_url('assets/template/frontend/pdf/') ?>Dr-Kapil-Dua.pdf" target="_blank" class="icons"><i class="fa fa-file-pdf-o fa-1x"></i></a> <a href="https://www.slideshare.net/akclinics1/profile-of-dr-kapil-dua-indias-leading-hair-transplant-surgeon" target="_blank" class="icons google"><i class="fa fa-slideshare fa-1x"></i></a> </div>
      </div>
      <div class="col-sm-8 doctors-details-section">
        
        <article class="dr-des mb-2 ">
        <h1>Dr Kapil Dua - India's Leading Hair Restoration Surgeon</h1>
         <h3>Qualification</h3> <em><strong><p>MBBS, MS, FISHRS, Diplomate ABHRS</p> <p>Founder & Chairman - AK Clinics</p></strong></em></article>
        <p>Dr. Kapil Dua, Co-Founder &amp; Chairman at <span itemprop="brand">AK Clinics</span> has over fourteen years of clinical and teaching experience in the fields of Otolaryngology &amp; Hair Transplant.</p>
       <blockquote class="doctors-quote">India's Leading Hair Restoration Surgeon. Dr Kapil Dua hosts India's first ISHRS Hair Transplant Workshop</blockquote> <p class="pt-1">As the Chief Hair Transplant Surgeon at AK Clinics, he has performed 1500+ successful surgeries. He holds the recognition for having transplanted over 3 million grafts, among the highest in the world, with a record of less than 3% wastage, among the best in the world. 
        He is among the key globally recognised FUE surgeons in the world and represents India on the Global Hair Transplant Council at ISHRS, USA. He had served as the Program Director of ISHRS Regional Workshop, the 1st ever ISHRS event in India. Dr. Kapil Dua has primarily practiced FUE (95% practice) from both scalp as well as body. He has performed many giga sessions (combining + FUT + Body Hair Transplant) successfully. He is one of the Best Hair Transplant Surgeon in India and has also treated patients from across the world.
        </p>
         <div class="text-center ptb-1" style="width:100%;">
                <a class="btn btn-gradient" href="#" data-target="#myModal" data-toggle="modal">Get Free Consultation</a>
              </div>
        
        <!--<div class="kd-logo">
          <div class="regular slider">
              <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>member-ishrs-min.png" alt="">
              <p class="logo-reco">Board of Governors ISHRS</p>
            </div>
            <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>aahrs.png" alt="">
              <p class="logo-reco">Board of Governors AAHRS</p>
            </div>
            <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>ahrs-india-min.png" alt="">
              <p class="logo-reco">Hon. President AHRS India</p>
            </div>
            <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>abhrs-min.png" alt="">
              <p class="logo-reco">Diplomate of ABHRS</p>
            </div>
            <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>htnetwork-min.png" alt="">
              <p class="logo-reco">Recommended by HTN</p>
            </div>
            <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>realself-topdoctor-min.png" alt="">
              <p class="logo-reco">Top Doctor Status realself</p>
            </div>
          </div>
        </div>--> 
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3  bg-light-gray">
  <div class="container">
    
    <div class="col-md-12 text-center">
      <div class="kd-logo">
        <div class="regular slider">
          <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>member-ishrs-min.png" alt="Board of Governors - ISHRS">
            <p class="logo-reco">Board of Governors ISHRS</p>
          </div>
          <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>aahrs.png" alt="Board of Governors - AAHRS">
            <p class="logo-reco">Board of Governors AAHRS</p>
          </div>
          <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>ahrs-india-min.png" alt="AHRAS">
            <p class="logo-reco">Hon. President AHRS India</p>
          </div>
          <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>abhrs-min.png" alt="Diplomate of ABHRS">
            <p class="logo-reco">Diplomate of ABHRS</p>
          </div>
          <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>htnetwork-min.png" alt="Recommended by HTN">
            <p class="logo-reco">Recommended by HTN</p>
          </div>
          <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>realself-topdoctor-min.png" alt="Top Doctor - Realself">
            <p class="logo-reco">Top Doctor Status realself</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="divider parallax layer-overlay overlay-dark-5 ptb-3" data-bg-img="<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg" style="background-image: url(&quot;<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg&quot;); ">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title">
          <h2 class="text-white text-center">Recognition & Achievements</h2>
        </div>
      </div>
    </div>
    <div class="col-md-12 pt-3">
      <div class="row text-muted">
        <div class="col-md-6">
          <ul style="list-style:none; padding:0;">
            <li class="pb-1">
              <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; Board of Governor at ISHRS, USA</span>
              <p class="text-muted">International Society of Hair Restoration Surgery Board of Governor ISHRS (International Society of Hair Restoration Surgery).</p>
            </li>
            <li class="pb-1">
              <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; Member of FUE Research Committee at ISHRS</span>
              <p class="text-muted">Member of FUE Research Committee of ISHRS (International Society of hair Restoration Surgery)</p>
            </li>
            <li class="pb-1">
              <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; Honorary President &amp; Founder Member of AHRS India</span>
              <p class="text-muted">Honorary President &amp; Founder Member of Association of Hair Restoration Surgeons of India(AHRS).</p>
            </li>
            <li class="pb-1">
               <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; Speaker &amp; Panelist in 40 Conferences</span>
              <p class="text-muted">Participation in 40 conferences and been a regular speaker &amp; panelist in them.</p>
            </li>
            <li class="pb-1">
              <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; Jt. Scientific Chairman of Jt 3rd Asian &amp; 5th AHRS India</span>
              <p class="text-muted">Jt. Scientific Chairman of Jt 3rd Asian &amp; 5th AHRS India Meeting of Hair Restoration Surgeons (Haircon 2013) 23rd Nov 2013 </p>
            </li>
            <li class="pb-1">
               <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; 1st Indian doctor to demonstrate FUE(2009)</span>
              <p class="text-muted">1st Indian doctor to demonstrate FUE along with Dr. Aman on live patient in Haircon 2009.</p>
            </li>
            <li class="pb-1">
               <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; 1st Indian doctor to be invited as a workshop director</span>
              <p class="text-muted">1st Indian doctor to be invited as a workshop director in ASM of ISHRS held in October 2013</p>
            </li>
            <li class="pb-1">
               <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; Jt. Scientific Secretary of 6th Haircon</span>
              <p class="text-muted">Jt. Scientific Secretary of 6th Haircon ( ASM of AHRS India), 19-21st Sept 2014</p>
            </li>
          </ul>
        </div>
        <div class="col-md-6">
          <ul style="list-style:none;padding: 0;">
            <li class="pb-1">
               <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; Board of Governor at AAHRS</span>
              <p class="text-muted">Asian Association of hair Restoration Surgeons Board of Governor AAHRS (Asian Association of hair Restoration Surgeons).</p>
            </li>
            <li class="pb-1">
               <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; 1st Indian doctor to be invited as a workshop director</span>
              <p class="text-muted">1st Indian doctor to be invited as a workshop director in ASM of ISHRS held in October 2013</p>
            </li>
            <li class="pb-1">
               <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; 1st Indian Doctor to present Video on FUE Technique.</span>
             <p class="text-muted">1st Indian doctor to present video on FUE technique in 1st Annual Scientific Meeting of AAHRS, 2011.</p>
            </li>
            <li class="pb-1">
              <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; Finance secretary of NW Zone of Association of Otolaryngologists of India</span>
              <p class="text-muted">Finance secretary of NW Zone of Association of Otolaryngologists of India (2008- Sept.2011)</p>
            </li>
            <li class="pb-1">
              <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; Member of FUE Research Committee at ISHRS</span>
             <p class="text-muted">Member of FUE Research Committee of ISHRS (International Society of hair Restoration Surgery)</p>
            </li>
            <li class="pb-1">
               <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; Only Indian to present a paper on Follicular Transection Rate</span>
             <p class="text-muted">Only Indian to present a paper on Follicular Transection Rate in ISHRS Meeting, Alaska 2011</p>
            </li>
            <li class="pb-1">
               <span class="text-white"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp; Workshop director on Ethnic Considerations in Hair Restoration</span>
              <p class="text-muted">Workshop director on Ethnic Considerations in Hair Restoration in 21st Annual Scientific Meeting of ISHRS, San Francisco, 23-26th Oct 2013.</p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!--<div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg" style="background-image: url(&quot;<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg&quot;); ">
  <div class="container pt-0 pb-0">
    <div class="section-content">
      <div class="row">
        <div class="col-md-8 sm-height-auto">
          <div class="p-30 bg-deep-transparent " style="height:500px; overflow-y:scroll;">
            <h4 class="title mt-0 line-bottom line-height-2 text-black-222">Recognition & <span class="text-theme-colored">Achievements</span></h4>
            <p>The patient satisfaction & results is the main criteria for the success of a surgeon. But equally important is the recognition from the very own medical fraternity. When a surgeon’s work is evaluated by critics from the fraternity, the surgeon works on further finesse. This again benefits the patients.</p>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  Board of Governor at ISHRS, USA</h5>
                <p class="text-black-333">International Society of Hair Restoration Surgery Board of Governor ISHRS (International Society of Hair Restoration Surgery).</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  Member of FUE Research Committee at ISHRS</h5>
                <p class="text-black-333">Member of FUE Research Committee of ISHRS (International Society of hair Restoration Surgery)</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  Honorary President & Founder Member of AHRS India</h5>
                <p class="text-black-333">Honorary President & Founder Member of Association of Hair Restoration Surgeons of India(AHRS).</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  Speaker & Panelist in 40 Conferences</h5>
                <p class="text-black-333">Honorary President & Founder Member of Association of Hair Restoration Surgeons of India(AHRS).</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  Jt. Scientific Chairman of Jt 3rd Asian & 5th AHRS India</h5>
                <p class="text-black-333">Jt. Scientific Chairman of Jt 3rd Asian & 5th AHRS India Meeting of Hair Restoration Surgeons (Haircon 2013) 23rd Nov 2013</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  1st Indian doctor to demonstrate FUE(2009)</h5>
                <p class="text-black-333">1st Indian doctor to demonstrate FUE along with Dr. Aman on live patient in Haircon 2009.</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  1st Indian doctor to be invited as a workshop director</h5>
                <p class="text-black-333">1st Indian doctor to be invited as a workshop director in ASM of ISHRS held in October 2013</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  Jt. Scientific Secretary of 6th Haircon</h5>
                <p class="text-black-333">Jt. Scientific Secretary of 6th Haircon (ASM of AHRS India), 19-21st Sept 2014</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  Board of Governor at AAHRS</h5>
                <p class="text-black-333">Asian Association of hair Restoration Surgeons Board of Governor AAHRS (Asian Association of hair Restoration Surgeons).</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  1st Indian doctor to be invited as a workshop director</h5>
                <p class="text-black-333">1st Indian doctor to be invited as a workshop director in ASM of ISHRS held in October 2013</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  1st Indian Doctor to present Video on FUE Technique.</h5>
                <p class="text-black-333">1st Indian doctor to present video on FUE technique in 1st Annual Scientific Meeting of AAHRS, 2011.</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  Finance secretary of NW Zone of Association of Otolaryngologists of India</h5>
                <p class="text-black-333">Finance secretary of NW Zone of Association of Otolaryngologists of India (2008- Sept.2011)</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  Member of FUE Research Committee at ISHRS</h5>
                <p class="text-black-333">Member of FUE Research Committee of ISHRS (International Society of hair Restoration Surgery)</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  Only Indian to present a paper on Follicular Transection Rate</h5>
                <p class="text-black-333">Only Indian to present a paper on Follicular Transection Rate in ISHRS Meeting, Alaska 2011</p>
              </div>
            </div>
            <div class="items">
              <div class="icon-box">
                <h5 class="icon-box-title"><i class="fa fa-trophy fa-2x" aria-hidden="true"></i>&nbsp;  Workshop director on Ethnic Considerations in Hair Restoration</h5>
                <p class="text-black-333">Workshop director on Ethnic Considerations in Hair Restoration in 21st Annual Scientific Meeting of ISHRS, San Francisco, 23-26th Oct 2013.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>-->
<div class="service-area ptb-3 bg-light-gray">
  <div class="container">
    <div class="col-md-12 fun-facts">
      <div class="row">
        <div class="col-md-3 fun-col"> <strong class="big-font">2000</strong>
          <p> Happy Patients</p>
        </div>
        <div class="col-md-3 fun-col"> <strong class="big-font">5 Million</strong>
          <p>Grafts transplanted</p>
        </div>
        <div class="col-md-3 fun-col"> <strong class="big-font">3% or Less</strong>
          <p>Controlled FTR</p>
        </div>
        <div class="col-md-3 fun-col"> <strong class="big-font">18-75</strong>
          <p>Age Group</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3 ">
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="section-title">
        <h2 class="black-text text-center">International Organizations that Recommend Dr. Kapil Dua</h2>
      </div>
    </div>
    <div class="col-md-12 text-center pt-3">
      <div class="row">
        <div class="col-md-4"> <a href="https://www.hairtransplantnetwork.com/Consult-a-Physician/doctors.asp?DrID=678"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/htn.png" alt="Hair Transplant Network"/> </a>
          <h4>Hair Transplant Network</h4>
          <p class="new">HTN is an International forum dedicated to share about hair loss remedies that actually work, view over 3680 hair restoration photo albums of actual patients. Hair Transplantation Network helps you to find the prescreened physician nearby and provides you all the educational information related to hair fall or its treatment.</p>
        </div>
        <div class="col-md-4"><a href="https://www.abhrs.com/members/D"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/abhrs.png" alt="American Board of Hair Restoration Surgery (ABHRS)"/></a>
          <h4>ABHRS</h4>
          <p class="new">The American Board of Hair Restoration Surgery (ABHRS) is a surgical certifying board organized to examine physicians in the area of hair restoration surgery and promote the safe practice of hair restoration surgery. The ABHRS is the only certification recognized by the International Society of Hair Restoration Surgery (ISHRS).</p>
        </div>
        <div class="col-md-4"><a href="https://www.realself.com/find/India/India/Hair-Restoration-Surgeon/Kapil-Dua"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/realself.png" alt="Realself "/></a>
          <h4>RealSelf</h4>
          <p class="new">Realself has created a world’s largest community for learning and also provides a wide range to choose from over 12,000 board-certified doctors. Realself even guide you to locate the top nearby doctor or surgeon and helps you to get your personalized answers or queries from worldwide doctors.</p>
        </div>
        <div class="clr pt-1"></div>
        <div class="col-md-4"> <a href="https://www.ishrs.org/users/drkapildua"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/ishrs.png" alt="American Board of Hair Restoration Surgery (ABHRS)" /></a>
          <h4>Board of Governor at ISHRS, USA </h4>
          <p class="new">The American Board of Hair Restoration Surgery (ABHRS) is a surgical certifying board organized to examine physicians in the area of hair restoration surgery and promote the safe practice of hair restoration surgery. The ABHRS is the only certification recognized by the International Society of Hair Restoration Surgery (ISHRS).</p>
        </div>
        <div class="col-md-4"><a href="https://www.ishrs.org/users/drkapildua"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/fellow-ishrs.png" alt="Dr Kapil Dua - Elite doctors ISHRS"  /></a>
          <h4>Fellow of the ISHRS, USA </h4>
          <p class="new">There are only a selected few hair transplant surgeons who are taken in the premier league and to be a fellow means that you are allowed to use the abbreviation, FISHRS, an honour that is not given to anyone till he has earned it. However, Dr. Kapil Dua has entered this list of the elite doctors.</p>
        </div>
        <div class="col-md-4"> <a href="https://www.ishrs.org/users/drkapildua"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/hairsite.png" alt="Hair Site"/></a>
          <h4>Hair Site </h4>
          <p class="new">For close to two decades, this has been the ultimate guide for people practising and looking for hair transplants. Being recommended by such a elite portal is definitely a achievement worth sharing. Hair site is serving people since 1997. You can connect up to 20 hair doctors or surgeons worldwide in just one click.</p>
        </div>
        
        <!--single service --> 
        
      </div>
    </div>
  </div>
</div>


<div class="service-area ptb-3 bg-light-orange">
  <div class="container">
    <div class="col-md-12">
      <div class="row clearfix"> 
        
        <!--Content Column-->
        <div class="col-lg-9 col-md-12- col-sm-12">
          <div class="">
            <strong style="font-size:25px;padding-top: .5rem;">Suffering from Hair Loss or Planning for hair Transplant?</strong>
       
          </div>
        </div>
        
        <!--Button Column-->
        <div class="button-column col-lg-3 col-md-12- col-sm-12">
          <div class="">
             <a class="btn btn-gradient" href="#" data-target="#myModal" data-toggle="modal">Get Free Consultation</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--<div class="service-area ptb-3 ">
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="section-title">
        <h2 class="black-text text-center">International Organizations that Recommend Dr. Kapil Dua</h2>
      </div>
    </div>
    <div class="col-md-12 text-center pt-3">
      <div class="regular slider">
        <div> <a href="https://www.hairtransplantnetwork.com/Consult-a-Physician/doctors.asp?DrID=678"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/htn.png"/> </a>
          <h3>Hair Transplant Network</h3>
          <p class="new">HTN is an International forum dedicated to share about hair loss remedies that actually work, view over 3680 hair restoration photo albums of actual patients. Hair Transplantation Network helps you to find the prescreened physician nearby and provides you all the educational information related to hair fall or its treatment.</p>
        </div>
        <div> <a href="https://www.abhrs.com/members/D"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/abhrs.png"/></a>
          <h3>ABHRS</h3>
          <p class="new">The American Board of Hair Restoration Surgery (ABHRS) is a surgical certifying board organized to examine physicians in the area of hair restoration surgery and promote the safe practice of hair restoration surgery. The ABHRS is the only certification recognized by the International Society of Hair Restoration Surgery (ISHRS).</p>
        </div>
        <div> <a href="https://www.realself.com/find/India/India/Hair-Restoration-Surgeon/Kapil-Dua"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/realself.png"/></a>
          <h3>RealSelf</h3>
          <p class="new">Realself has created a world’s largest community for learning and also provides a wide range to choose from over 12,000 board-certified doctors. Realself even guide you to locate the top nearby doctor or surgeon and helps you to get your personalized answers or queries from worldwide doctors.</p>
        </div>
        <div> <a href="https://www.ishrs.org/users/drkapildua"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/ishrs.png" /></a>
          <h3>Board of Governor at ISHRS, USA </h3>
          <p class="new">The American Board of Hair Restoration Surgery (ABHRS) is a surgical certifying board organized to examine physicians in the area of hair restoration surgery and promote the safe practice of hair restoration surgery. The ABHRS is the only certification recognized by the International Society of Hair Restoration Surgery (ISHRS).</p>
        </div>
        <div> <a href="https://www.ishrs.org/users/drkapildua"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/fellow-ishrs.png" /></a>
          <h3>Fellow of the ISHRS, USA </h3>
          <p class="new">There are only a selected few hair transplant surgeons who are taken in the premier league and to be a fellow means that you are allowed to use the abbreviation, FISHRS, an honour that is not given to anyone till he has earned it. However, Dr. Kapil Dua has entered this list of the elite doctors.</p>
        </div>
        <div> <a href="https://www.ishrs.org/users/drkapildua"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/hairsite.png"/></a>
          <h3>Hair Site </h3>
          <p class="new">For close to two decades, this has been the ultimate guide for people practising and looking for hair transplants. Being recommended by such a elite portal is definitely a achievement worth sharing. Hair site is serving people since 1997. You can connect up to 20 hair doctors or surgeons worldwide in just one click.</p>
        </div>
        
        <!--single service
        
      </div>
    </div>
  </div>
</div>--> 

  <?php $this->load->view('front/partials/dr_blog_block'); ?>

<?php $this->load->view('front/partials/doctor_contacts'); ?>
<?php $this->load->view('front/partials/form_dr'); ?>
<script>

 $(document).on('ready', function () {
        $(".regular").slick({
            dots: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
</script>