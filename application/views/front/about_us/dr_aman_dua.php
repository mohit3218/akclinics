<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/"> <span itemprop="name">Our Team</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/dr-aman-dua/"> <span itemprop="name">Dr Aman Dua</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="content ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="text-left col-manage-section">
                <h2>Doctors Profile</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 dr-figure">
                <figure> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/dr-aman-dua-1.png" class="img-fluid" alt="Dr Aman Dua - best Dermatologists in Delhi"> </figure>
                <div class="doctors-social-icons text-center"> 
                    <a href="https://www.twitter.com/akclinics" target="_blank" class="icons"><i class="fa fa-twitter fa-1x"></i></a> 
                    <a href="https://plus.google.com/+Akclinics" target="_blank" class="icons google"><i class="fa fa-google-plus fa-1x"></i></a> 
                    <a href="https://www.linkedin.com/company/ak-clinics/" target="_blank" class="icons in"><i class="fa fa-linkedin fa-1x"></i></a> </div>
            </div>
            <div class="col-sm-8 doctors-details-section">
                <h1>Dr. Aman Dua, Co-Founder & Managing Director at AK Clinics</h1>
                <article class="dr-des">
                    <span>Qualification</span>
                    <span>MBBS, MD, FISHRS</span></article>
                <span class="dr-des"><em>India’s leading Dermatologist & Hair Transplant Surgeon</em></span>
                <p class="set-para">Dr. Aman Dua, Co-Founder & Managing Director at AK Clinics has over 14 years of clinical and teaching experience in the fields of Dermatology and Hair Transplant. Clinically, she has practiced aesthetic dermatology as a Consultant at Dayanand Medical Hospital, where she also served as the Assistant Professor & Skin Specialist for a number of years. Over the course of her career, she has garnered acclaim as a facial aesthetics expert, with special interest in non-surgical acne scar removal. She practices as a Chief Dermatologist at South Delhi & Ludhiana Clinics.</p>
                <blockquote class="doctors-quote"> India’s leading Dermatologist & Hair Transplant Surgeon</blockquote>  <p class="set-para">She has been a pillar of excellence at AK Clinics and as the Chief Dermatologist & Consultant Hair Transplant Surgeon at AK Clinics, she has performed a record number of successful non-surgical dermatological procedures and hair transplants since 2008. She is one of the most sought after teachers and has also regularly served as a faculty and speaker at national conferences on Aesthetic Dermatology and <a href="<?= base_url(); ?>hair-transplant/" target="_blank">Hair Transplant Surgery</a>. Indeed she is acclaimed by many as one of the best dermatologist in Delhi NCR and Punjab Region for treatment of skin problems and hair loss issues.</p>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2 class="black-text text-center">More about Dr Aman Dua</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <p>Her significant achievements include serving as host of ISHRS Regional Hair Transplant Workshop, which was held in India for the first time in 2015. She is a Founder member and on the Board of Governors of Association of Hair Restoration Surgeons India (AHRS India) as well as the Founder Editor for the Journal of AHRS India. She holds the venerated position of being the first Indian Female FUE Hair Transplant Surgeon, as well as the first Female Indian doctor to a present research paper on Hair Transplant at an international conference. In the past, she has also served as the Finance Secretary of NW Zone of Indian Association of Dermatologists, Venereologists and Leprologists.</p>
                <blockquote>  <p>Dr. Aman has also received numerous awards and accolades in various fields, including winning gold medals in Pharmacology and Microbiology. She has co-authored numerous books and research papers which have received national and international acclaim. She is also an esteemed member of several globally recognised organisations, viz., International Society of Hair Restoration Surgeons, Association of Hair Restoration Surgeons India, Association of Cutaneous Surgeons of India, Indian Association of Dermatologists, Venereologists & Leprologists, People’s Health Organization (India) and the Indian Medical Association.</p>  </blockquote>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-orange">
    <div class="container">
        <div class="col-md-12 fun-facts">
            <div class="row">
                <div class="col-md-3 fun-col"> <strong class="big-font">2000</strong>
                    <p> Happy Patients</p>
                </div>
                <div class="col-md-3 fun-col"> <strong class="big-font">5 Million</strong>
                    <p>Grafts transplanted</p>
                </div>
                <div class="col-md-3 fun-col"> <strong class="big-font">3% or Less</strong>
                    <p>Controlled FTR</p>
                </div>
                <div class="col-md-3 fun-col"> <strong class="big-font">18-75</strong>
                    <p>Age Group</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg" style="background-image: url(&quot;<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg&quot;); ">
    <div class="container pt-0 pb-0">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 sm-height-auto">
                    <div class="p-30 bg-deep-transparent " style="height:500px; overflow-y:scroll;">
                        <h4 class="title mt-0 line-bottom line-height-2 text-black-222">Recognition & <span class="text-theme-colored">Achievements</span></h4>
                        <p>The patient satisfaction & results is the main criteria for the success of a surgeon. But equally important is the recognition from the very own medical fraternity. When a surgeon’s work is evaluated by critics from the fraternity, the surgeon works on further finesse. This again benefits the patients.</p>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Board of Governor Association of Hair Restoration Surgeons India (AHRS India)</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Former Finance Secretary Former Finance Secretary – NW Zone of Indian Association of Dermatologists, Venerologist and Leprologist .</h5>

                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">1st Female Indian doctor to present paper on Hair Transplant in international conference.</h5>

                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">1st Indian Female FUE Hair Transplant Surgeon.</h5>

                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Participation in 24 conferences and presented papers in them.</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">The youngest, sought after & respected teacher in FUE Hair Transplant in India.</h5>

                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Program Director of ISHRS Regional Workshop 2015 at New Delhi, 1st of its kind in India.</h5>

                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Faculty for FUE Hair Transplant in most ISHRS Conferences; to be held at Kualalumpur in 2014.</h5>

                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">
                                    REGULAR Faculty & Course Director for FUE Hair Transplant at DAAS Summit, Delhi Aesthetics, Hair India, Haircon – all conferences in India attended by 1000s of doctors.</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="section-title">
                <h2 class="black-text text-center">Dr. Aman Dua is member of following International Organizations</h2>
            </div>
        </div>
        <div class="col-md-12 text-center pt-3">
            <div class="regular slider">
                <div>
                    <img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/aahrs.png" alt="Member of Asian Association of Hair Restoration Surgeons (AAHRS)"/> 
                    <h3>AAHRS</h3>
                    <p class="new">The Asian Association of Hair Restoration Surgeons (AAHRS) is a non-profit voluntary organization of hair restoration doctors who practices for Asian patients.</p>
                </div>
                <div> 
                    <img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/ahrs.png" alt="Member of AHRS India"/>
                    <h3>AHRS India</h3>
                    <p class="new">Objectives of AHRS-India is to act for the benefit of the public to establish specialty standards and to examine surgeons' skill, knowledge and aesthetic judgment in the field of hair restoration.</p>
                </div>
                <div> 
                    <img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/IADVL.png" alt="Member of IADVL"/>
                    <h3>IADVL</h3>
                    <p class="new">IADVL is one of the largest dermatologic associations in the world. With a membership touching nearly 7500, IADVL is the most representative organization of the composite discipline of Dermato-venereo-leprology in India</p>
                </div>
                <div>  
                    <img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/ishrs.png" alt="Member of International Society of Hair Restoration Surgery" />
                    <h3>International Society of Hair Restoration Surgery</h3>
                    <p class="new">Her dedication to the improvement of the technique to benefit the entire mankind motivates him to create & share every day.</p>
                </div>
                <div>  
                    <img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/fellow-ishrs.png" alt="Fellow of the ISHRS, USA" />
                    <h3>Fellow of the ISHRS, USA </h3>
                    <p class="new">There are only a selected few hair transplant surgeons who are taken in the premier league and to be a fellow means that you are allowed to use the abbreviation, FISHRS, an honour that is not given to anyone till he has earned it. However, Dr. Aman Dua has entered this list of the elite doctors.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/dr_blog_block'); ?>
<?php $this->load->view('front/partials/meet_the_team_section'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>