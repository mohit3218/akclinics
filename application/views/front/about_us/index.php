<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a itemprop="item" href="<?= base_url(); ?>"><span itemprop="name">Home</span></a> <i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>about-us/"> <span itemprop="name">About Us</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">AK Clinics – India’s Leading Cosmetic Surgery Clinics</h1>
                </div>
                <p><strong><em>“AK Clinics was founded with the goal of transformation of looks of the entire human race. The purpose of existence of AK Clinics is to provide excellent treatments at an affordable cost.”</em></strong> This is not mere statement but a religion with us. Our team strives very hard to meet up with ever expanding needs &amp; wishes of the patients. AK Clinics is amongst the fastest growing cosmetic surgery clinics having served thousands patients every year in our various branches. We’ve been working very hard to achieve excellence in the services we provide especially <a href="<?= base_url(); ?>hair-transplant/">hair transplant</a>. This is the reason all our efforts &amp; standards are at par or better than the best in the world.</p>
                <ul>
                    <li>Infrastructure</li>
                    <li><a href="<?= base_url(); ?>about-us/our-team/">Our Team</a></li>
                    <li><a href="<?= base_url(); ?>results/">Our Results</a></li>
                    <li><a href="<?= base_url(); ?>virtual-consultation/">Free Online Counseling</a></li>
                </ul>
                <p><strong>AK Clinics</strong>, founded in 2008,with the aim of transforming the looks of the people by <em>Dr. Kapil Dua</em> and <em>Dr. Aman Dua</em>. Both the doctors were working in Dayanand Medical College as faculty at that time. They started off with Hair Transplant in 2007 in Dayanand Medical College &amp; Hospital, Ludhiana. But very soon found out that the hard work, dedication, commitment, patient comfort, team training and management of surgeries were not easy in a medical college. Therefore, they decided to set up their own Hair Transplant &amp; Aesthetic Dermatology Centre by the name of AK Clinics on 14th August, 2008. Then the second clinic was set up in Delhi in 2011 and the operations were started in 2016 and tie-ups in Bangalore. In these years, AK Clinics has made a name for itself and is regarded as a Premier Hair Transplant &amp; Aesthetic Dermatology Centre both by patients for treatment and doctors for learning these procedures. The <em>doctors</em> at AK Clinics understand the agony of the patients with <a href="<?= base_url(); ?>hair-loss/">hair loss</a> and treat with extreme sensitivity. Not only that we did many successful hair transplants but also have hope of looks to 100s of patients suffering different kind of alopecia. In fact we feel we’ve tried our level best not only to provide the best of the treatments but also right advice to everyone. That is the reason we provide completely free online counseling to the patients. Out doctors are accredited by many national &amp; international medical associations. </p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>about-us/our-team/">Our Team</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant-training/">Hair Transplant Training</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray  ">
    <div class="container">
        <div class="row">
            <div class="section-title text-center ">
                <h2 class=" black-text">Our Achievements</h2>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row ptb-1">
                <p>AK Clinics is credited for being a first of its kind exclusive hair restoration center of north India since 2007. We are pioneers of FUE Hair Transplant in India.We are proud of the fact that we have full hair restoration facilities starting from clinical diagnosis, hair analysis, scalp biopsy, customized medical treatments , Bio-therapy (PRP), meso-therapy, laser therapy for hair growth, derma-rollers and last but not the least hair transplant which includes both FUT and FUE Hair Transplant</p>
                <h3>We were credited with being Founder Member Award of AHRS- INDIA-Association of Hair Restoration Surgeons of India.</h3>
                <ul>
                    <li>Dr. Kapil Dua is currently the Vice President of AHRS India</li>
                    <li>Dr. Aman Dua is the Ex-Editor of AHRS India</li>
                    <li>Dr. Kapil Dua has been chosen as the Board of Governor of Asian Association of Hair Restoration Surgeons – AAHRS Asia</li>
                    <li>Dr. Kapil Dua has represented India in the Global Council Meeting of ISHRS ( 15 countries are a part of this committee)</li>
                    <li>Dr. Kapil Dua is a part of the FUE Research Committee of ISHRS ( 15 doctors selected from all over the world)</li>
                </ul>
            </div>
            <p>We are part of 6 clinics throughout the world credited to work on FUE Research project of ISHRS.We are trainers and teachers. we hold fellowships and observer ships for interested doctors in hair restoration.</p>
        </div>
    </div>
</div>
<div class="service-area ptb-3 ">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle ">Infrastructure</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Infrastructure</h3>
                    <p>AK Clinics has two 3 Hair Transplant centres in <a href="<?= base_url(); ?>locations/hair-transplant-delhi/">New Delhi</a>, <a href="<?= base_url(); ?>locations/hair-transplant-ludhiana/">Ludhiana</a> and <a href="<?= base_url(); ?>bangalore/">Bangalore</a>. The centres are fully modern , well equipped , spacious and technically designed works of architecture which cater to needs of hair restoration patients. Both the centres are aesthetically designed and have pleasing interiors.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Operation Theatres</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Operation Theatres</h3>
                    <ul>
                        <li>State-of-art designing</li>
                        <li>Ultra-hygienic ensures strict asepsis and cleanliness.</li>
                        <li>Comfort of home – comfortable OT tables, music system and</li>
                        <li>audio visual aids to keep the patient relaxed</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Machinery & Equipment</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Machinery & Equipment</h3>
                    <ul>
                        <li>Latest &amp; imported equipment &amp; instruments</li>
                        <li>Advanced Microscopes and OT lights.</li>
                        <li>Only use of disposables to ensure – Zero infection.</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Team</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Team</h3>
                    <ul>
                        <li>Highly trained, experienced &amp; full-time technicians</li>
                        <li>Passed through high health standards &amp; tests to ensure patient safety</li>
                        <li>Administration team is cordial &amp; trained to ensure best comfort</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
