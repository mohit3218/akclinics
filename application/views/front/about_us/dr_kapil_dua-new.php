<link rel="stylesheet" href="<?php echo cdn('assets/template/frontend/'); ?>css/easy-responsive-tabs-min.css">
<style>
.dr-des {
	font-size: 14px;
	font-style: normal;
	padding: 20px;
	background: #efefef;
}
.achiv-block {
	display: none;
}
a#achievements-block-1 {
	display: none;
}
.doctors-details-section {
	padding: 0!important;/* padding-left: 40px!important; */
}
.slick-slide {
	width: 170px;
	margin-top: 25px;
	border-radius: 5px;
}
.videoShow {
	width: 640px;
	height: 360px;
}
.btSuperTitle {
	margin: 0 0 3px;
	font-size: 36px;
	line-height: 1.55;
	font-weight: 700;
}
b.animate {
	font-size: 56px;
	font-weight: 300;
	line-height: 65px;
	color: #333;
}
.btSuperTitle:after {
	margin-left: 10%;
}
.regular {
	font-weight: 400!important;
}
.bonus {
	font-size: 15px;
	line-height: 1.66667;
	color: #a0a3a7;
	letter-spacing: .025em;
	margin-bottom: 3.4em;
}
.bold {
	font-weight: 700!important;
}
.red {
	color: #f16334!important;
}
/*.header-banner-content-area {
	display: none !important;
}*/


ul {
	list-style: none;
	padding: 0;
}
ul.navigation {
	display: inline-block;
	width: 100%;
}
ul.navigation li {
	float: left;
	padding: 5px;
}
ul li {
	display: list-item;
	padding: 3px 0;
}
ul li i {
	padding: 5px;
}
.fun-facts .fun-col {
	border-right: 2px solid #cdcdcd;
	color: #333;
	display: inline-block;
	float: left;
	text-align: center;
	width: 25%;
}
.fun-facts .fun-col:last-child {
	border-right: 0px solid #cdcdcd;
}
.icofont-hat-alt {
	font-size: 30px;
	margin-top: 4px;
	margin-right: 10px;
	color: #ff9000!important;
}
.slick-prev {
	left: -17px;
	z-index: 1;
}
.btImage img {
	max-width: 100%;
	height: auto;
	width: 90%;
	margin-bottom: -15px;
}
/*realself*/
#rsrvwrapper, #rsrvwrapper *, #rsrvwrapper *::before, #rsrvwrapper *::after {
	box-sizing: border-box;
}
#rsrvwrapper {
	overflow: hidden;
	border: 1px solid #adabab;
	margin-bottom: 30px;
	background-color: #fff;
	text-align: left;
	padding: 0 30px;
}
#rsrvwrapper a {
	text-decoration: none;
	outline: 0;
}
#rs-reviews-link {
	position: absolute;
	z-index: -1;
}
#rsrvwrapper .star {
	height: 17px;
	width: 17px;
	margin-top: -5px;
	display: inline-block;
	background-repeat: no-repeat;
	text-indent: -9999px;
	vertical-align: middle;
	background-size: 100% 100%;
	background-image: url("data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjQ1IiB2aWV3Qm94PSIwIDAgNDcgNDUiIHdpZHRoPSI0NyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJtNDcgMTcuNjA5aC0xNy45MTlsLTUuNTgxLTE3LjYwOS01LjU4MSAxNy42MDloLTE3LjkxOWwxNC40OTIgOS45NzgtNS42OCAxNy40MTMgMTQuNjg4LTEwLjc2IDE0LjY4OCAxMC43Ni01LjY4LTE3LjQxM3oiLz48L3N2Zz4=");
}
#rsrvwrapper .star-gray {
	background-image: url("data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgNTEuMzQgNDkuMTUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0ibTUxLjM0IDE5LjI1aC0xOS41MmwtNi4xNS0xOS4yNS02LjEyIDE5LjI1aC0xOS41NWwxNS43OCAxMC45LTYuMTUgMTkgMTYtMTEuNzcgMTYgMTEuNzctNi4xNS0xOXoiIGZpbGw9IiNlOWU0ZGUiLz48L3N2Zz4=");
}
#rsrvwrapper .star-half {
	background-image: url("data:image/svg+xml;base64,PHN2ZyBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MS4yIDQ5IiB2aWV3Qm94PSIwIDAgNTEuMiA0OSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJtNDkuNiAxOS42aC0xOC4ybC01LjgtMTgtNS43IDE4aC0xOC4zbDE0LjggMTAuMi01LjggMTcuOCAxNS0xMSAxNSAxMS01LjctMTcuOHoiIGZpbGw9IiNlOWU0ZGUiLz48cGF0aCBkPSJtNDkuNiAxOS42aC0xOC4ybC01LjgtMTgtNS43IDE4aC0xOC4zbDE0LjggMTAuMi01LjggMTcuOCAxNS0xMSAxNSAxMS01LjctMTcuOHoiIGZpbGw9Im5vbmUiIHN0cm9rZT0iI2U5ZTRkZSIvPjxwYXRoIGQ9Im0xOS41IDE5LjJoLTE5LjVsMTUuOCAxMC45LTYuMiAxOC45IDE2LTExLjd2LTM3LjN6Ii8+PC9zdmc+");
}
#rsrvwrapper .rsrvheader {
	background-color: #000;
	padding: 12px 0 16px 0;
	margin: 0 -30px 33px;
	text-align: center;
}
#rsrvwrapper .rsrvheader img {
	width: 100px;
	height: 22px;
	vertical-align: middle;
}
#rsrvwrapper .rsrvsubhead {
	display: flex;
	align-items: center;
	margin-bottom: 30px;
}
#rsrvwrapper .rsrvbyline a {
	color: #000 !important;
	cursor: pointer;
	font-weight: bold;
	font-family: "Poppins";
	font-size: 20px;
	outline: none;
}
#rsrvwrapper .rsrvimage {
	margin-right: 18px;
}
#rsrvwrapper .rsrvimage img {
	width: 70px;
	height: 70px;
	border: 1px solid #000;
	border-radius: 100%;
}
#rsrvwrapper .rsrvrating .star {
	width: 21px;
	height: 21px;
}
#rsrvwrapper .rsrvrating a {
	font-family: "Roboto";
	font-size: 14px;
	color: #848381 !important;
	text-decoration: underline !important;
	font-weight: normal;
}
#rsrvwrapper .rsrvtopic {
	text-align: center;
	margin-bottom: 20px;
}
#rsrvwrapper .rsrvtopicselect {
	width: 100%;
	padding: 5px;
	border-radius: 2px;
	background-color: #fff;
	border: solid 1px #cececd;
	font-family: "Poppins";
	font-size: 14px;
	color: #000;
}
#rsrvwrapper .rsrvreviews {
	padding-bottom: 15px;
}
#rsrvwrapper .rsrvreviewwrapper {
	display: block;
	margin-bottom: 15px;
}
#rsrvwrapper .rsrvreviewwrapper+.rsrvreviewwrapper {
	border-top: 1px solid #cececd;
	padding-top: 15px;
}
#rsrvwrapper .rsrvreview {
	color: #000;
}
#rsrvwrapper .rsrvreview:hover {
	background-color: #f3f3f2;
}
#rsrvwrapper .rsrvreviewtitle {
	display: inline;
	font-family: "Roboto";
	font-size: 16px;
	font-weight: bold;
	margin-left: 10px;
}
#rsrvwrapper .rsrvreviewsnippet {
	font-family: "Roboto";
	font-size: 14px;
	font-weight: normal;
	line-height: 28px;
}
#rsrvwrapper .rsrvreviewreadmore {
	font-size: 14px;
	position: relative;
	font-family: "Poppins";
	font-weight: bold;
	padding: 0 15px 0 5px;
}
#rsrvwrapper .rsrvreviewreadmore::after {
	content: "";
	width: 0;
	height: 0;
	position: absolute;
	top: 7px;
	right: 5px;
	border-style: solid;
	border-width: 5px 0 5px 7px;
	border-color: transparent transparent transparent #000;
}
#rsrvwrapper .rsrvmorereviews a {
	background-color: #ff8580;
	border-radius: 100px;
	display: block;
	font-family: "Poppins";
	font-size: 14px;
	color: #fff;
	font-weight: bold;
	text-transform: uppercase;
	padding: 5px 0;
	margin-bottom: 15px;
	text-align: center;
}
#rsrvwrapper .rsrvmorereviews button:hover {
	background-color: #ef5e5b;
}
#rsrvwrapper .rsrvdisclaimer {
	font-family: "Roboto";
	font-size: 14px;
	line-height: 1.43;
	color: #848381;
	margin-bottom: 20px;
}
@media only screen and (max-width:767px) {
#rsrvwrapper .ratingtext {
	display: block;
}
}
@media (min-width:320px) and (max-width:640px) {
.doctors-details-section {
	padding: 10px !important;
	text-align: center;
}
.videoShow {
	width: 100%;
	height: 100%;
}
}
</style>
<div class="header-banner-content-area light-orange">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-left m-2 text-dark" >
        <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
            <meta itemprop="position" content="1" />
          </li>
          <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/"> <span itemprop="name">Our Team</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
            <meta itemprop="position" content="2" />
          </li>
          <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/dr-kapil-dua/"> <span itemprop="name">Dr Kapil Dua</span></a>
            <meta itemprop="position" content="3" />
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="service-area" style="margin-top: -5px;">
  <div class=" pt-1" style="background-color: #dddfde;
         background-image: url(<?php echo cdn('assets/template/frontend/'); ?>img/dr-kapil-profile-bg.png);
         background-size: cover;
         background-position: 50% 50%;">
    <div class="container">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-6 col-ms-12">
            <div class="row">
              <div class="text-center">
                <div class="btImage text-center"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/dr-kapil-dua-profile.png" class="img-fluid"></div>
              </div>
            </div>
          </div>
          <div class=" col-md-6 col-ms-12 text-left pt5" style="padding-top:3rem !important;">
            <div class="btSuperTitle">Dr. Kapil Dua</div>
            <h1><span class="headline"><b class="animate animated">Every Single Graft Should Grow!!!</b></span></h1>
            <div class="btSubTitle ptb-1">
              <h3>Qualification</h3>
              <strong> <em>
              <p>MBBS, MS ( ENT & HNS), FISHRS, Dlpl. ABHRS</p>
              </em></strong> </div>
            <p>Dr. Kapil Dua, Co-Founder & Chairman at AK Clinics has over fourteen years of clinical and teaching experience in the fields of Otolaryngology & Hair Transplant.</p>
            <p class="pt-3">
              <button class="btn btn-gradient" data-wj-id="">Want to Consult with Dr Kapil Dua?</button>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--<div class="service-area ptb-1">
  <div class="container">
    <ul class="navigation">
      <li><a href="#Recognition-Achievements">Recognition & Achievements</a></li>
      <li><a href="#">About Dr. Kapil Dua</a></li>
      <li><a href="#">News</a></li>
      <li><a href="#">Case Study</a></li>
      <li><a href="#">Video</a></li>
    </ul>
  </div>
</div>-->
<div class="service-area ptb-3" id="Recognition-Achievements">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title">
          <h2 class="text-black text-center">Recognition & Achievements</h2>
        </div>
      </div>
    </div>
    <div class="col-md-12 pt-3">
      <div class="row text-black">
        <div class="col-md-6">
          <ul>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Exclusively practicing hair transplant for over a decade.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Fellow of ISHRS (FISHRS), USA</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Board of Governor, ISHRS, USA (International Society of Hair Restoration Surgery).</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Board of Director, ABHRS (American Board of Hair Restoration Surgery).</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Oral Examiner of ABHRS (American Board of Hair Restoration Surgeons).</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Regular faculty in ISHRS scientific meetings for over 5 years.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Member of Editorial Board, Journal of Cosmetology (JCOS)</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">•	Participating in 2 International Research studies on FUE under the aegis of ISHRS & IRB, USA (One as a principal investigator)</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Delivered more than 90 talks / panels on hair transplant in International and national workshops and conferences</p>
            </li>
          </ul>
        </div>
        <div class="col-md-6">
          <ul>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Diplomate of ABHRS (American Board of Hair Restoration Surgeons).</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Past President  & Founder Member of Association of Hair Restoration Surgeons of India  - AHRS India</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Board of Governor, AAHRS (Asian Association of Hair Restoration Surgeons). </p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Member of FUE Advancement Committee & Membership Committee of ISHRS (International Society of hair Restoration Surgery)</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Member, American Hair Loss Association</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Associate Editor of International Journal of Transplantation & Laser Therapy (IJTLT).</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Participating in 2 International Research studies on FUE under the aegis of ISHRS & IRB, USA (One as a principal investigator)</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Performed live surgery in International conferences & workshops in Manchester, Bangkok, Beijing& India.</p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3">
  <div class="container">
    <div class="col-md-12">
      <div class="tabs">
        <div class="tab">
          <button class="tab-toggle">Teaching Experience</button>
        </div>
        <div class="content">
          <h3 class="m-3">Teaching Experience</h3>
          <span>19+ years in Otolaryngology & Hair Transplant Dayanand Medical College & Hospital </span>
          <ul>
            <li><i class="fa fa-check fa-1x"></i>2000-2011</li>
            <li><i class="fa fa-check fa-1x"></i>2007 onwards:Hair Transplant Surgery</li>
          </ul>
          <strong>AK Clinics</strong>
          <ul>
            <li><i class="fa fa-check fa-1x"></i>2011 onwards: Chairman & Chief Hair Transplant Surgeon</li>
            <li><i class="fa fa-check fa-1x"></i>Sought after faculty in National & International Conferences for FUE Hair Transplant</li>
          </ul>
        </div>
        <div class="tab">
          <button class="tab-toggle">Main Accomplishments</button>
        </div>
        <div class="content">
          <h3 class="m-3">Main Accomplishments</h3>
          <ul>
            <li><i class="fa fa-check fa-1x"></i>Program chair of upcoming World Live Surgery Workshop – Triple Crown in Bangkok, Thailand, 16th – 17th November 2019.</li>
            <li><i class="fa fa-check fa-1x"></i>Delivered the prestigious AHRS India Oration “My evolution as an exclusive hair transplant surgeon” – 9thHaircon, Annual Scientific Meeting of Association  of  Hair  Restoration Surgeons  of  India, Chennai, 16-18th Feb, 2018.</li>
            <li><i class="fa fa-check fa-1x"></i>Organizing Chairperson of 8th Haircon - Annual Scientific Meeting of Association  of  Hair Restoration Surgeons  of  India, Ludhiana (Punjab),  24th to 26th Feb-2017</li>
            <li><i class="fa fa-check fa-1x"></i>Workshop Coordinator for the live surgery in 5th Asian Association meeting in Bangkok, 1-3rd April, 2017.</li>
            <li><i class="fa fa-check fa-1x"></i>Organizing Chairperson of Cadaveric HairTransplant workshop under the aegis of AHRS India,Goa, 15-16 July, 2016</li>
            <li><i class="fa fa-check fa-1x"></i>Jt. live surgery Workshop DirectorOf 7thHaircon, Meeting of Association of Hair Restoration Surgeons of India, Lonavala, 2-3 rd Oct., 2015</li>
            <li><i class="fa fa-check fa-1x"></i>Programdirectoron Asian FUE Hair Transplant meetingof ISHRS, New Delhi,27th Feb.-1st March, 2015.</li>
            <li><i class="fa fa-check fa-1x"></i>Jt. ScientificChairmanof6thHaircon, AHRS India Meeting of Hair Restoration Surgeons, Goa, 19-21 Sept., 2014</li>
            <li><i class="fa fa-check fa-1x"></i>Jt. Workshop Director for Cadaveric dissection, Haircon, 6th AHRS India Meeting of Hair Restoration Surgeons, Goa, 19-21 Sept., 2014</li>
            <li><i class="fa fa-check fa-1x"></i>Jt. ScientificChairman ofJt3rdAsian &5th AHRS India MeetingofHairRestoration Surgeons (Haircon 2013)Nov., 2013</li>
          </ul>
        </div>
        <div class="tab">
          <button class="tab-toggle">Memberships</button>
        </div>
        <div class="content">
          <h3 class="m-3">Memberships</h3>
          <ul>
            <li><i class="fa fa-check fa-1x"></i>Fellow of InternationalSocietyofHair Restoration Surgery – FISHRS, USA</li>
            <li><i class="fa fa-check fa-1x"></i>American Board of Hair Restoration Surgery - ABHRS</li>
            <li><i class="fa fa-check fa-1x"></i>American Hair Loss Association.</li>
            <li><i class="fa fa-check fa-1x"></i>Asian Association of Hair Restoration Surgeons -  AAHRS, Asia</li>
            <li><i class="fa fa-check fa-1x"></i>Association of Hair Restoration Surgeons India -  AHRS </li>
            <li><i class="fa fa-check fa-1x"></i>IndianMedical Association- IMA</li>
            <li><i class="fa fa-check fa-1x"></i>Association ofOtolaryngologistsofIndia - AOI</li>
            <li><i class="fa fa-check fa-1x"></i>All IndiaRhinology Society– AIRS</li>
          </ul>
        </div>
        <div class="tab">
          <button class="tab-toggle">Overseas Fellowship</button>
        </div>
        <div class="content">
          <h3 class="m-3">Overseas Fellowship</h3>
          <span>Hair Transplant surgery under Dr. Alex Ginzburg, Raanaana, Tel Aviv, IsraelinJuly,2007</span> </div>
        <div class="tab">
          <button class="tab-toggle">PositionsHeld</button>
        </div>
        <div class="content">
          <h3 class="m-3">PositionsHeld</h3>
          <ul>
            <li><i class="fa fa-check fa-1x"></i>President ofAssociationofHairRestoration Surgeonsof India, AHRS India- 2016-17</li>
            <li><i class="fa fa-check fa-1x"></i>Board of Governor of International Society of Hair Restoration Surgery, ISHRS 2017-19</li>
            <li><i class="fa fa-check fa-1x"></i>Board ofGovernorofAsianAssociationofHair Restoration Surgeons,AAHRS Asiasince 2014</li>
            <li><i class="fa fa-check fa-1x"></i>Member of Membership Committee of International Society of Hair Restoration Surgery, ISHRS since 2016</li>
            <li><i class="fa fa-check fa-1x"></i>MemberofFUEAdvancementCommitteeofInternationalSocietyofhairRestoration Surgery,ISHRS since 2014</li>
            <li><i class="fa fa-check fa-1x"></i>Financesecretaryof NW ZoneofAssociation ofOtolaryngologistsofIndia2008- Sept.2011</li>
          </ul>
        </div>
        <div class="tab">
          <button class="tab-toggle">Ongoing Research Studies in Hair Transplant</button>
        </div>
        <div class="content">
          <h3 class="m-3">Ongoing Research Studies in Hair Transplant under the aegis of ISHRS & IRB, USA </h3>
          <ul>
            <li><i class="fa fa-check fa-1x"></i>FUE and Strip Graft Harvest Survival Study – Principal Investigator, James Harris, MD, FACS, (USA) </li>
            <li><i class="fa fa-check fa-1x"></i>Comparison of Out of Body time of grafts with overall survival rates using FUE – Principal Investigator, Kapil Dua, MBBS, MS, FISHRS, Dipl. ABHRS (India) </li>
          </ul>
        </div>
        <div class="tab">
          <button class="tab-toggle">International Workshops & Conferences Organized</button>
        </div>
        <div class="content">
          <h3 class="m-3">International Workshops & Conferences Organized</h3>
          <ul>
            <li><i class="fa fa-check fa-1x"></i>Workshopdirectorof Mini Course Powered Blunt Punch System for FUE in24th World Congress Meetingof ISHRS, Las Vegas USA, Oct 2016.</li>
            <li><i class="fa fa-check fa-1x"></i>Workshop director of Asian FUE Hair Transplant meetingof ISHRS, New Delhi,March 2015.</li>
            <li><i class="fa fa-check fa-1x"></i>Jt. ScientificChairman ofjoint3rdAsian &5th AHRS India MeetingofHairRestoration Surgeons(Haircon 2013)Nov., 2013</li>
            <li><i class="fa fa-check fa-1x"></i>Workshopdirectoron EthnicConsiderationsinHair Restoration in21stAnnual Scientificmeetingof ISHRS, San Francisco,Oct., 2013.</li>
          </ul>
        </div>
        <div class="tab">
          <button class="tab-toggle">National Workshops & Conferences Organized</button>
        </div>
        <div class="content">
          <h3 class="m-3">National Workshops & Conferences Organized</h3>
          <ul>
            <li><i class="fa fa-check fa-1x"></i>Workshop Director of Hair Transplant worship in upcoming 10thHaircon in Indore, 8-10th Feb. 2019</li>
            <li><i class="fa fa-check fa-1x"></i>Workshop Director of Hair transplant workshop in EX2018 (Estheticxpress) – International conference on Aestheticsand Anti Aging, 20th-22nd April 2018Hotel Pullman Aerocity New Delhi, INDIA</li>
            <li><i class="fa fa-check fa-1x"></i>Organizing Chairperson of  8thHaircon  meeting of Association of Hair  Restoration Surgeons  of  India, Ludhiana (Punjab),  24th to 26th  Feb-2017.</li>
            <li><i class="fa fa-check fa-1x"></i>Coordinator of Hair Transplant session in Delhi Aesthetics, Aug. 2017</li>
            <li><i class="fa fa-check fa-1x"></i>Coordinator of Hair Transplant session in Delhi Aesthetics, Aug. 2016</li>
            <li><i class="fa fa-check fa-1x"></i>Organizing Chairperson of Cadaveric HairTransplant workshop under the aegis of AHRS India,Goa, July 2016.</li>
            <li><i class="fa fa-check fa-1x"></i>Jt. Workshopdirector of Live Surgery workshop in 7thHaircon, Lonavala, Oct 2015</li>
            <li><i class="fa fa-check fa-1x"></i>Coordinator of Hair Transplant - Video Session in Delhi Aesthetics, Aug. 2015</li>
            <li><i class="fa fa-check fa-1x"></i>Jt. workshop director for Cadaveric dissection, Haircon, 6th AHRS India Meeting of Hair Restoration Surgeons, Goa, 19-21 Sept., 2014</li>
            <li><i class="fa fa-check fa-1x"></i>Jt. ScientificSecretaryof 6thHaircon (ASMofAHRS India),Goa, 19-21 Sept.,2014 </li>
            <li><i class="fa fa-check fa-1x"></i>Workshopdirectoron FUE Hair Transplant heldin AK Clinics,New Delhi,Feb.,2014</li>
            <li><i class="fa fa-check fa-1x"></i>Workshopdirectoron FUE Hair Transplant heldin AK Clinics,New Delhi Dec., 2013.</li>
            <li><i class="fa fa-check fa-1x"></i>Workshopdirectoron FUE Hair Transplant heldin AK ClinicsonSept.,2013.</li>
          </ul>
        </div>
        <div class="tab">
          <button class="tab-toggle">Live Surgery Demonstration</button>
        </div>
        <div class="content">
          <h3 class="m-3">Live Surgery Demonstration</h3>
          <ul>
            <li><i class="fa fa-check fa-1x"></i>Live Surgery Demonstration in 6th Annual Scientific Conference of AAHRS, Beijing, China, 11th-13thMay, 2018</li>
            <li><i class="fa fa-check fa-1x"></i>Live Surgery Demonstration in World Live Surgery Workshop, Dubai,8th-10th March, 2018</li>
            <li><i class="fa fa-check fa-1x"></i>Live Surgery demonstration in 5th Annual Scientific Conference of AAHRS-Bangkok March 29th -April 2nd 2017</li>
            <li><i class="fa fa-check fa-1x"></i>Live Surgery demonstration in Haircon 2017-8th Annual Scientific Conference of AHRS-India,Ludhiana(Punjab)24-26th Feb, 2017</li>
            <li><i class="fa fa-check fa-1x"></i>Live Surgery demonstration in European Hair Transplant workshop MANCHESTER, June 2016</li>
            <li><i class="fa fa-check fa-1x"></i>Live Surgery demonstration in 7th Annual Scientific meeting of Haircon, Lonavala,Oct. 2015.</li>
            <li><i class="fa fa-check fa-1x"></i>Live Surgery demonstration in Asian FUE Hair Transplantmeetingof ISHRS,New Delhi, March 2015.</li>
            <li><i class="fa fa-check fa-1x"></i>Live Surgery demonstration in ISAPS (InternationalSocietyof AestheticPlastics Surgery) meetingheldin Jaipur, 15th Jan 2014.</li>
            <li><i class="fa fa-check fa-1x"></i>Demonstration of FUE hair transplant surgery on cadavers in 6thHaircon, Goa, 2014</li>
            <li><i class="fa fa-check fa-1x"></i>Demonstration of FUE hair transplant surgery on cadavers in 3rdHaircon, Mumbai, 2011</li>
            <li><i class="fa fa-check fa-1x"></i>Live Surgery demonstration in 1stHaircon, Ahmedabad, Dec.2011</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3  bg-light-gray" id="kd">
  <div class="container">
    <div class="col-md-12 text-center">
      <div class="regular slider">
        <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>member-ishrs-min.png" alt="Board of Governors - ISHRS">
          <p class="logo-reco">Board of Governors ISHRS</p>
        </div>
        <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>aahrs.png" alt="Board of Governors - AAHRS">
          <p class="logo-reco">Board of Governors AAHRS</p>
        </div>
        <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>ahrs-india-min.png" alt="AHRAS">
          <p class="logo-reco">Hon. President AHRS India</p>
        </div>
        <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>abhrs-min.png" alt="Diplomate of ABHRS">
          <p class="logo-reco">Diplomate of ABHRS</p>
        </div>
        <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>htnetwork-min.png" alt="Recommended by HTN">
          <p class="logo-reco">Recommended by HTN</p>
        </div>
        <div> <img src="<?= cdn('assets/template/frontend/images/'); ?>realself-topdoctor-min.png" alt="Top Doctor - Realself">
          <p class="logo-reco">Top Doctor Status realself</p>
        </div>
      </div>
      <a class="btn float-right" id="achievements-block">See more</a> </div>
    <div class="col-md-12 achiv-block clr pt-1 text-center">
      <div class="row pt-3 " >
        <div class="col-md-4"> <a href="https://www.hairtransplantnetwork.com/Consult-a-Physician/doctors.asp?DrID=678"> <img alt="Hair Transplant Network" src="<?php echo cdn('assets/template/frontend/'); ?>img/htn.png"/> </a>
          <h4>Hair Transplant Network</h4>
          <p class="new">HTN is an International forum dedicated to share about hair loss remedies that actually work, view over 3680 hair restoration photo albums of actual patients. Hair Transplantation Network helps you to find the prescreened physician nearby and provides you all the educational information related to hair fall or its treatment.</p>
        </div>
        <div class="col-md-4"><a href="https://www.abhrs.com/members/D"> <img alt="American Board of Hair Restoration Surgery (ABHRS)" src="<?php echo cdn('assets/template/frontend/'); ?>img/abhrs.png"/></a>
          <h4>ABHRS</h4>
          <p class="new">The American Board of Hair Restoration Surgery (ABHRS) is a surgical certifying board organized to examine physicians in the area of hair restoration surgery and promote the safe practice of hair restoration surgery. The ABHRS is the only certification recognized by the International Society of Hair Restoration Surgery (ISHRS).</p>
        </div>
        <div class="col-md-4"><a href="https://www.realself.com/find/India/India/Hair-Restoration-Surgeon/Kapil-Dua"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/realself.png" alt="Realself "/></a>
          <h4>RealSelf</h4>
          <p class="new">Realself has created a world’s largest community for learning and also provides a wide range to choose from over 12,000 board-certified doctors. Realself even guide you to locate the top nearby doctor or surgeon and helps you to get your personalized answers or queries from worldwide doctors.</p>
        </div>
        <div class="clr pt-1"></div>
        <div class="col-md-4"> <a href="https://www.ishrs.org/users/drkapildua"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/ishrs.png" alt="American Board of Hair Restoration Surgery (ABHRS)" /></a>
          <h4>Board of Governor at ISHRS, USA </h4>
          <p class="new">The American Board of Hair Restoration Surgery (ABHRS) is a surgical certifying board organized to examine physicians in the area of hair restoration surgery and promote the safe practice of hair restoration surgery. The ABHRS is the only certification recognized by the International Society of Hair Restoration Surgery (ISHRS).</p>
        </div>
        <div class="col-md-4"><a href="https://www.ishrs.org/users/drkapildua"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/fellow-ishrs.png"  alt="Dr Kapil Dua - Elite doctors ISHRS"/></a>
          <h4>Fellow of the ISHRS, USA </h4>
          <p class="new">There are only a selected few hair transplant surgeons who are taken in the premier league and to be a fellow means that you are allowed to use the abbreviation, FISHRS, an honour that is not given to anyone till he has earned it. However, Dr. Kapil Dua has entered this list of the elite doctors.</p>
        </div>
        <div class="col-md-4"> <a href="https://www.ishrs.org/users/drkapildua"> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/hairsite.png" alt="Hair Site"/></a>
          <h4>Hair Site </h4>
          <p class="new">For close to two decades, this has been the ultimate guide for people practising and looking for hair transplants. Being recommended by such a elite portal is definitely a achievement worth sharing. Hair site is serving people since 1997. You can connect up to 20 hair doctors or surgeons worldwide in just one click.</p>
        </div>
        
        <!--single service --> 
        
      </div>
      <a class="btn float-right" id="achievements-block-1">See less</a> </div>
  </div>
</div>
<div class="service-area ptb-3">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title">
          <h2 class="text-black text-center">More About Dr. Kapil Dua</h2>
        </div>
      </div>
    </div>
    <div class="col-md-12 pt-3">
      <div class="row">
        <div class="col-md-6 col-sm-6  pt-1">
          <p style="line-height:30px;">As the Chief Hair Transplant Surgeon at AK Clinics, he has performed 1500+ successful surgeries. He holds the recognition for having transplanted over 3 million grafts, among the highest in the world, with a record of less than 3% wastage, among the best in the world. He is among the key globally recognised FUE surgeons in the world and represents India on the Global Hair Transplant Council at ISHRS, USA. He had served as the Program Director of ISHRS Regional Workshop, the 1st ever ISHRS event in India. Dr. Kapil Dua has primarily practiced FUE (95% practice) from both scalp as well as body. He has performed many giga sessions (combining + FUT + Body Hair Transplant) successfully. He is one of the Best Hair Transplant Surgeon in India and has also treated patients from across the world.</p>
          <!--<blockquote class="doctors-quote">India's Leading Hair Restoration Surgeon. Dr Kapil Dua hosts India's first ISHRS Hair Transplant Workshop</blockquote>--></div>
        <div class="col-md-6 col-sm-6"> 
          <!--<div class="embed-responsive embed-responsive-21by9 videoShow" >
                      <iframe class="embed-responsive-item videoShow" src="https://www.youtube.com/embed/e5VevJbRSa0?rel=0&autohide=0&showinfo=0&controls=0" frameborder="0"></iframe>
                    </div>-->
          <div class="clr"></div>
          <div class="video slider">
            <div class="item embed-responsive embed-responsive-21by9 videoShow" >
              <iframe class="embed-responsive-item videoShow" src="https://www.youtube.com/embed/e5VevJbRSa0?rel=0&autohide=0&showinfo=0&controls=0" frameborder="0"></iframe>
            </div>
            <div class="item embed-responsive embed-responsive-21by9 videoShow" >
              <iframe class="embed-responsive-item videoShow" src="https://www.youtube.com/embed/3XDJRjMyANM?rel=0&autohide=0&showinfo=0&controls=0" frameborder="0"></iframe>
            </div>
            <div class="item embed-responsive embed-responsive-21by9 videoShow" >
              <iframe class="embed-responsive-item videoShow" src="https://www.youtube.com/embed/-ex3u0tB6NI?rel=0&autohide=0&showinfo=0&controls=0" frameborder="0"></iframe>
            </div>
            <div class="item embed-responsive embed-responsive-21by9 videoShow" >
              <iframe class="embed-responsive-item videoShow" src="https://www.youtube.com/embed/Lk0Ah__nKKI?rel=0&autohide=0&showinfo=0&controls=0" frameborder="0"></iframe>
            </div>
            <div class="item embed-responsive embed-responsive-21by9 videoShow" >
              <iframe class="embed-responsive-item videoShow" src="https://www.youtube.com/embed/rKlFP5X5PT4?rel=0&autohide=0&showinfo=0&controls=0" frameborder="0"></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3 ">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title">
          <h2 class="text-black text-center">Publications </h2>
          <p>Total Publications: 40, In hair transplant:23 / International:9, Chapters published:14 / Other subjects:17, Dummy text</p>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="row mb-2">
        <div class="col-md-12">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start"> <strong class="d-inline-block mb-2 text-orange">Hair Transplant</strong>
              <div class="mb-1 text-muted">Chapters: Fourteen(14)</div>
              <p class="card-text mb-auto">Published Chapters - Eleven (11):</p>
              <ul>
                <li>Dua K, Dua A, Chahar M. Dos and Don’ts of Follicular Unit Extraction: Hair Transplantation; 2016:205-212</li>
                <li>Dua K, Dua A, Chahar M. Complications of Follicular Unit Extraction: Hair Transplantation; 2016:288-292</li>
                <li>Dua K, Dua A, Chahar M. Repeat Hair Transplant: Hair Transplantation; 2016:293-302</li>
                <li>Dua K, Dua A, Chahar M. Facial Hair Transplantation: Hair Transplantation; 2016:313-320</li>
                <li>Dua A, Dua K, Kothottil R. Follicular Unit Extraction: Mega and Giga Sessions: Hair Transplantation; 2016:213-217</li>
              </ul>
            </div>
            <img class="card-img-right flex-auto d-none d-md-block" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/book-1.png" alt="" src="<?php echo cdn('assets/template/frontend/'); ?>img/book-1.png"> </div>
        </div>
        <div class="col-md-12">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start"> <strong class="d-inline-block mb-2 text-orange">Hair Transplant</strong>
              <div class="mb-1 text-muted">Chapters: Eleven (11)</div>
              <p class="card-text mb-auto">Published Chapters - Three (3):</p>
              <ul>
                <li>Dua K, Dua A, Chahar M. FUE in South Asians: Practical Aspects of Hair Transplantation in Asians; 2018: 267-279</li>
                <li>Dua K, Dua A, Chahar M. Complications in FUE: Practical Aspects of Hair Transplantation in Asians; 2018: 447-455</li>
                <li>Dua K, Dua A, Chahar M. Body hair transplant: Practical Aspects of Hair Transplantation in Asians; 2018: 589-599.</li>
                <li>Dua A, Desai N, Dua K. Recipient Site planning and Implantation techniques: IADVL Trichology; 2018: ( 44E) 445-455.</li>
                <li>Dua A, Desai N, Dua K. Hair Transplantation in Special groups:IADVL Trichology; 2018: 466-475</li>
                <li>Dua A, Desai N, Dua K. Surgical management in Nonscarring Alopecia: IADVL Trichology; 2018: 413</li>
                
              </ul>
            </div>
            <img class="card-img-right flex-auto d-none d-md-block" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/book-2.png" alt="" src="<?php echo cdn('assets/template/frontend/'); ?>img/book-2.png"> </div>
        </div>
        
        
      </div>
    </div>
    <div class="col-md-12">
    <ol>
                  <strong>To be published - Chapters :Three ( 3)</strong> <em>Unger’s Textbook- HAIR TRANSPLANTATION 6TH EDITION</em>
                  <li>Transplanting Requiring Special Consideration: Moustache and Beard</li>
                  <li>FUE Techniques: Motorized with Blunt </li>
                  <li>FUE Special Situations: FUE from Beard</li>
                </ol>
    
    </div>
  </div>
</div>
<div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?= cdn('assets/template/frontend/img/'); ?>dr-profile-cover.jpg" style="background-image: url(&quot;<?= cdn('assets/template/frontend/img/'); ?>dr-profile-cover.jpg&quot;); ">
  <div class="container pt-0 pb-0">
    <div class="section-content">
      <div class="row">
        <div class="col-md-8 sm-height-auto">
          <div class="p-30 bg-deep-transparent " style="height:500px; overflow-y:scroll;">
            <h4 class="title mt-0 line-bottom line-height-2 text-black-222">Participation as Faculty in <br />
              <span class="text-theme-colored">Speaker/ Panelist/ Moderator role International</span></h4>
            <ul>
              <li><i class="fa fa-check fa-1x"></i>1.	Participated as a Faculty and moderated a session on Selection of Interesting topics in General Session of 26th Annual Meeting of ISHRS in Los Angeles, 9-13th Oct., 2018.</li>
              <li><i class="fa fa-check fa-1x"></i>2.	Participated as a Faculty and presented a paper on Follicular Transection Rates in Revision Hair Transplantation in General Session of 26th Annual Meeting of ISHRS in Los Angeles, 9-13th Oct., 2018.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Faculty and presented a video on Modified Non Shaven FUE in General Session of 26th Annual Meeting of ISHRS in Los Angeles, 9-13th Oct., 2018.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Table Leader on How to start FUE in 26th Annual Meeting of ISHRS in Prague, 9-13th Oct., 2017.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and presented a talk on Tips & tricks in FUE at the 10th Annual Hair Transplant 360 Workshop, Saint Louis University, USA, August 3‐5, 2018</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and presented a paper on tips And Pearls in FUE in the 6th Annual Scientific Conference, AAHRS, Beijing, 11th-13thMay, 2018.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and presented a video on Non Shaven FUE in the 6th Annual Scientific Conference, AAHRS, Beijing, 11th-13th May, 2018.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and moderated a session on Body hair Transplantation in the 6th Annual Scientific Conference, AAHRS, Beijing, 11th-13thMay, 2018.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and presented a paper on Donor Area complications in the World Live Surgery Workshop, Dubai, 8th -10th March, 2018 </li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and presented a paper on Patient selection & Limitations of FUE in the World Live Surgery Workshop, Dubai, 8th -10th March, 2018 </li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and moderated a session on Special & Advanced cases of FUE in the World Live Surgery Workshop, Dubai, 8th -10th March, 2018</li>
              <li><i class="fa fa-check fa-1x"></i>Panelist on Difficult cases in hair Transplant in General Session of 25th Annual Meeting of ISHRS in Prague, 4-7th Oct., 2017.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Table Leader on How to start FUE in 25th Annual Meeting of ISHRS in Prague, 4-7th Oct., 2017.</li>
              <li><i class="fa fa-check fa-1x"></i>Presented a video on “ Body hair Transplant” in the FUE Immersion - ISHRS workshop in PolanicaDorj, Sept 30-Oct.2, 2017</li>
              <li><i class="fa fa-check fa-1x"></i>Delivered a talk on “Do transected hair grow” in the FUE Immersion - ISHRS workshop in PolanicaDorj, Sept 30-Oct.2, 2017</li>
              <li><i class="fa fa-check fa-1x"></i>Delivered a talk on “Treatment of overharvested areas” in the FUE Immersion - ISHRS workshop in PolanicaDorj, Sept 30-Oct.2, 2017</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Speaker at the “Ist Annual Meeting of Taiwan Society of Hair       Restoration surgery“in June 4, 2017, TAIPEI, TAIWAN and delivered talk on Tips & tricks in FUE.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Faculty Member in WCOCD 2017-12th World Congress of Cosmetic Dermatology & delivered talk on “Body Hair Transplant” in May, 2017 at Bengaluru.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated in Open House Panel discussion in WCOCD 2017-12th World Congress of Cosmetic Dermatology in May, 2017 at Bengaluru.</li>
              <li><i class="fa fa-check fa-1x"></i>Delivered talk on “ FUE in South Asians and Beard Transplantation” in 5th Annual Scientific Conference AAHRS, Bangkok, March 2017</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Moderator in 5th Annual Scientific Conference, “Body Hair Transplantation and Intra operative Awareness & Risk in Hair Transplantation” AAHRS, Bangkok, March 2017</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Panelist in 5th Annual Scientific Conference, “Recipient Side Creation and Inadequate Growth” AAHRS, Bangkok, March 2017</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Table leader in 5th Annual Scientific Conference, “ Body Hair Transplantation” AAHRS, Bangkok, March 2017</li>
              <li><i class="fa fa-check fa-1x"></i>Participated in Panel Discussion in 24th Annual World Congress,“ Ethical issues in FUE delegation” ISHRS, Las Vegas, Oct. 2016</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Workshop Director and delivered a talk in FUE Mini Course in 24th Annual World Congress in the Dull Punch Course “Combining blunt punch FUE with other techniques- How I do it” held in Las Vegas, Sept. 28-Oct.1, 2016</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Workshop Director and delivered a talk in FUE Mini Course in 24th Annual World Congress in the Dull Punch Course “Ergonomics in FUE Hair Transplant” held in Las Vegas, Sept. 28-Oct.1, 2016</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Faculty in European Hair Transplant Workshopand delivered talk on “Body Hair as FUE donor”, Manchester, UK in June2016 .</li>
              <li><i class="fa fa-check fa-1x"></i>Faculty on Hair Transplant and delivered talk on Tips & Tricks in FUEin ISAPS(InternationalSocietyof AestheticPlastics Surgery) meetingheldin Agra,Feb., 2016.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Course faculty in FUEHands- On Course: UtilizingSAFESystem forFUE- and delivered a talk on Powered Blunt Punch System for FUE in 23rdAnnual Scientific Meetingof ISHRS,Chicago, Sept.,2015. </li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Workshop Faculty on Beard & Moustache Reconstruction in 23rdAnnual Scientific Meetingof ISHRS,Chicago, Sept., 2015.</li>
              <li><i class="fa fa-check fa-1x"></i>VideodemonstrationofExtractionofBodyHairbyFUEin4thASMofAAHRS Asia,Mar.2015</li>
              <li><i class="fa fa-check fa-1x"></i>InvitedtalkonFUEinSouth Asiansin4thASMofAAHRS Asia,Mar.2015</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Workshop Director and delivered talk on MyExtractiontechnique(howIdoit?)inISHRSAsianFUEHairTransplantWorkshop,New Delhi, 27Feb-1stMar.,2015</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Workshop Director and delivered talk on FacialhairTransplantinISHRSAsianFUEHairTransplantWorkshop,inNewDelhi,27Feb-1stMar.,2015</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Workshop Director and table leader in Breakfast with Experts session“Body as a Recipient Area” ISHRS Asian FUE Hair Transplant Workshop, inNewDelhi, 27Feb-1stMar.,2015</li>
              <li><i class="fa fa-check fa-1x"></i>Course faculty in FUEHands- On Course: UtilizingSAFESystem forFUE- and delivered talk on How I do it in 22ndAnnual Scientific Meetingof ISHRS,Kuala lumpur Oct., 2014.</li>
              <li><i class="fa fa-check fa-1x"></i>Course faculty for BasicsCourse and table leader on FUE Tools selection in22ndAnnualScientific Meetingof ISHRS, Kuala lumpur, Oct. 2014</li>
              <li><i class="fa fa-check fa-1x"></i>Course Faculty in LiveSurgeryWorkshopin 3rdInternationalcongressofTrichology &HairIndia 2014 atTheLeela, Chennai, India, August 2014</li>
              <li><i class="fa fa-check fa-1x"></i>Faculty in General session and delivered talk on Revision hair Transplant in 3rdInternationalcongressofTrichology &HairIndia 2014 atTheLeela, Chennai, India, August 2014</li>
              <li><i class="fa fa-check fa-1x"></i>Panelistin General session on Hair Transplant in 3rdInternationalcongressofTrichology &HairIndia 2014 atTheLeela, Chennai, India, August 2014</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as faculty and delivered talk on onFUE Hair transplant (Harvesting indication,Instrumentation,technique, in ISAPS(InternationalSocietyof AestheticPlastics Surgery) meetingheldin Jaipur, 15th Jan 2014.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as faculty and delivered talk on “Limitations in FUE” inJt3rdAsian &5thIndian Association of Hair Restoration Surgeons Meeting, Bangalore, Nov2013</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as faculty and delivered talk on“Implantation-Theavailableapproaches”3rdAsian&5thIndianAssociationofHairRestoration Surgeons Meeting, Bangalore,Nov2013</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as faculty and delivered talk on“MotorizedFUE”–AdvanceCourse-Jt3rdAsian&5thIndianAssociationofHairRestoration Surgeons Meeting, Bangalore, Nov2013</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as faculty and delivered talk on“Selecting your punch for FUE” inJt3rdAsian &5thIndian Association of Hair Restoration Surgeons Meeting, Bangalore, Nov2013</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as faculty and delivered talk on“ Forwhomdoweoperate – ourselves or patients”Jt3rdAsian&5thIndianAssociationofHairRestoration Surgeons Meeting, Bangalore, Nov2013</li>
              <li><i class="fa fa-check fa-1x"></i>Coursefaculty ad presented a talk on How I do Motorized FUE inFUEHands-OnCourse:UtilizingSAFESystemforFUE in 21stAnnualScientific Meetingof ISHRS,San Francisco Oct., 2013.</li>
              <li><i class="fa fa-check fa-1x"></i>Facultyin PanelDiscussion on Hairline Discussion in 21stASMofISHRS, San Francisco, Oct. 2013.</li>
              <li><i class="fa fa-check fa-1x"></i>Presented paper on PoweredFUE:Ananalysisof237patients-19thAnnualMeetingoftheInternationalSociety ofHair Restoration Surgery, 2011, Anchorage, Alaska</li>
              <li><i class="fa fa-check fa-1x"></i>Video demonstration of FUE in 1stAnnual Meeting of Asian Hair Restoration Surgeons, Bangkok, 2011</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
  <div class="container">
    <div class="col-md-12 fun-facts">
      <div class="row">
        <div class="col-md-3 fun-col"> <strong class="big-font">2000</strong>
          <p> Happy Patients</p>
        </div>
        <div class="col-md-3 fun-col"> <strong class="big-font">5 Million</strong>
          <p>Grafts transplanted</p>
        </div>
        <div class="col-md-3 fun-col"> <strong class="big-font">3% or Less</strong>
          <p>Controlled FTR</p>
        </div>
        <div class="col-md-3 fun-col"> <strong class="big-font">18-75</strong>
          <p>Age Group</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?= cdn('assets/template/frontend/img/'); ?>dr-profile-cover.jpg" style="background-image: url(&quot;<?= cdn('assets/template/frontend/img/'); ?>dr-profile-cover.jpg&quot;); ">
  <div class="container pt-0 pb-0">
    <div class="section-content">
      <div class="row">
        <div class="col-md-8 offset-md-4 sm-height-auto">
          <div class="p-30 bg-deep-transparent " style="height:500px; overflow-y:scroll;">
            <h4 class="title mt-0 line-bottom line-height-2 text-black-222">Participation as Faculty in <br />
              <span class="text-theme-colored">Speaker/ Panelist/ Moderator role National</span></h4>
            <ul>
              <li><i class="fa fa-check fa-1x"></i>Participated as faculty and had a video presentation on Body to scalp – evolving trend in HT, 23-24th June, 2018</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered a talk on Available options for learning Hair Transplant in India & Abroad, Rajkot, 23-24th June, 2018</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty & delivered talk on Tips & Tricks in FUE,  EX 2018 (Estheticxpress) 20th-22nd April 2018 New Delhi</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as faculty and delivered a talk on Extraction of Body hair grafts by Follicular Unit Extraction technique, Haircon 2018-9th Annual Scientific Conference of AHRS-India, Chennai, 16-18th Feb 2018.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as faculty and delivered a talk on Manual to Motor Devices, Haircon 2018-9th Annual Scientific Conference of AHRS-India, Chennai, 16-18th Feb 2018</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as faculty and delivered a talk on Follicular Transection in FUE, Haircon 2018-9th Annual Scientific Conference of AHRS-India, Chennai, 16-18th Feb 2018.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as faculty and delivered AHRS Oration a talk on My evolution as an exclusive hair transplant surgeon, Haircon 2018-9th Annual Scientific Conference of AHRS-India, Chennai, 16-17th Feb 2018.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a faculty and delivered talk on “ Advances in Surgical techniques” in Delhi Aesthetics, Aug. 2017 in India Habitat Centre, New Delhi </li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Facultyand delivered talk on Body Hair Extraction techniques in Haircon 2017-8th Annual Scientific Conference of AHRS-India,Ludhiana(Punjab), Feb 2017</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Faculty and delivered talk FLAT PUNCH in Haircon 2017-8th Annual Scientific Conference of AHRS-India, Ludhiana (Punjab), Feb 2017</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Faculty and moderated a panel discussion on Recipient Area Planning & Slit makingin Haircon 2017-8th Annual Scientific Conference of AHRS-India, Ludhiana (Punjab), Feb 2017</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a faculty and delivered talk on “ Safe Donor Area” in Delhi Aesthetics, Aug. 2016 in India Habitat Centre, New Delhi</li>
              <li><i class="fa fa-check fa-1x"></i> Participated as a faculty and delivered talk on “ Ergonomics in hair Transplantation” in Delhi Aesthetics , Aug.2016 in India Habitat Centre, New Delhi</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a faculty and moderated session on “ Slit making” in Delhi Aesthetics, Aug. 2016 in India Habitat Centre, New Delhi</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty in AHRS Hair Transplant Cadaveric dissectionworkshop and delivered talk on FUE donor harvest manual and motorized held in Goa, July, 16-17, 2016</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty in AHRS Hair Transplant Cadaveric dissection workshop  and delivered talk on Non Scalp donor harvest motorizedheld in Goa, July, 16-17, 2016</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Faculty member and delivered talk on Facial Hair Transplant- The latest fad in 7thHaircon 2015 at Lonavala,Oct.2015</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Faculty member and delivered talk on “ Does Body hair work” in 7thHaircon 2015 at Lonavala,Oct.2015</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Faculty member and delivered talk on “Controversies and current scenario in hair transplantation” in 7thHaircon 2015 at Lonavala,Oct.2015</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a Faculty member and was a table leader on Complications in 7thHaircon 2015 at Lonavala,Oct.2015</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a faculty and had a video demonstration on “ How to extract grafts from Non Scalp Donor Areas” in Delhi Aesthetics 2015 in India Habitat Centre, New Delhi.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a faculty and had a video demonstration on “ Different ways of preparing donor area of FUE”  in Delhi Aesthetics 2015 in India Habitat Centre, New Delhi.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as a faculty and delivered talk on “ ENT &Hair Transplant Surgeryin 5thAlPCS,Amritsar, Punjab, April,2015.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “IS FTR:An- Overrated parametersin FUE” in6thHaircon 2014 atMarriotResort& Spa, Goa, India, September2014</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “Mega Session ofFUE inAdvanced Hair Loss”in6thHaircon 2014 atMarriotResort& Spa, Goa,India, September2014</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and panelist on “ Live patient discussion on assessment and planning of different patients”   in6thHaircon 2014 atMarriotResort& Spa, Goa, India, September2014</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and conducted Masterclass on FUE HairTransplantat Delhi Aesthetics2014atIndia HabitatCentre, Lodhi Road, NewDelhi, India fromAugust2014.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “ When allscalphairareexhausted”in Delhi Aesthetics2014atIndia Habitat Centre, LodhiRoad, NewDelhi, India,August2014.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “Failed Hair Transplant”in Delhi Aesthetics2014atIndia HabitatCentre, Lodhi Road, New Delhi, India from August2014.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and conducted Masterclasson FUE Hair Transplantin Daassummit2014atHotelEros,NehruPlace, NewDelhi, India from July2014.</li>
              <li><i class="fa fa-check fa-1x"></i> Participated as Faculty and conducted Masterclass on FUE hair Transplant in Delhi Aesthetics, Aug 2013.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on complications of hair transplant in Delhi Aesthetics, Aug 2013.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Guest Faculty and delivered lectureon“FUE Hair Transplant”inGovernment MedicalCollege, Jammu(J&K)inRhinoplasty& Cosmetic Surgery Work shop in June2013</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Facultyand delivered talk on “FUE Giga session”in4thHaircon heldat HotelGrand Park, Kolkata,Aug.,2012.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “Motorized FUE”in4thHaircon heldat HotelGrand Park, Kolkata,Aug.,2012.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “Sharp v/s blunt punches” in4thHaircon heldat HotelGrand Park, Kolkata,Aug.,2012.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and presented study on “FUE evaluation in patients with previous hair transplant”in 3rdHaircon 2011 atMumbai, November2011</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and showed video on “FUE Hair Transplant-How wedoIt” in in 3rdHaircon 2011 atMumbai, November 2011 </li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “Hair Transplant in Traction Alopecia”  in3rdHaircon 2011 atMumbai, November2011 </li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “FUE & Strip combine”  in3rdHaircon 2011 atMumbai, November2011 </li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “Limitations in FUE”  in3rdHaircon 2011 atMumbai, November2011 </li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Guest Speaker and delivered a talk on “ FUE Hair Transplant” in IMA, Malerkotla, Punjabin 2011.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Guest Speaker and delivered a talk on “ How we do FUE Hair Transplant” in Dermatology Forum, Patiala, Punjab,  2011</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “ Comparisonof0.8mmand0.9mmpunches inFUE”in 2ndHaircon2011at MountAbu, Sept., 2010</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “Powered FUE” in 2ndHaircon2010at MountAbu, Sept., 2010</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and paneliston“ComplicationsofHT” in2ndAnnualmeetingofHairrestorationSurgeons,Haircon, Mount Abu, Sept, 2010</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Guest Speaker and delivered a talk on “ FUE Hair Transplant “ – The AK Technique in Jagraon, Punjab, 2010</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “FUE –OurExperience”in 1stHaircon 2011at Ahmedabad, Dec, 2009.</li>
              <li><i class="fa fa-check fa-1x"></i>Participated as Faculty and delivered talk on “Internet and Hair Transplant”in 1stHaircon, 2011at Ahmedabad, Dec, 2009.</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3" id="Recognition-Achievements">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title">
          <h2 class="text-black text-center">Conferences and Workshops Attended as Delegate</h2>
        </div>
      </div>
    </div>
    <div class="col-md-12 pt-3">
      <div class="row text-black">
        <div class="col-md-6">
          <ul>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">SeptoRhinoplastyandParticipatedinContinuingMedicalEducationheldonSept,2009</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Participated in CME-1organized byAOIin Patiala heldon Dec 2008.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Attended the 3rd National Workshop on Basic & Advanced Micro Ear & Skull Base Surgery held in Jaipur on April 2008.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Participated in Endoscopic Skull base Workshop held in Mumbai in Bombay Hospital in August2007.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Attended a Gues Lecture by Mr. Martin Porter held on Feb,2007.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Delegate in 29th All India Rhinoplasty Course held in Delhi on Nov.,2006.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Attended the lecture on Otology and Skull Base Surgeon held in CMC, Ldh. on Oct 2006.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Participated in 39th Annual National Conference of Indian Medical Association held in Ludhiana on Oct., 2005.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Delegate in ICMR workshop on Scientific Paper Writing & Paper Presentation held in DMC&H March, 2004.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Attended the CME Programme and Live Surgical Demonstration on FESS in Chennai July, 2003.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Micro surgery of the Ear workshop Cum Updatein Otology in PGIMER,Chandigarh on Sept, 2002</p>
            </li>
          </ul>
        </div>
        <div class="col-md-6">
          <ul>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Participated in Workshop on Medical Tourism-Opportunities and Challenges in India organized by ICRI Health held on Feb 2009.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Participated in 8th Biennial Conference of the Association of Cutaneous Surgeons of India held on Nov, 2008.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Attended the 31st North West Association of Otolaryngologists of India Conference held in Amritsar on Sept 2007.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Participated in Workshop on Micro Surgery of the Ear held on 31st March-1st April 2007.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Attended a CME on Tuberculos is arranged by District TB Control Society Ludhiana held on Feb., 2007.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Attended th 12th Annual Conference of A.O.I Haryana State and 4th Live Surgery Workshop held on November, 2006.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Attended lectures on‘Endoscopic Sinus Surgery’held in CMC,Ludhianaon Nov.,2005.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Participated in Capacity Building workshop on HIV/AIDS held in DMC&H on March,2005.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Attended the 12th Functional Endoscopic Sinus Surgery Workshop in Mumbai,Aug 2003.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Attended the 26th North West Zone AOI Conference in GMC,Chandigarh on Oct., 2002.</p>
            </li>
            <li class="pb-1 text-black"><i class="icofont-hat-alt float-left" ></i>
              <p style="">Nasal Sinus Endoscopy Course at the Jaipur Golden Hospital in Delhi on Dec,2000.</p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3 ">
  <div class="container">
    <div class="col-md-12">
      <div class="section-title">
        <h2 class="text-black text-center">Moments</h2>
      </div>
    </div>
    <div class="card-columns pt-3">
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/3.jpg" /> </div>
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/1.jpg" /> </div>
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/2.jpg" /> </div>
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/33.jpg" /> </div>
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/11.jpg" /> </div>
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/22.jpg" /> </div>
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/44.jpg" /> </div>
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/55.jpg" /> </div>
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/66.jpg" /> </div>
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/77.jpg" /> </div>
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/88.jpg" /> </div>
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/99.jpg" /> </div>
      <div class="card"> <img class="card-img" src="<?php echo cdn('assets/template/frontend/'); ?>img/111.jpg" /> </div>
    </div>
  </div>
</div>
<!--<div class="service-area ptb-3 bg-light-gray">
  <div class="container">
    <div class="col-md-12">
      <div class="row clearfix"> 
        
     
        <div class="col-lg-9 col-md-12- col-sm-12">
          <div class=""> <strong style="font-size:25px;padding-top: .5rem;">Suffering from Hair Loss or Planning for hair Transplant?</strong> </div>
        </div>
        
     
        <div class="button-column col-lg-3 col-md-12- col-sm-12">
          <div class=""> <a class="btn btn-gradient" href="#" data-target="#myModal" data-toggle="modal">Get Free Consultation</a> </div>
        </div>
      </div>
    </div>
  </div>
</div>-->

<div class="service-area ptb-3">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title">
          <h2 class="text-black text-center">What Patient says about Dr Kapil Dua?</h2>
        </div>
      </div>
    </div>
    <div class="col-md-12 pt-3">
      <div class="row">
        <div id="realself-15727-rv">
          <div id="rsrvwrapper">
            <div class="rsrvheader"><a href="//www.realself.com" target="_blank" rel="nofollow"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAAAuCAYAAABtRVYBAAAAAXNSR0IArs4c6QAAD3JJREFUeAHtnAmQlMUVgKcXRVwEE8IhXigiWlpKonhEDeBJhKAJYGI8UFPxNmooowhRNxElxNIqNWqVMSLReBDjBeWJFxpNpLzwiFY8iJIFlgAiEZRlt/O93pnxn5m/j3+unTXzqt7+/fd7/d7r1/3+v/v9PatSdai4B7TWg1CyrUPRQqXUBgf9K0vCN1vQuWGODn6Ab5Y66LEk5Iq/jwAPArcC+4PdwLXgf8A3wDnIfotrHTrTAwzWDK3budjQDGZnmthpunHKcLtfxF/63CTGwT8KfMotMzMO+hif7E18DHV63QNdwQMERW/snJVK6fEJ7H3dx1sPEJ+H6vSa9wDBsQNGPkZwDA03Vq2H9z0ff4OPoU6ve6CWPUBw9MS+B5MFh+nRm+w/2n19qweIz0N1eq174CqCY88ijFwU0qYeICFeqvPUpAd4ewzAsJ+EG6c+T6UUGSzz5ggKkPoeJNy7dc7a88CpvD02c5ulPoZ+EbgQlJSxpLB6UJaUrxfqAeJ1UZ2hhj0wwm2bmgt9EkHxSZSPe94kYVAPkDA/1blqzAO8BWR7sK/dLPVfaCcSDGvsPH5KfQ/i91GdozY90I/l1ZYO0x4tNThEdj1AHB6uk2raA67gEMPfLIf19QAphxfrMjrDA74AWV0Oo4reg6RTbK28xlaVYghyFO0lUCWrINc2sB25cq0KYMOmKBoISkZkBbgG/ZprTQN2y/j1SaOsuVuwu6KHHtM6Jb3aCDaj7zOunQGbe5TKl/KSYRM6fJ1DipwyvT1DTzvnYu5PZ/23jdRTt5jLteB18Hq/TMInbVg/psaDE8FR1BQEKjxy4vJ+cA74VKhseL2A7F4wjQEngCNBWc9KoKZBfQbP89w8Cc5Gd4sQqJM+S8rQBtMzvDaGUuuxYQgyTgEPBvfGqu5RmdBXcv8SuAB8HHteidKTlpEnE3E0+APwMJCTsWaDTNH4RDbBfwfvBe9Hn4xbLcCx2L6Hx5BHsPcRJ4/71KPOCQ4UPmfn1w9Dj0yyQrXQe4CXgOvscjInLaNXvZA2+xVKTFaDjM3AC8E14fr15/DfDPYFh7nbxZ8Fol3Jp3mRsSP4IIhjor7xlfXrtDkLzAkkn+fg7waeAbaE69Pr4Z8OyvGPIIC3qNO8tBsRbpfNR7rJZ6QsaUJhCs8LOVtvAX0khHMtRHn6fgvaW8j4Neh7PeaJ0cOpeBEZV4POIMxrmL2l3T7cvI3umWDvLMFbkA9Rmg9SZtP3PS97BRiw/XjEiu+OAhP2X45h6BvEfuT4nqjGevh2pvAq7W4C+5nKoD/yAU5Pg/VdZBwQ1KTGmZIEyDkBfflZHA/OkmXMMzhvcBw9rE4mhp4M753Ikz1DMMDPxCpZ/wD0Tw9WWiZGbD8FUbzJkz5UCgxopOajgtq8CvR9m6oX0BcUTHnN07dmKTofWUfH07tObVCA0NGtcRgTxAuD4d0yysX9/tw/SvsET+2ohPyyPpYazv2HQXqQ2MtomSBdCrCdp3/qRmxP+NaI7eaZrLdlv2AF9Mlbnn2X7mtlCiaYgP4LMmX/0mUhKEDonWSYAsAMZJYX50hQ3InD5exLGUEfj2x5sjoBHtnQzkZ/aD+d8jqB2FQe36m7CQ45dmEFfCUPNjbaoW+qkGyZlrlwB7IludElYZMQq3Hux3SS7ITvyaI+hHdVRCYZMr1j5D6mqBZReTP4GvgxuDt4IHg+bV2bvd9h0xPoWwJvAUCToJiDjJw3WgFjtsJk4JZyK4Mqy6lyPLWz0pMW0pPqaHc79U/ofwTfAdeCvOlTsl/7PvZLGTBZpXM7ys6/7FNcS2Djn1uR8CdwkYwzNoqOY8Bf2f0sc0bJG/8IsIqglqPM+caEzpz2gDsTkJPFutzNK5kC/YuMOspDwTZ7G90K/Tww+8bJtJUr9YPA5+3tjb5ro22iZdoe526byWyY7NtY+NmMdwDlnuBE0JG1y7SPXsuXxUL3GLf9ei483TM2R6/US/p+EigPtuOjtLgyPLuBdCTal2hZvw/9m3FtpQ7aNqDHV/o7ce1pV6EslkmsxKlMVmd3ijgoJ0C6c/83O7+WTVl2KUP593ZeI/ssn6XI+AbIIEcHK1qWdHFhloW6buA79nZGv7SVp58T4DkT3OiWlbGprAFyjlunHuU0HCJ2B+274LvLrkuvhL5LgK7+8C11yHkiTgZtajpAshM6zvhoHa/UDdzzFFC/Bpd9SZMljrqQ+9HwmA+FdFoG5sQvefJLai68bD7dAM9KOBzLA7Nenhgj5WCmh2NQzVf6ccj/c0zbnCp4SHWmJuVUVuem1aNmXw89he3rfDyMlewT43yYaXoRct7N3Niu8LRAu8BGZzwOQ5ecVuhSEBwg0iucIEdLLgPpqNoG7E95O/AqsC3S8wNwSHbJEqnPFGdnCgFXMmDmB/Y21sNjCPJNxgUzsfdJF0OUBi+JBnVbtK4K5cUeHXx8NB/lvu7h85EPZawse1H1BY29D5GIgnn4aWPkPr84Or+i1u8tjvGbzaRpdnCNctCEdDGDK980QkE7GA9BFubknJ1yBIj6FFlXOeTZSGxE1Qn2yWRrVnT9s+hbjT5LAJjl7DR4zqf/ZJ9Sd4Pz8YNrgsYZ49o8f0aDG5Ef185Wtx5CLwtRdN1modVkddEB4unN7m665gxRuUCyVKo70uRpJ+tu+Yi4q5Qt8DCT6BMLzVpNm8XIfgGGEVamMhLQJ0dcyAKmLnOLNZm+k+ABFcdCtATKHbRf6G6XpTrGSveB67gsZ+mFnUoXUV0JiZZYCUzrl4C3HKwSIBnwpWgXZBiLuJbStgh1qelM+gQ6dX8eEezZ9EsSzCBLXS/QpmrA2HQt+IoGiHMQmp1UN7GUtm7JMVTeArJcYrmoZAmVEOTIiP4rQXIvuL2jcTUnbTWD0dHlcFKlAkSFm1AWTpM9S0uK/a4S0RLljVQHFUtpG6Qgn4kgWQeSjlYkI9Rr+XT/vZ4AD+l56/+OqtQciDPt87jKWq6r1B5EUn472zues6G2s4VTJAWdAdHtAjJwRUMpbYtWKg0JEvnOxN5NgiX1U/AQ3hCBk1vSq0o+LO6JnPyvy/LFWVK9MWC+wssX+nLBv8olqFpyKhkglj6oVxmkvSzEclT7AuRAlNxcpCJp22mA3+QNdo8gk31bJr18a5LM2m5+o2SZpS6HL/+7kgSI7WF2Izo9SQK/5q7MEfgEStzFxY4WuzO4tjSgo1kYiQHl45j6h4N7HPq3cNBjSbThu091MlixBuRV0s8l4AyQLJQaDpK9yvkWldfC3J5AP/K/T70Xx5iuYx/z/w2VCpD5drea80M89SoKLv3yXWFyEdov4UkdzZYVIaIyTQiSl8EfI51llzl9YFFkvqkQTDnwWM5d7o18Y9o2t+qrd0cf1Yorpg6Ua37vKhUg8pHri3xlkfsrMGZw5D6oSJs+4HYBzA97eORnvyM8PFkyvBMJjtOzFTVaIEgWYNqVHvPy/fe4/c1jDpLeQv8TzxPa9Cqmncf2spJbmqYMaZ42Zd7SX168ZsO69mauq/89bcpDy5umZOdm4o6HWMhAyRfYuXZe8yS7DwdubufJpcA7iJrnwGcps552Ak9F1zLLHK14BDnjnVIgwnMql7t8fJWio/9K8CawMVDH+x6+nlE6Y7WKe8cDxfzgKdE+BFvlrSPBOotyReZYtA/FlJddOvXQjRvV6zqVGguaJT9X+WnEuLbW1CKhi9xKGo9TXWtiPQz9kn4cKYbYALoc3T4b+hsdm1Hz+5JnqLMGCYNOX1O/tcnsqJcJp+UXb/PAI8Hs8olyIzgB5E2o2dDbziq5NZRKRf9+yLgQ/WdwfYt7TvjaAwVaD/h+7tG7JIZ+KWMlPrOBvHGvBy3HXjqaQW8AT+ROxorj8XoS5VlSbxPcGfUrZs7s1d7efit2xT50cETPtnY9a2VTU+9KZbEkLfk2BtyOA062O8Hk5mWyy4cweUq/AsoJ3v7gYPAQ8GQcvRXXCEiQKGk3Cj0fRQjR4h3wsCwyP/mN1ueV9VgqQMVvV/SHlLuBg2jXqYOKLfJ2nY0dYg+gd+DP9dgphxQJ3NTT4GJQslAy0HuA0l9bRgqy+fAoPs4BfPgaMudQ+aMcQvbGrM3Pof2x8MkSTk5jvydk7iXhMRQcCcKTv3SWIFHCdwpt2uHpdNj46SfjdSq1vdMQlvKtbRsmVixA0srZDCtSo65BE05Z4zuPXKfFRS/uIGEw+A2H/FhIPq6FZM3MRBwS1dDJ5Suwe5dCG8yETAd1IdVTMw+/rLDwnImv2MDrnSx0qs0vSq+hcA2+beW6lro+dv4MpbaChODYK2OZ68oTc6+KPiUZDE6jpsbh+MSHA12GR2jylJV1Yyyg/wMIJ6DflTCIbduZlUy+Eeg/r7w2mJ8MTLHJTI/VUfiKSR8Ccig0JDiysvpSyi5js7WdUjCHW72aFSnxigaIWIDj3+VyBKUlXosSMZjJfxDyWe/aAfpDUMeED7xdVhUpB6CLB125wCxtTk+PhVUo9LchjsJXH1mZiiKY/855NPJr46iJ0gXLzNhuNaRerniAiGIcs5DL3pSejjUkcaX5JeCByPVlbIxk+J6iMBL9YY6JtUd+R6JmxJLKXIm9v0Hkvuh7qXTR5leFxyDz9hBZ8ImPWGqp+SH8bh75JqPYl6ROQu5GN2/1qKox9YBSvn/YoFY2dOtxX1UCRLqOg1q4HEpJDt45n/p2V8mEUfLW+CG4zM5XSIH/VWr3of1poNiSANQ8mIeB9yRoVBIr9spE3R9bjwTngu3JBEpWyjxIdkXWfUnawr8CPDytWx5uCcGcZriaRkOQcwNYxrdhQlNi2AdOncE+TMkbNTZopZ4AOm2rpqYWlln6DzEyMlUvwnxL5qZcV3QqZI0Gx3ZcXZt4c4L1AfgexBY23KUD+lk/mwzZRK7fBbdmRZP3sDDfUeQpegt6F3GVTMx2XJqkbIGp8C7Pp9FuAnVj8usj9xfQbnXkvqCIjO2pJFhMtog9Stz/mjL7jJfhEbtnI3Mx15IB3bzNjG7xFRvcL1PiHcJNOr+Zsryp7wfln2av76C5/yJ7BzgucXDdiawn8+m0G0rdRfn1kXvp/4LIfUGxuWna8FRr+w0k2fjHEbqBSdlGJC9UmzacvXXTlWa1IRO10wHjemBEBmXyyqZaHLyeTsZGObSyAfolmzcQlPSynHZdjt61XGsWsHkzjOuXRvHRKlDsroa/voauAWAjuCytt51rl4TmpqbGhtSGwQN6931fTZ4cFNhdsqN1o+seKLcH/gdtwmLjacIuugAAAABJRU5ErkJggg=="></a></div>
            <div class="rsrvsubhead">
              <div class="rsrvimage"><img src="https://fi.realself.com/small/00a2adaf55a51aaac47d29dc25e2379c/3/1/2/1618705-2662745.png"></div>
              <div class="rsrvbylineandrating">
                <div class="rsrvbyline"><a rel="nofollow" href="#" target="_blank">Kelly E. Tjelmeland, MD</a></div>
                <div class="rsrvrating"><a href="#" target="_blank" rel="nofollow"><span class="star "></span><span class="star "></span><span class="star "></span><span class="star "></span><span class="star "></span> <span class="ratingtext">5.0&nbsp;stars from 22&nbsp;reviews</span></a></div>
              </div>
            </div>
            <div class="rsrvtopic">
              <select class="rsrvtopicselect">
                <option value="1">Hair Transplant</option>
              </select>
            </div>
            <div class="rsrvreviews"> <a href="#" target="_blank" rel="nofollow" class="rsrvreviewwrapper">
              <div class="rsrvreview"><span class="star "></span><span class="star "></span><span class="star "></span><span class="star "></span><span class="star "></span>
                <div class="rsrvreviewtitle">Wonderful Experience with Dr. Tjelmeland and Staff</div>
                <div class="rsrvreviewsnippet">My motivation to do my face, eyes and brows was to look younger. And it worked! Dr. Tjelmeland did such a wonderful job I look younger but still like myself...<span class="rsrvreviewreadmore">READ MORE</span></div>
              </div>
              </a> <a href="#" target="_blank" rel="nofollow" class="rsrvreviewwrapper">
              <div class="rsrvreview"><span class="star "></span><span class="star "></span><span class="star "></span><span class="star "></span><span class="star "></span>
                <div class="rsrvreviewtitle">Excision of a Basal Cell Carcinoma - Austin, TX</div>
                <div class="rsrvreviewsnippet">Dr. Tjelmeland's expertise is well known to me. My wife, mother and daughter have all been to him for various procedures. I was diagnosed with a basal cell...<span class="rsrvreviewreadmore">READ MORE</span></div>
              </div>
              </a> <a href="#" target="_blank" rel="nofollow" class="rsrvreviewwrapper">
              <div class="rsrvreview"><span class="star "></span><span class="star "></span><span class="star "></span><span class="star "></span><span class="star "></span>
                <div class="rsrvreviewtitle">Looking Tired All the Time - Austin, TX</div>
                <div class="rsrvreviewsnippet">Dr. Tjelmeland did my mother's facelift . He's also performed  my wife's tummy tuck - both with excellent outcome. I was constantly being told I looked...<span class="rsrvreviewreadmore">READ MORE</span></div>
              </div>
              </a> <a href="#" target="_blank" rel="nofollow" class="rsrvreviewwrapper">
              <div class="rsrvreview"><span class="star "></span><span class="star "></span><span class="star "></span><span class="star "></span><span class="star "></span>
                <div class="rsrvreviewtitle">Breast Augmentation Sientra 405 Hp and Hernia Repair :) - Austin, TX</div>
                <div class="rsrvreviewsnippet">Sientra 405 HP silicone smooth round
                  From 34A to 34C (or small D)
                  
                  Hernia repair - Had a slight hole and he fixed it and made my belly button cute :)
                  
                  I was...<span class="rsrvreviewreadmore">READ MORE</span></div>
              </div>
              </a> <a href="#" target="_blank" rel="nofollow" class="rsrvreviewwrapper">
              <div class="rsrvreview"><span class="star "></span><span class="star "></span><span class="star "></span><span class="star "></span><span class="star "></span>
                <div class="rsrvreviewtitle">39yo Mama of 2 Littles Finallllly Gets a MommyMakeover W/mini TT and BA. Austin, TX</div>
                <div class="rsrvreviewsnippet">I had my 1st baby at 35 and 2nd at 37. They are now 3&amp;5. Body popped right back after the 1st but w/2nd I was JACKED UP! My abs had split and even though I...<span class="rsrvreviewreadmore">READ MORE</span></div>
              </div>
              </a> </div>
            <div class="rsrvmorereviews"><a rel="nofollow" href="#" target="_blank">Read all my RealSelf reviews</a></div>
            <div class="rsrvdisclaimer">Disclaimer: Reviews shown here are curated from RealSelf. Results and patient experience may vary.</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('front/partials/dr_kapil_dua_blogs_block'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
<?php $this->load->view('front/partials/form_dr'); ?>
<script type="text/javascript">
        
        jQuery(".changeMovie").click(function ()
        {
            var val = jQuery(this).attr("alt");
            if (val != "")
            {
                var str = '<iframe class="embed-responsive-item videoShow" src="https://www.youtube.com/embed/' + val + '?autoplay=1"></iframe>';
                jQuery(".videoShow").html(str);
            }
        });

        $('#achievements-block').click(function () {
            $('a#achievements-block').css('display', 'none');
            $('.kd-logo').fadeOut(500);
            $('a#achievements-block-1').css('display', 'block');
            $(".achiv-block").fadeIn(1000);
        });

        $('#achievements-block-1').click(function () {
            $('a#achievements-block-1').css('display', 'none');
            $('.kd-logo').fadeIn(2000);
            $('a#achievements-block').css('display', 'block');
            $(".achiv-block").fadeOut(1000);
            window.location.href = "#kd";
        });


        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default',
                width: 'auto',
                fit: true,
                closed: 'accordion',
                activate: function (event) {
                    var $tab = $(this);
                    var $info = $('#tabInfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
        });

        
 $(document).on('ready', function () {
            $(".regular").slick({
                dots: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
    });

        $(document).on('ready', function () {
            $(".video").slick({
                dots: false,

                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true, fade: true,
                autoplaySpeed: 10000, swipe: true,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        });

    </script> 
<script defer="defer" src="<?php echo cdn('assets/template/frontend/'); ?>js/easy-responsive-tabs.min.js" ></script> 
<script>
	$(document).ready(function () {
		$('#horizontalTab').easyResponsiveTabs({
			type: 'default',
			width: 'auto',
			fit: true,
			closed: 'accordion',
			activate: function (event) {
				var $tab = $(this);
				var $info = $('#tabInfo');
				var $name = $('span', $info);
				$name.text($tab.text());
				$info.show();
			}
		});
	});
</script>