<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/"> <span itemprop="name">Our Team</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/dr-roshan-kumar/"> <span itemprop="name">Dr Roshan Kumar</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="content ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="text-left col-manage-section">
                <h2>Doctors Profile</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 dr-figure">
                <figure> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/dr-roshan-1.png" class="img-fluid" alt="Dr. Roshan - Best Dermatologist & Hair Restoration Surgeon in Bangalore"> </figure>
                <div class="doctors-social-icons text-center"> 
                    <a href="https://www.twitter.com/akclinics" target="_blank" class="icons"><i class="fa fa-twitter fa-1x"></i></a> 
                    <a href="https://plus.google.com/+Akclinics" target="_blank" class="icons google"><i class="fa fa-google-plus fa-1x"></i></a> 
                    <a href="https://www.linkedin.com/company/ak-clinics/" target="_blank" class="icons in"><i class="fa fa-linkedin fa-1x"></i></a> 
                </div>
            </div>
            <div class="col-sm-8 doctors-details-section">
                <h1>Dr. Roshan Kumar B</h1>
                <article class="dr-des">
                    <span>Qualification</span>
                    <span>MBBS, MD(Skin & VD),Ex ARMY (AMC)</span>
                    <span class="dr-des">Dermatologist & Hair Restoration Surgeon</span> </article>
                <p class="set-para">Dr. Roshan Kumar B is an aesthetic dermatologist and hair transplant surgeon, with experience of 14 years in the field of medicine. He has vast experience in Armed Forces Medical corps, and been medical director (Aesthetics) at Femiint health Bangalore. He completed his MBBS from renowned Karnataka Institute of Medical Sciences, Hubli and MD(DVL) from prestigious Command Hospital, Bangalore.</p>
                <blockquote class="doctors-quote"><strong>RESEARCH PROJECTS - </strong>Efficacy of BCG and MMR in Recalcitrant Verrucae</blockquote>
                <p class="set-para">Over the course of years, he has learned and mastered art of Dermatology, Cosmetology and Hair transplant. He was trained under Dr Zein Obagi (USA) in chemical peels, Dr paolo Sbano (ITALY) in medical lasers and Dr Pablo Naranjo (Spain) in Cryolipoloysis. He is member of IADVL, BDS, AFAASI, and CDSI</p>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-orange">
    <div class="container">
        <div class="col-md-12 fun-facts">
            <div class="row">
                <div class="col-md-3 fun-col"> <strong class="big-font">2000</strong>
                    <p> Happy Patients</p>
                </div>
                <div class="col-md-3 fun-col"> <strong class="big-font">5 Million</strong>
                    <p>Grafts transplanted</p>
                </div>
                <div class="col-md-3 fun-col"> <strong class="big-font">3% or Less</strong>
                    <p>Controlled FTR</p>
                </div>
                <div class="col-md-3 fun-col"> <strong class="big-font">18-75</strong>
                    <p>Age Group</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg" style="background-image: url(&quot;<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg&quot;); ">
    <div class="container pt-0 pb-0">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 sm-height-auto">
                    <div class="p-30 bg-deep-transparent " style="height:380px; overflow-y:scroll;">
                        <h4 class="title mt-0 line-bottom line-height-2  p-0">Journal & Poster Publications</h4>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title"><i class="fa fa-share fa-1x" aria-hidden="true"></i> &nbsp;Pemphigoid gestationis; A case report and review of literature</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title"><i class="fa fa-share fa-1x" aria-hidden="true"></i> &nbsp;Efficacy of Infliximab vs Etanercept series of 15 cases in psoriasis.</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title"><i class="fa fa-share fa-1x" aria-hidden="true"></i> &nbsp;Efficacy of intralesional BCG vs MMR in recalcitrant palmoplantar warts.</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box"><h5 class="icon-box-title"><i class="fa fa-share fa-1x" aria-hidden="true"></i> &nbsp;Tele dermatology a pilot project</h5>
                            </div>
                        </div>
                        <h4 class="title mt-0 line-bottom line-height-2  p-0">Awards</h4>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title"><i class="fa fa-share fa-1x" aria-hidden="true"></i> &nbsp;Aswath Ram Jay Nazir award for topper in Preventive and Social Medicine</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title"><i class="fa fa-share fa-1x" aria-hidden="true"></i> &nbsp;Best outstanding post graduate student command hospital</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/meet_the_team_section'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
