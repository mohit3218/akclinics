
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/"> <span itemprop="name">Our Team</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/dr-vivek-mehta/"> <span itemprop="name">Dr Vivek Mehta</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="content ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="text-left col-manage-section">
                <h2>Doctors Profile</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 dr-figure">
                <figure> <img src="<?php echo cdn('assets/template/frontend/'); ?>img/dr-vivek-1.png" class="img-fluid" alt="Dr Vivek - Dermatologist at AK Clinics"> </figure>
                <div class="doctors-social-icons text-center"> 
                    <a href="https://twitter.com/drkapildua" target="_blank" class="icons"><i class="icofont-twitter  icofont-1x"></i></a> 
                    <a href="https://plus.google.com/105305311373999416206" target="_blank" class="icons google"><i class="icofont-google-plus  icofont-1x"></i></a> 
                    <a href="https://in.linkedin.com/in/drkapildua" target="_blank" class="icons in"><i class="icofont-linkedin  icofont-1x"></i></a> 
                </div>
            </div>
            <div class="col-sm-8 doctors-details-section">
                <h3>Dr. Vivek Mehta</h3>
                <article class="dr-des">
                    <span>Qualification</span>
                    <span>M.B.B.S, M.D. (Dermatology)</span> 
                    <span>Dermatologist </span></article>
                <p class="set-para">Dr. Vivek Mehta is certified dermatologist at AK clinics has more than 10 years of experience in the field of dermatology. He is associated with MCI and DMC and has worked as a dermatologist at Primus Hospital, New Delhi. He is expert in diagnosis and management of hair, skin and nails infections or other similar diseases.</p>
                <p class="set-para">Dr. Vivek Mehta is certified dermatologist in Delhi associated with with MCI and DMC and has a speciality in diagnosis and management of hair, skin and nail infections or diseases. Having an experience of more than a decade, Dr. Vivek Mehta, who is a skin specialist in Delhi, has a seen all kinds of patients: national as well as international. He has worked as a proficient Dermatologist at Primus Hospital, New Delhi. Dr. Mehta has the capability of managing Pediatric dermatology, pregnancy related skin disorders and other types of dermatology related issues like cosmetic dermatology, aesthetic dermatology and all types of laser surgeries. He has been effectively disseminating awareness about the facts and myths related to skin laser treatment in Delhi, through interviews on public platforms like TV, radio and print media.</p>
            </div>
        </div>
    </div>
</div>

<div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg" style="background-image: url(&quot;<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg&quot;); ">
    <div class="container pt-0 pb-0">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 sm-height-auto">
                    <div class="p-30 bg-deep-transparent " style="height:500px; overflow-y:scroll;">
                        <h4 class="title mt-0 line-bottom line-height-2 text-black-222">Awards & <span class="text-theme-colored">Honors</span></h4>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Awarded Rising Star Scholarship to attend World Congress of Dermatology at Vancouver in Jun 2015</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Awarded Imrich Sarkany Non European Memorial Scholarship awarded by EADV in May 2013</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Awarded Prize for Poster Presentation at 34th National Conference held at Hyderabad in Jan 2006</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Awarded 2nd prize for Award Presentation in 7th Biennial Conference of ACSICON held at Delhi</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">  Faculty for IADVL lasers workshop Jan 2013</h5>
                            </div>
                        </div>

                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title"> Faculty for Aesthetics Delhi 2013-2017 </h5>
                            </div>
                        </div>

                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">  Faculty for IMCAS Bali 2015</h5>
                            </div>
                        </div>

                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title"> Faculty for IMCAS PARIS 2016, 2017 and 2018</h5>
                            </div>
                        </div>

                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">  Faculty for FACE LONDON 2016</h5>
                            </div>
                        </div>

                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">  Faculty for Star Asia Laser Surgeon Allies 2016 and 2017</h5>
                            </div>
                        </div>

                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">  Invited FACULTY for 5CC Barcelona 2017</h5>
                            </div>
                        </div>

                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title"> Invited FACULTY for World Congress of Cosmetic Dermatology 2017 </h5>
                            </div>
                        </div>

                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">  KOL for Jeisys Medical (South Korea)</h5>
                            </div>
                        </div>

                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title"> KOL for Reckitt Benckiser </h5>
                            </div>
                        </div>


                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">  KOL for 5CC Barcelona 2017 and 2018</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">  Organising secretary for Estheticxpress 2018</h5>
                            </div>
                        </div>


                        <h4 class="title mt-0 line-bottom line-height-2 text-black-222">Fellowship</h4>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Fellowship in Dermatosurgery in Safdurjung Hospital in December 2008</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Visiting Fellow in Dermatological Laser Surgery in December 2009 from Mahidol University,Bangkok, Thailand</h5>
                            </div>
                        </div>


                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">      Fellowship in Contact Dermatitis from AIIMS in 2011</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/meet_the_team_section'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
