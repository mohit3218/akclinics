<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/"> <span itemprop="name">About US</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/"> <span itemprop="name">Our Team</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area">
    <div class=" pt-5" style="background-color: #dddfde;
         background-image: url(https://medicare.bold-themes.com/laboratory/wp-content/uploads/sites/15/2015/12/bg-doktorka.jpg);
         background-size: cover;
         background-position: 50% 50%;">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class=" col-md-7 col-ms-12 text-center pt5 ">
                        <div class="btSuperTitle">Dr. Kapil Dua</div>
                        <div class="dash">
                            <h1><span class="headline">OUR <b class="animate animated">TEAM</b></span></h1>
                        </div>
                        <div class="btSubTitle ptb-1">
                            <p>Dr. Kapil Dua, Co-Founder & Chairman at AK Clinics has over fourteen years of clinical and teaching experience in the fields of Otolaryngology & Hair Transplant. He holds the recognition for having transplanted over 3 million grafts, among the highest in the world, with a record of less than 3% wastage, among the best in the world.</p>
                        </div>
                    </div>
                    <div class="col-md-5 col-ms-12">
                        <div class="row">
                            <div class="text-center">
                                <div class="btImage text-center"> 
                                    <img data-src="<?php echo cdn('assets/template/frontend/'); ?>img/dr-kapil-dua.png" class="img-fluid lazy" alt="Dr Kapil Dua">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/founder'); ?>
<?php $this->load->view('front/partials/meet_the_team_section'); ?>
<?php $this->load->view('front/partials/visitors_doctors_section'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>