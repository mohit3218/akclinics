<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>hair-transplant-training/"> <span itemprop="name">Hair Transplant Training</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Hair Transplant Training</h1>
                </div>
                <p>From the time when the brilliant minds of Dr Rassman, Dr Bernstein and Dr Woods came up with the initial concept of follicular unit extraction or FUE, there has been a lot of development in the field. Today, transplant has become an extremely popular domain and a growing number of young doctors and medical students are looking at trichology and hair transplant as a possible as well as promising career.</p>
                <p>At AK Clinics, we not only aim at offering some of the finest hair transplant and restoration services to clients, but also educate those who come to us seeking knowledge. Our panel of doctors are members of the esteemed ISHRS or International Society of Hair Restoration Surgery, which means that they are constantly in touch with the latest innovations as well as the pioneers in the field.</p>
                <p>This is why we have put together a range of training programs, which cater to the needs of those who come to this temple of learning, healing and restoration. As a matter of fact, our training programs are so thorough that once through; doctors will be able to set up their very own <a href="<?= base_url(); ?>hair-transplant/">hair transplant clinics</a>.</p>
                <h4>Our hair transplant training program includes:</h4> 
                <ul>
                    <li>On-site training for doctors at AK Clinics</li>
                    <li>List of equipment that will be required to set up the clinic</li>
                    <li>Advice on how to set up the clinic</li>
                    <li>Complete assistance for six months with consultations via email – however, this facility is offered only to those attending the one or two and a half month programs.</li>
                </ul>


            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>about-us/">About US</a></li>
                        <li><a href="<?= base_url(); ?>about-us/our-team/">Our Team</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs" style="min-height: 264px;">
                <div class="tab">
                    <button class="tab-toggle">Basic program (course duration – 1 day)</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Basic program (course duration – 1 day)</h3>
                    <ul>
                        <li>One-day live and practical observation of FUE surgery</li>
                        <li>Thorough and detailed discussion with surgeons about FUE surgery</li>
                        <li>Preoperative preparation &amp; post-operative care and handling of the patient</li>
                        <li>Training Fee – USD 200 + ST (18%)</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">AK Clinics Hair Transplant Training</button>
                </div>
                <div class="content">
                    <h3 class="m-3">AK Clinics Hair Transplant Training</h3>
                    <figure class="content-center "><img class="img-fluid lazy" data-src="<?= cdn('assets/template/uploads/'); ?>2015/12/hair-transplant-traning-1.png" alt="Hair Transplant Training at AK Clinics "></figure>

                </div>
                <div class="tab">
                    <button class="tab-toggle ">Intermediate program (course duration – 1 month)</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Intermediate program (course duration – 1 month)</h3>
                    <ul>
                        <li>Basics of trichology: anatomy of hair &amp; hair cycle</li>
                        <li>Diagnosis and symptoms of hair loss and related diseases</li>
                        <li>Setting up of the trichology clinic with essential amenities</li>
                        <li>All aspects of hair restoration surgery:
                            <ol>
                                <li>1. Selecting a patient for hair restoration surgery with best potion of available treatment</li>
                                <li>2. Setting up of operation theatre and what equipment to use</li>
                                <li>3. Follicular Unit Extraction as well as transplantation</li>
                                <li>4. Post operative care</li>
                                <li>5. Body hair transplants</li>
                                <li>6. Women hair transplant</li>
                                <li>7. Revision hair transplant</li>
                            </ol>
                        </li>
                        <li>Hands on training and experience of extraction of grafts on models and patients with all biological and technical assistance (depending on surgical skills of Doctor: 10-15 grafts per patient in last five cases).</li>
                        <li>Hands on training of implantation of grafts on models and patients (depending on surgical skill of physician: 10-15 grafts per patient in last five cases).</li>
                        <li>Complete operation theatre observation – 15 cases of live surgery</li>
                        <li>Training Fee – USD 4000 + ST (18%)</li>
                    </ul>

                </div>
                <div class="tab">
                    <button class="tab-toggle">Advanced (course duration – 3 months)</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Advanced (course duration – 3 months)</h3>
                    <ul>
                        <li>Basics of trichology: anatomy of hair and hair cycle</li>
                        <li>Details of causes of hair loss; diagnosis of hair loss and in-depth study of pattern of baldness</li>
                        <li>Vital and essential details about setting up of an ultra-modern trichology clinic.</li>
                        <li>All aspects of hair restoration surgery:
                            <ol>
                                <li>1. Selecting a patient for hair restoration</li>
                                <li>2. peration theatre &amp; setting up of instrumentation</li>
                                <li>3. Follicular Unit Extraction</li>
                                <li>4. Implantation</li>
                                <li>5. Postoperative care</li>
                                <li>6. Body hair transplants</li>
                                <li>7. Women hair transplant</li>
                                <li>8. Revision hair transplant</li>
                            </ol>
                        </li>
                        <li>Hands on complete training of extraction of grafts on models and patients</li>
                        <li>Hands on thorough training of implantation of grafts on models and patients</li>
                        <li>Training on Mesotherapy and LLLT</li>
                        <li>Training on sales and marketing of the trichology clinic</li>
                        <li>Complete observation with detailed analysis in operation theatre – 50 cases</li>
                        <li>Doubts removing session</li>
                        <li>Training fee – USD 8000 + ST (18%)</li>
                    </ul>

                    <p>While our primary aim is to offer every trainee intricate knowledge and clarify all doubts, we also strive to create hair transplant surgeons, who are capable of running their own clinic. This is why, we also offer important and essential tips on how to run the business aspect of the newly set up clinic. Hair restoration might seem glamourous on the outside, but there is a lot of hard work involved and the long hours can become tedious. This is why; we ensure that we talk in detail with the trainees, ensuring that they really want to do this. In addition, our training program is designed to allow the doctors a fledging career of their own, in minimal time.
                        We welcome doubts and queries and are always willing to cover any other aspects that trainees might feel a little less confident about.</p>
                </div>
            </div>
        </div>
    </div>
</div>