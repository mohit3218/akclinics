<div class="header-cover">
    <div class="carousel-caption text-center">
        <ul>
            <li><i class="fa fa-check fa-1x"></i>Virtually Undetectable Natual Looking Results</li>
            <li><i class="fa fa-check fa-1x"></i>Performed by renowned UK Hair Transplant Surgeons</li>
            <li><i class="fa fa-check fa-1x"></i>We do not use a robot machine it is too rough</li>
            <li><i class="fa fa-check fa-1x"></i>Over 10 years’ experience in perfecting the FUE technique</li>
        </ul>
    </div>
</div>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team"> <span itemprop="name">Our Team</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/dr-jigisha-jalu"> <span itemprop="name">Dr Jigisha Jalu</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/services'); ?>
<?php $this->load->view('front/partials/meet_the_team_section'); ?>