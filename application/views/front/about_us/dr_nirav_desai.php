<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/"> <span itemprop="name">Our Team</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/dr-nirav-desai/"> <span itemprop="name">Dr Nirav Desai</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="content ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="text-left col-manage-section">
                <h2>Doctors Profile</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 dr-figure">
                <figure> <img src="<?php echo cdn('assets/template/frontend/'); ?>images/dr-nirav-desai.png" class="img-fluid" alt="Dr Aman Dua - best Dermatologists in Delhi"> </figure>
                <div class="doctors-social-icons text-center"> 
                    <a href="https://www.twitter.com/akclinics" target="_blank" class="icons"><i class="fa fa-twitter fa-1x"></i></a> 
                    <a href="https://plus.google.com/+Akclinics" target="_blank" class="icons google"><i class="fa fa-google-plus fa-1x"></i></a> 
                    <a href="https://www.linkedin.com/company/ak-clinics/" target="_blank" class="icons in"><i class="fa fa-linkedin fa-1x"></i></a> 
                </div>
            </div>
            <div class="col-sm-8 doctors-details-section">
                <h1>Dr. Nirav Desai</h1>
                <article class="dr-des">
                    <span>Qualification</span>
                    <span>Aesthetic Dermatologist & Consultant Hair Transplant Surgeon</span></article>
                <span class="dr-des">MCI Reg No: G21393</span></article>
                <span class="dr-des">IADVL Membership No : LM/M/5737 </span></article>
                <p class="set-para">
                    Dr. Nirav V Desai, Dermatologist at AK Clinics has more than 7 years of experience in the field of 
                    Aesthetic Dermatology, Dermato-surgeries and hair restoration. He completed his DNB-DVD from 
                    india’s one of the apex institute Dr.Dhepe’s SkinCity, Pune accredited by Nation Board of 
                    Examination-Delhi, famous for providing training in various Laser procedure and Dermato-surgeries 
                    to many dermatologist across the world.
                </p>
               <!-- <blockquote class="doctors-quote"> Aesthetic Dermatologist & Consultant Hair Transplant Surgeon</blockquote>  -->
                <p class="set-para">
                    Prospective, Comparative Study of diode laser and Long Pulsed Nd:YAG laser in respect to their 
                    safety & efficacy for the laser hair removal in Indian skin? This was its kind of first study in 
                    India comparing two laser technologies in same patient to find out which technology is best as 
                    well as safe
                </p>
            </div>

        </div>
    </div>
    <div class="service-area ptb-3 bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2 class="black-text text-center">More about Dr. Nirav Desai</h2>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <p>Over the course of these years, He has mastered the art of lasers and dermatosurgeires by working with some of the leading dermatologists across India and abroad. He was also a part of team in establishing the Advanced Cosmetic Laser centre for the one of the biggest hospital in Africa. He has participated in all types of hair restoration surgeries including FUE, Strip and Body hair transplantation.</p>
                    <blockquote>  <p>This was its kind of first study in Indiacomparing two laser technologies in same patient to find out which technology is best as well as safe</p>  </blockquote>
                </div>
            </div>
        </div>
    </div>
    <div class="service-area ptb-3 bg-light-orange">
        <div class="container">
            <div class="col-md-12 fun-facts">
                <div class="row">
                    <div class="col-md-3 fun-col"> <strong class="big-font">2000</strong>
                        <p> Happy Patients</p>
                    </div>
                    <div class="col-md-3 fun-col"> <strong class="big-font">5 Million</strong>
                        <p>Grafts transplanted</p>
                    </div>
                    <div class="col-md-3 fun-col"> <strong class="big-font">3% or Less</strong>
                        <p>Controlled FTR</p>
                    </div>
                    <div class="col-md-3 fun-col"> <strong class="big-font">18-75</strong>
                        <p>Age Group</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg" style="background-image: url(&quot;<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg&quot;); ">
        <div class="container pt-0 pb-0">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-8 sm-height-auto">
                        <div class="p-30 bg-deep-transparent " style="height:500px; overflow-y:scroll;">
                            <h4 class="title mt-0 line-bottom line-height-2 text-black-222">Recognition & <span class="text-theme-colored">Achievements</span></h4>
                            <p>The patient satisfaction & results is the main criteria for the success of a surgeon. But equally important is the recognition from the very own medical fraternity. When a surgeon’s work is evaluated by critics from the fraternity, the surgeon works on further finesse. This again benefits the patients. </p>
                            <div class="items">
                                <div class="icon-box">
                                    <h5 class="icon-box-title">Successful treatment of post burn scar with fractional CO2 Laser in Indians. WCD 2011</h5>
                                </div>
                            </div>
                            <div class="items">
                                <div class="icon-box">
                                    <h5 class="icon-box-title">Successful treatment of Becker’s nevus with Light Energy Optimization (Modified IPL). WCD 2011, ASLMS 2011, EADV 2011.</h5>

                                </div>
                            </div>
                            <div class="items">
                                <div class="icon-box">
                                    <h5 class="icon-box-title">‘Cyanoacrylate Lamination Technique’ in Miniature Punch Grafting in stable vitiligo at difficult sites. ASDS 2011.</h5>

                                </div>
                            </div>
                            <div class="items">
                                <div class="icon-box">
                                    <h5 class="icon-box-title">Treatment of various scars with fraction CO2 laser in Indian patients. EADV 2011.</h5>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('front/partials/meet_the_team_section'); ?>
    <?php $this->load->view('front/partials/doctor_contacts'); ?>