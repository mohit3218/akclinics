
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/"> <span itemprop="name">Our Team</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>about-us/our-team/dr-bhawna-bhardwaj/"> <span itemprop="name">Dr Bhawna Bhardwaj</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="content ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="text-left col-manage-section">
                <h2>Doctors Profile</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 dr-figure">
                <figure> <img src="<?php echo base_url('assets/template/frontend/'); ?>img/dr-bhawna-1.png" class="img-fluid"> </figure>
                <div class="doctors-social-icons text-center"> 
                    <a href="https://www.twitter.com/akclinics" target="_blank" class="icons"><i class="fa fa-twitter fa-1x"></i></a> 
                    <a href="https://plus.google.com/+Akclinics" target="_blank" class="icons google"><i class="fa fa-google-plus fa-1x"></i></a> 
                    <a href="https://www.linkedin.com/company/ak-clinics/" target="_blank" class="icons in"><i class="fa fa-linkedin fa-1x"></i></a>   </div>
            </div>
            <div class="col-sm-8 doctors-details-section">
                <h3>Dr. Bhawna Bhardwaj</h3>
                <article class="dr-des">
                    <span>Qualification</span>
                    <span>MBBS, DVDL (Skin & VD)</span> 
                    <span>Consultant Dermatologist & Hair Transplant Surgeon</span></article>
                <p class="set-para">Dr. Bhawna Bhardwaj is an aesthetic dermatologist and hair transplant surgeon with contribution of 5 and ½ yrs in the field of dermatology. She passed her DVDL from MRMC , Gulbarga</p>
                <span class="dr-des"><em>RESEARCH PROJECTS</em></span>
                <p class="set-para">Genetic predisposition in leprosy</p>
                <p class="set-para">She has worked as a consultant dermatologist in Kutiz Skin Clinic, Dehradun & Kaya Skin Clinic, Vasant Kunj, New Delhi.Over the course of years, she has mastered the art of clinical dermatology ,aesthetic dermatology which includes injectables (Botox , Fillers, PRP for various indications ) , Peels, Mesotherapy , various lasers for laser hair removal (Diode, Long Pulse NdYAG, Alexandrite) , Fractional laser, Q switch NdYAG laser and lasers used for anti-aging (Thermage and Exilis, Microneedling RF) and Body Contouring.She is a member of IADVL and has attended many workshops and conferences held by IADVL, Dermacon, Cuticon and international associations.</p>

            </div>
        </div>
    </div>
</div>

<div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg" style="background-image: url(&quot;<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg&quot;); ">
    <div class="container pt-0 pb-0">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8 sm-height-auto">
                    <div class="p-30 bg-deep-transparent " style="height:500px; overflow-y:scroll;">
                        <h4 class="title mt-0 line-bottom line-height-2 text-black-222">Paper & <span class="text-theme-colored">Poster Presentations</span></h4>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Poster on giant melanocytic bathing trunk nevus</h5>
                            </div>
                        </div>


                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Psoriatic arthritis mutilans- a rare entity!!</h5>
                            </div>
                        </div>
                        <div class="items">
                            <div class="icon-box">
                                <h5 class="icon-box-title">Paper on Genetic predisposition in leprosy</h5>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/meet_the_team_section'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>

