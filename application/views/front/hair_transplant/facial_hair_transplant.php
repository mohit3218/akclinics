<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/"> <span itemprop="name">Facial Hair Transplant</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Facial Hair Transplant – Beard, Moustache and Eyebrow</h1>
                    <h4 class="text-left"> What is Facial Hair Transplant?</h4>
                    <p class="text-justify">People who are considering undergoing a facial hair transplant, which could include eyebrows, sideburns, moustache, beard or even eyelashes, generally tend to be people who might have lost their hair due to certain conditions. These conditions can be:</p>
                    <ul class="text-left list-unstyled">
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Scars which could have been caused due to accidents, certain surgical procedures or burns</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Medical conditions such as cancer, which require chemotherapy or radiation, which could lead to hair loss</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <a href="<?= base_url(); ?>blog/traction-alopecia-major-problem-hairs/">Traction alopecia</a> or really tight hair styles or pulling back of hair, leading to the pulling of the hair follicles. becoming weak</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Conditions such as long standing <a href="<?= base_url(); ?>blog/alopecia-areata/">alopecia areata</a></li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Scarring which can be caused by conditions such as folliculitis</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Previous transplant surgeries that might have gone wrong or might not have given as per liking</li>
                    </ul>
                    <p class="text-left">When you talk to some of the <a href="<?= base_url(); ?>about-us/our-team/dr-kapil-dua/">best facial hair transplant surgeons</a> in the country, you will realise that losing facial hair is not as uncommon and there are plenty of others who might be suffering the same plight as yours. However, with medical advancements, replacing facial hair has actually become quite easy.</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3  bg-light-gray ">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Facial Hair Transplant</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What is a facial hair transplant?</h3>
                    <p>Any facial hair, including eyebrows, eyelashes, moustache, beard and sideburns can be considered for a facial hair transplant. If someone has lost hair in these areas or is facing thinning, then a facial hair transplant would be a good option. However, it is important that you choose to get the procedure done through the best facial hair transplant clinic, because if not done by experienced hands, there can be serious side effects or the final results can be quite disappointing.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Who Need Facial Hair Transplant?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Who might need a facial hair transplant?</h3>
                    <p>It is important to understand that facial hair features such as moustache, beard and eyebrows are a part of what constitute a face. when these facial hair are not in alignment, the face will look odd, which is why when there is hair thinning, irregular shaping of the facial hair or loss of hair due to an accident, and people are able to notice it immediately.</p>
                    <ul class="text-left list-unstyled">
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Someone who is trying to gain a little more symmetry in their face</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Someone who is trying to hide marks or scars, which could range from birthmarks, acne or injury scars</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Adjusting or increasing the size of the eyebrows, moustache or beard</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Changing the shape or altogether creating new eyebrows, moustache or beard</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Providing a covering for a part of the face that might have been badly injured</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">An Ideal Candidate</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Who would prove to be an ideal candidate?</h3>
                    <p>There are certain criteria that need to be met, before a person can be considered an eligible candidate for a facial hair transplant:</p>
                    <ul class="text-left list-unstyled">
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The person has to be physically healthy and of sound mind</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The person who understands the pros and cons of undergoing the facial hair transplant should not be looking for the perfect facial hair transplant for any wrong purposes or under peer pressure</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The person should be willing to accept the final <a href="<?= base_url(); ?>results/">results</a>, because it is not always necessary that the final result will be exactly what you are expecting</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The person should not be suffering from any serious health conditions</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Facial Hair Transplant Done</button>
                </div>
                <div class="content">
                    <h3 class="m-3">How is a facial hair transplant done?</h3>
                    <p>A facial hair transplant is generally a minimally invasive one. And in the hands of an experienced surgeon, practicing at one of the finest clinics for facial hair transplant in India, the process is a reasonably simplistic one. As a matter of fact, an experienced surgeon will be able to grow hair, where there aren’t any. Hair will be removed from the side or the back of the head, via FUE or FUT and healthy hair follicles will be extracted. The area that needs to receive these hair follicles will be demarcated well in advance, and the surgeon will place the grafts in the same.<br />
                        The procedure is actually quite similar to an FUE that would be done on the head, and direction of hair and orientation is very important. The choice of procedure will be heavily dependent on aspects such as which part of the face is involved and how the final design will be. With an <a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE procedure</a>, there will be no scar, which is why it is the most chosen for facial hair transplant.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Investigations for Facial Hair Transplant?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What are the pre-op investigations for a facial hair transplant?</h3>
                    <p>Before you can undergo a facial hair transplant, you will have to spend some time with your doctor. For starters, your doctor will have to confirm that you are physically and mentally fit to undergo the procedure and you are looking to get the same done for valid reasons.<br />
                        You might be asked to undergo a few basic blood tests ( like haemogram, bleeding profile tests) and if you are on any ( herbal or multivitamin) medication, you might be asked to stop or alter them for a few days. This would also be the time when your donor and recipient areas would be scrutinised carefully, allowing the doctor to help you finalise a design for your facial features. You would also be given an idea of how many grafts will be required and how much the entire process will cost you.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Side Effects to a Facial Hair Transplant?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What are the possible side effects to a facial hair transplant?</h3>
                    <p>This is where the expertise of your surgeon comes into picture, because the more experienced the doctor is, the lesser the chances of anything going wrong. When the procedure has been done correctly, there will no scars, discomfort or infections. If the procedure has not been done properly, there is quite the chance that you might end up with scars, infection, swelling, bruising or even an improper hairline.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Points kept in mind after Facial Hair Transplant</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What are the points that need to be kept in mind after the facial hair transplant has been completed?</h3>
                    <p>Depending on the extent of the surgery, your doctor will administer you with local or general anaesthesia. In most cases, There is no requirement to stay overnight, but and you will be given detailed information on how to take care of your newly transplanted grafts. You will be asked to treat the area with immense care, including washing it and drying it. There might be a little bruising or swelling, but this is normal and should disappear in a few days. In addition, the transplanted hair will fall off ( after 2-4 weeks like any other transplanted hair on the scalp), leading way to new hair</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Things that you need to know, before Facial Hair Transplant</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What are the things that you need to know, before getting a facial hair transplant?</h3>
                    <p>A facial hair transplant is a <a href="<?= base_url(); ?>hair-transplant/">surgical procedure</a>, which means that there are bound to be some complications and risks, which you will have to be prepared about in advance.Given that this is a surgical procedure, it might not be the easiest on our pocket and many insurance companies will not cover the same, considering them cosmetic in nature.Designing a facial feature is possible, but you will have to keep your expectations realistic. The same will also be dependent on the hair in your donor area.While the procedure might last only a few hours, you will have to take some time off, for the healing process to be completed.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Why get a facial hair transplant at AK Clinics?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Why get a facial hair transplant at AK Clinics?</h3>
                    <p>When you come to AK Clinics, you get a team of doctors, who are not only extremely qualified, but also have several years of experience. Each of the doctors has trained at the most renowned organisations and has practised under some of the biggest names of the domain.For a doctor to be successful, having an adept team is just as necessary, which is what you will get at AK Clinics. The team is well trained and with their assistance, not only do procedures get completed well within time, but also with greater efficiency.<br />
                        The standards of patient care, hygiene and the equipment used are at par with any international centre for facial hair transplant, giving each patient the best possible experience.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3  ">
    <div class="container">
        <div class="row align-items-center about-video">
            <div class="col-12 col-lg-6 content-part">
                <h3>Watch our Results</h3>
                <p> Out results are results are testimony to the quality service delivery of our doctors & team. All our hair transplant results, hairless results, cosmetic surgery results or skin treatment results are purely due to standardised practices that we follow at all clinics.</p>
                <p> It is medically not possible to get 100% results but we strive very hard to remain as close as possible. </p>
                <div class=" ptb-1"><a class="btn btn-sm btn-outline waves-effect waves-light" data-scroll="" href="<?= base_url() ?>results/" role="button">Our Results</a></div>


            </div>
            <div class="col-12 col-lg-6 video-part"> 
                <a href="https://www.youtube.com/watch?v=RWKZS72xz3Y" class="media-lightbox">
                    <div class="embed-responsive embed-responsive-16by9">
                        <div class="embed-responsive-item element-cover-bg"> <span class="play-wrapper"> <i class="fa fa-play-circle-o fa-4x"></i> </span> </div>
                    </div>
                </a> 
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="section-title  text-center ">
                <h2 class="black-text"> What are the types of Facial Hair Transplants?</h2>
            </div>
        </div>
        <div class="row text-center pt-1">
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h3>Facial hair transplant for eyebrow</h3>
                <p>The eyebrows are the highlighting factor of the face, which is why their symmetry is of the utmost importance. It is also important to keep in mind their structure, density and size. If you are looking at increasing the length or the size of the eyebrows, the same can be done quite easily. New grafts can be placed to alter the shape or the density of the existing eyebrows. As a matter of fact, this method of transplant can also be used to help people who might have been born without eyebrows, as this is a permanent method and if it is being done via FUE, there will be no scars or marks.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h3>Facial hair transplant for moustache</h3>
                <p>There are several people who have very little growth in their moustache and this could be attributed to genetics, illness or hormonal changes. Conditions such as cleft lip, accidents or illnesses such as cancer could also lead to the moustache being thin and not having the desired density. With facial hair transplant for moustache, not only can lost hair be replaced, the density can be increased as can the length and width. In the hands of a skilled surgeon, you could even have a completely different style of moustache.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h3>Facial hair transplant for beard</h3>
                <p>Several men around the world might have hair growth on their face, but it might not extend properly to a full-fledged beard. There might be a beard that is really sparse or it could have a very irregular hairline. There are also those who might have a bald patch in their beard area, which could look very odd. With a beard transplant, not only can men enjoy a proper beard, but also one that is designed as per their liking. They can ask their doctor to make it as thick or as thin as they want and even the density can be as per individual liking.</p>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>