<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>hair-transplant/body-hair-transplant/"> <span itemprop="name">Body Hair Transplant</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
	<div class="container ">
		<div class="row"> 
			<div class="col-md-9">
				<div class="section-title-1 text-left">
					<h1 class="black-text">Body Hair Transplant</h1>
				</div>
				<p>In any hair transplant procedure normally scalp hair are used for transplant. But in many cases the patients do not have a good donor area to serve the purpose. In such cases, we can use hair from the body e.g. transplant beard to head. Some people call this as chest hair transplant but mostly it means the chest to serve as donor area. Though there is always some difference in texture & thickness of the hair taken from other parts but it actually gives distinct advantage in many cases like:</p>
				<ul class="text-left list-unstyled">
					<li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Limited donor area on scalp (Grade VII patients) looking for covering the complete area.</li>
					<li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Achieving higher density is sometimes very difficult given the limitation of grafts that can be extracted from scalp in one sitting.</li>
					<li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Some cases like burns etc</li>
				</ul>


				<p>We can use the hair from Beard, Shoulders, Abdomen, Legs, Arms, Underarm or Pubic area. Body hair transplant is generally done through <a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE procedure</a> that leaves no linear scar at the end of the surgery. Beard is most suitable as its hair matches exactly with the scalp hair and also because hair from the beard is considered good and permanent.</p>
			</div>
			<div class="col-md-3">
				<div class="services-list">
					<ul class="list mt-0">
						<li class=""><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
						<li class=""><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
						<li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
						<li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
						<li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
						<li><a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a></li>
						<li><a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a></li>
						<li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
						<li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="service-area ptb-3 bg-light-gray">
	<div class="container">
		<div class="col-md-12">
			<div class="tabs">
				<div class="tab">
					<button class="tab-toggle">Who Should undergo Body Hair Transplant</button>
				</div>
				<div class="content ">
					<h3 class="m-3">Who Should undergo Body Hair Transplant</h3>
					<p>We normally see patients who have a very weak donor area, have undergone repeated hair transplants, or have caught some kind of alopecia. It is important to understand that if the donor area or the grafts are weak, it is most likely to see a compromise in result. Patients with insufficient hair in the donor area are unsuitable for a normal transplant.<br />
						Hairs are extracted by using special punches that rarely leave visible marks behind. The USP of AK Clinics is that even in such cases virtually no visible linear scar is left behind from the areas where extraction is being done</p>


				</div>
				<div class="tab">
					<button class="tab-toggle">Differences between a BHT-FUE and a conventional FUE</button>
				</div>
				<div class="content">
					<h3 class="m-3">Differences between a BHT-FUE and a conventional FUE</h3>


					<p>The technique of body hair transplant is basically Follicular Unit Extraction (FUE) or one by one. The donor areas extend from just scalp hair to beard, chest, arms, legs etc. BHT is used primarily in cases where the donor area on the head is no longer suitable for the extraction of donor material or is not dense enough.
						Then we do the evolution of possibility of hair transplant from body. If the physiological requirements are met, follicular units can be extracted directly from different area of the body – chest, abdomen, arms or legs – and transplanted to the scalp.
						The grafts taken in BHT are known to have different growth cycle but a concrete study is yet to prove. Also these are different from scalp grafts as these contain only 1 hair compared to 1 to 4 hairs in scalp grafts</p>
				</div>
				<div class="tab">
					<button class="tab-toggle">Important Things in BHT</button>
				</div>
				<div class="content">
					<h3 class="m-3">Important Things in BHT</h3>
					   <ul class="text-left list-unstyled">
					<li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The experience of the surgeon in BHT. It is important because the Body hairs are quite different from Scalp Hair.</li>
						<li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The experience of the team in BHT. It is important because the direction and texture are different. So the same need to be mixed properly with scalp hair.</li>
						<li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The correct extraction of donor hairs, storage and hydration of the extracted grafts</li>
						<li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The slits that determine the direction should be given very carefully.</li>
						<li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Minimum FTR: A careful approach and the use of the right technique, damaging as few grafts as possible</li>
					</ul>

				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray">
	<?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
