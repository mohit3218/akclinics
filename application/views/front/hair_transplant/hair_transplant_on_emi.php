<?php
$enquire_for_value = 'Hair Transplant';
?>
<style>
    .col-md-6 {float: left;}
    .modal-open .modal { overflow-x: hidden;    overflow-y: auto;    padding: 10px;}
    .modal .pop-emi {margin: 25px auto;width: 100% !important;max-width: 800px; }
    .modal-content{padding: 15px !important;}
    .form-control {height: 43px !important;border-radius: 0 !important;border: none;border-bottom: solid 1px #ccc;font-size: .70em;FONT-WEIGHT: 400;LETTER-SPACING: 2PX;text-transform: uppercase;}
    .uppercase-head{font-size: 1em;text-transform: uppercase;text-align: center;margin-top: 25px;}
    .model-blue {background: #069 !important;opacity: 1 !important;}
    .model-blue h2 strong {color: #ffffff !important;font-family: geomanist_regularregular !important;}
    .modal-header .close {margin-top: -2px;color: #fff !important;opacity: 1;}
    .checkbox-inline, .radio-inline {position: relative;display: inline-block;padding-left: 20px;margin-bottom: 0;font-weight: 400;vertical-align: middle;cursor: pointer;max-width: 100%;}
    .display-hide {display: none}
    /*button.popup-class{width: 25px;border-radius: 50%;background: #fff;height: 25px;position: absolute;right: -6px;top: 15px;z-index: 999;}*/
    button.popup-class{position: absolute;top: 0px;right: 5px;}
    .error-form {display: block;text-align: center;margin: 10px 0;color: #ff0000;}
    .success-form{display: block;text-align: center;margin: 10px 0;color: #57bf06;}
    .display-hide{ display:none;}
</style>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant-on-emi/"> <span itemprop="name">Hair Transplant On EMI</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row"> 
            <div class="col-md-12">
                <div class="section-title-1 text-left">
                    <h1 class="black-tex main-title content-center margin-top-10 animated fadeInLeft delay-1s">Hair Transplant <strong> with EMI Option</strong></h1>
                    <div class="col-md-6 animated fadeInLeft delay-1s" style="float: left">
                        <figure><img class="img-fluid alignnone lazy" data-src="<?= cdn('assets/template/uploads/2016/08/') ?>akclinics-finance.png" alt="Hair Transplant on EMI" width="787" height="296"></figure>
                        <div class="clr"></div>
                        <p></p>
                    </div>
                    <div class="col-md-6 animated fadeInRight delay-1s" style="float: left">
                        <div>
                            There are several people who are facing severe hair loss and recognise the fact that 
                            they might need a 
                            <a href="<?= base_url(); ?>hair-transplant/">hair transplant surgery</a> immediately. 
                            They head to a good hair transplant surgeon and after examination,they are informed about
                            how many grafts will be required and the subsequent cost. When you find out about how 
                            much the process will <a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">cost</a>, 
                            you realise that it will eat into your savings or that you will not be able to afford it at all.
                            <p></p>
                            <p>If you come to AK Clinics, we will ensure that you are not left disappointed with 
                                anything – either the services or the final results. In addition, we will also 
                                arrange for the money that you will require for the procedure. This has become 
                                possible because our tie-up with <strong>Bajaj Finance!</strong>
                            </p>
                        </div>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 animated fadeInUp delay-1s ptb-1"> 
            When you come to AK Clinics’s <strong>Delhi, Ludhiana and Bangalore</strong> center and are 
            looking for a financial arrangement, which will allow you to get the hair transplant done 
            immediately, but pay on a monthly instalment basis, it will become possible. With Bajaj 
            Finance as our financial partners, we can ensure an<strong> EMI plan</strong> for you and 
            depending on how much you will be able to afford on a monthly basis, you can decide the 
            duration of the same.<p></p><br/>
            <p>
                We welcome you to visit AK Clinics, where our experienced doctors will give you a proper 
                examination, post which you will know how many grafts will be required and how much it 
                will eventually <a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">cost </a>
                you. Once you have the final number in hand, you will be able to spend time with a representative from 
                Bajaj Finance, who will explain the entire payment plan to you, after taking into consideration your 
                financial condition.
            </p><br/>
            <p>
                What is perhaps the greatest benefit of opting for a loan through Bajaj Finance is the fact that all loans 
                are offered at <strong>0% interest</strong>, making them all the more affordable!
            </p>
            <div class="clr"></div>
            <div class="content-center text-center">
                <a class="btn btn-warning btn-lg right-btn c" href="#" data-target="#myModal" data-toggle="modal">
                    Get Free Consultation</a>
            </div>
            <p></p>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade">
    <div class="modal-dialog pop-emi">
        <div class="modal-content" style="background:#fff !important;">
            <button class="close popup-class" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
            <div class="modal-body" style="background:none;">
                <div class="row">
                    <div class="col-md-12">
                        <form id="bajajfinance" name="bajajfinance">
                            <input style="display: none;" name="lp_url" type="hidden" value="https://akclinics.org/" />
                            <input style="display: none;" name="return_url" type="hidden" value="https://akclinics.org/thanks.html" />
                            <input style="display: none;" name="lead_source" type="hidden" value="62" />
                            <input style="display: none;" name="enquire_for" type="hidden" value="<?= $enquire_for_value; ?>" /> 
                            <input style="display: none;" name="submit_without_captcha" type="hidden" value="true" />
                            <div class="error-form display-hide">Failed !! please check required fields&#8230;.</div>
                            <div class="success-form display-hide">Thank you for submitting your details, Our patient advisor will contact you soon.</div>
                            <div class="col-md-6">
                                <div style="background:#ff9000;padding: 53px 20px;">
                                    <p class="uppercase-head" style="color:#fff; font-size:1em; letter-spacing: 2px;">Still Waiting?</p>
                                    <p class="center" style="font-size:1.5rem;text-align: center;color:#fff;margin-top: 10px !important;">Waiting won&#8217;t Help!!</p>
                                </div>
                                <p class="" style="font-size:1rem; text-align:center;">Get in touch with us today.<i class="icon icon-arrow1 smaller-lower"></i></p>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input class="form-control input-design" name="Name" required type="text" placeholder="Full Name *" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input class="form-control input-design" id="email" name="Email" required type="email" placeholder="Email*" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input class="form-control input-design" id="phone" name="Mobile" required type="tel" placeholder="Mobile*" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input class="form-control input-design" id="city" name="City" required type="text" placeholder="City*" >
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="button" name="submit" id="bajaj_submit" class="btn btn-warning  btn-outline  margin-top-10">Get Free Consultation</button>
                            </div>
                        </form>
                    </div>
                </div>
                <br /><br />
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-10 col-md-offset-2">
                            <div class="form-group">
                                <figure><img class="img-fluid" style="width:80%" src="<?= cdn('assets/template/uploads/') ?>2016/11/bajaj-text.png"></figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="<?= cdn('assets/template/uploads/') ?>2016/11/bajaj-emi.png" class="img-fluid rgt-side lazy"/> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=" bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
<!-- Modal -->
<script type="text/javascript">
    $("#bajaj_submit").click(function () {
        var a = $(this);
        var iframegtag = '<iframe src="https://akclinics.org/ga_ht.html" style="width:0;height:0;border:0; border:none;"></iframe>';
        var b = $("form#bajajfinance").serialize(),
            c = encodeURIComponent(window.location.href);
        b = b + "&page_url=" + c, $.ajax({
            url: "https://akclinics.org/submit_form.php",
            type: "post",
            data: b,
            success: function (a, b) {
                obj = JSON.parse(a), "failed" == obj.status && $(".error-form").show(), "success" == obj.status && ($(".success-form").append(iframegtag), $(".success-form").show(), $(".error-form,.form_fields").hide(), setTimeout(function () {
                    $("#myModal").modal("hide")
                }, 4e3))
            },
            error: function (a, b, c) {
                console.log(a), console.log("Details: " + b + "\nError:" + c)
            }
        })
    });
</script>