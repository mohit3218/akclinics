<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/bio-dht/"> <span itemprop="name">Bio DHT</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Bio- DHT in India</h1>
                    <div class="col-md-12">
                        <ul>
                            <li>Bio DHT is a combination of Direct Hair Transplant and <a href="https://akclinics.org/prp-treatment/" target="_blank">PRP Therapy</a> in which the hair follicles are extracted from the recipient area and implanted simultaneously in the recipient area</li>
                            <li>PRP Therapy is also given where the injection of Platelet Rich plasma in the patient’s scalp takes place</li>
                            <li>The method has been generating high rates of success</li>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 p-0">
                <!-- <a href="https://www.youtube.com/watch?v=CDn_zdj5MUM?rel=0" target="_blank">-->
                <a href="#" data-toggle="modal" data-target="#exampleModalCenter"> 
                    <div class="ht bg-img cover-bg sm-height-550px xs-height-350px" data-background="<?php echo cdn('assets/template/frontend/'); ?>img/player.png" style="background-image:url(<?php echo cdn('assets/template/frontend/'); ?>img/player.png);background-position: 1px 0 !important;
                         background-repeat: no-repeat !important; "></div>
                </a>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 bg-white pd-60 pl-sm10 pr-sm10">
                <div class="cent">
                    <div class="section-head">
                        <h4 class="text-black">This method includes the improvisation of conventional FUE.</h4>
                        <p>The doctors of AK Clinics have done a lot of research to create a combination of these two technique for the past two years for securing maximum benefit.</p>
                        <figure><img class="img-fluid lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2014/08/BIO-DHT-TM.png" alt="BIO DHT"></figure>
                        <ul>
                            <li>Bio DHT is a combination of Direct Hair Transplant and <a href="<?= base_url(); ?>prp-treatment/" target="_blank">PRP Therapy</a> in which the hair follicles are extracted from the recipient area and implanted simultaneously in the recipient area</li>
                            <li>PRP Therapy is also given where the injection of Platelet Rich plasma in the patient’s scalp takes place</li>
                            <li>The method has been generating high rates of success</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/hair_transplant_blog'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>