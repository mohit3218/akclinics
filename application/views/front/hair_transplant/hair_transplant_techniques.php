<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/hair-transplant-techniques/"> <span itemprop="name">Hair Transplant Techniques</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Hair Transplant Techniques</h1>
                </div>
                <p class="text-justify">Hair loss has been around for a really long time, but methods of transplanting hair are actually a little more recent. While the very first hair transplant procedures were conducted in the 1890s, the modern methods were established in the 1930s and by the time we reached the 1960s, there was much that was happening in this domain.
                    Today, a hair transplant is hardly considered a major surgery and a growing number of people are opting for it, to ensure they have their head of hair back. There are several reasons why people are opting for <a href="<?= base_url(); ?>hair-transplant/">hair transplant</a> and just some of them include:</p>
                <ul class="text-left list-unstyled">
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The <a href="<?= base_url(); ?>results/">results</a> are permanent, because even though the transplanted hair will fall off, the hair that replaces it, will be like normal, natural hair.</li>
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Given that your own hair is being used for the procedure, there is little you have to worry about.</li>
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Transplanted hair can be treated like normal hair, which means you can cut, colour and style the way you like.</li>
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Great solution for baldness</li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                        <li ><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title ">
                    <h2 class="black-text">Hair Transplant Method</h2>
                </div>
            </div>
        </div>
        <div class="row pt-1 text-center ">
            <div class="col-md-6">
                <h4>FUT or strip surgery</h4>
                <p>In this procedure, a strip of the scalp is removed and then hair grafts are removed from that piece. The area from where the strip has been removed will be closed using sutures or staples. The hair will be transplanted in such a way that when the regrowth happens the linear scar will get covered. This is normally the preferred method of hair<br>
                    transplant in cases, where a large recipient area needs to be covered. However, there are a few causes for concern with FUT, especially if the procedure has not been done by an experienced professional. The scar could be very visible, there could be infection in the stitches and there could be bleeding at the donor site.</p>
            </div>
            <div class="col-md-6">
                <h4>FUE Hair Transplant</h4>
                <p>This is the more advanced version of hair transplant and only those with true experience attempt this, because of its complexity. The idea behind the procedure, though, is quite a simple one – with a suction like method, individual grafts are removed from the scalp. There are no cuts involved, which means no sutures are required. his also means that there are no scars. However, this is a slightly more time consuming method, which is why not a lot of doctors prefer it. This procedure is most suited in cases, where only a small area needs to be covered with grafts or a lesser number of grafts are required. It is important to understand that hair transplant is not the only option for hair loss, because if the hair loss is minimal, then you could try other methods such as hair extensions and weaves.</p>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2 class=" black-text">Procedure Available for Hair Transplant</h2>
                </div>
            </div>
        </div>
        <div class="row pt-1 ">
            <div class="col-md-4">
                <h4>FUE Hair Transplant</h4>
                <p>Follicular Unit Extraction is a revolutionary method and in this we can assure you that you will have next to no pain. Not only will you not have to worry about any scars, but there will also be no stitches. As there are no stitches or sutures, there is minimal possibility of infections too. Once our doctors have completed their magic, you will be free to style your hair, the way you want!</p>
            </div>
            <div class="col-md-4">
                <h4>FUT Hair Transplant</h4>
                <p>FUT Hair Transplant is quite similar to FUE with only differences in the way hair are extracted. In this a strip of hair around 0.8 inches to an inch is taken from ear to ear and there on the area is sutured. This strip is slivered & dissected under sophisticated microscope into individual grafts and implanted in bald area</p>
            </div>
            <div class="col-md-4">
                <h4>Body Hair Transplant</h4>
                <p>Whether you are looking for a fuller head of hair or a lush beard, we can make it happen for you. We offer reconstruction of eyebrows, beards, moustaches and even eyelashes. We can also utilise hair from other parts of your body to complete the transplantation procedure.</p>
            </div>
            <div class="col-md-4">
                <h4>Bio-FUE<sup>TM</sup></h4>
                <p>An exclusively AK Clinics procedure, Bio FUE or follicular unit extraction allows us to extract platelet cells from your own blood and utilise the same to ensure better hair growth. We have spent immense amount of time and resources to finalise the perfect conditions for centrifugation, which increases the efficacy of the cells harvested. These cells are then carefully injected back into the scalp, and in no time, you will have a full head of hair again.</p>
            </div>


            <div class="col-md-4">
                <h4>Bio-DHT<sup>TM</sup></h4>
                <p>With this latest method, we can ensure that your own hair spends minimal time outside your body. Bio-DHT or Direct Hair Transplant ensures that the hair that has been extracted from the donor area is transplanted almost immediately. We understand how precise this procedure has to be, which is why only our experts handle the same. In addition, we also inject platelet rich plasma, as this allows for better hair growth.</p>
            </div>

            <div class="col-md-4">
                <h4>Giga-Sessions</h4>
                <p>In these we serve patients who are Grade IV or above of the baldness. We combine various techniques to achieve more than 5000 t0 6000 grafts in one go.</p>
            </div>



        </div>
    </div>
</div>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/hair_transplant_blog'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>