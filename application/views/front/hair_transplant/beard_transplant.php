<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"><span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"><span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/beard-transplant/"> <span itemprop="name">Beard Transplant</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Beard Transplant</h1>
                </div>
                <h4 class="text-gray text-left"> Men, looking to undergo a facial hair transplant, might be those who have:</h4>
                <ul class="text-left list-unstyled">
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Scars because of any trauma, surgical procedure or burns</li>
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Suffered from traction alopecia, which could have led to loss of hair</li>
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Other medical reasons such cancer or infections</li>
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Long standing conditions such as alopecia areata</li>
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Scarring caused by conditions such as folliculitis</li>
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Loss of hair, due to previously conducted <a href="<?= base_url(); ?>hair-transplant/">transplant surgery</a> or surgeries</li>
                </ul>
                <p class="text-justify">For most men, hair is a sign of masculinity and for most men, losing hair is equivalent to losing their manhood. While hair does start sprouting for most men during their teenage years, there are some who are unable to enjoy a moustache or beard even after their teenager days are over and for most of them a facial hair transplant, would be a great option.</p>
                <p class="text-justify">With the assistance of the <a href="<?= base_url(); ?>about-us/our-team/dr-kapil-dua/">best beard transplant surgeons</a>, who are now present in India, in cities such as Ludhiana and Delhi, having a full beard or moustache has now become not only a reality, but also an easy way out. Today, if you want a new beard or are only looking at rejuvenating a beard that you already had, it is possible and the procedure is quite an uncomplicated one.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Beard Transplant</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What is a Beard Transplant?</h3>
                    <p>In the simplest of terms, when a transplant is done in the area where a beard would be, it can be referred to as a beard transplant. However, a more detailed definition would be a procedure by which hair can be restored to the chin and cheek areas, where there might have been loss of hair, due to medical or physical trauma. The same procedure can also be used to make an existing beard look thicker and fuller. It is imperative though, that the transplant be done by an experienced professional, for the <a href="<?= base_url(); ?>results/">best results</a>.</p>
                </div>

                <div class="tab">
                    <button class="tab-toggle">Anatomy of the Beard Transplant</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Anatomy of the Beard Transplant</h3>
                    <p>Before you decide to undergo a beard transplant, you need to understand its anatomy. The formation of the beard starts during the puberty and it continues to grow and develop till the mid-thirties. For the purpose of a beard transplant, the face will normally be divided into two sections – the lateral and the frontal. The lateral part will include the sideburns, the cheeks and the jawline. In the frontal part, it will be the moustache and the goatee, along with the chin and lowest part of the jawline.
                        The doctor performing the <a href="<?= base_url(); ?>hair-transplant/hair-transplant-techniques/">transplant procedure</a> will need to keep in mind that the hair in the beard and moustache are often of a different density. The number of grafts required will also vary from person to person, but on a general basis 2000 to 2500 grafts are required</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Who Need Beard Transplant?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Who might need a beard transplant?</h3>
                    <p>In most cases, a hair transplant is required by people who have lost hair due to genetic, medical, lifestyle or trauma related reasons. The same is the case with beard transplants as well – they are required by people who might have lost hair in the beard area due to a genetic condition, illness, certain medications, accidents or any other such similar reasons. By undergoing a beard transplant, they can enjoy a full beard once again or in some cases, for the very first time.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Ideal Candidate?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Who Would Prove to be an Ideal Candidate?</h3>
                    <p>While there might be several people who would want to get a beard transplant done, not all of them will prove to be worthy candidates for the same. There are some requirements, rather criteria, which need to be met for a person to be considered eligible for a beard transplant. For starters, the person has to have realistic expectations and should not expect a full beard, the very same day as the procedure. It is important to remember that any hair transplant will take time to settle in. the person needs to be in sound medical and mental health and the decision to undergo a transplant needs to be a genuine one and not one that has been taken under peer or any other pressures.</p>
                    <p>The factors that a doctor will keep in mind, before considering a person worthy enough for a beard transplant would include whether the beard has obvious spots or patches, which need to be covered for the face to look good. There also has to be an above average donor area, which could include the side and the back of the head. A thorough physical exam will allow an experienced doctor to judge whether you are an ideal candidate for the procedure or not.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">What are the things that you need to know, before getting a beard transplant?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What are the things that you need to know, before getting a beard transplant?</h3>
                    <p>You need to accept that this is a surgical procedure, which means that there could be some risks and complications involved and it would be best that you talk to your doctor about the same beforehand. Being a surgical procedure, it will also not come cheap and in most cases, these procedures are considered cosmetic, which means that insurance will not cover it.</p>
                    <p>It is possible to design a beard, but this will be possible only after a close examination of your beard region and your donor area. The texture of the donor hair will play an important role in creating a natural looking beard. And no matter how well the transplant has been done, the hair will fall out within a few days. However, this will lead the way to a fuller, healthier and more permanent beard.</p>
                    <p>The procedure will not take more than a few hours, but you will have to take a few days off from work, because you will have to take immense care of your newly transplanted beard. This will also mean that you will not be able to shave for close to 10 days, no matter what your beard looks like.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title text-center">
                    <h2 class=" black-text">How is a beard transplant done?</h2>
                    <p>The procedure for a beard transplant is reasonably simplistic – the area where the proposed beard has to grow will be demarcated in advance. This will provide the doctor a clear area to work within, making their task easier and laying foundation for a more natural looking beard. The next step will be to take hair grafts from the back or side of your head and then transplanted into your cheeks and chin. Normally, facial hair transplant procedures, especially beard transplants are done using the <a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">follicular unit extraction or FUE methods</a>, which means there are no scars, no bleeding and the healing process is much faster.</p>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-lg-4 col-md-6 col-sm-12 "> 
                <h4>What are the pre-op investigations for a beard transplant?</h4>

                <p>Your face and donor area on the head will be examined first, and the design of the beard will be decided well in advance. Your doctor will discuss the density and design you are looking for, in great detail and the final plan will be one that is acceptable to you. You might also be asked to undergo a few basic blood tests and physical checks, just to ensure that you are in good health.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 "> 
                <h4>What are the possible side effects to a beard transplant?</h4>

                <p>What are the points that need to be kept in mind after the beard transplant has been completed?
                    If the procedure has been done by a professional and an experienced doctor, there should be no side effects, however, there is a chance that the final appearance of the beard might not be in exact accordance to what you had expected. In very rare cases, the transplant might not take root at all, or there might be infections.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 "> 
                <h4>What are the points that need to be kept in mind after the beard transplant has been completed?</h4>

                <p>You will be given a proper list of <a href="<?= base_url(); ?>hair-transplant/post-operative-care/">dos and don’ts</a>, which you will be expected to follow in the days after the procedure has been completed. For instance, you will be asked not to shave for at least 10 to 15 days and even washing your face will have to be done with extreme care. The hair that has been implanted will fall off, but this is a natural process and hair will grow back in the days to come.</p>
            </div>
        </div>
    </div>
</div>
<div class="service-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 bg-dark pd-60 pl-sm10 pr-sm10">
                <div class="cent">
                    <div class="section-head">
                        <h4 class="text-muted">Why get a beard transplant at AK Clinics?</h4>
                    </div>
                    <p class="pb-20 text-white"> There are innumerable reasons why you should get your beard transplant done at AK Clinics and here some of the most important ones:</p>
                    <ul class="text-left list-unstyled text-white">
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The team of doctors here is not only extremely experienced, but also highly professional, which means that your procedure will be completed in the manner that will have been explained to you in advance.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The staff that assists the doctor performing the procedure is equally well trained and experienced, which means that the chances of something going wrong during your procedure are next to nil.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> All beard and facial transplants are done via the FUE method, which means there is minimal hair loss, next to no pain and the healing process is extremely fast.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The operating or procedure rooms are equipped with the most modern and efficient devices and tools, allowing the doctors greater speed and more precision.</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 p-0">
                <!-- <a href="https://www.youtube.com/watch?v=CDn_zdj5MUM?rel=0" target="_blank">-->
                <a href="#" data-toggle="modal" data-target="#exampleModalCenter"> 
                    <div class="ht bg-img cover-bg sm-height-550px xs-height-350px" data-background="<?php echo cdn('assets/template/frontend/'); ?>img/player.png" style="background-image:url(<?php echo cdn('assets/template/frontend/'); ?>img/player.png);background-position: 0px 0 !important;
                         background-repeat: no-repeat !important; ">
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="service-area bg-light-gray">
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>