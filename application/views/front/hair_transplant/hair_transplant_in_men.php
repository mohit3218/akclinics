<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/hair-transplant-in-men/"> <span itemprop="name">Hair Transplant In Men</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Hair Transplant in Men</h1>
                </div>
                <p>Losing hair is never easy and while most men would think that this is not something that would happen to them, <a href="<?= base_url(); ?>hair-loss/">hair loss</a> can happen to anyone. We all lose some hair each day, and 60-100 strands a day is considered normal. However, when the number of strands goes above and beyond this limit, there is trouble in paradise and this is where hair loss starts.<br />
                    However, with the advances in modern science, growing a head full of hair is now possible and actually quite uncomplicated. This can happen in the form of a hair transplant!.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12 ptb-1">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">What is Hair Transplant in Men?</button>
                </div>
                <div class="content "> <h3 class="m-3">What is Hair Transplant in Men?</h3>
                    <p>In the past few years, the demand for hair transplants has increase manifold, because people are now aware that a hair transplant is not that complicated procedure and neither is it a very expensive one. In addition, when done properly, the transplant can give you results that look natural and will be permanent too.<br>
                        In the simplest of terms, a hair transplant is a procedure, wherein hair is taken from the scalp or any other part of the body and then transplanted into those parts, where the hair loss has taken place. The bald spots get covered and over a short period of time, the <a href="<?= base_url(); ?>blog/will-the-hair-on-the-donor-area-grow-back-after-an-fut/" target="_blank">donor site also grows back</a> hair, making the head look healthy and <strong>‘hairy’!</strong></p>
                    <h4>Here are the scenarios, wherein a transplant can be done successfully in men:</h4> 
                    <ul >
                        <li><a href="<?= base_url(); ?>hair-loss/hair-loss-women/">Female pattern baldness</a> that has not shown signs of improvement, even with regular medication</li>
                        <li><a href="<?= base_url(); ?>blog/want-know-scarring-alopecia/">Scarring alopecia</a> that has been stable for a minimum of twelve months</li>
                        <li>Alopecia that is a results of a trauma, burn or surgery</li>
                        <li><a href="<?= base_url(); ?>blog/traction-alopecia-major-problem-hairs/">Traction alopecia</a></li>
                        <li>Restoration of eyebrows and eyelashes</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">How is Hair Transplant in Men done?</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">How is Hair Transplant in Men done?</h3>
                    <p>On a superficial level, the <a href="<?= base_url(); ?>hair-transplant">process of a hair transplant</a> is incredibly simple – hair, or follicular units or grafts are extracted from the donor area, and these are then implanted into the area where there is balding.<br>
                        Before a transplant procedure can be done, it has to be established that the candidate is suited for a transplant or not. Depending on the grade of baldness, the quality and the extent of the donor area, the density and thickness of the hair in the recipient area and the results of the hair analysis test, our doctors will be able to decide whether a person can undergo a hair transplant or not. These factors will also help them decide which kind of procedure will be most suited.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle ">Steps in Hair Transplant for Men</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Steps in Hair Transplant for Men</h3>
                    <h4>Extraction of grafts from the donor area</h4>
                    <p>The idea is that the grafts should be extracted with minimum transection, and there should be no damage to the donor area. The quality and the quantity of grafts are both important and in the hands of an experienced doctor, both should be near perfect.</p>
                    <h4>Transplanting the grafts in the recipient area</h4>
                    <p>The grafts that have been extracted are meticulously placed in the recipient area, where small holes or slits are created. The experience of the doctor will shine through, in the manner in which the grafts are placed. The direction as well the angle in which the hair is placed are crucial and will determine how natural the hair eventually looks. The density will also make a difference in the final result.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">What are the advantages?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What are the advantages?</h3>
                    <p>If one were to ask, what the advantages of getting a hair transplant were, most people would say getting their lost confidence back. But truth be told there are several other advantages too, such as:</p>
                    <ul>
                        <li>With a good hair transplant, you can be sure that your hair will look natural.</li>
                        <li>The hair that will be transplanted will be permanent and you will not have to struggle with bald spots anymore.</li>
                        <li>The donor area will always be a permanent zone, which means that even if hair is extracted from there, hair will grow back.</li>
                        <li>Hair transplant these days is so strong that you can do anything, including dance, play and swim, without a worry in the world.</li>
                        <li>Once the hair has grown to a certain length, you will be able to cut and style it in a manner that you like. As a matter of fact, if you have undergone an FUE, you can wear your hair as short as you want.</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Why get it done at AK Clinics?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Why get it done at AK Clinics?</h3>
                    <p>The fact that we are the leaders in this field should be enough to assist you in making the decision, but here are a few more that will push you in the right direction:</p>
                    <ul>
                        <li>Our team of surgeons have decades of experience, which means that the chances of anything going wrong with your transplant are minimal.</li>
                        <li>While our doctors are the best at what they do, they are able to excel each time, because their support staff is just as good.</li>
                        <li>The operating rooms are state of the art and extremely hygienic.</li>
                        <li>We have equipment and tools that modern and extremely advanced.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>