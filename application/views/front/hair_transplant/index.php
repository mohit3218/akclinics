<style>.background-one {background:#f0ebd8;opacity:0.9}
    .custom-padding{padding-top:60px !important;padding-right:40px !important;	padding-bottom: 0px !important;	padding-left: 0px !important;}
    .card {	min-height: 260px;}
    .card-text {min-height: 60px;}
    .services-list a{margin:5px 0 8px;position: relative;padding-left:30px;background-color:#f8f8f8;border: 1px solid #f1f1f1;display: inline-block; padding: 10px; width: 100%;}
    @media (min-width:320px) and (max-width:640px) {.card {min-height: 160px !important;}.slick-list.draggable{height: auto;}.carousel-indicators {display: none;}}
</style>

<div itemscope itemtype="https://schema.org/MedicalProcedure">
    <?php $this->load->view('front/partials/main_hair_transplant_banner'); ?>
    <div class="header-banner-content-area light-orange">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left m-2 text-dark" >
                    <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                            <meta itemprop="position" content="1" />
                        </li>
                        <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a>
                            <meta itemprop="position" content="2" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="service-area ptb-3">
        <div class="container ">
            <div class="row">
                <div class="col-md-9">
                    <div class="section-title-1 text-left">
                        <h1 class="black-text" itemprop="name">Hair Transplant Surgery</h1>
                        <p>Hair Transplant Surgery is a gold standard for treatment of <span itemprop="indication">Male Pattern Baldness and Female Androgenetic Alopecia</span>. In addition to scalp hair restoration, hair transplants is widely used for restoring the eyebrows, facial hair – beard and moustache, and hair lost due to trauma or other forms of alopecia. <br><span itemprop="description">Hair transplantation is a surgical procedure. In this procedure, the surgeon takes hair from the back of the scalp and transplants them in the bald area. Starting from old punch graft technique, hair transplant has evolved to advanced techniques like Bio-FUE™ and Robotic Surgery. This is the reason that more and more number of people are today opting for this surgery worldwide. </span> <br> According to <strong>‘The Hair Society’,</strong> 35 million men and 21 million women suffer from the hair loss. But this procedure is more popular among the men; as per ISHRS’s 2017 census, 85.7% of procedures were performed on men, while 14.3% were performed on women. India today is seeing increased medical tourism because of quality procedure at an affordable cost. Along with India, hair transplant has also become very popular in Turkey.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="services-list more-link-1">
                        <a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a>
                        <a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a>
                        <a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a>
                        <a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a>
                        <a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a>
                        <a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a>
                        <a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a>
                        <a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a>
                        <a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a>           
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="service-area ptb-3 bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h2 class="text-black">How is each hair transplant step different at AK Clinics?</h2>
                        <span>The general principle of the procedure is based on the fact that the hair that originates in the permanent area or Safe donor area’ are not affected by the DHT. Since, the genetic vulnerability to the DHT residues in the hair follicle, the implanted hair will be resistant to the effect of DHT and will continue to grow. At AK Clinics, we have improvised each and every steps of the hair transplant to give results in most patients. Though this is a medical procedure and has its limitations but still it is really rewarding at AK Clinics. Following is how we have developed our own technique Bio-FUE™ and improvised the conventional technique.</span> </div>
                </div>
            </div>
            <div class="regular slider pt-3">
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img data-src="<?= cdn('assets/template/frontend/img/'); ?>grade-sclap.png" class="card-img-top img-fluid lazy" alt="Planning & Hairline Design for hair transplantation">
                        <div class="card-body text-center"> <strong>Planning & Hairline Design</strong>
                            <p class="card-text addReadMore showlesscontent"> <span itemprop="procedureType">Surgical</span> Hair Restoration is both art and science. 
                                The intention is to use minimum grafts to give natural look keeping in mind that the patient 
                                should age gracefully. Before surgery, our surgeon discusses and does the necessary planning to 
                                create the hairline design with AK Clinics’ methodology. The plan is always realistic and closest 
                                to patient’s need or expectation. </p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>donor-area-trimming-1.png" alt="Donor Area Trimming & Local Anesthesia ">
                        <div class="card-body  text-center"> <strong>Donor Area Trimming & Local Anesthesia</strong>
                            <p class="card-text addReadMore showlesscontent">Our technique ensures very comfortable experience during this process. On the day of surgery, donor area is trimmed properly for graft harvesting. At AK Clinics, local anesthesia is virtually pain free. First, a numbing cream or gel is applied to minimize the pain while administering the anesthesia. We use nerve blocks along with anesthesia through thinnest needles to give comfort to the patient. Further the use of vibrator along further reduces the pain sensation.</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>Recipient-Site-Creation.png" alt="Slit making in Hair Transplant Surgery">
                        <div class="card-body  text-center"> <strong>Recipient Site Creation or Slit making</strong>
                            <p class="card-text  addReadMore showlesscontent">This is a very critical procedure and this decides the final direction and density. Our Surgeons perform the slit-makingas the 1st step. This gives 2 benefits, one it minimizes the out of body time of grafts and second the planning is fresh in the mind – so the extraction is planned accordingly. We use customized CTS (cut to size) blades that ensure proper depth reducing the trauma to underlying vessels.</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>Follicular-Extraction-ht.png" alt="Follicular Extraction during Hair Transplant">
                        <div class="card-body  text-center"> <strong>Follicular Extraction</strong>
                            <p class="card-text  addReadMore showlesscontent">After the slit making, hair follicles are extracted using ‘FLAT Punch’ designed by our doctors. With the use of Flat Punch, there will be minimal graft wastage. Grafts are extracted from the safe donor area only and the follicles are kept 100% hydrated. This ensures that minimal wastage of donor area happens and you can undergo further sessions</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>Graft-Counting.jpeg" alt="Graft Counting & Consent form ">
                        <div class="card-body  text-center"> <strong>Graft Counting & Consent form</strong>
                            <p class="card-text  addReadMore showlesscontent">After the extraction, the patient asked to count the grafts. For ease the arrangement is made in row-column form.There after the patient signs the consent that he/she has counted the grafts. After this,the further implantation process is initiated. At AK Clinics, only 100% viable grafts are counted. The partially transected grafts may be implanted but not counted.</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>Implantation-AK-HT.png" alt="Implantation of grafts">
                        <div class="card-body  text-center"> <strong>Implantation</strong>
                            <p class="card-text  addReadMore showlesscontent">For graft implantation in pre-made slits, our technicians use double forceps and follow the FIRST-OUT-FIRST-IN methodology. A technician continually sprays the saline solution to keep the grafts hydrated. The use of double forceps or dilator ensures that there is no mechanical trauma to the grafts.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="service-area ptb-3">
        <div class="container">
            <div class="col-md-12">
                <div class="tabs">
                    <div class="tab">
                        <button class="tab-toggle">What are the types of Hair Transplant?</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">What are the types of Hair Transplant?</h3>
                        <span>With the advancements, hair transplant techniques have evolved. Now, not only the hair can be transplanted on scalp only, they can be transplanted on different parts of the body. The different types of hair transplant are;</span>
                        <div class="row text-center pt-1">
                            <div class="col-md-6"> <a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a>
                                <p> FUT (Follicular Unit Transplantation), also known as strip surgery, is an ideal choice for patients with higher grade of baldness. In this procedure, the hair bearing skin is removed by using a scalpel and the resultant gap is sutured together. This strip is divided in grafts and then implanted on the recipient area.</p>
                            </div>
                            <div class="col-md-6"> <a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a>
                                <p>FUE (Follicular Unit Extraction) is a more sophisticated method. In this method individual grafts are taken out one by one and placed in the bald area. This is the reason there is no linear scar, but just only tiny, inconspicuous dot-like scars. Though unlike the popular myth, FUE is not a scar-less surgery.</p>
                            </div>
                            <div class="clr pt-1"></div>
                            <div class="col-md-6"> <a href="<?= base_url(); ?>hair-transplant/bio-fue/">Bio-FUE <sup>TM</sup> Hair Transplant</a>
                                <p>Bio-FUE™ is an advanced version of FUE hair transplant developed by AK Clinics team of doctors. It is a long-term customized plan for hair restoration using indigenous equipment like ‘Flat Punch’. Also, this takes into account the fact that every patient is different, so needs different plan.</p>
                            </div>
                            <div class="col-md-6"> <a href="<?= base_url(); ?>hair-transplant/bio-dht/">Bio-DHT <sup>TM </sup>Hair Transplant</a>
                                <p> Bio-DHT™ is a further improvement of Bio-FUE™ in which the extraction and implantation of grafts happen at the same time. This helps in reducing the out-of-body time of grafts to a minimum and hence improves the graft survival. This is also being currently studied by the ISHRS FUE Research Committee.</p>
                            </div>
                            <div class="clr pt-1"></div>
                            <div class="col-md-6"> <a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a>
                                <p> Facial Hair Transplant is carried out for the restoration of areas like eyebrows, eyelashes, mustache, beard and sideburns. The hair is generally taken from the back of the head through FUE or FUT and transplanted on the face. Though this procedure very safe but requires very high skill due to the importance of hair direction.</p>
                            </div>
                            <div class="col-md-6"> <a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Hair Transplant</a>
                                <p> Beard transplant is a type of facial hair transplant but is increasingly becoming more and more popular among the youth. Inadequate facial hair becomes a source of embarrassment as this is generally linked to masculinity. The procedure though simple needs a highly experienced surgeon to give a natural look.</p>
                            </div>
                            <div class="clr pt-1"></div>
                            <div class="col-md-6"> <a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a>
                                <p> Body hair transplant is used in patients with weak donor area. There are many areas of the body from which grafts can be harvested like chest, back, pubis, armpits etc but the best source is beard grafts. The body hair is usually give good results when used in conjunction with the scalp hair.</p>
                            </div>
                            <div class="col-md-6"> <a href="<?= base_url(); ?>hair-transplant/giga-session/">Giga Sessions at AK Clinics</a>
                                <p> A giga-session is a very careful combination of a follicular unit transplant and a follicular unit extraction. This is actually the best bet when you have the requirement for more than 5000 grafts. With our expertise, we can extract close to 4000-4500 grafts from the scalp alone with this technique and the rest by Body Hair Transplant.</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">Suitable candidates for Hair Transplant?</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">Who are the suitable candidates for Hair Transplant?</h3>
                        <span>As a general rule, when a clear-cut baldness pattern has established and patient has a healthy donor area, hair transplant can be planned. But it is important to establish the reasons for hair loss. In many-a-cases the procedure may not be advisable or advisable in due course of time only or even after a pre-treatment</span>
                        <p>During the consultation, our experienced counselors and surgeons diagnose the condition & help you to reach the decision in planning the hair restoration journey. Following are the few key factors which makes a person right candidate for the hair transplant:</p>
                        <ul>
                            <li>A diagnosis of the reason for hair loss that is amenable to the surgeon for hair transplantation</li>
                            <li>Adequate hair on donor area to satisfy current and future needs</li>
                            <li>Realistic expectations about surgical hair restoration can accomplish</li>
                        </ul>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">Hair growth after the surgery</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">Hair growth after the surgery</h3>
                        <span>How much time it will take to regrow? How many hair will grow every month? These are the common questions that people seek for after the surgery. It will take approximately 6-12 months for the new transplanted hair to grow. Following is a general growth illustration after the procedure.</span>
                        <figure><img class="img-fluid rounded lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/11/ht-growth-AK-clinics.png" alt="Chart for hair re-growth after surgery"></figure>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">Post-operative Instructions</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">Post-operative Instructions</h3>
                        <span>The time after your transplant procedure is significantly important. It is vital that to understand the goals, complications or limitations of the hair transplant. The surgeon & team will explain you the complete schedule during the consultation& after the procedure. He may ask you to avoid:</span>
                        <ul>
                            <li itemprop="followup">wearing hair piece or cap for few weeks</li>
                            <li itemprop="followup">hair styling tools or products</li>
                            <li itemprop="followup">heavy lifting or workout</li>
                            <li itemprop="followup">harsh shampoos etc.</li>
                        </ul>
                        <span>Your adherence to the <a href="<?= base_url(); ?>hair-transplant/post-operative-care/">post op instructions after hair transplant</a> will help you to assure the best results after the surgery. </span> </div>
                    <div class="tab">
                        <button class="tab-toggle">Advantages of hair transplant?</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">What are the advantages of hair transplant?</h3>
                        <span>When you decide to go for a surgical hair restoration, it is important to know the key advantages. At AK Clinics, our motive is that every transplanted hair should grow. The most important thing our doctors have achieved is the level of expertise to provide great aesthetic look with minimum possible number of grafts. This hugely helps in saving the donor area.</span>
                        <figure class="ptb-3"><img class="img-fluid lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>hair-transplant-advantage.png" alt="hair transplant advantage"></figure>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6"><strong>Permanent Results</strong>
                                    <p>It is the only known permanent solution of baldness. During the hair transplant, our physicians only transplant the healthy and viable follicles from the safe donor area only. You can style, color or cut them as you want. </p>
                                </div>
                                <div class="col-md-6"><strong>Natural Results</strong>
                                    <p>Hair transplant gives you the natural results as our doctors use the next generation Bio-FUE™ technique. They artistically transplant hair, complementing to the natural growth of the patient. This will give you healthy and natural results.</p>
                                </div>
                                <div class="clr pt-1"></div>
                                <div class="col-md-6"><strong>Negligible Downtime</strong>
                                    <p>At AK Clinics, hair transplant surgery is simple and outpatient procedure. You can go back to your home at the same day of surgery as the procedure is minimally invasive. Please go through post-operative instructions to know in detail about the recovery period.</p>
                                </div>
                                <div class="col-md-6"><strong>Safe Procedure</strong>
                                    <p>Hair transplant surgery, if performed by an experienced surgeon, is generally a safe procedure. At the same time it is a surgical procedure and has the inherent risks that are associated with any minimally invasive surgeries.	Our team of highly experienced surgeons takes care of the patient and procedure at every step so that there is minimal risk of any infection after the hair transplant surgery.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="service-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 p-0"> 
                    <!-- <a href="https://www.youtube.com/watch?v=CDn_zdj5MUM?rel=0" target="_blank">--> 
                    <a href="#" data-toggle="modal" data-target="#exampleModalCenter">
                        <div class="ht bg-img cover-bg sm-height-550px xs-height-350px" data-background="<?php echo cdn('assets/template/frontend/'); ?>img/player.png" style="background-image:url(<?php echo cdn('assets/template/frontend/'); ?>img/player.png);background-position: 0px 0 !important;
                             background-repeat: no-repeat !important; ">
                        </div>
                    </a> 
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 bg-dark pd-60 pl-sm10 pr-sm10">
                    <div class="cent">
                        <div class="section-head text-white">
                            <h2 class="">Why to choose AK Clinics for Hair Transplant Surgery?</h2>
                            <cite>One who is undergoing hair transplant surgery always looks for the best treatment. Our surgeons have experience of successfully extracting and implanting millions of grafts procedures. We thrive to provide highest quality results at affordable cost at our exclusive centers across India. Following factors makes AK Clinics unique from other hair transplant clinics;</cite> </div>
                        <ul  class="text-white">
                            <li>Our trandemarked Bio-FUE<sup>™</sup> procedure</li>
                            <li>Great Resuts since 2007</li>
                            <li>Experienced Surgeons &amp; team</li>
                            <li>Low FTR (below 3% in FUE hair transplant)</li>
                            <li>Minimum out-of-body-time of grafts</li>
                            <li>Grafts given by counting</li>
                            <li>Ultra hygienic OTs</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?= cdn('assets/template/frontend/img/'); ?>dr-profile-cover.jpg" style="background-image: url(&quot;<?= cdn('assets/template/frontend/img/'); ?>dr-profile-cover.jpg&quot;); ">
        <div class="container pt-0 pb-0">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-7 sm-height-auto">
                        <div class="p-30 bg-deep-transparent " style="height:500px; overflow-y:scroll;">
                            <h4 class="title mt-0 line-bottom line-height-2 text-black-222">Hair Transplant & <span class="text-theme-colored">AK Clinics</span></h4>
                            <cite>AK Clinics was founded with the goal of transformation of looks and the purpose of existence is to provide excellent treatments at an affordable cost. Our experienced doctors always work to satisfy and to fulfill the patient’s expectations. From the traditional FUE technique to their owned technique – Bio-FUE™, they always strive for giving good and natural results to their patients.</cite>
                            <div class="items">
                                <div class="icon-box"> <span class="icon-box-title">2007</span>
                                    <p class="text-black-333">Visit to Israel for Hair transplant training & started FUE Hair Transplant in India</p>
                                </div>
                            </div>
                            <div class="items">
                                <div class="icon-box"> <span class="icon-box-title">2008</span>
                                    <p class="text-black-333">Hair Transplant & Aesthetic Dermatology Centre by the name of AK Clinics on 14th August, 2008 in Ludhiana</p>
                                </div>
                            </div>
                            <div class="items">
                                <div class="icon-box"> <span class="icon-box-title">2010</span>
                                    <p class="text-black-333">Motorized FUE and Body Hair Transplant</p>
                                </div>
                            </div>
                            <div class="items">
                                <div class="icon-box"> <span class="icon-box-title">2011</span>
                                    <p class="text-black-333">Set up their own Hair Transplant & Aesthetic Dermatology Centre in <a href="<?= base_url(); ?>locations/hair-transplant-delhi/">Delhi</a></p>
                                </div>
                            </div>
                            <div class="items">
                                <div class="icon-box"> <span class="icon-box-title">2013-14</span>
                                    <p class="text-black-333">Developed Bio-FUE™ & Bio-DHT™ & Developed “FLAT Punch”</p>
                                </div>
                            </div>
                            <div class="items">
                                <div class="icon-box"> <span class="icon-box-title">2015</span>
                                    <p class="text-black-333">AK Clinics in Bangalore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="service-area ptb-3">
        <div class="container-fluid text-center">
            <div class="col-md-12">
                <div class="row">
                    <div class="section-title text-center">
                        <h2 class="text-black">Historical Evolution of Hair Transplantation</h2>
                    </div>
                    <cite>The development up to today’s latest hair transplant technique was started way back in the 1822 when Professor Dom Unger along with his student Diffenbach performed experimental surgery in human and animals of moving hair from area to the other. There are few mentions of people trying the same in the following decades.  The modern surgical technique was first developed in Japan in 1930s but that was largely for trauma corrections. Dr. S. Okuda, a Japanese dermatologist, is known to have moved hair bearing skin to hairless areas. Hair Transplantation as treatment for androgenetic alopecia only became popular in 1959 with a paper of Norman Orentreich, MD. </cite> </div>
            </div>
            <div class="col-md-12 fun-facts">
                <div class="row">
                    <div class="col-md-2 offset-md-1 fun-col"> <strong class="big-font">1822</strong>
                        <p> Dieffenbach described the experimental hair transplant surgery</p>
                    </div>
                    <div class="col-md-2 fun-col"> <strong class="big-font">1959</strong>
                        <p> Norman Orentreich experiment with donor grafts to balding area-Hair plugs</p>
                    </div>
                    <div class="col-md-2 fun-col"> <strong class="big-font">1995</strong>
                        <p> FUT (Follicular Unit Transplantation) hair transplant technique</p>
                    </div>
                    <div class="col-md-2 fun-col"> <strong class="big-font"> 2002</strong>
                        <p> FUE (Follicular Unit Extraction) hair transplant technique introduced</p>
                    </div>
                    <div class="col-md-2 fun-col"> <strong class="big-font"> 2011</strong>
                        <p> ARTAS system became the first FDA approved hair transplant robot</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="service-area ptb-3 bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12" id="testimonails">
                    <div class="col-md-12 text-center">
                        <h2>Testimonials</h2>
                    </div>
                    <div id="carouselExampleIndicators" class="carousel slide text-white" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
                        </ol>
                        <div class="carousel-inner mt-4  black-text">
                            <div class="carousel-item text-center">
                                <div class="img-box p-1 border rounded-circle m-auto"> 
                                <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>raj-n.jpg" alt="First slide"> </div>
                                <strong class="mt-4 text-uppercase">Raj Jaswal</strong>
                                <p class="m-0 pt-2"> I started facing hair loss at the age of 26 but it was not very noticeable. But at 29 
                                    the hair loss progressed very quickly and crown almost became empty. And further my 
                                    hair line receded quickly. Now at the age of 31 I started looking very old. 
                                    I tried many medicines like homeopathic etc etc but nothing seems to work. 
                                    Then I started searching for my options and finally decided for a hair transplant. </p>
                            </div>
                            <div class="carousel-item text-center">
                                <div class="img-box p-1 border rounded-circle m-auto"> 
                                <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>supal.jpg " alt="First slide"> </div>
                                <strong class="mt-4 text-uppercase">Vikalp Gaur</strong>
                                <p class="m-0 pt-2"> I can see good results post-transplant. Overall good. As suggested, I may go for 
                                    further transplant for crown area. I am currently taking 6 months cyclical therapy 
                                    again to stop hair fall. Then will take the decision accordingly. I can say I would 
                                    prefer someone for A K Clinic. As it is giving person the way to live more happily. </p>
                            </div>
                            <div class="carousel-item text-center active">
                                <div class="img-box p-1 border rounded-circle m-auto"> 
                                <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="First slide"> </div>
                                <strong class="mt-4 text-uppercase">Rahul Bhatia</strong> 
                                <!--<h6 class="text-white ">Seo Analyst</h6>-->
                                <p class="m-0 pt-2"> Had a hair transplant at AK clinic under care of Doc. KAPIL DUA.And after treatment it 
                                    has been 3 month now and I have very much inproved growing hair. He is best surgeon 
                                    ever seen and very experienced in this art. They also provided very good atmosphere 
                                    while surgery. With his experience he gave very confident to his patients while surgery. </p>
                            </div>
                            <div class="carousel-item text-center">
                                <div class="img-box p-1 border rounded-circle m-auto"> 
                                <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="First slide"> </div>
                                <strong class="mt-4 text-uppercase">Suresh</strong> 
                                <!--<h6 class="text-white ">Seo Analyst</h6>-->
                                <p class="m-0 pt-2 text-black"> After talking to Dr Kapil I have decided to go for a hair transplant surgery which turned
                                    out to be very good. Total of 6000 grafts were transplanted. The density was medium 
                                    density. The procedure was FUE without any scar. The procedure was by done both Dr 
                                    Kapil and Dr Aman. I strongly recommend anyone who is looking for a FUE surgery in India. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6  col-sm-12 video-part">
                    <div class="col-md-12 text-center">
                        <h2>Video Results</h2>
                    </div>
                    <a href="<?= base_url(); ?>results/videos/" target="_blank" class="media-lightbox">
                        <div class="embed-responsive embed-responsive-16by9">
                            <div class="embed-responsive-item element-cover-bg"> <span class="play-wrapper"> <i class="fa fa-play-circle-o fa-4x"></i> </span> </div>
                        </div>
                    </a> 
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" style="padding:0px !important;">
                <div class="modal-body">
                    <div class="embed-responsive embed-responsive-16by9">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="    top: 20px;
                                z-index: 5; position: relative;height: 25px;idth: 25px;"> <span aria-hidden="true">&times;</span> </button>
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CDn_zdj5MUM" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('front/partials/hair_transplant_blog'); ?>
    <div class=" bg-light-gray">
        <?php $this->load->view('front/partials/location_block'); ?>
    </div>
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
    <span itemprop="code" content="Z41.0"></span> <span itemprop="code" content="007205"></span> <span itemprop="sameas" content="http://en.wikipedia.org/wiki/Hair_transplantation"></span> <span itemprop="sameas" content="http://www.webmd.com/skin-problems-and-treatments/hair-loss/men-hair-loss-17/hair-transplants"></span> <span itemprop="sameas" content="http://apps.who.int/classifications/icd10/browse/2010/en#Z41.0"></span> <span itemprop="sameas" content="http://www.nlm.nih.gov/medlineplus/ency/article/007205.htm"></span> </div>
    <form class="more-link">
    <select  onchange="location = this.value;"  class="custom-select">
        <option value="More" selected> - Related links - </option>
        <option value="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</option>
        <option value="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</option>
        <option value="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</option>
        <option value="<?= base_url(); ?>hair-transplant/beard-transplant/" >Beard Transplant</option>
        <option value="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</option>
        <option value="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</option>
        <option value="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</option>
        <option value="<?= base_url(); ?>hair-transplant/post-operative-care/" >Post Op Instructions</option>
        <option value="<?= base_url(); ?>hair-transplant/faqs/" >Faq's</option>
    </select>
</form>
<script type="text/javascript">
    $(document).on('ready', function () {
        $(".regular").slick({
            dots: true,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
</script> 
