<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/"> <span itemprop="name">FUT Hair Transplant</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">FUT Hair Transplant</h1>
                </div>
                <p>FUT or follicular unit transplant is also known as strip surgery. This is one of the most commonly used procedures for hair transplant and is the ideal choice for people, who have a large area that has gone bald. In this procedure, the hair bearing skin is removed by using a scalpel from the back of a person’s head and the gap at the back of the person’s head is sutured together. The strip then gets divided in grafts consisting of one or two follicles and then implanted in the same way as FUE.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 p-0">
                <!-- <a href="https://www.youtube.com/watch?v=CDn_zdj5MUM?rel=0" target="_blank">-->
                <a href="#" data-toggle="modal" data-target="#exampleModalCenter"> 
                    <div class="ht bg-img cover-bg sm-height-550px xs-height-350px" data-background="<?php echo cdn('assets/template/frontend/'); ?>img/player.png" style="background-image:url(<?php echo cdn('assets/template/frontend/'); ?>img/player.png);background-position: 1px 0 !important;
                         background-repeat: no-repeat !important; "></div>
                </a>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 bg-dark pd-60 pl-sm10 pr-sm10">
                <div class="cent">
                    <div class="section-head">
                        <h4 class="text-muted">FUT Hair Transplant Procedure</h4>
                    </div>
                    <p class="pb-20 text-white">We read a lot of things on FUT & FUE on internet but when it comes to the procedure, there are a rare few who are truly aware of what happens during the procedure.
                        In FUT, an entire strip of hair is removed from the scalp’s donor area and then the dissection takes place under a microscope. The area from where the strip is taken is stitched carefully, which leaves a linear scar on the scalp. The intention of a follicular unit transplantation is to take hair from a donor area, and transplant it into the area where the balding is most pronounced. Normally, small follicular units are harvested, and each unit contains anywhere between a single to four hairs.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title ">
                    <h2 class="black-text">FUT Hair Transplant Steps</h2>
                </div>
                <p>Generally, on the day of surgery, you are asked to come a few hours before the surgery is schedule.This time will allow the staff to guide you through all that will happen and also complete the requisite paperwork including the informed consent form. This is also the last time to talk to your FUT surgeon and clear all your doubts. The surgeon will demarcate the area, in accordance to the prior consultations, including drawing the exact hairline. These markings will be the final ones.</p>
            </div>
        </div>
        <div class="col-md-12 pt-1">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Local Anaesthesia</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Local Anaesthesia</h3>
                    <p>You will be administered sedation, local anaesthesia or a combination of both, to ensure that you are comfortable and do not feel any pain or discomfort. In many cases, like at our clinic, anaesthetic gel & high frequency vibrations are also used to reduce the discomfort or pain caused by the injections. So we make sure procedure is virtually pain free</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Donor Tissue</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Donor Tissue</h3>
                    <p>The doctor will make long vertical incisions to remove the strip of scalp with the hair grafts, which will be used for your hair transplant. The exact location of the donor area has to be chosen very carefully, because this has to be the area where the hair density is maximum, as only this can ensure long term stability of the transplanted hair. At AK Clinics we only use trichophytic closure to have a fine scar</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Storage of Donor Tissue</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Storage of Donor Tissue</h3>
                    <p>As soon as the strip of hair is removed, it is placed in a physiological solution, which minimizes the body’s natural fluids and gives time to close the wound created by the strip removal.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Slivering & Dissection</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Slivering & Dissection</h3>
                    <p>The harvested strip is now divided into single layer of hair follicular units called the Slivers.
                        Each sliver is then trimmed for any extra tissue. The grafts are then separated individually which will be finally implanted in the bald area.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Creation of the Recipient Sites</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Creation of the Recipient Sites</h3>
                    <p>The creation of the recipient site has to be done with precision, because this will decide the complete look. Fine blades or needles are used to create the site and then the angle of hair is decided. A good surgeon will ensure that the sites are created in a way that the final look is completely natural. At AK Clinics, we use custom blades as per size of graft.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Implantation & End of Surgery</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Implantation & End of Surgery</h3>
                    <p>After the sites have been created the grafts will be transplanted into holes made by the surgeon.
                        At the end of the procedure, each of the grafts will be checked again and you will be told about the post op schedule. You will also be given a printed copy of the same, so that you can follow all the instructions. Your donor area will be covered with bandage and you will be able to wear a bandana or cap to provide further protection.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area  ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title ">
                    <h2 class="black-text">Important facts about FUT Hair Transplant</h2>
                </div>
            </div>
        </div>
        <div class="row text-center pt-1">
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h4>FUT scar</h4>
                <p>Given that a strip of the scalp is removed, there will be an exposed strip of skin on your head. This will have to be stitched together, and over time, the same will heal, but a scar will be left behind. In most cases, there will be a linear scar, which in the hands of a good doctor will not be visible after a while. This will happen because the new hair will be transplanted in such a manner that when they grow to a certain length, the scar will be hidden beneath it.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h4>FUT Advantage</h4>
                <p>FUT is still more prevalent due to the advantages it offers. The patients with large area of baldness should choose FUT Hair Transplant. The scar in most people remains hidden as more than 75% of people do not shave of head ever. And then there are many ways to cover it.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h4>FUT side effects & disadvantages</h4>
                <p>If the procedure has been done by an experienced surgeons like at AK Clinics, there is little to worry about. However, if the procedure has been done by someone inexperienced, there are several things that could go wrong. For starters, the stitches or sutures could develop infection, pus, itching or redness. The downtime is more as compared to FUE Hair Transplant. The patients can also see some effluvium around the scar but can happen with FUE too.</p>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class=" black-text">FUT Strip Scars Restoration</h2>
                </div>
                <p>Restoring scars after an FUT procedure is not easy and can be handled only by a surgeon with immense experience. There are mainly three methods by which the same can be done – by implanting new hair follicles using the FUE method, by using the W-plasty technique wherein the wound is excised and stitched and finally, there is the option of micropigmentation.</p>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h4>Implanting new hair follicles with FUE</h4>
                <p>This is perhaps one of the most popular methods for scar restoration and offers results that are completely natural. FUE is a minimally invasive procedure, which means that people are able to return to their normal routines, within a few days.Hair follicles are removed from the donor area, and then carefully placed around the scar, allowing for greater coverage.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h4>Application of excision on the scar and stitching the wound using W-plasty</h4>
                <p>The scar will first be excised in W shapes and once the area around the scar has been mobilised, the wound is stitched, one layer at a time. The technique is called trichophytic closure and is an ideal option for scars that are of a medium or large width. However, the skin surrounding the scar has to be elastic or else the results will not be satisfactory enough.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h4>Micropigmentation</h4>
                <p>Also known as medical tattooing, this is one of the easiest methods to cover FUT scars. Tiny spots or fine lines are created across the scar line in a colour that matches the natural hair colour. Tattoos being permanent will not only help cover the scar, but also last a lifetime, without requiring any maintenance.</p>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray ">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>