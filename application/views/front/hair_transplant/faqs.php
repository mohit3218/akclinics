<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/faqs/"> <span itemprop="name">FAQ's</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Hair Transplantation – Questions & Answers</h1>
                </div>
                <div class="col-md-12">
                    <div id="accordion" class="mt-4">
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Why Should One Opt For AK Clinics?</a> </div>
                            <div id="collapseOne" class="collapse " data-parent="#accordion">
                                <div class="card-body">AK clinics is well equipped with latest and ultramodern state-of-the-art technology of international standards and there are world renowned super specialist surgeons of high skills and rich experience of hair transplantation procedure provide you the best treatment at competitive but low cost. To know more</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Who Can Undergo This Procedure?</a> </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">Generally it depends upon the diagnosis. Mostly patients suffering from baldness that cannot be cured with medicines undergo this procedure. Before going for this procedure patient has to disclose any other disease or medications that he/she is using.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Who Can Undergo This Procedure?</a> </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body">Generally it depends upon the diagnosis. Mostly patients suffering from baldness that cannot be cured with medicines undergo this procedure. Before going for this procedure patient has to disclose any other disease or medications that he/she is using.<br />
                                    One must have good number of hair follicles. Specific pattern of hair loss, emotional distress, thin and receding hair line and hair density also determine the probability of this procedure. Age also plays important factor but not in all the cases. There are certain diseases like alopecia areata in which individuals own immunity system damages the hair follicles and in such a condition this treatment has no more significance.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">What Problems Can Occur If Patient Does Not Take Proper Treatment From Professional And Specialist Doctor?</a> </div>
                            <div id="collapsefour" class="collapse " data-parent="#accordion">
                                <div class="card-body">If one does not seek for professional advice and treatment it will create a bundle of problems such as scalp infections, allergies, formation of bumps, big scars, huge wastage of hair follicles, irregular hair growth, excessive bleeding and swelling etc. Hair growth from such a poor transplant by unskilled and inexperienced does not look natural.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefive">What Is Time Period To Expect The Result Of This Procedure?</a> </div>
                            <div id="collapsefive" class="collapse " data-parent="#accordion">
                                <div class="card-body">Generally 6-12 months are sufficient to observe the effective result of this procedure.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsesix">What Is The Difference Between Hair Follicles And Grafts?</a> </div>
                            <div id="collapsesix" class="collapse " data-parent="#accordion">
                                <div class="card-body">Hair grows naturally in follicular units of one, two or three hair. One such group is called a graft that if extracted together grows in a very natural way.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseseven">What Is Hair Transplant?</a> </div>
                            <div id="collapseseven" class="collapse " data-parent="#accordion">
                                <div class="card-body">Hair transplant is a procedure in which hair follicles from donor sites at back of head are removed surgically with great care. Removed follicles are harvested and transplanted in the bald area of scalp by our super specialist in correct angle and proper orientation so that hair follicles give rise to normal, healthy, permanent and natural hairs. Hair transplant is a minimally invasive surgery performed by our <a href="<?= base_url(); ?>about-us/our-team/dr-kapil-dua/">highly skilled surgeons at AK Clinics</a>. Before carrying out this procedure our doctors at AK clinics made a thorough diagnosis. Local anesthesia is given to reduce pain or discomfort of procedure. Patient remains fully conscious and active during the procedure. Surgeons do prescribe some antibiotics and anti-inflammatory medicines to prevent any infection and complication.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseeig">What Is Hair Graft?</a> </div>
                            <div id="collapseeig" class="collapse " data-parent="#accordion">
                                <div class="card-body">Hair graft is implantation of living tissue of skin especially dermis along with hair follicle. Hair follicles are removed surgically from donor site. Donor sites are present on the lower back side of head. Implantation of healthy and living hair follicles in the bald area is called hair graft.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsenine">What Is Donor And Recipient Area Of Scalp?</a> </div>
                            <div id="collapsenine" class="collapse " data-parent="#accordion">
                                <div class="card-body">Donor is the part of scalp from where hair follicles are removed and recipient area is that part scalp which is bald and hair follicles are transplanted into this area.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseten">What Are Side Effects Of This Procedure?</a> </div>
                            <div id="collapseten" class="collapse " data-parent="#accordion">
                                <div class="card-body">There is no known side effects of this procedure as such because of little use of medicines.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseele">Is This Procedure Very Expensive?</a> </div>
                            <div id="collapseele" class="collapse " data-parent="#accordion">
                                <div class="card-body">The cost of hair transplant differs from patient to patient and depends upon many factors such as the bald area, number of follicles to be transplanted, availability of healthy hair follicles, type of body part associated with transplant such as scalp of head, eyebrow, beard etc. Therefore only expert medical professional can estimate the <a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">cost of hair transplantation</a> which varies from patient to patient.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsetwe">Is There Any Role Of LASER In Hair Transplant Procedure?</a> </div>
                            <div id="collapsetwe" class="collapse " data-parent="#accordion">
                                <div class="card-body">No, there is no role of LASER in this procedure however low level LASER is useful in increasing the circulation and preventing the clogging of blood capillaries.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsethr">Is There Any Hormonal Effect That Can Cause The Loss Of Transplanted Hairs?</a> </div>
                            <div id="collapsethr" class="collapse " data-parent="#accordion">
                                <div class="card-body">No, as you know these transplanted hairs are grown from hair follicles which are resistant to genetic and androgenetic effects (DHT- dihydro testosterone hormone in men)</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefor">Is There Any Alternate Of This Procedure?</a> </div>
                            <div id="collapsefor" class="collapse " data-parent="#accordion">
                                <div class="card-body">Since this procedure is surgical some patients of baldness opt for wigs and patches as an alternate but these are not very much effective as we know that wig becomes loose by the time and may cause infections and allergies. Medicines too are not effective in all the cases of baldness although some medicines may help in thickening the hair and indirectly helping the hair to grow by removing other factors such as allergies, infections etc. But in many situations the <a href="<?= base_url(); ?>hair-transplant/">procedure of hair transplant</a> becomes inevitable and has no exception.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefve">How Soon One Can Resume Work Or Office?</a> </div>
                            <div id="collapsefve" class="collapse " data-parent="#accordion">
                                <div class="card-body">It depends upon the transplanted area and procedure but within 2-4 days one is likely to resume his work or office.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsesx">How Much Time Does This Procedure Take?</a> </div>
                            <div id="collapsesx" class="collapse " data-parent="#accordion">
                                <div class="card-body">Mostly it takes maximum of one-two days to complete the whole procedure but generally it depends upon the bald area.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseset">How Much Time Does It Take To Recovery?</a> </div>
                            <div id="collapseset" class="collapse " data-parent="#accordion">
                                <div class="card-body">Generally patients go home the same day and no need to stay in hospital or clinics.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseseta">How Much Pain Is There During The Procedure?</a> </div>
                            <div id="collapseseta" class="collapse " data-parent="#accordion">
                                <div class="card-body">Local anesthesia is used so there is no pain. Further the use of syringe with very fine and short needles is used to reduce the pain of injection. Even one can read newspaper/magazine or can enjoy watching movie or television etc. Patient feels a little bit of discomfort when a doctor begins this procedure. Further at AK Clinics, we have improvised technique to such an extent that there is absolutely minimal discomfort.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseeteen">Can One Lose Transplanted Hair?</a> </div>
                            <div id="collapseeteen" class="collapse " data-parent="#accordion">
                                <div class="card-body">Generally transplanted hairs are permanent and very much natural. One does not lose them under normal healthy conditions required for hair growth. Moreover there is no affect of hormones and other hereditary factors on the transplanted hairs. But there are some situations in which one can lose transplanted hairs such as scalp infections, fungal infections, dandruff, allergic reactions, mechanical injury etc</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseNteen">Can Doctor Transplant Hairs Of Another Person?</a> </div>
                            <div id="collapseNteen" class="collapse " data-parent="#accordion">
                                <div class="card-body">Normally it is not in practice of doctors to transplant hairs from any other person to one who is suffering from baldness because of many issues and complications such as histocompatibility, allergic reactions, infections, immunity responses, hypersensitivity etc. involved in such a procedure. Such factors reduce the chances of successful transplant. There will be need of more medications on routine bases to make such procedure successful. Also we should not forget that such procedure involves two different individuals and this will further raise the cost of treatment significantly high with low chances of success rate.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseTNTY">Can One Follow Some Hair Styles On These Transplanted Hairs?</a> </div>
                            <div id="collapseTNTY" class="collapse " data-parent="#accordion">
                                <div class="card-body">Yes after completing successful hair transplant and following post operative care and instructions one can go for different hairstyles and other different combing practices.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseTNTYONE">Are There Any Other Benefits?</a> </div>
                            <div id="collapseTNTYONE" class="collapse " data-parent="#accordion">
                                <div class="card-body">Hair transplant procedure can be used to increase the density of hairs. Moreover this procedure is also very helpful in maintaining the healthy hairline. The artistic planning of this procedure is further helpful in reducing the number of grafts and hence the cost of the procedure.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

