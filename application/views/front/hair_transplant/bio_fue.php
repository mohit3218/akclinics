<style>
    thead-dark th {	color: #fff;	background-color: #212529;	border-color: #32383e}
    @media only screen and (max-width:760px), (min-device-width:768px) and (max-device-width:1024px) {
        .cost-table tr td, table.cost-table tr th {	font-size: 12px}
        .cost-table thead tr {	position: absolute;	top: -9999px;	left: -9999px}
        .cost-table tr {	margin: 0 0 1rem}
        .cost-table tr.headtr{	    background: #333 !important;    color: #fff;    width: 100%;    padding-left: 0 !important;}
        .cost-table tr.headtr td{ padding-left: 10px !important;}
        .cost-table tr:nth-child(odd) {	background: #ccc}
        .cost-table tr td {	border: none;	border-bottom: 1px solid #fff;	position: relative;	padding-left: 45%}
        .cost-table tr td:before {	position: absolute;	top: 0;	left: 6px;	width: 45%;	padding-right: 10px;	white-space: nowrap}
        .cost-table tr td:nth-of-type(1):before {	content: "   "}
        .cost-table tr td:nth-of-type(2):before {	content: "Bio-FUE"}
        .cost-table tr td:nth-of-type(3):before {	content: "Routine"}
    }
    @media (max-width:640px) and (min-width:320px) {
        .cost-table tbody tr td {text-align: right!important}
        .cost-table td:before {position: absolute;top: 9px!important;left: 6px;text-align: left;width: 45%;padding-right: 10px;white-space: nowrap}
    }
</style>
<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/bio-fue/"> <span itemprop="name">BIO FUE</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row">
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">What is Bio-FUE<sup style="height: 10px; top: -15px; font-size: 37%; position: relative;">TM</sup>?</h1>
                    <p class="text-justify">Bio FUE is a trademarked procedure of FUE hair transplant developed by our own team of doctors at AK Clinics. It is a long term customized plan of hair restoration for our patients. It takes into account the fact that hair loss is a progressive condition & different patients have different hair characteristics or different patterns of hair loss. In Bio FUE technique, the hair follicles or grafts are extracted by our special technique along with administration of growth factors into the scalp, making hair stronger, longer and healthier. There are 5 steps of Bio FUE and we shall now discuss the same.</p>
                </div>
                <div class="col-md-12 text-center m-5">
                    <figure><img class="img-fluid center lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/steps-of-bio-fue-hair-transplant.png" alt="Steps of Bio FUE at AK Clinics"></figure>
                </div>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <ul class="list mt-0">
                            <li class=""><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                            <li class=""><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                            <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
                            <li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
                            <li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
                            <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                            <li><a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a></li>
                            <li><a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a></li>
                            <li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
                            <li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
                        </ul>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-areac bg-light-gray">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 p-0">
                <!-- <a href="https://www.youtube.com/watch?v=CDn_zdj5MUM?rel=0" target="_blank">-->
                <a href="#" data-toggle="modal" data-target="#exampleModalCenter"> 
                    <div class="ht bg-img cover-bg sm-height-550px xs-height-350px" data-background="<?php echo cdn('assets/template/frontend/'); ?>img/player.png" style="background-image:url(<?php echo cdn('assets/template/frontend/'); ?>img/player.png);background-position: 1px 0 !important;
                         background-repeat: no-repeat !important; "></div>
                </a>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 bg-white pd-60 pl-sm10 pr-sm10">
                <div class="cent">
                    <div class="section-head">
                        <h4 class="text-black">How is Local Anaesthesia Different in Bio-FUE<sup>TM</sup></h4>
                    </div>
                    <p class="pb-20 text-black">Local Anesthesia is used to block the sensations of pain in the scalp or other body part which is going to be operated during <a href="<?= base_url(); ?>hair-transplant/">hair transplantation</a>. It can be administered either by applying topical anesthesia or by injecting into the skin directly.
                        Routinely at other clinics, the area to be operated upon is made numb by injecting local anesthesia into the scalp and there is pain at the time of administration.</p>
                    <figure><img class="img-fluid lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2014/08/bio-fue.png" alt="Bio FUE"></figure>

                    <strong class="pb-20 text-black">Virtually pain free Local Anesthesia</strong>
                    <p class="pb-20 text-black">But at AK clinics, it is virtually pain free as we follow a special procedure to curb the pain sensation.</p>
                    <ul class="text-left list-unstyled">
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> We firstly apply anesthetic gel to the base of the scalp 40 minutes before starting the anesthesia.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Then we use world’s thinnest needles and vibrator on the area to make it virtually pain free anesthesia.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class=" section-title ">
                    <h2 class=" black-text">How is Slit Making Different in Bio-FUE<sup style="height: 10px; top: -15px; font-size: 37%; position: relative;">TM</sup></h2>
                </div>
            </div>
        </div>
        <div class="row align-items-center about-video pt-1">
            <p>Slit making is a process of creating space in the scalp where the grafts will be implanted. Routinely at other clinics, slits are made in the recipient area using needles after the extraction of grafts is over. The slits are variable in size and depth which leads to uneven level growth of hair.</p>  <div class="col-md-7 col-sm-12 content-part">

                <strong> Innovative & Customized Slit Making</strong>
                <p> But At AK clinics, it is different. Our experienced surgeons perform slit making before extraction so as to decrease the out of body time of grafts. (It is the time interval between the extraction and implantation for which the grafts keep on lying outside the body in the storage solution). They make the slits with the special CTS (cut to size) blades. The procedure for doing the same is as below:</p>
                <p>Five test grafts are taken out from the patients scalp. The average length of the grafts is measured. Then, the length of the shaft of the blade is adjusted as per your hair length and then the slits are made at proper angle, direction and density.The use of imported CTS (cut to size) blades with guard on the holder ensures that the depth of each and every slit is as per the length of your graft which gives following benefits:</p>
                <ul class="text-left list-unstyled">
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> There is minimal chance of mechanical trauma to the underlying vessels.</li>
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The time spent by grafts outside the body is minimum which helps to keep your hair viable and gives better results.</li>
                </ul>
            </div>
            <div class="col-md-5 col-sm-12">
                <figure><img class="img-responsive img-thumbnail center lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/Innovative-Customized-Slit-Making.png" alt="Innovative & Customized Slit Making"></figure>
            </div>
        </div>
    </div>
</div>
<div class="service-area text-center ptb-3 bg-light-gray">
    <div class="container ">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class=" section-title ">
                    <h2 class=" black-text">How is Extraction Different in Bio-FUE</h2>
                    <p>Extraction is the process of removal of grafts from the back side of scalp. Routinely, the grafts are extracted using sharp punches. These punches go deeper into the soft tissues making the unit loose and finally removed. The transection rates in most cases are not recorded &amp; are in the range of 30% or more. Further these clinics target very high number of grafts by going out permanent zone and as a result these hair do not last long and fall off soon.</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 pt-1">
            <div class="row">
                <div class="col-md-4">
                    <figure><img class="img-fluid rounded-circle lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/Safe-donor-area.png" alt="Donor area"></figure>
                    <h4 class="pt-1">Safe donor area</h4>
                    <p> At AK clinics, our surgeons extract strictly from safe donor with our special FLAT punches. Safe donor area or permanent zone is a horse shoe shaped area on the backside of the head which is not affected by <a href="<?= base_url(); ?>blog/12-most-powerful-natural-dht-blockers-that-stop-hair-loss/">DHT</a> hormone.</p>
                </div>
                <div class="col-md-4 ">
                    <figure><img class="img-fluid rounded-circle lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/Flat-Punch-1.png" alt="Flat Punch"></figure>
                    <h4 class="pt-1">Flat Punch</h4>
                    <p> FLAT punch is a special punch developed by our doctors which has a flat surface and minimizes wastage during extraction. The extraction process starts with selection of the flat punch which is generally 1mm less than the average length of the grafts. </p>
                </div>
                <div class="col-md-4 ">
                    <figure><img class="img-fluid rounded-circle lazy" src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/Zig-Zag-Extraction.png" alt="Zig-Zag Extraction"></figure>
                    <h4 class="pt-1">Zig-Zag Extraction</h4>
                    <p> Extraction is done in a zig-zag manner ensuring that no two consecutive grafts are extracted. This way the graft next to the extracted graft covers the spot after hair growth. By doing so:</p>
                </div>
                <h4 class="text-left">By doing so:</h4>
                <ul class="text-left list-unstyled">
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The donor area does not thin out &amp; almost remains the same as before the surgery.</li>
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> This highly precise procedure ensures that you get the best quality grafts for implantation &amp; the wastage limited to 2-3% only in most of the surgeries.</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tabs">
                    <div class="tab">
                        <button class="tab-toggle">Implantation</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">How is Implantation Different in Bio-FUE<sup>TM</sup></h3>
                        <div class="col-md-12">
                            <div class="span12"> <img class="img-fluid rounded pull-left m-1 lazy" style="width:45%" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/Smooth-Implantation.png" alt="Grafts Implantation"> <strong>Smooth Implantation</strong>
                                <p>Implantation is the process of putting back of extracted grafts back into the scalp. Routinely, grafts are extracted and they are implanted into the slit of the bald area by other clinics and grafts are stored in storage solution while the extraction is being done. Then they are implanted by keeping them on a gauze piece on hand end holding it at the level of hair of hair root at the time of implantation. In this technique, the former leads to dehydration and the latter trauma of grafts which leads to a poor yield.</p>
                                <strong>Smooth Implantation</strong>
                                <p>But at AK clinics, we implant them immediately after extraction, thereby decreasing the outside body time of grafts as mentioned  above, We follow the FIRST OUT, FIRST IN principle i.e. we implant those grafts first which were taken out earlier thereby for this decreasing the outside body time of grafts. One person keeps on pouring saline at the time of implantation thereby preventing dehydration of grafts. </p>
                                <p>And the implantation is done by Double forceps technique in which we hold the grafts from the top of the hair shaft and not hair root. </p>
                                <ul class="text-left list-unstyled">
                                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> We keep the grafts moist all the time. </li>
                                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> We get better yield and results there by making it <strong>“value for money”</strong> for you.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">Bio Therapy Different in Bio-FUE</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">How is Bio Therapy Different in Bio-FUE<sup>TM</sup></h3>
                        <p> Bio therapy is the unique procedure of doing <a href="<?= base_url(); ?>prp-treatment/">PRP (Platelet Rich Therapy) at AK Clinics</a>. The therapy is based on the concept that human germinative cells have got the ability to stimulate various important structures in embryonic life.<br>
                            At AK Clinics, we do everything “In House”. We draw around 40 cc of blood. We then process it in a specialized refrigerated centrifuge. The plasma is activated to release growth factors. This Platelet Rich Plasma is then injected into the superficial part of the scalp. The growth factors stimulate the stem cells and help to prolong the growth phase of the existing hair on the scalp.</p>
                        <strong>Steps of Bio Therpy</strong>
                        <ul class="text-left list-unstyled">
                            <li><strong>Step-1:</strong> Blood is drawn from the body and transferred into the centrifuge tubes.</li>
                            <li><strong>Step-2:</strong> Blood is then spun carefully in a centrifuge which will separate  the blood components.</li>
                            <li><strong>Step-3:</strong> The platelets rich plasma is then injected into the scalp or bald area.</li>
                            <li><strong>Step-4:</strong> This platelet rich plasma will act like growth serum and help to promote the hair growth.</li>
                        </ul>
                        <p><strong>By doing so:</strong> There are innumerable benefits of Bio-FUE and just some of the most important ones are mentioned here:</p>
                        <ul class="text-left list-unstyled">
                            <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Texture of transplanted hair is improved</li>
                            <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Hair growth is boosted</li>
                            <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The healing process is expedited, in both the donor and recipient areas</li>
                            <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Most importantly, transplant results become better</li>
                        </ul>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">Advantages Bio-FUE over FUE+PRP</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">Advantages of Bio-FUE<sup>TM</sup> over routine FUE+PRP</h3>
                        <table class="table table-hover cost-table ">
                            <thead role="rowgroup" class="thead-dark ">
                                <tr role="row">
                                    <th role="columnheader"></th>
                                    <th role="columnheader">Bio FUE</th>
                                    <th role="columnheader">Routine</th>
                                </tr>
                            </thead>
                            <tbody role="rowgroup">

                                <tr role="row" class="headtr">
                                    <td role="cell" colspan="3"><strong>Local Anesthesia</strong></td> </tr>

                                <tr role="row">
                                    <td role="cell">Virtually pain free anesthesia</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">Mostly painful</td>
                                </tr>
                                <tr role="row" class="headtr">
                                    <td role="cell" colspan="3"><strong>Slit Making</strong></td>

                                </tr>
                                <tr role="row">
                                    <td role="cell">Slits are made before extraction</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell"> After extraction</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Slits are made before extraction</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell"> After extraction</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Imported CTS blades are used</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell"> Needles</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Minimal trauma &amp; bleeding</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">Variable</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Uniform depth of slits</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">Mostly painful</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Proper direction of slits</td>
                                    <td role="cell"> Yes</td>
                                    <td role="cell">Haphazard</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Minimal out of body time of grafts</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">No</td>
                                </tr>

                                <tr role="row" class="headtr">
                                    <td role="cell" colspan="3"><strong>Extraction</strong></td></tr>
                                <tr role="row">
                                    <td role="cell">Only from safe donor area</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">Any ordinary punches</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">With Flat punch</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">Any ordinary punches</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Length of punch as per length of hair</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">One length of punch only</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Very less wastage at the time of extraction</td>
                                    <td role="cell">Yes (&lt;3%) </td>
                                    <td role="cell">Around 30%</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Grafts given as per the commitment</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">No</td>
                                </tr>
                                <tr role="row" class="headtr">
                                    <td role="cell" colspan="3"><strong>Implantation</strong></td></tr>
                                <tr role="row">
                                    <td role="cell">Minimal trauma at the time of implantation</td>
                                    <td role="cell">Yes (Double forceps technique)</td>
                                    <td role="cell"> Hair root is held with forceps</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Proper hydration of grafts</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">No</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Only 100% viable and complete grafts implanted</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">No</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">First out first in principle of implantation</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">No</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Direction of hair growth matches the direction of natural hair</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">Sometimes</td>
                                </tr>
                                <tr role="row" class="headtr">
                                    <td role="cell" colspan="3">
                                        <strong>Biotherapy</strong></td></tr>

                                <tr role="row">
                                    <td role="cell">Biotherapy is done with every procedure</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">May or may not</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Complete procedure in-house</td>
                                    <td role="cell">Yes</td>
                                    <td role="cell">May or may not</td>
                                </tr>  
                                <tr role="row" class="headtr">
                                    <td role="cell" colspan="3">
                                        <strong>Post op</strong></td></tr>

                                <tr role="row">
                                    <td role="cell">Swelling on forehead after surgery</td>
                                    <td role="cell">No</td>
                                    <td role="cell">Yes</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Infection in donor &amp; recipient area</td>
                                    <td role="cell">Extremely rare</td>
                                    <td role="cell">Yes</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Poor growth in recipient area</td>
                                    <td role="cell">Rarely</td>
                                    <td role="cell">Yes</td>
                                </tr>
                                <tr role="row">
                                    <td role="cell">Thinning in donor area </td>
                                    <td role="cell">No</td>
                                    <td role="cell">Yes</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area bg-light-gray ">
    <?php $this->load->view('front/partials/hair_transplant_blog'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
