<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/"> <span itemprop="name">FUE Hair Transplant</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">FUE Hair Transplant and Cost</h1>
                    <p class="text-justify">Losing hair can be traumatic for almost anyone, and when someone tells you that there is hope for you and you can get your hair back, it would seem like nothing short of a miracle. Over the past several years, numerous methods of getting hair back have emerged, including transplantation of hair. Today, there are several types of transplantation methods, but the most popular of them all happens to be FUE. <br />
                        <br />
                        This is another very important aspect when it comes to choosing a <a href="<?= base_url(); ?>hair-transplant/">hair transplant in India</a> or elsewhere. The price actually varies with the number of grafts and the baldness area. For a very large area we also need to use other hair restoration techniques like <a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a> or <a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT technique</a>. The cost starts at around INR 90000 for the FUE hair transplant.</p>     </div>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12  sm-height-550px xs-height-350px">
                <figure>
                    <img class="lazy" data-src="<?php echo base_url('assets/template/frontend/'); ?>img/fueinfographics.png" alt="FUE Hair Transplant Surgery Step by Step" style="width:80%;text-align:center;"/></figure>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 bg-dark pd-60 pl-sm10 pr-sm10">
                <div class="cent">
                    <div class="section-head">
                        <h4 class="text-muted">FUE Hair Transplant Procedure</h4>
                    </div>
                    <p class="pb-20 text-white">Here is a more in depth look at how an FUE procedure is done:</p>
                    <ul class="pb-20 text-white">
                        <li>The donor site and the recipient sites are demarcated beforehand. In case the donor site on the scalp will not yield sufficient grafts, other parts of the body, such as beard, chest and legs can also be looked at.</li>
                        <li>The hair follicles or grafts are then extracted one by one from the donor site.</li>
                        <li>The hair follicles or grafts will then be implanted almost immediately into the recipient site.</li>
                        <li>The procedure is minimally invasive and leaves behind no scars, especially when done by an expert. In addition, the healing time is minimal and people are able to return to their normal lives in no time.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tabs">
                    <div class="tab">
                        <button class="tab-toggle">FUE Hair Transplant Method</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">FUE Hair Transplant Method</h3>
                        <p>FUE or follicular unit extraction is one of the most modern and advanced methods of hair transplantation, which can assure you <a href="<?= base_url(); ?>results/">results</a> which are completely natural. Unlike FUT or follicular unit transplantation, FUE is a far more sophisticated method, which is why there are very few clinics around the world, offering the same. In FUT, a strip of the scalp is removed from the donor site and hair grafts are harvested from the same. The area from where the strip was removed is then sutured back and this leaves a linear scar. However, with FUE, there are no incisions, which means that there are also no sutures or scars. Via a suction like method, individual grafts are removed and are then almost immediately placed into the recipient site.</p>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">Advantages of FUE Hair Transplant?</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">Advantages of FUE Hair Transplant?</h3>
                        <div class="row">
                            <div class="span12"> 
                                <img class="img-fluid rounded pull-left m-1 lazy" style="width:45%" data-src="<?= cdn('assets/template/uploads/') ?>2015/12/advantage-fue-hair-transplant.jpg" alt="Advantages of FUE">
                                <p>There is a reason why innumerable people are now opting for FUE, as opposed to other forms of hair transplant, and that reason is that this is the most effective method of enjoying natural looking hair. Here are some of the other reasons:</p>
                                <ul>
                                    <li>Since there is no linear scar, which is prominent in a follicular unit transplant, you can wear your hair, as short as you want. </li>
                                    <li>There are no cuts, which means that there are no stitches either. </li>
                                    <li>As there are no cuts or stitches, the recovery period is minimal, which means that you can go back to your normal life, within no time. </li>
                                    <li>Again, because there are no cuts or sutures, there is minimal chance of any infection in the donor area or recipient area. </li>
                                    <li>The donor area can actually extend beyond the scalp, because with FUE, grafts can be extracted from the beard, arms, legs or chest. </li>
                                </ul>  
                            </div>
                        </div>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">Disadvantages of FUE Hair Transplant?</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">What are the Disadvantages of FUE Hair Transplant?</h3>
                        <div class="row">
                            <div class="span12"> 
                                <img class="img-fluid rounded pull-left m-1 lazy" style="width:45%" data-src="<?= cdn('assets/template/uploads/'); ?>2015/12/disadvantage-fue-hair-transplant.jpg" alt="Disadvantage of FUE">
                                <p class="">While the number of advantages of FUE are immense, there are a few drawbacks as well:</p>
                                <ul>
                                    <li>There is a limitation to the number of grafts that can be extracted in one session.</li>
                                    <li>There are often issues related to mechanical trauma. </li>
                                    <li>If the grafts are not handled properly, they could die.</li>
                                    <li>If the procedure is not being done by an expert, there could be over harvesting. </li>
                                </ul>     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="embed-responsive embed-responsive-21by9 ht">
                    <iframe src="https://www.youtube.com/embed/e5VevJbRSa0?rel=1" class="embed-responsive-item"></iframe>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 bg-dark pd-60 pl-sm10 pr-sm10">
                <div class="cent">
                    <div class="section-head">
                        <h4 class="text-muted">Why get it done at AK Clinics?</h4>
                    </div>
                    <p class="pb-20 text-white">Apart from the fact that AK Clinics is one of the leaders in the world of hair transplantation, there are a few other reasons why you should get your FUE done here:</p>
                    <ul class="pb-20 text-white">
                        <li>We can assure you that we have a highly controlled follicular transection rate – less than 3 percent, each time.</li>
                        <li>The time spent by a graft, outside the body, is minimal, ensuring its good health.</li>
                        <li>Since highly experienced surgeons will be working on you, we can assure you that the extraction will happen, very quickly and efficiently.</li>
                        <li>The grafts are given by counting, which is in tandem with ethical medical practices.</li>
                        <li>Our support staff is well trained in the procedures, which means that they can assist the surgeons, quite ably.</li>
                        <li>Our operating rooms are state of the art and extremely hygienic.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray ">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>