<style>
    thead-dark th{color:#fff;background-color:#212529;border-color:#32383e}
    @media only screen and (max-width:760px),(min-device-width:768px) and (max-device-width:1024px)
    {
        .cost-table tr td,table.cost-table tr th{font-size:12px}
        .cost-table thead tr{position:absolute;top:-9999px;left:-9999px}
        .cost-table tr{margin:0 0 1rem}
        .cost-table tr:nth-child(odd){background:#ccc}
        .cost-table tr td{border:none;border-bottom:1px solid #fff;position:relative;padding-left:50%}
        .cost-table tr td:before{position:absolute;top:0;left:6px;width:45%;padding-right:10px;white-space:nowrap}
        .cost-table tr td:nth-of-type(1):before{content:"No. of Follciles"}
        .cost-table tr td:nth-of-type(2):before{content:"Cost on Lower Side"}
        .cost-table tr td:nth-of-type(3):before{content:"Cost on Higher Side"}
        .cost-table trtd:nth-of-type(4):before{content:"No of Sittings"}
    }
    @media (max-width:640px) and (min-width:320px)
    {
        .cost-table tbody tr td{text-align:right!important}
        .cost-table td:before{position:absolute;top:9px!important;left:6px;text-align:left;width:45%;padding-right:10px;white-space:nowrap}
    }
</style>
<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/"> <span itemprop="name">Hair Transplant Cost</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
                <!-- Breadcrumbs /--> 
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Hair Transplant cost in India</h1>
                </div>
                <p>No matter what type of procedure you are getting done, you have to remember that there will be a price tag attached to it and most medical procedures tend to be expensive. Most of the times, people have this notion that <a href="<?= base_url(); ?>hair-transplant/hair-transplant-techniques/">hair transplant procedure</a> will burn a hole in their pocket, but it is important to remember that good quality, experience and a high level of professionalism will demand money.	<br /><br />However, at AK Clinics, the final cost of hair transplantation will depend solely on what kind of procedure & surgeon you are going for. If you are going for a non-surgical procedure, it will not cost you a lot, but if you are going for a proper transplant, you will need to shell out a little money. Hair Weaving cost may be less in short term but in long term most people go for transplant or stop using hair patch. <br />
                    <br />You need to remember that when you come to AK Clinics for a hair transplant, we will offer you a procedure that is minimally invasive and our team will ensure that you have the <a href="<?= base_url(); ?>results/">most natural looking</a> results, with the least amount of discomfort. We will be using instruments and equipment that is state of the art and our team knows how to utilise it in the best possible manner. When you add all these together you will realise that what you pay will actually be a great investment for yourself!</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title  ">
                    <h2 class="black-text">Hair transplant requirement for Different Grades of Baldness</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row text-center">
                <div class="col-md-3">
                    <figure><img class="alignnone img-thumbnail lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2016/08/grade-2.gif" alt="Hair Transplant Cost for Grade" width="400" height="200"></figure>
                    <p>Hair Transplant is not recommended and one should only start with some medicines to promote hair growth </p>
                </div>
                <div class="col-md-3">
                    <figure><img class="alignnone img-thumbnail lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2016/08/grade-2A.gif" alt="Hair Transplant Cost for Grade 2A" width="400" height="200"></figure>
                    <p>Hair Transplant can be done only if one is is not happy with the hairline. The requirement will be 800 to 2000 follicles depending on the expectation </p>
                </div>
                <div class="col-md-3">
                    <figure><img class="alignnone img-thumbnail lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2016/08/grade-3V-3A.gif" alt="Hair Transplant Cost for Grade 3V AND 3A" width="400" height="200"></figure>
                    <p>One should ask about hair loss treatments like <a href="<?= base_url(); ?>prp-treatment/">PRP Therapy</a>, Low Level Laser Therapy, Hair Gain Therapy. In case one plans to go for hair transplant 1500 to 3000 follicles will be required </p>
                </div>
                <div class="col-md-3">
                    <figure><img class="alignnone img-thumbnail lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2016/08/grade-4A-4.gif" alt="Hair Transplant Cost for Grade 4A and 4" width="400" height="200"></figure>
                    <p>Almost 4000 follicles to 4500 follicles will be required to treat the area. It is important that Medical therapy is given along to preserve the existing hair. </p>
                </div>
                <div class="clr ptb-1"></div>
                <div class="col-md-3">
                    <figure><img class="alignnone img-thumbnail lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2016/08/grade-5-5A-5V.gif" alt="Hair Transplant Cost for Grade 5, 5A and 5V" width="400" height="200"></figure>
                    <p>This is certainly a case of hair transplant where requirement will be 5000 to 6500 follicles which can be given over 2 sittings or combination technique </p>
                </div>
                <div class="col-md-3">
                    <figure><img class="alignnone img-thumbnail lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2016/08/grade-6.gif" alt="Hair Transplant Cost for Grade 6" width="400" height="200"></figure>
                    <p>Definitely a case of <a href="<?= base_url(); ?>hair-transplant/giga-session/">Gigasession</a> or requires 2 sitting to cover the complete area. </p>
                </div>
                <div class="col-md-3">
                    <figure><img class="alignnone img-thumbnail lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2016/08/grade-7.gif" alt="Hair Transplant Cost for Grade 7" width="400" height="200"></figure>
                    <p>This kind of baldness should also explore Artificial hair restoration or plan to cover the area in 2-3 sittings</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 p-0">
                <!-- <a href="https://www.youtube.com/watch?v=CDn_zdj5MUM?rel=0" target="_blank">-->
                <a href="#" data-toggle="modal" data-target="#exampleModalCenter"> 
                    <div class="ht bg-img cover-bg sm-height-550px xs-height-350px" data-background="<?php echo cdn('assets/template/frontend/'); ?>img/player.png" style="background-image:url(<?php echo cdn('assets/template/frontend/'); ?>img/player.png);background-position: 1px 0 !important;
                         background-repeat: no-repeat !important; ">
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 bg-dark pd-60 pl-sm10 pr-sm10">
                <div class="cent">
                    <div class="section-head">
                        <h4 class="text-muted">Why get it done at AK Clinics?</h4>
                    </div>
                    <p class="pb-20 text-white">Here are some of the main reasons why you should consider getting the transplant done at AK Clinics:</p>
                    <ul class="pb-20 text-white">
                        <li>A team of high trained and experienced professionals</li>
                        <li>An equally trained and dedicated team of support staff</li>
                        <li>Customised transplant plans, which are followed with care</li>
                        <li>Proper pre and post-operative care</li>
                        <li>A promise of good results and natural looking hair</li>
                    </ul>
                    <p class="pb-20 text-white">Getting a proper head of hair is often all that a lot of people need to get their confidence in themselves back and at AK Clinics, we will do just that for you!</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area p-5 bg-light-gray p0 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tabs">
                    <div class="tab">
                        <button class="tab-toggle">Hair Transplant Cost</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">Hair Transplant Estimated Cost</h3>
                        <p>Following are the approximate cost range for different standard <a href="<?= base_url(); ?>hair-transplant/">clinics for hair transplant</a>. The cost of hair transplant procedure is determined on a <strong>“per graft”</strong> or <strong>“per follicle”</strong> basis and the number is dependent on individual’s bald area, clinic, surgeon’s experience and expectation from the hair transplant. That is the reason it is no realistic to provide an exact estimate. Instead, we encourage patients to&nbsp;share their hair loss pictures and schedule a free online consultation with our doctors.</p>
                        <div class="table-responsive">

                            <table class="table table-hover cost-table" role="table">
                                <thead role="rowgroup" class="thead-dark ">
                                    <tr role="row">
                                        <th role="columnheader"><strong>Number of Follciles</strong></th>
                                        <th role="columnheader">Cost on Lower Side</th>
                                        <th role="columnheader">Cost on Higher Side</th>
                                        <th role="columnheader">No of Sittings</th>
                                    </tr>
                                </thead>
                                <tbody role="rowgroup">
                                    <tr role="row">
                                        <td role="cell">1000 – 1500 Hair Follicles</td>
                                        <td role="cell">INR 30,000/-</td>
                                        <td role="cell">INR 65,000/-</td>
                                        <td role="cell">1 Sitting</td>
                                    </tr>
                                    <tr role="row">
                                        <td role="cell">2000 – 3000 Hair Follicles</td>
                                        <td role="cell">INR 60,000/-</td>
                                        <td role="cell">INR 145,000/- </td>
                                        <td role="cell">1 Sitting</td>
                                    </tr>
                                    <tr role="row">
                                        <td role="cell">4000 – 5000 Hair Follicles</td>
                                        <td role="cell">INR 120,000/-</td>
                                        <td role="cell">INR 225,500/- </td>
                                        <td role="cell">1 Sitting</td>
                                    </tr>
                                    <tr role="row">
                                        <td role="cell">6000 – 8000 Hair Follicles</td>
                                        <td role="cell">INR 200,000/- </td>
                                        <td role="cell">INR 350,000/- </td>
                                        <td role="cell">1-2 Sitting</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <p><strong>Disclaimer: </strong>The above mentioned price is only for reference, every patient is given a custom quote depending on the choice of surgeon, location and technique. There is Service Tax/GST applicable on the cosmetic surgeries in India.</p>
                    </div>

                    <div class="tab">
                        <button class="tab-toggle">FUE and FUT Cost</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">FUE and FUT Procedure Cost</h3>
                        <p>Both FUE and FUT costs almost the similar but only the clinics which are highly experienced & have a good stable team offer FUT as this requires more trained manpower and only good surgeon can perform. But patients with higher grade of baldness </p>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">EMI with 0% Interest</button>
                    </div>
                    <div class="content">
                        <h3 class="m-3">Hair Transplant on EMI with 0% Interest</h3>
                        <p>AK Clinics offers its local patients with a facility payment in easy monthly installments by getting the hair transplant finance from Bajaj Finance. <a href="<?= base_url(); ?>hair-transplant-on-emi/" target="_blank">Know More</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray ">
<?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="padding:0px !important;">
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="    top: 20px;
                            z-index: 5; position: relative;height: 25px;idth: 25px;"> <span aria-hidden="true">&times;</span> </button>
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CDn_zdj5MUM" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>