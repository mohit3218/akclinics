<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/post-operative-care/"> <span itemprop="name">Post Operative Care</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text ">Post Op Care Instruction</h1>
                </div>
                <p >Hair Transplant is a very delicate surgery. The adherence to some very basic post-operative care will definitely help yield good results. Most the things mentioned below do not require a lot of extra time outside your routine. All you need to do is to just be little more conscious about the fact that you’ve undergone a surgery. <br />
                    And this more important as AK Clinics we try to minimise any intra or post operative discomfort and patients sometime tend to careless as they do not feel anything at all.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs" style="min-height: 236px;">
                <div class="tab">
                    <button class="tab-toggle">Instructions for the Recipient/ Transplanted area</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Instructions for the Recipient/ Transplanted area</h3>
                    <strong>Day Zero of the surgery:</strong>
                    <p>On discharge, you would be provided with a headband over the forehead region and a surgical cap.</p>
                    <p>You should wear the cap for three days and can remove the cap at the time of spraying (mentioned below) but it is advisable that you wear the headband continuously for the next 5 days. A spray bottle with Normal Saline solution is also provided to you on discharge. You need to spray the transplanted area with this solution starting from the day of the surgery. For the first 2 days, you should spray over the transplanted area once every hour and then from Day 3 to Day 5 of the surgery, you should spray it at 2 hourly intervals. There is no need to get up at night to spray during sleeping hours. You may use sterile water (bottled drinking water) once the Normal Saline solution provided is finished. Spraying is essential as it prevents scab formation and keeps the grafts hydrated. On the day of the discharge you should also start with the forehead massage. You are also advised to sleep on a flat surface or with a thin pillow for the next 5 days to prevent swelling over forehead. It is advised not to wash your head for initial 4 days after the transplant.</p>
                    <strong>* Details of massage are described later in detail under the subheading- Swelling.</strong> <strong>On Day 5 after the surgery:</strong>
                    <p> you can wash the transplanted area with water alone by pouring it gently with a mug over the non-transplanted part of the scalp so the water just seeps over the transplanted grafts. You can also block the flow of water partially with your other hand so water falls with less pressure over the grafts. Allow your hair to dry naturally, do not towel dry or blow-dry your hair. </p>
                    <strong>On Day 7:</strong>
                    <p> you can wash your scalp using shampoo with water. Mix a coin size amount of baby shampoo in a mug of water and pour it gently over your transplanted area of the head. You can partially block the flow of the water with your other hand so that the scalp is not subjected to strong, direct pressure. Rinse off any remaining soapy solution.  Do not stand under a shower. Allow your hair to dry naturally, do not towel dry or blow-dry your hair.</p>
                    <strong>On Day 10:</strong>
                    <p> the washing of the transplanted area is done as follows- If you are in town, it is advisable that you come to the clinic for this wash. In case you can’t come you can also do this yourself at home. First apply some aloe-vera gel gently on the transplanted area and keep it for 10 minutes to soften the crusts. Afterwards take a coin size amount of baby shampoo on your palm and lightly apply over the transplanted area with finger tips in a back to front direction making sure not to rub /scratch the area. Wash the shampoo by pouring some water on the head gently with mug.</p>
                    <strong>After the second shampoo wash at day 10: </strong>
                    <p> you can wash your hair regularly using baby shampoo/ prescribed shampoo twice a week. The scabs should be removed gently at the time of each head wash. <strong>It is imperative that you DO NOT rub/scratch the transplanted area.</strong></p>
                    <strong>Covering of the Recipient area: </strong>
                    <p> Continue to wear the surgical cap provided to you for 3 days after the transplant. You can start wearing the routine cap (Polo, cricket, etc.) from 3rd day onwards. Make sure that the routine cap does not impinge over the graft area/ put pressure over the recipient area. While wearing the cap make sure you wear it from back to front direction to avoid pressure on the transplanted area. </p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Instructions for the Donor area</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Instructions for the Donor area</h3>
                    <ul class="text-left list-unstyled">
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>Sleeping Instructions: </strong>There is bandage in the donor area that well makes it slightly uncomfortable to sleep. Please lie down flat on your back with a very thin pillow under your neck. Also keep a dark colored towel on your pillow, as there will be some soiling of the reddish fluid from the donor area. It is not the blood but the fluid coming out of donor area. It is normal phenomenon and you should not be worried about it.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>Immediately after the surgery</strong> your donor area would be bandaged. Remove your bandage on the 3rd post-operative day. Wet the bandage with the saline so as to soften the area and then remove the bandage softly. There might be some discharge coming out of the donor area. You would be provided with some gauze bandage pieces after the transplant, use them to clean the discharge. After the removal of the bandage on the 3rd day, apply the antibiotic ointment prescribed to you over the entire donor area as a thin layer. Take coin-sized amount of the antibiotic and apply it gently using your fingertips twice daily for the next 5 days. You may also experience some itching in the donor area due to the crusts. In this case, apply some aloe Vera gel over the donor area and remove it after 10 minutes using clean piece of bandage/ gauze provided to you. <em><strong>** For further details please see the text below under the subheading ‘Itching’.</strong></em></li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>FUT/ Strip surgery:</strong> In case of FUT/ Strip surgery, you would be called back to the clinic between Day 7-10 to remove your sutures. (Special Instructions for FUT patients have been provided in detail later in the text)</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">General Instructions</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">General Instructions</h3>
                    <ul class="text-left list-unstyled">
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>MEDICATIONS:</strong> After the surgery you would be started on certain oral medications for the next 5-7 days such as: mild pain killers to minimize discomfort; antibiotics for control of any infection; anti-allergic tablets once a day to reduce itching and multivitamins to aid in your recovery. A short course (5-day) of oral steroids would be started on the 3rd post-operative day to reduce signs of any inflammation. After around 2 weeks of the transplant surgery, we begin you on our Cyclical Therapy that consists of oral medicines and <a href="<?= base_url(); ?>blog/minoxidil-uses-dosage-precautions-and-side-effects/">topical solutions to be applied on the scalp</a>. The choice of taking the cyclical therapy is optional however we recommend it to every patient of hair transplant for a period of 3-6 months.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>SWELLING:</strong> There might be occurrence of swelling on the forehead after the <a href="<?= base_url(); ?>hair-transplant/">transplant surgery</a>. It usually starts from the hairline and may gravitate downwards to the forehead region and even around eyes. Following precautions diminish the incidence of post-transplant swelling:
                            <ol>
                                <ol>
                                    <li>After the surgery we provide you with a headband that is to be kept in place for the next 5 days.</li>
                                    <li>You are also advised to sleep flat without using any pillow rest for the next few days. If you want to use a pillow, use a very thin one and cover it with a towel, as there might be some yellowish-red discharge from the donor area in the first few days.(as described above)</li>
                                    <li>Massage your forehead with both hands using four fingers starting from the center of the forehead and going laterally towards each side. Massaging should be done around 10-15 times every hour for the initial 5 days.</li>
                                </ol>
                            </ol>
                            <p>Most importantly, do not keep your head in forward bending position to avoid swelling. In case the swelling arises, it usually occurs on the 3rd-5th post-operative day, increases on 6th- 7th day and decreases on its own by 8th and 9th day. This is again a normal phenomenon and will resolve on its own. You can use cool ice compresses over the area. In case of any fever/redness/ pain associated with the swelling, please contact our clinic.</p>
                        </li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>INFECTION:</strong> You would be prescribed an oral antibiotic course after the surgery to prevent any infection. In case, you get any signs of infection like white spots around hair or pain in the area of the transplanted hair, fever, chills, pus or discharge in the suture line, please contact our clinic as it may require treatment. It is important to note that few pimples in the transplanted area may be a sign of new hairs coming out through the skin. They usually resolve in few days and are not accompanied by pain/ redness. Ingrown hairs may also present as small cyst/ pimples. Please contact our clinic in case of any concerns.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>ITCHING:</strong> You might experience some itching in the donor or recipient area in the post-operative period which mostly occurs due to the residual scabs/crust formation. It should resolve after the first wash on day 7. You will be also prescribed an anti-allergic in the initial few days after transplant that should take care of the same. If it does not, then application of aloe Vera gel can also be done over the donor area in the first week to relieve the itching. Do not scratch the scalp as it may damage the grafts.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>BLEEDING:</strong> Rarely sometimes a little bleeding may occur after the transplant from the donor /recipient area. You are suggested to apply continuous firm pressure over the bleeding point with the bandage provided for 10 minutes continuously. This should stop the bleeding. In case the bleeding continues, please contact our clinic.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>LOSS OF GRAFTS:</strong> Sometimes you may find few loose grafts in the first week after the procedure. Do not be concerned as sometimes the hair may be shed with the crust and the hair may have a bulb at the bottom. This does not represent a lost graft. The transplanted hair may fall out, usually 2 to 4 weeks after the surgery. You will start to see the hair growth start at 3 to 4 months after the surgery; this hair will be very fine and minute at first. At 6 months around 40% the hair will have appeared, however they won’t be fully grown. It takes up to 12-18 months for a full growth to occur. Shedding of the native (original) hair can also sometimes occur post operatively. This is quite rare and if it occurs it will always regrow in 3-4 months.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>NUMBNESS/ TINGLING SENSATION</strong> in the recipient area sometimes occurs, this will resolve without intervention. It can last from weeks to a couple of months.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>ROUTINE ACTIVITY/EXERCISE/SWIMMING:</strong> On the day of the surgery, you will be under the effect of sedatives so it is advised you don’t drive back yourself/ engage in any other kind of work which requires precision. You can resume your daily normal routine from the next day itself but you might experience mild discomfort in your donor/recipient area for 1-2 days post-transplant. In case of FUE, we advise refraining from exercise during the first 10 days. You can start with light exercises after 10-14 days. In FUT/ Strip surgeries, the main concern is the widening of suture scar. Hence in FUT cases it is recommended to avoid strenuous physical activity 3 weeks post operatively; you should avoid gym work, swimming, cycling, tennis, golf, any heavy lifting, and strenuous hill walking for 3 weeks after the surgery. This is to avoid any stretching of the scar in the case of strip surgery. When you return to physical activity you should start off lightly and build up slowly to your previous activity levels. Do not swim in swimming pools with chlorine in the first 14 days as the chemicals may damage the grafts, it is however safe to swim in the sea from 5th day onward. Do not dive. After day 14 you can swim in swimming pools and apply sun lotion to the scalp and wear a  hat if needed. Avoid direct pressure/trauma to the transplanted grafts for around 10-14 days after the procedure. Avoid sexual activity for 5 days following the procedure.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>DIET: </strong>You would be provided with a diet chart upon your discharge to aid in the recovery and the growth of the grafts. It is advisable to have adequate intake of water and have a protein-rich diet in the immediate post-operative period.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>ALCOHOL/SMOKING:</strong> You would be on mild painkillers in the immediate post-operative period and it is advised to avoid alcohol for 7 days following the procedure.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>SMOKINGCAUSES:</strong> Smoking causes vasoconstriction and can decrease the blood supply to the scalp and also impact the wound healing process. It is recommended to stop smoking 1 week before and after the procedure. But in case you are a heavy and chronic smoker, you can continue smoking even on the day of the surgery to avoid any withdrawal symptoms. But you should quit smoking after the surgery.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>SUN EXPOSURE:</strong> It is advised to always wear a cap in the sun from 3rd day onwards. If you are in the sun prior to day 21, you must sit under an umbrella and avoid sitting in direct sun light for long periods.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>HAIR COSMETICS:</strong> You can use hair color after 4 weeks of surgery once the transplanted hair fall off in the donor area. It is advisable to wait for 3-4 months for applying hair color in the recipient area. Also avoid blow drying your hair for at least 3 -4 weeks following the surgery. We do not advise use of hair spray after the transplant for around 1 year.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>HAIR CUT:</strong> You can get a haircut after around 3 weeks of surgery.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>HAIR OIL:</strong> We do not recommend application of any hair stimulating oil/ castor oil or any other oil on the scalp after the surgery. You would be prescribed certain lotions and serums for conditioning after the surgery in our cyclical therapy. There is no need to apply any other lotion/ shampoo/conditioner apart from the ones mentioned in the prescription. In case you still wish to use some hair oil after the transplant, only coconut oil/Almond/ Olive oil should be used and its use can be started 12 weeks after the surgery.</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Special Instructions for FUT/ Strip Technique Patients</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Special Instructions for FUT/ Strip Technique Patients</h3>
                    <ul class="text-left list-unstyled">
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>Day of the Surgery (Day Zero):</strong> There might be some discomfort in the donor area over the sutures. There might be some crusting over the suture line. If you find washing the sutured area is uncomfortable, there might be some oozing of yellowish-red fluid from the suture line. This is normal. Use the gauze pieces provided to you and pat dry the area. It’s recommended you use a towel underneath your donor area while sleeping</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> If any bleeding occurs, do not be concerned. Apply direct pressure with the clean gauze pieces provided to you for few minutes. This should stop the bleeding. In case the bleeding continues, please contact our clinic.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> Your stitches will need to be removed between 7-10 days post-operatively. It is advisable to return to AK Clinics to have these removed. After the stitch removal, keep on applying the antibiotic ointment over the suture line prescribed to you for the next 5 days.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> <strong>Note:</strong> There may be a dissolvable suture underneath the skin that will self dissolve between 3 and 4 months post operatively. This can feel lumpy but it will return to normal in time</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">special instructions for female patients</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Special Instructions for Female Patients</h3>
                    <ul class="text-left list-unstyled">
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> The hair in the donor area need to be trimmed for the procedure. The total area trimmed depends on the number of follicles required. Usually, a strip of your normal long hairs is left above and below the trimmed area so that it can be cover the donor area post- surgery.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> After the surgery, your transplanted along with your original hair may give a messy/ shrubby appearance for few weeks till they grow in length.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> There might be some shock loss of your native /original hair after the transplant. You have been prescribed topical Minoxidil lotion (to be started after 14 days)and the hairs usually grow back in 3-4 months.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> When the hair start coming back, they are small and tiny and projecting forward. Eventually, they will grow long and go backwards.</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Special Instructions for Beard Transplant Patients</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Special Instructions for Beard Transplant Patients</h3>
                    <ul class="text-left list-unstyled">
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> If you have opted for a beard transplant; there is bound to be some swelling on the lower face, especially on the lips. This usually resolves in the next 3-5 days. You should follow a liquid/ semi-solid diet on the day of surgery. There might be some deviation of the angle of mouth to one side on the day of the surgery which also resolves by next morning.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> In case of <a href="<?= base_url(); ?>hair-transplant/beard-transplant/">beard hair transplantation</a>, these hair will fall off and start growing in the same way as that of the scalp. It is better to avoid shaving in this area.</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Special Instructions for Body Hair Transplant Patients</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Special Instructions for Body Hair Transplant Patients</h3>
                    <ul class="text-left list-unstyled">
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> In case of extraction using body hair, the site is bandaged after the procedure. You can remove the bandage on the next day and take a shower. Make sure not to rub the area too vigorously while showering. After the shower, apply the antibiotic cream prescribed to you on the donor site as a thin layer using fingertips. Re-apply the cream once at night time also. Continue the twice-daily application of antibiotic cream for the next 7 days. You can trim/ shave this area once the healing is complete, which usually takes 10-14 days.</li>
                        <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> In case of beard extraction, you can trim/ shave your beard hair after 8-10 days of the surgery.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>