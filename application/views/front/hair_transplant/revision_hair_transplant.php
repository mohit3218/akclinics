<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/"> <span itemprop="name">Hair Transplant</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/"> <span itemprop="name">Revision Hair Transplant</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Revision Hair Transplant</h1>
                </div>
                <p>With the advancement of Hair Restoration and specifically Hair Transplant, there is ever increasing demand of Revision Hair Transplant. Hair Transplant has come a long way in establishing itself as a technique of Hair Restoration. However, with more number of people undergoing hair transplant through Strip or FUE technique, more are number of people are undergoing revision hair transplant.</p>
                <strong>Now the point is why do they need to undergo Revision Hair Transplant.They need to undergo in two conditions:</strong>
                <ul class="text-left list-unstyled">
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> They are happy are want more grafts.</li>
                    <li><i class="fa fa-check fa-1x" aria-hidden="true"></i> When the first one has not given satisfactory results.</li>
                </ul>
                <p>This brings us to the question as is being written on many websites: that there is 100% success or 100% guarantee of results. The simple answer is ‘NO’. Hair Transplantation is just like any other surgical procedure where success differs from patient to patient. If any surgeon or clinic worldwide is claiming 100% guarantee of results, please take it you’re dealing with a quack. Even the Medical Council of India forbids any doctor to make such claims.</p>
                <p>Even at our clinic, we have seen patients with less &amp; no results. With improvising our technique, the failures have gone below 5% compared to 30%-40% failures in relatively new clinics. Though establishing clear-cut reasons is extremely difficult as the results are evaluated only after a year and in a year many things could have resulted the same. For the benefit and economical reasons, we tell our patients that if they get less 70% growth of transplanted grafts (only in androgenetic alopecia), we can do a revision with just the payment for consumables and no other charges. Again we promise a 100% ethical &amp; correct procedure not 100% results.</p>
                <p>So, for someone who can not take a failure should go in for non-surgical procedures like wigs, patches, Scalp Micro Pigmentation, hair fibers etc. The revision hair transplant is normally done and executed in the following manner:</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs" style="min-height: 206px;">
                <div class="tab">
                    <button class="tab-toggle">Revision after Punch grafting</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Revision after Punch grafting</h3>
                    <div class="row ptb-1">
                        <div class="col-md-3"><img class="img-fluid rounded lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/hair1.jpg" width="293" height="188" alt="Revision Hair Transplant after Punch grafting"></div>
                        <div class="col-md-3"><img class="img-fluid rounded lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/hair-21.jpg" alt="Revision after Punch grafting" width="293" height="188"></div>
                        <div class="col-md-3"><img class="img-fluid rounded lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/hair-4.jpg" alt="Before Revision Hair Transplant" width="293" height="188"></div>
                        <div class="col-md-3"><img class="img-fluid rounded lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/hair-3.jpg" alt="Before Revision Hair Transplant" width="293" height="188"></div>
                    </div>
                    <p> In such cases we examine the donor area first to establish the possibility of scalp to scalp transfer. If the donor area is too weak, we recommend Body Hair Transplant.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Revision after FUT Hair Transplant</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Revision after FUT Hair Transplant</h3>
                    <div class="row">
                        <div class="col-md-6"><img class="img-fluid rounded lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/hair-transplant-2.jpg" alt="Revision after FUT Hair Transplant - 1" width="400" height="314"></div>
                        <div class="col-md-6"><img class="img-fluid rounded lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/hair-transplant.jpg" alt="Revision after FUT Hair Transplant - 11" width="400" height="314"></div>
                    </div>
                    <p> This is normally the best-case scenario because the donor area to quite an extent is virgin. This is true if a seasoned surgeon has performed the surgery. A careful assessment is though conducted to ensure the exact number of grafts possible. We can go for another strip or FUE surgery depending on case to case.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Revision after FUE Hair Transplant</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Revision after FUE Hair Transplant</h3>
                    <div class="row">
                        <div class="col-md-3"><img class="img-fluid rounded lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/bald-1.jpg" alt="Revision of FUE overharvesting" width="292" height="204"></div>
                        <div class="col-md-3"><img class="img-fluid rounded lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/bald-21.jpg" alt="Revision after FUE Hair Transplant" width="292" height="204"></div>
                        <div class="col-md-3"><img class="img-fluid rounded lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/bald-31.jpg" alt="Revision after FUE" width="292" height="204"></div>
                        <div class="col-md-3"><img class="img-fluid rounded lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/bald-4.jpg" alt="FUE revision surgery" width="292" height="204"></div>
                    </div>
                    <p>We have seen many cases of FUE overharvesting where revision become almost impossible by any method. So again a careful assessment needs to made before planning the revision surgery.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Revision after Poor Results</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Revision after Poor Results</h3>

                    <p>Before planning a <a href="<?= base_url(); ?>hair-transplant/">hair transplant</a> it is important to counsel the patient for a realistic expectation and chances of success &amp; failure. In many cases the implanted grafts have grown but patient expected a density that was surgically impossible. So please see results carefully of patients to make fair assessment of post-operative results. There are many known reasons the <a href="<?= base_url(); ?>results/">hair transplant results</a> do not show up, like</p>
                    <p><strong>Tobacco consumption:</strong> Nicotine and other chemicals in smoked tobacco may reduce elasticity of small blood vessels in the skin, diminishing the blood supply to hair transplants and thus increasing risk for transplant failure</p>
                    <p><strong>Being careless:</strong> during early days of hair transplant about the transplanted grafts and not-following post-op instruction could result in the same.</p>
                    <p><strong>No Contact:</strong> Some of the problems certainly could be taken care of if the patients keep in touch with clinic. We give facility to the patient to be in touch through emails also with no need to visit back.</p>
                    <p>But in many cases revision is seemingly the solution. We recommend our patients that if they have waited for 12-14 months with not expected results, it is time for revision.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray ">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>