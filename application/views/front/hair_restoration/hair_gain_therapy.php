<?php $this->load->view('front/partials/hair_loss_restoration_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-restoration/"> <span itemprop="name">Hair Restoration</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>hair-gain-therapy/"> <span itemprop="name">Hair Gain Therapy</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row">
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-tex">HAIR GAIN THERAPY</h1>
                </div>
                <p>
                    AK Clinics has treated over 10000 patients suffering from various kind of hair loss 
                    problems. The doctors had been researching about the best possible treatment 
                    especially in the case of Male & female androgenetic alopecia. They understand that 
                    patient are very skeptical of surgical & medicine therapies as hair loss. 
                    So they have devised an indigenous methodology - ‘Hair-Gain Therapy’ that 
                    has given very effective results in thinning & even new hair growth with minimal 
                    medicines. Though the only way to treat complete baldness is Hair Transplant 
                    but great results can be achieved with method. This technique uses only the power 
                    of healing within the human body so it much safer than any homeopathic treatment 
                    or Ayurvedic treatment of hair loss. Our Six step methodology has helped many hair 
                    loss sufferers
                </p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-restoration/">Hair Restoration</a></li>
                        <li><a href="<?= base_url(); ?>prp-treatment/">PRP Therapy</a></li>
                        <li><a href="<?= base_url(); ?>hair-loss-treatment-men-women/">Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>artificial-hair-restoration/">Artificial Hair Restoration</a></li>
                        <li><a href="<?= base_url(); ?>hair-restoration/scalp-micro-pigmentation-india/">Scalp Micro Pigmentation</a></li>
                        <li><a href="<?= base_url(); ?>mesotherapy/">Mesotherapy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="tabs">
                    <div class="tab">
                        <button class="tab-toggle">Hair Analysis</button>
                    </div>
                    <div class="content">
                        <h4>Hair Analysis</h4>
                        <p class="pull-left">
                            <img data-src="<?= cdn('assets/template/frontend/images/'); ?>33.png" alt="Hair Analysis" class="img-fluid lazy">
                        </p>
                        <p>
                            Hair Analysis is almost the backbone of this new revolutionary treatment of 
                            hair loss. We are able to find out the exact density, caliber & quality of 
                            the hair in different areas. The special camera allows for close examination 
                            upto 50X magnification. This test also gives us a better idea about the scalp 
                            condition so that any underlying problem can also be identified and treated. 
                            This helps us gauge the improvement in the patient during the course of the 
                            therapy.
                        </p>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">Bio-Therapy</button>
                    </div>
                    <div class="content">
                        <h4>BIO-THERAPY | PRP THERAPY</h4>
                        <p class="pull-left">
                            <img data-src="<?= cdn('assets/template/frontend/images/'); ?>55.png" alt="Bio-Therapy" class="img-fluid lazy">
                        </p>
                        <p class="description">
                            We’ve been experimenting for over 7 years for the most optimum methodology 
                            for the treatment of hair loss. This PRP Therapy (<a href="<?= base_url(); ?>prp-treatment/" target="_blank">PRP Hair Treatment</a>) in 
                            conjunction with the other non-invasive procedures help patients get 
                            excellent results. PRP or Platelet-rich plasma (Abbreviation: PRP) is blood 
                            plasma that has been enriched with platelets that contains several growth 
                            factors. AK Clinics’ doctors have mastered the right method for generating 
                            the growth factors (PRP) with safety & efficacy and use them at the right 
                            level & instrumentation. So it is not about what to do but how to do that 
                            makes us different.
                        </p>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">Oxygenate</button>
                    </div>
                    <div class="content">
                        <h4>Oxygenate</h4>
                        <p class="pull-left">
                            <img data-src="<?= cdn('assets/template/frontend/images/'); ?>22.png" alt="Oxygenate" class="img-fluid lazy">
                        </p>
                        <p class="description">
                            This is the first step of the therapy in which includes simple cleaning and 
                            disinfection treatment on the scalp. Any foreign particles present are 
                            cleaned with the help of oxygen spray pen that can generate saline spray 
                            by the airflow generated by the machine.
                        </p>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">Med Atomizer</button>
                    </div>
                    <div class="content">
                        <h4 >Med Atomizer</h4>
                        <p class="pull-left">
                            <img data-src="<?= cdn('assets/template/frontend/images/'); ?>66.png" alt="Laser Phototherapy" class="img-fluid lazy">
                        </p>
                        <p class="description">
                            This gives us a uniform spray of hair growth medicines into the hair follicles after cleaning of the scalp. This is very important as the treatment needs to ensure that the medicine is properly sprayed all over the treatment area. This also ensure that the medicine is not sprayed anywhere else.
                        </p>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">Massage Comb</button>
                    </div>
                    <div class="content">
                        <h4>Massage Comb</h4>
                        <p class="pull-left">
                            <img data-src="<?= cdn('assets/template/frontend/images/'); ?>44.png" alt="massage comb" class="img-fluid lazy">
                        </p>
                        <p class="description">
                            There are Red light and Electrodes on the massager machine. The red light 
                            generated through a special filter increases cell metabolism and the 
                            micro-current generated by the electrodes accelerate the opening of vessels 
                            on the scalp.
                        </p>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">Laser Phototherapy</button>
                    </div>
                    <div class="content">
                        <h4>Laser Phototherapy</h4>
                        <p class="pull-left">
                            <img data-src="<?= cdn('assets/template/frontend/images/'); ?>66.png" alt="Laser Phototherapy" class="img-fluid lazy">
                        </p>
                        <p>
                            Low-powered (cold) lasers generate laser light which are known to provide 
                            light energy to all the hair roots which accelerates the growth of hair. 
                            The laser light is known as LLLT or Low-Level Laser Therapy. LLLT is many 
                            a time included under a wider definition of ‘Phototherapy’ which is known as 
                            Low-Level Light Therapy.
                        </p> 
                        <br />
                        <ul >
                            <h4>Merits of LLLT</h4>
                            <li>The progression of loss of hair is stopped</li>
                            <li>Augments the supply of blood to the scalp</li>
                            <li>Accelerates the hair follicles</li>
                            <li>Increases the elasticity and strength of hair</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>