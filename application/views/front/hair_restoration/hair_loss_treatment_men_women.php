<?php $this->load->view('front/partials/hair_loss_restoration_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-restoration/"> <span itemprop="name">Hair Restoration</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-loss-treatment-men-women/"> <span itemprop="name">Hair Loss Treatment Men Women</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1">
                    <h1 class="black-text text-left">Understanding Hair Fall & Treatment</h1>
                </div>
                <p>Hair is an important part of a person’s personality, which is why when someone starts to see more hair on their comb than on their head, they start to worry. However, hair fall is much more common than most people would think and while a few hairs a day is considered normal hair loss, there is actually a fine line where it goes overboard and a hair fall treatment or baldness treatment or Hair Regrowth becomes necessary. Today, hair loss has become common with men as well as women, but so have methods of hair loss prevention & Hair Regrowth.</p>
                <p>In the simplest of terms, the loss of hair from any part of the body, especially the head, is known as hair loss. There can many causes for hair loss in fact a range of reasons for hair loss, including genetics, hormonal changes, medical conditions or even certain hair care products.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-restoration/">Hair Restoration</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>prp-treatment/">PRP Therapy</a></li>
                        <li><a href="<?= base_url(); ?>artificial-hair-restoration/">Artificial Hair Restoration</a></li>
                        <li><a href="<?= base_url(); ?>hair-restoration/scalp-micro-pigmentation-india/">Scalp Micro Pigmentation</a></li>
                        <li><a href="<?= base_url(); ?>mesotherapy/">Mesotherapy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 bg-img cover-bg sm-height-550px xs-height-350px " data-background="<?php echo cdn('assets/template/frontend/'); ?>img/01.jpg" style="background-image:url(<?php echo cdn('assets/template/frontend/'); ?>img/01.jpg);"></div>
            <div class="col-md-6 col-sm-12 col-xs-12 bg-light-gray pd-60 pl-sm10 pr-sm10">
                <div class="cent">
                    <div class="section-head">
                        <h4 class="text-black">Symptoms of Hair Loss</h4>
                        <p>There are several indications that you might notice, which could actually be leading to severe loss of hair. Some of them include:</p>
                        <ul>
                            <li>The most common symptom is the gradual thinning of hair, especially near the temples and the forehead.</li>
                            <li>Many men are able to notice a bald patch appearing on the crown of their head.</li>
                            <li>When someone has undergone a physical or mental trauma, hair could fall out in bunches, especially while washing or combing.</li>
                            <li>If there is ringworm, there could be scaly patches and this could also lead to hair loss.</li>
                            <li>In treatments such as chemotherapy, there can be full body hair loss.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Prevention from Hair Loss</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Prevention from Hair Loss</h3>
                    <p>There are however certain things that you can do to ensure that you can reduce if not completely prevent loss of hair. These include:</p>
                    <ul>
                        <li>Eating a diet that is balanced and contains sufficient amount of protein</li>
                        <li>Treat your hair with gentle hands, ensuring that you do not wash, dry or comb them in a rough manner. Use a shampoo and conditioner that suits your hair type and comb with a wide toothed comb.</li>
                        <li>Try to avoid tight hairstyles, which pull at the hair or general pulling of the hair.</li>
                        <li>While using curling or straightening irons once in a while is fine, try to ensure that you do not use them on a regular basis. Even if you are using a hair dryer try to use a lower setting or at a gentler heat.</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Causes of Hair Loss</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Causes of Hair Loss</h3>

                    <p class="text-center">
                    <img class="img-fluid lazy" style="width:70%;" data-src="<?= cdn('assets/template/frontend/images/'); ?>hair-loss-men-women.jpg" alt="hair loss men women"></p>
                    <p>Several reasons could contribute to hair loss, but some of the main causes include:</p> <ul>
                        <li><strong>Genetics – </strong>Male or female pattern baldness is most commonly attributed to genetics, which means that if your parents had the condition, chances are that you will have it too. Heredity can also affect the manner in which you lose your hair, at what age the hair loss starts and the rate at which the hair loss takes place.</li>
                        <li><strong>Hormones – </strong>When the hormones in your body are not in a steady state, there could be a change in your hair follicles too, which could lead to loss of hair. For instance, pregnancy, childbirth, menopause or hyperthyroidism could all lead to loss of hair.</li>
                        <li><strong>Infections of the scalp –</strong> If you have scalp infections such as ringworms, then too you could notice more hair on your comb, as opposed to your head.</li>
                        <li><strong>Trichotillomania –</strong> This a medical condition, wherein people are unable to resist the temptation to pull out their hair, and this would quite obviously lead to loss of hair.</li>
                        <li><strong>Certain medication –</strong> Medicines for conditions such as depression, heart problems, cancer and birth control can also cause hair loss.</li>
                        <li><strong>Medical treatments –</strong> Treatments such as radiation and chemotherapy are also known to lead to hair loss.</li>
                        <li><strong>Trauma –</strong> When a person goes through physical or mental trauma, there is often a state of shock in the body, which could lead to hair loss too. Even sudden gain or loss of weight, high fever or a death in the family could lead to such hair loss.</li>
                        <li><strong>Lifestyle –</strong> When hair is pulled back really tight or tied up too tight, or when too many hair styling products are used, there can be loss of hair. Even when you use too much heat on your hair, you could damage the hair follicles, leading to hair loss.</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Hair Loss Treatment in Men and Women</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Hair Loss Treatment in Men and Women</h3>
                    <p>For a proper treatment, the situations of loss of hair and causes have to be evaluated very cautiously. The highly experienced doctors at AK Clinics make sure that each effort is directed correctly for outstanding results. They follow an extremely scientific & structured approach to Hair Loss treatment.</p>
                    <p class="text-center">
                    <img class=" img-fluid lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>training-hair-transplant.png" alt="Methodology of Hair Loss Treatment" width="" height=""></p>
                    <h4>Tests &amp; Diagnosis:</h4>
                    <p>There are a few basic tests which will be conducted and these will allow the doctor to offer a proper diagnosis:</p>
                    <ul>
                        <li>A blood test will allow the doctor to uncover any and all medical conditions, including hyperthyroidism.</li>
                        <li>A pull test will enable the doctor to know how far along or how severe the hair loss is.</li>
                        <li >A scalp biopsy, in which the doctor will scrape some skin from the scalp, will help ascertain the presence of an infection.</li>
                        <li>A tool known as light microscopy will allow the doctor to examine the base of the hair, which in turn will allow them to discover if anything is wrong with the hair shaft.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2 class="black-text text-center">AK Clinics methodology for Hair Loss Treatment</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row">
                <div class="col-md-4">
                    <h3>Evaluate the cause of Hair Loss</h3>
                    <p>It is done to find out the exact cause of hair loss and treat it in a proper way. The following points are noted in the patient:</p>
                    <ul>
                        <li>The frequency and degree of hair loss.</li>
                        <li>Stress level, diet and hormonal disturbances.</li>
                        <li>Case history of loss of hair in her/his family.</li>
                        <li>Menstrual disturbances in women</li>
                    </ul>
                    <p>Some investigations like <em>Serum Iron level</em>, <em>Prolactin levels</em>, <em>Haemogram</em>, <em>Thyroid levels</em></p>
                </div>
                <div class="col-md-4">
                    <h3>Hair Analysis Test</h3>
                    <p>Assessment of degree of hair loss by hair analysis machine Caslite. It is the most advanced device of hair analysis in the whole world. This is a general test and is a must for all those suffering from hair loss. Primarily it assesses the :</p>
                    <ul>
                        <li>Hair density</li>
                        <li>Hair thickness</li>
                        <li>Grade of loss of hair</li>
                        <li>The ratio of Telogen-Anagen</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3>Plan a treatment action plan</h3>
                    <p>After the complete diagnosis is established, the doctors decide about the course of action to be followed for the patient. Our experienced counselors guide the patients very patiently to ensure that they chose the right kind of treatment. The treatment plan is normally as follows:</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text text-center">Non-surgical Treatment for Hair Loss</h2>
                    <p>Today, there are numerous methods of handling hair loss and the most common ones include medication, surgery, laser therapy as well as wigs and hairpieces. However, there are certain conditions, such as alopecia universalis, where most of these treatments might not prove to be effective.</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row">
                <div class="col-md-6"><p><strong>Wigs and hairpieces</strong> are perhaps the easiest way to tackle hair loss, especially if you are not looking forward to surgery or long term medication. These days, there are several natural looking hair pieces on offer, which will allow you to maintain a normal look. This is especially a good idea if the hair loss is temporary.</p>
                    <p>If your hair loss is just started, then you could consider low level laser therapy, which will instigate the healthy hair follicles to jump into action. There are certain laser devices which have been approved by FDA and have shown tremendous results. Perhaps the most positive aspect of laser therapy is the fact that there are almost no side effects.Today, there are certain medications, which have managed to improve the rate of hair loss including Minoxidil and Finasteride. Both of these have been approved by the FDA and while the former is normally available in the form of a topical lotion the latter is a pill that can be taken orally.</p></div>
                <div class="col-md-6"><p> In most cases, the medications have shown positive effects, while there have been a few side effects too such as scalp irritation and diminished sex drive</p>
                    <p>And then there are certain cases, where the only way out is the surgical one – which actually refers to hair transplant. Depending on whether you are undergoing a <a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/" target="_blank">follicular unit transplant</a> or follicular unit extraction, the doctor will harvest follicles from your head or other part of the body and transplant them into the area where the hair loss is most apparent. Given that it is a surgical procedure, there will be some amount of pain and the recovery will also take time. However, the results will be much more natural and long lasting too.</p></div>
            </div>
            <p class="text-center ptb-3">
                <a class="btn btn-outline " href="<?= base_url(); ?>mesotherapy/" target="_blank">Mesotherapy</a>&nbsp;
                <a class="btn btn-outline" href="<?= base_url(); ?>hair-gain-therapy/" target="_blank">Special Medical Therapy</a>&nbsp;
                <a class="btn btn-outline" href="<?= base_url(); ?>hair-gain-therapy/" target="_blank"> Low Level Laser Therapy</a>&nbsp;
                <a class="btn btn-outline" href="<?= base_url(); ?>hair-gain-therapy/" target="_blank">Bio-therapy</a>&nbsp;
                <a class="btn btn-outline" href="<?= base_url(); ?>hair-gain-therapy/" target="_blank">Oral &amp; Topical Therapy</a></p>
        </div></div>
</div>
</div>

<div class="service-area ptb-3 text-center bg-light-gray">
    <div class="container">
        <div class="col-md-12 text-center"> 
            <div class="row">
                <div class="section-title">
                    <h2 class="black-text text-center">Surgical Treatments for Hair Loss</h2>
                </div>
            </div>
        </div></div><div class="container-fluid">
        <p class="content-center">
            AK Clinics is India’s leading &amp; pioneer Hair Transplant center. 
            <a href="<?= base_url(); ?>about-us/our-team/dr-kapil-dua/">Dr. Kapil Dua</a> &amp; 
            <a href="<?= base_url(); ?>about-us/our-team/dr-aman-dua/">Dr Aman Dua</a> 
            were instrumental in making FUE Hair Transplant come to India in the year 2007. 
            Following are the kind of transplants we provide
        </p>
        <p class="ptb-1">
            <a class="btn btn-outline" href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a>&nbsp;
            <a class="btn btn-outline" href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/" target="_blank">FUT Hair Transplant</a>&nbsp;
            <a class="btn btn-outline" href="<?= base_url(); ?>hair-transplant/bio-fue/" target="_blank">Bio-FUE Hair Transplant</a>&nbsp;
            <a class="btn btn-outline" href="<?= base_url(); ?>hair-transplant/bio-dht/" target="_blank">Bio-DHT Hair Transplant</a>&nbsp;
            <a class="btn btn-outline" href="<?= base_url(); ?>hair-transplant/giga-session/" target="_blank">Giga-Sessions</a>
        </p>
    </div>
</div>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>