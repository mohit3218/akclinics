<?php $this->load->view('front/partials/hair_loss_restoration_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-restoration/"> <span itemprop="name">Hair Restoration</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled  anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>artificial-hair-restoration/"> <span itemprop="name">Artificial Hair Restoration</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Hair Patches & Extensions</h1>
                </div>
                <h4>What is Artificial Hair Restoration?</h4>
                <p >Hair is an important part of a person’s physical image and when someone starts losing the same, there is an obvious worry. For most people, losing hair is not only embarrassing, but also a dent to their confidence. This is why, when someone starts losing hair, they want to find a way to cover it up. </p>
                <p> While the most permanent solution is a <a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a>, this is something that often scares a lot of people. They feel that a surgery will be too expensive or it will hurt a lot, which is why they are not too enthusiastic about the same. However, they still want to have a solution for hair loss which will not only look natural, but also last for some time. </p>
                <p>This is where our solutions for artificial hair restoration, come into the picture. What the world might see as patches and extensions, are actually excellent options to cover hair loss, without any surgery and over the past few years, we have gained somewhat an expertise in the same. Our patches and extensions are designed in such a manner that they will sit well with your natural hair and offer a look that you will love to flaunt.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-restoration/">Hair Restoration</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>prp-treatment/">PRP Therapy</a></li>
                        <li><a href="<?= base_url(); ?>hair-loss-treatment-men-women/">Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>hair-restoration/scalp-micro-pigmentation-india/">Scalp Micro Pigmentation</a></li>
                        <li><a href="<?= base_url(); ?>mesotherapy/">Mesotherapy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area bg-light-gray ptb-3 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2 class="black-text text-center">Artificial Hair Restoration</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center m-5">
                <figure><img class="img-fluid lazy" data-src="<?= cdn('assets/template/uploads/'); ?>2015/10/%5EFC78A6667E03F21EEDC6CFC5FAA0BDBFBC105B433E9B1FDF66%5Epimgpsh_fullsize_distr.png" alt="Artificial Hair Restoration Clinic"></figure>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">Hair Patches</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <p>Hair Patch is an artificial hair restoration method and is one of the most sophisticated technologies in the modern field of non-clinical hair replacement. Hair patch treatment is a non-surgical treatment and these patches are quite an effective method of hair restoration for the individuals who do not want to undergo a surgical procedure of hair restoration or do not have the sufficient funds for a hair transplant surgery. Hair patch is placed on the area of baldness. Usually, these hair patches are stacked with the help of cosmetic glue or even some clips.At AK Clinicsthe moment, </p>
                <h4>we offer 3 kinds of patches, namely:</h4>
                <ul>  <li>Cheveux Basic</li>
                    <li>Cheveux Nettoyer</li>
                    <li>Cheveux Naturels</li></ul>
            </div>
            <div class="col-md-6">
                <p>We specially design the hair patch that will perfectly fit the patient’s scalp and it will look absolutely natural. You will end up looking much better than your previous looks. These hair patches are designed in such a way that they will complement your existing hair completely including same hair texture, hair color and same density. These patches or glues for stacking them are completely safe and will not cause any allergic reactions to the scalp. The hair patch is usually smaller as compared to the traditional hair wig and is designed in such a manner that it will sit on your bald patch completely. You can comb or style them as the way you want and you can resume your everyday chores easily.</p>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">Hair Extensions</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <p>Yet again, we are leaders in the field of offering hair extensions, and with our range, you will have the kind of hair length that you have always wanted. As a matter of fact, with our extensions in place, you will be able to truly “play around with your hair”! Whether you have fine hair or hair that is really short, our extensions will allow you to have hair that looks truly transformed.</p>
                <p>Our procedure is so much gentler on the hair, because we do not utilise any glues or waxes to put the extensions in place. There are not even any weaving procedures required, because the extensions will simply link onto an existing lock of hair. What is great about our extensions is that they can be used over and over again, and you can have your hair looking thicker and longer all the time, without it looking unnatural at all.</p>
            </div>
            <div class="col-md-6">
                <p> As a matter of fact, your hair extensions will be so natural that you will get to colour them, style them or even perm them, the way you like. You can either choose from an extensive range of readymade extensions or we will customise them to your liking.</p><p>You will be able to wash these extensions in a normal manner and we would encourage you to keep them clean and moisturise them too. With a natural bristled brush, you can comb your hair all day long and have little to worry about. It would be ideal that you loosely plait your hair, while sleeping, exercising or when you are going swimming.</p>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                    <p>Here are some of the most commonly asked questions regarding artificial hair restoration:</p>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Is Artificial Hair Restoration Safe?</a> </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">If the procedure has been done at a recognised clinic under the supervision of an experienced doctor, there are little to no chances of anything going wrong.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">How Does Artificial Hair Restoration Differ From Surgical Methods?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">In non-surgical hair restoration, there is no surgery or needles. Prosthetics or hair pieces will be created for you and those will be fitted on the head or areas with maximum balding.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Will The Restoration Process Cause Any Allergies?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">The hair prosthetic will be created in accordance with the person, which means that the hair type and colour as well as texture will be in correspondence with their actual hair. The hair used will normally be human and will have been sanitised properly. The prosthetic will be kept in place either using clips or cosmetic glue, both of which normally do not cause any allergies.</div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">What Kind Of Timeline Does Artificial Hair Restoration Require?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">While in a surgical procedure, you would have to invest at least three to five months, to start seeing any actual effects, in an artificial hair restoration process, it is immediate after proper setting. In order to design the prosthesis, the doctor will need about a week or a little more, and the setting or fitting process will not take more than a few hours.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">Can Complete Baldness Too Be Covered?</a> </div>
                        <div id="collapsefive" class="collapse" data-parent="#accordion">
                            <div class="card-body">With advanced techniques of artificial hair restoration, covering complete baldness is actually possible.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsesix"> How Does The Procedure Actually Work And Will It Affect The Already Present Hair?</a> </div>
                        <div id="collapsesix" class="collapse" data-parent="#accordion">
                            <div class="card-body">An artificial prosthesis will be created, in tandem with the natural texture and colour of your hair and this will be held in place using clips, tapes or glue, all of which will be skin friendly. And no, your natural hair will not be affected in any which way, although they might have to be trimmed to allow better setting of the prosthetic.</div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseseven">Will I My Hair Look Natural And Will I Be Able To Style It In A Manner That I Like?</a> </div>
                        <div id="collapseseven" class="collapse " data-parent="#accordion">
                            <div class="card-body">Once the prosthetic has been set properly, it will look absolutely natural and you will get to treat it like normal hair. You will be able to wash and condition it, use styling products and cut them in a manner that you feel suits you. Since the hair used to create the prosthetic is actual human hair, you will also be able to colour or straighten them.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseeight">Are Women Candidates For Artificial Hair Restoration Too?</a> </div>
                        <div id="collapseeight" class="collapse" data-parent="#accordion">
                            <div class="card-body">Absolutely – women too can get artificial hair restoration done.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsenine">What Kind Of Maintenance Will Be Required?</a> </div>
                        <div id="collapsenine" class="collapse" data-parent="#accordion">
                            <div class="card-body">The maintenance is pretty much the same as is with normal hair, which means that you can wash them, condition them and brush them daily.</div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseten">Will I Be Able To Indulge In Physical Activity?</a> </div>
                        <div id="collapseten" class="collapse" data-parent="#accordion">
                            <div class="card-body">Once the prosthetic is in place, you will be able to do anything and everything, from dancing to swimming!</div>
                        </div>
                    </div>



                </div>
            </div>

            <div class="col-md-6" id="testimonails">
                <div class="col-md-12 text-center">
                    <h2>Testimonials</h2>
                </div>
                <div id="carouselExampleIndicators" class="carousel slide text-white" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
                    </ol>
                    <div class="carousel-inner mt-4  black-text">
                        <div class="carousel-item text-center">
                            <div class="img-box p-1 border rounded-circle m-auto"> 
                                <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>raj-n.jpg" alt="RAJ JASWAL - Hair Transplant Patient at AK Clinics"> </div>
                            <h5 class="mt-4"><strong class=" text-uppercase">Raj Jaswal</strong></h5>
                            <p class="m-0 pt-2">
                                I started facing hair loss at the age of 26 but it was not very noticeable. But at 29 
                                the hair loss progressed very quickly and crown almost became empty. And further my 
                                hair line receded quickly. Now at the age of 31 I started looking very old. 
                                I tried many medicines like homeopathic etc etc but nothing seems to work. 
                                Then I started searching for my options and finally decided for a hair transplant.
                            </p>
                        </div>
                        <div class="carousel-item">
                            <div class="img-box p-1 border rounded-circle m-auto"> 
                                <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg " alt="VIKALP GAUR - Hair Transplant Patient at AK Clinics"> </div>
                            <h5 class="mt-4 "><strong class="text-uppercase">Vikalp Gaur</strong></h5>
                            <p class="m-0 pt-2 text-black">
                                I can see good results post-transplant. Overall good. As suggested, I may go for 
                                further transplant for crown area. I am currently taking 6 months cyclical therapy 
                                again to stop hair fall. Then will take the decision accordingly. I can say I would 
                                prefer someone for A K Clinic. As it is giving person the way to live more happily.
                            </p>
                        </div>
                        <div class="carousel-item text-center active">
                            <div class="img-box p-1 border rounded-circle m-auto"> 
                                <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="Rahul - Hair Transplant Patient at AK Clinics"> </div>
                            <h5 class="mt-4"><strong class="text-uppercase">Rahul Bhatia</strong></h5>
                            <p class="m-0 pt-2">
                                Had a hair transplant at AK clinic under care of Doc. KAPIL DUA.And after treatment it 
                                has been 3 month now and I have very much inproved growing hair. He is best surgeon 
                                ever seen and very experienced in this art. They also provided very good atmosphere 
                                while surgery. With his experience he gave very confident to his patients while surgery.
                            </p>
                        </div>
                        <div class="carousel-item text-center">
                            <div class="img-box p-1 border rounded-circle m-auto"> 
                                <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="Suresh - Hair Transplant Patient at AK Clinics"> </div>
                            <h5 class="mt-4"><strong class="text-uppercase">Suresh</strong></h5>
                            <p class="m-0 pt-2 text-black">
                                After talking to Dr Kapil I have decided to go for a hair transplant surgery which turned
                                out to be very good. Total of 6000 grafts were transplanted. The density was medium 
                                density. The procedure was FUE without any scar. The procedure was by done both Dr 
                                Kapil and Dr Aman. I strongly recommend anyone who is looking for a FUE surgery in India.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/hair_transplant_blog'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>