<?php $this->load->view('front/partials/hair_loss_restoration_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-restoration/"> <span itemprop="name">Hair Restoration</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>mesotherapy/"> <span itemprop="name">Mesotherapy</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-tex">What is Mesotherapy</h1>
                </div>
                <p>Although there is a lot of talk about medical therapies such as Mesotherapy, there are not a lot of people, who actually know what these terms actually mean. Mesotherapy, a method first invented in 1948 by Frenchman Michael Pistor, derives its name from mesoderm. Mesoderm is a layer of the human skin, which is comprised mainly of fat cells. The layer sits right beneath the surface of the skin. When the method was first introduced, it was thought that it would help reduce bulges caused due to excess fat and cellulite.</p>
                <p>Mesotherapy is actually a non-invasive method, in which microinjections are administered right below the epidermis. The idea behind the therapy is that it will stimulate the mesoderm, which in turn will help alleviate a range of medical conditions. The solution that is injected into the mesoderm is a combination of several ingredients and the combination will vary from person to person, as it will be customised. However, there will be minerals, vitamins, amino and nucleic acids as well as co-enzymes in this medical cocktail.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-restoration/">Hair Restoration</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>prp-treatment/">PRP Therapy</a></li>
                        <li><a href="<?= base_url(); ?>hair-loss-treatment-men-women/">Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>artificial-hair-restoration/">Artificial Hair Restoration</a></li>
                        <li><a href="<?= base_url(); ?>hair-restoration/scalp-micro-pigmentation-india/">Scalp Micro Pigmentation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">What can Mesotherapy be used for?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What can Mesotherapy be used for?</h3>
                    Mesotherapy was originally designed to handle and reduce excess weight, but today it is being used extensively to battle hair loss. Tests and studies have shown that not only can it delay pattern baldness, but also help with the regrowth of hair.
                </div>

                <div class="tab">
                    <button class="tab-toggle">The Treatment Procedure</button>
                </div>
                <div class="content">
                    <h3 class="m-3">The Treatment Procedure</h3>
                    <p class="ptb-1">Mesotherapy is not just chemical or physical – rather it is a combination of both. Areas have to be demarcated on the scalp beforehand and a special blend of vitamins, minerals and amino acids are injected into the mesoderm. The chemical cocktail works like a bullet, which hits the nail on its head. </p>
                    <ul class="text-left">
                        <li>A microinjection is used to deliver the medication to the demarcated parts of the scalp and this leads to the creation of elastin as well as collagen. With new collagen and elastin, there emerges the scope for growth of new hair. </li>
                        <li>There are two main active ingredients – Acetyl tetrapeptide-3 and Biochanin A. While AT3 will help reduce the inflammation, which will in turn allow for new hair to grow, the latter can help with the modulation of the conversion of testosterone. AT 3 also stimulates the remodelling of tissues, which will affect the manner in which the hair follicles grow. This signal for remodelling will ensure that the hair anchors better and has a longer and healthier life.</li>
                        <li>Generally, there is a time release system in place, which helps improve blood circulation, stimulate the collagen, neutralise the effects of DHT, increase the size of the hair follicles, which will reduce the loss of hair and finally, stimulate the growth of new hair. </li>
                        <li>The active ingredients are meant to stimulate the organs and depending on the quality of the ingredients used the hair loss should reduce and new hair growth should start.</li>
                    </ul>
                    <p class="clr">The actual procedure takes only about 15 to 30 minutes, depending on the area that needs to be covered. In most cases, at least ten sessions are required and the same has to be spaced out. While the first sessions need to be every two weeks, the subsequent ones can be spaced over two to three months. You will also need to undergo maintenance treatments, to make sure that your hair continues to look great. </p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">How it Really Works and Helps</button>
                </div>
                <div class="content">
                    <h3 class="m-3">How it Really Works and Helps</h3>
                    <p>It is important to understand that facial hair features such as moustache, beard and eyebrows are a part of what constitute a face. when these facial hair are not in alignment, the face will look odd, which is why when there is hair thinning, irregular shaping of the facial hair or loss of hair due to an accident, and people are able to notice it immediately.</p>
                    <ul class="">
                        <li>The combination of medicines that will be used for you will have been created especially for you, keeping your specific type of hair loss in mind. With the improved blood circulation, you will notice that your hair follicles feel more nourished and this will mean better hair growth. </li>
                        <li>Over time, the DHT hormone will also start getting neutralised, which in turn will help hair to grow again. </li>
                        <li>People who have tried Mesotherapy have not required hair transplant surgeries.</li>
                        <li>The therapy is almost painless and there is no requirement for any dressing or sedation.</li>
                        <li>Combine the therapy with a good diet and a reasonable amount of exercise and you will soon have healthy hair. </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3">
<div class="container ">
  <div class="section-title">
<h2 class="black-text text-center">Mesotherapy Results – Before &amp; After</h2>
</div>
<figure class="ptb-3 text-center">
	<img alt="Mesotherapy Before and After result" data-src="<?= cdn('assets/template/uploads/2015/06/'); ?>mesotherapy1.jpg" class="img-responsive center lazy"></figure>
</div>
</div>
<div class="bg-light-gray">
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>