<?php $this->load->view('front/partials/hair_loss_restoration_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-restoration/"> <span itemprop="name">Hair Restoration</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>prp-treatment/"><span itemprop="name">PRP Hair Treatment</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<span itemtype="https://schema.org/MedicalProcedure" itemscope="">
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-tex" itemprop="name">PRP (Platelet Rich Plasma) Therapy for Hair Loss Treatment</h1>
                </div>
                <h4 class="">Here’s Why PRP Therapy Is The Way To The Future!</h4>
                <p>
                    Recent years have brought forth a range of new treatments and therapies for people who are having hair trouble. 
                    While a hair transplant is a good choice for people who have large bald spots, there are those who are not ready for such an 
                    invasive procedure and wish to look at other avenues first. PRP or Platelet Rich Plasma therapy is one such method that is 
                    fast gaining a lot of popularity with people with hair loss.
                </p>
                <h4>What is PRP?</h4>
                <p>
                    Platelet rich plasma or PRP is present in human blood. Platelets are the clotting cells of our blood having potential of faster healing. Platelets release growth factors in the blood which augments the healing process. <span itemprop="description">PRP or Platelet Rich Plasma Therapy is a non-surgical and therapeutic option for people suffering from hair loss. As a matter of fact, this is the best option for people who are looking at a method of restoring hair, at a point when the loss has just started setting in. </span>if the hair loss is extreme, then <a href="<?= base_url(); ?>hair-transplant/" target="_blank">hair transplant</a> might be the only option, but for those who have just started noticing baldness or thinning of hair, this might be a great option. 
                </p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-restoration/">Hair Restoration</a></li>
                        <li class=""><a href="<?= base_url(); ?>hair-gain-therapy/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>hair-loss-treatment-men-women/">Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>artificial-hair-restoration/">Artificial Hair Restoration</a></li>
                        <li><a href="<?= base_url(); ?>hair-restoration/scalp-micro-pigmentation-india/">Scalp Micro Pigmentation</a></li>
                        <li><a href="<?= base_url(); ?>mesotherapy/">Mesotherapy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center  pt-1">
                <div class="section-title  ">
                    <h2 class="black-text">How PRP works?</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12" itemprop="howperformed">
            <div class="row text-center">
                <div class="col-md-3">
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/prp-11.png" alt="Blood Sampling" class="rounded-circle lazy"><br>
                    <h4>Blood Sampling</h4>
                    <p>Blood is collected from the body using syringe before the treatment. Blood sample is collected approximately 30 minutes before the PRP therapy. Around 40-60cc of blood is drawn from the body using sterilized needles and is transferred into the vials.</p>
                </div>
                <div class="col-md-3">
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/prp-12.png" alt="Separating platelets" class="rounded-circle lazy"><br>
                    <h4>Separating platelets</h4>
                    <p>Blood is then spun in the centrifuge for few minutes and blood components are separated. After centrifugation, sample is kept in specialized refrigerator ensuring that there will be no risk of contamination in the sample.</p>
                </div>
                <div class="col-md-3">
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/prp-13.png" alt="Plasma Concentration &amp; Growth Factors" class="rounded-circle lazy"><br>
                    <h4>Plasma Concentration &amp; Growth Factors</h4>
                    <p>After centrifugation, plasma is divided into three parts i.e. platelet poor plasma, platelet rich plasma and red blood cells. Growth factors are separated and diluted in plasma and used for injections</p>
                </div>
                <div class="col-md-3">
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/prp-14.png" alt="PRP Injection" class="rounded-circle lazy"><br>
                    <h4>PRP Injection</h4>
                    <p>Platelet rich plasma is then transfused into scalp carefully. This is almost a painless procedure. Growth factors present in the plasma help to stimulates the stem cells in the body and help to prolong the growth phase of existing hair, reduces hair fall and thicken hair</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">PRP Hair Treatment Cost</button>
                </div>
                <div class="content">
                    <h3 class="m-3">PRP Hair Treatment Cost</h3>
                    <p>PRP hair treatment cost in India will vary from clinic to clinic and will also be dependent on other factors such as the equipment used, experience of the doctor, the number of sessions you will require and the number of injections you might need.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Why PRP Therapy?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Why PRP Therapy?</h3>
                    <p>PRP helps in natural healing. There will be no risk of any infection as plasma used is drawn from your own blood. There is no chance that your body will oppose the procedure. Moreover the procedure is not lengthy and will be completed in just 1 to 2 hours.</p>
                    <ul>
                        <li>New Hair growth in 2-3 sessions</li>
                        <li>Increase & thickening Hair growth in 6-8 sessions</li>
                        <li>No Cuts, No Scars, No Surgery</li>
                        <li>Natural Hair. You can style the way you want</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">What should be keeping in mind before going for PRP therapy?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What should be keeping in mind before going for PRP therapy?</h3>
                    <p>PRP therapy is a simple procedure which does not require any special care before therapy. But still there are few things that you have to take care off.</p>
                    <ul>
                        <li>If you are using any blood thinners, stop using them before 5-7 days.</li>
                        <li>Wash head before antibacterial shampoo before treatment.</li>
                        <li>Avoid smoking for at least a week before treatment.</li>
                        <li>Avoid consumption of alcohol.</li>
                        <li>Have healthy breakfast on the day of treatment.</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">PRP specific cells that cause hair growth include</button>
                </div>
                <div class="content">
                    <h3 class="m-3">PRP specific cells that cause hair growth include</h3>
                    <figure><img class="rounded img-fluid lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/platelts.png" alt="PRP cells that cause hair growth include"></figure>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center ">
                <div class="section-title">
                    <h2 class="black-text">Benefits of PRP</h2>
                </div>
                <p>There are actually innumerable reasons why a growing number of people are opting for PRP therapy and why even doctors are suggesting the same. The major ones include:</p>
            </div>
        </div>
        <div class="row  text-center  pt-1">
            <div class="col-md-3">
                <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/prp-benifit-1.jpg" alt="Texture" class="rounded-circle lazy"> </figure>
                <h4>Texture</h4>
                <p>of transplanted hair is improved. Hair will be smooth and soft after the treatment.</p>
            </div>
            <div class="col-md-3">
                <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/prp-benifit-12.jpg" alt="Hair Growth" class="rounded-circle lazy"> </figure>
                <h4>Hair Growth</h4>
                <p>Your hair growth will be boosted. You will notice good hair growth after the whole treatment.Reduction &amp; Stoppage of hair fall</p>
            </div>
            <div class="col-md-3">
                <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/prp-benifit-3.jpg" alt="Surgical Boost" class="rounded-circle lazy"> </figure>
                <h4>Surgical Boost</h4>
                <p>Helps in the faster healing after hair transplant both in donor area and recipient area</p>
            </div>
            <div class="col-md-3">
                <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/prp-benifit-4.jpg" alt="No Infections" class="rounded-circle lazy"> </figure>
                <h4>No Infections</h4>
                <p>Given that your blood will be used, there are no chances of any infections or allergies either, increasing the possibilities of success.</p>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">Side effects of PRP</h2>
                </div>
                <p>Because the procedure is done using the patient’s own blood, the chances of anything going wrong are minimal, but as is the case with any medical procedure there are the possibilities of a few side effects:</p>
            </div>
        </div>
        <div class="row  text-center pt-1">
            <div class="col-md-3">
                <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-11.jpg" alt="Pain after PRP " class="rounded lazy"> </figure>
                <p>Even though the entire procedure is done under anesthesia, some people tend to complain pain. However, the tenderness does tend to subside in a matter of hours, or at the most, within a day.</p>
            </div>
            <div class="col-md-3">
                <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-21.jpg" alt="Bleeding" class="rounded lazy"> </figure>
                <p>For certain people, there can be a tiny amount of bleeding from the sites, where the injections were administered. This too should subside in a matter of hours.</p>
            </div>
            <div class="col-md-3">
                <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-3.jpg" alt="Redness and swelling" class="rounded lazy"> </figure>
                <p>In some cases, there is a little redness and swelling at the injection sites, but these too will not last for more than a few hours, or at the most a day.</p>
            </div>
            <div class="col-md-3">
                <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-4.jpg" alt="Headache after PRP class="rounded lazy"> </figure>
                <p>In some cases, people have complained of a headache or heaviness in head, but generally it does not last for more than a day or two.</p>
            </div>

            <p class="ptb-3">Some people have also complained of tightness in their scalp, and this emerges immediately after the injections. This feeling should last for at the most one day.</p>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-6 ">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">What Can I Expect From The Treatment?</a> </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body"> You can expect reduction/stoppage of hair fall, new hair growth along your existing hair line and improved density, health and texture of your pr-existing hair.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Can Woman Get PRP Therapy?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">It is equally effective in both men and women and works best in patients with diffuse thinning and miniaturized hair and ongoing hair fall</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">How Many Treatment Sessions Will I Need?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body"> PRP therapy can be repeated every 1-2 months and usually 3-4 sittings are sufficient, though some patients may require maintenance therapy once a year or more frequently depending upon the severity of hair loss. The visible results in form of new hair growth can be seen after 3-4 months of treatment and further improvement is noticed in the subsequent months.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Can I Use Other Medicines Along With The Therapy?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body"> Yes. You can use other medicines along with the PRP therapy. PRP therapy is a non-surgical procedure. You can use any other topical or oral medications.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">How Long Does The Treatment Take?</a> </div>
                        <div id="collapsefive" class="collapse" data-parent="#accordion">
                            <div class="card-body">PRP therapy is a lunch time procedure and it takes around 1 to 2 hours for the procedure and you can go back to your work place immediately after the procedure.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsesix">How Does PRP Work?</a> </div>
                        <div id="collapsesix" class="collapse" data-parent="#accordion">
                            <div class="card-body">Blood is taken from your body and the powerful Platelet Rich Plasma is extracted from the blood. This plasma is then fortified with other growth factors and then injected back into your body. The platelets promote hair growth, leading to better results, especially after a hair transplant session.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseseven">Can PRP Therapy Be Combined With Hair Transplant?</a> </div>
                        <div id="collapseseven" class="collapse " data-parent="#accordion">
                            <div class="card-body">Yes, it can be combined with hair transplant – pre-operatively, intra-operatively, or post-operatively. It not only helps in healthier and faster growth of transplanted hair, but also stimulates the existing thinning hair at the same time.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseeight">How Many Treatment Sessions Will I Need?</a> </div>
                        <div id="collapseeight" class="collapse" data-parent="#accordion">
                            <div class="card-body">PRP therapy can be repeated every 1-2 months and usually 3-4 sittings are sufficient, though some patients may require maintenance therapy once a year or more frequently depending upon the severity of hair loss. The visible results in form of new hair growth can be seen after 3-4 months of treatment.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsenine">Are There Any Side-Effects Of PRP Therapy?</a> </div>
                        <div id="collapsenine" class="collapse" data-parent="#accordion">
                            <div class="card-body">No. As blood is drawn from your own body, you will not get any type of infection or reaction. Your immune system will not reject this treatment. PRP therapy is safe and natural process. However one can get mild redness, bruising or headache which responds well to routine analgesics.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" id="testimonails">
                <div class="col-md-12 text-center">
                
                    <h2>Testimonials</h2>
                </div>
                <div id="carouselExampleIndicators" class="carousel slide text-white" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
                    </ol>
                    <div class="carousel-inner mt-4 black-text">
                        <div class="carousel-item text-center">
                            <div class="img-box p-1 border rounded-circle m-auto"> 
                                <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>raj-n.jpg" alt="First slide"> </div>
                            <h5 class="mt-4"><strong class=" text-uppercase">Raj Jaswal</strong></h5>
                            <p class="m-0 pt-2">
                                I started facing hair loss at the age of 26 but it was not very noticeable. But at 29 
                                the hair loss progressed very quickly and crown almost became empty. And further my 
                                hair line receded quickly. Now at the age of 31 I started looking very old. 
                                I tried many medicines like homeopathic etc etc but nothing seems to work. 
                                Then I started searching for my options and finally decided for a hair transplant.
                            </p>
                        </div>
                        <div class="carousel-item">
                            <div class="img-box p-1 border rounded-circle m-auto"> 
                                <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>supal.jpg " alt="First slide"> </div>
                            <h5 class="mt-4 "><strong class="text-uppercase">Vikalp Gaur</strong></h5>
                            <p class="m-0 pt-2 text-black">
                                I can see good results post-transplant. Overall good. As suggested, I may go for 
                                further transplant for crown area. I am currently taking 6 months cyclical therapy 
                                again to stop hair fall. Then will take the decision accordingly. I can say I would 
                                prefer someone for A K Clinic. As it is giving person the way to live more happily.
                            </p>
                        </div>
                        <div class="carousel-item text-center active">
                            <div class="img-box p-1 border rounded-circle m-auto"> 
                                <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="First slide"> </div>
                            <h5 class="mt-4"><strong class="text-uppercase">Rahul Bhatia</strong></h5>
                            <p class="m-0 pt-2">
                                Had a hair transplant at AK clinic under care of Doc. KAPIL DUA.And after treatment it 
                                has been 3 month now and I have very much inproved growing hair. He is best surgeon 
                                ever seen and very experienced in this art. They also provided very good atmosphere 
                                while surgery. With his experience he gave very confident to his patients while surgery.
                            </p>
                        </div>
                        <div class="carousel-item text-center">
                            <div class="img-box p-1 border rounded-circle m-auto"> 
                                <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="First slide"> </div>
                            <h5 class="mt-4"><strong class="text-uppercase">Suresh</strong></h5>
                            <p class="m-0 pt-2 text-black">
                                After talking to Dr Kapil I have decided to go for a hair transplant surgery which turned
                                out to be very good. Total of 6000 grafts were transplanted. The density was medium 
                                density. The procedure was FUE without any scar. The procedure was by done both Dr 
                                Kapil and Dr Aman. I strongly recommend anyone who is looking for a FUE surgery in India.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<span itemprop="sameAs" content="//en.wikipedia.org/wiki/Management_of_hair_loss#Platelet-Rich_Plasma"></span>
<span itemprop="sameAs" content="//meshb.nlm.nih.gov/record/ui?ui=D053657"></span>
<span itemprop="Code" content="D053657"></span>
</span>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>

