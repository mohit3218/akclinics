<?php $this->load->view('front/partials/hair_loss_restoration_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-restoration/"><span itemprop="name">Hair Restoration</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-restoration/"> <span itemprop="name">Hair Restoration</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>hair-restoration/scalp-micro-pigmentation-india/"><span itemprop="name">Scalp Micro Pigmentation India</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-tex">SMP – an alternate styling option of hair transplant</h1>
                </div>
                <p class="text-justify">
                    Scalp micro pigmentation (SMP) is a technique to create series of tiny dots with natural, nontoxic ink onto the scalp in between hairs or on completely bald area to give a look of tiny hairs. The purpose is to cover up the areas of the scalp where the skin is prominently visible due to the baldness, thinning hair, scarring or any other form of alopecia so it can be used between the hair give it a dense look or
                    on a bald area to create a shaven look. The procedure is non-invasive and cost-effective. The results are immediate, realistic, and hassle-free. Scalp Micro pigmentation can restore even a completely bald head to look like a full head of well-groomed hair.
                    Scalp Micro pigmentation is suitable for all <a href="<?= base_url(); ?>hair-loss/"> types and stages of hair-loss</a>, on all ages, colours and skin type.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-restoration/">Hair Restoration</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>prp-treatment/">PRP Therapy</a></li>
                        <li><a href="<?= base_url(); ?>hair-loss-treatment-men-women/">Hair Loss Treatment</a></li>
                        <li><a href="<?= base_url(); ?>artificial-hair-restoration/">Artificial Hair Restoration</a></li>
                        <li><a href="<?= base_url(); ?>mesotherapy/">Mesotherapy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Method of SMP</button>
                </div>
                <div class="content">
                    <h3 class="m-3">How does SMP work?</h3>
                    <p>
                        The creation of tiny dots, also known as “replication of follicles” usually requires 2-3 
                        sessions with the gap of 2-4 weeks between each sessions. One session can take upto 3-4 hours
                        depending upon the area to be covered. During the procedure, a cartridge containing 1-3 
                        needles working in symphony gently inject natural pigment very superficially into the skin 
                        (the 2nd dermal skin layer). Pigment spacing is determined by replicating the remaining 
                        natural hair follicles. Sometimes different shades of ink are also used to give soft and 
                        natural look to hair line, temples and side burn.The process allows a very gradual and 
                        subtle change in the appearance which is rarely identified even by close friends and family.
                    </p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">How does SMP work?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">How does SMP work?</h3>
                    <p>
                        The human head has on average 200 hair per centimetre square. When thinning or hair loss 
                        occurs, it can increase the visibility of scalp skin through the hair. Such thing can be 
                        very depressing for any person and in severe cases; it can affect the confidence of that 
                        person. By creating tiny black to grey dots on the scalp, it creates an illusion of fullness 
                        on the head. The visibility of the scalp skin through hair is reduced to and thus it reduces 
                        the unwanted attention of other person to balding scalp. It can also be used on completely 
                        bald scalp to create an illusion of shaven look.
                    </p>
                    <ul>
                        <li>Give the look of a full, youthful head of cropped hair</li>
                        <li>Simulate a full-front, side and/or rear hairline</li>
                        <li>Restore hairlines on part-bald or fully bald heads</li>
                        <li>Camouflage – permanently – the symptoms of all levels of alopecia</li>
                        <li>Camouflage scarring resulting from previous hair transplant surgery</li>
                        <li>Hide scars, burns and birthmarks</li>
                        <li>Boost the visual effect of a hair transplant</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Who should get SMP done?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Who should get SMP done?</h3>
                    <p>PRP helps in natural healing. There will be no risk of any infection as plasma used is drawn from your own blood. There is no chance that your body will oppose the procedure. Moreover the procedure is not lengthy and will be completed in just 1 to 2 hours.</p>
                    <ul class="">
                        <li>Androgenic alopecia:
                            <ol>
                                <li>Higher grade of baldness where sufficient donor area is not there so surgery is not possible.</li>
                                <li>Patient already under gone multiple sessions of surgeries and now further surgery is not possible but still balding is present or not satisfied with the result or density.</li>
                                <li>Patient with diffuse thinning</li>
                                <li>Patient with contraindications for <a href="<?= base_url(); ?>hair-transplant/" target="_blank">hair transplant surgery</a> and doesn’t want to use hair patch.</li>
                                <li>Patient demanding non-surgical option.</li>
                                <li>Patients already undergone <a href="<?= base_url(); ?>hair-transplant/" target="_blank">hair transplant surgery</a> but wants more covering of scalp without undergoing another surgery</li>
                                <li>Patients who have had a strip surgery can also use it to conceal their wounds and scars.</li>
                            </ol>
                        </li>
                        <li>Extensive Alopecia areata (alopecia totalis or universalis).</li>
                        <li>Scarring alopecia and scarring from neurosurgical procedure, trauma and burns.</li>
                        <li>Alopecia due to systemic disease (e.g SLE or any other connective tissue disease) leading to diffuse thinning all over the scalp or use of cytotoxic drugs like in cancer patients or patient having condition where medical treatments fails to restore the hair and surgery is contraindicated.</li>
                        <li>Bald patient with severe liver or kidney disease or other serious immunological problem.</li>
                        <li>Alopecia due to genetic problem in hair growth.</li>
                        <li>In cases when the patient desires to restore facial hairs without undergoing surgery.</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Revision and Reversal</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Revision and Reversal</h3>
                    <p>
                        As the time passes, the area may require touch up to maintain the same look as with time the 
                        colour of pigment will fade. Further due to on-going process of baldness there may be new areas 
                        where the density is reduced and skin has started to become more visible. Such area will require 
                        “replication of follicles” to maintain the same look.<br>
                        Reversal is done in case where someone wantsto change in hair line with advancing age. Changes can 
                        include changing the hairline position, raising the hairline to create an older, more receded look, 
                        changing side hair shape, addition of sideburns and changing the hairline shape.<br>
                        The pigment goes far less deep than skin tattooing so by taking laser treatment one can remove it. 
                        It is much more straightforward and painless procedure, usually requiring only 1-2 sessions.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray ">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>