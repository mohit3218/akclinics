<style>
    .background-one{background: #f0ebd8; opacity:0.9}
    .custom-padding {padding-top: 60px !important;padding-right: 40px !important;padding-bottom: 0px !important;padding-left: 0px !important;}
    a.slide-over{ opacity:0.8; background:#ccc;}
    a.slide-over:hover{ opacity:1;}
    .addReadMore.showlesscontent .SecSec,.addReadMore.showlesscontent .readLess {display: none;}
    .addReadMore.showmorecontent .readMore {display: none;}
    .addReadMore .readMore,.addReadMore .readLess {margin-left: 2px;cursor: pointer;}
    .addReadMoreWrapTxt.showmorecontent .SecSec,.addReadMoreWrapTxt.showmorecontent .readLess {display: block;}
</style>
<?php $this->load->view('front/partials/hair_loss_restoration_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>hair-restoration/"> <span itemprop="name">Hair Restoration</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area"> 
    <div class="jumbotron">
        <div class="container text-center">
            <h1 class="black-text">Hair Restoration</h1>
            <p class="ptb-1">Over the past few years, AK Clinics has emerged as a one stop shop for all hair related issues – starting from treatment of hair fall to surgical as well as non-surgical therapies and procedures. As a matter of fact, AK Clinics has a large body of experts, who will not only take the time to understand and diagnose your exact hair issue, but also provide the most accurate and long lasting solution for the same. We believe in offering the best solutions, with least discomfort to the people seeking our help. Just some of the services that we offer include:</p>
        </div>
        <div class="col-md-12">
            <div class="regular slider">
                <div>
                    <div class="card mb-3 box-shadow">
                        <img data-src="<?= cdn('assets/template/frontend/img/'); ?>hait-restoration-img.jpg" class="card-img-top img-fluid lazy" alt="Hair Transplant">
                        <div class="card-body text-center"> <strong>Hair Transplant</strong>
                            <p class="card-text addReadMore showlesscontent">Whether you are looking for a follicular hair transplant or a follicular unit extraction, we hold an expertise in offering all the surgical hair transplant procedures. Our team of doctors and support staff have completed innumerable procedures on people of all age groups. We ensure that the patient is given proper care from start to finish – this includes proper guidance about which procedure would be most suitable, a detailed explanation of the actual procedure, information about post-operative care and also how to take care of the surgical area in the future. Our mission is to ensure that the patient is able to face life with confidence, without having to worry about their hair looking natural.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>hair-transplant/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid  lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>bio-fue-restoration.jpg" alt="BIO - FUE ">
                        <div class="card-body  text-center"> <strong>BIO - FUE <sup>TM</sup></strong>
                            <p class="card-text addReadMore showlesscontent">This path breaking procedure has been developed in-house at AK Clinics, which is our doctors are recognised not only as pioneers of the same, but also the most reputed experts. Bio FUE is a process that is similar to follicular unit extraction, in the sense that hair follicles are carefully extracted and implanted into the balding areas. However, unlike the traditional FUE procedure, we inject the surgical area with a special growth serum that contains platelets as well as germinative cells. This ensures that the implanted hair takes root faster and the surgical area becomes healthier, leading to improved hair growth.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>hair-transplant/bio-fue/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline ">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>hair-gain-therapy.jpg" alt="Hair Gain Therapy">
                        <div class="card-body  text-center"> <strong>Hair Gain Therapy</strong>
                            <p class="card-text  addReadMore showlesscontent">A combination of low level laser therapy and bio therapy, this is yet another therapy that has been pioneered in-house, which is why our doctors are the true experts in the same. Studies have shown that low level laser therapy can initiate hair growth and encourage slow hair follicles to grow at a better rate. And when this therapy is combined with the concentrated solution that contains germinative cells along with platelets, hair growth is sure to happen. We have actually helped several patients combat hair loss with this proven therapy.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>hair-gain-therapy/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline ">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>other-therapies-AKclinics.jpg" alt="other therapies">
                        <div class="card-body  text-center"> <strong>Other Therapies</strong>
                            <p class="card-text  addReadMore showlesscontent">We also offer a range of other therapies that can help with hair growth and restoration. Looking at the extent of the hair loss, we will be able to suggest from a range of non-surgical treatments and therapies such as Specialized Medical Therapy, Low-Level Laser Therapy, PRP Therapy, Mesotherapy and Scalp Micro Pigmentation. Each of these procedures has been designed to accelerate hair growth and ensure that hair loss is reduced by an obvious margin. In addition, almost all of these procedures are painless, which is why it is great for people who have just started losing their hair.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>contact-us/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline ">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>non-surgical-treatment.jpg" alt="Card image cap">
                        <div class="card-body  text-center"> <strong>Non-Surgical</strong>
                            <p class="card-text  addReadMore showlesscontent">A combination of low level laser therapy and bio therapy, this is yet another therapy that has been pioneered in-house, which is why our doctors are the true experts in the same. Studies have shown that low level laser therapy can initiate hair growth and encourage slow hair follicles to grow at a better rate. And when this therapy is combined with the concentrated solution that contains germinative cells along with platelets, hair growth is sure to happen. We have actually helped several patients combat hair loss with this proven therapy.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>contact-us/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline ">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?= base_url(); ?>assets/template/frontend/img/dr-profile-cover.jpg" style="background-image: url(&quot;<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg&quot;); ">
    <div class="container pt-0 pb-0">
        <div class="section-content">
            <div class="row">
                <div class="col-md-6 sm-height-auto">
                    <div class="background-one">
                        <h4 class="title mt-0 line-bottom line-height-2 text-black-222">BEGINNING OF THE NEW<span class="text-theme-colored"> AGE</span></h4>
                        <b class="small-separator"></b>
                        <div class="dr-kapil-quote">
                            <blockquote>
                                <p> We began out Journey of Hair Transplant in the year 2007. At that moment the surgery was in very nascent stage... we did not have great success at that time. But today we have one of the highest successes across the globe...but what has not changed the passion & ethics that made it possible  </p>
                                <p><strong>Dr. Kapil Dua</strong><br>

                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ptb-3 doctors-details-section text-white">
                    <h2>Our Complete List Of Procedures</h2>
                    <div class="row drlist">
                        <div class="col-md-6">
                            <ul class="">
                                <li><a href="<?= base_url(); ?>hair-gain-therapy/">Hair Gain Therapy</a></li>
                                <li><a href="<?= base_url(); ?>artificial-hair-restoration/">Artificial Hair Restoration</a></li> 
                                <li><a href="<?= base_url(); ?>hair-restoration/scalp-micro-pigmentation-india/">SMP</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="">
                                <li><a href="<?= base_url(); ?>prp-treatment/">PRP Treatment</a></li> 
                                <li><a href="<?= base_url(); ?>mesotherapy/">Mesotherapy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/hair_transplant_blog'); ?>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
