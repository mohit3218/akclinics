<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"> <span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/pigmentation-treatment/"> <span itemprop="name">Pigmentation Treatment</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<span itemtype="https://schema.org/MedicalCondition" itemscope="">
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text" itemprop="name">Pigmentation Treatment</h1>
                </div>
                <p itemprop="description">
                    Hyper-pigmentation of the skin resulting in skin discoloration occurs due to the excessive production of melanin. 
                    It may affect any area of the body and could be a source of great distress to the patients but most people get 
                    impacted with pigmentation on face. In some cases skin pigmentation could be a manifestation of an underlying medical 
                    condition and therefore each case of hyper-pigmentation should be evaluated thoroughly.
                </p>
                <h4 class="text-muted">Causes of dark spots on your skin</h4>
                <figure><img data-src="<?= cdn('assets/template/frontend/images/'); ?>pigmentation-cause.png" class="img-fluid lazy"></figure>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text text-center">Types of hyperpigmentation</h2>
                    <p>There are various types of hyperpigmentation but the following are the most commonly seen:</p>   </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1 text-center">
            <div class="row">
                <div class="col-md-4">
                    <h4>Sunspots</h4>
                    <p>Sunspots, which are also known as solar lentigines, are often caused due to extensive exposure to the sun. These also appear on those parts of the body, which are constantly exposed to the sun, such as the face and hands. </p>
                </div>
                <div class="col-md-4">
                    <h4>Melasma</h4>
                    <p>Melasma is often considered to be caused due to a change in the hormonal activity in the body. This is why it is most commonly seen in women, during their pregnancy. While the condition could show up in any part of the body, it is seen most commonly on the face and abdomen. </p>
                </div>
                <div class="col-md-4">
                    <h4>Post-inflammatory</h4>
                    <p>There is also something known as post-inflammatory hyperpigmentation, which is caused due to an injury to the skin. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3  ">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text text-center">Causes and Methods of Diagnosis</h2>
                    <p>The method of diagnosis of pigmentation treatment depends upon the causes of these dark spots. Following are the various causes of the pigmentation and methods of diagnosis based upon it.</p>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle ">Causes</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Causes</h3>
                    <p>There are a few basic causes for hyperpigmentation:</p>
                    <ul>
                        <li>Excess production of melanin, the pigment that gives skin the colour can cause hyperpigmentation- remove from here</li>
                        <li>Excessive exposure to the sun can cause an increase in melanin production </li>
                        <li>Certain medications, especially certain chemotherapy drugs can lead to hyperpigmentation </li>
                        <li>When there is a change in the hormonal levels due to pregnancy or any other condition, there can be an increase in the melanin production</li>
                        <li>Underlying medical conditions such as Addison’s disease tends to affect the adrenal glands, which in turn will affect the hormonal and melanin production </li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Methods of diagnosis</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Methods of diagnosis</h3>
                    <p>The main methods of diagnosis include:</p>
                    <ul>
                        <li>Wood’s Lamp: This is a device that emits UV light and is one of the most preferred methods of diagnosing hyperpigmentation. The procedure is performed in a dark room. This helps in the determination of the type and extent of the pigmentation. </li>
                        <li>Magnifying lamp: When the skin is looked at, under the magnifying lamp, the extent of the hyperpigmentation will be easy to determine. In addition, the same tool will also allow for the diagnosis of other associated skin conditions too</li>
                        <li>Skin biopsy: In situations, where the lamps are not proving to be effective enough to gauge the extent of the pigmentation, the dermatologist might have to resort to a skin biopsy. This method might be especially useful for diagnosing the associated skin disorder</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Advantage of Pigmentation Treatment</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Advantage of Pigmentation Treatment</h3>
                    <p>After a few sessions, you will start to notice that the pigments have started to fade away and the natural colouring of your skin is starting to return. In addition, the treatment can reduce the spread of the pigmentation. The treatment will also include treating the root cause of the condition, which will deter further appearance of the pigment.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text text-center">Before, during &amp; after the Pigmentation treatment</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12" itemtype="https://schema.org/MedicalTherapy" itemscope="" itemprop="possibleTreatment">
            <h3 itemprop="name">Before laser treatment for pigmentation</h3>
            <div itemprop="description">
                <p>Before treatment (laser, chemical or peel based) can be started, a detailed skin analysis will be required, because only after gauging the severity of the condition, can a course of treatment be charted out. This is why, it is important that an experienced doctor do the examination. It is also important to collect information regarding the lifestyle, medical history, consumption of certain medication and even genetics.Some of the important points of investigation will include: </p>
                <ul>
                    <li>UV exposure – Studies have shown that over exposure to sunlight can lead to pigmentation. Excessive usage of tanning beds can also lead to the condition as can, not using enough sunscreen. </li>
                    <li>Current and past medical conditions – There are several medical conditions that can contribute to the hyperpigmentation, including thyroid imbalance, Cushing’s disease, Addison’s disease, pregnancy, use of birth control pills or even hormone replacement. The doctor will also need to know about the medications that you might have taken in the past few months, because certain medications can also cause the condition. </li>
                    <li>Dermatological or surgical treatments – If you have had any surgeries or dermatological procedures in the past, it would be wise to tell your doctor about the same. </li>
                </ul>
            </div>
        </div>
        <div class="col-md-12" itemtype="https://schema.org/MedicalTherapy" itemscope="" itemprop="possibleTreatment">
            <h3 itemprop="name">During laser treatment for pigmentation</h3>
            <div itemprop="description">
                <p>Once you have been brought into the laser treatment room, here are the steps that will be followed:</p>
                <ul>
                    <li>A laser will be used and the intense light from it will be passed through the epidermis. The light will be absorbed into the skin pigments.</li>
                    <li > The energy that is emitted will break up the pigment cells and the particles hence created will be removed by the immune system of the body.</li>
                    <li > Almost immediately after the pulse of laser light has been applied, the skin will start to look grey. There could also be a little bleeding. This is part of removing pigmentation treatment. </li>
                    <li>A sign of the top layer of skin turning would be the blistering and crusting.</li>
                    <li>The time taken for the crust to fall off will depend on how deep the pigments are and also where on the body they are.</li>
                    <li>The pigments might not fade after the first session and a few more might be required.</li>
                </ul>
            </div>
        </div>
        <div class="col-md-12" itemtype="https://schema.org/MedicalTherapy" itemscope="" itemprop="possibleTreatment">
            <h3 itemprop="name">After laser treatment for pigmentation</h3>
            <p itemprop="description"> Chances are that you will not be done in one session and you will be asked to come back a fair few times. However, immediately after the first session, your skin will feel tender and there could be some superficial bleeding.<br>
                You will be given instructions on how to care for your skin and you might also be given certain medicines to help with the same. You might be asked not to step out into the sun for a few days or protect the treated area, if you do need to step out. </p>
        </div>
    </div>
</div>
<div class="service-area ptb-3 ">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Are The Lasers Safe?</a> </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">Absolutely; although it is important that all parties involved wear protective eye gear, because the bright light could hurt the eyes.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Are The Effects Long Lasting?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">In most cases, after 2-3 sessions, the effects start to become obvious and the effects are long lasting.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Will Insurance Cover The Treatment?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">In most cases, insurance companies will ask for an explanation for the medical requirement of the procedure. If the condition is severe, most companies will offer compensation.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Will The Treatment Hurt?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">As the treatment is done using lasers only, there will be next to no pain, however, there could be little discomfort, but this is not long lasting. However, if you are undergoing a chemical peel, there could be discomfort, itching or irritation, but all these will be temporary.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">Can Chemical Peels Be Used To Treat Pigmentation?</a> </div>
                        <div id="collapsefive" class="collapse" data-parent="#accordion">
                            <div class="card-body">Of late, chemical peels are being used for pigmentation, but these will work best, only if the pigmentation is on the epidermal level. If the pigmentation has entered the dermis, then chemical peels might not be the ideal solution, especially in pigmented skin. However, this is something that only a dermatologist will be able to decide.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" id="testimonails">
                <div class="col-md-12 text-center">
                    <h2>Possible Risks</h2>
                    <p class=" text-left">If the procedure has been done by professionals, then there is little that can go wrong, but there are certain side effects or possible complications, such as:</p>
                </div>
                <div class="row ptb-1">
                    <ul>
                        <li>The skin can take on a grey colour</li>
                        <li>The pigments do not fade at all</li>
                        <li>There could be some bleeding</li>
                        <li>There is uneven colouring over the treated area</li>
                        <li>There could be excessive blistering and crusting, which does not fall off easily</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div></span>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/cosmetology_blogs'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>