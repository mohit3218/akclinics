<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"> <span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/"> <span itemprop="name">Anti Ageing Treatments</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="3" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/fillers/"> <span itemprop="name">Fillers</span></a>
                        <meta itemprop="position" content="4" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Dermal Fillers Injection Reduce Wrinkles and Fill Lines</h1>
                </div>
                <h4>What are dermal fillers?</h4>
                <p>Injectable fillers or dermal fillers are fillers that can be injected into the soft tissues of the skin and it can help with plumping up the skin and reducing fine lines, especially those which appear under the eyes, around the lips and cheek filling. Most of these fillers are temporary in nature, because the body will eventually absorb the same. In most cases, the effect of the fillers will last for about 8-10 months, some as long as 24 months after which you will have to get a repeat dose.</p>
                <p>Dermal fillers can be used to not only reduce the fine lines on the face, but also return a sense of fullness on the face, which might be sagging. Over time, the skin starts to lose its fat deposits, which makes it look loose and creates the foundation for wrinkles. What such fillers do is that they create a sense of volume under the skin and this not only eliminates the lines, but also leaves the skin looking plumper and more elastic. This is why dermal fillers are used to create fuller lips, reduce fine lines and wrinkles and also reduce the appearance of scars.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="section-title">
                    <h2 class="black-text text-center ">How is the procedure done?</h2>
                    <p class="text-center"> While there are several types of dermal fillers, a <strong>filler on face</strong> will be applied in more or less the same type of procedure.</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row text-left">
                <div class="col-md-3">
                    <h4>Step one</h4>
                    <p> Would be a proper assessment of the face, in which the doctor or the support staff will evaluate your face with care. They will decide which parts of the face require the fillers and this will also help them determine which kind of filler needs to be used and in what quantity. This will also be the time when the areas for the filler injection will be demarcated and certain doctors will take photographs to determine the effect of the fillers.</p>
                </div>
                <div class="col-md-3">
                    <h4>Step two </h4>
                    <p>Would be to clean the site where the dermal fillers need to be injected and this will be done using antibacterial agents. Given that needles will be used, most doctors will numb the area using either cold, a local anaesthetic or numbing cream. However, there are several people who are able to tolerate the pain of the dermal filler injections and choose not to have anaesthetic.</p>
                </div>
                <div class="col-md-3">
                    <h4>Step three</h4>
                    <p> Would be the actual injection, which normally takes only a few minutes per site. Once the filler has been injected, the area will be massaged gently as this allows the filler to spread out in a more even manner. Depending on how much area needs to be covered, a single session can take anywhere from 15 minutes to an hour.</p>
                </div>
                <div class="col-md-3">
                    <h4>Step four</h4>
                    <p> Would be a proper cleanup, wherein all the markings that had been made would be cleaned up. Ice pack would be applied to reduce the swelling that might have appeared post the injections. Ideally the skin should return to normal within two to three days.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text text-center">Types of Dermal Fillers</h2>
                    <p>There are actually several types of dermal fillers, but not all of them can be used as a filler on face. The main four categories of dermal fillers include:</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Autologous wrinkle fillers</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Autologous wrinkle fillers</h3>
                    <p>This is perhaps one of the most commonly used filler in the world, mainly because it is the fat from your own body. Fat from body parts such as thighs, buttocks or arms will be removed and this will be treated. Post treatment, the fat will be injected into the facial parts. The biggest advantage of this type of filler is that there are minimal chances of infections or allergic reactions, because the fat has been harvested from your own body. As a matter of fact, even the vampire lift or plasma rich platelet therapy is the same thing, because blood from your body is used to treat your skin and make it look plumper and firmer.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Synthetic wrinkle fillers</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Synthetic wrinkle fillers</h3>
                    This is actually a much smaller category of dermal fillers, because it is made using substances that are not natural. However, because the fillers are synthetic in nature, they tend to have effect for a much longer time and some of them can actually offer a permanent solution. However, there is also a big problem, because if the filler is not delivered properly, it could lead to facial disfigurement. Sculptra and Radiesse are the most popular synthetic wrinkle fillers on the market today. </div>
                <div class="tab">
                    <button class="tab-toggle">Hyaluronic acid wrinkle fillers</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Hyaluronic acid wrinkle fillers</h3>
                    <p>Perhaps the most popular of dermal fillers happen to be hyaluronic acid and what is interesting is that each one of them works in a different manner. The reason why this filler is so popular is the fact that it rarely causes any side effects and even if there are any, they are hardly ever anything more than slight swelling, redness or bruising. In some cases, the filler could start to become visible, right under the skin in the form of tiny bumps. However, it is the fact that the results could last close to a year is what makes it most possible. Studies related to this filler have shown that repeated use could actually stimulate the natural production of collagen in the body, leading to lesser wrinkles and reduced requirement of repeat doses. Some of the most popular hyaluronic fillers include Juvederm, Perlane, Restylane, Captique and HylaForm.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Collagen wrinkle fillers</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Collagen wrinkle fillers</h3>
                    <p>The very first type of dermal fillers were made using collagen that had been extracted from cows. While the results were immediate and quite natural, the same did not last very long, which is why they never became really popular. In addition, because the collagen was extracted from animals, the rate of allergic reactions was really high. However, newer ways of creating collagen started appearing on the scene and the risks reduced along with it. Even though the results are supposed to be most natural, because they do not last more than a few months, these fillers never really became as popular as the others. Some of the most popular collagen fillers include Cosmoderm, Zyderm, Zyplast, Evolence and ArteFill.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title text-center">
                    <h2 class="black-text">Dermal fillers injections treatment advantage & Uses</h2>
                    <p>Dermal fillers do exactly what their name suggests – they fill out the area under the skin which might have started looking sunken, because of loss of fat or collagen. The idea behind using these fillers is that wrinkles, fine lines and even deep creases are filled out and the facial features look fuller and plumper.</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row text-left">
                <div class="col-md-4">
                    <strong>Here are some of the benefits of dermal fillers</strong>
                    <ul class="text-left" style="padding:0">
                        <li>The effects are very natural and last a reasonably long time</li>
                        <li>The side effects are minimal and rarely go beyond the temporary swelling and bruising</li>
                        <li>The cases of allergic reactions are also minimal</li>
                        <li>There is a range of fillers you can choose from</li>
                        <li>Since dermal fillers are considered only medical grade, it is not compulsory to go to a doctor, because certified professionals can also administer the same</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <strong>Side effects of dermal fillers injections</strong>
                    <p>If the filler has been injected in the correct manner and dosage, chances of any side effects arising are minimal, however, exceptions are always there. Some of the most commonly noted side effects include:</p>
                    <ul class="text-left">
                        <li>Temporary bruising, redness, swelling, itching or slight pain</li>
                        <li>Bleeding from the <strong>injection</strong> site</li>
                        <li>Damage to the skin, which could lead to scars</li>
                        <li>Lumps or small acne like bumps</li>
                        <li>Asymmetry or lack of consistency</li>
                        <li >Infection</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h4>How visible is the change?</h4>
                    <p>The effect of the dermal fillers should become visible within a few hours of the injection being administered. Depending on what kind of filler you have opted for, the results could last for anywhere between six months to two years. In addition, much of the longevity of the results will also depend on how well you take care of your skin and body in general.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row text-center">
            <div class="section-title">
                <h2 class="black-text text-center">FAQs</h2>
            </div>
        </div>
        <div class="col-md-12">
            <div id="accordion" class="mt-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">What Is The Recovery Period For This Procedure?</a> </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                <div class="card-body">There is virtually no down time for fillers ..the effect being almost instantaneous although gets better over further fortnight.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">What Are Fillers?</a> </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">Dermal fillers rejuvenate appearance, but need to have repeat treatments to maintain that fresh, full new look. Consult your doctor to discuss whether to opt for a liquid facelift.

                                    Excess sagging skin – this is one instance when soft tissue fillers can’t help. But otherwise, they can add more volume at a cheaper rate than other forms of cosmetic surgery. They work best in conjunction with other rejuvenation treatments such as botox injections but only a qualified doctor or surgeon can prescribe the right kind of therapy for you. Like all other procedures its effect is temporary – reducing forehead wrinkles and it cannot stop the ageing process .

                                    These injectable fillers work differently from botox injections by filling the line or crease with several different substances, while the latter works by making a muscle relax.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree"> How Will I Know If I Am The Right Candidate?</a> </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body">Hyaluronic acid filler stimulates collagen production in the body and issued for moderate to severe wrinkles and folds. Calciumhydroxylapatite on the other hand, fills deepest creases and frown lines by enhancing fullness of cheeks and other facial contours. Polylactic acid takes the longest to show results and is generally used to treat laugh lines. All synthetic wrinkle fillers have similar side effects.

                                    This makes it all the more necessary to approach a qualified surgeon and consult a dermatologist, as these fillers channelized through the wrong hands could have disastrous results. Your doctor can provide scientific analysis of various types of fillers and prescribe the right one for you.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">How Many Repeat Sessions Are Necessary For This Procedure?</a> </div>
                            <div id="collapsefour" class="collapse " data-parent="#accordion">
                                <div class="card-body">It differs from person to person. Some people age faster and their face shows those effects. At times the body absorbs fillers quickly. It is factors like these which determine the timing of repeat treatment. Usually the effects last from 8-10 months. Some fillers last for as long as 2 years.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefive">Can Fillers Have Side Effects?</a> </div>
                            <div id="collapsefive" class="collapse " data-parent="#accordion">
                                <div class="card-body">Yes, there’s a downside: allergic reactions, permanent bumps under the skin.Temporary numbness of facial muscles, acne, bleeding, itching, infection at the injection site, could also occur in experienced hands but are not very common. Consult your doctor, do a thorough check before signing up.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=" bg-light-gray">
    <?php $this->load->view('front/partials/cosmetology_blogs'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>