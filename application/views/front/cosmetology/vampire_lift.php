<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"> <span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/"> <span itemprop="name">Anti Ageing Treatments</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="3" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/Vampire Lift/"> <span itemprop="name">Vampire Lift</span></a>
                        <meta itemprop="position" content="4" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1">
                    <h2 class="black-text text-left">Vampire Lift</h2>
                </div>
                <h4>What is Vampire Lift?</h4>
                <p>At AK Clinics, we are always looking at ground breaking methods to rejuvenate the skin and one of our latest offerings is a Vampire Lift. From the time we introduced this treatment, we have not only treated several people, but also given several people the chance to see their face looking fresher and healthier, with a natural glow.</p>
                <br /><p>Vampire facelift is actually a popular name for a procedure otherwise known as Selphyl. In this procedure, a gel like substance is injected into the body, and this gel is actually derived from the patient’s own blood. The gel is actually a generic term used to describe PRFM or platelet rich fibrin matrix, which when injected into the skin can rejuvenate it naturally. The platelet rich fibrin matrix or platelet rick plasma is actually blood plasma, which has been enriched with additional platelets. When the PRP comes in contact with calcium chloride or thrombin, it becomes viscous and takes on a gel like consistency.</p>

                <h4 class="text-muted">Vampire Lift Infographics</h4>
                <figure><img data-src="<?= cdn('assets/template/frontend/images/'); ?>vampire-lift-infographic.png" class="img-fluid lazy" alt="Vampire Face lift treatment"></figure>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2 class="black-text text-center">Important things before Vampire Lift.</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row">
                <div class="col-md-6">
                    <h4>Who are ideal candidates for a vampire facelift?</h4>
                    <ul>
                        <li>If your skin has lost its natural pink colour and has started look pale</li>
                        <li>If you have lost the facial shape that you once enjoyed and your skin looks droopy and flaccid</li>
                        <li>If your skin no longer feels smooth and healthy</li>
                    </ul>
                    <p>Then, you are the ideal candidate for a vampire facelift! </p>
                </div>
                <div class="col-md-6">
                    <h4>Why choose a vampire facelift?</h4>
                    <ul class="list-unstyled margin-bottom-20">
                        <li class="icon-chk">If you go for a cosmetic surgery, you will have to get your skin and face cut up, which will mean some scars. </li>
                        <li class="icon-chk">If you go for dermal fillers, they can plump up your skin, but the results will be only temporary. </li>
                    </ul>
                    <p>However, when you go for a vampire facelift, you get the benefits of both procedures. Not only will you have better skin tone and texture, but also healthier skin from within! </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <div class="col-md-12 text-center"> <h2>What is the vampire facelift procedure?</h2>
                    <p>Here are some of the most important steps in a vampire facelift:</p>

                </div>

                <div class="row">
                    <div class="col-md-12">
                    </div><ul >
                        <li>We will firstly inject a hyaluronic acid, with the intention of sculpting your face to make it look younger, without hampering the natural integrity.  </li>
                        <li>Blood will be harvested from the body, normally around two tablespoons worth. The blood will have natural healing properties, which will help rejuvenate tissue that has been damaged. </li>
                        <li>The blood will then be placed inside a centrifuge, where the platelets will be isolated from the blood. The process normally does not take more than a few minutes. </li>
                        <li>The platelets are then activated, which ensures that growth factors are released, and it is these factors, that encourage the skin to heal on its own. They will encourage collagen to form and allow better blood flow too.</li>
                        <li>A numbing cream will be applied to your face and then using a minute needle, we will inject the growth factors back into your face.</li>
                        <li>Once the growth factors are injected, they will in turn activate the multipotent stem cells, which are already present in the skin. These cells will then regenerate collagen, blood vessels and even tissues.</li>
                    </ul></div>


            </div><div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Why Does A Vampire Facelift Work?</a> </div>
                        <div id="collapseOne" class="collapse " data-parent="#accordion">
                            <div class="card-body">One of the main reasons why a vampire facelift works is because your very own blood is being used. The growth factors from the treated blood will activate the multipotent stem cells, which in term will heal the skin. The intention of the cells will be to repair skin, which has not been damaged in the first place!</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">When Will The Results Start Becoming Visible?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">The results should start to become visible within two to three months, but you will start to feel a change from within, hours after the procedure has been completed.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">How Long Will The Results Last?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">If you take good care of your skin, the results can last anywhere between ten to twenty months!</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">How Do I Know That A Vampire Facelift Is The Best Idea?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">This is where we will be able to guide you – after checking your skin, we will suggest the best option for you, including whether a vampire facelift is the best idea for you. chances are that your problem could be solved with a few dermal fillers or it could be more severe and the only way out would be a surgery.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/cosmetology_blogs'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>