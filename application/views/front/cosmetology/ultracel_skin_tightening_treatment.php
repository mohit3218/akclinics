<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"><span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/"> <span itemprop="name">Ultracel Skin Tightening</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">ULTRAcel Skin Lifting & Tightening Treatment</h1>
                </div>
                <p>ULTRAcel- A revolutionary technique to reverse your signs of aging. It is an exciting new treatment that works by targeting the same layer that Plastic Surgeons lift and tighten during a surgical face lift. ULTRAcel is the latest skin uplifting device that offers non-surgical skin tightening and lifting of the skin along with addressing of fine lines and wrinkles, leaving your skin glowing, smooth and spanking new. Doesn’t matter if you want to enhance or contour your facial features or you want to reverse your aging signs, ULTRAcel is the best option of skin tightening and enhancing. ULTRAcel for skin tightening have given clinically proven results. <br />
                    <br /> AK Clinics is providing this revolutionary skin tightening treatment at their clinics at Delhi, Bangalore and Ludhiana. If you don’t want to undergo multiple skin tightening treatments at different time intervals, then ULTRAcel- a 3 in 1 skin lifting treatment is the best for you. The cost of ULTRAcel skin tightening treatment at AK Clinics is very affordable and will give you the proven and long lasting results for sure.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12 content-center">
            <div class="row  text-center">
                <div class="section-title">
                    <h2 class="black-text">What is ULTRAcel Treatment?</h2>
                </div>
                <p>ULTRAcel treatment is the recent and advanced non-surgical &amp; non-invasive process for the skin lifting &amp; tightening. ULTRAcel will haul up, tighten and tone your wobbly skin. With the use of handheld devices that transfers the grid radio frequency and ultrasound waves to the skin. These devices not only targets the outer epidermal layer, but also the deeper facial and neck soft tissue structures which allow the more face lifting along with neck tightening. </p>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row text-center">
                <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/04/Grid-RF.png" alt="" class="rounded-circle lazy"><br>
                    <br>
                    <strong>Grid RF</strong>
                    <p>It redefines the RF skin tightening treatment or anti-aging treatment which rejuvenates your skin completely from inside out and it works on wrinkles, skin texture, nasolabial folds, chin, jaw line or even large pores. Grid RF uses radiofrequency energy that creates the thermal grid shapes on the epidermis &amp; a uniform thermal zone in the upper dermis during the RF emission. </p>
                </div>
                <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/04/Needle-RF.png" alt="" class="rounded-circle lazy"><strong><br>
                        <br>
                        Needle RF </strong>
                    <p>This is the ideal for skin tightening &amp; rejuvenation by applying the precisely controlled radio frequency waves directly to the skin with the help of minimally invasive micro-needles. The treatment is best for eliminating wrinkles, fine lines, pore minimization, acne &amp; acne scars, stretch marks and pigmentation. The optimal radiofrequency that is delivered to the dermal layers are up to 60-80ºC. </p>
                </div>
                <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/04/HIFU.png" alt="" class="rounded-circle lazy"><br>
                    <br>
                    <strong>HIFU/FUS</strong>
                    <p>HIFU offers the non-invasive alternative to the surgical face lift while improving the fine lines, wrinkles, décolleté etc. It is the best skin treatment for face contouring also. HIFU uses the ultrasound waves to safely lift &amp; tighten the skin. It penetrates deep into the dermal layers at high temperature and also protects the skin. During the procedure, thermal heat is created within the skin tissue that creates the tiny wounds and cellular friction. This promotes the healing mechanism of the skin and also enhances the collagen production, giving skin a tight and lifted effect. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="section-title  text-center">
                    <h2 class="black-text">What areas can be treated with ULTRAcel?</h2>
                </div>
                <p>ULTRAcel is a 3 step skin layer lifting which targets the skin tissues and with the help of body’s natural healing process, new cells or tissues are generated resulting in more collagen production and tighter or firm skin. The areas which can be treated with the ULTRAcel treatment are;</p>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-6">
                            <ul >
                                <li>Mid face skin tightening and lifting</li>
                                <li>Eyes rejuvenation</li>
                                <li>Eye lift </li>
                                <li>Neck lift</li>
                                <li>Marionette (around lips)</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul>
                                <li>Lower face skin tightening </li>
                                <li>Eye Brow lift</li>
                                <li>Chin</li>
                                <li>Decollete Skin Area</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/04/ULTRAcel-treated-area.png" alt="ULTRAcel skin tightening treatment" class="img-fluid lazy"></div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container ">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">How ULTRAcel Works?</h2>
                </div>
                <p>ULTRAcel is the advanced, non-invasive skin tightening treatment which requires no downtime. In ULTRAcel skin uplifting and tightening treatment, small and focused ultrasound waves penetrate through the epidermal layer to reduce the signs of aging through non-invasive lifting &amp; tightening skin. ULTRAcel technology combines the 3 powerful &amp; highly effective technologies in one process. With the use of radiofrequency with the ultrasound waves, ULTRAcel targets and transfers heat to the collagen in the deeper layers of skin. After this, stimulation of new collagen which helps to lift the skin resulting in firmer and tighter skin.</p>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row text-center">
                <h4 class="clr text-left">1<sup>st</sup> Step</h4>
                <ul>
                    <li class="clr text-left"><strong><em>Grid RF</em></strong> In the Upper dermis, radio frequency and high intensity focused ultrasound waves are emitted to 3.0/4.5 mm depth with fractional shape. It helps to stimulates the collagen production and smoothens the skin surface.</li>
                </ul>
                <div class="col-md-3"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/06/Grid-FR-1.png"  class="rounded lazy img-fluid" alt="Grid RF">
                    <p class="pt-1">RF targets the area to be treated</p>
                </div>
                <div class="col-md-3"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/06/Grid-FR-2.png"  class="rounded lazy img-fluid" alt="Deep dermal layer of skin">
                    <p class="pt-1">It penetrates into the deep dermal layer of skin</p>
                </div>
                <div class="col-md-3"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/06/Grid-FR-3.png"  class="rounded lazy img-fluid" alt="collagen production">
                    <p class="pt-1">Increased collagen production</p>
                </div>
                <div class="col-md-3"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/06/Grid-FR-4.png" class="rounded lazy img-fluid" alt="Smooth skin">
                    <p class="pt-1">Smooth skin surface</p>
                </div>
                <h4 class="clr text-left">2<sup>nd</sup> Step</h4>
                <ul>
                    <li class="clr text-left"><strong><em>RF Micro-needling:</em></strong> RF Micro-needling is an avant-garde skin repairing technique. In RF micro-needling, small insulated micro-needles create tiny wounds by penetrating into the dermal layer of skin. When it is combined with RF, it enables the thermal energy to stimulate body’s natural healing mechanism which enhances the collagen production.</li>
                </ul>
                <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/06/RF-Micro-needling-1.png" class="rounded lazy img-fluid" alt="RF Micro-needling">
                    <p class="pt-1">Delivers heat energy through the needles in the dermis</p>
                </div>
                <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/06/RF-Micro-needling-2.png"  class="rounded lazy img-fluid" alt="Collagen production">
                    <p class="pt-1">Collagen production is stimulated through the natural healing process</p>
                </div>                <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/06/RF-Micro-needling-3.png" class="rounded lazy img-fluid" alt=" smooth & wrinkle free skin">
                    <p class="pt-1">Enhanced collagen results in smooth &amp; wrinkle free skin</p>
                </div>
                <h4 class="clr text-left">3<sup>rd</sup> Step:</h4>
                <ul>
                    <li class="clr text-left"><strong><em>HIFU/FUS:</em></strong> HIFU targets the deep SMAS layer (Superficial muscular aponeurotic system) of face. In HIFU, the high intensity of ultrasound is emitted to the deep structural layers such as fascia, SMAS fat layer, dermis boundary layer of subcutaneous which causes thermal wounds, prompting the body to launch its natural healing process. Skin begin to produce more collagen, which lifts, tightens and contours the skin resulting in wrinkle free skin.</li>
                </ul>
                <div class="col-md-3"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/06/HIFU-1.png" class="rounded lazy img-fluid" alt="HIFU/FUS">
                    <p class="pt-1">HIFU targets the deep fat layer of skin and imparts thermal energy</p>
                </div>
                <div class="col-md-3"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/06//HIFU-2.png" class="rounded lazy img-fluid" alt="collagen production ">
                    <p class="pt-1">After 1 month, collagen production start increasing through wound healing process</p>
                </div>
                <div class="col-md-3"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/06/HIFU-3.png" class="rounded lazy img-fluid" alt="more collagen production">
                    <p class="pt-1">After 3 month, more collagen production </p>
                </div>
                <div class="col-md-3"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/06/HIFU-4.png" class="rounded lazy img-fluid" alt="tight and uplifted skin">
                    <p class="pt-1">Smooth, tight and uplifted skin </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle ">Why ULTRAcel treatment for skin tightening?</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Why ULTRAcel treatment for skin tightening?</h3>
                    <p>ULTRAcel skin tightening process is done with the world’s hi-tech or advanced face lifting machine and it is the non-surgical face lift machine. ULTRAcel increases the density of collagen by producing more collagen peptides & elastin in the dermis. This technique is so far advanced from the only laser skin tightening. This ULTRAcel skin tightening treatment is the best for non-surgical neck tightening or non-surgical neck lift, chin lift without surgery, jaw line lift and non-surgical eyebrow lift.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">ULTRAcel vs. Other Combination Machines</button>
                </div>
                <div class="content">
                    <h3 class="m-3">ULTRAcel vs. Other Combination Machines</h3>
                    <p>Specifications of ULTRAcel Skin Tightening & Uplifting Machine</p>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <h3>Transducers with high supremacy</h3>
                                <ul class="list-unstyled margin-bottom-20">
                                    <li class="icon-chk">High power transducers in ULTRAcel helps to create a coagulation zone which is required of an ample size for desired in-depth. </li>
                                    <li class="icon-chk">With its powerful frequency, it helps to create a coagulation zone over the temperature of 60ºC in the dermal layer.</li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h3>Cartridges with high &amp; reliable quality</h3>
                                <ul class="list-unstyled margin-bottom-20">
                                    <li class="icon-chk">With the help of mimicking phantoms, cartridges of ULTRAcel machine is tested to ensure better &amp; high-quality results.</li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h3>Easy to handle device:</h3>
                                <ul class="list-unstyled margin-bottom-20">
                                    <li class="icon-chk">The handheld devices of this skin uplifting machine are designed vertically to provide a clear vision &amp; easy handling during the treatment.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab">
                    <button class="tab-toggle">How is ULTRAcel skin uplifting procedure different from other skin tightening procedures?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">How is ULTRAcel skin uplifting procedure different from other skin tightening procedures?</h3>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <p>ULTRAcel is 3 in one technique for the skin tightening &amp; uplifting. It uses the three different technologies which are;</p>
                                <ul class="list-unstyled margin-bottom-20">
                                    <li class="icon-chk">Grid Fractional Radiofrequency (GFR)</li>
                                    <li class="icon-chk">High-Intensity Focused Ultrasound (HIFU/FUS)</li>
                                    <li class="icon-chk">Fractional Radiofrequency Micro-needling (FRM), also known as INTRAcel</li>
                                </ul>
                                <p>Among the above-given procedures for skin tightening, GFR &amp; FUS are performed in one session while FRM is performed in one single session. </p>
                                <p></p>
                            </div>
                            <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2018/04/ULTRAcel-skin-uplifting-procedure.png" class="img-fluid lazy" alt="Benefits of Ultracel skin tightening procedures" ></div>
                            <div class="col-md-4">
                                <h3>FRM</h3>
                                <ul class="list-unstyled margin-bottom-20">
                                    <li class="icon-chk">With dermal micro-needling technique along with fractional radio frequency, there is enhanced collagen &amp; elastin production.</li>
                                    <li class="icon-chk">You may feel heat or pricking sensation and skin will turn into red which will be normal within 2-3 days.</li>
                                </ul>
                                <h3>GFR &amp; FUS</h3>
                                <ul class="list-unstyled margin-bottom-20">
                                    <li class="icon-chk">With the help of thermal heat energy, there is simulation in the collagen production to tighten the skin.</li>
                                    <li class="icon-chk">You may feel the a stinging sensation and mild soreness which will resolve within 2-3 weeks</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab">
                    <button class="tab-toggle">What is the difference between ULTRAcel & INTRAcel?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What is the difference between ULTRAcel & INTRAcel?</h3>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-unstyled margin-bottom-20">
                                    <li class="icon-chk">ULTRAcel is also known as HIFU or FUS and provides a structural lift to the skin. ULTRAcel is a non-invasive method which uses ultrasound and radiofrequency energy in synergy to work at the deeper layer of the skin (the SMAS layer). </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-unstyled margin-bottom-20">
                                    <li class="icon-chk">INTRAcel is also referred to as FRM. It works by resurfacing the skin’s surface and stimulates the collagen and elastin production. INTRAcel combines the Micro-needling with radiofrequency to target the heat energy below the surface of the skin. </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="section-title text-center">
                    <h2 class="black-text">FAQs</h2>
                    <p>We strive to provide highest quality at affordable cost with hygiene and imported equipments</p>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div id="accordion" class="mt-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Is ULTRAcel A Safe Procedure?</a> </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                <div class="card-body">As the treatment involves the use of high-frequency waves, then you must take your treatment from an expert and highly trained doctor. Then there will be less to no chances of any complications.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">What Are The Side Effects Of ULTRAcel Skin Lifting Treatment?</a> </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body">There are no serious side effects or complications. After the treatment, you may experience mild sun burnt sensation which will resolve itself within few days. Your skin may appear red for few hours. Swelling or bruising is minimal under the eyes which will resolve within 1-2 days.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">When Can I See The Results?</a> </div>
                            <div id="collapsefive" class="collapse" data-parent="#accordion">
                                <div class="card-body">Most probably you will feel your skin tighter or smoother after one treatment and this will continue for the next 6 months. However, it may vary with your skin texture, treated area or frequency used during your treatment.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsesix">Is ULTRAcel Surgery?</a> </div>
                            <div id="collapsesix" class="collapse" data-parent="#accordion">
                                <div class="card-body">No. The ULTRAcel is a non-invasive skin tightening treatment that treats the skin and the support layer below it, but it doesn’t entail cutting or disrupting the surface of the skin.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseeig">Who Is A Good Candidate For ULTRAcel?</a> </div>
                            <div id="collapseeig" class="collapse" data-parent="#accordion">
                                <div class="card-body">You must have a relaxed skin with mild to moderate amount of skin laxity. Apart from this, a lowered brow line or sagging skin on the eyelids, drooping jaw line, acne, acne scars, pimples or blemishes, stretch marks etc.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseten">What Happens During The ULTRAcel Treatment?</a> </div>
                            <div id="collapseten" class="collapse" data-parent="#accordion">
                                <div class="card-body">You will feel the mild tingling or heat sensation during your treatment. After your ULTRAcel treatment, your skin may appear red or even scabs may appear, but they will shed off on their own and redness fill fade away within 2-3 hours to 2-3 days. This can vary from person to person</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Is There Any Downtime?</a> </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">There is no to minimal downtime for ULTRAcel skin tightening procedure.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">What Are The Aftercare Instructions For ULTRAcel Treatment?</a> </div>
                            <div id="collapsefour" class="collapse " data-parent="#accordion">
                                <div class="card-body">After the treatment, you have to apply the sunscreen on your face for better healing. If you have a burning sensation, then you may apply aloe vera gel and cold compress on your face. However, after 24 hours, you can apply makeup too.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefivee">How Many Sessions Will Be Needed For Skin Lifting?</a> </div>
                            <div id="collapsefivee" class="collapse" data-parent="#accordion">
                                <div class="card-body">This is what that purely depends on your skin condition. The dermatologist will analyze your skin condition and area to be treated for skin uplifting. On the basis of that, the doctor will tell you about the number of sessions required.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsesev">Can ULTRAcel Replace A Facelift?</a> </div>
                            <div id="collapsesev" class="collapse" data-parent="#accordion">
                                <div class="card-body">The ULTRAcel is not a “facelift.” It is actually an uplift. While it is not a replacement for surgery, it is a viable option for those not ready for undergoing a surgery. You can enhance or contour your facial features also.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsenine">How Long Does The ULTRAcel Treatment Take?</a> </div>
                            <div id="collapsenine" class="collapse" data-parent="#accordion">
                                <div class="card-body">A full face-and-neck treatment takes approximately 30 mins -1 hour or depending upon the area to be treated.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseele">Will I Experience Severe Pain During The Treatment?</a> </div>
                            <div id="collapseele" class="collapse" data-parent="#accordion">
                                <div class="card-body">Usually there is no need of anesthesia for the ULTRAcel treatment. But you if you are sensitive to pain, then you can ask the dermatologist for anesthesia application. A topical or injectable anesthesia can be applied prior to the treatment in that case.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetology_blogs'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>