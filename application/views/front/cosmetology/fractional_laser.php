<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"> <span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/"> <span itemprop="name">Anti Ageing Treatments</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="3" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/fractional-laser/"> <span itemprop="name">Fractional Laser</span></a>
                        <meta itemprop="position" content="4" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1">
                    <h1 class="black-text text-left">Fractional Laser</h1>
                </div>
                <h4>What is Fractional Laser?</h4>
                <p>Over the past few years, there has been a lot talk about laser treatment, and while the most popular use of the same is to remove unwanted body hair, there are actually several other uses too.</p>
                <br/>
                <p>A fractional laser treatment, which is a non-invasive treatment, makes use of a special device, which delivers a laser beam, directly into the skin. The section of the skin that is targeted, is in fractions and the laser beam itself is then divided into multiple microscopic treatment zones. Imagine a digital photograph being altered pixel by pixel – this is exactly what happens with fractional laser too!</p>
                <br/>
                <p>The origin of fractional laser treatment lies in trying to bridge the gap between ablative and non-ablative laser treatments. While ablative treatment works on cells on the surface level of the skin, non-ablative treatments work on the dermal collagen layer. The benefit of fractional laser treatment, is that it can be used to treat both the dermal and epidermal layers.</p>
                <h4 class="text-muted">Causes of dark spots on your skin Infographics</h4>

                <figure><img data-src="<?= cdn('assets/template/')?>themes/AKclinics/images/fractional-laser-infographics.png" class="img-fluid lazy" alt="fractional laser for acne scars"></figure>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="section-title ">
                    <h2 class="black-text text-center">Types of Hyperpigmentation</h2>
                    <p>There are various types of hyperpigmentation but the following are the most commonly seen:</p>  </div>
            </div>
        </div>
        <div class="col-md-12 text-center ptb-1">
            <div class="row">
                <div class="col-md-4">
                    <h4>Sunspots</h4>
                    <p>Sunspots, which are also known as solar lentigines, are often caused due to extensive exposure to the sun. These also appear on those parts of the body, which are constantly exposed to the sun, such as the face and hands. </p>
                </div>
                <div class="col-md-4">
                    <h4>Melasma</h4>
                    <p>Melasma is often considered to be caused due to a change in the hormonal activity in the body. This is why it is most commonly seen in women, during their pregnancy. While the condition could show up in any part of the body, it is seen most commonly on the face and abdomen. </p>
                </div>
                <div class="col-md-4">
                    <h4>Post-inflammatory</h4>
                    <p>There is also something known as post-inflammatory hyperpigmentation, which is caused due to an injury to the skin. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text text-center">Fractional Laser Treatment</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle ">Here are all the things that we can treat with our fractional laser treatment</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Here are all the things that we can treat with our fractional laser treatment</h3>
                    <ul>
                        <li>Fine lines and wrinkles on the face or any other part of the body </li>
                        <li>Repair the damage caused by constant exposure to the sun</li>
                        <li>Repair and reduce pigmentation caused due to ageing, scars caused due to surgery or even severe acne</li>
                        <li>Other pigmentation disorders such as melasma</li>
                        <li>Reduce stretch marks</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">How fractional laser treatment works</button>
                </div>
                <div class="content">
                    <h3 class="m-3">How fractional laser treatment works</h3>
                    <p>As the laser beam enters the skin, it gets divided into multiple zones, known as microthermal treatment zones or MTZs. Each MTZ will have cells which are not functioning properly and the laser beam will systematically remove them, creating scope for new cells to grow. We ensure that that only the unhealthy cells are removed and the healthy cells are left intact and untouched. This allows you to heal faster and require minimal downtime!</p>
                    <p>The number of required treatment will be something that we will be able to decide only once we have examined your skin. We will normally space the treatment sessions about a month apart and you should be able to see the results after the second session.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row text-center">
            <div class="section-title">
                <h2 class="black-text text-center">FAQ's about Fractional Laser</h2>
            </div>
        </div>
        <div class="col-md-12">
            <div id="accordion" class="mt-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Are There Any Side Effects?</a> </div>
                            <div id="collapseOne" class="collapse " data-parent="#accordion">
                                <div class="card-body">Given our level of experience, there are hardly any side effects, but there could be some side effects, including:

                                    <ul class="text-left">
                                        <li>Excessive swelling, scaling, peeling or crusting</li>
                                        <li>Post-inflammatory pigmentation</li>
                                        <li>Contact dermatitis</li>
                                        <li>Bacterial and candida infections</li>
                                        <li>Acne or cold sores</li>
                                    </ul>


                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Is It Possible To Combine Fractional Resurfacing With Other Cosmetic Treatments?</a> </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">Absolutely – we can combine fractional laser treatment with temporary dermal fillers. However, we will need to plan out the timing because while in certain cases, the laser needs to be done before the fillers and in certain cases, after.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">How Long Will The Results Last?</a> </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body">As surprising as it might sound, the duration for which the effects of a fractional laser treatment will last, actually depend on you! You need to make sure that you protect yourself from the sun. Every time you step out, make sure you apply sunscreen and also wear clothes that will protect the treated parts of your body. We would suggest that you avoid tanning booths or artificial tans, because these could prove to be potentially dangerous. With annual maintenance treatments, we can assure you that your skin will look great for a long time.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">When Will I Be Able To See The Results?</a> </div>
                            <div id="collapsefour" class="collapse " data-parent="#accordion">
                                <div class="card-body">In most cases, the results start to become visible within a week of the very first treatment, and you will notice that your skin is smooth and glowing. If you are getting the fractional laser in conjunction with some other treatment, then the results could be a little delayed.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefive">What Will I Be Asked To Do Before The Procedure?</a> </div>
                            <div id="collapsefive" class="collapse " data-parent="#accordion">
                                <div class="card-body">There is not much you have to do, except come without any makeup or contact lenses. Once you arrive, we will wash your face yet again and make sure that it is absolutely clean, after which we will apply the numbing gel.</div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsess">What Will I Be Asked To Do After The Procedure?</a> </div>
                            <div id="collapsess" class="collapse " data-parent="#accordion">
                                <div class="card-body">We will ask you to avoid medications such as Aspirin, Advil and Retinols. We will also suggest that you have ice packs in stock, because this will help with the swelling. We will also ask you to replace your regular facial cleansers with gentler versions and avoid sunlight for a few days.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetology_blogs'); ?>
<div class=" bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
