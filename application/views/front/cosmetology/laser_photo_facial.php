<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"> <span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/laser-photo-facial/"> <span itemprop="name">Laser Photo Facial</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Laser Photo Facial</h1>
                </div>
                <p>The face is perhaps the most important part of a person’s identity, which is why, it is important that you take good care of it. There are several treatments in the world today, which are aimed at improving the quality of the skin and photo facial happens to be one of them.</p>
                <p>The premise behind a photo facial is reasonably simple – a special kind of light is applied to the skin, and when it penetrates deep into the skin, it is able to treat brown spots, pigmentation, repair broken capillaries or even boost the amount of collagen in the skin. While a photo facial can do a lot to improve the condition of your skin, it is important that you combine it with a good daily facial care routine.</p>

                <h3 class="pt-1">Types of Laser Photo Facial</h3>

                <h4>LED (Light-emitting diode)</h4>
                <p>LED (Light-emitting diode) – This is the type of photo facial that is offered by most spas and salons. A narrow spectrum light is used with the aim to boost the collagen, which in turn will allow the skin to look younger and plumper. The same process can also be used to kill the bacteria that give birth to acne. The process is painless and carries no risk of causing burns. Within six treatments, the effects should be visible, and the skin should look healthier and younger.</p>

                <h4>IPL (Intense-pulsed light)</h4>
                <p>IPL (Intense-pulsed light) – This treatment is meant to handle broken capillaries, brown spots, spider veins as well as facial redness. A very bright light with high levels of energy will be directed to the face, using a handheld device. The treatment is known to be uncomfortable and at times, even painful. Some devices might have a cooling device, but this will only reduce the discomfort a little.</p>

            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="section-title text-center ">
                <h2 class="black-text">Laser Photo Facial</h2>
            </div>
        </div>

        <div class="row ptb-1">
            <div class="col-md-4">
                <h3>Before procedure</h3>
                <p>There is not much that you will be required to do before the procedure, apart from arriving well in time. Your face will be washed and cleaned thoroughly, before the procedure starts.</p>
            </div>
            <div class="col-md-4">
                <h3>During procedure</h3>
                <p>There is literally no downtime for a laser photo facial, which means that you can go back to your normal routine within a day. Immediately after the session, you might feel a little burning on your face, but it will last less than 24 hours.<br>
                    After the first two or three treatments, you will notice that your skin has a smoother and a more even tone. Over the next few days, the sunspots and pores will start to fade and even the wrinkles will start to disappear. If someone is suffering from acne, there will be a decrease in the redness too.</p>
            </div>
            <div class="col-md-4">
                <h3>After procedure</h3>
                <p>There is actually no downtime with this procedure and you can return to your normal life within hours. You might need to invest in some special creams or moisturizers, which your doctor will suggest to you. There might be a little bruising, redness or skin sensitivity, immediately after the procedure, but the same will disappear in a matter of days.</p>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">What Does The Process Feel Like? Is There Any Pain?</a> </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">Most people consider this a part of a spa treatment, because with an LED photo facial, there is no pain. As a matter of fact, most people consider this relaxing, as there is a gentle warmth that is emitted by the device. However, there is a slight discomfort with the IPL photo facial, but no anaesthesia or sedation will be required.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">What Are The Areas That Can Be Treated?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">The procedure is most effective on the face and neck, but it can be used to treat scars on any part of the body. The same process can also be used to remove redness that might be prevalent on any part of the body.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">How Many Sessions Will Be Required?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">This is something that depends on your skin and your doctor. Normally, four to six sessions are required, but subtle changes should become visible after the very first session.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">What Precautions Will I Have To Take After The Facial?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">Nothing much actually, but you do need to make sure that every time you step out, you wear sunscreen. In addition, you might want to avoid heavy makeup for a few days, but if you must apply the same, make sure that you remove it carefully.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">When Will I Be Able To Return To My Normal Routine?</a> </div>
                        <div id="collapsefive" class="collapse" data-parent="#accordion">
                            <div class="card-body">The very next day!</div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>Possible Risks</h2>
                    <p class="text-left pt-1">If the procedure has been done by professionals, then there is little risk, but the possible side effects include:</p>
                </div>
                <div class="row pt-1">

                    <ul>
                        <li>Redness or pinkness</li>
                        <li>Slight pain, as in a sunburn</li>

                        <li>Darkening of skin or hyperpigmentation</li>
                        <li>Potential skin burn, should someone step out in the sun, without sunscreen</li>
                    </ul>

                </div></div>
        </div>
    </div>
</div>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/cosmetology_blogs'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>