<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"> <span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/"> <span itemprop="name">Anti Ageing Treatments</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Anti-Ageing Treatments</h1>
                </div>
                <p>
                    Ageing is a natural process and all of us have to go through the same as the days progress and with age, the signs of the same start to appear. For some people, the signs might project in the form of grey hair, others might feel that their skin is starting to sag. But one of the most common signs of ageing happens to be fine lines or wrinkles and most people, especially women, are never happy to see the same so they want some anti ageing skin procedures. <br />
                    However, with the rapid advancements in science, turning back the clock has actually become a little easier. There are plenty of options for people who want to go for anti wrinkle treatment to get rid of the fine lines, crow’s feet and sagging or drooping facial features. Many of them are surgical, but for people who are concerned about undergoing a surgical procedure, there is also the option of several non-surgical procedures, which will help you look younger. AK Clinics is a specialized anti-ageing treatment clinic fully equipped with different lasers, face tightening procedures, different kind of fillers etc.
                </p>
                <h4 class="text-muted">Anti-Ageing Treatment Advantage</h4>
                <figure><img data-src="<?= cdn('assets/template/frontend/images/'); ?>Anti-Ageing-advantage.png" alt="Benefits of anti aging treatments" class="img-fluid lazy"></figure>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text text-center">Types of Anti-Ageing Treatments</h2>
                    <p>At AK Clinics, we off you a range of anti-ageing treatments and these include:</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="tabs" style="min-height: 150px;">
                <div class="tab">
                    <button class="tab-toggle ">Fillers & Botox</button>
                </div>
                <div class="content">
                    <h3 class="m-3"><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/fillers/" target="_blank">Fillers</a> & <a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/botulinum-toxin-botox/" target="_blank">Botox</a></h3>
                    <p>This is perhaps one of the most minimally invasive anti-ageing treatments, because all that is required is a couple of syringes. There is a wide range of dermal fillers that can be used to plump facial features up, and we use hyaluronic acid. The manner in which the filler will work is that it will act like a cushion underneath the skin, offering volume or even elasticity to the facial features that might have lost their shape. In addition, the acid will also provide better hydration to the face, making it look healthy. <br />
                        Hyaluronic acid is actually a naturally occurring substance that is found in all things alive and in humans, it is what relays essential nutrition from the bloodstream to the skin cells. The acid is found in high levels in the soft connective tissue as well as the fluid that surrounds the eyes. The acid can actually be extracted from the body, reformulated and used to inject into the body. As a matter of fact, apart from being used a dermal filler, the same acid is also used to ease joint pain caused by arthritis and rheumatism as well as heal certain types of wounds.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Facial volumizers</button>
                </div>
                <div class="content">
                    <h3 class="m-3"><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/facial-volumizers/" target="_blank">Facial volumizers</a></h3>
                    <p>The reason why your face looks like it is sagging is that it has lost the volume, and this is something that happens over the course of time. With the help of facial volumizers, the lost volume can be regained and your face can look young and healthy, once again. In the simplest of terms, facial volumizers are like a mini facelift, within a syringe. When the supportive tissue structures start facing the wear and tear that comes with age, facial volumizers can help. <br />
                        Facial volumizing actually has several sub categories, and procedures such as fat grafting and liquid face lifts are meant for the same. By injecting fat or a certain medical liquid, the creases of the face can be filled yet again, offering the face a fuller and plumper look. However, it is important to remember that these dermal fillers and facial volumizing procedures are by no means, permanent. After examining your particular requirement, we will chart a course of treatment for you, which will give you a face that is several years younger.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Fractional laser</button>
                </div>
                <div class="content">
                    <h3 class="m-3"><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/fractional-laser/" target="_blank">Fractional laser</a></h3>
                    <p>This is perhaps one of the most revolutionary and modern methods of rejuvenation of the face. This method has also been proven to be one of the most effective, when it comes to making the face look younger. A powerful laser is directed on the skin, and the heat travels into the dermis, reenergising the collagen, which in turn will rejuvenate the skin and also smoothen fine lines. The same process can also be used to reduce or even completely remove scars or pigmentation. <br />
                        While earlier laser often used carbon dioxide, modern day versions steer clear of the same, simply because the carbon dioxide could lead to severe side effects. Now, the skin is first numbed and a gel is applied to the skin. This gel allows for the more precise application of the laser and because the skin is numb, there is hardly any pain. Perhaps the biggest benefit of fractional laser is the fact that you need not get the entire face treated – you can choose which parts you want worked on.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Vampire lift</button>
                </div>
                <div class="content">
                    <h3 class="m-3"><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/vampire-lift/" target="_blank">Vampire lift</a></h3>
                    <p>Although the procedure might sound slightly scary, it is actually quite an advanced method. We will take blood from your body and harvest the platelets from the same. These platelets will then be processed to convert them into a powerful serum, which will then be injected into the areas where the sagging is most prominent. The energised platelets will actually rejuvenate the collagen production in the body, eventually making your skin look firmer and healthier. <br />
                        There are little to no side effects, because there are no external products being used and it will be your very own blood.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container text-center">
        <div class="col-md-12 ">
            <div class="row">
                <div class="section-title">
                    <h2 class="black-text text-center">Anti Ageing Treatments</h2>
                    <p>Our highly qualified plastic surgeons at AK Clinics routinely perform breast reduction of patients suffering from this condition. AK Clinics is equipped with ultra modern equipments with all facilities and state-of-art-technology. Moreover our doctors are highly sensitive to your feelings and emotions and ensures you of masculine look through this surgery.</p></div>

            </div>
        </div>
        <div class="col-md-12 ptb-1 ">
            <div class="row ">
                <div class="col-md-6">
                    <h3>Before the procedure</h3>
                    <p>During your very first consultation session, we will examine your skin and gauge the extent of the damage. This will also allow us to give you a clear idea of what treatment will work best for you. We will ask you to avoid or alter certain medications as well as certain habits, such as those related to alcohol and smoking. </p>
                </div>
                <div class="col-md-6">
                    <h3>After the procedure</h3>
                    <p>With most fillers and facial volumizers, there is little that you will be required to do, as post procedure care. While we might ask you to avoid stepping out in direct sunlight for a few days, we will suggest that you apply liberal amounts of sunscreen, when you do. We will also ask you to wear hats or caps, in order to cover your face and protect it from sunlight and dirt, because your face will be sensitive for a few days. In addition, we will ask you to use gentle facial cleansers and not scrub your skin too much. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-6" >
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">What Is The General Recovery Time?</a> </div>
                        <div id="collapseOne" class="collapse " data-parent="#accordion">
                            <div class="card-body">Normally, there is no downtime with these procedures and you should be able to return to your regular day in no time.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">How Many Repeat Sessions Will Be Required?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">This depends on how much your skin has been affected by ageing and also by how much of the face or body, you want covered. The number of sessions will be decided only after the completion of the first session.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">What Precautions Will Need To Be Taken?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">You will be asked to avoid alcohol and smoking for a while, and we might also ask you to avoid or alter certain medications. We might also ask you to avoid swimming in pools which have chlorinated water and staying out in the sun for too long.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Will The Procedures Be Painful?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">Most of these procedures are nothing more than needle pricks, but if you are worried too much about the pain, we will offer you options such as local anaesthetic or mild sedation.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" >
                <div class="col-md-12 text-center">
                    <h2>Possible Risks</h2>
                </div>
                <div class="row ptb-1">
                    <div class="col-md-12">
                        <p class="text-left">Although we are experts and pioneers in the field of offering a wide range of anti-ageing treatments, there are chances of side effects and complications. There will be redness, sensitivity, swelling and even bruising; but all of these are actually normal and will fade away in a matter of days. In certain cases, there could also be crusting on the skin and this too should fall off on its own in a few days. In the rarest of cases, there could be blood clots, which will have to be taken care of immediately.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetology_blogs'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>