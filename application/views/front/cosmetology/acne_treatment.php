<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"><span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/acne-treatment/"> <span itemprop="name">Acne Treatment</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Acne Scar Laser Treatment</h1>
                </div>
                <p>Acne can have reflective social and psychological effects on life. Even mild acne can be drastically disabling. Acne can affect anyone but it predominantly occurs during the teen years. Approximately 85% of people between the age of 12 and 25 develop acne. If you have an acne outbreak, there’s a lot you can do to keep it from putting a dent in your self esteem. We at AK Clinics provide the best acne treatments in Delhi, Ludhiana & Bangalore, helping you to get a clear, acne free skin.</p>
                <h4>What is acne?</h4>
                <p>Acne is referred to as an innate condition of the pores. When our skin is prone to acne, our skin pores clog with the dead skin cells much fast as compared to the normal skin pores. Healthy pores are known to shed about the one layer of dead skin cells per day but in case of acne prone skin, pores shed up to five layers of dead skin cells per day. Due to this, body is not able to clear the pore resulting in the clogged pore. Precisely, this condition is known as “Retention Hyperkeratosis” – shedding of dead skin cells more quickly as compared to the normal shedding.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area bg-light-gray ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="row text-center text-center">
                <div class="section-title">
                    <h2 class="black-text">What are the symptoms & types of acne?</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center">
            <div class="row">
                <p>It is important to identify the symptoms of acne first and then identify the type of acne you are experiencing. Your dermatologist will plan your treatment accordingly. Acne can be of two types and then further categorized into subtypes. </p>
                <ul class="text-left">
                    <li>Inflammatory acne</li>
                    <li>Non-Inflammatory acne</li>
                </ul>
                <div class="clr"></div>
                <div class="col-md-4 ">
                    <h4>Blackheads</h4>
                    <p>Blackheads are the clogged pores by the accumulation of sebum and dead skin cells. In blackhead, the top of the pore stays open while the rest of the pore is clogged which makes pore look like a black colored.</p>
                </div>
                <div class="col-md-4">
                    <h4>Whiteheads</h4>
                    <p>Whiteheads also form when a pore gets clogged by sebum and dead skin cells. However, the top of the pore is closed unlike blackheads. Whiteheads look like a small bump protruding from the skin. As compared to blackheads, whiteheads are more difficult to treat because the pores of the skin are already closed. </p>
                </div>
                <div class="col-md-4">
                    <h4>Papules</h4>
                    <p>Papules occur when the walls surrounding the pores of skin break down due to severe inflammation. Further, the pore become clogged which is tender to touch and appears pink in the color.</p>
                </div>
                <div class="clr"></div>
                <div class="col-md-4">
                    <h4>Pustules</h4>
                    <p>Pustules also occurs as the papules but pustules are filled with the pus. This look like a red bump coming out of the skin and are red in color with often yellow or white head on the top. </p>
                </div>
                <div class="col-md-4">
                    <h4>Nodules</h4>
                    <p>Nodules occurs due to the clogged, swollen pore which further causes the irritation in the area and grow larger. Unlike the pustules and papules, nodules are deeper beneath the skin and are difficult to treat.</p>
                </div>
                <div class="col-md-4">
                    <h4>Cysts</h4>
                    <p>Cysts are the results of clogged pores with the accumulation of the bacteria, sebum and dead skin cells. The clogged pores occur deep within the skin and are below the surface than nodules. Cysts are red or white bumps which are painful to touch. Cysts are the largest form of acne and are mostly due to the severe infection and causes scarring. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text">How acne occurs?</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 pt-1 text-center">
            <div class="row">
                <div class="col-md-4">
                    <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/acne-occurs-1.png" alt="" class="img-fluid lazy"></figure>
                    <p class="ptb-1">It begins with the clogging of pore. Skin cells are in a steady state of regeneration. As old cells die, they are sloughed off to divulge the fresh skin. But if you are acne prone, these dead skin cells starts clogging.</p>
                </div>
                <div class="col-md-4">
                    <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/acne-occurs-2.png" class="img-fluid lazy"></figure>
                    <p class="ptb-1">These dead skin cells start to accumulate inside the pore; cells become sticky and get trapped inside the pore to form a plug. This condition is referred to as “Microcomedone”.</p>
                </div>
                <div class="col-md-4">
                    <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/acne-occurs-3.png" alt="" class="img-fluid lazy"></figure>
                    <p class="ptb-1">Changes in hormones trigger the more oil production inside the pore. In the acne prone skin, skin starts shedding more dead skin cells which results in the blockage forming the optimum environment for acne causing bacteria i.e. P. Acnes. </p>
                </div>
                <div class="clr"></div>
                <div class="col-md-4">
                    <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/acne-occurs-4.png" alt="" class="img-fluid lazy"></figure>
                    <p class="ptb-1">Oil (sebum) in the skin serves as the nutrient for the bacterial proliferation. This results in the inflammation and redness on the blocked pore.</p>
                </div>
                <div class="col-md-4">
                    <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/acne-occurs-5.png" alt=""class="img-fluid lazy" srcset="<?php echo cdn('assets/template/uploads/'); ?>2015/06/acne-occurs-5.png 159w, <?php echo cdn('assets/template/uploads/'); ?>2015/06/acne-occurs-5-150x150.png 150w" sizes="(max-width: 159px) 100vw, 159px"></figure>
                    <p class="ptb-1">After the inflammation, there is a formation of tender bump filled with puss, known as pimple or acne. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container  text-center">
        <div class="col-md-12">
            <div class="row">
                <div class="section-title">
                    <h2 class="black-text">What are the causes of Acne?</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row">
                <p>Acne can occur on the face, back, neck, chest, buttocks, arms or any other skin area with a dispersion of the sebaceous pores. The main causes of acne breakout are as following;</p>
                <div class="col-md-4">
                    <h4>Hormonal Imbalance</h4>
                    <p>Acne mainly Acne Vulgaris is caused by the hormonal changes in the body and is directly caused by the rise of androgen hormone levels. With the rise in androgen levels, oil glands start producing excessive sebum causing the skin cells to rupture. The acne causing bacteria i.e. P.acnes acts on the ruptured cells which lead to the blockage of the pore and results in the acne. </p>
                </div>
                <div class="col-md-4">
                    <h4>Stress</h4>
                    <p>Stress does not cause acne directly. Stress aggravates the appearance of acne. It causes the expansion of the capillaries and also increases the blood flow. Due to this, skin appears red and inflamed. Stress also increases the androgen levels in adrenal glands of body which allows the sebaceous oil glands to secrete more sebum. Stress can also make acne breakouts severe and last longer.</p>
                </div>
                <div class="col-md-4">
                    <h4>Medications</h4>
                    <p>Some drugs or medications containing steroids, iodides, bromides etc can cause or even make the existing acne worse. Other medications which exacerbate the acne breakouts are anticonvulsant drugs. Sometimes, with the continuous use of oral contraceptive pills also can also cause acne. Women taking pills or medicines for disorders like PCOS are more prone to acne.</p>
                </div>
                <div class="clr"></div>
                <div class="col-md-4">
                    <h4>Dirt or Pollution</h4>
                    <p>Dust particles in the air or environment blocks the pores of the skin. Various chemical gases present in the environment causes inflammation on the blocked pore which further on bacterial action forms acne.</p>
                </div>
                <div class="col-md-4">
                    <h4>Cosmetics</h4>
                    <p>Cosmetic products can fill up the pores, worsen the pimples and prevents the skin from breathing. The excessive use of foundations, concealers and other cosmetics can work their way into and block up your pores resulting in acne.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 text-center">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="section-title">
                    <h2 class="black-text">Best Acne Scar Treatments</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <p>Our skin specialists before the treatment will perform a detailed and thorough skin test profile to determine the exact condition of your skin. Only after the examination our skin specialist will make a customized treatment plan for the best results. From the different treatments of acne we offer, dermatologist may recommend you from the following treatments accordingly.</p>
                <div class="col-md-6">
                    <h4>Combination Therapy for Acne</h4>
                    <p>In the combination therapy, if your acne is in initial stages, dermatologist may prescribe topical, lotions containing less concentration of isotretinoin along with the antibiotics like doxycycline, azithromycin, erythromycin etc. The concentration of the isotretinoin can be increased if the acne is severe. Doctor may also prescribe oral contraceptive pills like spironolactone as hormonal therapy for acne. Always consult with the doctor before taking any medications for acne.</p>
                </div>
                <div class="col-md-6">
                    <h4>Chemical Peels for Acne</h4>
                    <p>Chemical peels work by removing the outer dead skin layer. The controlled destruction of epidermal layer leads to the exfoliation and removal of the superficial lesions on the skin. After the exfoliation, there is regeneration of the new epidermal layer and dermal tissues of the skin. <a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical peels</a> like TCA (Trichloroacetic acid) Peel and Salicylic Acid peel are best for treating acne and even helps to fade the acne scars. </p>
                </div>
                <div class="col-md-6">
                    <h4>Microdermabrasion for Acne</h4>
                    <p>Microdermabrasion or Skin Polishing for acne is non-invasive procedure in which crystals are used to buff away the dead skin layer. It is the advanced skin treatment to resurface the skin texture and flush out the dead pores. It helps to remove the acne, age spots or uneven skin tone. Microdermabrasion helps in the stimulation the growth of new cells and makes skin clear, glowing and shiny after the treatment. </p>
                </div>
                <div class="col-md-6">
                    <h4>Laser Resurfacing for Acne</h4>
                    <p>In Laser Resurfacing technique for the acne, a device with high energy amplified light is used to vaporize the outer most layer of skin which further allows the regeneration of the new dermal cells of the skin. It also kills the acne causing bacteria. The lasers which can be used in the treatment are CO2, erbium YAG laser and pulsed dye yellow or blue light to treat acne or acne scars. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container ">
        <div class="col-md-12">
            <div class="row">
                <div class="section-title text-center">
                    <h2 class="black-text">What happens Before, During and After the Acne Treatment?</h2></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tabs">
                    <div class="tab">
                        <button class="tab-toggle ">Before the Acne Treatment</button>
                    </div>
                    <div class="content ">
                        <h3 class="heading">Before the Acne Treatment</h3>
                        <p class="pull-left m-1"> 
                            <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/before-acne.png" alt="Before Acne Scar Treatment" class="img-fluid lazy"> </p>
                        <p>During the consultation, doctor will examine your skin condition and will make a chart of the exact course of treatment. This would also be a good time to ask questions and clarify your doubts. Mention all your medical information, previous surgeries and current medical conditions with the doctor. </p>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">During the Acne Treatment</button>
                    </div>
                    <div class="content">
                        <h3 class="heading">During the Acne Treatment</h3>
                        <p class="pull-left m-1"> 
                            <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/during-acne.png" alt="During the Acne Treatment" class="img-fluid lazy"> </p>
                        <p class="description"> During the procedure, our technicians will give you protective eye shields to wear. A numbing gel is then applied properly to the acne areas which are to be treated. Our dermatologist then will use the method to treat acne which is customized according to your skin condition. The treatment takes approximately 30-60 minutes. </p>
                    </div>
                    <div class="tab">
                        <button class="tab-toggle">After the Acne Treatment</button>
                    </div>
                    <div class="content">
                        <h3 class="heading">After the Acne Treatment</h3>
                        <p class="pull-left m-1"> 
                            <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2015/06/after-acne.png" alt="Benefits after Acne Scar Treatment" class="img-fluid lazy"> </p>
                        <p class="description"> Once your treatment has been completed, our dermatologist will give some topical creams or lotions to continue applying. You will be asked to keep a tab on what you eat, drink a lot of water and ensure that you wash your face regularly. You might also be given medications, oral or topical, which contain some form of antibiotic. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Does Stress Cause Acne?</a> </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">Stress does not cause acne directly. It can aggravate acne but given the right strategy there is no reason why it should be an uncontrollable problem. There are some simple things you can do to help clear up your skin just through managing stress before seeking out other acne treatments.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">How Fast Can I Expect To Have Clear Skin?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">There is no overnight cure for the acne, and in most instances and even with the strongest of medications, it often takes months to get acne under control and years of maintenance to keep your skin clear. The treatment of the duration totally depends on the severity of the acne.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">What’s The Difference Between A Dark Spot And An Acne Scar?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">Darks spots are the post-inflammatory pigmentation after your acne has healed. They are not scars and most will fade over time. While scars are caused by acne that took a long time to heal. Scars appear as either raised bumps on the skin or as indentations.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Does Greasy Food, Milk Or Chocolate Cause Acne?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">The answer is probably not. Anecdotal associations between acne and particular foods like chocolate, ice cream and pizza have largely been discredited by scientific research. But research does point to a connection between overall diet and the development of acne symptoms. Researchers have shown that people whose diets include lots of high glycemic index foods (foods that are high in sugar and simple carbohydrates) tend to experience acne at a greater frequency than those who have low glycemic index diets.</div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsesix">Is There Any Downtime Of Acne Treatment?</a> </div>
                        <div id="collapsesix" class="collapse" data-parent="#accordion">
                            <div class="card-body">Following your procedure, the treated areas can appear red with some peeling for 2-7 days. Some patients have an exuberant response to PDT, and experience marked redness of their skin. Temporary swelling can occur. Darker pigmented patches called liver spots can become temporarily darker and then peel off leaving normal skin.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsenine">Will Acne Return After The Treatment?</a> </div>
                        <div id="collapsenine" class="collapse " data-parent="#accordion">
                            <div class="card-body">Acne is a condition which is stimulated primarily by the hormonal change, so as you age there is a possibility of a re-occurrence with acne. Do not leave your acne treatment incomplete. It will minimize the chances of the acne re-occurrence.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">

                <h2 class="center-txt main-title content-center">Side Effects of the Acne Treatment</h2>
                <div class="col-md-12 pt-1">
                    <p>Always get your treatment done by an <a href="<?= base_url(); ?>about-us/our-team/dr-aman-dua/">experienced dermatologist</a>. It will reduce the chances of any side effects or further complications. Here are some of the possible risks associated with acne treatment. However, these are common and will not last long.</p>
                    <ul class="pt-1">
                        <li>Dryness of the skin</li>
                        <li>Irritation</li>
                        <li>Burning sensation or peeling of skin</li>
                        <li>If you are taking oral medication, it could lead to dizziness or a feeling of being light headed</li>
                        <li>If you are using birth control pills to control acne, it could lead to high blood pressure and increase the chance of blood clots</li>
                        <li>Certain medications can also affect liver functions or increase the levels of cholesterol</li>
                    </ul></div>
            </div>
        </div>
    </div>
</div>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/cosmetology_blogs'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>