<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"> <span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/"> <span itemprop="name">Anti Ageing</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="3" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/botulinum-toxin-botox/"> <span itemprop="name">Botox</span></a>
                        <meta itemprop="position" content="4" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Anti-wrinkle treatment with Botox</h1>
                </div>
                <h4>What is Botulinum Toxin?</h4>
                <p>Botox®, botulinum toxin type A or OnabotulinumtoxinA is actually a toxin, which is created from bacteria that causes a condition known as botulism. The Botulinum toxin can actually block the nerve activity within the muscles, leading to a reduced activity in the muscles. Initially, Botox was used to treat cervical dystonia, severe underarm sweating and muscle stiffness, but today, its most popular use happens to be to treat fine lines and improve the tautness of facial skin.</p>
                <h4>Sites & Indications for Botox Treatment</h4>
                <figure><img data-src="<?= cdn('assets/template/frontend/images/'); ?>botox.png" class="img-fluid lazy" style="width:70%;" alt="Sites & Indications for Botox Treatment" ></figure>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text">Types of Botulinum Toxin</h2></div>
                <p>While most people might think that Botox is only meant to treat muscle related problems, there are actually several types of Botox and some of these include:</p>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 ptb-1">
                    <h4>OnabotulinumtoxinA</h4>
                    <p class="">This is the variety that has been used for the longest time and was one of the first to be approved by FDA. Its uses include:</p>
                    <p class="text-left ptb-1">
                        <em><strong>Medical</strong></em> – Being one of the most potent versions of the bacteria, it can be used to treat medical conditions such as blepharospasm, strabismus, hyperhidrosis, poststroke spasticity and even headaches and back spasms.<br>
                        <em><strong>Cosmetic</strong></em> – The cosmetic usage of Botox was approved by USFDA only in the early 2000s and could be used to treat wrinkles and fine lines. Utilising the type A version of the toxin, this has today become one of the most popular cosmetic treatments in the world. Today, it can help treat crow’s feet, neck lines, melolabial folds, hyperkinetic facial lines as well as horizontal forehead lines.</p>
                </div>
                <div class="col-md-6">
                    <h4>Myobloc<sup>®</sup></h4>
                    <p>This is a type B toxin, which when used in the right dosage can reduce the production of neutralizing as well as non-neutralizing antibodies. The main use of this type of Botox is mainly for cosmetic purposes, however studies are showing positive results for treating cervical dystonia.</p>
                </div>
                <div class="col-md-6">
                    <h4>Dysport<sup>®</sup></h4>
                    <p>This is yet another type A toxin, but this is used exclusively to treat medical conditions such as hemifacial spasms, blepharospasm and spasmodic torticollis.</p>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="service-area ptb-3 ">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Botox Uses & Treatment Advantages</button>
                </div>
                <div class="content" style=" height:315px;">
                    <h3 class="m-3">Botox Uses & Treatment Advantages</h3>
                    <figure><img class="img-fluid lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>botox-advantage.png" alt="Botox Uses &amp; treatment advantages"></figure>  </div>

                <div class="tab">
                    <button class="tab-toggle">Before, during & after the procedure</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Before, during & after the procedure</h3>
                    <div class="col-md-12">
                        <h4>Before procedure</h4>
                        <p>Before you can be administered Botox, there are a few tests and checks the doctor will have to run on you. They will be checking for:</p>
                        <ul class="">
                            <li class="">Infections in the area where the Botox has to be injected</li>
                            <li class="">Bladder infections</li>
                            <li class="">Conditions such as Lou Gehrig’s disease, Lambert-Eaton syndrome or myasthenia gravis</li>
                            <li class="">Breathing conditions such as bronchitis or asthma</li>
                            <li class="">Muscle weakness in the facial muscles or trouble swallowing</li>
                            <li class="">Bleeding tendency or heart related issues</li>
                            <li class="">Previous surgeries, blood thinning medication or other pre-existing medical conditions</li>
                        </ul>
                        <p>In addition, the doctor will also need to know whether you have had Botox injections in the past and whether there were any complications or side effects then. Pregnant women and nursing mothers need to have an in-depth conversation with their doctor, before deciding to go for the procedure.</p>

                        <h4>During procedure</h4>
                        <p>It is important that you get your Botox injections only from medically trained professionals, because the injection has to be administered into a muscle and only professionals will know how to do it. The injection could be administered in more than one site at a time, but this is something that the doctor will have pre-decided.<br>
                            Once the sites have been demarcated, the doctor might use a marker pen to make marks, so as to ensure that the injections are made at the precise sites. After this, it is just a matter of injecting the toxin, very carefully into the muscle.</p>

                        <h4>After procedure</h4>
                        <p>There might be bruising, but it will be temporary and will disappear in a few hours. Many a times, people have complained of having a headache, which could last for up 40 hours. In a very small percentage of people, there is eyelid drooping, but this is a very rare scenario. This normally happens, when the Botox moves around, which is why your doctor will suggest that you not rub the area for a minimum of 12 hours.<br>
                            You will be asked to not lie down for a few hours post the injection as that too can lead to eye lids drooping. You will also be given specific instructions on how to take care of the area for the next few days and when you will have to return for a repeat session.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>Frequently Asked Questions about Botox</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">What Happens If A Dose Is Missed?</a> </div>
                        <div id="collapseOne" class="collapse " data-parent="#accordion">
                            <div class="card-body">Botox is meant to provide temporary results, which means that if a dose is missed those results will not be visible. Apart from that, there will be little to worry about.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">What Happens In The Case Of An Overdose?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">If you notice any abnormal symptoms, it is important that you call your doctor immediately, because if caught in time, there is chance for a cure.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">What All Needs To Be Avoided?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">Ideally you should not drive immediately after Botox injections, because could be a slight vision impairment. Ideally you should not use any underarm deodorants or antiperspirants, especially if you are being treated for excessive underarm sweating. Take some time to return to your normal routine.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Will It Hurt?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">Only as much as it hurts to get an injection; the pain is that of a needle prick and the solution being pumped in. There can be a little discomfort, but it should fade away in some time.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>Possible Risks</h2>
                </div>
                <div class="row ptb-1">
                    <div class="col-md-12">
                        <p>There are a fair few risks involved with Botox injections, including:</p>
                        <ul class="">
                            <li>Allergic reaction</li>
                            <li>Severe weakness in the muscles</li>
                            <li>Trouble breathing or swallowing</li>
                            <li>Swelling in eyes or blurred vision</li>
                            <li>Inefficient bladder control or burning sensation while urinating</li>
                            <li>Pain in the chest or palpitations</li>
                            <li>Increase in sweat</li>
                            <li>Headache, exhaustion, stiffness in various body parts</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetology_blogs'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
