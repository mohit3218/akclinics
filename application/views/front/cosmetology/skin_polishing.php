<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology"> <span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/skin-polishing"> <span itemprop="name">Skin Polishing</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Skin Polishing / Microdermabrasion</h1>
                </div>
                <p>In the past few years, there has been a range of new methods and techniques that are being used to revitalize facial skin. Gone are the days, when a facial was considered as the ultimate facial care regime, because today, science has taken over and there are brand new methods of making your face shine.
                    <br /><br /> One such technique is known as skin polishing procedure or microdermabrasion – if the word would be broken up, it would consist of three parts – micro, derma and abrasion. Derma refers to the skin and abrasion is a method by which the dead skin cells of the skin are rubbed away. The method basically uses instruments that will create the effect of abrasion on the skin, while keeping it really gentle. The process will scrub away the thicker layer of your skin, which is the outer most and if often really uneven. When this layer is scrubbed away, the softer and more even skin that is hidden just beneath becomes visible, giving you a glow. This procedure is many a times called as face polishing or body polishing depending on the area that is being treated.</p>
                <h4>Types of Skin Polishing</h4><p>Microdermabrasion is one of the most common types of skin polishing methods, and these days, tools as well as diamonds are being used to do the same. There are also methods which can be tried out at home, using ingredients such as olive oil, sugar, baking soda, strawberry and wheat flour</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="section-title text-center ">
                <h2 class="black-text">Skin Polishing</h2>
            </div>
        </div>
        <div class="row text-left">
            <div class="col-md-6 ">
                <div class="col-sm-12 ">
                    <div class="col-lg-12 ">
                        <h4>Before the Surgery</h4>
                        <p>When you first head for a microdermabrasion treatment, you will have a consultation session with the doctor or medical specialist who will be conducting the procedure. <strong>You will be asked about a variety of things, such as:</strong>
                        <ul class="text-left">
                            <li>Why you want to get the treatment done</li>
                            <li>Whether you are undergoing any medical treatment</li>
                            <li>Whether you have any allergies</li>
                            <li>Whether you use alcohol or tobacco on a regular basis</li>
                        </ul></p>
                    </div>
                    <div class="col-lg-12 ">
                        <h3>After the Surgery</h3>
                        <p>There is actually no downtime with this procedure and you can return to your normal life within hours. You might need to invest in some special creams or moisturizers, which your doctor will suggest to you. There might be a little bruising, redness or skin sensitivity, immediately after the procedure, but the same will disappear in a matter of days.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="col-sm-12 ">
                    <h4>During the Surgery</h4>
                    <p>In simple terms, a skin polishing or microdermabrasion procedure will sand away your top layer of skin, which is normally thick and uneven. Normally, the procedure is done using a wand that has diamonds in the tip or has micro-particles. Once the top layer has been removed, there is a stimulation of the newer skin growth.</p>
                    <ul class="text-left pt-1">
                        <li><strong>Procedure style 1</strong> – A hand help device, which has some sort of a relay system for tiny crystals will be used on your face. The crystals will scrub away the dead skin cells and the uneven epidermis, which a vacuum or suction device will suck the crystals as well the dead skin cells back inside, revealing clean and smooth skin.</li>
                        <li><strong>Procedure style 2</strong> – This is a newer approach where a diamond tipped wand is worked gently across your skin. Your skin will be exfoliated, and the only discomfort you will feel is a slight scratching on your face. There is also a sensation of vibration on your skin, which is the dead skin being sucked away.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="col-md-12 text-center">
                    <h2>Skin Polishing Advantages</h2>
                </div>
                <div class="row">
                    <figure><img class="img-fluid lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>skin-polishing-advantage.png" alt="skin polishing advantage"></figure>
                </div>
            </div>  <div class="col-md-4">
                <div class="col-md-12 text-center">
                    <h2>Possible Risks</h2>
                    <p class="text-left">If the procedure has been done by professionals, then there is little risk, but the possible side effects include:</p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="margin-bottom-20 pt-1">
                            <li>Redness</li>
                            <li>Bruising</li>
                            <li>Swelling</li>
                            <li>Itching or irritation</li>
                            <li>Infection</li>
                            <li>Pain</li>
                        </ul>
                    </div>

                </div>
            </div> <div class="col-md-4">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">How Much Time Will The Procedure Take?</a> </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">Normally, the process takes only a few minutes, normally around 30 minutes.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Can I Go Back To Work Immediately After The Procedure?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">Absolutely – there is no downtime and the results are almost immediately visible, which means that you can go to work or even a party, after the procedure.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Will It Hurt?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">There can be a slight tingling sensation or a gentle irritation, but nothing that you will required anaesthesia or sedation for.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Should I Go For Microdermabrasion Even Though My Skin Looks Fine?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">The procedure can be done to reveal healthier skin and even though, your skin might look good enough, there are bound to be some dead skin cells, which will be removed through this method.</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/cosmetology_blogs'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>