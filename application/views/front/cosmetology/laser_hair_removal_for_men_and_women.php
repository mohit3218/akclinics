<style>thead-dark th {	color: #fff;background-color: #212529;	border-color: #32383e}
    @media only screen and (max-width:760px), (min-device-width:768px) and (max-device-width:1024px) {
        .cost-table tr td, table.cost-table tr th {	font-size: 12px}
        .cost-table thead tr {	position: absolute;	top: -9999px;	left: -9999px}
        .cost-table tr {	margin: 0 0 1rem}
        .cost-table tr:nth-child(odd) {	background: #ccc}
        .cost-table tr td {	border: none;	border-bottom: 1px solid #fff;	position: relative;	padding-left: 50%}
        .cost-table tr td:before {	position: absolute;	top: 0;	left: 6px;	width: 45%;	padding-right: 10px;	white-space: nowrap}
        .cost-table tr td:nth-of-type(1):before {	content: "City"}
        .cost-table tr td:nth-of-type(2):before {	content: "Face"}
        .cost-table tr td:nth-of-type(3):before {	content: "Side Locks"}
        .cost-table tr td:nth-of-type(4):before {	content: "Chin"}
        .cost-table tr td:nth-of-type(5):before {	content: "Lower Legs"}
        .cost-table tr td:nth-of-type(6):before {	content: "Neck"}
        .cost-table tr td:nth-of-type(7):before {	content: "Bikini line"}

        .specifications tr td, table.specifications tr th {	font-size: 12px}
        .specifications thead tr {	position: absolute;	top: -9999px;	left: -9999px}
        .specifications tr {	margin: 0 0 1rem}
        .specifications tr:nth-child(even) {	background: #eee}
        .specifications tr:nth-child(odd) {	background: #ccc}
        .specifications tr td {	border: none;	border-bottom: 1px solid #fff;	position: relative;	padding-left: 50%}
        .specifications tr td:before {	position: absolute;	top: 0;	left: 6px;	width: 45%;	padding-right: 10px;	white-space: nowrap}
        .specifications tr td:nth-of-type(1):before {	content: "Laser Used"}
        .specifications tr td:nth-of-type(2):before {	content: "Wavelength(nm)"}
        .specifications tr td:nth-of-type(3):before {	content: "Light Source"}
    } @media (min-width:320px) and (max-width:640px){
        .cost-table tbody tr td {text-align: right!important}
        .cost-table tr td:before {	position: absolute;	top: 9px!important;text-align: left;width: 45%;padding-right: 10px;white-space: nowrap}
        .specifications tbody tr td {text-align: right!important}
        .specifications tr td:before {	position: absolute;	top: 9px!important;left:6px;text-align: left;width: 45%;padding-right: 10px;white-space: nowrap}
    } </style>
    <?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"> <span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/"> <span itemprop="name">Laser Hair Removal</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row">
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Full Body Laser Hair Removal Clinic</h1>
                </div>
                <p>In today’s era, cosmetic world is full of dealings which aim at enhancing or improving the skin appearance. Laser hair removal/reduction is one of these popular techniques among cosmetic procedures. People in almost every cities are now concerned about their looks. Whether metro cities or non-metro cities, every person have to face the issue of unwanted hair growth. Nowadays, even men are also going for the hair removal treatments to get a metro sexual look. AK Clinics offer a range of cosmetological solutions, which will allow you to look young and fresh for a longer time. We have laser hair removal clinics in Delhi, Bangalore and Ludhiana which offers you the best and affordable cost for laser hair removal.
                    <br />Living in the world where falling necklines, thin spaghetti straps and short pants top are in the fashion, everyone wants the revealing skin to be hair free. Hair removal creams and epilators do offer you a temporary solution but don’t you get tired of using them every month or regular. We at AK clinics, offer you a permanent solution for unwanted hair in the form of laser treatment.</p>
                <h4>What is Laser Hair Removal/Reduction?</h4>
                <p>Laser Hair Removal technique is one of the most advanced procedures for removal of unwanted hair it’s faster and more comfortable. Simple and permanent unwanted hair reduction solution and known to be one of the safest hair reduction option too; it is an alternative to plucking, shaving, waxing, electrolysis, or chemical depilatories. The reason why this is not called permanent hair removal is because almost invisible hair may still be left after the complete treatment.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">How Does the Laser Hair Removal Procedure Work?</h2>
                    <p>This laser treatment for hair removal works on the principle of selective photothermolysis (SPTL). The process is to use a specific match of wavelength & pulse duration to get the optimal effect on the targeted hair follicles. The efficacy of the procedures depends on the cause of unwanted hair and the skill of the clinic; some patients may require touch sessions after completing the complete cycle of sittings.</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row text-center">
                <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/lhr-1.png" alt="Laser hair removal Clinic in Delhi" class=" img-fluid rounded-circle wth-50 pb-1 lazy">
                    <p>Laser beam first passes through the epidermal layer of skin and further to the hair follicles</p>
                </div>
                <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/lhr-2.png" alt="Full body laser hair removal" class="rounded-circle img-fluid wth-50 pb-1 lazy">
                    <p>Laser gets absorbed by the hair shaft and heat is generated around the hair follicle due to high beam intensity</p>
                </div>
                <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/lhr-3.png" alt="Laser hair removal cost in Delhi" class="rounded-circle img-fluid wth-50 pb-1 lazy">
                    <p>Generated heat damages the hair follicles and impedes the hair growth, resulting in hair free skin</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title ">
                    <h2 class="black-text">Areas treated with Laser For Men & Women</h2>
                    <p>We treat almost every kind of unwanted hair for both Men & Women. At AK Clinics, our motive is to provide comfort and excellent results. We most treated the areas include face laser hair removal or facial hair removal, bikini line laser hair removal, Leg Laser hair removal. But a complete detail is given as below.</p>
                </div>
            </div>
        </div>
        <div class="row ptb-1">
            <div class="col-md-7">
                <p class="ptb-1"><strong>Upper lip : </strong>For women, hair on upper lip is displeasure. There are various painful methods like wax, plucking which helps to remove hair but you have to do on regular basis. To get rid of this, more and more people are going for laser hair removal on upper lip.</p>
                <p class="ptb-1"><strong>Chin : </strong>Several women have undesirable hair around the chin area too. Laser hair removal of chin will hardly takes 15 minutes and is a straightforward procedure. </p>
                <p class="ptb-1"><strong>Cheeks &amp; Side locks : </strong> Women have to deal with unpleasant hair and wish for clear and smooth skin. On regular basis, they have to do wax, shave and bleach to hide the facial hair. Frequent use of these can irritate the skin. Facial hair removal is a quick process. A sequence of facial hair removal treatments can be life changing and gives you confidence. You may require touch up treatments if you want to keep the hair at bay. </p>
                <p class="ptb-1"><strong>Ear : </strong> No one wants the tufts of hair which are poking out of ears. Laser ear hair removal is the recent technique which can help you to get rid of these unwanted hair from the ears. This technique uses the pulse light that works to impair the hair follicle. This treatment can require multiple sessions and can be costly too. </p>
                <p class="ptb-1"><strong>Neck : </strong>Neck is quite the common part to have laser hair removal for men or women. Neck laser hair removal is the most suitable for people having light skin and dark hair. Every type of laser and IPL can be used for laser hair removal. </p>
                <p class="ptb-1"><strong>Upper Torso : </strong>It is no furtive that you are much confident having unsightly hair on your arms, under arms, legs, back etc. having hair on your upper body can bound you to select clothes and can make you uncomfortable in daily life. Do not let the hair to be the reason for keeping your full sleeves shirt on. Repeat shaving can make skin prone to ingrown hair, cysts or dark skin with marks. Go for laser hair treatment. You can go for full body laser hair removal. First hair is smashed using laser, hair will shed out completely and pores in the skin will close making your skin smooth and silky.</p>
                <p class="ptb-1"><strong>Lower Extremity : </strong>Laser hair removal treatment works better on the area having dark, thick or coarse hair. Legs often are the best hair removal for men and women through laser treatment. As the skin is thick on legs, you will experience almost minimal pain. Apart from this, there are few experiences in life which are bumpier or awkward than getting a bikini wax. Shaving always led to ingrown hair and uneven hair growth. Laser hair removal therapy can help you to attain a smooth, attractive skin. You may require sittings for proper results but unlike waxes, you don’t have to grow your hair out stuck between sessions, and you can uphold a groomed bikini area through shaving until your next appointment.</p>
                </ul>
            </div>
            <div class="col-md-5"> 
                <img class="img-fluid alignnone lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>laser.jpg" alt="Area treated with laser hair removal" title="Full body Laser Hair Removal in Delhi" width="752" height="837"> 
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title ">
                    <h2 class="black-text">Laser Hair removal cost in India</h2>
                    <p>The cost of laser hair removal treatment in India depends on certain factors which are;</p>
                </div>
            </div>
        </div>
        <div class="row ptb-1">
            <div class="col-md-7">
                <div class="row text-left">
                    <div class="col-md-6">
                        <h4>Area to be treated</h4>
                        <p>It is the most important factor to finalize the cost of the treatment. The more sensitive area, the more costly will be the treatment. Larger areas are costly as compared to small ones as it also takes a long time. </p>
                    </div>
                    <div class="col-md-6">
                        <h4>Sessions require</h4>
                        <p>The procedure requires number of sittings or sessions which are divided differently. Consult with your doctor and analyze the whole procedure criteria for your treatment. Higher numbers of session will increase the overall cost of treatment. </p>
                    </div>
                    <div class="clr"></div>
                    <div class="col-md-6">
                        <h4>Type of Hair</h4>
                        <p>Your hair type plays an important role in your laser hair removal. How dense and thick your hair are, how lengthy your hair are will determine the overall cost for the treatment.</p>
                    </div>
                    <div class="col-md-6">
                        <h4>Skin color</h4>
                        <p>Skin color is the most important factor which is to be considered for the laser hair removal. Light skin is considered easy for laser hair removal than dark skin. It is little difficult to focus on hair only in the dark skin which needs more precision and takes more time. This will also increase the cost.</p>
                    </div>
                    <div class="clr"></div>
                    <div class="col-md-6">
                        <h4>Standard of the clinic</h4>
                        <p>The overall total cost of the treatment also depends on the standard, quality and location of the clinic from where you are getting your laser hair removal treatment. Well hygienic, fully equipped and modern devices with services which will be provided to you will determine the cost of your treatment.</p>
                    </div>
                    <div class="col-md-6">
                        <h4>Types of laser system</h4>
                        <p>The type of laser system to be used also influences the cost of the treatment. Recent or advanced laser machines will give better results and efficient or fast technology gives better results making treatment costly. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-5 pt-5">
                <div class="video-wrapper">
                    <div class="video-container">
                        <iframe style="border: none;" src="//www.slideshare.net/slideshow/embed_code/key/s5HZHY5V0tLuuh" width="100%" height="355px" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <p class="clr m-1">Average cost of laser hair removal treatment for full face ranges between Rs. 3000-12,500 per session depending on which body area is being treated You may need more sittings for the proper hair removal. Laser hair treatment can be done on actively growing hair. You may need minimum of 6-8 sessions to get rid of unwanted hair.</p>
            <div class="table-responsive">
                <table class="table table-hover cost-table" role="table">
                    <thead role="rowgroup" class="thead-dark ">
                        <tr role="row">
                            <th role="columnheader">City</th>
                            <th role="columnheader">Face</th>
                            <th role="columnheader">Side locks</th>
                            <th role="columnheader">Chin</th>
                            <th role="columnheader">Lower legs</th>
                            <th role="columnheader">Neck</th>
                            <th role="columnheader">Bikini line</th>
                        </tr>
                    </thead>
                    <tbody role="rowgroup">
                        <tr role="row">
                            <td role="cell">Delhi</td>
                            <td role="cell">3,000/- to 4,000/-</td>
                            <td role="cell">1,800/- to 2,000/-</td>
                            <td role="cell">1,800/- to 2,000/-</td>
                            <td role="cell">7,000/- to 8,000/-</td>
                            <td role="cell">2,800/- to 3,200/-</td>
                            <td role="cell">2,800/- to 3,200/-</td>
                        </tr>
                        <tr role="row">
                            <td role="cell">Ludhiana</td>
                            <td role="cell">3,000/- to 4,000/-</td>
                            <td role="cell">1,800/- to 2,000/-</td>
                            <td role="cell">1,800/- to 2,000/-</td>
                            <td role="cell">7,000/- to 8,000/-</td>
                            <td role="cell">2,800/- to 3,200/-</td>
                            <td role="cell">2,800/- to 3,200/-</td>
                        </tr>
                        <tr role="row">
                            <td role="cell">Mumbai</td>
                            <td role="cell">4,000/- to 4,500/-</td>
                            <td role="cell">2,000/- to 2,500/-</td>
                            <td role="cell">2,000/- to 2,500/-</td>
                            <td role="cell">8,000/- to 8,500/-</td>
                            <td role="cell">3,000/- to 3,500/-</td>
                            <td role="cell">3,000/- to 3,500/-</td>
                        </tr>
                        <tr role="row">
                            <td role="cell">Bangalore</td>
                            <td role="cell">4,000/- to 4,500/-</td>
                            <td role="cell">2,000/- to 2,500/-</td>
                            <td role="cell">2,000/- to 2,500/-</td>
                            <td role="cell">8,000/- to 8,500/-</td>
                            <td role="cell">3,000/- to 3,500/-</td>
                            <td role="cell">3,000/- to 3,500/-</td>
                        </tr>
                        <tr role="row">
                            <td>Chennai</td>
                            <td role="cell">4,000/- to 4,500/-</td>
                            <td role="cell">2,000/- to 2,500/-</td>
                            <td role="cell">2,000/- to 2,500/-</td>
                            <td role="cell">8,000/- to 8,500/-</td>
                            <td role="cell">3,000/- to 3,500/-</td>
                            <td role="cell">3,000/- to 3,500/-</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <P class="small">*The above prices are only indicative for various cities &amp; different clinics and keep changing from time to time. Please get in touch with us for the best prices and package offers.</P>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container" >
        <div class="row">
            <div class="col-md-12 text-center">
                <div class=" section-title ">
                    <h2 class="black-text">Types of Laser Hair Removal</h2>
                    <p>There are number of wavelengths used as laser energy which are used for laser hair removal procedure in delhi. For hair removal in specific areas, there are some lasers which are better and effective.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h4>Intense Pulsed Light</h4>
                <p>This technique, most commonly called as IPL, is even currently in use for permanent hair removal. This is very effective in darker &amp; coarser hair. Though the operating doctor need a lot of experience as this may also cause skin burns. </p>
            </div>
            <div class="col-md-4">
                <h4>Electrolysis</h4>
                <p>In this technique, individual hair are removed from the targeted body area. At AK Clinics, we have found this technique extremely helpful for white hair as other forms prove ineffective. After a fine probe is inserted in the hair follicle, the hair are removed wit tweezers. 
            </div>
            </p>
            <div class="col-md-4">
                <h4>Alexandrite Laser</h4>
                <p>This treatment is found most effective in finer and thinner hair. Though this technique is not much in use independently, as it could most effectively treat the light skin tones. In the darker tones it has tendency to burn the skin. The laser has very good skin penetration rate. 
            </div>
            </p>
            <div class="clr ptb-1"></div>
            <div class="col-md-4">
                <h4>Pulsed Diode Array</h4>
                <p> It is considered as most suitable technology for laser hair removal. In this technique, a diode laser uses the semiconductor technology which targets specific chromophores in the skin. It offers the deepest penetration levels and superior melanin absorption. 
            </div>
            </p>
            <div class="col-md-4">
                <h4>Nd: YAG</h4>
                <p>This is the recent form of laser hair removal technique. “Nd” stands for Neodymium and “YAG” stands for yttrium aluminum garnet. This technique is mostly used for tattoo removal and hyper-pigmentation, but it can be used for removing hair too. 
            </div>
            </p>
        </div>
        <div class="row pt-3">
            <h3>Specifications of Various Lasers </h3>
            <table class="table table-hover specifications" role="table">
                <thead role="rowgroup" class="thead-dark ">
                    <tr role="row">
                        <th role="columnheader">Laser Used</th>
                        <th role="columnheader">Wavelength (nm)</th>
                        <th role="columnheader">Light Source</th>
                    </tr>
                </thead>
                <tbody>
                <tbody role="rowgroup">
                <td role="cell">Ruby</td>
                <td role="cell">694.3 nm</td>
                <td role="cell">Deep red</td>
                </tr>
                <tbody role="rowgroup">
                <td role="cell">Alexandrite</td>
                <td role="cell">755 nm</td>
                <td role="cell">Near-infrared</td>
                </tr>
                <tbody role="rowgroup">
                <td role="cell">Pulsed diode array</td>
                <td role="cell">810 nm</td>
                <td role="cell">Near-infrared</td>
                </tr>
                <tbody role="rowgroup">
                <td role="cell">Nd:YAG</td>
                <td role="cell">1064 nm</td>
                <td role="cell">Near-infrared</td>
                </tr>
                <tbody role="rowgroup">
                <td role="cell">Intense pulsed light (IPL is not a laser)</td>
                <td role="cell">650 nm</td>
                <td role="cell">Not a laser</td>
                </tr>
                </tbody>

            </table>
        </div>
    </div>
</div>

<div class="service-area ptb-3  bg-light-gray ">
    <div class="container" >
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title ">
                    <h2 class="black-text">8 Side effects and Risks of Laser Hair Removal Treatment</h2>
                    <p> You may experience following side effects after laser hair reduction but don’t worry, these side effects are common and rarely seen. They are not permanent &amp; 100% recoverable or reversible with time. However, these side-effects can be minimized if laser hair treatment is performed by a skilled doctor.</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-3 text-center">
            <div class="row ">
                <div class="col-md-4"> 
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-laser-hair-1.png" alt="Blistering" class="rounded-circle img-fluid lazy"><br>
                    <h4>Blistering </h4>
                    <p>You may experience blisters on the treated area after laser hair removal. Do not pop these blisters. Let them heal naturally. Blister fluid will aid in healing and act as biological dressing. Consult with your doctor. He will prescribe you ointment for these blisters. 
                </div>
                <div class="col-md-4"> 
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-laser-hair-2.png" alt="Burning &amp; Scaring" title="Burning & Scaring" class="rounded-circle img-fluid lazy"><br>
                    <h4>Burning &amp; Scaring</h4>
                    <p>If laser temperature is set too high, you may notice burns or scars on your skin. Just consult with your doctor and allow the new skin to heal underneath the burned skin. Try to keep cool the burned area after laser hair removal treatment. 
                </div>
                <div class="col-md-4"> 
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-laser-hair-3.png" alt="Hyper-pigmentation" title="Hyper-pigmentation" class="rounded-circle img-fluid lazy"><br>
                    <h4>Hyper-pigmentation<br>
                        (Skin darkening) </h4>
                    <p>This is another very common problem after an ablative laser treatment. It can be treated by using skin creams. Visit doctor and discuss this. He will prescribe you skin ointments which reduces the melanin content. 
                </div>
                <div class="clr ptb-1"></div>
                <div class="col-md-4"> 
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-laser-hair-4.png" alt="Hypo-pigmentation" title="Hypo-pigmentation" class="rounded-circle img-fluid lazy"><br>
                    <h4>Hypo-pigmentation (Skin lightening)</h4>
                    <br>
                    <p>Skin has the ability to treat itself. But you never know how long it will take. There are topical creams available for hypo-pigmentation. You can use those creams by consulting with doctor which will maintain the melanin content in your skin. 
                </div>
                <div class="col-md-4"> 
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-laser-hair-5.png" alt="Scab formation" title="Scab formation" class="rounded-circle img-fluid lazy"> <br>
                    <h4>Scab formation</h4>
                    <p>You may also observe scabs formation after laser hair removal on the treated area. Along with it, mild bruising may also occur if your skin is sensitive. No need to worry. These side effects will not last long. They will disappear shortly 
                </div>
                <div class="col-md-4"> 
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-laser-hair-6.png" alt="Irritation in Eyes" class="rounded-circle img-fluid lazy"><br>
                    <h4>Irritation in Eyes</h4>
                    <p>Eyes are the most sensitive body part. Due to the laser used in the treatment, you may see redness or may feel irritation in eyes. It is important to cover your eyes properly before your laser hair removal treatment. 
                </div>
                <div class="clr ptb-1"></div>
                <div class="col-md-4"> 
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-laser-hair-7.png" alt="Numbness" title="Numbness" class="rounded-circle img-fluid lazy"><br>
                    <h4>Numbness</h4>
                    <p>Tingling sensation or numbness is a common side effect of laser hair removal. It will not last long and will be normal within few days. 
                </div>
                <div class="col-md-4"> 
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-laser-hair-8.png" alt="Skin discoloration" title="Skin discoloration" class="rounded-circle img-fluid lazy"> <br>
                    <h4>Skin discoloration</h4>
                    <p>You may observe skin discoloration i.e. purple color patches on skin after laser hair removal. However, this is very rare. You may feel mild pain also in the discolored skin. 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container text-center" >
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title ">
                    <h2 class="black-text">Advantages of Laser Hair Removal over Waxing/Threading</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-left">
                    <h3>Waxing</h3>
                    <ul>
                        <li>Very costly in long Run though Cost per session is less</li>
                        <li>Not a permanent solution and recurrence is high</li>
                        <li>No standards avaiable of the products used</li>
                        <li>No precision possible for smal areas like ears</li>
                    </ul>
                </div>
                <div class="col-md-12 text-left ">
                    <h3>Laser Hair Removal</h3>
                    <ul>
                        <li>Cost per session is high but very cost effective</li>
                        <li>Near permanent results in most patients</li>
                        <li>Completely medical procedure with standard machine &amp; products &amp; safe</li>
                        <li>High percision possible for targetting specific areas and coarse hair</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 text-left ">
                    <h3>Did You Know</h3>
                    <p>1n 10 years, a women will spend an average of…</p>
                    <div class="row ptb-1 text-center">
                        <div class="col-md-4">
                            <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/shaving.png" alt="Shaving" class="rounded border lazy"><br>
                            <h4>SHAVING</h4>
                            Time: 360 hours 
                        </div>
                        <div class="col-md-4">
                            <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/waxing.png" alt="Waxing" class="rounded border lazy"><br>
                            <h4>WAXING</h4>
                            Time: 180 hours 
                        </div>
                        <div class="col-md-4">
                            <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/laser.png" alt="Laser Hair Removal" class="rounded border lazy"><br>
                            <h4>LASER</h4>
                            Time: 6 hours 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text text-center">What to expect during & after the LHR Procedure?</h2>
                    <p>LHR is completely a non-surgical procedure. This is generally considered an office hour procedure with no downtime. Some of the people may experience mild burns but that is generally rare and 100% recoverable. This procedure is much better, cost effective and hygienic than a parlour waxing or razors</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">During the Procedure</button>
                </div>
                <div class="content">
                    <h3 class="m-3">During the Procedure</h3>
                    <p>Some level of pain should be expected when undergoing Laser Hair removal procedure. Though we use anaesthetic creams to reduce the pain sensation. We also use icing during the procedure to even give a better experience of no pain sensation. Our doctors and technicians are very sensitive to patient’s discomfort and have large experience with lasers. We offer trial sessions to the patients so that they can make a informed choice.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Post Procedure Complications</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Post Procedure Complications</h3>
                    <p>Some of very rare incidents of hypo-pigmentation and hyper-pigmentation are also seen with diode lasers. Also some patients must have experienced acne and swelling around hair follicles. We have reduced the chances to negligible by choosing appropriate laser and custom settings for every patient. Some patients also are allergic to anaesthetic creams so we take measures to avoid pain.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Reduction not Removal</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Reduction not Removal</h3>
                    <p>Most commonly the procedure is called as Laser Hair Removal but no one in the world has seen 100% removal. The growth gets limited to unnoticeable thin hair. That is the reason that now the terminology is slowly changing to hair reduction rather than hair removal. After the complete procedure, some patients need maintenance sittings every 12-15 months.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h2 class="black-text text-center">Frequently Ask Questions</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id="accordion" class="mt-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <span class="card-link collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">Is The Procedure Painless And Safe For All Skin Types?</span> </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion" style="">
                                <div class="card-body">The laser used by us makes the procedure nearly painless. With very few before and after treatment guidelines to be ensured, make the whole procedure an absolute pleasure. This is the most effective, painless and permanent solution for full body hair removal. In addition, our laser is uniquely safe for all skin types including tanned skin, something that many other hair removal lasers are unable to offer.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <span class="collapsed card-link" data-toggle="collapse" href="#collapsefive">Do I Need To Plan ‘Time-Off’ For The Treatment?</span> </div>
                            <div id="collapsefive" class="collapse" data-parent="#accordion">
                                <div class="card-body">No. the procedure requires no downtime or recovery time. You can return to work immediately after laser therapy.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <span class="collapsed card-link" data-toggle="collapse" href="#collapsethi">How Long Do The Effects Of Laser Hair Removal Last?</span> </div>
                            <div id="collapsethi" class="collapse" data-parent="#accordion">
                                <div class="card-body">Usually it takes almost 2 weeks for the treated hair to shed. As laser hair removal treatment reduces the hair growth, some hair may grow back after the treatment. After 6-8 sittings, there is reduction in hair growth and you will need sessions after long intervals.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <span class="card-link collapsed" data-toggle="collapse" href="#collapsesteen" aria-expanded="false">Can Laser Hair Removal Be Performed On Darker Skin Tones?</span> </div>
                            <div id="collapsesteen" class="collapse" data-parent="#accordion" style="">
                                <div class="card-body">Yes. Hair removal on dark skin tones is little complicated as the skin is highly pigmented along with the hair. Focusing on hair only is difficult in dark skin.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <span class="card-link collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">Can Laser Hair Removal Be Performed On The Full Body?</span> </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion" style="">
                                <div class="card-body">Laser hair removal can be performed anywhere on the body including the bikini, face, underarms, and legs. Most men choose to have their back, chest, and beard line treated. For full body hair removal in Delhi you could visit AK Clinics.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <span class="card-link collapsed" data-toggle="collapse" href="#collapsesix" aria-expanded="false">Why Should AK Clinics Be Your First Choice?</span> </div>
                            <div id="collapsesix" class="collapse" data-parent="#accordion" style="">
                                <div class="card-body">
                                    <ul>
                                        <li> We use the latest State-of-the-Art Technology for the procedure</li>
                                        <li>Affordable treatment</li>
                                        <li> Minimal Pain (Painless)</li>
                                        <li> Vaccum assiisted device for body hair removal</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <span class="card-link" data-toggle="collapse" href="#collapsenine">What Kind Of Care And Precautions Should Be Taken When I Opt For Laser Hair Removal?</span> </div>
                            <div id="collapsenine" class="collapse " data-parent="#accordion">
                                <div class="card-body">It is vital to thoroughly check the credentials of the doctor who will adjust the equipment based on your skin and hair colour, thickness of hair and several other parameters. Just because ‘zapping’ unwanted hair is convenient doesn’t mean you forego certain rules of hygiene and safety. First and foremost, you should not undergo plucking, waxing, or electrolysis for six weeks before treatment, as the laser targets the hairs’ roots, which have already been (although temporarily) removed by waxing or plucking.
                                    Also avoid sun exposure, six weeks prior & post laser therapy.
                                    Another extremely important precaution to take is protective eye gear to be worn. The technician also needs to test the treatment on a smaller area and ice packs, anti-inflammatory creams need to be provided to you, post-treatment. A gap of more than a month is required between sessions.
                                    Feel free to ask your surgeon, how many laser sessions he or she has conducted before signing for any treatment. Ensure that expertise level is high to prevent complications later on. Laser hair removal (LHR) works best on dark hair and light skin.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <span class="card-link collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false">How Many Sittings Will I Need?</span> </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion" style="">
                                <div class="card-body">It actually depends upon the area, amount and density of the hair. Generally it takes 6-8 sittings which are 4 to 6 weeks apart. This is most effective at the time when the hair follicle is most pigmented.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <span class="collapsed card-link" data-toggle="collapse" href="#collapsese">What Is Laser Hair Removal And What Are Its Benefits?</span> </div>
                            <div id="collapsese" class="collapse" data-parent="#accordion">
                                <div class="card-body">Shaving, tweezing and waxing are traditional ways to remove unwanted hair, while laser hair removal is fast gaining popularity as a cosmetic procedure to remove unwanted hair. When hair follicles are beamed with highly concentrated light, it destroys the hair. Its main advantage, is mainly its speed, as small areas such as the upper lip can be treated in less than a minute, while large areas – the back or legs, barely require an hour.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <span class="card-link" data-toggle="collapse" href="#collapseelev">How Many Sessions Are Mandatory For Laser Hair Removal?</span> </div>
                            <div id="collapseelev" class="collapse" data-parent="#accordion">
                                <div class="card-body">There is no stipulated or mandatory number of sessions. Effect will differ from person to person as skin texture varies even between family members. Visible effects in the form of permanent hair reduction could on an average take four to six weeks, while some require even a dozen sessions for the same result. There have also been cases where people have been satisfied with their results across just two sessions.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <span class="card-link collapsed" data-toggle="collapse" href="#collapsefif" aria-expanded="false">Is It Safe To Get Laser Hair Removal On Your Face?</span> </div>
                            <div id="collapsefif" class="collapse" data-parent="#accordion" style="">
                                <div class="card-body">Yes, facial hair removal for women is safe and effective.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <span class="card-link collapsed" data-toggle="collapse" href="#collapsenteen" aria-expanded="false">Is Laser Hair Removal Permanent?</span> </div>
                            <div id="collapsenteen" class="collapse" data-parent="#accordion" style="">
                                <div class="card-body">Laser hair removal reduces the hair growth. If you have very thick, coarse hair, you will see an 85- 90% reduction in hair growth. Someone with fine hair will only see a reduction of approximately 45- 60%.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <span class="card-link collapsed" data-toggle="collapse" href="#collapsefour" aria-expanded="false">What Does Reduction Mean?</span> </div>
                            <div id="collapsefour" class="collapse" data-parent="#accordion" style="">
                                <div class="card-body">Reduction means that the coarse hair will become softer over sessions and eventually quite a percentage will become dormant and not grow. At the same time some patients who have hormonal issues may need maintenance sessions for long term.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <span class="card-link collapsed" data-toggle="collapse" href="#collapsefourH" aria-expanded="false">What Color Hair Can Be Removed?</span> </div>
                            <div id="collapsefourH" class="collapse" data-parent="#accordion" style="">
                                <div class="card-body">Brown and black hair is the easiest to remove. White and grey hair, which lack pigment, will not respond to treatment. But at AK Clinics, we perform Electrolysis technique which is extremely helpful for white hair as other forms prove ineffective. After a fine probe is inserted in the hair follicle, the hair are removed wit tweezers.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-light-gray ">
    <?php $this->load->view('front/partials/cosmetology_blogs'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>
