<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"> <span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/chemical-peels/"> <span itemprop="name">Chemical Peels</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1">
                    <h1 class="black-text text-left">Chemical Peels Treatment for Acne Scars, Hyperpigmentation & Wrinkles</h1>
                </div>
                <p>Everyone dream of a spotless, flawless complexion which is just lying under the surface of outer layer of our skin. Outer epidermal layer protects the inner dermal layer from external damage caused by sun, pollution, microbes in the environment etc. this makes our skin dull, hyper-pigmented, acne prone etc. by forming a dead skin cells layer. With the casting off this dull or dead layer of skin cells, we can have wrinkles free, hyper-pigmented free or acne free skin. Don’t worry, we offer the solution to your problem and the solution is <strong>“Chemical Peels”</strong></p>
                <h4>What is Chemical Peel?</h4>
                <p>A chemical peel, which is also known as derma peeling or chemo-exfoliation, is perhaps one of the most minimally invasive method of improving the nature or appearance of facial skin. The method can be used to smoothen the texture of the skin, by causing controlled destruction of the top most layer of the skin resulting in a fresher and healthier looking skin.It is important to note that the chemicals peels are not exclusively for the face, and can be used for other parts of the body too, especially the hands, neck and back. A chemical peel is an ideal solution to treat signs of skin damage due to ageing, over exposure to the sun, excessive usage of cosmetic products or even acne.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">How Chemical Peel Works?</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <p>Chemical peel works on three basic principals’ i.e. Exfoliation, Removal of skin imperfections, Regeneration
                    In this process, chemical peels are applied directly to the skin which destroys the outer layer of the skin through chemical activities. However, a controlled destruction of epidermis is there so as to avoid excessive damage to skin. Destruction of epidermis leads to the exfoliation and removal of superficial lesions on the skin followed by the regeneration of new epidermal & dermal tissues.</p>
                <div class="col-md-12 pt-3 text-center">
                    <figure class="pb-1">
                        <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/Exfoliation-1.png" alt="how ultherapy works" class="img-fluid lazy">
                    </figure>
                </div>
                <h4>Are you a Right Candidate for Chemical Peel?</h4>
                <p>People who have light hair or fair skin are considered to be the best candidates for chemical peel. If you have any skin infection, skin disorder, cut or burns, then you cannot go for a chemical peel. Other conditions which are to be kept in mind are;</p>
                <ul class="pt-1">
                    <li >If you are nursing or pregnant, you are not a right candidate for chemical peel</li>
                    <li >Suffering from skin disorders like psoriasis, rosacea, dermatitis etc.</li>
                    <li >Recently used skin products containing ascorbic acid, skin lightening agents or acid within 48 hours</li>
                </ul>

            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">Skin Conditions that Chemical Peel Treat</h2>
                </div>
            </div>
        </div>
        <div class="row pt-1">
            <div class="col-md-6">
                <p class="number">Chemical peel is largely a cosmetic procedure and it is indicated depending upon the patient’s requirement to correct skin textural problems. Treatments vary with the severity of the condition and the wishes of the patient. We at AK Clinics always help the patient with information that what is possible and what is desirable for the best results after the treatment.</p>
                <ul class="pt-1">
                    <li><strong>Skin lesions:</strong> Acne Vulgaris, Rosacea in remission</li>
                    <li><strong>Acne Complications:</strong> Hyper-pigmentation, Athropic scars &amp; Enlarged skin pores</li>
                    <li><strong>Skin Hyper-pigmentation:</strong> On Hormonal basis, connected to age or caused by sun radiation</li>
                    <li><strong>Wrinkles:</strong> Solar origin as well as mimic &amp; those connected to age</li>
                    <li><strong>Others:</strong> Melasma, Freckles, Lentigines, Facial Melanoses, Periorbital hyper-pigmentation </li>
                </ul>
            </div>
            <div class="col-md-6">
                <figure><img class="img-fluid text-center lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/chemical-peels-info.png" alt="Chemical Peeling Cost & Benefits"></figure>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">Types of Chemical Peels</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Alpha Hydroxy Acid Peels</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Alpha Hydroxy Acid Peels</h3>
                    <p>Alpha hydroxy acids (AHAs) are naturally occurring carboxylic acids and can be found in foods such as tomato juice and sour milk. These are the mildest of peels and can be used to handle dryness, fine wrinkles, acne and even pigmentation. As a matter of fact, AHA can even be combined with your regular facial wash or cream in a milder concentration and used on a daily basis.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Beta Hydroxy Acid Peels/ Salicylic Acid Peel</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Beta Hydroxy Acid Peels/ Salicylic Acid Peel</h3>
                    <p>Beta hydroxyl acid is now being preferred over AHA, because they are able to penetrate the pores in a more effective manner. Research has shown that BHA can control the excretion of sebum as well as acne. In addition, it will also remove dead skin cells in a more effective manner. Salicylic acid peels are a type of beta hydroxy (BHA) peel. These are best for treating comedonal acne. They also have anti-inflammatory effects that can decrease the erythema (redness) associated with acne.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Retinoic Acid Peel (Yellow Peel)</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Retinoic Acid Peel (Yellow Peel)</h3>
                    <p>This is the type of chemical peel that your local salon might not offer, because this needs more professional expertise. Yellow Peel is a recent addition to a wide range of anti-pigmentation solutions. It is an efficacious reversal method against hyper-pigmentation caused by sun exposure, photo-ageing or scars. It helps to increase the microcirculation and oxygen supply to the skin which results in even skin tone.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Croton Oil / Phenol Peel</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Croton Oil / Phenol Peel</h3>
                    <p>The origins of this peel go back to the 1920s and it was in the 1960s that it emerged as a popular skin treatment. The exfoliation will be more intense, and this will lead to more effective regeneration of the dermis.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Jessner’s Peel</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Jessner’s Peel</h3>
                    <p>Formerly known as the Coombe’s formula, this type of chemical peel was pioneered by Dr Max Jessner, who was a German-American dermatologist and it was on his name that the method was renamed. He combined salicylic acid, lactic acid and resorcinol in an ethanol base, because this was meant to break the intracellular bridges that occurred naturally between keratinocytes. The chemical solution does not enter the pores as deeply as certain other peels.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Trichloroacetic Acid (TCA) Peel</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Trichloroacetic Acid (TCA) Peel</h3>
                    <p>TCA used as an intermediate to deep peeling agent in concentration which range from 20-50%. Depth of penetration increases with 50%. TCA penetrates into the reticular dermis. TCA is preferred for darker skinned patients. It helps to smoothen the surface wrinkles and removes superficial blemishes. TCA also help to correct the skin pigmentation.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">Types of Chemical Peels</h2><p>Chemical peel is a sensitive process which needs precision while doing procedure. There are few factors which determine the cost of the chemical peel treatment. </p>
                </div>

            </div>
        </div>
        <div class="col-md-12 text-center pt-1">
            <div class="row">
                <div class="col-md-4">
                    <h4>Type of peel</h4>
                    <p>The type of peel used for the treatment determines the cost of treatment. The price of light chemical peel is less as compared to the deep chemical peel. There are medium chemical peels also which are of average cost.</p>
                </div>
                <div class="col-md-4">
                    <h4>Area and Nature</h4>
                    <p>The surface area of the skin for treatment also affects the cost of treatment. Cost of face treatment will be different from the cost for back or other body parts. If treatment demands for only minor skin imperfections, basic chemical peel treatment will be sufficient which costs less but if severe skin imperfections are to be treated then deep peels will be required which will charge more. </p>
                </div>
                <div class="col-md-4">
                    <h4>Number of Sessions</h4>
                    <p>Number of sessions also determines the cost of treatment. If less number of treatments will be required, then the overall cost will be less but if numbers of sessions are more, cost will rise.</p>
                </div>
                <div class="clr ptb-1"></div>
                <div class="col-md-4">
                    <h4>Aftercare</h4>
                    <p>After your chemical peel treatment, if there will be need of some after care treatments for effective results, then these can add up to the cost of treatment.</p>
                </div>
                <div class="col-md-4">
                    <h4>Standard of clinic</h4>
                    <p>If you are getting your treatment from a well reputed clinic by trained and skilled cosmetologist, your treatment cost may rise. </p>
                </div>
                <div class="col-md-4">
                    <h4>Location</h4>
                    <p>Location or cost of living also contributes to the cost of treatment. If you are living in a metro city or city having high cost of living, this will raise the treatment cost. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">4 Side effects of Chemical Peel Treatment</h2>
                    <p>There are very common side effects after the chemical peel treatment. But these side effects can be managed or treated easily with precautions. Just consult with your doctor if you face any of the side effects.</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center pt-1 ">
            <div class="row">
                <div class="col-md-3">
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/cp-1.png" alt="Dry Skin" title="Dry Skin" class="img-fluid rounded-circle lazy"><br>
                    <h4>Dry Skin</h4>
                    <p>Due to the chemical peel, your skin may become dry. But with the help of regular moisturizing, you can get rid of dry skin. Drink plenty of water and try to keep your body hydrated.</p>
                </div>
                <div class="col-md-3"> 
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/cp-2.png" alt="Flaking" title="AFlaking" class="img-fluid rounded-circle lazy"><br>
                    <h4>Flaking</h4>
                    <p>Chemical peel is about peeling off your dead skin. So if you see flakes on your skin or face, don’t get scared. It is a normal process and your new layer of skin will be visible within few days of flaking.</p>
                </div>
                <div class="col-md-3"> 
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/cp-3.png" alt="Redness" title="Redness" class="img-fluid rounded-circle lazy"><br>
                    <h4> Redness</h4>
                    <p>You may see red patches on the skin after the chemical peel procedure. As your outer layer of skin has undergone the chemical procedure, you might suffer from redness but it will not last long.</p>
                </div>
                <div class="col-md-3"> 
                    <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/cp-4.png" alt="Pigmentation" title="Pigmentation" class="img-fluid rounded-circle lazy"><br>
                    <h4>Pigmentation</h4>
                    <p> If you are going for chemical peel treatment by a specialized dermatologist, you may not have to face this. This is very rare side effect of chemical peel.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle ">What you should keep in mind before going for a Chemical Peel Treatment?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What you should keep in mind before going for a Chemical Peel Treatment?</h3>
                    <div class="row">
                        <div class="col-md-12"> While most people would be thrilled by the idea of all that a chemical peel can do for facial skin, not everyone can be considered a worthy enough candidate for the procedure. Your doctor will have to ensure that you are an ideal candidate for the same and in order to do this, he or she will have to ascertain that you:<br>
                            <p>When a person approaches a doctor with the intention of getting a chemical peel, there will be a fair few steps involved.</p> 
                            <strong class="ptb-1">These would include:</strong>
                            <ul>
                                <li>Why you want to get a Chemical Peel and whether you have realistic expectations from the Procedure</li>
                                <li>Whether you have any medical conditions, allergies or prior surgeries</li>
                                <li>Your alcohol consumption and smoking habits</li>
                                <li>Whether you are on any medications, vitamins or herbal supplements</li>
                            </ul>
                            <figure class="ptb-1">
                                <img class="img-responsive lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/Before-procedure-Concerns.png" alt="Before Chemical Peeling Treatment"></figure>
                            <p>Your doctor might also ask you to undergo a few medical tests and checks, to ensure that you are fit to undergo a chemical peel. You will also be made to understand the possible risks and side effects, so that you are able to make a fully informed decision.<br>
                                This would also be a good time to have all your doubts and queries clarified and you should not feel embarrassed or ashamed to ask anything.</p>
                        </div>

                    </div>
                </div>
                <div class="tab">
                    <button class="tab-toggle">What you can Experience during the Chemical Peel Procedure?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What you can Experience during the Chemical Peel Procedure?</h3>
                    <div class="row">
                        <div class="col-md-12"> Before a chemical peel is actually started, your face will be cleaned thoroughly. If you are opting for a deep chemical peel, then you will be given an anaesthetic or mild sedation, because there could be some pain involved. The pre-prepared chemical solution will then be brushed onto your skin and allowed to do its work. Timing is crucial, which is why you should get the treatment only from a reputed clinic or salon. There could be some tingling, burning or itching sensation and all these are considered normal.<br>
                            <figure text-center><img class="img-fluid lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/During-procedure.png" alt="During the Chemical Peeling Procedure"></figure>Depending on the type of chemical peel you have opted for, your doctor will either, simply wash off the peel, use a saline compress or slather your skin with ointment. If you have undergone a deep chemical peel, chances are that your skin will be dressed and the ointment will have to be left untouched for a day. </div>
                    </div>
                </div>
                <div class="tab">
                    <button class="tab-toggle">What are the benefits that you can expect after the chemical peel treatment?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">What are the benefits that you can expect after the chemical peel treatment?</h3>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Once the procedure has been completed, you are bound to feel a little itching, irritation or redness, but all of these are normal and will disappear in a few days. You will be given medication to apply on your skin, which will help reduce the redness or irritation and also help with the possible pain. There is a chance of infection or hyper-pigmentation and if you notice either, it would be best that you meet a doctor immediately. You will be asked to avoid makeup for a few days, because your skin will require time to heal. You will also be asked to apply a gentle moisturizer as well as sunscreen.</p>
                                <figure class="ptb-1 text-center">
                                <img class="img-fluid lazy" data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/After-procedure-benefits.png" alt="Benefits after Chemical Peel Treatment"></figure>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <h4>Mild Chemical Peel</h4>
                                    <li>Reduces pigmentation </li>
                                    <li>Controls acne </li>
                                    <li>Removes blemishes </li>
                                    <li>Helps with mild sun-damage</li>
                                    <li>Reduces mild blotchiness</li>
                                    <li>Fresher and younger looking appearance</li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <h4>Medium Depth Chemical Peel</h4>
                                    <li>Removes thin facial lines</li>
                                    <li>Reduces facial birthmarks </li>
                                    <li>Helps with dry skin and old acne scarring </li>
                                    <li>Helps with sun-damaged skin </li>
                                    <li>Improves blotchy skin</li>
                                    <li>Younger looking overall appearance </li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <h4>Deep Chemical Peel</h4>
                                    <li>Emanates deeper acne scar </li>
                                    <li>Long lasting effect</li>
                                    <li>Controls acne </li>
                                    <li>Helps with deep or course wrinkles</li>
                                    <li>Age spot removal</li>
                                    <li>Smoother skin, younger appearance</li>
                                    <li>Long-term results </li>
                                </ul>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="section-title text-center">
                    <h2 class="black-text text-center">FAQs</h2>
                    <p>We strive to provide highest quality at affordable cost with hygiene and imported equipments</p>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id="accordion" class="mt-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Will I Experience Any Side Effects?</a> </div>
                            <div id="collapseOne" class="collapse " data-parent="#accordion">
                                <div class="card-body">Most people have felt a sense of burning, itching, irritation, swelling or redness, but all of these are normal and tend to disappear in a few days.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Will A Chemical Peel Help My Skin?</a> </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body">That is the whole idea – when the peel is done by a professional and with the right chemicals, it should peel away the damaged cells, revealing the healthier and smoother skin.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">What Is The Recovery Time For A Chemical Peel?</a> </div>
                            <div id="collapsefive" class="collapse" data-parent="#accordion">
                                <div class="card-body">The procedure is generally done on an outpatient basis, which means that you should be able to return home the same day. However, your skin will now be extra sensitive for a few days, which means that you will have to take extra care, such as applying gentle moisturiser and sunscreen. Your skin should be ready and recovered in about 10 days’ time.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseseven">How Many Treatments Will I Require?</a> </div>
                            <div id="collapseseven" class="collapse " data-parent="#accordion">
                                <div class="card-body">This is something that is dependent on your skin – how unkempt it is or how tired it is will decide how many sessions you will need. This is a decision that your doctor will take for you.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">What Do Chemical Peels Do?</a> </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">Chemical peels help to resurface the skin and thus removing superficial layers of the skin. As a result, chemical peels promote the growth of a new healthy top skin layer and improve skin problems like hyper-pigmentation, fine lines and wrinkles, uneven texture and skin impurities.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">What Will My Skin Feel Like After A Chemical Peel?</a> </div>
                            <div id="collapsefour" class="collapse " data-parent="#accordion">
                                <div class="card-body">The state of your skin after a peel depends on the type of chemical used and your skin type. Superficial peels have limited effects. Medium peels may cause some redness and the deeper peels may require 10-14 days to recover.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsesix">Is A Chemical Peel Safe?</a> </div>
                            <div id="collapsesix" class="collapse" data-parent="#accordion">
                                <div class="card-body">Yes, it is. When performed correctly by a certified, experienced Medical Esthetician, such as our dermatologist at AK Clinics, a chemical peel is both safe and effective.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseeight">Can I Go Out Into The Sun After Having A Chemical Peel?</a> </div>
                            <div id="collapseeight" class="collapse" data-parent="#accordion">
                                <div class="card-body">You may go outside, but sunscreen must be worn for at least a few days after the peel as your skin will likely be a little more sensitive to sun exposure.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetology_blogs'); ?>
<div class=" bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>