<style>
    .background-one{background: #f0ebd8; opacity:0.9}
    .custom-padding {padding-top: 60px !important;padding-right: 40px !important;padding-bottom: 0px !important;padding-left: 0px !important;}
    a.slide-over{ opacity:0.8; background:#ccc;}
    a.slide-over:hover{ opacity:1;}
	

</style>
<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"> <span itemprop="name">Cosmetology</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area"> 
    <div class="jumbotron">
        <div class="container text-center">
            <h1 class="black-text">Experience Skin Specialist For Skin Treatment And Rejuvenation</h1>
            <p class="ptb-1">Looking good is important to almost everyone and for most people, their youth is when they have looked their best. It is with this thought that numerous people try to keep their skin and body looking young for as long as possible. Having understood the same, we at AK Clinics, offer a range of cosmetological solutions, which will allow you to look young and fresh for a longer time. Our team of doctors has decades of experience and will be able to gauge the precise condition and the most appropriate solution for the same. Some of the procedures that you can avail at AK Clinics include:</p>
        </div>
        <div class="container ">
            <div class="col-md-12">
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="card mb-3 box-shadow">
                        <img data-src="<?= cdn('assets/template/frontend/img/'); ?>service-image-4.jpg" class="card-img-top img-fluid lazy" alt="Laser Hair Removal">
                        <div class="card-body text-center"> <strong>Laser Hair Removal</strong>
                            <p class="card-text addReadMore showlesscontent">Excess hair can always be a problem, especially for women when they choose to wear short sleeves or sleeveless clothes. While hair removal creams and epilators might offer a temporary solution, we at AK Clinics offer a more permanent solution for unwanted hair, in the form of laser hair removal. Hair is removed almost without any pain and the area remains clean for the longest time. The process is safe and can be done on hands, legs and even the bikini area. This process is also becoming very popular with men, who wish to look to project a more metrosexual look!</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= base_url(); ?>assets/template/frontend/img/service-image-3.jpg" alt="Anti-Ageing Treatment">
                        <div class="card-body  text-center"> <strong>Anti-Ageing Treatments</strong>
                            <p class="card-text addReadMore showlesscontent">Ageing is a natural process and will happen to everyone, however there is no reason why it cannot be delayed a while. Doctors at AK Clinics have gained an expertise in offering a range of treatments that can help you defy Mother Nature for a few years. Some of the most popular treatments include Botox	, Dermal Filler	, Facial Volumizers	, Fractional Laser and Vampire Lift	. These treatments and therapies are meant to tighten the skin, reduce the appearance of wrinkles and make the skin look tighter and younger. Talking to someone from our team of experts will allow you make the decision of which procedure would be best for you.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= base_url(); ?>assets/template/frontend/img/service-image-1.jpg" alt="Cosmetology Treatments">
                        <div class="card-body  text-center"> <strong>Other Treatments</strong>
                            <p class="card-text  addReadMore showlesscontent">In addition to hair removal and anti-ageing treatments, we also offer a range of other cosmetology treatments, which allow your patients to look younger and have a better skin tone. With treatments such as skin polishing, laser Photo Facial, laser for acne, laser for pigmentation, acne scars treatment and chemical peels, patients can bid farewell to tired looking skin, acne, pigmentation and even scars. These treatments are conducted with surgical precision, leaving little to chance and ensuring that our patients walk away happy with their new image.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>cosmetology/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?= cdn('assets/template/frontend/img/'); ?>dr-profile-cover.jpg" style="background-image: url(&quot;<?= cdn('assets/template/frontend/img/'); ?>dr-profile-cover.jpg&quot;); ">
    <div class="container pt-0 pb-0">
        <div class="section-content">
            <div class="row">
                <div class="col-md-6 sm-height-auto">
                    <div class="background-one">
                        <h4 class="title mt-0 line-bottom line-height-2 text-black-222">BEGINNING OF THE NEW<span class="text-theme-colored"> AGE</span></h4>
                        <b class="small-separator"></b>
                        <div class="dr-aman-quote">
                            <blockquote>
                                <p>Our aspiration to become India's leading hair & skin clinic has driven us to provide best-in-class equipment & experienced doctors. This is the reason we had been consistently giving great results. We now aspire to take this to all major cities of India. </p>
                                <p><strong>Dr. Aman Dua</strong>
                                    <br>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ptb-3 doctors-details-section text-white">
                    <h2>Our Complete List Of Procedures</h2>
                    <div class="row drlist">
                        <div class="col-md-6">
                            <ul class="">
                                <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/fractional-laser/">Fractional Laser</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">Ultracel Skin Tightening Treatment</a></li>


                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="">
                                <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/facial-volumizers/">Facial volumizers</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/fillers/">Fillers</a></li>
                                <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/botulinum-toxin-botox/">Botox</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetology_blogs'); ?>
<?php $this->load->view('front/partials/location_block'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
