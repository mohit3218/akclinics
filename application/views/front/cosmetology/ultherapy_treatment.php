<?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/"> <span itemprop="name">Cosmetology</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetology/ultherapy-treatment/"> <span itemprop="name">Ultherapy Treatment</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">The A to Z of Ultherapy</h1>
                </div>
                <p>Modern medicine and science has made such a lot of progress in the past few years that it has actually become possible to defy time and look younger than what you would traditionally look at a certain age. Ultherapy is one of the latest methods in the world of skincare and is fast becoming a popular choice with people, all over the world.</p>
                <h4>What is Ultherapy?</h4>

                <p> Imagine a high intensity focused ultrasound being directed on your skin, and it working its magic underneath all the layers to ensure that you have skin that is firmer, more elastic and having a more even tone. Approved by the FDA, this is a non-invasive facelift, which will ensure that you have skin that you had in your late teens and early twenties. Initially, the treatment was being used only for the face, but now the same procedure can also be used to improve your décolletage. This means that from your forehead to your chest, you can now look younger.
                <p> 
                <p>The ulthera skin tightening treatment has been designed in such a manner that not only will it tighten your skin, it will also stimulate the collagen production within the skin, ensuring that your skin looks firmer and younger. The treatment is available at our clinic.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetology/">Cosmetology</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">ULTRAcel Skin Tightening</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non-surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">How Ultherapy Works?</h2>
                </div>
                <p>The ultherapy procedure is non-invasive and involves no cuts or injections – the procedure is done solely with the help of an ultrasound wand. Ultrasound rays will generate energy which will lead to stimulation of collagen, renewing your skin.</p>
            </div>
        </div>
        <div class="col-md-12 text-center pt-3">
            <figure  class="text-center">
                <img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/how-ultherapy-works.png" alt="ultherapy skin tightening treatment Clinic Delhi, India" class="img-fluid rounded lazy">
            </figure>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">Working theory of the HIFU</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center">
            <p>High Intensity Focused Ultrasound (HIFU) delivers the heat energy to skin layers and subcutaneous tissue that can stimulate and renew the skin’s collagen and therefore improving the surface and reducing sagging of the skin. This technique can be applied to the face as well as the whole body, and also, it works equally well on every skin tone.</p>
            <div class="row pt-3">
                <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/HIFU-1.png" alt="Before treatment" class="rounded img-fluid border lazy"><p class="pt-1">Before the treatment, collagen in our skin is weak and disorganized which makes our skin saggy with fine lines or wrinkles</p></div>
                <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/HIFU-2.png" alt="during treatment" class="rounded img-fluid border lazy"><p class="pt-1">Micro focused ultrasonic rays or energy passes through the skin tissue without damaging the cells in skin layer. It stimulates the collagen in skin </p></div>
                <div class="col-md-4"><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/HIFU-3.png" alt="after treatment" class="rounded img-fluid border lazy"><p class="pt-1">The skin reacts to this stimulus and new collagen and elastin is created which are now stronger. After a period of month, skin becomes tighter, firmer and lifted back into youthful position</p></div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Cost of Ultherapy Procedure</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Cost of Ultherapy Procedure</h3>
                    <p>The cost of an Ultherapy treatment can range depends upon the following factors which can make treatment less costly or costly.</p>
                    <ul class="ptb-1">
                        <li><strong>Areas to be Treated: </strong>Area of the body which is to be treated is an important factor which determines the cost of the ultherapy treatment. Larger the area to be treated higher will be the cost of ultherapy treatment.</li>
                        <li><strong>Geographical Site: </strong>The location of the clinic also contributes to the cost of procedure.</li>
                        <li><strong>Experienced Dermatologist:</strong> If you are getting your ultherapy treatment done from a highly experienced dermatologist, you will have to pay little more if you want your treatment to be perfect without any side effects.</li>
                    </ul>
                    <p>Since the treatment is so personalized, to get the most accurate price quote, it is best for you to visit AK Clinics nearby. You can fix your appointment on call and consult with our expert dermatologist.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Right Candidate for Ultherapy</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Right Candidate for Ultherapy</h3>
                    <p>A right candidate for Ultherapy is someone with skin that has relaxed to the point of looking, and often feeling, less firm. A lowered brow line or sagging skin of the eyelids, for instance, is often the first sign of “maturing” skin. Typically, those who are in their thirties and older who have mild to moderate skin laxity are considered to be the right candidates for ultherapy.
                    </p> <p> 
                        Ultherapy is not a replacement for surgery, there are many people who want some lifting but are not ready for surgery, either mentally, financially or logistically.
                    </p>   
                </div>
                <div class="tab">
                    <button class="tab-toggle">Why Ultherapy & its Benefits?</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Why Ultherapy & its Benefits?</h3>
                    <p>Ultherapy stimulates the collagen production and provides skin tightening & lifting effects. Think of Ultherapy as a non-surgical face lift – this treatment uses ultrasound energy to rejuvenate your skin. Patients who are undergoing ultherapy can look forward to the following benefits</p>
                    <div class="row ptb-3 text-center">
                        <div class="col-md-4">
                            <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/no-pain.png" class="rounded-circle lazy"></figure>
                            <h4>No Pain</h4> 
                        </div>
                        <div class="col-md-4">
                            <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/no-bleeding.png" class="rounded-circle lazy"></figure>
                            <h4>No Bleeding</h4>
                        </div>
                        <div class="col-md-4">
                            <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/no-downtime.png" class="rounded-circle lazy"> </figure>
                            <h4>No Downtime</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="black-text">Side effects of Ultherapy</h2>
                </div>
                <p>There are very few side effects which you can face after the ultherapy skin tightening procedure. However, these side effects will not last long and you will feel the new rejuvenated and tightened skin within few days. However, if your treatment is done by an experienced dermatologist, there are minimal chances of any side effects</p>
            </div>
        </div>
        <div class="col-md-12 text-center pt-3">
            <div class="row">
                <div class="col-md-4">
                    <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-ult-1.png" class="rounded-circle lazy" alt="Redness"></figure>
                    <h4>Redness</h4>
                    <p>Once the procedure has been completed, there is bound to be a little redness; in some people, it presents almost as if the skin is looking flushed. This redness will however, disappear in a matter of hours. </p>
                </div>
                <div class="col-md-4">
                    <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-ult-2.png" class="rounded-circle lazy" alt="Tenderness and Swelling"></figure>
                    <h4>Tenderness and Swelling</h4>
                    <p>In certain people there can be a small amount of swelling, tenderness and a tingling sensation. These too are temporary and will not last for more than a few hours. </p>
                </div>
                <div class="col-md-4">
                    <figure><img data-src="<?php echo cdn('assets/template/uploads/'); ?>2017/09/side-effects-ult-3.png" class="rounded-circle lazy" alt="Temporary Bruising"> </figure>
                    <h4>Temporary Bruising</h4>
                    <p>  This is a rare and once again temporary side effect, and is normally seen on a very small section of the skin. If the bruising does not reduce even after 24 hours, you might want to head back to get it checked. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="section-title text-center">
                    <h2 class="black-text text-center">FAQs</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id="accordion" class="mt-4"> 
                <div class="row">

                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Is Ultherapy A Surgery?</a> </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                <div class="card-body"> No. Unlike a facelift, Ultherapy is a non-invasive procedure that addresses the skin and support layers below it but doesn’t entail cutting or disrupting the surface of the skin. the procedure is done solely with the help of an ultrasound wand. Ultrasound rays will generate energy which will lead to production of collagen, renewing your skin.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">How Is Ultherapy Different From Other Cosmetic Procedures?</a> </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body">Ultherapy targets deep tissue below the skin, an area typically reached with cosmetic surgery. Ultherapy also provides ultrasound imaging, which allows the doctors to actually see the layers of tissue that are targeted during the treatment, and thus ensure the energy is delivered where it will be most effective.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">How Does Ultherapy Stimulate The Creation Of Collagen?</a> </div>
                            <div id="collapsefive" class="collapse" data-parent="#accordion">
                                <div class="card-body">Collagen is a natural protein that gives skin its youthfulness by keeping it firm, toned and elastic. As we age, collagen loses its elasticity and its ability to stand up to the effects of gravity that pull the skin downward. Ultherapy jumpstarts a repair process that produces fresh, new collagen. Ultrasound rays generate the energy to the skin layers which will lead to production of collagen in skin and renewing your skin.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseseven">Does Ultherapy Really Work?</a> </div>
                            <div id="collapseseven" class="collapse " data-parent="#accordion">
                                <div class="card-body">Absolutely! As a matter of fact, you will be able to see the results almost immediately and with multiple sessions, you will notice a visible improvement in the texture of your skin as well as the general complexion.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsenine">How Long Does The Ultherapy Treatment Take?</a> </div>
                            <div id="collapsenine" class="collapse" data-parent="#accordion">
                                <div class="card-body">This is something that is totally dependent on how many parts of your face and body you are going to get treated. The more the area, the more time it will take. However, a normal session could last anywhere between 30 to 45 minutes.</div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Can Ultherapy Replace A Face Lift?</a> </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">Ultherapy is an “uplift” not a “face lift.” While it is not a replacement for surgery, it is a viable option for those not ready for a face lift or those looking to prolong the effects of cosmetic surgery.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Are Men Good Candidates For Ultherapy?</a> </div>
                            <div id="collapsefour" class="collapse " data-parent="#accordion">
                                <div class="card-body"> Absolutely! Ultherapy works equally well on both men and women. Ultherapy delivers the heat energy to skin layers and subcutaneous tissue that can stimulate and renew the skin’s collagen and therefore be improving the surface and reduce sagging of the skin. This technique can be applied to the face as well as the whole body, and also, it works equally well on every skin tone.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsesix">What Is Unique About This Procedure?</a> </div>
                            <div id="collapsesix" class="collapse" data-parent="#accordion">
                                <div class="card-body">One of the main reasons why ultherapy stands apart from all other skin care procedures is that it is completely non-invasive and the procedure is absolutely painless. you will feel no pain during the procedure and even after the procedure. moreover, there is no downtime, you can go back to your normal routine after the treatment.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseeight">Is Ultherapy Safe?</a> </div>
                            <div id="collapseeight" class="collapse" data-parent="#accordion">
                                <div class="card-body">This is a procedure that has been deemed safe by FDA and is now being used by doctors and skincare experts, all over the world. Because only an ultrasound wand is used, there are no chances of any nicks, cuts or bruises. In addition, because this is a non-surgical procedure, there are no stitches and hence no downtime.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseten">How Long Until I See Results?</a> </div>
                            <div id="collapseten" class="collapse" data-parent="#accordion">
                                <div class="card-body">The results will be visible almost immediately; however, you will be able to see the actual results in a matter of days. In addition, when you undergo multiple sessions, you will notice a visible change in your skin tone and elasticity.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('front/partials/cosmetology_blogs'); ?>
<div class=" bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>