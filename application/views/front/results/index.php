<link rel="stylesheet" href="<?php echo base_url('assets/template/frontend/'); ?>css/result-video.css">
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>results/"> <span itemprop="name">Results</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="service-area ptb-3">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left"> 
                    <h1 class="black-text">Hair Transplant Before and After Results – AK Clinics </h1>
                </div>
                <p style="text-align:justify"> Hair Transplant Results and Hair Transplant reviews are the two main consideration for all patients planning hair transplant. 
                    All the results shown are 100% authentic; our <a href="<?= base_url(); ?>about-us/our-team/">hair transplant surgeons</a> are recommended in many of the international forums and platforms.
                    FUE Results Vs FUT Results : FUE Hair Transplant and FUT Hair Transplant is the ways grafts are extracted from the donor area. 
                    The out come of results is equally good in both. The choice much depends on the total requirement and future need. As of today most 
                    patient demand FUE for even very large areas. So we need to depend on Body <a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a>. In fact body hair transplant results are 
                    very good but still first choice is scalp. We recommend patient everything enabling him to make the right choice. 
                </p>
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item m-1"> <a class="btn btn-sm btn-outline filter-button cate_id" id="show-all" data-id="show-all" data-toggle="pill" href="<?= base_url(); ?>results/" role="tab" aria-controls="Cars" data-filter="hdpe" ari-selected="false">Show All</a> </li>
                    <li class="nav-item m-1"> <a class="btn btn-sm btn-outline filter-button cate_id" id="hair-transplant" data-id="success-stories" data-toggle="pill" href="<?= base_url(); ?>hair-transplant/" role="tab" aria-controls="Forest" data-filter="sprinkle" aria-selected="false">Hair Transplant</a>  </li>
                    <li class="nav-item m-1"> <a class="btn btn-sm btn-outline filter-button cate_id" id="prp-therapy" data-id="prp-therapy" data-toggle="pill" href="<?= base_url(); ?>results/prp-therapy" role="tab" aria-controls="Forest" data-filter="spray" aria-selected="false">PRP Therapy</a> </li>
                    <li class="nav-item m-1"> <a class="btn btn-sm btn-outline filter-button cate_id" id="cosmetic-surgery" data-id="cosmetic-surgery" data-toggle="pill" href="<?= base_url(); ?>results/cosmetic-surgery-results/" role="tab" aria-controls="Forest" data-filter="irrigation" aria-selected="false">Cosmetic Surgery</a> </li>
                </ul>
                <div class="ptb-1">
                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio"> 
                            <img class="card-img" alt="Hair Transplant Result of 1815 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-62.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1815 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1815 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1815 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-62.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio"> 
                            <img class="card-img" alt="Hair Transplant Result of 2131 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-61.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2131 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2131 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2131 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-61.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img" alt="Hair Transplant Result of 2526 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-60.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2526 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2526 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2526 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-60.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img" alt="Hair Transplant Result of 2358 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-59.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2358 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2358 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2358 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-59.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img" alt="Hair Transplant Result of 2000 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-58.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2000 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2000 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2000 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-58.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img" alt="Hair Transplant Result of 1250 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-57.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1250 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1250 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1250 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-57.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 1814 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-56.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1814 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1814 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1814 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-56.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 1540 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-55.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1540 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1540 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1540 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-55.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 1300 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-54.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1300 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1300 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1300 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-54.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 1650 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-53.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1650 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1650 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1650 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-53.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 2200 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-52.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2200 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2200 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2200 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-52.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 2030 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-51.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2030 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2030 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2030 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-51.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 1400 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-50.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1400 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1400 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1400 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-50.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 1920 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-49.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1920 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1920 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1920 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-49.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 2200 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-48.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2200 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2200 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2200 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-48.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 2125 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-47.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2125 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2125 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2125 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-47.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 1410 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-46.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1410 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1410 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1410 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-46.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 1100 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-45.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1100 Grafts">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1100 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-45.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 1100 Grafts" itemprop="contentUrl"  src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-42.jpg" alt="">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2674  Grafts (Grade 4) Frontal Area</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1100 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-42.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 2284  Grafts (Grade 3) Frontal Area" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-43.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2284  Grafts (Grade 3) Frontal Area">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2284  Grafts (Grade 3) Frontal Area</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2250  Grafts (Grade 3) Frontal Area" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-43.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 2250  Grafts (Grade 3) Frontal Area" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-31.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2250  Grafts (Grade 3) Frontal Area">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2250  Grafts (Grade 3) Frontal Area</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2250  Grafts (Grade 3) Frontal Area" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-31.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 2300 Grafts (Grade 5)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-33.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2300 Grafts (Grade 5)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2300 Grafts (Grade 5)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2300 Grafts (Grade 5)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-33.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 3000 Grafts (Grade 5) Frontal Area" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-1.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 3000 Grafts (Grade 5) Frontal Area">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 3000 Grafts (Grade 5) Frontal Area</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 3000 Grafts (Grade 5) Frontal Area" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-1.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Hair Transplant Result of 2530 Grafts (Grade 5)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-2.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2530 Grafts (Grade 5)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2530 Grafts (Grade 5)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2530 Grafts (Grade 5)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-2.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Result of 3000 Grafts (Grade 5)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-3.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 3000 Grafts (Grade 5)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 3000 Grafts (Grade 5)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 3000 Grafts (Grade 5)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-3.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 2284 Grafts (Grade 3)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-10.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 2284 Grafts (Grade 3)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 2284 Grafts (Grade 3)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 2284 Grafts (Grade 3)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-10.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 2020 Grafts (Grade 5) Crown Area" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-11.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 2020 Grafts (Grade 5) Crown Area">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 2020 Grafts (Grade 5) Crown Area</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 2020 Grafts (Grade 5) Crown Area" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-11.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 1400 Grafts (Grade 2)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-19.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 1400 Grafts (Grade 2)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 1400 Grafts (Grade 2)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 1400 Grafts (Grade 2)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-19.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 1300 Grafts (Traction Alopecia)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-20.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 1300 Grafts (Traction Alopecia)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 1300 Grafts (Traction Alopecia)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 1300 Grafts (Traction Alopecia)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-20.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 5240 Grafts (Grade 6)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-22.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 5240 Grafts (Grade 6)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 5240 Grafts (Grade 6)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 5240 Grafts (Grade 6)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-22.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 5100 Grafts (Grade 6)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-21.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 5100 Grafts (Grade 6)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 5100 Grafts (Grade 6)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 5100 Grafts (Grade 6)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-21.jpg">read more</a> 
                            </div>    
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 2000 Grafts (Grade 3v)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-23.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 2000 Grafts (Grade 3v)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 2000 Grafts (Grade 3v)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 2000 Grafts (Grade 3v)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-23.jpg">read more</a> 
                            </div>  
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Beard Hair Transplant Results of 2300 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-24.jpg" alt="">
                            <meta itemprop="name" content="Beard Hair Transplant Results of 2300 Grafts">
                            <div class="box-content">
                                <h4 class="title">Beard Hair Transplant Results of 2300 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Beard Hair Transplant Results of 2300 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-24.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Beard Hair Transplant Results of 1800 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-25.jpg" alt="">
                            <meta itemprop="name" content="Beard Hair Transplant Results of 1800 Grafts">
                            <div class="box-content">
                                <h4 class="title">Beard Hair Transplant Results of 1800 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Beard Hair Transplant Results of 1800 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-25.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Eyebrow Hair Transplant Results of 100 Grafts" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-26.jpg" alt="">
                            <meta itemprop="name" content="Eyebrow Hair Transplant Results of 100 Grafts">
                            <div class="box-content">
                                <h4 class="title">Eyebrow Hair Transplant Results of 100 Grafts</h4>
                                <a class="read-more" href="javascript:void(0);" title="Eyebrow Hair Transplant Results of 100 Grafts" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-26.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 2600 Grafts (Cicatrical Alopecia)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-27.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 2600 Grafts (Cicatrical Alopecia)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 2600 Grafts (Cicatrical Alopecia)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 2600 Grafts (Cicatrical Alopecia)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-27.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 2674 Grafts (Grade 6)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-28.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 2674 Grafts (Grade 6)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 2674 Grafts (Grade 6)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 2674 Grafts (Grade 6)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-28.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img" alt="Hair Transplant Results of 2000 Grafts (Grade 5)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-18.jpg" src="" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 2000 Grafts (Grade 5)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 2000 Grafts (Grade 5)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 2000 Grafts (Grade 5)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-18.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img" alt="Hair Transplant Result of 2200 Grafts (Grade 4)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-38.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2200 Grafts (Grade 4)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2200 Grafts (Grade 4)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2200 Grafts (Grade 4)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-38.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img" alt="Hair Transplant Result of 1500 Grafts (Grade 3V)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-37.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1500 Grafts (Grade 3V)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1500 Grafts (Grade 3V)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1500 Grafts (Grade 3V)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-37.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img" alt="Hair Transplant Result of 2200 Grafts (Grade 4)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-39.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2200 Grafts (Grade 4)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2200 Grafts (Grade 4)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2200 Grafts (Grade 4)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-39.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img" alt="Hair Transplant Result of 4000 Grafts (Grade 6)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-4.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 4000 Grafts (Grade 6)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 4000 Grafts (Grade 6)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 4000 Grafts (Grade 6)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-4.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img" alt="Hair Transplant Result of 6050 Grafts (Grade 7)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-6.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 6050 Grafts (Grade 7)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 6050 Grafts (Grade 7)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 6050 Grafts (Grade 7)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-6.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Result of 2400 Grafts (Grade 5)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-5.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2400 Grafts (Grade 5)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2400 Grafts (Grade 5)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2400 Grafts (Grade 5)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-5.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Result of 1370 Grafts (Grade 4)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-7.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1370 Grafts (Grade 4)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1370 Grafts (Grade 4)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1370 Grafts (Grade 4)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-7.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Result of 1540 Grafts (Grade 3v) (Crown Area)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-9.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 1540 Grafts (Grade 3v) (Crown Area)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 1540 Grafts (Grade 3v) (Crown Area)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 1540 Grafts (Grade 3v) (Crown Area)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-9.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 4550 Grafts (Grade 5)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-14.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 4550 Grafts (Grade 5)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 4550 Grafts (Grade 5)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 4550 Grafts (Grade 5)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-14.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 2200 Grafts (Grade 5)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-15.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 2200 Grafts (Grade 5)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 2200 Grafts (Grade 5)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 2200 Grafts (Grade 5)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-15.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 4050 Grafts (Grade 6)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-16.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 4050 Grafts (Grade 6)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 4050 Grafts (Grade 6)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 4050 Grafts (Grade 6)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-16.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Result of 2800 Grafts (Grade 5)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-36.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2800 Grafts (Grade 5)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2800 Grafts (Grade 5)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2800 Grafts (Grade 5)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-36.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Result of 2000 Grafts (Grade 5)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-32.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 2000 Grafts (Grade 5))">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 2000 Grafts (Grade 5)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 2000 Grafts (Grade 5)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-32.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Result of 3100 Grafts (Grade 3v)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-34.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 3100 Grafts (Grade 3v">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 3100 Grafts (Grade 3v)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 3100 Grafts (Grade 3v)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-34.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Result of 5200 Grafts (Grade 5)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-35.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 5200 Grafts (Grade 5)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 5200 Grafts (Grade 5)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 5200 Grafts (Grade 5)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-35.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Result of 4730 Grafts (Grade 6)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-40.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Result of 4730 Grafts (Grade 6)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Result of 4730 Grafts (Grade 6)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Result of 4730 Grafts (Grade 6)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-40.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe sprinkle" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="Hair Transplant Results of 2080 Grafts (Grade 4)" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-12.jpg" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 2080 Grafts (Grade 4)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 2080 Grafts (Grade 4)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Hair Transplant Results of 2080 Grafts (Grade 4)" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/results/ht-result-12.jpg">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe spray" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img" alt="PRP Hair Gain Therapy" itemprop="contentUrl"  src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-1.png" src="" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" title="PRP Hair Gain Therapy"  href="javascript:void(0);" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-1.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe spray" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-2.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-2.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe spray" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-3.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-3.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe spray" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-5.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-5.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe spray" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-6.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-6.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe spray" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-7.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-7.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe spray" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-8.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-8.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe spray" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-9.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-9.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe spray" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img class="card-img lazy" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-10.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-10.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe irrigation" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img" alt="Rhinoplasty  The Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-1.png" alt="">
                            <meta itemprop="name" content="Rhinoplasty The Surgery">
                            <div class="box-content">
                                <h4 class="title">Rhinoplasty  The Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="BRhinoplasty – The Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-1.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe irrigation" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img" alt="Gynecomastia" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-2.png" alt="">
                            <meta itemprop="name" content="Gynecomastia">
                            <div class="box-content">
                                <h4 class="title">Gynecomastia</h4>
                                <a class="read-more" href="javascript:void(0);" title="Gynecomastia" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-2.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe irrigation" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img" alt="Liposuction" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-3.png" alt="">
                             <meta itemprop="name" content="Liposuction">
                            <div class="box-content">
                                <h4 class="title">Liposuction</h4>
                                <a class="read-more" href="javascript:void(0);" title="Liposuction" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-3.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe irrigation" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img" alt="Breast Reconstruction Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-4.png" alt="">
                            <meta itemprop="name" content="Breast Reconstruction Surgery">
                            <div class="box-content">
                                <h4 class="title">Breast Reconstruction Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Breast Reconstruction Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-4.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe irrigation" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img" alt="Facial Cismetic Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-5.png" alt="">
                            <meta itemprop="name" content="Facial Cismetic Surgery">
                            <div class="box-content">
                                <h4 class="title">Facial Cismetic Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Facial Cismetic Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-5.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe irrigation" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img" alt="Breast Reconstruction Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-6.png" alt="">
                            <meta itemprop="name" content="Breast Reconstruction Surgery">
                            <div class="box-content">
                                <h4 class="title">Breast Reconstruction Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Breast Reconstruction Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-6.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe irrigation" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Liposution Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-8.png" alt="">
                            <meta itemprop="name" content="Liposution Surgery">
                            <div class="box-content">
                                <h4 class="title">Liposution Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Liposution Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-8.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe irrigation" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Liposution Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-9.png" alt="">
                            <meta itemprop="name" content="Liposution Surgery">
                            <div class="box-content">
                                <h4 class="title">Liposution Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Liposution Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-9.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe irrigation" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Blepharoplasty Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-10.png" alt="">
                            <meta itemprop="name" content="Blepharoplasty Surgery">
                            <div class="box-content">
                                <h4 class="title">Blepharoplasty Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Blepharoplasty Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-10.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe irrigation" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Breast Reconstruction Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-11.png" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 2200 Grafts (Grade 5)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 2200 Grafts (Grade 5)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Breast Reconstruction Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-11.png">read more</a> 
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 filter hdpe irrigation" itemscope itemtype="http://schema.org/ImageObject">
                        <div class="box21 Portfolio">
                            <img  class="card-img lazy" alt="Breast Reconstruction Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-12.png" alt="">
                            <meta itemprop="name" content="Blepharoplasty Surgery">
                            <div class="box-content">
                                <h4 class="title">Blepharoplasty Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Blepharoplasty Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-12.png">read more</a> 
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-3 pt-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>results/">Results</a></li>
                        <li><a href="<?= base_url(); ?>results/videos/">Videos</a></li>
                        <li><a href="<?= base_url(); ?>results/cosmetic-surgery-results/">Cosmetic Surgery Results</a></li>
                        <li><a href="<?= base_url(); ?>results/prp-therapy/">PRP Therapy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>



<div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="modalIMG" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <button type="button" class="close popup-class pull-right" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <div class="modal-content">
            <div class="modal-body mb-0 p-0">
                <img class="result-img-view-modal" src="" alt="" style="width:100%">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".read-more").click(function () {
            var postImage = $(this).data('id');
            $(".result-img-view-modal").attr("src", postImage);
            $('#modalIMG').modal('show');
        });
    });

    $(document).ready(function () {
        $(".filter-button").click(function () {
            var value = $(this).attr('data-filter');
            if (value == "all")
            {
                $('.filter').show('1000');
            } else
            {
                $(".filter").not('.' + value).hide('3000');
                $('.filter').filter('.' + value).show('3000');

            }
        });

        if ($(".filter-button").removeClass("active"))
        {
            $(this).removeClass("active");
        }
        $(this).addClass("active");
    });
</script>

