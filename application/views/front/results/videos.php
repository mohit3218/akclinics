<link rel="preload" href="<?php echo base_url('assets/template/frontend/'); ?>css/result-video.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="<?php echo base_url('assets/template/frontend/'); ?>css/result-video.css"></noscript>
<style>
@media (min-width:320px) and (max-width:640px) {.embed-responsive {width: 90%;margin: 0 auto;}.section-title, .section-title-1 {margin: 0 auto;width: 80%;}h1 {font-size: 18px;line-height: 1.5rem;text-align: center;}} </style>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>/results/"> <span itemprop="name">Results</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>/results/videos/"> <span itemprop="name">Videos</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
                <!-- Breadcrumbs /--> 
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> <div class="section-title-1 text-left"> <h1 class="black-text">Hair Transplant Result Videos – Before After</h1></div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom: 10px">
                        <div class="embed-responsive embed-responsive-21by9 videoShow">
                            <iframe class="embed-responsive-item videoShow" src="https://www.youtube.com/embed/rxS69PHXAw0?rel=0"></iframe>
                        </div>
                    </div>
                </div>
                
                <div class="regular slider">
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=rxS69PHXAw0' data-src="https://img.youtube.com/vi/rxS69PHXAw0/0.jpg" alt="rxS69PHXAw0"/>
                        <span itemprop="name" >Hair Transplant result - 2098 Grafts Bio-FUE</span>
                        <meta itemprop="description" content="30 years old patient from Ludhiana (Punjab) underwent hair transplant surgery at AK Clinics. The surgery was an uneventful and the patient is very happy with the results.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl"class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=i56LQbPrrOA' data-src="https://img.youtube.com/vi/i56LQbPrrOA/0.jpg" alt="i56LQbPrrOA"/>
                        <span itemprop="name" >AK Clinics Hair Transplant result - 1610 Grafts</span>
                        <meta itemprop="description" content="A 24 years old patient suffering from receding hairline problem so decided to underwent hair transplant surgery to maintain a complete natural look. The result is really good and patient feel very happy.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=8iFzTXWNXnY' data-src="https://img.youtube.com/vi/8iFzTXWNXnY/0.jpg" alt="8iFzTXWNXnY"/>
                        <span itemprop="name" >AK Clinics Beard Hair Transplant Result - 633 Grafts</span>
                        <meta itemprop="description" content="A 39 years old patient suffering from burn scars on his beard so underwent beard hair transplant with 633 grafts. They whole surgery is painless and the patient is happy with the result.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=3YJXqG6uO24' data-src="https://img.youtube.com/vi/3YJXqG6uO24/0.jpg" alt="3YJXqG6uO24"/>
                        <span itemprop="name" >Body Hair Transplant Surgery Result by Dr. Kapil - 2520 Grafts</span>
                        <meta itemprop="description" content="A 54 year old gentleman with NH Grd VII baldness presented to us for hair restoration after having undergone 4 unsuccessful surgeries outside ( 1 FUT; 2 FUE scalp; 1 FUE - body plus scalp; - One of the surgeries included hair being extracted from body as well.) The donor area of the scalp was depleted and only body hair could be taken out for transplantation. So, we extracted body hair from different areas. The patient got good results and was very happy.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=aTbAMA3d-9I' data-src="https://img.youtube.com/vi/aTbAMA3d-9I/0.jpg" alt="aTbAMA3d-9I"/>
                        <span itemprop="name" >Hair Transplant Result – 1020 Grafts by AK Clinics</span>
                        <meta itemprop="description" content="A 31 year old patient suffering from hair loss problem so underwent Bio-FUE Hair Transplant surgery at AK Clinics and came back for a review to share his experience. He is very happy with the result and post op instruction given by AK Clinics staff.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=U0jLD6Rl21M' data-src="https://img.youtube.com/vi/U0jLD6Rl21M/0.jpg" alt="U0jLD6Rl21M"/>
                        <span itemprop="name" >Bio-FUE Hair Transplant Results – 2530 Grafts </span>
                        <meta itemprop="description" content="The patient underwent Bio-FUE Hair transplant surgery (2530 grafts) at AK Clinics. The result is really good and patient feel very happy.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>

                      <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=r2qvuxvBPrY' data-src="https://img.youtube.com/vi/r2qvuxvBPrY/0.jpg" alt="r2qvuxvBPrY"/>
                        <span itemprop="name" >Hair Transplant result - 2098 Grafts Bio-FUE </span>
                        <meta itemprop="description" content="26 years old patients from Canada suffering from hair fall problem from 5-6 years underwent hair transplant surgery at AK Clinics. The surgery was painless and the patient feels happy after the transplant.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=eDUKRokEh4U' data-src="https://img.youtube.com/vi/eDUKRokEh4U/0.jpg" alt="eDUKRokEh4U"/>
                        <span itemprop="name" >1807 Grafts Bio-FUE Hair Transplant - AK Clinics</span>
                        <meta itemprop="description" content="A 37 year old patient speaks about the result he got after Bio-FUE Hair Transplant of 1807 grafts.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=Y7gsk5Ky0lI' data-src="https://img.youtube.com/vi/Y7gsk5Ky0lI/0.jpg" alt="Y7gsk5Ky0lI"/>
                        <span itemprop="name" >Hair Transplant Result - 1850 Bio-FUE</span>
                        <meta itemprop="description" content="A 26 years old patient underwent a successful hair transplant procedure at AK Clinics. We're extremely happy to share his review. For more information, call: +91-97799-44207 or visit https://akclinics.org">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=cqKFAY53XMo' data-src="https://img.youtube.com/vi/cqKFAY53XMo/0.jpg" alt="cqKFAY53XMo"/>
                        <span itemprop="name" >Hair Transplant result - 2192 Grafts Bio-FUE </span>
                        <meta itemprop="description" content="21 years old patient from underwent hair transplant surgery at AK Clinics. The surgery was an uneventful and the patient was very happy.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=X-Co7BsGXyE' data-src="https://img.youtube.com/vi/X-Co7BsGXyE/0.jpg" alt="X-Co7BsGXyE"/>
                        <span itemprop="name" >Hair Transplant Result at AK Clinics by Dr. Kapil Dua</span>
                        <meta itemprop="description" content="A 34 yrs old male presented with the complaint of hair loss due to turban. He has gone to 1800 Grafts Bio FUE Hair Transplant Surgery and was happy to see results.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=HGlgGNQSaYY' data-src="https://img.youtube.com/vi/HGlgGNQSaYY/0.jpg" alt="HGlgGNQSaYY"/>
                        <span itemprop="name" >Moustache Hair Transplant Result - AK Clinics</span>
                        <meta itemprop="description" content="A 28 year old patient speaks about the result he got after Moustache Hair Transplant at AK Clinics">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>

                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=nlHfXVwVzGU' data-src="https://img.youtube.com/vi/nlHfXVwVzGU/0.jpg" alt="nlHfXVwVzGU"/>
                        <span itemprop="name" >Hair Transplant Result at AK Clinics by Dr. Kapil Dua</span>
                        <meta itemprop="description" content="A 42 yrs old male from Hoshiarpur (Punjab) presented with the complaint of hair loss due. He has gone to 1800 Grafts Bio FUE Hair Transplant Surgery and was happy to see results. Visit AK Clinics to know more about hair transplant- https://akclinics.org/">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=SBiodJ9N9_A' data-src="https://img.youtube.com/vi/SBiodJ9N9_A/0.jpg" alt="SBiodJ9N9_A"/>
                        <span itemprop="name" >Hair Transplant Result at AK Clinics </span>
                        <meta itemprop="description" content="34 years old female suffering from hair loss problem underwent PRP hair treatment at AK Clinics. She is very happy to see the results. To know more about PRP hair treatment visit https://akclinics.org/prp-treatment/">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=OS8nzd6ZhTw' data-src="https://img.youtube.com/vi/OS8nzd6ZhTw/0.jpg" alt="OS8nzd6ZhTw"/>
                        <span itemprop="name" >2018 Grafts Bio-FUE Hair Transplant - AK Clinics</span>
                        <meta itemprop="description" content="A 31 year old patient speaks about the result he got after Bio-FUE Hair Transplant of 2018 grafts.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=DCZHxuu06mc' data-src="https://img.youtube.com/vi/DCZHxuu06mc/0.jpg" alt="DCZHxuu06mc"/>
                        <span itemprop="name" >Hair Transplant result - 2200 Grafts Bio-FUE </span>
                        <meta itemprop="description" content="36 years old patients suffering from hair loss problem underwent hair transplant surgery at AK Clinics. The surgery was painless and the patient feels happy after the transplant.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=_i-pKAsHaac' data-src="https://img.youtube.com/vi/_i-pKAsHaac/0.jpg" alt="_i-pKAsHaac"/>
                        <span itemprop="name" >AK Clinics Acne Scar Treatment Result </span>
                        <meta itemprop="description" content="27 years old patients from Bathinda (Punjab) suffering from acne scar underwent CO2 + Derma Roller procedure at AK Clinics and happy with the result.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=9sMjIoLo0_M' data-src="https://img.youtube.com/vi/9sMjIoLo0_M/0.jpg" alt="9sMjIoLo0_M"/>
                        <span itemprop="name" >Bio-Therapy Experience at AK Clinics</span>
                        <meta itemprop="description" content="A 32 year old patient from Barnala (Punjab) underwent Bio-Therapy share his experience about AK Clinics.">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>

                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=hr9HKN33vwc' data-src="https://img.youtube.com/vi/hr9HKN33vwc/0.jpg" alt="hr9HKN33vwc"/>
                        <span itemprop="name" >Hair Gain Therapy at AK Clinics</span>
                        <meta itemprop="description" content="A 26 year old patient suffering from hair fall problem share his experience about hair gain therapy done by AK Clinics doctors. To know more about AK Clinics visit">
                        <meta itemprop="uploadDate" content="2018-04-16T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=RWKZS72xz3Y' data-src="https://img.youtube.com/vi/RWKZS72xz3Y/0.jpg" alt="RWKZS72xz3Y"/>
                        <span itemprop="name" >5000 Graft Hair Transplant of New Zealand Patient</span>
                        <meta itemprop="description" content=" A 50 year old patient from New Zealand have gone 1st 5000 graft Hair Transplant surgery. He also gone 2nd surgery after 2 year. For more information, call: +91-97799-44207 or visit https://akclinics.org or http://www.akclinics.com">
                        <meta itemprop="uploadDate" content="2014-12-30T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=gZyIXd57byE' data-src="https://img.youtube.com/vi/gZyIXd57byE/0.jpg" alt="gZyIXd57byE"/>
                        <span itemprop="name" >FUE Hair Transplant Result - 2500 Grafts - AK Clinics</span>
                        <meta itemprop="description" content="The patient underwent a successful hair transplant procedure at AK Clinics. We're extremely happy to share the hair transplant result. For more information, call: +91-97799-44207 or visit https://akclinics.org or http://www.akclinics.com">
                        <meta itemprop="uploadDate" content="2014-12-30T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/2500_grafts.png" />
                        <meta itemprop="duration" content="PT0M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=nzd4W8HYwoU' data-src="https://img.youtube.com/vi/nzd4W8HYwoU/0.jpg" alt="nzd4W8HYwoU"/>
                        <span itemprop="name" >FUE Hair Transplant Results after 8 months </span>
                        <meta itemprop="description" content="The patient underwent hair transplant procedure at Ludhiana for the frontal region. He is extremely happy about his hair transplant result. For more information, call-+91-9779944207 or visit https://akclinics.org or htttp://www.akclinics.com">
                        <meta itemprop="uploadDate" content="2014-12-30T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/result_after_8.png" />
                        <meta itemprop="duration" content="PT1M37S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=UG9ohOtXY14' data-src="https://img.youtube.com/vi/UG9ohOtXY14/0.jpg" alt="UG9ohOtXY14"/>
                        <span itemprop="name" >Hair Transplant Testimonial @ AK Clinics </span>
                        <meta itemprop="description" content="This patient from US underwent Hair Transplant Surgery in Delhi. for more information on Hair Transplant visit https://akclinics.org/ or call:+91-97799-44207">
                        <meta itemprop="uploadDate" content="2014-12-26T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/testimonial@akclinics.png" />
                        <meta itemprop="duration" content="PT2M45S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=3jWZ6G1RnXU' data-src="https://img.youtube.com/vi/3jWZ6G1RnXU/0.jpg" alt="3jWZ6G1RnXU"/>
                        <span itemprop="name" >FUE Hair Transplant Results - 4000 Grafts - Patient Testimonial</span>
                        <meta itemprop="description" content="TOur patient speaks about the positive reviews he got about us for Hair Transplant. If you need further information, please visit https://akclinics.org/ or call +91-97799-44207">
                        <meta itemprop="uploadDate" content="2014-12-26T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/result_4000.png" />
                        <meta itemprop="duration" content="PT2M54S" />
                    </div>

                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=uy03adEtDfE' data-src="https://img.youtube.com/vi/uy03adEtDfE/0.jpg" alt="uy03adEtDfE"/>
                        <span itemprop="name" >FUE Hair Transplant in Delhi - Partial Trim - AK Clinics</span>
                        <meta itemprop="description" content="The patient wanted a Hair Transplant with FUE but had a concern about trimming the complete head. So his surgery was done by trimming only partial area. Please listen to his review about the same">
                        <meta itemprop="uploadDate" content="2014-12-20T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/partial_trim.png" />
                        <meta itemprop="duration" content="PT2M05S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=GdUKmCxlae0' data-src="https://img.youtube.com/vi/GdUKmCxlae0/0.jpg" alt="GdUKmCxlae0"/>
                        <span itemprop="name" >FUE Hair Transplant - FUE after Strip FUT - AK Clinics</span>
                        <meta itemprop="description" content="The patient underwent a FUE Hair Transplant in Ludhiana. Dr Kapil Dua & Dr Aman Dua, the founders & leads surgeons completed the surgery which was absolutely pain free. The results were extremely good .">
                        <meta itemprop="uploadDate" content="2014-12-20T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/FUE_after_strip.png" />
                        <meta itemprop="duration" content="PT2M35S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=keDTaX9DupM' data-src="https://img.youtube.com/vi/keDTaX9DupM/0.jpg" alt="keDTaX9DupM"/>
                        <span itemprop="name" >Hair Transplant Results - 1 Year Post Surgery - AK Clinics </span>
                        <meta itemprop="description" content="Hair Loss is a very emotionally disturbing problem which many face without mentioning a word. Hair Transplant can help patients get rid of such situation. With our Hair Transplant Clinics in India we provide best in class patient service to achieve best results with minimally invasive technique">
                        <meta itemprop="uploadDate" content="2014-12-18T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/results_1_year_post.png" />
                        <meta itemprop="duration" content="PT1M07S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=VX3ausn3tVY' data-src="https://img.youtube.com/vi/VX3ausn3tVY/0.jpg" alt="VX3ausn3tVY"/>
                        <span itemprop="name" >Happy Patient with his FUE Hair Transplant Result After 1 Year - Dr. Kapil Dua </span>
                        <meta itemprop="description" content="Revolutionary Hair Transplant Results From Dr Kapil Dua & Dr Aman Dua. Our patients share how carefully they evaluate hair transplant clinics across the world and then reach out to us for Hair transplant. Thereon they are happy with their decision. Call us now +91-97799-44209 or visit https://akclinics.org/">
                        <meta itemprop="uploadDate" content="2014-12-18T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/happy_patient_wit_FUE.png" />
                        <meta itemprop="duration" content="PT2M44S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=kUrkMIB68p0' data-src="https://img.youtube.com/vi/kUrkMIB68p0/0.jpg" alt="kUrkMIB68p0"/>
                        <span itemprop="name" >Hair Transplant in Delhi - Medical Tourism on the Rise </span>
                        <meta itemprop="description" content="We'd like share how patients from worldwide are reaching out to us for hair transplant surgery. Dr Kapil Dua & Dr Aman Dua are world-renowned surgeons for hair transplant giving best advice to patients. call us now +91-97799-44207 or visit https://akclinics.org/">
                        <meta itemprop="uploadDate" content="2014-12-18T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/delhi_medical_tourism.png" />
                        <meta itemprop="duration" content="PT1M40S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=X23r2jIgE-E' data-src="https://img.youtube.com/vi/X23r2jIgE-E/0.jpg" alt="X23r2jIgE-E"/>
                        <span itemprop="name" >Satisfied Patient with his Hair Transplant Results – 2400 Grafts by Dr Kapil Dua</span>
                        <meta itemprop="description" content="The patient underwent FUE surgery at AK Clinics, New Delhi. He came back for a review and sharing his experience. AK Clinics has state of art facilities for Hair Transplant in Ludhiana & Hair Transplant in Delhi. The patient is young & was suffering for Androgenetic Alopecia. We planned a transplant for front half. The results are excellent after one year of surgery.">
                        <meta itemprop="uploadDate" content="2014-12-03T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/satisfied_patient.png" />
                        <meta itemprop="duration" content="PT04M42S" />
                    </div>

                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=zgI1wFC23hg' data-src="https://img.youtube.com/vi/zgI1wFC23hg/0.jpg" alt="zgI1wFC23hg"/>
                        <span itemprop="name" >3000 Grafts Bio FUE Hair Transplant Surgery Result After 9 Months</span>
                        <meta itemprop="description" content="A 38 yrs old male presented with the complaint of hair loss for the past 10 years with no active hair fall for 2 years. He was suggest to for 3000 Grafts FUE Hair Transplant Surgery and was happy to Result after 9 month.">
                        <meta itemprop="uploadDate" content="2014-11-11T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/FUE_hair_transplant_cntr_in_ldh.png" />
                        <meta itemprop="duration" content="PT00M34S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=jDGObBawsK4' data-src="https://img.youtube.com/vi/jDGObBawsK4/0.jpg" alt="jDGObBawsK4"/>
                        <span itemprop="name" >Cicatricial Alopecia Patient 2600 Graft Hair Transplant</span>
                        <meta itemprop="description" content="Cicatricial Alopecia patient life has changed after 2600 Graft Hair Transplant by Dr Kapil Dua. She have gone three session of surgery.">
                        <meta itemprop="uploadDate" content="2012-02-08T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/best_fue_hair_trnsplnt_in_ldh.png" />
                        <meta itemprop="duration" content="PT00M41S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=aX1rS3d90ZU' data-src="https://img.youtube.com/vi/aX1rS3d90ZU/0.jpg" alt="aX1rS3d90ZU"/>
                        <span itemprop="name" > 2020 Graft Bio-FUE Hair Transplant</span>
                        <meta itemprop="description" content="When Raghuveer first arrived at our Delhi clinic, he was extremely disheartened, because he had been losing so much hair. However, 2020 grafts and a medically advanced BIO FUE process later, he is now extremely happy with what he sees in the mirror.">
                        <meta itemprop="uploadDate" content="2012-01-11T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/FUE_hair_trnsplnt_srgry_in_dubai.png" />
                        <meta itemprop="duration" content="PT00M54S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=SOGYb46IC-E' data-src="https://img.youtube.com/vi/SOGYb46IC-E/0.jpg" alt="SOGYb46IC-E"/>
                        <span itemprop="name" >Facial Beard & Moustache Transplant Results Patient from Punjab </span>
                        <meta itemprop="description" content="Facial Hair transplant Results of India's rising Hair Transplant Centre AK Clinics' Patients. You can watch Bread and Moustache hair transplant Results of our patients from India in this video. Explore our website now at https://akclinics.org/">
                        <meta itemprop="uploadDate" content="2014-03-03T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/facial_beard_moustache_trnsplnt.png" />
                        <meta itemprop="duration" content="PT00M59S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=NRoeogmViDU' data-src="https://img.youtube.com/vi/NRoeogmViDU/0.jpg" alt="NRoeogmViDU"/>
                        <span itemprop="name" >FUE Hair Transplant, Happy Patients Results: 2200, 2500, 1200 Grafts</span>
                        <meta itemprop="description" content="Follicular Unit Extraction (FUE) Hair transplant technique used by India's leading Hair Transplant Centre AK Clinics to their Patients. In video, you can see hair transplantation Results before and after 12 months of our patients from around the world. Visit our website now at https://akclinics.org">
                        <meta itemprop="uploadDate" content="2014-02-28T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/FUE_hair_trnsplnt_happy_patient.png" />
                        <meta itemprop="duration" content="PT01M09S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=qn32zDjeGg0' data-src="https://img.youtube.com/vi/qn32zDjeGg0/0.jpg" alt="qn32zDjeGg0"/>
                        <span itemprop="name" >Afghanistan Patient 3100 Graft BIO-FUE Hair Transplant Result</span>
                        <meta itemprop="description" content="Abdul arrived all the way from Afghanistan and with just 3100 grafts, our experienced team of doctors were able to ensure a full head of hair. Our in house method of BIO FUE ensured that Abdul enjoyed healthy hair for the rest of his life.">
                        <meta itemprop="uploadDate" content="2012-11-21T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/hair_trnsplnt_rslt_aftr_7_months.png" />
                        <meta itemprop="duration" content="PT00M23S" />
                    </div>

                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=cQnN1OToZ4w' data-src="https://img.youtube.com/vi/cQnN1OToZ4w/0.jpg" alt="cQnN1OToZ4w"/>
                        <span itemprop="name" >1300 Grafts Bio FUE Hair Transplant Result After 1 Year</span>
                        <meta itemprop="description" content="A 25 yrs old male presented with the complaint of hair loss due to turban for the past 3 years with no active hair fall. He was diagnosed with tractional alopecia due to turban. He has gone to 1300 Grafts Bio FUE Hair Transplant Surgery and was happy to see result after 1 Year">
                        <meta itemprop="uploadDate" content="2014-05-05T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/hair_trnsplnt_rslt_sftr_6_months.png" />
                        <meta itemprop="duration" content="PT00M24S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=vHtvCtVqz' data-src="https://img.youtube.com/vi/vHtvCtVqz-s/0.jpg" alt="vHtvCtVqz-s"/>
                        <span itemprop="name" >Hair Transplant Result After 9 Months – 2200 Grafts </span>
                        <meta itemprop="description" content="To see video of patient result After 9 Months for 2200 Grafts">
                        <meta itemprop="uploadDate" content="2014-12-18T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/hair_trnsplnt_rslt_aftr_9_months.png" />
                        <meta itemprop="duration" content="PT00M50S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=qMJwarXl91Q' data-src="https://img.youtube.com/vi/qMJwarXl91Q/0.jpg" alt="qMJwarXl91Q"/>
                        <span itemprop="name" >2000 Grafts FUE Hair Transplant Result after 8 Month</span>
                        <meta itemprop="description" content="A 31 yrs old male patient from Ludhiana, presented with the complaint of hair loss for the past 5 years with no active hair fall for 1 year have gone 2000 Grafts FUE Hair Transplant. He was happy to result Result after 8 Month.">
                        <meta itemprop="uploadDate" content="2014-12-18T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/hair_trnsplnt_rslt_aftr_9_months.png" />
                        <meta itemprop="duration" content="PT00M50S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=Smmi0sAkang' data-src="https://img.youtube.com/vi/Smmi0sAkang/0.jpg" alt="Smmi0sAkang"/>
                        <span itemprop="name" >USA Patient 2647 Graft Hair Transplant Result with BIO-FUE Technique</span>
                        <meta itemprop="description" content="When Yash arrived at our clinics, all the way from USA, he had little hope of having any hair, but with just 2647 grafts and our revolutionary BIO FUE, he is now back to having a head full of hair.">
                        <meta itemprop="uploadDate" content="2014-12-18T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/hair_trnsplnt_rslt_aftr_9_months.png" />
                        <meta itemprop="duration" content="PT00M50S" />
                    </div>
                    
                    <div class="item embed-responsive embed-responsive-16by9" itemscope itemtype="http://schema.org/VideoObject">
                        <img itemprop="embedUrl" class="embed-responsive-item changeMovie lazy" src='https://www.youtube.com/watch?v=-q1SwiiG3zA' data-src="https://img.youtube.com/vi/-q1SwiiG3zA/0.jpg" alt="-q1SwiiG3zA"/>
                        <span itemprop="name" >2530 Grafts, FUE Hair Transplant Surgery result after 3 years</span>
                        <meta itemprop="description" content="Surgery result after 3 years	39 yrs old male presented with the complaint of hair loss for the past 15 years with no active hair fall for 1 year. He was suggested to 2530 Grafts, FUE Hair Transplant Surgery and was happy to result.">
                        <meta itemprop="uploadDate" content="2014-12-18T08:00:00+08:00"/>
                        <meta itemprop="thumbnailUrl" content="<?= cdn('assets/template/uploads/'); ?>2016/01/hair_trnsplnt_rslt_aftr_9_months.png" />
                        <meta itemprop="duration" content="PT00M50S" />
                    </div>
                </div>
                
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>results/videos/">Videos</a></li>
                        <li class=""><a href="<?= base_url(); ?>results/">Results</a></li>
                        <li><a href="<?= base_url(); ?>results/cosmetic-surgery-results/">Cosmetic Surgery Results</a></li>
                        <li><a href="<?= base_url(); ?>results/prp-therapy/">PRP Therapy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(".changeMovie").click( function ()
{
	var val = jQuery(this).attr("alt");
	if(val != "")
	{
		var str = '<iframe class="embed-responsive-item videoShow" src="https://www.youtube.com/embed/'+val+'?autoplay=1&rel=0"></iframe>';
		jQuery(".videoShow").html(str);
	}
});
</script>

