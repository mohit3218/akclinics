<link rel="stylesheet" href="<?php echo base_url('assets/template/frontend/'); ?>css/result-video.css">
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>results/"> <span itemprop="name">Results</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>results/prp-therapy/"> <span itemprop="name">PRP Therapy</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container-fluid">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left"> <h1 class="black-text">PRP Hair Gain Therapy Before and After Results – AK Clinics</h1></div>
                <div class="tab-content" id="pills-tabContent">
                    <div class=" fade show active" id="PRPtherapy" role="tabpanel" aria-labelledby="PRP-therapy">

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-1.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-1.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-2.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-2.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-3.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-3.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-5.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-5.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-6.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-6.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-7.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-7.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img lazy" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-8.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-8.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img lazy" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-9.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-9.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img lazy" alt="PRP Hair Gain Therapy" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-10.png" alt="">
                            <meta itemprop="name" content="PRP Hair Gain Therapy">
                            <div class="box-content">
                                <h4 class="title">PRP Hair Gain Therapy</h4>
                                <a class="read-more" href="javascript:void(0);" title="PRP Hair Gain Therapy" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/prp/prp-result-10.png">read more</a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>results/">Results</a></li>
                        <li><a href="<?= base_url(); ?>results/videos/">Videos</a></li>
                        <li><a href="<?= base_url(); ?>results/cosmetic-surgery-results/">Cosmetic Surgery Results</a></li>
                        <li><a href="<?= base_url(); ?>results/prp-therapy/">PRP Therapy</a></li>    
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="modalIMG" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <button type="button" class="close popup-class pull-right" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <div class="modal-content">
            <div class="modal-body mb-0 p-0">
                <img class="result-img-view-modal" src="" alt="" style="width:100%">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".read-more").click(function () {
            var postImage = $(this).data('id');
            $(".result-img-view-modal").attr("src", postImage);
            $('#modalIMG').modal('show');
        });
    });
</script>