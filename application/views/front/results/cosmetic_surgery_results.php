<link rel="stylesheet" href="<?php echo base_url('assets/template/frontend/'); ?>css/result-video.css">
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>results/"> <span itemprop="name">Results</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>results/cosmetic-surgery-results/"> <span itemprop="name">Cosmetic Surgery Results</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
                <!-- Breadcrumbs /--> 
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                <div class="section-title-1 text-left"> <h1 class="black-text">Cosmetic Surgeries Before and After Results – AK Clinics</h1></div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="Cosmeticsurgery" role="tabpanel" aria-labelledby="Cosmetic-surgery">

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img" alt="Rhinoplasty  The Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-1.png" alt="">
                            <meta itemprop="name" content="Rhinoplasty The Surgery">
                            <div class="box-content">
                                <h4 class="title">Rhinoplasty  The Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="BRhinoplasty – The Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-1.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img" alt="Gynecomastia" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-2.png" alt="">
                            <meta itemprop="name" content="Gynecomastia">
                            <div class="box-content">
                                <h4 class="title">Gynecomastia</h4>
                                <a class="read-more" href="javascript:void(0);" title="Gynecomastia" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-2.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img" alt="Liposuction" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-3.png" alt="">
                            <meta itemprop="name" content="Liposuction">
                            <div class="box-content">
                                <h4 class="title">Liposuction</h4>
                                <a class="read-more" href="javascript:void(0);" title="Liposuction" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-3.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img" alt="Breast Reconstruction Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-4.png" alt="">
                            <meta itemprop="name" content="Breast Reconstruction Surgery">
                            <div class="box-content">
                                <h4 class="title">Breast Reconstruction Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Breast Reconstruction Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-4.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img" alt="Facial Cismetic Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-5.png" alt="">
                            <meta itemprop="name" content="Facial Cismetic Surgery">
                            <div class="box-content">
                                <h4 class="title">Facial Cismetic Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Facial Cismetic Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-5.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img" alt="Breast Reconstruction Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-6.png" alt="">
                            <meta itemprop="name" content="Breast Reconstruction Surgery">
                            <div class="box-content">
                                <h4 class="title">Breast Reconstruction Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Breast Reconstruction Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-6.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img lazy" alt="Liposution Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-8.png" alt="">
                            <meta itemprop="name" content="Liposution Surgery">
                            <div class="box-content">
                                <h4 class="title">Liposution Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Liposution Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-8.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img lazy" alt="Liposution Surgery" itemprop="contentUrl"src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-9.png" alt="">
                            <meta itemprop="name" content="Liposution Surgery">
                            <div class="box-content">
                                <h4 class="title">Liposution Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Liposution Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-9.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img lazy" alt="Blepharoplasty Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-10.png" alt="">
                            <meta itemprop="name" content="Blepharoplasty Surgery">
                            <div class="box-content">
                                <h4 class="title">Blepharoplasty Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Blepharoplasty Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-10.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img lazy" alt="Breast Reconstruction Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-11.png" alt="">
                            <meta itemprop="name" content="Hair Transplant Results of 2200 Grafts (Grade 5)">
                            <div class="box-content">
                                <h4 class="title">Hair Transplant Results of 2200 Grafts (Grade 5)</h4>
                                <a class="read-more" href="javascript:void(0);" title="Breast Reconstruction Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-11.png">read more</a> 
                            </div>
                        </div>

                        <div class="box21 Portfolio" itemscope itemtype="http://schema.org/ImageObject">
                            <img class="card-img lazy" alt="Blepharoplasty Surgery" itemprop="contentUrl" src="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-12.png" alt="">
                            <meta itemprop="name" content="Blepharoplasty Surgery">
                            <div class="box-content">
                                <h4 class="title">Blepharoplasty Surgery</h4>
                                <a class="read-more" href="javascript:void(0);" title="Blepharoplasty Surgery" data-toggle="modal" data-id="<?php echo cdn('assets/template/frontend/'); ?>images/cos/cosmetic-surgery-result-12.png">read more</a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>results/">Results</a></li>
                        <li><a href="<?= base_url(); ?>results/videos/">Videos</a></li>
                        <li><a href="<?= base_url(); ?>results/cosmetic-surgery-results/">Cosmetic Surgery Results</a></li>
                        <li><a href="<?= base_url(); ?>results/prp-therapy/">PRP Therapy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="modalIMG" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <button type="button" class="close popup-class pull-right" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <div class="modal-content">
            <div class="modal-body mb-0 p-0">
                <img class="result-img-view-modal" src="" alt="" style="width:100%">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".read-more").click(function () {
            var postImage = $(this).data('id');
            $(".result-img-view-modal").attr("src", postImage);
            $('#modalIMG').modal('show');
        });
    });
</script>