 <?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>anti-ageing-treatments-post-op-care-instructions/"> <span itemprop="name">Anti Ageing Treatments Post Op Care Instructions</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
                <!-- Breadcrumbs /--> 
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Anti Ageing Treatments</h1>
                </div>
                <p>Ageing is a natural process but everyone wants to age with poise. Everyone wants to delay their ageing signs. With the advancements in the cosmetology, it is now possible to do so. You can now reverse the signs of ageing with the advanced anti-ageing treatments. We at AK Clinics, provides the best anti-ageing treatments for skin lifting, wrinkles reduction, fine lines removal, crow feet reduction and many more. Among the vast procedures, you can choose your desired anti-ageing treatment according to your requirement or our dermatologist will recommend you the best anti-ageing treatment after diagnosing your skin condition during the consultation. The most common and widely used anti-ageing treatment includes Botox, Dermal Fillers and threads for skin lift, wrinkles, fine lines etc. </p>
                <h3 class="font-weight-bold text-center ptb-1">Post Op Care Instructions after the Laser Vaginal Rejuvenation Treatment</h3>
                <p>The vaginal tightening procedure is a sensitive treatment and special care has to be taken after the treatment. Follow proper instructions given by the doctor to avoid side effects.</p>
                <ul>
                    <li>You will be given antibiotic to prevent the post operative infection,  anti-inflammatory drugs or pain killer to reduce the pain.</li>
                    <li>Take rest for faster healing.</li>
                    <li>Keep the genital area dry. It will help in faster healing.</li>
                    <li>Clean the area properly after the urination.</li>
                    <li>Itching or white colored discharge due to vaginal fungal microbes. Your doctor will give you anti-fungal to prevent fungal infection.</li>
                    <li>Avoid smoking, alcohol, pickled food for 2-3 days before or even after the surgery.</li>
                    <li>Refrain yourself from the heavy activities for at least 4-5 days.</li>
                    <li>You can do relaxing exercises, light weight lifting after the 4 weeks of surgery.</li>
                    <li>You will be on pelvic rest so avoid the use of tampons or no intercourse for 7-8 days after the surgery.</li>
                    <li>In case of more bleeding, reddish blood clot, high fever, consult the doctor immediately. </li>
                    <li>Wear loose fitting pants/pajamas for 1-2 days. Avoid wearing undergarments.</li>
                </ul>
                <div class="row">
                    <h3 class="font-weight-bold text-center ptb-1">Post Op Care Instructions after Anti-Ageing Treatment with Botox</h3>
                    <div class="col-md-12">
                        <ul>
                            <li>Keep your head in slightly elevated position. Do not bend upwards or downwards for at least 4 hours to reduce the swelling on the face after the treatment.</li>
                            <li>There may be redness, bruising or pain at the injected site for 2-3 days.</li>
                            <li>You can use gentle skin cleanser like Cetaphil or the one prescribed by the physician to wash your face.</li>
                            <li>Avoid touching the treated area again and again.</li>
                            <li>Use a gentle cleanser and use an effective moisturizer to keep your skin moisturized. </li>
                            <li>Apply the antibiotic cream on the treated area as prescribed by the doctor.</li>
                            <li>Do not take any blood thinning drugs 1 week before and after the treatment.</li>
                            <li>Avoid drinking excessive alcohol, as it opens the blood vessels and can increase the swelling or bleeding.</li>
                            <li>Do not apply the excessive pressure on the injected site for first few hours after the treatment.</li>
                            <li>Avoid any strenuous exercise or workout to avoid sweating and also avoid extreme hot &amp; cold showers for at least 24 hours after the Botox treatment.</li>
                            <li>Do not go for sun beds, saunas and steam rooms for 2-3 weeks after the treatment.</li>
                            <li>Do not undergo any other cosmetology treatment like facial laser hair removal, chemical peels for 7days after the anti-ageing treatment.</li>
                            <li>Use the sunscreen liberally before 20 minutes going out and avoid direct exposure to the UV rays.</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <h3 class="font-weight-bold text-center ptb-1">Post Op Care Instructions after Anti-Ageing Treatment with Fillers</h3>
                    <div class="col-md-12">
                        <ul>
                            <li>Do not use any products on lips if fillers are injected in lips for 3-5 days.</li>
                            <li>Do not bite or prick your lips for 3-5 days after the surgery.</li>
                            <li>Do not squeeze or pull your cheeks.</li>
                            <li>Avoid massaging your face vigorously at least for 1 week after the treatment.</li>
                            <li>Drink plenty of water to keep your body hydrated.</li>
                            <li>Use the gentle cleanser for cleaning the face.</li>
                            <li>Apply sunscreen 20 minutes before going out in the sun.</li>
                            <li>Avoid any physical strenuous activities for at least 24 hours after the treatment.</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <h3 class="font-weight-bold text-center ptb-1">Post Op Care Instructions after Anti-Ageing Treatment with Threads</h3>
                    <div class="col-md-12">
                        <ul>
                            <li>Clean your face with soft hands and with gentle cleanser.</li>
                            <li>Avoid salon activities like threading if you have undergone thread lift procedure on eyebrows, bleaching etc after the treatment.</li>
                            <li>Do not undergo any chemical procedure like chemical peels, facial hair removal etc for at least 7 days after the treatment.</li>
                            <li>Refrain yourself from any strenuous exercises. Avoid going to gym for 3-4 days after the treatment.</li>
                            <li>Apply sunscreen liberally 20 minutes before going out in the sun.</li>
                            <li>Do not pull your cheeks after the thread lift procedure on cheeks.</li>
                            <li>Drink plenty of water and eat a balanced diet.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>acne-scar-treatment-post-op-instructions">Acne Scar Treatment</a></li>
                        <li><a href="<?= base_url(); ?>chemical-peels-post-op-instructions">Chemical Peels</a></li>
                        <li><a href="<?= base_url(); ?>co2-laser-treatment-post-op-instructions">Co2 Laser Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmedico-treatment-post-op-instructions">Cosmedico Treatment</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia-post-op-instructions">Gynecomastia</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy-post-op-care-instructions">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>laser-hair-removal-post-op-care-instructions">Laser Hair Removal</a></li>
                        <li><a href="<?= base_url(); ?>laser-vaginal-rejuvenation-post-op-instructions">Laser Vaginal Rejuvenation</a></li>
                        <li><a href="<?= base_url(); ?>non-surgical-ultrasonic-liposuction-post-op-care-instructions">Non Surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>pigmentation-treatment-post-op-care-instructions">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty-post-op-instructions">Rhinoplasty</a></li>
                        <li><a href="<?= base_url(); ?>scalp-micropigmentation-post-op-instructions">Scalp Micropigmentation</a></li>
                        <li><a href="<?= base_url(); ?>stretch-marks-treatment-post-op-care-instructions">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>ultherapy-treatment-post-op-care-instructions">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment-post-op-instructions">Vitiligo Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>