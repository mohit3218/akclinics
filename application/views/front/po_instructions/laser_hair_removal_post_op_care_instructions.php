 <?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>laser-hair-removal-post-op-care-instructions"> <span itemprop="name">Laser Hair Removal Post Op Care Instructions</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Laser Hair Removal</h1>
                </div>
                <p>Laser hair removal reduces the hair growth by 80 to 95 percent. Six to nine sessions are characteristically considered necessary to get to an appreciable reduction. In accordance with the area of interest, treatment sessions are executed every 4-8 weeks. Postoperative care is very significant. There will be the minimization of unnecessary pain, complications, infections and swelling if the post op instructions are followed carefully. However, when in uncertainty of thoughts regarding the do’s and dont’s, follow these post op instructions or guidelines for clarification. Just keep in mind that the return is at the end of the curative journey.</p>
                <h3 class="font-weight-bold text-center ptb-1">Immediately Following Laser hair Reduction Surgery</h3>
                <ul>
                    <li>Take the medications as per the prescription by the time you start to feel uncomfortable.</li>
                    <li>Incase there is swelling or redness, talk with the doctor and he may ask you to use the cold packs.</li>
                </ul>
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="font-weight-bold text-center ptb-1">Post op Instructions for Laser Hair Reduction</h3>
                        <ul>
                            <li>Do not use any deodorant for at least 24 to 48 hours on the treated area.</li>
                            <li>After the LHR session abrasive scrubs or loofahs or any sort of rubbing or picking must be avoided for at least a week.</li>
                            <li>Carry on the application of sunscreens for the period of the treatments and if there is crust formation moist the area with restorative ointment.</li>
                            <li>Do not wax, tweeze, use depilatory cream or any electrolysis throughout the entire duration of time.</li>
                            <li>Avoid direct sun exposure to the treated area for at least 2-3 days after the laser hair removal session.</li>
                            <li>Application of post-treatment recovery ointment should be 2-4 times a day (for at least 3 days) after each session of the treatment to calm the skin directed by the physician. </li>
                            <li>Please stick to the suitable treatment intervals as recommended by the physician. This is  determined in accordance to the hair growth cycle, it’s effectiveness and numerals of necessary treatments. </li>
                            <li>Avoid irritants such as glycolics or retinoids etc for at least seven days after the LHR session.</li>
                            <li>People with dark complexion might have to face more uneasiness as compare to fairer complexion ones. Hydrocortisone based cream preparation could also be used or the one prescribed by physician to soothe the symptoms.</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="font-weight-bold text-center ptb-1">Shedding after Laser Hair Removal</h3>
                        <p>The process is called “shedding,” as one can remove the hair by gently rubbing or scrubbing the skin in the shower as such to remove the dead skin cells and release the hair.<br>
                            Your hair needs to drop out naturally, throughout the shedding stage so never  pluck, tweeze, or wax the area of interest. If any resistance from a hair is found, that implies that the root is still alive.<br>
                            This hair would further be targeted during the coming follow-up session. One can shave after <a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">laser hair removal</a>, but keep away from pulling hair out by the roots.Those hair were in their resting phase during the first session and would be treated in the next session. Always keep in mind that laser only influence hairs that are in their growing phase and not the ones which are in resting or dormant phase at the time of laser session.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>acne-scar-treatment-post-op-instructions/">Acne Scar Treatment</a></li>
                        <li><a href="<?= base_url(); ?>anti-ageing-treatments-post-op-care-instructions/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>chemical-peels-post-op-instructions/">Chemical Peels</a></li>
                        <li><a href="<?= base_url(); ?>co2-laser-treatment-post-op-instructions/">Co2 Laser Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmedico-treatment-post-op-instructions/">Cosmedico Treatment</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia-post-op-instructions/">Gynecomastia</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy-post-op-care-instructions/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>laser-vaginal-rejuvenation-post-op-instructions/">Laser Vaginal Rejuvenation</a></li>
                        <li><a href="<?= base_url(); ?>non-surgical-ultrasonic-liposuction-post-op-care-instructions/">Non Surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>pigmentation-treatment-post-op-care-instructions/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty-post-op-instructions/">Rhinoplasty</a></li>
                        <li><a href="<?= base_url(); ?>scalp-micropigmentation-post-op-instructions/">Scalp Micropigmentation</a></li>
                        <li><a href="<?= base_url(); ?>stretch-marks-treatment-post-op-care-instructions/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>ultherapy-treatment-post-op-care-instructions/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment-post-op-instructions/">Vitiligo Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>