<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>cosmedico-treatment-post-op-instructions/"> <span itemprop="name">Cosmedico Treatment Post Op Instructions</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Cosmedico Treatment for Skin Whitening</h1>
                </div>
                <p>Cosmedico treatment stimulates the formulation of new collagen fibers in the skin. This gives a visible smoothing effect on wrinkles or fine lines. In the procedure, light energy is converted into the heat energy. The energy produced during the process focuses deep into the dermal layers, which contains the tear proof collagen fibers and elastic fibers. The cosmedico treatment for skin rejuvenation and whitening stimulates the dermal cells to enhance the production of the cell matrix building components i.e. collagen and elastin resulting in healthy, young and fresh looking skin. </p>
                <div class="row pt-1">
                    
                    <div class="col-md-6"><strong>Electroporation Method for Skin Rejuvenation</strong>
                        <p>Electroporation method allows the products like serums, lotions or creams to absorb through the electrical pulses which split open skin cells. It alleviates the dry skin without disrupting the normal pH level of the skin. Electric pulses in  electroporation create  temporary pores in the cell membrane of the skin which allows the skin care products to percolate through  deep layers of the skin. It results in skin brightness, hydration and skin rejuvenation after  electroporation treatment.   </p>
                    </div>
                    <div class="col-md-6">
                        <img data-src="<?= cdn('assets/template/uploads/'); ?>2017/12/Electroporation-Method.jpg" alt="" class="img-fluid lazy">
                    </div>
                </div>

                <div class="row pt-1">
                    

                    <div class="col-md-6"><strong>Diamond Microdermabrasion for Skin Whitening</strong>
                        <img data-src="<?= cdn('assets/template/uploads/'); ?>2017/12/Diamond-Microdermabrasion.jpg" alt="" class="img-fluid lazy">
                    </div>
                    <div class="col-md-6">
                        <p>Diamond peel microdermabrasion is a safe, non-invasive skin resurfacing treatment which helps to deeply polish and stimulates the skin using the compactly packed laser cut diamonds to remove dead skin cells and promote the healthy, new cells for a smooth, radiant and flawless complexion. In diamond microdermabrasion, diamond tip gently exfoliates the upper layer of the skin to polish the skin surface and removes all the impurities from the skin leaving it crystal clear.  </p>
                    </div>
                </div>
                <div class="row pt-1">
                    <strong>Post Op Care Instructions of Skin Rejuvenation and Skin Whitening Treatment</strong>
                    <div class="col-md-12">
                        <ul>
                            <li>Do not use cosmetics or irritating creams/toner or scrubs that cause abrasions on the skin and can cause allergic reactions.</li>
                            <li>The treated area may appear red for 2-3 hours after  treatment. Your skin may also appear like a sunburn that can last for 2-3days.</li>
                            <li>Scabbing or peeling of the skin may occur which is common. Keep your skin moisturized properly with the intensive rich moisturizer.</li>
                            <li>Do not scratch or rub your face vigorously. </li>
                            <li>Avoid aspirin, or other blood thinners 2-3 days before and 2-3 days after the treatment.</li>
                            <li>Do not drink alcohol or smoke for 10 days after  treatment and even 2 weeks before the treatment.</li>
                            <li>Avoid facial wax, bleach or shaving for 1-2 weeks after the treatment.</li>
                            <li>Do not undergo any other cosmetology treatment like <a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/botulinum-toxin-botox/">Botox</a>, <a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/fillers/">Fillers</a>, <a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser hair removal</a> for 10-14 days after the treatment.</li>
                            <li>You can use cold compress or ointment to avoid the burning sensation or itching after the treatment.</li>
                            <li>Refrain yourself from heavy workouts or tasks that cause sweating for 1-2 days.</li>
                            <li>Drink plenty of water to rehydrate your skin.</li>
                            <li>Gently cleanse your skin and use rich moisturizer or the one prescribed by <a href="<?= base_url(); ?>about-us/our-team/dr-aman-dua/">dermatologist</a> properly to avoid the crust formation.</li>
                            <li>Do not apply any topical if you are using for 7-8 days after the treatment or you can use them after consulting the physician.</li>
                            <li>Do not use any skin peeling agents like glycolic acid for few days as your skin is sensitive and can cause excessive damage.</li>
                            <li>Avoid the direct exposure to the UV rays and apply sunscreen liberally.</li>
                            <li>Do not use any tan beds or self tanning lotions until your skin has healed properly.</li>
                            <li>Do not exfoliate your skin until the redness has subsided.</li>
                            <li>If you experience severe pain or itching on the face, consult with the physician. </li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>acne-scar-treatment-post-op-instructions/">Acne Scar Treatment</a></li>
                        <li><a href="<?= base_url(); ?>anti-ageing-treatments-post-op-care-instructions/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>chemical-peels-post-op-instructions/">Chemical Peels</a></li>
                        <li><a href="<?= base_url(); ?>co2-laser-treatment-post-op-instructions/">Co2 Laser Treatment</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia-post-op-instructions/">Gynecomastia</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy-post-op-care-instructions/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>laser-hair-removal-post-op-care-instructions/">Laser Hair Removal</a></li>
                        <li><a href="<?= base_url(); ?>laser-vaginal-rejuvenation-post-op-instructions/">Laser Vaginal Rejuvenation</a></li>
                        <li><a href="<?= base_url(); ?>non-surgical-ultrasonic-liposuction-post-op-care-instructions/">Non Surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>pigmentation-treatment-post-op-care-instructions/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty-post-op-instructions/">Rhinoplasty</a></li>
                        <li><a href="<?= base_url(); ?>scalp-micropigmentation-post-op-instructions/">Scalp Micropigmentation</a></li>
                        <li><a href="<?= base_url(); ?>stretch-marks-treatment-post-op-care-instructions/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>ultherapy-treatment-post-op-care-instructions/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment-post-op-instructions/">Vitiligo Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>