<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>rhinoplasty-post-op-instructions/"> <span itemprop="name">Rhinoplasty Post Op Instructions</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Rhinoplasty Surgery</h1>
                </div>
                <p>Rhinoplasty is also known as the nose job. It is a surgical procedure used to correct and reconstruct the nose form, to restore the functions and to aesthetically enhance the nose by resolving the nasal trauma, to correct the congenital defect and respiratory impediment. We are hereby presenting a general overview of the post op instructions which will be followed and the actual instructions may be bit varied depending upon the extent of the surgery. </p>

                <div class="row">
                    <strong class="clr">Post Op Care Instructions after Rhinoplasty Surgery</strong><p></p>
                    <div class="col-md-6">
                        <ul>
                            <li>There will be swelling around the eyes or on face after the surgery. It will resolve itself by the end of 2nd- 3rd week of the surgery.  However, the outer skin of the nose may remain swollen for little long time.</li>
                            <li>There will be a small amount of loose packing which is placed into the nostrils. It will be removed 24-48 hours post surgery depending upon the extent of the surgery.</li>
                            <li>There will be a nasal discharge of small drops of pink colored water for few days. Gently wipe the area with a soft tissue.</li>
                            <li>Avoid taking any blood thinning agents 2 weeks before and even 2 weeks after the surgery. Do not consume alcohol too. As these agents can cause bleeding and also promotes the swelling.</li>
                            <li>Do not bend your head below the heart level. Try to keep it in normal or upward direction.</li>
                            <li> Try to sneeze with your open mouth.</li>
                            <li>Keep on drinking plenty of fluids as you feel dryness of throat. </li>
                            <li>You may experience pain in the treated area and mild headache too. The doctor will prescribe the painkillers to get rid of pain after the rhinoplasty surgery.</li>
                            <li>Do not drive if you are on narcotics or taking any sedative medications.</li>
                        </ul></div>
                    <div class="col-md-6">
                        <li>Bruising or skin discoloration will also occur but will disappear completely after 2-3 weeks.</li>
                        <li>A small silicone splint is placed at the top of the upper two-thirds of the nose after the <a href="<?= base_url(); ?>rhinoplasty/">rhinoplasty surgery</a> in certain cases or plaster may be. It acts as a protector for the freshly operated nose helps to keep the position intact of the recently sculpted nasal bones. It may be removed after 7- 10 days.</li>
                        <li>Keep your head slightly elevated by the keeping 2 pillows under the head to reduce the swelling.</li>
                        <li>Do not wear any pullovers to avoid pressure on nasal bones.</li>
                        <li>You can take shower after the 2nd day of the surgery but with the tilted head.</li>
                        <li>You may feel numbness in the nose tip and upper lip. This will return slowly to the normal state over a period of time.</li>
                        <li>Prefer liquid diet and avoid the hard chewable foods or nuts.</li>
                        <li>You will be given antibiotics also to avoid infection.</li>
                        <li>Do not smoke for at least 7-10 days after the surgery as it can cause irritation in the nose lining, coughing and bleeding.
                            <ol>
                                <li>Call the doctor if you have; </li>
                                <li>Vigorous nose bleeding</li>
                                <li>High Fever</li>
                                <li>Purulent discharge (pus)</li>
                                <li>Severe nasal pain or a headache </li>
                            </ol>
                        </li>
                    </div>
                    
                </div>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>acne-scar-treatment-post-op-instructions/">Acne Scar Treatment</a></li>
                        <li><a href="<?= base_url(); ?>anti-ageing-treatments-post-op-care-instructions/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>chemical-peels-post-op-instructions/">Chemical Peels</a></li>
                        <li><a href="<?= base_url(); ?>co2-laser-treatment-post-op-instructions/">Co2 Laser Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmedico-treatment-post-op-instructions/">Cosmedico Treatment</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia-post-op-instructions/">Gynecomastia</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy-post-op-care-instructions/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>laser-hair-removal-post-op-care-instructions/">Laser Hair Removal</a></li>
                        <li><a href="<?= base_url(); ?>laser-vaginal-rejuvenation-post-op-instructions/">Laser Vaginal Rejuvenation</a></li>
                        <li><a href="<?= base_url(); ?>non-surgical-ultrasonic-liposuction-post-op-care-instructions/">Non Surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>pigmentation-treatment-post-op-care-instructions/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>scalp-micropigmentation-post-op-instructions/">Scalp Micropigmentation</a></li>
                        <li><a href="<?= base_url(); ?>stretch-marks-treatment-post-op-care-instructions/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>ultherapy-treatment-post-op-care-instructions/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment-post-op-instructions/">Vitiligo Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>