 <?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>stretch-marks-treatment-post-op-care-instructions/"> <span itemprop="name">Stretch Marks Treatment Post Op Care Instructions</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Stretch Marks Treatment</h1>
                </div>
                <p>Stretch Marks in medical term in known as Striae Distensae. Stretch marks can appear after the excess weight gain or loss, during or post pregnancy or puberty. These marks do not indicate any serious health problem as it happens when after excessive weight loss, body is not able to bounce back properly. These scars are just under the top layer of the skin and initially appear red or pink in color then after sometime white or silver. Stretch mark treatment may include Non-ablative fractional laser treatment including CO2 + PRP + DR (Derma Roller). It penetrates deep into the dermal layers and denatures the proteins as collagen.</p>

                <h3 class="font-weight-bold text-center ptb-1">Post Op Care Instructions after Stretch Marks Treatment</h3>
                <ul>
                    <li>Your skin may look like sun burn (red) or can even feel warmer after the treatment. Do not rub your skin vigorously.</li>
                    <li>Your skin may look or feel tighter as it was before. This is because of the cell renewing and rejuvenation.</li>
                    <li>Use sunscreen liberally while going out in the sun and avoid direct exposure to the sun.</li>
                    <li>Re-apply sunscreen after every 2 hours on the areas like armpits, thighs or knees, if you are longer in contact with the UV rays.</li>
                    <li>Avoid saunas or steam/hot tubs for 1-2 weeks.</li>
                    <li>Refrain from hot scorching showers.</li>
                    <li>Avoid the use of aggressive skin care products or strong deodorants on the area like armpits for few days after stretch marks treatment.</li>
                    <li>Patients, who have oily skin, may have the chances of acne breakouts. Do not pop the acne.</li>
                    <li>Once the redness has resolved, skin may appear dry. Use the intensive rich moisturizer on the dry skin.</li>
                    <li>If you are using any topical medications then after using it, wash the hands properly to avoid the contact with the eyes.</li>
                    <li>In case of <a href="<?= base_url(); ?>cosmetology/chemical-peels/">chemical peels</a> for stretch marks, there may be scabbing or shedding of the dead skin cells after the treatment. Do not pick or scratch them.</li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>acne-scar-treatment-post-op-instructions/">Acne Scar Treatment</a></li>
                        <li><a href="<?= base_url(); ?>anti-ageing-treatments-post-op-care-instructions/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>chemical-peels-post-op-instructions/">Chemical Peels</a></li>
                        <li><a href="<?= base_url(); ?>co2-laser-treatment-post-op-instructions/">Co2 Laser Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmedico-treatment-post-op-instructions/">Cosmedico Treatment</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia-post-op-instructions/">Gynecomastia</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy-post-op-care-instructions/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>laser-hair-removal-post-op-care-instructions/">Laser Hair Removal</a></li>
                        <li><a href="<?= base_url(); ?>laser-vaginal-rejuvenation-post-op-instructions/">Laser Vaginal Rejuvenation</a></li>
                        <li><a href="<?= base_url(); ?>non-surgical-ultrasonic-liposuction-post-op-care-instructions/">Non Surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>pigmentation-treatment-post-op-care-instructions/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty-post-op-instructions/">Rhinoplasty</a></li>
                        <li><a href="<?= base_url(); ?>scalp-micropigmentation-post-op-instructions/">Scalp Micropigmentation</a></li>
                        <li><a href="<?= base_url(); ?>ultherapy-treatment-post-op-care-instructions/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment-post-op-instructions/">Vitiligo Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>