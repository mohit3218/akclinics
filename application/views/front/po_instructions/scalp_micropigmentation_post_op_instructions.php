<?php $this->load->view('front/partials/hair_loss_restoration_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>scalp-micropigmentation-post-op-instructions/"> <span itemprop="name">Scalp Micropigmentation Post Op Instructions</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Scalp Micropigmentation</h1>
                </div>
                <p>Scalp Micropigmentation (SMP) is also known as trichopigmentation. SMP is a highly sophisticated technique of a cosmetic pigmentation. In scalp micropigmentation, a series of tiny dots are created with the natural or non-toxic ink onto the bald area of the scalp or even in between the hair. Due to the pigmenting of the scalp, you can have the appearance of the fuller, thicker and look of the closely shaved head. If these pigments fade away or you want a darker shade, you can also go for a touch-up process. </p>
                <h3 class="font-weight-bold text-center ptb-1">Post Op Care Instructions after Scalp Micropigmentation</h3>
                <p>Scalp micropigmentation involves a very negligible discomfort or minimal downtime after the treatment. However, you have to be careful for the first 3-4 days and there are few things which you have to take care after your scalp micropigmentation treatment. If you will follow these post op instruction of scalp micropigmentation properly, you will get the best and long lasting results.</p>
                <ul>
                    <li>After the treatment, avoid the direct sun exposure.</li>
                    <li>Do not let immediately fall the excessive sunlight on your micro pigments to avoid fading of these pigments.</li>
                    <li>Do not touch the treated area.</li>
                    <li>Use a clean cotton swab to clean the area for 1-2 days after the treatment.</li>
                    <li>Do not rub forcefully the scalp.</li>
                    <li>Do not wash your scalp with regular shampoo for 6-7 days  after the scalp micropigmentation treatment.</li>
                    <li>Use a mild or gentle baby shampoo to wash the scalp after 3 days.</li>
                    <li>Avoid swimming in chlorinated water for 1 week.</li>
                    <li>Do not apply soap or harsh chemicals on the scalp for few days.</li>
                    <li>Refrain yourself from a heavy workout or from tasks that cause sweating at least for one week.</li>
                    <li>Do not apply any solutions or lotions like <a href="<?= base_url(); ?>blog/minoxidil-uses-dosage-precautions-and-side-effects/">minoxidil</a> to the scalp for at least one week after your treatment. </li>
                    <li>Avoid prolonged use of helmets.</li>
                    <li>There will be scab formation as the scalp micropigmentation is done with the help of needles. Do not pick them.</li>
                    <li>Do not scratch the scabs as scratching with nails can cause infection at the site or treated area. Allow these scabs to fall off naturally.</li>
                    <li>Moisturize your scalp properly after the treatment as this will benefit your scalp micro pigments.</li>
                    <li>If itching or bumps occur on the scalp, consult with the physician.</li>
                    <li>You may experience pain after the treatment. However, it will not last long. But in case of severe pain, ask the physician for pain killers.</li>
                    <li>As SMP only gives the look of trimmed hair, if you want to use a hair piece after the scalp micropigmentation treatment, the solution of lotion which is used for the hair piece or extensions may interact with the ink and can change the color of the pigmented dots on scalp. It may cause infection too. Only use hair piece after SMP by consulting the doctor.</li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>acne-scar-treatment-post-op-instructions/">Acne Scar Treatment</a></li>
                        <li><a href="<?= base_url(); ?>anti-ageing-treatments-post-op-care-instructions/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>chemical-peels-post-op-instructions/">Chemical Peels</a></li>
                        <li><a href="<?= base_url(); ?>co2-laser-treatment-post-op-instructions/">Co2 Laser Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmedico-treatment-post-op-instructions/">Cosmedico Treatment</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia-post-op-instructions/">Gynecomastia</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy-post-op-care-instructions/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>laser-hair-removal-post-op-care-instructions/">Laser Hair Removal</a></li>
                        <li><a href="<?= base_url(); ?>laser-vaginal-rejuvenation-post-op-instructions/">Laser Vaginal Rejuvenation</a></li>
                        <li><a href="<?= base_url(); ?>non-surgical-ultrasonic-liposuction-post-op-care-instructions/">Non Surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>pigmentation-treatment-post-op-care-instructions/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty-post-op-instructions/">Rhinoplasty</a></li>
                        <li><a href="<?= base_url(); ?>stretch-marks-treatment-post-op-care-instructions/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>ultherapy-treatment-post-op-care-instructions/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment-post-op-instructions/">Vitiligo Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>