<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>privacy-policy/"> <span itemprop="name">Privacy Policy</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-12">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Our Privacy Statement</h1>
                </div>
                <p> 
                    Every individual’s privacy is important to AK Clinics Pvt Ltd, most popularly identified as 
                    AK Clinics – Hair Restoration & Aesthetic Dermatology. The below mentions the policy we follow 
                    for the Privacy of akclinics.com & akclinics.org or any other website of ours.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>FAQs</h2>
            </div>
            <div id="accordion" class="mt-4">
                <div class="card">
                    <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">What all we collect?</a> </div>
                    <div id="collapseOne" class="collapse show" data-parent="#accordion">
                        <div class="card-body">We offer many facilities to the patients which include online counseling at the comfort of their home, answers to various problems they face about hair or any other cosmetic procedures information our office offers to provide.
                            <ul>
                                <li>Name</li>
                                <li>Address</li>
                                <li>Email Address</li>
                                <li>Phone Number</li>
                                <li>Gender</li>
                                <li>City</li>
                                <li>Country</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Data Security</a> </div>
                    <div id="collapseTwo" class="collapse" data-parent="#accordion">
                        <div class="card-body">We deploy all possible standard operating procedures both technical & administrative to keep the data secured. All our employees are bound by this code of ethics. In case you find any of our employees working otherwise, you can write to info@akclinics.com.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Websites not owned by us</a> </div>
                    <div id="collapseThree" class="collapse" data-parent="#accordion">
                        <div class="card-body">Our websites link to man other websites that are outside our control to give better information to patients. We are not responsible for their data management and privacy policy. If you have any concerns regarding this, you can email us at info@akclinics.com.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">What all can you copy</a> </div>
                    <div id="collapsefour" class="collapse " data-parent="#accordion">
                        <div class="card-body">You’re permitted to view, print our download parts from our websites strictly for your personal & non-commercial use. You’re not allowed for any reason to use any part in full or partial to be reproduced on any other website.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">What are our IP Rights?</a> </div>
                    <div id="collapsefive" class="collapse" data-parent="#accordion">
                        <div class="card-body">Any and all material, content, data, information contained within this website is the property of AK CLINICS. This same can’t be reproduced or copied for any reason whatsoever for commercial or non-commercial use.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsesix">What is the law applicable?</a> </div>
                    <div id="collapsesix" class="collapse" data-parent="#accordion">
                        <div class="card-body">The company AK Clinics Pvt Ltd is registered at Ludhiana, Punjab, India. For any complaints, arbitration for the website or any service, the courts of Ludhiana will have exclusive jurisdiction in all manners.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseseven">Important Declaration</a> </div>
                    <div id="collapseseven" class="collapse " data-parent="#accordion">
                        <div class="card-body">Entering this site establishes your “digital signature” and acknowledgement that you are at least 18 years of age and agree to all of the terms and conditions.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseeight">Get in touch with Us</a> </div>
                    <div id="collapseeight" class="collapse" data-parent="#accordion">
                        <div class="card-body">Should you have any concerns, you can contact us by writing to info@akclinics.com</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>