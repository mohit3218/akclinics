<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>laser-vaginal-rejuvenation-post-op-instructions/"> <span itemprop="name">Laser Vaginal Rejuvenation Post Op Instructions</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Laser Vaginal Rejuvenation After Care Instructions</h1>
                </div>
                <p>Laser vaginal rejuvenation is also known as vaginal tightening or vaginoplasty. LVR or laser vaginal rejuvenation is a new cosmetic medical procedure which is used to tighten the vaginal muscles, to restore its tone & strength. Over the time, vagina and its surrounding tissues become stretched which causes relaxation or loosening of the vagina. In the laser vaginal tightening procedure, tiny columns of the laser energy are used which stimulate the production of the collagen and even result in the regeneration of the vaginal tissues. </p>
                <h3 class="font-weight-bold text-center ptb-1">Post Op Care Instructions after the Laser Vaginal Rejuvenation Treatment</h3>
                <p>The vaginal tightening procedure is a sensitive treatment and special care has to be taken after the treatment. Follow proper instructions given by the doctor to avoid side effects.</p>
                <ul>
                    <li>You will be given antibiotic to prevent the post operative infection,  anti-inflammatory drugs or pain killer to reduce the pain.</li>
                    <li>Take rest for faster healing.</li>
                    <li>Keep the genital area dry. It will help in faster healing.</li>
                    <li>Clean the area properly after the urination.</li>
                    <li>Itching or white colored discharge due to vaginal fungal microbes. Your doctor will give you anti-fungal to prevent fungal infection.</li>
                    <li>Avoid smoking, alcohol, pickled food for 2-3 days before or even after the surgery.</li>
                    <li>Refrain yourself from the heavy activities for at least 4-5 days.</li>
                    <li>You can do relaxing exercises, light weight lifting after the 4 weeks of surgery.</li>
                    <li>You will be on pelvic rest so avoid the use of tampons or no intercourse for 7-8 days after the surgery.</li>
                    <li>In case of more bleeding, reddish blood clot, high fever, consult the doctor immediately. </li>
                    <li>Wear loose fitting pants/pajamas for 1-2 days. Avoid wearing undergarments.</li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>acne-scar-treatment-post-op-instructions/">Acne Scar Treatment</a></li>
                        <li><a href="<?= base_url(); ?>anti-ageing-treatments-post-op-care-instructions/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>chemical-peels-post-op-instructions/">Chemical Peels</a></li>
                        <li><a href="<?= base_url(); ?>co2-laser-treatment-post-op-instructions/">Co2 Laser Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmedico-treatment-post-op-instructions/">Cosmedico Treatment</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia-post-op-instructions/">Gynecomastia</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy-post-op-care-instructions/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>laser-hair-removal-post-op-care-instructions/">Laser Hair Removal</a></li>
                        <li><a href="<?= base_url(); ?>non-surgical-ultrasonic-liposuction-post-op-care-instructions/">Non Surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>pigmentation-treatment-post-op-care-instructions/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty-post-op-instructions/">Rhinoplasty</a></li>
                        <li><a href="<?= base_url(); ?>scalp-micropigmentation-post-op-instructions/">Scalp Micropigmentation</a></li>
                        <li><a href="<?= base_url(); ?>stretch-marks-treatment-post-op-care-instructions/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>ultherapy-treatment-post-op-care-instructions/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment-post-op-instructions/">Vitiligo Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>