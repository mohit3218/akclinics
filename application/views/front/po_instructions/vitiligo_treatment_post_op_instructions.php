<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>vitiligo-treatment-post-op-instructions/"> <span itemprop="name">Vitiligo Treatment Post Op Instructions</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Vitiligo Treatment</h1>
                </div>
                <p>Vitiligo Treatment is also known as Melanocyte transplantation. Vitiligo treatment or Melanocyte transplantation is the advanced and newest surgical procedure used to treat the stable vitiligo, leucoderma and loss of pigmentation. Melanocytes are the dermal cells which generate melanin pigment. In some disorders like vitiligo, the body starts producing less melanin pigment. Vitiligo can occur due to an auto immune disorder which can cause minor to prevalent changes in the skin and can be due to hereditary predisposition too. The pigment loss can occur due to other reasons also such as post burn skin lesions, chemical injury, Piebaldism, nevus depigmentosus or Discoid Lupus Erythematosus.</p>

                <h3 class="font-weight-bold text-center ptb-1">Post Op Care Instructions after Vitiligo Treatment</h3>
                <p>The whole procedure will take around 2-3 hours. Once you are discharged, there will be dressing which will remain for at least one week on the recipient area while for 3-4 days on the donor area. The physician will give you the directions to follow to make certain that the recovery is smooth. <br /><strong><em>Follow the simple post op care instructions after the melanocyte transplantation treatment;</em></strong></p>

                <ul>
                    <li>You can resume your normal activities immediately after the vitiligo procedure. </li>
                    <li>Redness, swelling and tenderness can occur in the treated area which will subside within few days.</li>
                    <li>The intensity of the pigment or color will be considerably darker immediately after the <a href="<?= base_url(); ?>vitiligo-treatment/">vitiligo treatment</a>. This will reduce with the time to deliver a more subtle or natural look.</li>
                    <li>As skin shave is done, it is important to keep the treated area clean properly with the antiseptic cream and clean with a sterile cotton swab.</li>
                    <li>Once the dressing has been removed, apply the anti-biotic cream prescribed by the physician for several days.</li>
                    <li>The general healing duration is approximately 5-10 days. After that, thin crusting may appear which will naturally flake away. </li>
                    <li>Do not pick or scratch the crusting to avoid <a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">hyper-pigmentation</a>.</li>
                    <li>There can be a hypo-pigmented spot or ring at the borders of the treated and re-pigmented patches in some patients. They will disappear automatically or you can use the local creams prescribed by the physician. </li>
                    <li>You can also undergo next sitting to remove these pigmented patches.</li>
                    <li>You will need to maintain the medical enhancement time to time in the form of ‘color boost’ re-touch for the optimum color. This is mostly recommended for 12-18 months following the initial procedure.</li>
                    <li>The treated area will be sensitive, so avoid the use of chemicals or harsh products on it.</li>
                    <li>You may ask for the painkiller to the physician in case of severe pain, irritation or burning sensation.</li>
                    <li>Follow the instructions regarding sun exposure strictly post procedure.</li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>acne-scar-treatment-post-op-instructions/">Acne Scar Treatment</a></li>
                        <li><a href="<?= base_url(); ?>anti-ageing-treatments-post-op-care-instructions/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>chemical-peels-post-op-instructions/">Chemical Peels</a></li>
                        <li><a href="<?= base_url(); ?>co2-laser-treatment-post-op-instructions/">Co2 Laser Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmedico-treatment-post-op-instructions/">Cosmedico Treatment</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia-post-op-instructions/">Gynecomastia</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy-post-op-care-instructions/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>laser-hair-removal-post-op-care-instructions/">Laser Hair Removal</a></li>
                        <li><a href="<?= base_url(); ?>laser-vaginal-rejuvenation-post-op-instructions/">Laser Vaginal Rejuvenation</a></li>
                        <li><a href="<?= base_url(); ?>non-surgical-ultrasonic-liposuction-post-op-care-instructions/">Non Surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>pigmentation-treatment-post-op-care-instructions/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty-post-op-instructions/">Rhinoplasty</a></li>
                        <li><a href="<?= base_url(); ?>scalp-micropigmentation-post-op-instructions/">Scalp Micropigmentation</a></li>
                        <li><a href="<?= base_url(); ?>stretch-marks-treatment-post-op-care-instructions/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>ultherapy-treatment-post-op-care-instructions/">Ultherapy Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>



