 <?php $this->load->view('front/partials/cosmetology_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>co2-laser-treatment-post-op-instructions/"> <span itemprop="name">Co2 Laser Treatment Post Op Instructions</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">CO2 Laser Treatment</h1>
                </div>
                <p>CO2 Laser treatment or fractional lasers surpass at treating the deep wrinkles, blotchiness of skin, acne scars, traumatic facial scars, warts or moles etc. Laser treatment works by removing the outer layer of the skin. In fractional CO2 laser treatment, CO2 laser beam fractionates or pixellates into the thousands of tiny little shafts of light. These shafts penetrate deep into the dermal layer of the skin. The skin repairs these shafts by pushing out the damaged skin cells and replaces them with the new skin cells. The CO2 laser also generates heat which stimulates the collagen production resulting in the regeneration of the new skin cells.</p>

                <h3 class="font-weight-bold text-center ptb-1">Post Op Instructions of Acne Scar Removal Treatment with CO2 Laser Treatment</h3>
                <p>It is important to ensure that you take care of your skin properly to avoid the side effects or complications. Follow the post op instructions of acne scar removal treatment carefully.</p>
                <ul>
                    <li>Immediately after the treatment, your skin may appear red and raw for few days and then later on will turn pink and normal.</li>
                    <li>There will be scab formation or crust formation after your <a href="<?= base_url(); ?>cosmetology/acne-treatment/">acne scar removal treatment</a>, try to keep your skin moisturized.</li>
                    <li>You can use gentle skin cleanser like Cetaphil or the one prescribed by the physician to wash your face.</li>
                    <li>Avoid contact with person who has history of cold sores or tell your doctor if you had a history of cold sores too.</li>
                    <li>Skin may appear flaky or scaly, do not peel your skin. Let it heal naturally. </li>
                    <li>Avoid the topical/creams if you are using any for few days or  until your skin has healed</li>
                    <li>Avoid hot saunas, steam bath or showers for 1-2 weeks of your treatment.</li>
                    <li>Use sunscreen liberally before going out in the sun and after every 2 hours.</li>
                    <li>Do not undergo any other facial treatment for 10-15 days after your treatment.</li>
                </ul>
                <div class="row">
                    <h3 class="font-weight-bold text-center ptb-1">Post Op Instructions of Laser Facial Scar Removal Treatment with CO2 Laser Treatment</h3>
                    <p>Facial scars can occur due to any injury, trauma, burns or surgery. In facial scar removal treatment, there is a stimulation of the collagen and elastin, The dermal cells which further stimulates the fibroblasts of scar to produce more collagen in the treated area or even after the treatment. Scar resumes the normal shape and arrangement just like the surrounding tissues of the face. Following are the post op instructions of the laser facial scar removal for the effective treatment.</p>
                    <div class="col-md-12">
                        <ul>
                            <li>You may feel itching or burning sensation for few hours after your treatment. You may apply ice cold compress or the topical prescribed by the physician to soothe the skin. </li>
                            <li>Scab formation or bronze coloration of the skin is normal after the facial scar removal treatment. It is important that you do not pick or scratch these scabs.</li>
                            <li>Facial discoloration or pigmentation may occur if you rub or scratch the treated area. This discoloration can be permanent. So avoid touching the treated area.</li>
                            <li>There may be redness for 2-3 days after the treatment as skin becomes sensitive. It will resolve by itself.</li>
                            <li>You may also experience acneiform eruptions on the treated area. Consult with the doctor. He/she may prescribe you the antibiotics or ointment to treat them. Do not pop them. It may lead to scarring.</li>
                            <li>Use  Sunscreen liberally on the treated area and re-apply after every 2 hours if you have to stay longer in the UV rays. </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <h3 class="font-weight-bold text-center ptb-1">Post Op Instructions of Wart or Mole Removal Treatment with CO2 Laser Treatment</h3>
                    <p>CO2 laser imparts a thermal injury to the denuded skin. After the mole or wart removal treatment, it does not take much time to ensure a fast recovery with minimal scarring. You just have to follow the simple steps and have to avoid few things to ensure that your skin will be as good as new in a matter of just couple of weeks. Following are the post op instructions after mole or wart removal surgery to avoid complications.</p>
                    <div class="col-md-12">
                        <ul>
                            <li>Apply the antibiotic ointment on the treated area until the wound has healed.</li>
                            <li>Clean the treated area as prescribed by the physician.</li>
                            <li>Moisturize the treated area properly to avoid dryness or itching.</li>
                            <li>Allow the scab to fall off naturally. Once the skin has healed, use the scar treatment cream prescribed by the physician.</li>
                            <li>Avoid the direct sun exposure on the treated area and use the broad spectrum sunscreen.</li>
                            <li>There may be mild serous or watery discharge after the treatment which will subside instinctively by 2nd – 3rd day after the treatment.</li>
                            <li>In case of severe redness, pain, draining pus or fever, consult with the doctor and get diagnosed. </li>
                        </ul>          </div>
                </div>
                <div class="row">
                    <h3 class="font-weight-bold text-center ptb-1">Post Op Instructions of Tattoo Removal Treatment with CO2 Laser Treatment</h3>
                    <p>Laser tattoo reduction treatment creates a superficial skin wound which will heal within few days. The proper aftercare is necessary to prevent infection, changes in skin texture or other unwanted side effects. To achieve the best possible results and to minimize the post treatment complications, follow the below given tattoo removal after care instructions.</p>
                    <div class="col-md-12">
                        <ul>
                            <li>Use a cold compress to reduce the discomfort or inflammation on the treated area for the next 24 hours.</li>
                            <li>Apply the antibiotic ointment on the area properly and change your dressing daily.</li>
                            <li>Avoid direct sun exposure, hot tubs, saunas or swimming in the chlorinated water for 1-2 weeks after the procedure.</li>
                            <li>There may be scabbing, blistering, light bleeding and itching. Do not pick the scabs to avoid scarring.</li>
                            <li>Immediately after the treatment, there may be erythema (redness) and edema (swelling) at the treatment site. This will lasts for 2 hours or longer. The redness can last up to 10 days.</li>
                            <li>Blisters can also be formed after the treatment. Do not pop them. Use the ointment prescribed by the physician. </li>
                            <li>Discoloration like white, pink or red may occur on the treated area. Use the topical/cream to balance melanin after consulting with the doctor.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>acne-scar-treatment-post-op-instructions/">Acne Scar Treatment</a></li>
                        <li><a href="<?= base_url(); ?>anti-ageing-treatments-post-op-care-instructions/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>chemical-peels-post-op-instructions/">Chemical Peels</a></li>
                        <li><a href="<?= base_url(); ?>cosmedico-treatment-post-op-instructions/">Cosmedico Treatment</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia-post-op-instructions/">Gynecomastia</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy-post-op-care-instructions/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>laser-hair-removal-post-op-care-instructions/">Laser Hair Removal</a></li>
                        <li><a href="<?= base_url(); ?>laser-vaginal-rejuvenation-post-op-instructions/">Laser Vaginal Rejuvenation</a></li>
                        <li><a href="<?= base_url(); ?>non-surgical-ultrasonic-liposuction-post-op-care-instructions/">Non Surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>pigmentation-treatment-post-op-care-instructions/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty-post-op-instructions/">Rhinoplasty</a></li>
                        <li><a href="<?= base_url(); ?>scalp-micropigmentation-post-op-instructions/">Scalp Micropigmentation</a></li>
                        <li><a href="<?= base_url(); ?>stretch-marks-treatment-post-op-care-instructions/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>ultherapy-treatment-post-op-care-instructions/">Ultherapy Treatment</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment-post-op-instructions/">Vitiligo Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>