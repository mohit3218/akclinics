<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>ultherapy-treatment-post-op-care-instructions/"> <span itemprop="name">Ultherapy Treatment Post Op Care Instructions</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Ultherapy Skin Tightening Treatment</h1>
                </div>
                <p>In Ultherapy treatment for skin tightening, focused ultrasound energy is delivered non-invasively and selectively into the dermal layer as well as into the skin’s deep support matrix without disturbing the superseding tissue. This skin tightening treatment can be done around the eyes, cheeks and upper neck.</p>
                <h3 class="font-weight-bold text-center ptb-1">Post op care Instruction after Ultherapy Skin Tightening Treatment</h3>
                <p>As ultherapy is a completely non-invasive procedure and involves no cuts or injections which mean no bleeding. Further there will be no to minimal chances of any infection. But due to the use of ultrasonic energy, your skin may become sensitive for few days and you have to take care of few things after the procedure. Following are the few post treatment instructions of ultherapy treatment for skin tightening which you have to keep in mind;</p>
                <ul>
                    <li>Do not use any products containing Retin-A, Retinoids or vitamin A, strong exfoliating agents, bleaching creams or scrubs on face for at least 3 days after your treatment or until the pinkish color has subsided. </li>
                    <li>There may be temporary swelling on face after ultherapy treatment. Do not worry, it will not last long. To avoid the swelling, try to keep your head upwards or sleep on 2 pillows. </li>
                    <li>You may observe mild tenderness or bruising around your jaw line. If there is more momentous discoloration, then use the topical prescribed by the physician to minimize the bruising.</li>
                    <li>In case of welts or hives on the skin after ultherapy procedure, you can apply the ice on them or if they persist for longer, consult with the physician.</li>
                    <li>There is a possibility of the nerve inflammation. But this will resolve itself within few days.</li>
                    <li>Avoid the direct sun exposure after the treatment for at least 2-3 days. Always use the sunscreen while going out and re apply it after every 2 hours if you have to stay longer in UV rays.</li>
                    <li>Do not scratch or vigorously rub your skin after the ultherapy treatment as skin is sensitive. Scratching the skin can cause rashes or scars.</li>
                    <li>If your skin appear broken or there are blisters, use the antibiotic ointment or consult the physician immediately.</li>
                    <li>Avoid any other cosmetology procedure like <a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/botulinum-toxin-botox/">Botox</a>, <a href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/fillers/">dermal fillers</a> etc after 2 weeks of treatment.</li>
                    <li>As there is collagen production in the skin after the penetration of ultrasound energy, there may be a tingling sensation or numbness for few days after the treatment. No need to worry. It will resolve by itself.  </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li><a href="<?= base_url(); ?>acne-scar-treatment-post-op-instructions/">Acne Scar Treatment</a></li>
                        <li><a href="<?= base_url(); ?>anti-ageing-treatments-post-op-care-instructions/">Anti Ageing Treatments</a></li>
                        <li><a href="<?= base_url(); ?>chemical-peels-post-op-instructions/">Chemical Peels</a></li>
                        <li><a href="<?= base_url(); ?>co2-laser-treatment-post-op-instructions/">Co2 Laser Treatment</a></li>
                        <li><a href="<?= base_url(); ?>cosmedico-treatment-post-op-instructions/">Cosmedico Treatment</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia-post-op-instructions/">Gynecomastia</a></li>
                        <li><a href="<?= base_url(); ?>hair-gain-therapy-post-op-care-instructions/">Hair Gain Therapy</a></li>
                        <li><a href="<?= base_url(); ?>laser-hair-removal-post-op-care-instructions/">Laser Hair Removal</a></li>
                        <li><a href="<?= base_url(); ?>laser-vaginal-rejuvenation-post-op-instructions/">Laser Vaginal Rejuvenation</a></li>
                        <li><a href="<?= base_url(); ?>non-surgical-ultrasonic-liposuction-post-op-care-instructions/">Non Surgical Ultrasonic Liposuction</a></li>
                        <li><a href="<?= base_url(); ?>pigmentation-treatment-post-op-care-instructions/">Pigmentation Treatment</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty-post-op-instructions/">Rhinoplast</a></li>
                        <li><a href="<?= base_url(); ?>scalp-micropigmentation-post-op-instructions/">Scalp Micropigmentation</a></li>
                        <li><a href="<?= base_url(); ?>stretch-marks-treatment-post-op-care-instructions/">Stretch Marks Treatment</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment-post-op-instructions/">Vitiligo Treatment</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div><div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>