<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>contact-us/"> <span itemprop="name">Contact Us</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
                <!-- Breadcrumbs /--> 
            </div>
        </div>
    </div>
</div>
<div class="service-area">
    <div class="container-fluid"> <div class="row">
      
        <div id="map"></div></div>
    </div>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
<style>
    #map {height: 500px;width: 100%;}
</style>
<script>
    var citymap = {
        ludhiana: {
            center: {lat: 30.893368, lng: 75.821912},
            label: {location: 'L'},
            title: {address: 'AK Clinics - Hair Transplant in Ludhiana, 51-E, Sarabha Nagar, Opp Kipps Market, Ludhiana, Punjab 141001'},
            population: 100000
        },
        delhi: {
            center: {lat: 28.549279, lng: 77.234676},
            label: {location: 'D'},
            title: {address: 'AK Clinics - Hair Transplant, Skin Treatment in Delhi, M 20, Near M Block Market, Hans Raj Gupta Marg, M Block, Greater Kailash-1, New Delhi, Delhi 110048'},
            population: 100000
        },
        banglore: {
            center: {lat: 12.9818802, lng: 77.6404595},
            label: {location: 'B'},
            title: {address: 'AK Clinics - Hair Transplant, Skin Treatment in Bangalore, 1st Floor, 316 The Mayfair, 100 Feet Rd, Indiranagar, Bengaluru, Karnataka 560038'},
            population: 100000
        }
    };

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4.7,
            center: {lat: 23.30, lng: 80.80},
            mapTypeId: 'terrain'
        });

        for (var city in citymap) {
            console.log(citymap);
            console.log(city);
            var cityCircle = new google.maps.Marker({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                map: map,
                position: citymap[city].center,
                label: citymap[city].label.location,
                title: citymap[city].title.address
            });
        }
    }
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBA3rxQZrOWXjWqzPnKz9_9rxRB5MfJyNw&callback=initMap">
</script>