<style>
    .address, .map-container {min-height: 400px;position: relative;}
    .address {background: #ff9000;padding-top: 88px;}
    section .location ul {display: block;padding: 0;width: 100%;border: 0px solid rgba(0,0,0,.2)!important;background: transparent;}
    .location ul li {border-right: 1px solid #f3f3f3;display: inline-block;float: left;width: 33.333%;}
    .location img {display: block;margin: 0 auto;opacity: .9;padding: 5px;width: 48px;}
    .location h4 {color: #f3f3f3;font-size: 14px!important;font-weight: 500;margin: 5px auto;font-family: open sans;text-align: center;text-transform: capitalize; width:100%;}
    section h4:last-child {border-bottom: 0px solid rgba(0,0,0,.2);border: 0px #ddd solid;}
    .location ul li:last-child { border: 0px #ddd solid;}
</style>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>locations/"> <span itemprop="name">Locations</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
                <!-- Breadcrumbs /--> 
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
<section id="ludhiana" class="container-fluid">
    <div class="row social">
        <div class="col-md-6 address">
            <div class="row location">
                <div class="col-md-12">
                    <ul>
                        <li>
                            <img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>images/location-map.svg" alt="" />
                            <h4>51-E, Sarabha Nagar,
                                Opp Kipps Market, Ludhiana,
                                Punjab 141001 - India</h4>
                        </li>
                        <li>
                            <img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>images/phone-icon.svg" alt="" />
                            <h4>+91-97799-44207</h4>
                        </li>
                        <li>
                            <img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>images/email-icon.svg" alt="" />
                            <h4><a href="mailto:info@akclinics.com">info@akclinics.com</a></h4>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row social-profiles"></div>
        </div>
        <div class="col-md-6 map-container">
            <iframe style="border: 0; float: right;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d855.9418352961369!2d75.82153649072075!3d30.893168905441573!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x639729204cedce96!2sAK+Clinics-FUE+Hair+Transplant+Ludhiana%2C+Jalandhar%2C+Chandigarh%2C+Amritsar!5e0!3m2!1sen!2sin!4v1399632583167" width="100%" height="400" frameborder="0"></iframe>
        </div>
    </div>
</section>
<section id="delhi" class="container-fluid">
    <div class="row social">
        <div class="col-md-6 map-container">
            <iframe style="border: 0; float: right;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28037.042937623854!2d77.2424201107058!3d28.550827967529386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x86a558e406078cfd!2sA+K+Clinics+(FUE+Hair+Transplant%2C+New+Delhi)!5e0!3m2!1sen!2sin!4v1412150602156" width="100%" height="400" frameborder="0"></iframe>
        </div>
        <div class="col-md-6 address">
            <div class="row location">
                <div class="col-md-12">
                    <ul>
                        <li>
                            <img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>images/location-map.svg" alt="" />
                            <h4>M-20, Greater Kailash I,
                                Near M Block Market, New Delhi-110048,
                                India.</h4>
                        </li>
                        <li>
                            <img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>images/phone-icon.svg" alt="" />
                            <h4>+91-81309-44227
                                +91-78385-99227</h4>
                        </li>
                        <li>
                            <img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>images/email-icon.svg" alt="" />
                            <h4><a href="mailto:info@akclinics.com">info@akclinics.com</a></h4>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row social-profiles"></div>
        </div>
    </div>
</section>
<section id="bangalore" class="container-fluid">
    <div class="row social">
        <div class="col-md-6 address">
            <div class="row location">
                <div class="col-md-12">
                    <ul>
                        <li><img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>images/location-map.svg" alt="" />
                            <h4>1st Floor, 316 The Mayfair, 100 Feet Road, Indiranagar, Bengaluru, Karnataka 560038</h4>
                        </li>
                        <li><img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>images/phone-icon.svg" alt="" />
                            <h4>+91-97799-44207</h4>
                        </li>
                        <li><img class="lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>images/email-icon.svg" alt="" />
                            <h4><a href="mailto:info@akclinics.com">info@akclinics.com</a></h4>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row social-profiles"></div>
        </div>
        <div class="col-md-6 map-container">
            <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15556.050660773453!2d77.59973899999997!3d12.906907000000007!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae151bc82db5f9%3A0xda6389d915267a4e!2sAK+Clinics+-+Hair+Transplant+Bengaluru!5e0!3m2!1sen!2sin!4v1440415994952" width="100%" height="400" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
        </div>
    </div>
</section>
