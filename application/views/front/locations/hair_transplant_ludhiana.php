<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>locations/"> <span itemprop="name">Locations</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>locations/hair-transplant-ludhiana/"> <span itemprop="name">Hair Transplant Ludhiana</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">FUE and FUT Hair Transplant in Ludhiana</h1>
                </div>
                <p>The team at AK Clinics are pioneers in a myriad different ways and the fact that they brought the most modern techniques of hair transplant and hair loss treatment to the city of Ludhiana happens to be just one of them. The clinic itself isan ode to state of the art equipment and the hands that handle the equipment are immensely experienced and known to work magic!</p>
                <h4>Here are just some of the services you can avail at the clinic:</h4>
                <ul class="text-left">
                    <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT </a>and <a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE hair transplant</a> – We offer you both FUT and FUE methods of hair transplant and the most suitable one for you, would be advised post examination. Ensure that the scars from the strip surgery or FUT are carefully hidden underneath the new growth areas.</li>
                    <li><a href="<?= base_url(); ?>hair-transplant/bio-fue/">Bio FUE</a> – An innovative method, which has been created in house, this procedure has allowed us to give ground breaking results. It is our Trademark Technology.</li>
                    <li><a href="<?= base_url(); ?>hair-transplant/giga-session/">Giga sessions</a> – Even if you require as many as 4000 grafts, we can cover it one single session, giving you the best results, with minimal sittings.</li>
                    <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body hair transplant</a> – There is no need to be disappointed if you do not have a large enough donor area on your head. Our team will locate another way to give you back your crowning glory!</li>
                </ul>
                <h4>These are just some of the reason why you would want to visit AK Clinics:</h4>
                <ul class="text-left">
                    <li>We attribute much of our success to the fact that our transection rate is one of the lowest in the country, if not the world, at a meagre 5%.</li>
                    <li>Our aim is to make our clients happy, which is why we charge based on the total number of grafts – this means, the lesser the grafts, lower your final bill!</li>
                    <li>We know how much your persona means to you, which is why we will painstakingly create a hairline that looks completely natural.</li>
                    <li>We understand that most clients are worried about the pain, which is why our procedures are designed to be minimally painful.</li>
                    <li>Our clinic has been appreciated several times for the high standards of hygiene and we personally ensure that the clinic is sanitised regularly.</li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                        <li class=""><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Op Instructions</a></li>
                        <li><a href="<?= base_url(); ?>hair-transplant/faqs/">Faq's</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle ">Why AK Clinics for Hair Transplant in Ludhiana?</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Why AK Clinics for Hair Transplant in Ludhiana?</h3>
                    <p>There are several reasons why people from all over Ludhiana and Punjab, choose to come to AK Clinics, a leading hair transplant clinic of Ludhiana with their hair transplantation related requirements, and just some of them include.</p>
                    <div class="row text-left">
                        <div class="col-md-6">
                            <h4>Natural Hair Line</h4>
                            <p> Since the donor hair will be harvested from your head, you can be sure that they will look completely natural. Additionally, they will be permanent and will grow healthy in a matter of time. Once they fit in, you can cut them, style them or even colour them, the way you want. </p>
                        </div>
                        <div class="col-md-6">
                            <h4>Pain-free Procedure</h4>
                            <p> We have developed numerous ingenious methods of anaesthesia and sedation, which will ensure that you feel absolutely comfortable <a href="<?= base_url(); ?>blog/before-during-after-hair-transplant/">during the procedure</a>. If you are really worried about the pain, we will provide you with sedation and you can sleep through the entire procedure. </p>
                        </div>
                        <div class="col-md-6 ">
                            <h4>High Graft Yield</h4>
                            <p> Our Follicular Transection Rate is well below 5% in <a href="<?= base_url(); ?>blog/understanding-ftr-follicular-transection-rate/">follicular unit extraction</a>, and this is proof that we go to extraordinary lengths to ensure the health of the follicles. In addition, we harvest only from the permanent zones and ensure that the grafts spend minimum time outside the body </p>
                        </div>
                        <div class="col-md-6">
                            <h4>Graft Given by Counting</h4>
                            <p> The patient is given an option to count to the grafts/follicles to instil a level of confidence in the clinic. Only viable follicles are counted </p>
                        </div>
                        <div class="col-md-6">
                            <h4>Hair Transplant cost</h4>
                            <p> The <a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">cost</a> of hair transplant in Ludhiana ranges from INR 90K (USD1500) to INR 2.0 Lac (USD 3450) depending on various factors. A complete assessment is required before transplant is planned. </p>
                        </div>
                        <div class="col-md-6">
                            <h4>Ultra Hygiene</h4>
                            <p> We give utmost importance to the hygiene to give patient a complete infection free surgery. It is very important aspect that amounts to final results </p>
                        </div>
                    </div>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Hair Transplant Method</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Hair Transplant Method</h3>
                    <p>The initial methods of hair transplant were based around removing an entire patch of the scalp and then removing the grafts from that piece of scalp. The improved version of that procedure is what we know as follicular unit transplant or FUT. The more modern method is what is known as FUE or follicular unit extraction, wherein there are no cuts.</p>
                    <div class="row text-left">
                        <div class="col-md-6 ">
                            <h4>FUE Hair Transplant</h4>
                            <p>This is the more advanced version of hair transplant and only those with true experience attempt this, because of its complexity. The idea behind the procedure, though, is quite a simple one – with a suction like method, individual grafts are removed from the scalp. There are no cuts involved, which means no sutures are required. This also means that there are no scars. However, this is a slightly more time consuming method, which is why not a lot of doctors prefer it. This procedure is most suited in cases, where only a small area needs to be covered with grafts or a lesser number of grafts are required. It is important to understand that hair transplant is not the only option for hair loss, because if the hair loss is minimal, then you could try other methods such as hair extensions and weaves.</p>
                        </div>
                        <div class="col-md-6 ">
                            <h4>FUT or strip surgery</h4>
                            <p>In this procedure, a strip of the scalp is removed and then hair grafts are removed from that piece. The area from where the strip has been removed will be closed using sutures or staples. The hair will be transplanted in such a way that when the regrowth happens the linear scar will get covered. This is normally the preferred method of hair transplant in cases, where a large recipient area needs to be covered. However, there are a few causes for concern with FUT, especially if the procedure has not been done by an experienced professional. The scar could be very visible, there could be infection in the stitches and there could be bleeding at the donor site.</p>
                        </div>
                    </div>
                </div>
                <div class="tab">
                    <button class="tab-toggle">FUE and FUT Comparison</button>
                </div>
                <div class="content">
                    <h3 class="m-3">FUE and FUT Comparison</h3>
                    <div class="responsive-table">
                        <table class="table table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <th scope="col">FUE</th>
                                    <th scope="col">FUT</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><strong>1.</strong></td>
                                    <td><strong>Method</strong></td>
                                    <td>Extraction of individual follicular units through small punches. No Sutures</td>
                                    <td>Scalpel excision of donor strip with suturing</td>
                                </tr>
                                <tr>
                                    <td><strong>2.</strong></td>
                                    <td><strong>Scarring</strong></td>
                                    <td>Small scars are formed which are inconspicuous and get merged very well most of the times but not always especially if overdone</td>
                                    <td>Linear Scar that may be visible if hair is trimmed /shaved</td>
                                </tr>
                                <tr>
                                    <td><strong>3.</strong></td>
                                    <td><strong>Donor Source</strong></td>
                                    <td>Scalp and Body</td>
                                    <td>Back of Scalp</td>
                                </tr>
                                <tr>
                                    <td><strong>4.</strong></td>
                                    <td><strong>Quantity of Hair</strong></td>
                                    <td>Large session difficult (2000 – 2500 graft)</td>
                                    <td>Larger session (Upto 3000 graft possible)</td>
                                </tr>
                                <tr>
                                    <td><strong>5.</strong></td>
                                    <td><strong>Natural Result</strong></td>
                                    <td>Yes, Permanent</td>
                                    <td>Yes, Permanent</td>
                                </tr>
                                <tr>
                                    <td><strong>6.</strong></td>
                                    <td><strong>Recovery</strong></td>
                                    <td>Back to routine activity within few days</td>
                                    <td>Suturing used healing over 10 days</td>
                                </tr>
                                <tr>
                                    <td><strong>7.</strong></td>
                                    <td><strong>Cost</strong></td>
                                    <td>Moderate</td>
                                    <td>Slightely Less</td>
                                </tr>
                                <tr>
                                    <td><strong>8.</strong></td>
                                    <td><strong>Time Take</strong></td>
                                    <td>Moderate</td>
                                    <td>Slightely Less</td>
                                </tr>
                                <tr>
                                    <td><strong>9.</strong></td>
                                    <td><strong>Pain</strong></td>
                                    <td>Not Painless. But anesthesia is given in such a way that it feels painless to the patient</td>
                                    <td>Not Painless. But anesthesia is given in such a way that it feels painless to the patient</td>
                                </tr>
                                <tr>
                                    <td><strong>10.</strong></td>
                                    <td><strong>Preferred</strong></td>
                                    <td>Hair restoration of short hair/small number of graft or for patients that have undergone several STRIP procedures and their scalp has become too tight</td>
                                    <td>Grade of baldness is higher and the donor area is not very good, strip technique is usually preferred</td>
                                </tr>
                            </tbody>
                        </table></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h2 class="black-text">Type of Hair Transplant in Ludhiana</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="row text-center">
                <div class="col-lg-3" ><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/" target="_blank"> <strong>FUE Hair Transplant</strong></a>
                    <p>In this technique individual follicular units containing 1 to 4 hairs are taken from the donor area with punches ranging 0.6mm to 1.0mm in diameter and implanted in the recipient area.<br />
                    </p>
                </div>
                <div class="col-lg-3">  <a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/" target="_blank"><strong>FUT Hair Transplant</strong></a>
                    <p>In this technique a strip of hair is taken from the permanent zone and it is dissected to individual grafts under magnification for implantation. Thereon the resultant donor area is sutured.<br />
                        read more</p>
                </div>
                <div class="col-lg-3"> <a href="<?= base_url(); ?>hair-transplant/bio-fue/" target="_blank"><strong>Bio-FUE<sup style="position: relative; font-size: 10px; top: -18px;">TM</sup></strong></a>
                    <p>This is a trademark technique of AK Clinics. This technique is improvised version of FUE that has not only given better results over FUE but also quicker healing to the patients. The even further refined technique is Bio-DHT<sup>™</sup><br />
                    </p>
                </div>
                <div class="col-lg-3"> <a href="<?= base_url(); ?>hair-transplant/giga-session/" target="_blank"><strong>Giga-Session @ AK</strong></a>
                    <p>This technique is highly recommended for patients with larger requirement i.e. over 4000 grafts. We use a combination of FUE, Body FUE & Strip technique for graft harvesting.<br />
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="divider parallax layer-overlay overlay-black animated fadeIn delay-5s" data-bg-img="<?php echo base_url('assets/template/frontend/'); ?>img/testimonial.jpg" style="background-image: url(&quot;<?php echo base_url('assets/template/frontend/'); ?>img/testimonial..jpg&quot;); ">
    <div class="container ptb-3">
        <div class="row">
            <div class="col-md-12 text-center text-white">
                <h2 class="">Testimonials</h2>
            </div>
            <div id="carouselExampleIndicators" class="carousel slide text-white" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
                </ol>
                <div class="carousel-inner mt-4  text-white">
                    <div class="carousel-item text-center  text-white">
                        <div class="img-box p-1 border rounded-circle m-auto"> 
                            <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>raj-n.jpg" alt="First slide"> </div>
                        <h5 class="mt-4"><strong class=" text-uppercase">Raj Jaswal</strong></h5>
                        <p class="m-0 pt-2 text-white">
                            I started facing hair loss at the age of 26 but it was not very noticeable. But at 29 
                            the hair loss progressed very quickly and crown almost became empty. And further my 
                            hair line receded quickly. Now at the age of 31 I started looking very old. 
                            I tried many medicines like homeopathic etc etc but nothing seems to work. 
                            Then I started searching for my options and finally decided for a hair transplant.
                        </p>
                    </div>
                    <div class="carousel-item text-center">
                        <div class="img-box p-1 border rounded-circle m-auto"> 
                            <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>supal.jpg " alt="First slide"> </div>
                        <h5 class="mt-4 "><strong class="text-uppercase">Vikalp Gaur</strong></h5>
                        <p class="m-0 pt-2 text-white">
                            I can see good results post-transplant. Overall good. As suggested, I may go for 
                            further transplant for crown area. I am currently taking 6 months cyclical therapy 
                            again to stop hair fall. Then will take the decision accordingly. I can say I would 
                            prefer someone for A K Clinic. As it is giving person the way to live more happily.
                        </p>
                    </div>
                    <div class="carousel-item text-center active">
                        <div class="img-box p-1 border rounded-circle m-auto"> 
                            <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="First slide"> </div>
                        <h5 class="mt-4"><strong class="text-uppercase">Rahul Bhatia</strong></h5>
                        <p class="m-0 pt-2 text-white">
                            Had a hair transplant at AK clinic under care of Doc. KAPIL DUA.And after treatment it 
                            has been 3 month now and I have very much inproved growing hair. He is best surgeon 
                            ever seen and very experienced in this art. They also provided very good atmosphere 
                            while surgery. With his experience he gave very confident to his patients while surgery.
                        </p>
                    </div>
                    <div class="carousel-item text-center">
                        <div class="img-box p-1 border rounded-circle m-auto"> 
                            <img class="d-block w-100 rounded-circle lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="First slide"> </div>
                        <h5 class="mt-4"><strong class="text-uppercase">Suresh</strong></h5>
                        <p class="m-0 pt-2 text-white">
                            After talking to Dr Kapil I have decided to go for a hair transplant surgery which turned
                            out to be very good. Total of 6000 grafts were transplanted. The density was medium 
                            density. The procedure was FUE without any scar. The procedure was by done both Dr 
                            Kapil and Dr Aman. I strongly recommend anyone who is looking for a FUE surgery in India.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h2 class="black-text">Hotels Near AK Clinics Ludhiana</h2>
                </div>
            </div>
        </div>
        <div class="row text-center ptb-1">
            <div class="col-lg-3 col-md-3"> <h4 class="text-left">Where is the clinic Located</h4>
                <ul class="text-left">
                    <li>Distance from Airport(Chandigarh/Amritsar)</li>
                    <li>Distance from Ludhiana Railway Station <strong>8 KM</strong></li>
                    <li>Distance from ISBT Bus Stand Ludhiana <strong>07 KM</strong></li>
                </ul>
            </div>
            <div class="col-lg-9 col-md-9">
                <div class="row">
                    <div class="col-md-4"> <img class="img-fluid rounded lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>hotel-img6.jpg" alt="Hotel Ivory 32"> <h4 class="uppercase">Hotel Maharaja Regency</h4>
                        <p >This hotel is situated almost about 1.5 KM from the clinic and is one of the landmarks in Ludhiana.It has a beautiful imposing building architecture.<br />
                            Room Rent 4000-6500</p>
                    </div>
                    <div class="col-md-4"> <img class="img-fluid rounded lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>hotel-img5.jpg" alt="Hotel Private Affair"> <h4 class="uppercase">Hotel Onn</h4>
                        <p>It is a budget hotel & property of the Taksonz group that has been in the business of hospitality for over three decades.<br>
                            Prices range – INR 3000 – INR 4500</p>
                    </div>
                    <div class="col-md-4"> <img class="img-fluid rounded lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>hotel-img4.jpg" alt="Hotel Eros"> <h4 class="uppercase">Hotel Hyatt Regency</h4>
                        <p>It is one of the best five star hotels in Ludhiana located on Ferozepur road around 4 KMs from the clinic.<br>
                            Prices range – INR 4500 – INR 7500</p>
                    </div>
                </div>
            </div>
        </div>
    </div><div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/services_ldh'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
     <style>.carousel-item {
    height: 20rem;
}</style>
    <script type="application/ld+json">
        {
        "@context": "http://schema.org",
        "@type": "Service",
        "name": "Hair Transplant Clinic in Ludhiana",
        "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "4.8",
        "reviewCount": "268"
        }
        }
    </script>