
<style>
thead-dark th{    color: #fff;
    background-color: #212529;
    border-color: #32383e;}
	
	
	a.slide-over {
	opacity: 0.8;
	background: #ccc;
}
a.slide-over:hover {
	opacity: 1;
}
.carousel-item {
	height: 23rem;
}
	@media only screen and (max-width: 760px), (min-device-width: 768px) and (max-device-width: 1024px) {
table, thead, tbody, th, td, tr {
	display: block;
}
table.cost-table tr th, table.cost-table tr td {
    font-size: 12px;
}

table.cost-table thead tr {
	position: absolute;
	top: -9999px;
	left: -9999px;
}
table.cost-table tr {
	margin: 0 0 1rem 0;
}
table.cost-table tr:nth-child(odd) {
	background: #ccc;
}
table.cost-table tr td {
	border: none;
	border-bottom: 1px solid #fff;
	position: relative;
	padding-left: 50%;
}
table.cost-table tr td:before {

	position: absolute;

	top: 0;
	left: 6px;
	width: 45%;
	padding-right: 10px;
	white-space: nowrap;
}

td:nth-of-type(1):before {
	content: "No. of Follciles";
}
td:nth-of-type(2):before {
	content: "Cost on Lower Side";
}
td:nth-of-type(3):before {
	content: "Cost on Higher Side";
}
td:nth-of-type(4):before {
	content: "No of Sittings";
}


table.futfue td:nth-of-type(1):before {
	content: "";
}
table.futfue td:nth-of-type(2):before {
	content: "";
}
table.futfue td:nth-of-type(3):before {
	content: "FUE";
}
table.futfue td:nth-of-type(4):before {
	content: "FUT";
}


}

@media (max-width: 640px) and (min-width: 320px){
table.cost-table tbody tr td {
    text-align: left !important;
}

table.cost-table td:before {
    position: absolute;
    top: 9px !important;
    left: 6px;
    text-align: left;
    width: 45%;
    padding-right: 10px;
    white-space: nowrap;
}


table.dr td:nth-of-type(1):before, table.dr td:nth-of-type(2):before  {
    content: "" !important;
}

.carousel-item {
    height: 26rem;
}

.carousel-indicators {
    top: 450px;
}}
@media only screen and (max-width: 760px), (max-device-width: 1024px) and (min-device-width: 768px){
table.futfue tr td {
   border-bottom:1px solid #fff;
   border: none;
}
}



.table td, .table th {
    padding: .45rem;
    border: none;
}

</style>
<?php $this->load->view('front/partials/hair_transplant_banner'); ?>
<div class="header-banner-content-area light-orange">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-left m-2 text-dark" >
        <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
            <meta itemprop="position" content="1" />
          </li>
          <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>locations/"> <span itemprop="name">Locations</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
            <meta itemprop="position" content="2" />
          </li>
          <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>locations/hair-transplant-delhi/"> <span itemprop="name">Hair Transplant Delhi</span></a>
            <meta itemprop="position" content="3" />
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="section-title-1 text-left">
          <h1 class="black-text">Hair Transplant in Delhi</h1>
          <p>Trust and Quality are what we have earned in Delhi since the start of the journey. Having best in-the-class facilities accompanied by successful history in providing results are what we are today proud of. Unlike other, we don't believe in just claiming best hair transplant clinic in Delhi rather we have proved it. By now, our team of experienced surgeons has provided thousands with exceptional results and number is still increasing. Our every endeavor is to meet the expectations and aspirations of people seeking results and help them in bringing their charm back.</p>
        </div>
      </div>
      <div class="col-md-6">
        <div class="section-title text-center mt-5">
          <figure><img data-src="<?php echo cdn('assets/template/frontend/'); ?>img/d-2.png" class="img-fluid rounded lazy" alt="Best Hair Transplant Clinic in  Delhi" title="Best Hair Transplant" style="width:80%;"></figure>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title text-center">
          <h2 class="black-text">FUE and FUT Hair Transplant in Delhi</h2>
          <p>Are you struggling with hair loss? Are you in the India’s National Capital Region? Then the easiest way of getting your hair back is in the posh M Block market of Greater Kailash, Delhi where you will find AK Clinics. This is your one stop for all issues related to hair and skin, because we offer you not only the most modern hair transplant techniques, but also methods to maintain or improve your skin. Hair Transplant in Delhi is not going to cut your pockets if you are at AK Clinics. We provide you treatment at affordable cost with EMI option. We offer our clients a range of services including:</p>
        </div>
      </div>
    </div>
    <div class="col-md-12 ptb-1">
      <div class="row text-center">
        <div class="col-lg-3" > <a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/" target="_blank">
          <h4>FUE Hair Transplant</h4>
          </a>
          <p>In this technique individual follicular units containing 1 to 4 hairs are taken from the 
            donor area with punches ranging 0.6mm to 1.0mm in diameter and implanted in the recipient 
            area</p>
        </div>
        <div class="col-lg-3"> <a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/" target="_blank">
          <h4>FUT Hair Transplant</h4>
          </a>
          <p> In this technique a strip of hair is taken from the permanent zone and it is dissected to 
            individual grafts under magnification for implantation. Thereon the resultant donor area is 
            sutured</p>
        </div>
        <div class="col-lg-3"> <a href="<?= base_url(); ?>hair-transplant/bio-fue/" target="_blank">
          <h4>Bio-FUE<sup style="position: relative; font-size: 10px; top: -18px;">TM</sup></h4>
          </a>
          <p>This is a trademark technique of AK Clinics. This technique is improvised version of FUE that has not only given better results over FUE but also quicker healing to the patients. The even further refined technique is Bio-DHT<sup>™</sup><br />
          </p>
        </div>
        <div class="col-lg-3"> <a href="<?= base_url(); ?>hair-transplant/giga-session/" target="_blank">
          <h4>Giga-Session @ AK</h4>
          </a>
          <p> This technique is highly recommended for patients with larger requirement i.e. over 4000 
            grafts. We use a combination of FUE, Body FUE &amp; Strip technique for graft harvesting<br />
            read more </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12 bg-img cover-bg sm-height-550px xs-height-350px " data-background="<?php echo cdn('assets/template/frontend/'); ?>img/slide1.png" alt="Hair Transplant in Delhi" title="Hair Restoration and Transplantation" style="background-image:url(<?php echo cdn('assets/template/frontend/'); ?>img/slide1.png);"></div>
      <div class="col-md-6 col-sm-12 col-xs-12 bg-dark pd-60 pl-sm10 pr-sm10">
        <div class="cent">
          <div class="section-head">
            <h4 class="text-white">Why AK Clinics is among Top 10 Hair Transplant Centers of Delhi?</h4>
          </div>
          <p class="pb-20 text-white">AK Clinics is highly rated by many websites because of the advanced follicle harvesting techniques that ensure a successful hair transplant with extraordinary results. These techniques entail minimal wastage of hair follicles leading to donor area safety.</p>
          <ul class="text-white">
            <li> We hold an expertise in ensuring really high graft yields and our transection rate is less than 5%</li>
            <li>We will ensure that the hair line that is created is natural looking and permanent</li>
            <li>We charge based on the number of grafts that have been successfully extracted, giving customers true value for money</li>
            <li>We ensure that our customers have to undergo minimal pain or discomfort.</li>
            <li>We maintain the highest standards to hygiene, ensuring complete safety for our clients</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area ptb-3 ">
  <div class="container">
    <div class="col-md-12">
      <div class="tabs">
        <div class="tab">
          <button class="tab-toggle">Hair Transplant Estimated Cost in Delhi</button>
        </div>
        <div class="content">
          <h3 class="m-3">Hair Transplant Estimated Cost in Delhi</h3>
          <p class="ptb-1">The cost of hair transplant in Delhi ranges from INR 90K (USD1500) to INR 2.0 Lac (USD 3450) depending on various factors. A complete assessment is required before transplant is planned.</p>
       <div class="table-responsive">
         
         <table class="table table-hover cost-table" role="table">
            <thead role="rowgroup" class="thead-dark ">
              <tr role="row">
                <th role="columnheader"><strong>Number of Follciles</strong></th>
                <th role="columnheader">Cost on Lower Side</th>
                <th role="columnheader">Cost on Higher Side</th>
                <th role="columnheader">No of Sittings</th>
              </tr>
            </thead>
            <tbody role="rowgroup">
              <tr role="row">
                <td role="cell">1000 – 1500 Hair Follicles</td>
                <td role="cell">INR 30,000/-</td>
                <td role="cell">INR 65,000/-</td>
                <td role="cell">1 Sitting</td>
              </tr>
              <tr role="row">
                <td role="cell">2000 – 3000 Hair Follicles</td>
                <td role="cell">INR 60,000/-</td>
                <td role="cell">INR 145,000/- </td>
                <td role="cell">1 Sitting</td>
              </tr>
              <tr role="row">
                <td role="cell">4000 – 5000 Hair Follicles</td>
                <td role="cell">INR 120,000/-</td>
                <td role="cell">INR 225,500/- </td>
                <td role="cell">1 Sitting</td>
              </tr>
              <tr role="row">
                <td role="cell">6000 – 8000 Hair Follicles</td>
                <td role="cell">INR 200,000/- </td>
                <td role="cell">INR 350,000/- </td>
                <td role="cell">1-2 Sitting</td>
              </tr>
            </tbody>
          </table>
         
         
         </div>
         <p>To know more about hair transplant cost <a href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/" target="_blank">click here</a></p>
        </div>
        <div class="tab">
          <button class="tab-toggle">Why AK Clinics for Hair Transplant in Delhi?</button>
        </div>
        <div class="content">
          <h3 class="m-3">Why AK Clinics for Hair Transplant in Delhi?</h3>
          <p>here are several reasons why people from all over Delhi, Noida and Gurgoan, choose to come to AK Clinics, a leading hair transplant clinic of Delhi NCR Region with their hair transplantation related requirements, and just some of them include</p>
          <div class="row text-left">
            <div class="col-md-6 ">
              <h4>Natural Hair Line</h4>
              <p>Since the donor hair will be harvested from your head, you can be sure that they will look completely natural. Additionally, they will be permanent and will grow healthy in a matter of time. Once they fit in, you can cut them, style them or even colour them, the way you want. </p>
            </div>
            <div class="col-md-6 ">
              <h4>Pain-free Procedure</h4>
              <p>We have developed numerous ingenious methods of anaesthesia and sedation, which will ensure that you feel absolutely comfortable during the procedure. If you are really worried about the pain, we will provide you with sedation and you can sleep through the entire procedure. </p>
            </div>
            <div class="col-md-6 ">
              <h4>High Graft Yield</h4>
              <p>Our Follicular Transection Rate is well below 5% in follicular unit extraction, and this is proof that we go to extraordinary lengths to ensure the health of the follicles. In addition, we harvest only from the permanent zones and ensure that the grafts spend minimum time outside the body </p>
            </div>
            <div class="col-md-6 ">
              <h4>Graft Given by Counting</h4>
              <p>The patient is given an option to count to the grafts/follicles to instil a level of confidence in the clinic. Only viable follicles are counted </p>
            </div>
            <div class="col-md-6 ">
              <h4>Experienced Surgeons </h4>
              <p>At AK Clinics, the surgeons are trained on the technique which is unique. This can also be only performed at our centres because it requires Standardized structure with technology platform ‘intelligible’.</p>
            </div>
            <div class="col-md-6 ">
              <h4> Ultra Hygiene</h4>
              <p>We give utmost importance to the hygiene to give patient a complete infection free surgery. It is very important aspect that amounts to final results </p>
            </div>
          </div>
        </div>
        <div class="tab">
          <button class="tab-toggle">Hair Transplant Method</button>
        </div>
        <div class="content">
          <h3 class="m-3">Hair Transplant Method</h3>
          <p>The initial methods of hair transplant were based around removing an entire patch of the scalp and then removing the grafts from that piece of scalp. The improved version of that procedure is what we know as follicular unit transplant or FUT. The more modern method is what is known as FUE or follicular unit extraction, wherein there are no cuts.</p>
          <div class="col-md-12">
            <div class="row">
              <h4><a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></h4>
              <p>This is the more advanced version of hair transplant and only those with true experience attempt this, because of its complexity. The idea behind the procedure, though, is quite a simple one – with a suction like method, individual grafts are removed from the scalp. There are no cuts involved, which means no sutures are required. This also means that there are no scars. However, this is a slightly more time consuming method, which is why not a lot of doctors prefer it. This procedure is most suited in cases, where only a small area needs to be covered with grafts or a lesser number of grafts are required. It is important to understand that hair transplant is not the only option for hair loss, because if the hair loss is minimal, then you could try other methods such as hair extensions and weaves.</p>
              <h4><a href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT or strip surgery</a></h4>
              <p>In this procedure, a strip of the scalp is removed and then hair grafts are removed from that piece. The area from where the strip has been removed will be closed using sutures or staples. The hair will be transplanted in such a way that when the regrowth happens the linear scar will get covered. This is normally the preferred method of hair transplant in cases, where a large recipient area needs to be covered. However, there are a few causes for concern with FUT, especially if the procedure has not been done by an experienced professional. The scar could be very visible, there could be infection in the stitches and there could be bleeding at the donor site.</p>
            </div>
          </div>
        </div>
        <div class="tab">
          <button class="tab-toggle">FUE and FUT Comparison</button>
        </div>
        <div class="content">
          <h3 class="m-3">FUE and FUT Comparison</h3>
             <table class="table table-hover cost-table futfue" role="table">
            <thead role="rowgroup" class="thead-dark ">
              <tr role="row">
               <th role="columnheader"></th>
               <th role="columnheader"></th>
               <th role="columnheader">FUE</th>
               <th role="columnheader">FUT</th>
              </tr>
            </thead>
            <tbody>
             <tr role="row">
                <td role="cell"><strong>1.</strong></td>
                 <td role="cell"><strong>Method</strong></td>
                <td role="cell">Extraction of individual follicular units through small punches. No Sutures</td>
                 <td role="cell">Scalpel excision of donor strip with suturing</td>
              </tr>
              <tr>
                <td role="cell"><strong>2.</strong></td>
               <td role="cell"><strong>Scarring</strong></td>
                <td role="cell">Small scars are formed which are inconspicuous and get merged very well most of the times but not always especially if overdone</td>
                <td role="cell">Linear Scar that may be visible if hair is trimmed /shaved</td>
              </tr>
              <tr>
               <td role="cell"><strong>3.</strong></td>
                 <td role="cell"><strong>Donor Source</strong></td>
                 <td role="cell">Scalp and Body</td>
                <td role="cell">Back of Scalp</td>
              </tr>
              <tr>
                <td role="cell"><strong>4.</strong></td>
                <td role="cell"><strong>Quantity of Hair</strong></td>
                 <td role="cell">Large session difficult (2000 – 2500 graft)</td>
               <td role="cell">Larger session (Upto 3000 graft possible)</td>
              </tr>
              <tr>
                 <td role="cell"><strong>5.</strong></td>
                <td role="cell"><strong>Natural Result</strong></td>
                <td role="cell">Yes, Permanent</td>
               <td role="cell">Yes, Permanent</td>
              </tr>
              <tr>
                <td role="cell"><strong>6.</strong></td>
                <td role="cell"><strong>Recovery</strong></td>
                <td role="cell">Back to routine activity within few days</td>
                <td role="cell">Suturing used healing over 10 days</td>
              </tr>
              <tr>
                <td role="cell"><strong>7.</strong></td>
                <td role="cell"><strong>Cost</strong></td>
                 <td role="cell">Moderate</td>
               <td role="cell">Slightely Less</td>
              </tr>
              <tr>
                <td role="cell"><strong>8.</strong></td>
                <td role="cell"><strong>Time Take</strong></td>
                <td role="cell">Moderate</td>
                <td role="cell">Slightely Less</td>
              </tr>
              <tr>
                <td role="cell"><strong>9.</strong></td>
               <td role="cell"><strong>Pain</strong></td>
                <td role="cell">Not Painless. But anesthesia is given in such a way that it feels painless to the patient</td>
                 <td role="cell">Not Painless. But anesthesia is given in such a way that it feels painless to the patient</td>
              </tr>
              <tr>
                 <td role="cell"><strong>10.</strong></td>
                <td role="cell"><strong>Preferred</strong></td>
                 <td role="cell">Hair restoration of short hair/small number of graft or for patients that have undergone several STRIP procedures and their scalp has become too tight</td>
                <td role="cell">Grade of baldness is higher and the donor area is not very good, strip technique is usually preferred</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="divider parallax layer-overlay overlay-black animated fadeIn delay-5s" data-bg-img="<?php echo base_url('assets/template/frontend/'); ?>img/testimonial.jpg" style="background-image: url(&quot;<?php echo base_url('assets/template/frontend/'); ?>imgtestimonial.jpg&quot;); ">
  <div class="container ptb-3">
    <div class="row">
      <div class="col-md-12 text-center text-white">
        <h2 class="">Testimonials</h2>
      </div>
      <div id="carouselExampleIndicators" class="carousel slide text-white" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="4" class=""></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="5" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="6" class=""></li>
        </ol>
        <div class="carousel-inner mt-4  text-white">
          <div class="carousel-item text-center  text-white">
            <div class="img-box p-1 border rounded-circle m-auto"> <img class="d-block w-100 rounded-circle" src="<?= cdn('assets/template/frontend/images/'); ?>raj-n.jpg" alt="Raj hair transplant patient of AK Clinics, Delhi"  > </div>
            <h5 class="mt-4"><strong class=" text-uppercase">Raj Jaswal</strong></h5>
            <p class="m-0 pt-2 text-white">I started facing hair loss at the age of 26 but it was not very noticeable. But at 29 the hair loss progressed very quickly and crown almost became empty. And further my hair line receded quickly. Now at the age of 31 I started looking very old.I tried many medicines like homeopathic etc etc but nothing seems to work.Then I started searching for my options and finally decided for a hair transplant.</p>
          </div>
          <div class="carousel-item text-center">
            <div class="img-box p-1 border rounded-circle m-auto"> <img class="d-block w-100 rounded-circle" src="<?= cdn('assets/template/frontend/images/'); ?>supal.jpg " alt="Vikalp Gaur hair transplant patient in Delhi at AK Clinics"> </div>
            <h5 class="mt-4 "><strong class="text-uppercase">Vikalp Gaur</strong></h5>
            <p class="m-0 pt-2 text-white">I can see good results post-transplant. Overall good. As suggested, I may go for further transplant for crown area. I am currently taking 6 months cyclical therapy again to stop hair fall. Then will take the decision accordingly. I can say I would prefer someone for A K Clinic. As it is giving person the way to live more happily. </p>
          </div>
          <div class="carousel-item text-center active">
            <div class="img-box p-1 border rounded-circle m-auto"> <img class="d-block w-100 rounded-circle" src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="AK Clinics's transplant patient"> </div>
            <h5 class="mt-4"><strong class="text-uppercase">Rahul Bhatia</strong></h5>
            <p class="m-0 pt-2 text-white">Had a hair transplant at AK clinic under care of Dr. Kapil Dua and after treatment it has been 3 month now and I have very much improved growing hair. He is best surgeon ever seen and very experienced in this art. They also provided very good atmosphere while surgery. With his experience he gave very confident to his patients while surgery. </p>
          </div>
          <div class="carousel-item text-center">
            <div class="img-box p-1 border rounded-circle m-auto"> <img class="d-block w-100 rounded-circle" src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="AK Clinics's transplant patient"> </div>
            <h5 class="mt-4"><strong class="text-uppercase">Suresh</strong></h5>
            <p class="m-0 pt-2 text-white"> After talking to Dr. Kapil I have decided to go for a hair transplant surgery which turned out to be very good. Total of 6000 grafts were transplanted. The density was medium density. The procedure was FUE without any scar. The procedure was by done both Dr. Kapil and Dr. Aman. I strongly recommend anyone who is looking for a FUE surgery in India. </p>
          </div>
          <div class="carousel-item text-center">
            <div class="img-box p-1 border rounded-circle m-auto"> <img class="d-block w-100 rounded-circle" src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="AK Clinics's transplant patient"> </div>
            <h5 class="mt-4"><strong class="text-uppercase">Rajneesh singh</strong></h5>
            <p class="m-0 pt-2 text-white"> My friend recommends me for hair transplant form AK clinics, Delhi. I got a very good result and I surely recommend this clinic to all who are suffering from hair loss problems. Thanks to AK clinic for their hospitality and treatment... :) </p>
          </div>
          <div class="carousel-item text-center">
            <div class="img-box p-1 border rounded-circle m-auto"> <img class="d-block w-100 rounded-circle" src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="AK Clinics's transplant patient"> </div>
            <h5 class="mt-4"><strong class="text-uppercase">Mrinal Kumar</strong></h5>
            <p class="m-0 pt-2 text-white">The experience with AK clinics was very good , the doctors here are very well trained and do a marvelous job , the staff including the counselors , technicians and so on are very polite and helpful , the results are more than satisfactory and is one of the best places to trust when it comes to something like hair transplant.</p>
          </div>
          <div class="carousel-item text-center">
            <div class="img-box p-1 border rounded-circle m-auto"> <img class="d-block w-100 rounded-circle" src="<?= cdn('assets/template/frontend/images/'); ?>avtor.jpg" alt="AK Clinics's transplant patient"> </div>
            <h5 class="mt-4"><strong class="text-uppercase">Lochan singh</strong></h5>
            <p class="m-0 pt-2 text-white">AK Clinics is truly a fantastic clinic. Came here for FUE Hair Transplant. Doctor Dua is very knowledgeable and provided the best guidance with no sugar coating. The staff was excellent and takes care of the patient very well. The experience was wonderful and the doctor and the strand communicated on all aspects and ensured that they provide me all the post op information that I needed to know. Highly recommend AK Clinics.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="service-area">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12 bg-img cover-bg sm-height-550px xs-height-350px " data-background="<?= cdn('assets/template/frontend/img/') ?>d-3.png"  alt="Hair Transplant Clinics GK 1 Delhi" title="hair transplant & restoration Clinic" style="background-image:url(<?= cdn('assets/template/frontend/img/') ?>d-3.png);"></div>
      <div class="col-md-6 col-sm-12 col-xs-12 bg-dark pd-60 pl-sm10 pr-sm10">
        <div class="cent">
          <div class="section-head">
            <h4 class="text-muted">What Infrastructure is Available</h4>
          </div>
          <p class="pb-20 text-white">At AK Clinics, we have an infrastructure that is at par with any international clinic – from the interiors of the operation theatres to the equipment we utilise! We take pride in the fact that each of our treatment and procedure rooms is equipped to handle all our clients’ needs. All our rooms are extremely clean and all our procedures are done with hygiene being the utmost priority. After each procedure, the rooms are cleaned and sanitised and all the equipment is sterilised for the next procedure. We realise that our clients should be offered nothing but the best, which is why we constantly upgrade all our machines and equipment and at any given point of time, you can be sure that your treatment will be done using state of the art and truly modern equipment. </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="bg-light-gray">
  <?php $this->load->view('front/partials/delhi_the_team_section'); ?>
</div>
<div class="service-area ptb-3 ">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title text-center">
          <h2 class="black-text">Hotels Near AK Clinics Delhi</h2>
        </div>
      </div>
    </div>
    <div class="row text-left ptb-1">
      <div class="col-lg-3 col-md-3"> <strong>Where is the clinic Located</strong>
        <ul class="text-left">
          <li>Distance from Airport <strong>15 KM</strong></li>
          <li>Distance from New Delhi Railway Station <strong>13 KM</strong></li>
          <li>Distance from India Gate <strong>09 KM</strong></li>
          <li>Nearest Metro Station <strong>Kailash Colony</strong></li>
        </ul>
      </div>
      <div class="col-lg-9 col-md-9">
        <div class="row">
          <div class="col-md-4"> <img class="img-fluid rounded lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>hotel-img6.jpg" alt="Hotel Ivory 32">
            <h4 class="uppercase">Hotel Ivory 32</h4>
            <p>Situated almost at a walkable distance from the clinics is a decent budget hotel. <br>
              Prices range – INR 2700 – INR 3200</p>
          </div>
          <div class="col-md-4"> <img class="img-fluid rounded lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>hotel-img5.jpg" alt="Hotel Private Affair">
            <h4 class="uppercase">Hotel Private Affair</h4>
            <p>This is again a good quality budget hotel situated very close to the clinic.<br>
              Prices range – INR 2800 – INR 3500</p>
          </div>
          <div class="col-md-4"> <img class="img-fluid rounded lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>hotel-img4.jpg" alt="Hotel Eros">
            <h4 class="uppercase">Hotel Eros</h4>
            <p>It is a 5 Star luxury hotel in the business district of South Delhi which offers opulent facilities for both leisure and business travelers.<br>
              Prices range – INR 7000 – INR 12500</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  
</div>
<div class="service-area bg-light-gray">
<?php $this->load->view('front/partials/services_delhi'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Service",
  "name": "Hair Transplant Clinic in Delhi",
  "aggregateRating": {
	"@type": "AggregateRating",
	"ratingValue": "4.9",
	"reviewCount": "207"
  }
}
</script>