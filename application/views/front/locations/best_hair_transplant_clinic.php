<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active"> <img class="first-slide lazy" data-src="<?php echo base_url('assets/template/frontend/'); ?>img/slide-1.png" alt="First slide">
            <div class="container">
                <div class="carousel-caption text-center">
                    <h2>Another Example headline.</h2>
                    <p class="card-box-btn"><a class="btn btn-lg btn-outline" href="#" role="button">Sign up today</a></p>
                </div>
            </div>
        </div>
        <div class="carousel-item"> <img class="second-slide lazy" data-src="<?php echo base_url('assets/template/frontend/'); ?>img/slide-2.png" alt="Second slide">
            <div class="container">
                <div class="carousel-caption text-center">
                    <h2>Another example headline.</h2>
                    <p class="card-box-btn"><a class="btn btn-lg btn-outline" href="#" role="button">Learn more</a></p>
                </div>
            </div>
        </div>
        <div class="carousel-item"> <img class="third-slide lazy" data-src="<?php echo base_url('assets/template/frontend/'); ?>img/slide-3.png" alt="Third slide">
            <div class="container">
                <div class="carousel-caption text-center">
                    <h2>One more for good measure.</h2>
                    <p class="card-box-btn"><a class="btn btn-lg btn-outline" href="#" role="button">Browse gallery</a></p>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
<div class="orange-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p class="mg-0 text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
            </div>
            <div class="col-md-4 text-center">
                <button class="oranger-bar-btn">+91 977 9944 207</button>
            </div>
        </div>
    </div>
</div>
<div class="service-area">
    <div class="jumbotron">
        <div class="container text-center">
            <h1 class="black-text">AK Clinics Bangalore</h1>
            <p class="black-text">There are clinics that will offer you a handful of services, and then there is the best hair and skin clinic in Bangalore, where you can find every solution that your hair & skin might have been looking for. Whether you want your skin to look younger or to feel healthier or you are facing hair loss or baldness, AK Clinics in Bangalore is the best Hair & Skin clinic Bangalore that you will want to come to.</p>
        </div>
        <div class="container ">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 ">
                    <div class="card mb-3 box-shadow">
                        <img data-src="<?php echo base_url('assets/template/frontend/'); ?>img/service-4.png" class="img-fluid lazy" alt="smaple image">      
                        <div class="card-body text-center"> <strong>Hair Transplant</strong>
                            <p class="card-text">AK Clinics offers the complete services ranging from medical treat to surgical treatment to non-surgical treatment.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 ">
                    <div class="card mb-3 box-shadow"> <img class="card-img-top lazy" data-src="<?php echo base_url('assets/template/frontend/'); ?>img/service-1.png"  alt="Card image cap">
                        <div class="card-body  text-center"> <strong>Hair Restoration</strong>
                            <p class="card-text">AK Clinics offers the complete services ranging from medical treat to surgical treatment to non-surgical treatment.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline ">Read More <i class="icofont-long-arrow-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 ">
                    <div class="card mb-3 box-shadow"> <img class="card-img-top lazy" data-src="<?php echo base_url('assets/template/frontend/'); ?>img/service-2.png"  alt="Card image cap">
                        <div class="card-body  text-center"> <strong>Cosmetic Surgery</strong>
                            <p class="card-text">AK Clinics offers the complete services ranging from medical treat to surgical treatment to non-surgical treatment.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 ">
                    <div class="card mb-3 box-shadow"> <img class="card-img-top lazy" data-src="<?php echo base_url('assets/template/frontend/'); ?>img/service-3.png"  alt="Card image cap">
                        <div class="card-body  text-center"> <strong>Cosmetology</strong>
                            <p class="card-text">AK Clinics offers the complete services ranging from medical treat to surgical treatment to non-surgical treatment.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container ">
        <div class="row">
            <div class="col-md-3">
                df
            </div>
            <div class="col-md-6">
                <div class="section-title">
                    <h2 class="black-text">AK CLINICS - CENTER FOR WORLD CLASS</h2>
                    <p class="text-left">“AK Clinics was founded with the goal of transformation of looks of the entire human race. The purpose of existence of AK Clinics is to provide excellent treatments at an affordable cost.” </p>
                    <p class="text-left">This is not mere statement but a religion with us. Our team strives very hard to meet up with ever expanding needs & wishes of the patients. AK Clinics is amongst the fastest growing cosmetic surgery clinics having served thousands patients every year in our various branches. We’ve been working very hard to achieve excellence in the services we provide especially hair transplant. This is the reason all our <a href="#" target="_blank"  class="link">efforts & standards</a> are at par or better than the best in the world.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/services_bangalore'); ?>
<?php $this->load->view('front/partials/location_block'); ?>