<style>
.slick-slide{ text-align:center;}
.slick-slide p a{ font-size:14px;}
.slick-slide h3 span{  margin:10px 0; display:block;}
</style>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12 text-center">
            <div class="row">
                <div class="section-title ">
                    <h2 class="black-text">Our Services In hyderabad</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 kd-logo ptb-1">
            <div class="regular slider">
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/2.png" alt="Hair transplant in hyderabad" class="img-thumbnail lazy"> <p><a href="<?= base_url(); ?>hyderabad/best-hair-transplant-clinic/" target="_blank">Hair Transplant in Hyderabad</a></p> </div>
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/3.png" alt="Laser Hair Removal in Hyderabad" class="img-thumbnail lazy"><p> <a href="<?= base_url(); ?>hyderabad/laser-hair-removal-for-men-and-women/" target="_blank">Laser Hair Removal Treatment in Hyderabad</a> </p></div>
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/4.png" alt="Hair loss Treatment in Hyderabad" class="img-thumbnail lazy"> <p><a href="<?= base_url(); ?>hyderabad/hair-loss-treatment/" target="_blank">Hair Loss Treatment in Hyderabad </a></p> </div>
            </div>
        </div>
    </div>
</div>
