<div class="service-area  animated fadeInUp delay-7s">
    <div class="container-fluid">
        <div class="row">
            <div id="horizontalTab">
                <ul class="resp-tabs-list tab-ak">
                    <li> Blog </li>
                    <li> Case Study</li>
                    <li> News</li>
                </ul>
                <div class="resp-tabs-container">
                    <div>
                        <div class="container-fluid">
                            <div class="col-md-12">
                                 <div class="row">
                  <?php if(isset($data['all_blog']) && !empty($data['all_blog'])) : foreach ($data['all_blog'] as $k => $record) :  ?>
                  <div class="col-md-4 col-sm-12 ">
                    <div class="card mb-3 box-shadow">
                      <?php if($record['is_cron'] == 1) { ?>
                                                <?php if (!empty($record['image'])) { ?>
                        <img src="<?php echo cdn($record['image']); ?>" class="img-fluid lazy" alt="<?= $record['image_alt']; ?>">
                                                <?php } else { ?>
                        <img src="<?php echo cdn('assets/template/frontend/'); ?>img/ak-blog.png" class="img-fluid lazy" alt="AK Clinics">
                      <?php } } else { if (!empty($record['image'])) { ?>
                        <img src="<?php echo base_url() . $record['image']; ?>" class="img-fluid lazy" alt="<?= $record['image_alt']; ?>">
                                                <?php } else { ?>
                                                    <img src="<?php echo cdn($record['image']); ?>img/ak-blog.png" class="img-fluid lazy" alt="AK Clinics">
                      <?php } }?>
                      <div class="card-body text-center"> <strong class="txt-line">
                        <?= $record['title']; ?>
                        </strong>
                        <p class="card-text">
                          <?= $record['content']; ?>
                        </p>
                        <div class="d-flex justify-content-between align-items-center card-box-btn">
                          <div class="btn-group"> 
                            <a href="<?= $record['post_url']; ?>" target="_blank">
                                <button type="button" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i></button>
                            </a> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                  <?php endforeach; endif;?>
                        </div>
                    </div>
                        </div>
                    </div>
                    <div>
                        <div class="container-fluid">
                            <div class="col-md-12">
                                <div class="row">
                                    <?php if (isset($data['patient_blog']) && !empty($data['patient_blog'])) : foreach ($data['patient_blog'] as $k => $record) : ?>
                                    <div class="col-md-4 col-sm-12 ">
                                        <div class="card mb-3 box-shadow">
                                            <?php if ($record['is_cron'] == 1) { ?>
                                                <?php if (!empty($record['image'])) { ?>
                                                    <img src="<?php echo cdn($record['image']); ?>" class="img-fluid lazy" alt="<?= $record['image_alt']; ?>">
                                                <?php } else { ?>
                                                    <img src="<?php echo cdn('assets/template/frontend/'); ?>img/ak-blog.png" class="img-fluid lazy" alt="AK Clinics">
                                                <?php }
                                            } else {
                                                if (!empty($record['image'])) { ?>
                                                    <img src="<?php echo cdn($record['image']); ?>" class="img-fluid lazy" alt="<?= $record['image_alt']; ?>">
                                                        <?php } else { ?>
                                                    <img src="<?php echo cdn('assets/template/frontend/'); ?>img/ak-blog.png" class="img-fluid lazy" alt="AK Clinics">
                                                <?php } } ?>
                                            <div class="card-body text-center"> 
                                                <strong class="txt-line"><?= $record['title']; ?></strong>
                                                <p class="card-text">
                                                <?= $record['content']; ?>
                                                </p>
                                                <div class="d-flex justify-content-between align-items-center card-box-btn">
                                                    <div class="btn-group"> 
                                                        <a href="<?= $record['post_url']; ?>" target="_blank">
                                                            <button type="button" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i></button>
                                                        </a> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="container-fluid">
                            <div class="col-md-12">
                                <div class="row">
                                    <?php if (isset($data['success_stories_blog']) && !empty($data['success_stories_blog'])) : foreach ($data['success_stories_blog'] as $k => $record) : ?>
                                    <div class="col-md-4 col-sm-12 ">
                                        <div class="card mb-3 box-shadow">
                                            <?php if ($record['is_cron'] == 1) { ?>
                                                <?php if (!empty($record['image'])) { ?>
                                                    <img src="<?php echo cdn($record['image']); ?>" class="img-fluid lazy" alt="<?= $record['image_alt']; ?>">
                                                    <?php } else { ?>
                                                        <img src="<?php echo cdn('assets/template/frontend/'); ?>img/ak-blog.png" class="img-fluid lazy" alt="AK Clinics">
                                                        <?php }
                                                        } else {
                                                            if (!empty($record['image'])) { ?>
                                                    <img src="<?php echo cdn($record['image']); ?>" class="img-fluid lazy" alt="<?= $record['image_alt']; ?>">
                                                    <?php } else { ?>
                                                    <img src="<?php echo cdn('assets/template/frontend/'); ?>img/ak-blog.png" class="img-fluid lazy" alt="AK Clinics">
                                                    <?php } } ?>
                                            <div class="card-body text-center"> 
                                                <strong class="txt-line"> <?= $record['title']; ?></strong>
                                                <p class="card-text"><?= $record['content']; ?></p>
                                                <div class="d-flex justify-content-between align-items-center card-box-btn">
                                                    <div class="btn-group"> 
                                                        <a href="<?= $record['post_url']; ?>" target="_blank">
                                                            <button type="button" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i></button>
                                                        </a> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
