<style>
    .slick-slide{ text-align:center;}
    .slick-slide p a{ font-size:14px;}
    .slick-slide h3 span{  margin:10px 0; display:block;}
</style>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12 text-center">
            <div class="row">
                <div class="section-title ">
                    <h2 class="black-text">Our Services In Delhi</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="regular slider">
                <div> 
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/1.png" alt="Scalp Micropigmentation" class="img-thumbnail lazy"> 
                    <p>
                        <a href="<?= base_url(); ?>hair-restoration/scalp-micro-pigmentation-india/" target="_blank">Scalp Micropigmentation</a> 
                    </p>
                </div>
                <div> 
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/2.png" alt="Hair Transplant" class="img-thumbnail lazy"> 
                    <p>
                        <a href="<?= base_url(); ?>hair-transplant/" target="_blank">Hair Transplant</a>
                    </p> 
                </div>
                <div> 
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/3.png" alt="Laser Hair Removal Treatment" class="img-thumbnail lazy">
                    <p> 
                        <a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/" target="_blank">Laser Hair Removal Treatment</a> 
                    </p>
                </div>
                <div> 
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/4.png" alt="Hair Loss Treatment" class="img-thumbnail lazy"> 
                    <p>
                        <a href="<?= base_url(); ?>hair-loss-treatment-men-women/" target="_blank">Hair Loss Treatment</a>
                    </p> 
                </div>
                <div> 
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/5.png" alt="PRP Hair Treatment" class="img-thumbnail lazy"> 
                    <p>
                        <a href="<?= base_url(); ?>prp-treatment/" target="_parent">PRP Hair Treatment</a>
                    </p> 
                </div>
                <div> 
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/6.png" alt="Stretch Marks Removal" class="img-thumbnail lazy"> 
                    <p>
                        <a href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/" target="_blank">Stretch Marks Removal</a> 
                    </p>
                </div>
                <div> 
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/7.png" alt="Tattoo Removal" class="img-thumbnail lazy"> 
                    <p>
                        <a href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/" target="_blank">Tattoo Removal</a>
                    </p> 
                </div>
                <div> 
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/8.png" alt="Acne Scar Treatment" class="img-thumbnail lazy"> 
                    <p>
                        <a href="<?= base_url(); ?>cosmetology/acne-treatment/" target="_blank">Acne Scar Treatment</a></p> 
                </div>
                <div> 
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/9.png" alt="Pigmentation Treatment" class="img-thumbnail lazy"> 
                    <p>
                        <a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/" target="_blank">Pigmentation Treatment</a></p> 
                </div>
                <div> 
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/11.png" alt="Skin Polishing" class="img-thumbnail lazy"> 
                    <p>
                        <a href="<?= base_url(); ?>cosmetology/skin-polishing/" target="_blank">Skin Polishing</a>
                    </p> 
                </div>
            </div>
        </div>
    </div>
</div>
