<?php
$current_main_page = $this->uri->segment(1);
$enquire_for_value = 'Hair Transplant';
if($current_main_page == 'hair-transplant'){$enquire_for_value = 'Hair Transplant';}
else if($current_main_page == 'hair-loss'){$enquire_for_value = 'Hair Loss Treatment';}
else if($current_main_page == 'cosmetic-surgery'){$enquire_for_value = 'Cosmetic Surgery';}
else if($current_main_page == 'cosmetology'){$enquire_for_value = 'Cosmetology';}
?>
<style>
    .col-md-6 {float: left;}
    .modal-open .modal { overflow-x: hidden;    overflow-y: auto;    padding: 10px;}
    .modal .pop-emi {margin: 25px auto;width: 100% !important;max-width: 800px; }
    .modal-content{padding: 15px !important;}
    .form-control {height: 43px !important;border-radius: 0 !important;border: none;border-bottom: solid 1px #ccc;font-size: .70em;FONT-WEIGHT: 400;LETTER-SPACING: 2PX;text-transform: uppercase;}
    .uppercase-head{font-size: 1em;text-transform: uppercase;text-align: center;margin-top: 25px;}
    .model-blue {background: #069 !important;opacity: 1 !important;}
    .model-blue h2 strong {color: #ffffff !important;font-family: geomanist_regularregular !important;}
    .modal-header .close {margin-top: -2px;color: #fff !important;opacity: 1;}
    .checkbox-inline, .radio-inline {position: relative;display: inline-block;padding-left: 20px;margin-bottom: 0;font-weight: 400;vertical-align: middle;cursor: pointer;max-width: 100%;}
    .display-hide {display: none}
    /*button.popup-class{width: 25px;border-radius: 50%;background: #fff;height: 25px;position: absolute;right: -6px;top: 15px;z-index: 999;}*/
    button.popup-class{position: absolute;top: 0px;right: 5px;}
    .error-form {display: block;text-align: center;margin: 10px 0;color: #ff0000;}
    .success-form{display: block;text-align: center;margin: 10px 0;color: #57bf06;}
    .display-hide{ display:none;}
</style>
<div id="myModal" class="modal fade">
    <div class="modal-dialog pop-emi">
        <div class="modal-content" style="background:#fff !important;">
            <button class="close popup-class" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
            <div class="modal-body" style="background:none;">
                <div class="row">
                    <div class="col-md-12">
                        <form id="laserhairremoval" name="laserhairremoval">
                            <input style="display: none;" name="lp_url" type="hidden" value="https://akclinics.org/" />
                            <input style="display: none;" name="return_url" type="hidden" value="https://akclinics.org/thanks.html" />
                            <input style="display: none;" name="lead_source" type="hidden" value="9" />
                            <input style="display: none;" name="enquire_for" type="hidden" value="<?= $enquire_for_value; ?>" /> 
                            <input style="display: none;" name="submit_without_captcha" type="hidden" value="true" />
                            <div class="error-form display-hide">Failed !! please check required fields&#8230;.</div>
                            <div class="success-form display-hide">Thank you for submitting your details, Our patient advisor will contact you soon.</div>
                            <div class="col-md-6">
                                <div style="background:#ff9000;padding: 53px 20px;">
                                    <p class="uppercase-head" style="color:#fff; font-size:1em; letter-spacing: 2px;">Still Waiting?</p>
                                    <p class="center" style="font-size:1.5rem;text-align: center;color:#fff;margin-top: 10px !important;">Waiting won&#8217;t Help!!</p>
                                </div>
                                <p class="" style="font-size:1rem; text-align:center;">Get in touch with us today.<i class="icon icon-arrow1 smaller-lower"></i></p>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input class="form-control input-design" name="Name" required type="text" placeholder="Full Name *" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input class="form-control input-design" id="email" name="Email" required type="email" placeholder="Email*" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input class="form-control input-design" id="phone" name="Mobile" required type="tel" placeholder="Mobile*" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input class="form-control input-design" id="city" name="City" required type="text" placeholder="City*" >
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="button" name="submit" id="get_discount" class="btn btn-warning  btn-outline  margin-top-10">Get Free Consultation</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#get_discount").click(function () {
        var a = $(this);
        var iframegtag = '<iframe src="https://akclinics.org/ga_ht.html" style="width:0;height:0;border:0; border:none;"></iframe>';
        var b = $("form#laserhairremoval").serialize(),
            c = encodeURIComponent(window.location.href);
        b = b + "&page_url=" + c, $.ajax({
            url: "https://akclinics.org/submit_form.php",
            type: "post",
            data: b,
            success: function (a, b) {
                obj = JSON.parse(a), "failed" == obj.status && $(".error-form").show(), "success" == obj.status && ($(".success-form").append(iframegtag), $(".success-form").show(), $(".error-form,.form_fields").hide(), setTimeout(function () {
                    $("#myModal").modal("hide")
                }, 4e3))
            },
            error: function (a, b, c) {
                console.log(a), console.log("Details: " + b + "\nError:" + c)
            }
        })
    });
</script>