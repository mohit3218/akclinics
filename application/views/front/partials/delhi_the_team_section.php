<style>
    .slick-slide img {width: 100%;text-align: center;}
    .slick-slide h3 {font-size: 1rem;text-align: left;margin-bottom: 0;}
</style>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h2 class="black-text ">Best Hair Transplant Doctors in Delhi</h2>
                    <p>Meet our multi-talented & dedicated team of hair transplant surgeons & dermatologists in Delhi. Whether it’s Hair Transplant or any skin procedure, we are incomplete without our team because all procedures require team efforts. Milestones achieved by AK Clinics in a field of hair transplant only become possible due to this multidisciplinary team.  Our team has the potential to meet all your requirements and can face all challenges that often occur while surgery. Under the leadership of co-founders (Dr. Kapil Dua and Dr. Aman Dua), every member understands the vision & goals of AK Clinics, thus act accordingly. We have a very successful track record of serving more than 1000s patients in Delhi till now, and it was unmanageable without this hard working & competitive team.</p>
                </div>
            </div>
            <div class="col-md-12 pt-3">
                <div class="kd-logo">
                    <div class="regular slider">
                        <div> 
                            <a href="<?= base_url(); ?>about-us/our-team/dr-kapil-dua/" target="_blank" class="read-more"> 
                                <img data-src="<?= cdn('assets/template/frontend/images/'); ?>DrKapilDua.jpg" alt="Best hair transplant doctor in delhi" title="Dr. Kapil Dua" class="img-fluid lazy">
                                <h3>Dr. Kapil Dua</h3>
                                <p>Chairman & Chief Hair Transplant Surgeon</p>
                            </a> 
                        </div>

                        <div> 
                            <a href="<?= base_url(); ?>about-us/our-team/dr-aman-dua/" target="_blank" class="read-more"> 
                                <img data-src="<?= cdn('assets/template/frontend/images/'); ?>DrAmanDua.jpg" alt="Chief Dermatologist &amp; Hair Transplant Surgeon in Delhi" title="Dr. Aman Dua" class="img-fluid lazy">
                                <h3>Dr. Aman Dua</h3>
                                <p>Chief Dermatologist & Hair Transplant Surgeon</p>
                            </a> 
                        </div>

                        <div> 
                            <a href="<?= base_url(); ?>about-us/our-team/dr-shraddha-uprety/" target="_blank" class="read-more"> 
                                <img data-src="<?= cdn('assets/template/frontend/images/'); ?>dr-shraddha.png" alt="hair transplant doctor in Delhi" title="Dr. Shraddha Uprety" class="img-fluid lazy">
                                <h3>Dr. Shraddha Uprety</h3>
                                <p>Dermatologist & Hair Transplant Surgeon</p>
                            </a> 
                        </div>

                        <div> 
                            <a href="<?= base_url(); ?>about-us/our-team/dr-madhu-kuthial/" target="_blank" class="read-more"> 
                                <img data-src="<?= cdn('assets/template/frontend/images/'); ?>dr-madhu.png" alt="Hair Transplant surgeon in delhi" title="Dr Madhu kuthial" class="img-fluid lazy">
                                <h3>Dr. Madhu Kuthial</h3>
                                <p>Dermatologist & Hair Transplant Surgeon</p>
                            </a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
