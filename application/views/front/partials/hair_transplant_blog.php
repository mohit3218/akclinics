<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
          <div class="section-title  text-center ">
            <h2 class="black-text"> Blog</h2>
          </div>
        </div>
        <div class="row pt-1">
            <div class="col-md-12">
                <div class="regular slider">
                <?php if(isset($data['hair_loss_transplant']) && !empty($data['hair_loss_transplant'])) : 
                    foreach ($data['hair_loss_transplant'] as $k => $record) :  ?>
                    <div> 
                        <?php if (!empty($record['image'])) { ?>
                            <img data-src="<?php echo cdn($record['image']); ?>" class="img-fluid lazy">
                        <?php } else { ?>
                            <img data-src="<?php echo cdn('assets/template/frontend/'); ?>img/ak-blog.png" class="img-fluid lazy">
                        <?php } ?>
                        <a href="<?= $record['post_url']; ?>" target="_blank">
                            <h3><?= $record['title']; ?></h3>
                        </a>
                    </div>
                <?php endforeach; endif;?>
                </div>
            </div>
        </div>
    </div>
</div>
