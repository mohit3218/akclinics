<?php
$google_review_data = $this->home_model->get_google_review_record();

$ludhian_review = $delhi_review = $banglore_review = array();
foreach($google_review_data as $k => $google_review)
{
    if($google_review['id'] == 1)
    {
        $ludhian_review['name'] = $google_review['name'];
        $ludhian_review['rating'] = $google_review['rating'];
        $ludhian_review['review'] = $google_review['review'];
    }
    if($google_review['id'] == 2)
    {
        $delhi_review['name'] = $google_review['name'];
        $delhi_review['rating'] = $google_review['rating'];
        $delhi_review['review'] = $google_review['review'];
    }
    if($google_review['id'] == 3)
    {
        $banglore_review['name'] = $google_review['name'];
        $banglore_review['rating'] = $google_review['rating'];
        $banglore_review['review'] = $google_review['review'];
    }
}
$ldh_width = $delhi_width = $banglore_width = '';

if($ludhian_review['rating'] == 1){$ldh_width = '20%';}
else if($ludhian_review['rating'] == 1.5){$ldh_width = '30%';}
else if($ludhian_review['rating'] == 1.6){$ldh_width = '32%';}
else if($ludhian_review['rating'] == 1.7){$ldh_width = '34%';}
else if($ludhian_review['rating'] == 1.8){$ldh_width = '36%';}
else if($ludhian_review['rating'] == 1.9){$ldh_width = '39%';}
else if($ludhian_review['rating'] == 2){$ldh_width = '40%';}
else if($ludhian_review['rating'] == 2.5){$ldh_width = '50%';}
else if($ludhian_review['rating'] == 2.6){$ldh_width = '52%';}
else if($ludhian_review['rating'] == 2.7){$ldh_width = '54%';}
else if($ludhian_review['rating'] == 2.8){$ldh_width = '56%';}
else if($ludhian_review['rating'] == 2.9){$ldh_width = '59%';}
else if($ludhian_review['rating'] == 3){$ldh_width = '60%';}
else if($ludhian_review['rating'] == 3.1){$ldh_width = '62%';}
else if($ludhian_review['rating'] == 3.2){$ldh_width = '64%';}
else if($ludhian_review['rating'] == 3.3){$ldh_width = '66%';}
else if($ludhian_review['rating'] == 3.4){$ldh_width = '69%';}
else if($ludhian_review['rating'] == 3.5){$ldh_width = '70%';}
else if($ludhian_review['rating'] == 3.6){$ldh_width = '72%';}
else if($ludhian_review['rating'] == 3.7){$ldh_width = '74%';}
else if($ludhian_review['rating'] == 3.8){$ldh_width = '76%';}
else if($ludhian_review['rating'] == 3.9){$ldh_width = '79%';}
else if($ludhian_review['rating'] == 4){$ldh_width = '80%';}
else if($ludhian_review['rating'] == 4.1){$ldh_width = '82%';}
else if($ludhian_review['rating'] == 4.2){$ldh_width = '84%';}
else if($ludhian_review['rating'] == 4.3){$ldh_width = '86%';}
else if($ludhian_review['rating'] == 4.4){$ldh_width = '89%';}
else if($ludhian_review['rating'] == 4.5){$ldh_width = '90%';}
else if($ludhian_review['rating'] == 4.6){$ldh_width = '92%';}
else if($ludhian_review['rating'] == 4.7){$ldh_width = '94%';}
else if($ludhian_review['rating'] == 4.8){$ldh_width = '96%';}
else if($ludhian_review['rating'] == 4.9){$ldh_width = '99%';}
else if($ludhian_review['rating'] == 5){$ldh_width = '100%';}


if($delhi_review['rating'] == 1){$delhi_width = '20%';}
else if($delhi_review['rating'] == 1.5){$delhi_width = '30%';}
else if($delhi_review['rating'] == 1.6){$delhi_width = '32%';}
else if($delhi_review['rating'] == 1.7){$delhi_width = '34%';}
else if($delhi_review['rating'] == 1.8){$delhi_width = '36%';}
else if($delhi_review['rating'] == 1.9){$delhi_width = '39%';}
else if($delhi_review['rating'] == 2){$delhi_width = '40%';}
else if($delhi_review['rating'] == 2.5){$delhi_width = '50%';}
else if($delhi_review['rating'] == 2.6){$delhi_width = '52%';}
else if($delhi_review['rating'] == 2.7){$delhi_width = '54%';}
else if($delhi_review['rating'] == 2.8){$delhi_width = '56%';}
else if($delhi_review['rating'] == 2.9){$delhi_width = '59%';}
else if($delhi_review['rating'] == 3){$delhi_width = '60%';}
else if($delhi_review['rating'] == 3.1){$delhi_width = '62%';}
else if($delhi_review['rating'] == 3.2){$delhi_width = '64%';}
else if($delhi_review['rating'] == 3.3){$delhi_width = '66%';}
else if($delhi_review['rating'] == 3.4){$delhi_width = '69%';}
else if($delhi_review['rating'] == 3.5){$delhi_width = '70%';}
else if($delhi_review['rating'] == 3.6){$delhi_width = '72%';}
else if($delhi_review['rating'] == 3.7){$delhi_width = '74%';}
else if($delhi_review['rating'] == 3.8){$delhi_width = '76%';}
else if($delhi_review['rating'] == 3.9){$delhi_width = '79%';}
else if($delhi_review['rating'] == 4){$delhi_width = '80%';}
else if($delhi_review['rating'] == 4.1){$delhi_width = '82%';}
else if($delhi_review['rating'] == 4.2){$delhi_width = '84%';}
else if($delhi_review['rating'] == 4.3){$delhi_width = '86%';}
else if($delhi_review['rating'] == 4.4){$delhi_width = '89%';}
else if($delhi_review['rating'] == 4.5){$delhi_width = '90%';}
else if($delhi_review['rating'] == 4.6){$delhi_width = '92%';}
else if($delhi_review['rating'] == 4.7){$delhi_width = '94%';}
else if($delhi_review['rating'] == 4.8){$delhi_width = '96%';}
else if($delhi_review['rating'] == 4.9){$delhi_width = '99%';}
else if($delhi_review['rating'] == 5){$delhi_width = '100%';}


if($banglore_review['rating'] == 1){$banglore_width = '20%';}
else if($banglore_review['rating'] == 1.5){$banglore_width = '30%';}
else if($banglore_review['rating'] == 1.6){$banglore_width = '32%';}
else if($banglore_review['rating'] == 1.7){$banglore_width = '34%';}
else if($banglore_review['rating'] == 1.8){$banglore_width = '36%';}
else if($banglore_review['rating'] == 1.9){$banglore_width = '39%';}
else if($banglore_review['rating'] == 2){$banglore_width = '40%';}
else if($banglore_review['rating'] == 2.5){$banglore_width = '50%';}
else if($banglore_review['rating'] == 2.6){$banglore_width = '52%';}
else if($banglore_review['rating'] == 2.7){$banglore_width = '54%';}
else if($banglore_review['rating'] == 2.8){$banglore_width = '56%';}
else if($banglore_review['rating'] == 2.9){$banglore_width = '59%';}
else if($banglore_review['rating'] == 3){$banglore_width = '60%';}
else if($banglore_review['rating'] == 3.1){$banglore_width = '62%';}
else if($banglore_review['rating'] == 3.2){$banglore_width = '64%';}
else if($banglore_review['rating'] == 3.3){$banglore_width = '66%';}
else if($banglore_review['rating'] == 3.4){$banglore_width = '69%';}
else if($banglore_review['rating'] == 3.5){$banglore_width = '70%';}
else if($banglore_review['rating'] == 3.6){$banglore_width = '72%';}
else if($banglore_review['rating'] == 3.7){$banglore_width = '74%';}
else if($banglore_review['rating'] == 3.8){$banglore_width = '76%';}
else if($banglore_review['rating'] == 3.9){$banglore_width = '79%';}
else if($banglore_review['rating'] == 4){$banglore_width = '80%';}
else if($banglore_review['rating'] == 4.1){$banglore_width = '82%';}
else if($banglore_review['rating'] == 4.2){$banglore_width = '84%';}
else if($banglore_review['rating'] == 4.3){$banglore_width = '86%';}
else if($banglore_review['rating'] == 4.4){$banglore_width = '89%';}
else if($banglore_review['rating'] == 4.5){$banglore_width = '90%';}
else if($banglore_review['rating'] == 4.6){$banglore_width = '92%';}
else if($banglore_review['rating'] == 4.7){$banglore_width = '94%';}
else if($banglore_review['rating'] == 4.8){$banglore_width = '96%';}
else if($banglore_review['rating'] == 4.9){$banglore_width = '99%';}
else if($banglore_review['rating'] == 5){$banglore_width = '100%';}
?>
<div class="service-area ptb-3 fadeInDown delay-7s">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class=" black-text"> Clinic Locations</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 pt-1">
            <div class="row">
                <div class="col-md-4"> 
                    <a href="<?= base_url(); ?>locations/hair-transplant-ludhiana/" target="_blank" >
                        <div class="card mb-4 loc-box"> 
                            <img class="card-img-top lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/location-1.png"  alt="Ak Clinics, Ludhiana">
                            <div class="card-body  text-center">
                                <h3 class="loc-name">Kipps Market, Ludhiana</h3>
                            </div>
                            <div class="rating">
                                <span><i class="icofont-star icofont-2x"></i>Google Review</span>
                                <div class="rating-box"> 
                                    <div class="containerdiv">
                                        <div class="rating-div">
                                            <img class="rating-img lazy" data-src="<?= cdn('assets/template/frontend/'); ?>img/stars_blank.png" alt="rating">
                                        </div>
                                        <div class="cornerimage lazy" style="width:<?= $ldh_width; ?>">
                                            <img class="rating-img lazy" data-src="<?= cdn('assets/template/frontend/'); ?>img/stars_full.png" alt="rating">
                                        </div>
                                    </div>
                                </div>
                                <div class="score pull-left">Score<span><?= $ludhian_review['rating']; ?></span></div>
                                <div class="review pull-right">Review<span><?= $ludhian_review['review']; ?></span></div>
                            </div> 
                        </div>
                    </a> 
                </div>
                <div class="col-md-4"> 
                    <a href="<?= base_url(); ?>locations/hair-transplant-delhi/" target="_blank" >
                        <div class="card mb-4 loc-box"> 
                            <img class="card-img-top lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/location-2.png"  alt="Ak Clinics, Delhi">
                            <div class="card-body  text-center">
                                <h3 class="loc-name">Greater Kailash, Delhi</h3>
                            </div>
                            <div class="rating">
                                <span><i class="icofont-star icofont-1x"></i>Google Review</span>
                                <div class="rating-box"> 
                                    <div class="containerdiv">
                                        <div class="rating-div">
                                            <img class="rating-img lazy" data-src="<?= cdn('assets/template/frontend/'); ?>img/stars_blank.png" alt="rating">
                                        </div>
                                        <div class="cornerimage" style="width:<?= $delhi_width; ?>">
                                            <img class="rating-img lazy" data-src="<?= cdn('assets/template/frontend/'); ?>img/stars_full.png" alt="rating">
                                        </div>
                                    </div> 
                                </div>
                                <div class="score pull-left">Score<span><?= $delhi_review['rating']; ?></span></div>
                                <div class="review pull-right">Review<span><?= $delhi_review['review']; ?></span></div>
                            </div>
                        </div>
                    </a> 
                </div>
                <div class="col-md-4"> 
                    <a href="<?= base_url(); ?>bangalore" target="_blank" >
                        <div class="card mb-4 loc-box"> 
                            <img class="card-img-top lazy" data-src="<?php echo cdn('assets/template/frontend/'); ?>img/location-3.png"  alt="Ak Clinics, Bangalore">
                            <div class="card-body  text-center">
                                <h3 class="loc-name">Indiranagar, Bengaluru</h3>
                            </div>
                            <div class="rating">
                                <span><i class="icofont-star icofont-1x"></i>Google Review</span>
                                <div class="rating-box"> 
                                    <div class="containerdiv">
                                        <div class="rating-div">
                                            <img class="rating-img lazy" data-src="<?= cdn('assets/template/frontend/'); ?>img/stars_blank.png" alt="rating">
                                        </div>
                                        <div class="cornerimage" style="width:<?= $banglore_width; ?>;">
                                            <img class="rating-img lazy" data-src="<?= cdn('assets/template/frontend/'); ?>img/stars_full.png" alt="rating">
                                        </div>
                                    </div>
                                </div>
                                <div class="score pull-left">Score<span><?= $banglore_review['rating']; ?></span></div>
                                <div class="review pull-right">Review<span><?= $banglore_review['review']; ?></span></div>
                            </div>
                        </div>
                    </a> 
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .containerdiv {border: 0;float: left;position: absolute;width: 180px;top: 5px;right: 3px;} 
    .cornerimage {border: 0;position: absolute;top: 0;left: 0;overflow: hidden;} 
    .rating-box {position: relative;padding: 23px;}
    img.rating-img {    width: 180px !important;    min-height: auto;
}
</style>