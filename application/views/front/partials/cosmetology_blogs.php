<style>
.slick-slide img {height: 250px;}
.slick-slide h3 {font-size: 1.25rem;text-align: center !important;line-height: 29px;font-weight: 400;margin: 15px 0;}
</style>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
          <div class="section-title  text-center ">
            <h2 class="black-text"> Blog</h2>
          </div>
        </div>
        <div class="row pt-1">
            <div class="col-md-12">
                <div class="kd-logo">
                    <div class="regular slider">
                    <?php if(isset($data['cosmetology']) && !empty($data['cosmetology'])) : 
                        foreach ($data['cosmetology'] as $k => $record) :  ?>
                        <div> 
                            <?php if (!empty($record['image'])) { ?>
                                <img data-src="<?php echo cdn($record['image']); ?>" class="img-fluid lazy" alt="<?= $record['image_alt']; ?>">
                            <?php } else { ?>
                                <img data-src="<?php echo cdn('assets/template/frontend/'); ?>img/ak-blog.png" class="img-fluid lazy" alt="AK Clinics">
                            <?php } ?>
                            <a href="<?= $record['post_url']; ?>" target="_blank">
                                <h3><?= $record['title']; ?></h3>
                            </a>
                        </div>
                    <?php endforeach; endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>