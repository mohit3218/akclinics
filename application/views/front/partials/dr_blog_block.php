<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
          <div class="section-title  text-center ">
            <h2 class="black-text">Blog by <?= isset($dr_name) ? $dr_name: ''; ?></h2>
          </div>
        </div>
        <div class="row pt-1">
            <div class="col-md-12">
                <div class="kd-logo">
                    <div class="regular slider">
                    <?php if(isset($data['dr_blogs']) && !empty($data['dr_blogs'])) : 
                        foreach ($data['dr_blogs'] as $k => $record) :  ?>
                        <div> 
                            <?php if (!empty($record['image'])) { ?>
                                <img data-src="<?php echo base_url() . $record['image']; ?>" class="img-fluid lazy" alt="<?= $record['image_alt']; ?>">
                            <?php } else { ?>
                                <img data-src="<?php echo base_url('assets/template/frontend/'); ?>img/ak-blog.png" class="img-fluid lazy" alt="AK Clinics">
                            <?php } ?>
                            <a href="<?= $record['post_url']; ?>" target="_blank">
                                <h3><?= $record['title']; ?></h3>
                            </a>
                        </div>
                    <?php endforeach; endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>