<div class="header-cover ht-banner">
<div class="container">
    <div class="carousel-caption text-center">
        <ul>
            <li><i class="fa fa-check fa-1x"></i>India's Leading Hair Transplant Clinics</li>
            <li><i class="fa fa-check fa-1x"></i>Bio-FUE<sup>TM </sup> - our trademarked programme</li>
            <li><i class="fa fa-check fa-1x"></i>10 Years + experience in Hair Transplant</li>
            <li><i class="fa fa-check fa-1x"></i>Strictly as per international standards</li>
        </ul>
    </div>
</div></div>