<style>
    .slick-slide img {width: 100%;text-align: center;}
    .slick-slide h3 {font-size: 1rem;text-align: left;margin-bottom: 0;}
</style>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            
            <div class="col-md-12 ptb-1">
           
                    <div class="regular slider">
                        <div> 
                            <a href="<?= base_url(); ?>about-us/our-team/dr-kapil-dua/" target="_blank" class="read-more"> 
                                <img data-src="<?= cdn('assets/template/frontend/images/'); ?>DrKapilDua.jpg" class="img-fluid lazy" alt="Chairman & Chief Hair Transplant Surgeon Ak Clinics, hyderabad">
                                <h3>Dr. Kapil Dua</h3>
                                <p>Chairman & Chief Hair Transplant Surgeon</p>
                            </a> 
                        </div>
                        <div> 
                            <a href="<?= base_url(); ?>about-us/our-team/dr-aman-dua/" target="_blank" class="read-more"> 
                                <img data-src="<?= cdn('assets/template/frontend/images/'); ?>DrAmanDua.jpg" class="img-fluid lazy" alt="Chief Dermatologist & Hair Transplant Surgeon - AK Clinics, hyderabad">
                                <h3>Dr. Aman Dua</h3>
                                <p>Chief Dermatologist & Hair Transplant Surgeon</p>
                            </a> </div>
                        <div> 
                            <a href="<?= base_url(); ?>about-us/our-team/dr-roshan-kumar/" target="_blank" class="read-more"> 
                                <img data-src="<?= cdn('assets/template/frontend/images/'); ?>dr-roshan.png" class="img-fluid lazy" alt="Dermatologist & Hair Transplant Surgeon in hyderabad">
                                <h3>Dr. Roshan Kumar</h3>
                                <p>Dermatologist & Hair Transplant Surgeon</p>
                            </a> 
                        </div>
                        <div> 
                            <a href="<?= base_url(); ?>about-us/our-team/dr-shraddha-uprety/" target="_blank" class="read-more"> 
                                <img data-src="<?= cdn('assets/template/frontend/images/'); ?>dr-shraddha.png" class="img-fluid lazy" alt="Dermatologist & Hair Transplant Surgeon in hyderabad">
                                <h3>Dr. Shraddha Uprety</h3>
                                <p>Dermatologist & Hair Transplant Surgeon</p>
                            </a> 
                        </div>
                        <div> 
                            <a href="<?= base_url(); ?>about-us/our-team/dr-bhawna-bhardwaj/" target="_blank" class="read-more"> 
                                <img data-src="<?= cdn('assets/template/frontend/images/'); ?>dr-bhawna.png" class="img-fluid lazy" alt="Dermatologist & Hair Transplant Surgeon in hyderabad">
                                <h3>Dr. Bhawna Bhardwaj</h3>
                                <p>Dermatologist & Hair Transplant Surgeon</p>
                            </a> 
                        </div>
                        <div> 
                            <a href="<?= base_url(); ?>about-us/our-team/dr-madhu-kuthial/" target="_blank" class="read-more"> 
                                <img data-src="<?= cdn('assets/template/frontend/images/'); ?>dr-madhu.png" class="img-fluid lazy" alt="Dermatologist & Hair Transplant Surgeon in hyderabad">
                                <h3>Dr. Madhu Kuthial</h3>
                                <p>Dermatologist & Hair Transplant Surgeon</p>
                            </a> 
                        </div>
                        
                    </div>
            
            </div>
        </div>
    </div>
</div>
