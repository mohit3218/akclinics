<style>
    .slick-slide img {width: 100%;text-align: center;}
    .slick-slide h3 {font-size: 1rem;text-align: left;margin-bottom: 0;}
</style>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h2 class="black-text ">Visitor Doctors</h2>
                </div>
            </div>
            <div class="col-md-12 ptb-1">
                <div class="kd-logo">
                    <div class="regular slider">
                        <div> 
                            <a href="<?= base_url(); ?>about-us/our-team/dr-nirav-desai/" target="_blank" class="read-more"> 
                                <img data-src="<?= cdn('assets/template/frontend/images/'); ?>dr-nirav-passport.png" class="img-fluid lazy" alt="Dr Kapil Dua - Chairman & Chief Hair Transplant Surgeon">
                                <h3>Dr. Nirav Desai</h3>
                                <p>Aesthetic Dermatologist & Consultant Hair Transplant Surgeon</p>
                            </a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
