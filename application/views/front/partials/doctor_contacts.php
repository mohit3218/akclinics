<style>
.icon-sm{ float:right;}
.dr tr td{ font-size:15px;}

@media (min-width:320px) and (max-width:640px){
	.carousel {    margin-bottom: 0;    padding: 0 25px;}
.dr tr td{ font-size:13px;} 
} </style>
<?php
$current_main_page = $this->uri->segment(1);
$enquire_for_value = 'Hair Transplant';
if($current_main_page == 'hair-transplant'){$enquire_for_value = 'Hair Transplant';}
else if($current_main_page == 'hair-loss'){$enquire_for_value = 'Hair Loss Treatment';}
else if($current_main_page == 'cosmetic-surgery'){$enquire_for_value = 'Cosmetic Surgery';}
else if($current_main_page == 'cosmetology'){$enquire_for_value = 'Cosmetology';}

$page_url_for = base_url(uri_string());
?>

<div class="service-area ptb-3 bg-light-gray" id="contact-us">
    <div class="container bg-white">
        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-12 col-sm-6 padding-60 p0">
                    <h2 class="mg-top">Make An Appointment</h2>
                    <hr>
                    <form action="https://akclinics.org/submit_form_new.php" method="POST">
                        <input style="display: none;" name="lp_url" type="hidden" value="https://akclinics.org/">
                        <input style="display: none;" name="return_url" type="hidden" value="https://akclinics.org/thanks.html">
                        <input style="display: none;" name="lead_source" type="hidden" value="9">
                        <input style="display: none;" name="enquire_for" type="hidden" value="<?= $enquire_for_value; ?>">
                        <input style="display: none;" name="page_url" type="hidden" value="<?= $page_url_for; ?>">
                        <div class="form-group">
                            <input type="text" class="form-control" name="Name" id="name" placeholder="Full name*" required="required">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="Email" placeholder="Email*" id="email" required="required">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="Mobile" placeholder="Mobile*" id="email" required="required">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="City" placeholder="City*" id="email" required="required">
                        </div>
                        <div class="space-15"></div>
                        <button type="submit" name="submit" class="btn btn-sm btn-outline float-right">Get Free Consultation</button>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-6 padding-60 contact-details  p0">
                    <h2 class="mg-top">Contact Us</h2>
                    <hr>
                    <table class="table dr">
                        <tbody>
                            <tr>
                                <td><div class="icon-sm"><i class="fa fa-phone fa-1x" aria-hidden="true"></i></div></td>
                                <td><a href="tel:+919779944207"><i class="fa fa-phone fa-fw"></i>+91-97799-44207</a></td>
                            </tr>
                            <tr>
                                <td><div class="icon-sm"><i class="fa fa-envelope-o fa-1x"></i></div></td>
                                
                               
                                
                                <td class="orange-text"> <a href="mailto:info@akclinics.com?subject=Enquiry">info@akclinics.com</a></td>
                            </tr>
                            <tr>
                                <td><div class="icon-sm"><i class="fa fa-map-o fa-1x"></i></div></td>
                                <td>M-20, GK-I, Near M Block Market, Delhi</td>
                            </tr>
                            <tr>
                                <td><div class="icon-sm"><i class="fa fa-map-o fa-1x"></i></div></td>
                                <td>316, 100 Ft Rd, Indiranagar, Bengaluru</td>
                            </tr>
                            <tr>
                                <td><div class="icon-sm"><i class="fa fa-map-o fa-1x"></i></div></td>
                                <td>51-E, Sarabha Nagar, Ludhiana, Punjab</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="space-20"> </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var messageDelay = 2000;  
   $(function(){
      $('.apply-form').submit(function(e){
        var thisForm = $(this);
        e.preventDefault();
        $(this).fadeIn(function(){
          $("#loading").fadeIn(function(){
            $.ajax({
              type: 'POST',
              url: thisForm.attr("action"),
              data: thisForm.serialize(),
              success: function(data){
                $("#loading").fadeOut(function(){
                  $("#success").text(data).fadeIn().delay(messageDelay).fadeOut();
                });
              }
            });
          });
        });
      })
    });
</script>