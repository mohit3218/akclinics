<?php
$curret_url_slug = $this->uri->segment(1);

$about_active = $hair_transplant_active = $hair_loss_active = $hair_restoration_active = $cosmetic_surgery_active = $cosmetology_active = $results_active = $blog_active =$locations_active = $contact_us_active = '';
if($curret_url_slug == 'hair-transplant'){ $hair_transplant_active = 'active';}
else if($curret_url_slug == 'hair-loss'){ $hair_loss_active = 'active';}
else if($curret_url_slug == 'hair-restoration'){ $hair_restoration_active = 'active';}
else if($curret_url_slug == 'cosmetic-surgery'){ $cosmetic_surgery_active = 'active';}
else if($curret_url_slug == 'cosmetology'){ $cosmetology_active = 'active';}
else if($curret_url_slug == 'results'){ $results_active = 'active';}
else if($curret_url_slug == 'blog'){ $blog_active = 'active';}
else if($curret_url_slug == 'locations'){ $locations_active = 'active';}
else if($curret_url_slug == 'contact-us'){ $contact_us_active = 'active';}
else if($curret_url_slug == 'about-us'){ $about_active = 'active';}
?>
<header>
<?php
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (!preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) :
    ?>   
        <nav class="navbar navbar-expand-md navbar-light  fixed-top bg-light rounded"> 
            <a class="navbar-brand" href="/"><img src="<?php echo cdn('assets/template/frontend/'); ?>img/akclinics-logo.png" alt="AK Clinics logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item <?= $about_active; ?> dropdown"> 
                            <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>about-us/" aria-haspopup="true" aria-expanded="false" data-hover="dropdown">About Us</a>
                            <div class="dropdown-menu"> 
                                <a class="dropdown-item" href="<?= base_url(); ?>about-us/our-team/">Our Team</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant-training/">Hair Transplant Training</a> 
                            </div>
                        </li>

                        <li class="nav-item <?= $hair_transplant_active; ?> dropdown"> 
                            <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>hair-transplant/" aria-haspopup="true" aria-expanded="false">Hair Transplant</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown03"> 	
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/hair-transplant-in-men/">Hair Transplant In Men</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/hair-transplant-in-women/">Hair Transplant In Women</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/hair-transplant-techniques/">Hair Transplant Techniques</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/bio-fue/">Bio FUE</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Operative Care</a>
                            </div>
                        </li>
                        <li class="nav-item  <?= $hair_loss_active; ?>"> 
                            <a class="nav-link" href="<?= base_url(); ?>hair-loss/">Hair Loss</a> 
                        </li>
                        <li class="nav-item <?= $hair_restoration_active; ?> dropdown"> 
                            <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>hair-restoration/"  aria-haspopup="true" aria-expanded="false">Hair Restoration</a>
                            <div class="dropdown-menu"> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-gain-therapy/"> Hair Gain Therapy</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>prp-treatment/">PRP Hair Treatment</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>artificial-hair-restoration/">Artificial Hair Restoration</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-restoration/scalp-micro-pigmentation-india/">Scalp Micro Pigmentation</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>mesotherapy/">Mesotherapy</a> 
                            </div>
                        </li>
                        <li class="nav-item <?= $cosmetic_surgery_active; ?> dropdown"> 
                            <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>cosmetic-surgery/" aria-haspopup="true" aria-expanded="false"> Cosmetic Surgery</a>
                            <div class="dropdown-menu"> 
                                <a class="dropdown-item" href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>gynecomastia/">Gynecomastia</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Treatment</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>liposuction/">Liposuction</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>breast-reduction/">Breast Reduction</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynaecology</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a> 
                            </div>
                        </li>
                        <li class="nav-item <?= $cosmetology_active; ?> dropdown"> 
                            <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>cosmetology/" aria-haspopup="true" aria-expanded="false">Cosmetology</a>
                            <div class="dropdown-menu"> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser Hair Removal</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non Surgical Liposuction</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">Ultracel Skin Tightening</a> 
                            </div>
                        </li>
                        <li class="nav-item <?= $results_active; ?>"> 
                            <a class="nav-link" href="<?= base_url(); ?>results/">Result</a> 
                        </li>
                        <li class="nav-item <?= $blog_active; ?>"> 
                            <a class="nav-link" href="<?= base_url(); ?>blog/">Blog</a> 
                        </li>
                        <li class="nav-item  <?= $locations_active; ?> dropdown"> 
                            <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>locations/" aria-haspopup="true" aria-expanded="false">Our Location</a>
                            <div class="dropdown-menu"> 
                                <a class="dropdown-item" href="<?= base_url(); ?>locations/hair-transplant-ludhiana/">Ludhiana</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>locations/hair-transplant-delhi/">Delhi</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>bangalore/">Bangalore</a> 
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
<?php endif; ?>

<?php
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) :
    ?>        
        <nav class="navbar navbar-expand-md navbar-light  fixed-top bg-light rounded"> 
            <a class="navbar-brand" href="/"><img src="<?php echo cdn('assets/template/frontend/'); ?>img/akclinics-logo.png" alt="AK Clinics logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav ml-auto">
                        <div class="nav-item <?= $about_active; ?> dropdown show"> 
                            <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>about-us/" id="dropdownMenuLink"  data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">About Us</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"> 
                                <a class="dropdown-item" href="<?= base_url(); ?>about-us/">About Us</a>
                                <a class="dropdown-item" href="<?= base_url(); ?>about-us/our-team/">Our Team</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant-training/">Hair Transplant Training</a> 
                            </div>
                        </div>
                        <div class="nav-item <?= $hair_transplant_active; ?> dropdown show"> 
                            <a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" id="dropdownMenuLink"  aria-haspopup="true" aria-expanded="false">Hair Transplant</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a>	
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/hair-transplant-in-men/">Hair Transplant In Men</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/hair-transplant-in-women/">Hair Transplant In Women</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/hair-transplant-techniques/">Hair Transplant Techniques</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/bio-fue/">Bio FUE</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Operative Care</a>
                            </div>
                        </div>
                        <li class="nav-item  <?= $hair_loss_active; ?>"> 
                            <a class="nav-link" href="<?= base_url(); ?>hair-loss/">Hair Loss</a> 
                        </li>
                        <div class="nav-item <?= $hair_restoration_active; ?> dropdown show"> 
                            <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>hair-restoration/"  data-toggle="dropdown" id="dropdownMenuLink" aria-haspopup="true" aria-expanded="false">Hair Restoration</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"> 
                                <a class="dropdown-item" href="#">Hair Restoration</a>
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-gain-therapy/"> Hair Gain Therapy</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>prp-treatment/">PRP Hair Treatment</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>artificial-hair-restoration/">Artificial Hair Restoration</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>hair-restoration/scalp-micro-pigmentation-india/">Scalp Micro Pigmentation</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>mesotherapy/">Mesotherapy</a> 
                            </div>
                        </div>
                        <div class="nav-item <?= $cosmetic_surgery_active; ?> dropdown show"> 
                            <a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" id="dropdownMenuLink" aria-haspopup="true" aria-expanded="false"> Cosmetic Surgery</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetic-surgery/"> Cosmetic Surgery</a>
                                <a class="dropdown-item" href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>gynecomastia/">Gynecomastia</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Treatment</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>liposuction/">Liposuction</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>breast-reduction/">Breast Reduction</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynaecology</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a> 
                            </div>
                        </div>
                        <div class="nav-item <?= $cosmetology_active; ?> dropdown show"> 
                            <a class="nav-link dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">Cosmetology</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/">Cosmetology</a>
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser Hair Removal</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non Surgical Liposuction</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">Ultracel Skin Tightening</a> 
                            </div>
                        </div>
                        <li class="nav-item <?= $results_active; ?>"> 
                            <a class="nav-link" href="<?= base_url(); ?>results/">Result</a> 
                        </li>
                        <li class="nav-item <?= $blog_active; ?>"> 
                            <a class="nav-link" href="<?= base_url(); ?>blog/">Blog</a> 
                        </li>
                        <div class="nav-item  <?= $locations_active; ?> dropdown show"> 
                            <a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" id="dropdownMenuLink" aria-haspopup="true" aria-expanded="false">Our Location</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"> 
                                <a class="dropdown-item" href="<?= base_url(); ?>locations/">Our Location</a>
                                <a class="dropdown-item" href="<?= base_url(); ?>locations/hair-transplant-ludhiana/">Ludhiana</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>locations/hair-transplant-delhi/">Delhi</a> 
                                <a class="dropdown-item" href="<?= base_url(); ?>bangalore/">Bangalore</a> 
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
        </nav>
<?php endif; ?>
</header>