<style>.carousel-caption {z-index: 10;width: 50%;}</style>
<div class="header-cover restoration-banner">
	<div class="container">
		<div class="carousel-caption text-center">
			<h3 class="pt-2">Complete range of</h3>
			<h2 class="pb-2">Non-Surgical & surgical Hair Loss Treatments</h2>	   
			<a class=" btn-outline" href="#contact-us" role="button">Know More</a>
		</div>
	</div>
</div>