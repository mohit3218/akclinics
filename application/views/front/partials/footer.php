<style>
    .sticky-call-button {display: none;}
    .zopim { bottom: 0px;}
    @media (min-width:320px) and (max-width:767px)
    {
        .sticky-call-button {background:#333; opacity:0.95;bottom: 0;display: block;height: 40px;padding-top: 8px;position: fixed;text-align: center;width: 100%;z-index: 9999;}
        .sticky-call-button ul{ list-style:none; padding:0;}
        .sticky-call-button li {padding: 0px 25px;float: left;}
        .zopim{right: 4px !important;bottom: 4px !important;}
    }
</style>
<footer class="main-footer"> 
    <div itemscope itemtype="https://schema.org/MedicalClinic">
        <div class="bg-black p-2-tb ">
            <span itemprop="url" content="https://akclinics.org"></span>
            <span itemprop="logo" content="https://cdn.akclinics.org/wp-content/themes/AKclinics/images/logo-footer.png"></span>
            <span itemprop="image" content="https://cdn.akclinics.org/wp-content/themes/AKclinics/images/logo-footer.png"></span>
            <span itemprop="name" content="AK Clinics"></span>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 ">
                        <div class="font-weight-bold text-uppercase text-lg light-orange-color mb-3">
                            <a href="<?= base_url() ?>" itemprop="url" title="AK Clinics" rel="home">
                                <img class="lazy" itemprop="image" src="<?= cdn('assets/template/'); ?>uploads/2015/05/logo-footer.png" width="158" height="70" alt="Logo AK Clinics"/>
                            </a>
                        </div>
                        <p class="text-muted" itemprop="description">
                            AK Clinics- India most trusted & recommended hair transplant clinic in India run by Dr. Kapil Dua.
                        </p>
                        <a data-target="#bajajFinace" data-toggle="modal">
                            <img data-src="<?= cdn('assets/template/'); ?>uploads/2016/11/bajaj-emi.png" width="162px" class="ptb-1 lazy" alt="Bajaj EMI"/>
                        </a>
                        <span itemprop="priceRange" content="1000 - 5,00000"></span>
                        <span itemprop="paymentAccepted" content= "Cash, Visa, MasterCard, bank transfer"></span>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h6 class="text-uppercase light-orange-color mb-3">Quick Links</h6>
                        <ul class="list-unstyled">
                            <li class="p-1 text-muted"><i class="fa fa-check fa-1x" aria-hidden="true" ></i> <a href="<?= base_url(); ?>hair-loss/">Hair Loss Problems</a></li>
                            <li class="p-1 text-muted"><i class="fa fa-check fa-1x" aria-hidden="true"></i> <a href="<?= base_url(); ?>hair-transplant/">Hair Transplant</a></li>
                            <li class="p-1 text-muted"><i class="fa fa-check fa-1x" aria-hidden="true"></i> <a href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a></li>
                            <li class="p-1 text-muted"><i class="fa fa-check fa-1x" aria-hidden="true"></i> <a href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser Hair Removal</a></li>
                            <li class="p-1 text-muted"><i class="fa fa-check fa-1x" aria-hidden="true"></i> <a href="<?= base_url(); ?>hair-restoration/">Hair Restoration</a></li>

                        </ul>
                    </div>
                    <div class="col-lg-5 col-md-6">
                        <h6 class="text-uppercase light-orange-color mb-3">Contact Information</h6>
                        <ul class="list-unstyled">
                            <li class="text-muted p-1" itemprop="address"><i class="fa fa-address-card-o fa-1x" aria-hidden="true" ></i>  M-20, GK-I, Near M Block Market, Delhi</li>
                            <li class="text-muted p-1" itemprop="address"><i class="fa fa-address-card-o fa-1x" aria-hidden="true" ></i>  51-E, Sarabha Nagar, Ludhiana, Punjab</li>
                            <li class="text-muted p-1" itemprop="address"><i class="fa fa-address-card-o fa-1x" aria-hidden="true" ></i>  316, 100 Feet Rd, Indiranagar, Bengaluru</li>
                            <li class="text-muted orange-tex p-1" itemprop="telephone"><i class="fa fa-address-card-o fa-1x" aria-hidden="true" ></i>  +91-97799-44207</li>
                            <!-- <li class="ptb-1">   
                                 <a href="<?= base_url(); ?>contact-us/" class="oranger-bar-btn">Free Online Consultation</a>
                             </li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Copyright section of the footer-->
        <div class="ptb-1 font-weight-light bg-gray-800 text-gray-300">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-9 text-center text-md-left">
                        <p class="text-small">© 2018 AK Clinics, All rights reserved | <a href="<?= base_url(); ?>privacy-policy/">Privacy Policy</a></p>
                        <p class="text-small">Disclaimer: Results may vary patient to patient</p>

                    </div>
                    <div class="col-md-3">
                        <ul class="list-inline text-center text-md-right white-text social">
                            <li class="text-small float-left">Follow us here</li>
                            <li class="arrow-social"></li>
                            <li class="list-inline-item" itemprop="sameas"><a href="https://www.facebook.com/AKClinics" target="_blank"><i  class="fa fa-facebook-square fa-2x"></i></a></li>
                            <li class="list-inline-item" itemprop="sameas"><a href="https://twitter.com/AKClinics" target="_blank"><i class="fa fa-twitter-square fa-2x"></i></a></li>
                            <li class="list-inline-item" itemprop="sameas"><a href="https://www.linkedin.com/company/ak-clinics/" target="_blank"><i class="fa fa-linkedin-square fa-2x"></i></a></li>
                            <li class="list-inline-item" itemprop="sameas"><a href="https://www.youtube.com/user/TheFuehairtransplant" target="_blank"><i class="fa fa-youtube-square fa-2x"></i></a></li>
                            <li class="list-inline-item" itemprop="sameas"><a href="https://www.instagram.com/ak.clinics" target="_blank"><i class="fa fa-instagram fa-2x"></i></a></li>
                            <li class="list-inline-item" itemprop="sameas"><a href="https://plus.google.com/+Akclinics/posts?cfem=1" target="_blank"><i class="fa fa-fa-google-plus-square fa-2x"></i></a></li>
                        </ul>
                        <p><img class="lazy" src="<?= cdn('assets/template/'); ?>uploads/2015/06/cs-wh-3d-234x16.png" alt="copyscape"></p>
                    </div>
                </div>
            </div>
        </div>    

        <?php
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) :
            ?>
            <div class="col-md-12">
                <div class="row sticky-call-button">
                    <ul>
                        <li><a href="tel:+919779944207"><i class="fa fa-phone fa-2x orange-text" aria-hidden="true"></i></a></li>
                        <li><a href="mailto:info@akclinics.com"><i class="fa fa-envelope-o fa-2x orange-text" aria-hidden="true"></i></a></li>
                        <li><a href="https://akclinics.org/contact-us/"><i class="fa fa-location-arrow fa-2x orange-text" aria-hidden="true"></i></a></li>
                    <!--    <li> <a href="javascript:void($zopim.livechat.window.show())"><i class="icofont-ui-chat icofont-2x orange-text"></i></a> </li>-->

                    </ul>
                </div>
            </div>

        <?php endif; ?>
        <?php
        $current_page_slug = uri_string();
        if ($current_page_slug != 'about-us/our-team/dr-kapil-dua') {
            $this->load->view('front/partials/form_modal');
        }
        ?>
        <div  itemprop="Founders" itemscope  itemtype="https://schema.org/Person" >
            <span itemprop="name" content="Dr. Kapil Dua"></span>
            <span itemprop="jobTitle" content="Co-Founder & Chairman at AK Clinics"></span>
            <span itemprop="url" content="https://akclinics.org/about-us/our-team/dr-kapil-dua/"></span>
        </div>
        <div  itemprop="Founders" itemscope  itemtype="https://schema.org/Person">
            <span itemprop="name" content="Dr. Aman Dua"></span>
            <span itemprop="jobTitle" content="Co-Founder & Managing Director at AK Clinics"></span>
            <span itemprop="url" content="https://akclinics.org/about-us/our-team/dr-aman-dua/"></span>
        </div>
        <div  itemprop="employee" itemscope  itemtype="https://schema.org/Person">
            <span itemprop="name" content="Aman Bansal"></span>
            <span itemprop="jobTitle" content="CEO"></span>
            <span itemprop="url" content="https://in.linkedin.com/in/amankbansal/"></span>
        </div>
        <span itemprop="openingHours" content="Monday through Sunday, all day 10am - 7pm"></span>
    </div>

</footer>
<?php
$current_time = date('H:i');
?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-7675462-9"></script>
<script type="text/javascript">
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-7675462-9');

    var time_res = '<?= $current_time ?>';
    if ((time_res >= '19:30' && time_res <= '23:59') || (time_res >= '00:00' && time_res <= '09:30'))
    {
        (function (ng, a, g, e) {
            var l = document.createElement(g);
            l.async = 1;
            l.src = (ng + e);
            var c = a.getElementsByTagName(g)[0];
            c.parentNode.insertBefore(l, c);
        })('https://messenger.ngageics.com/ilnksrvr.aspx?websiteid=', document, 'script', '2-155-173-17-143-10-218-67');
    } else
    {
        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) {
                z._.push(c)
            }, $ = z.s =
                d.createElement(s), e = d.getElementsByTagName(s)[0];
            z.set = function (o) {
                z.set.
                    _.push(o)
            };
            z._ = [];
            z.set._ = [];
            $.async = !0;
            $.setAttribute('charset', 'utf-8');
            $.src = '//v2.zopim.com/?220YUT9hQPwPH76fhuRBaJptxYXW7gPK';
            z.t = +new Date;
            $.
                type = 'text/javascript';
            e.parentNode.insertBefore($, e)
        })(document, 'script');
    }
</script>

