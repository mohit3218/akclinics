<!doctype html>
<html amp lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
        <link rel="shortcut icon" href="<?php echo cdn('assets/template/'); ?>favicon.ico" />
        <?= (isset($meta) ? meta_tags($meta) : meta_tags()); ?>
        <script type='text/javascript' src='https://cdn.ampproject.org/v0.js' async></script>
        <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
        <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
        <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        <style amp-custom>alignright{float:right}.alignleft{float:left}.aligncenter{display:block;margin-left:auto;margin-right:auto}.amp-wp-enforced-sizes{max-width:100%;margin:0 auto}.amp-wp-unknown-size img{object-fit:contain}.amp-wp-content,.amp-wp-title-bar div{margin:0 auto;max-width:600px}html{background:#ff9000}body{background:#fff;color:#353535;font-family:'Merriweather','Times New Roman',Times,serif;font-weight:300;line-height:1.75em}p,ol,ul,figure{margin:0 0 1em;padding:0}a,a:visited{color:#ff9000}a:hover,a:active,a:focus{color:#353535}blockquote{color:#353535;background:rgba(127,127,127,.125);border-left:2px solid #ff9000;margin:8px 0 24px 0;padding:16px}blockquote p:last-child{margin-bottom:0}.amp-wp-meta,.amp-wp-header div,.amp-wp-title,.wp-caption-text,.amp-wp-tax-category,.amp-wp-tax-tag,.amp-wp-comments-link,.amp-wp-footer p,.back-to-top{font-family:-apple-system,BlinkMacSystemFont,"Segoe UI","Roboto","Oxygen-Sans","Ubuntu","Cantarell","Helvetica Neue",sans-serif}.amp-wp-header{background-color:#ff9000}.amp-wp-header div{color:#fff;font-size:1em;font-weight:400;margin:0 auto;max-width:calc(840px - 32px);padding:.875em 16px;position:relative}.amp-wp-header a{color:#fff;text-decoration:none}.amp-wp-header .amp-wp-site-icon{background-color:#fff;border:1px solid #fff;border-radius:50%;position:absolute;right:18px;top:10px}.amp-wp-article{color:#353535;font-weight:400;margin:1.5em auto;max-width:840px;overflow-wrap:break-word;word-wrap:break-word}.amp-wp-article-header{align-items:center;align-content:stretch;display:flex;flex-wrap:wrap;justify-content:space-between;margin:1.5em 16px 0}.amp-wp-title{color:#353535;display:block;flex:1 0 100%;font-weight:900;margin:0 0 .625em;width:100%}.amp-wp-meta{color:#696969;display:inline-block;flex:2 1 50%;font-size:.875em;line-height:1.5em;margin:0 0 1.5em;padding:0}.amp-wp-article-header .amp-wp-meta:last-of-type{text-align:right}.amp-wp-article-header .amp-wp-meta:first-of-type{text-align:left}.amp-wp-byline amp-img,.amp-wp-byline .amp-wp-author{display:inline-block;vertical-align:middle}.amp-wp-byline amp-img{border:1px solid #ff9000;border-radius:50%;position:relative;margin-right:6px}.amp-wp-posted-on{text-align:right}.amp-wp-article-featured-image{margin:0 0 1em}.amp-wp-article-featured-image amp-img{margin:0 auto}.amp-wp-article-featured-image.wp-caption .wp-caption-text{margin:0 18px}.amp-wp-article-content{margin:0 16px}.amp-wp-article-content ul,.amp-wp-article-content ol{margin-left:1em}.amp-wp-article-content amp-img{margin:0 auto}.amp-wp-article-content amp-img.alignright{margin:0 0 1em 16px}.amp-wp-article-content amp-img.alignleft{margin:0 16px 1em 0}.wp-caption{padding:0}.wp-caption.alignleft{margin-right:16px}.wp-caption.alignright{margin-left:16px}.wp-caption .wp-caption-text{border-bottom:1px solid #c2c2c2;color:#696969;font-size:.875em;line-height:1.5em;margin:0;padding:.66em 10px .75em}amp-carousel{background:#c2c2c2;margin:0 -16px 1.5em}amp-iframe,amp-youtube,amp-instagram,amp-vine{background:#c2c2c2;margin:0 -16px 1.5em}.amp-wp-article-content amp-carousel amp-img{border:none}amp-carousel>amp-img>img{object-fit:contain}.amp-wp-iframe-placeholder{background:#c2c2c2 url(https://cdn.akclinics.org/wp-content/plugins/amp/assets/images/placeholder-icon.png) no-repeat center 40%;background-size:48px 48px;min-height:48px}.amp-wp-article-footer .amp-wp-meta{display:block}.amp-wp-tax-category,.amp-wp-tax-tag{color:#696969;font-size:.875em;line-height:1.5em;margin:1.5em 16px}.amp-wp-comments-link{color:#696969;font-size:.875em;line-height:1.5em;text-align:center;margin:2.25em 0 1.5em}.amp-wp-comments-link a{border-style:solid;border-color:#c2c2c2;border-width:1px 1px 2px;border-radius:4px;background-color:transparent;color:#ff9000;cursor:pointer;display:block;font-size:14px;font-weight:600;line-height:18px;margin:0 auto;max-width:200px;padding:11px 16px;text-decoration:none;width:50%;-webkit-transition:background-color .2s ease;transition:background-color .2s ease}.amp-wp-footer{border-top:1px solid #c2c2c2;margin:calc(1.5em - 1px) 0 0}.amp-wp-footer div{margin:0 auto;max-width:calc(840px - 32px);position:relative}.amp-wp-footer h2{font-size:1em;line-height:1.375em;margin:0 0 .5em}.amp-wp-footer p{color:#696969;font-size:.8em;line-height:1.5em;margin:0 85px 0 0}.amp-wp-footer a{text-decoration:none}.back-to-top{bottom:1.275em;font-size:.8em;font-weight:600;line-height:2em;position:absolute;right:16px}.amp-wp-inline-b6883367306ea495916aaf8cfb4d3a65{background:#f4f4f4 none repeat scroll 0 0;border:1px solid #aaa;margin:30px 0}.amp-wp-inline-2fff4c4e01a78e962b44392877027338{padding-left:10px}.amp-wp-inline-56f6d43c01a18f10e8980fc022676bb6{color:#333;font-size:14px;line-height:35px}.amp-wp-inline-2f0158eb062d1ac553a7edcb8a744628{text-align:center}.amp-wp-inline-520cb464f049571bfce436c36ff0a043{display:inline-block}strong {clear: both;width: 100%;display: block;}.ak-social-icons{width: 100%;}.amp-wp-footer{position: fixed;bottom: -20px;width: 100%;background: #fff;padding-top: 20px;}.ak-social-icons > ul > li{list-style-type: none;display: inline;}</style>
    </head>
    <body>
        <header id="top" class="amp-wp-header">
            <div>
                <a href="<?= base_url(); ?>">
                    <span class="amp-site-title">
                        Ak Clinics			</span>
                </a>
            </div>
        </header>
        <article class="amp-wp-article">
            <?php echo $contents; ?>
        </article>
        <!-- FOOTER -->
        <footer class="amp-wp-footer">
            <!-- Main block - menus, subscribe form-->
            <div class="ak-social-icons">
                <ul>
                    <li>
                        <a href="http://www.facebook.com/sharer.php?u=<?= base_url(); ?>blog/<?= $slug_name ?>">
                            <amp-img src="<?= cdn('assets/template/frontend/img/') ?>fb.png" alt="Facebook This" height="40" width="40"></amp-img>
                        </a>
                    </li>
                    <li>
                        <a href="https://plus.google.com/share?url=<?= base_url(); ?>blog/<?= $slug_name ?>">
                            <amp-img src="<?= cdn('assets/template/frontend/img/') ?>gp.png" alt="Google Plus This" height="40" width="40"></amp-img>
                        </a>
                    </li>
                    <li>
                        <a href="http://twitter.com/share?text=<?= urlencode($title) ?>&url=<?= rawurlencode(base_url(uri_string() . '/')); ?>">
                            <amp-img src="<?= cdn('assets/template/frontend/img/') ?>tw.png" alt="Tweet This" height="40" width="40"></amp-img>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= base_url(); ?>blog/<?= $slug_name ?>">
                            <amp-img src="<?= cdn('assets/template/frontend/img/') ?>in.png" alt="Linkdin This" height="40" width="40"></amp-img>
                        </a>
                    </li>
                    <li>
                        <a href="#top" class="back-to-top">Back to top</a>
                    </li>
                </ul>
            </div>
        </footer>
        <amp-analytics type="googleanalytics">
        <script type="application/json">
            {
            "vars": {
            "account": "UA-7675462-9"
            },
            "triggers": {
            "trackPageview": {
            "on": "visible",
            "request": "pageview"
            }
            }
            }
        </script>
    </amp-analytics>
</body>
</html>