<style>.carousel-caption {z-index: 10;width: 50%;}</style>
<div class="header-cover hair-loss-banner">
	<div class="container">
		<div class="carousel-caption text-center">
			<h3 class="pt-2">Unmatched Results for</h3>
			<h2 class="pb-2">Hair Loss Treatment in Men & Women</h2>	   
			<a class=" btn-outline " href="#contact-us" role="button">Know More</a>
		</div>
	</div>
</div>