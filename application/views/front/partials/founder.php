<style>
    .slick-slide img {width: 100%;text-align: center;}
    .slick-slide h3 {font-size: 1rem;text-align: left;margin-bottom: 0;}
</style>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h2 class="black-text ">Meet The Team</h2>

                    <p>Our Passion – Quality, Trust & Results!!</p>
                </div>
            </div>
            <div class="col-md-12 pt-1">
                <p class="text-left pb-1">AK Clinicshas a team of highly trained doctors &amp; who are dedicated to field of hair restoration. The team is ably led by Dr. Kapil Dua MBBS, MS, along with Dr. Aman Dua, MBBS, MD and The team has permanent inhouse technicians who are well experienced. The aesthetic plastic surgery is support by Dr Dinkar Sood, MBBS, MS, MCh.</p>
                <p class="text-left">The clinic has its own training program for new team members where they are trained over models for a minimum of 6 months before being allowed to work on patients. We have a seasoned team of hair transplant counselors and relationship managers ably led by our CEO Mr. Aman Bansal.</p>
            </div>
        </div>
        <div class="col-md-12 ptb-3">
            <div class="kd-logo">
                <div class="regular slider">
                    <div> 
                        <a href="<?= base_url(); ?>about-us/our-team/dr-kapil-dua/" target="_blank" class="read-more"> 
                            <img data-src="<?= cdn('assets/template/frontend/images/'); ?>DrKapilDua.jpg" class="img-fluid lazy" alt="Dr Kapil Dua - Co-Founder AK Clinics">
                            <h3>Dr. Kapil Dua</h3>
                            <p>Chairman & Chief Hair Transplant Surgeon</p>
                        </a> 
                    </div>
                    <div> 
                        <a href="<?= base_url(); ?>about-us/our-team/dr-aman-dua/" target="_blank" class="read-more"> 
                            <img data-src="<?= cdn('assets/template/frontend/images/'); ?>DrAmanDua.jpg" class="img-fluid lazy" alt="Dr Aman Dua - Co-Founder AK Clinics">
                            <h3>Dr. Aman Dua</h3>
                            <p>Chief Dermatologist & Hair Transplant Surgeon</p>
                        </a> 
                    </div>
                    <a  href="https://www.linkedin.com/in/amankbansal" target="_blank" class="read-more"> 
                        <img data-src="<?= cdn('assets/template/frontend/images/'); ?>ab.jpg" class="img-fluid lazy" alt="Mr. Aman Bansal - CEO AK Clinics">
                        <h3>Mr. Aman Bansal</h3>
                        <p>As CEO of AK Clinics, Aman coordinates process, delivery of treatments and growth plans.</p>
                    </a> 
                </div>
            </div>
        </div>
    </div>
</div>