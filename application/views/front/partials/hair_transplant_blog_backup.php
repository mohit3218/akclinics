<div class="service-area">
    <div class="container-fluid">
        <div class="row">
            <div id="horizontalTab">
                <ul class="resp-tabs-list tab-ak">
                    <li class=""> Blog </li> 
                </ul>
                <div class="resp-tabs-container">
                    <div>
                        <div class="container">
                            <div class="resCarousel" data-items="1-4-4-4" data-interval="2000" data-slide="1" data-animator="lazy">
                                <div class="resCarousel-inner">
                                    <?php if(isset($data['hair_loss_transplant']) && !empty($data['hair_loss_transplant'])) : 
                                        foreach ($data['hair_loss_transplant'] as $k => $record) :  ?>
                                        <div class="item">
                                            <div class="tile">
                                                <div>
                                                    <figure>
                                                        <?php if (!empty($record['image'])) { ?>
                                                            <img src="<?php echo base_url() . $record['image']; ?>" class="img-fluid">
                                                        <?php } else { ?>
                                                            <img src="<?php echo cdn('assets/template/frontend/'); ?>img/ak-blog.png" class="img-fluid">
                                                        <?php } ?>
                                                    </figure>
                                                </div>
                                                <h3><?= $record['title']; ?></h3>
                                                <p><?= $record['content']; ?></p>
                                                <a href="<?= $record['post_url']; ?>" target="_blank" class="read-more">
                                                    <button type="button" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i></button>
                                                </a>
                                            </div>
                                        </div>
                                    <?php endforeach; endif;?>
                                </div>
                                <button class='btn btn-default leftRs'><i class="icofont-long-arrow-left icofont-1x"></i></button>
                                <button class='btn btn-default rightRs'><i class="icofont-long-arrow-right icofont-1x"></i></button>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
          <div class="section-title  text-center ">
            <h2 class="black-text"> Blog</h2>
          </div>
        </div>
        <div class="row pt-1">
            <div class="col-md-12">
                <div class="kd-logo">
                    <div class="regular slider">
                    <?php if(isset($data['hair_loss_transplant']) && !empty($data['hair_loss_transplant'])) : 
                        foreach ($data['hair_loss_transplant'] as $k => $record) :  ?>
                        <div> 
                            <?php if (!empty($record['image'])) { ?>
                                <img src="<?php echo base_url() . $record['image']; ?>" class="img-fluid">
                            <?php } else { ?>
                                <img src="<?php echo cdn('assets/template/frontend/'); ?>img/ak-blog.png" class="img-fluid">
                            <?php } ?>
                            <h3><?= $record['title']; ?></h3>
                        </div>
                    <?php endforeach; endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    
    $(document).on('ready', function() {
      $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1
      });
    });
</script>