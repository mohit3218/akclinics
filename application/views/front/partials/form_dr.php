<style>
    .popup-team-img{border-radius:100%;height:auto;margin:0 auto;overflow:hidden;position:relative;top:-70px;transition:all .3s ease 0;width:25%;z-index:2;border:2px solid #fcefeb}
    .modal-open .modal{overflow-x:hidden;overflow-y:auto;padding:10px}
    .modal .pop-emi{margin:25px auto;width:100%!important;max-width:800px}
    .form-control{height:43px!important;border-radius:0!important;border:none;border-bottom:solid 1px #ccc;font-size:.7em;FONT-WEIGHT:400;LETTER-SPACING:1.78PX;text-transform:uppercase;background-color:transparent}
    .modal-header .close{margin-top:-2px;color:#fff!important;opacity:1;position:absolute;z-index:9}
    .error-form,.success-form{display:block;text-align:center;margin:10px 0}
    .form_fields{padding:25px}
    .model-white{background:#fff!important;opacity:1!important;height:45px;border-bottom:none}
    .modal-content{padding:15px!important;background:#fcefeb}.form-group{width:100%}
    button.popup-class{width:25px;border-radius:50%;background:#fff;height:25px;position:absolute;right:-6px;top:15px;z-index:999}
    .error-form{color:red}
    .success-form{color:#57bf06}
    .display-hide{display:none}
    button.close{width:10%;position:absolute;right:0}
    .btn-outline:hover{background:#fbe0d7;border:2px solid #333}
	
	
	@media (min-width:320px) and (max-width:640px){
	.popup-team-img {
    top: -50px;
    width: 35%;
  
}
	
	
	}
	
</style>
<div id="myModal" class="modal fade" style="top:100px!important;">
    <div class="modal-dialog pop-emi-1">
        <div class="modal-content"><button class="close" type="button" data-dismiss="modal">×</button>
            <div class="modal-header model-white ">
                <div class="popup-team-img"> 
                    <img class="img-fluid transform" alt="Dr Kapil Dua - Chairman & Chief Hair Transplant Surgeon" src="<?= cdn('assets/template/frontend/') ?>images/DrKapilDua.jpg">
                </div>
            </div>
            <div class="modal-body" style="background:rgba(255,255,255,1);">
                <div class="cpl-md-12">
                    <form id="drkapildua" name="drkapildua">
                        <input style="display: none;" name="lp_url" type="hidden" value="https://akclinics.org/" />
                        <input style="display: none;" name="return_url" type="hidden" value="https://akclinics.org/thanks.html" />
                        <input style="display: none;" name="lead_source" type="hidden" value="78" />
                        <div class="error-form display-hide">Failed !! please check required fileds....</div>
                        <div class="success-form display-hide center">Thank you for submitting your details, Our patient advisor will contact you soon.</div>
                        <div class="row form_fields">
                            <div class="form-group">
                                <input class="form-control" name="Name" required type="text" placeholder="Full Name *" >
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="email" name="Email" required type="email" placeholder="Email*" >
                            </div>
                            <div class="form-group">
                                <input id="phone" class="form-control" name="Mobile" required="" type="tel" placeholder="Mobile*">
                            </div>
                            <div class="form-group">
                                <select id="clientselection" title="I would Like To" name="City" class="form-control">
                                    <option selected="selected">- Clinic location -</option>
                                    <option value="Ludhiana">Ludhiana</option>
                                    <option value="Delhi">New Delhi</option>
                                    <option value="Bangalore">Bangalore</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="clientselection" title="I would Like To" required=""  name="enquire_for" class="form-control">
                                    <option selected="selected">- Please Select our Services -</option>
                                    <option value="Hair Transplant" selected=""> Hair Transplant</option>
                                    <option value="Beard Hair Transplant">Beard Hair Transplant</option>
                                </select>
                                <input name="Country" type="hidden" value="India" />
                                <input name="submit_without_captcha" type="hidden" value="withoutcaptcha" />
                            </div>
                            <div class="text-center" style="width:100%;">
                                <button type="button" name="submit" id="get_discount_submit" class="btn btn-warning  btn-outline">Get Free Consultation</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#get_discount_submit").click(function () {
        var a = $(this);
        var iframegtag = '<iframe src="https://akclinics.org/ga_ht.html" style="width:0;height:0;border:0; border:none;"></iframe>';
        var b = $("form#drkapildua").serialize(),
            c = encodeURIComponent(window.location.href);
        b = b + "&page_url=" + c, $.ajax({
            url: "https://akclinics.org/submit_form.php",
            type: "post",
            data: b,
            success: function (a, b) {
                obj = JSON.parse(a), "failed" == obj.status && $(".error-form").show(), "success" == obj.status && ($(".success-form").append(iframegtag), $(".success-form").show(), $(".error-form,.form_fields").hide(), setTimeout(function () {
                    $("#myModal").modal("hide")
                }, 4e3))
            },
            error: function (a, b, c) {
                console.log(a), console.log("Details: " + b + "\nError:" + c)
            }
        })
    });
</script>