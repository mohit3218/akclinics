<style>.carousel-caption {padding-top: 110px;}</style>
<div class="header-cover skin-banner">
	<div class="container">
		<div class="carousel-caption">
			<h3 class="pt-2">Providing world class</h3>
			<h2>Skin care treatments & procedures</h2>
			<h3>with experienced Dermatologists & </h3>
			<h3 class="pb-2">Excellent Equipment</h3>	   
			<a class=" btn-outline" href="#contact-us" role="button">Know More</a>
        </div>
    </div>
</div>