<style>@media (max-width: 768px) and (min-width: 320px){
.carousel-caption {     width: 75%; }.header-cover{ height:190px;    background-position: -90px 0;}}
</style>
<div class="header-cover  ht-banner">
    <div class="container">
        <div class="carousel-caption">
            <p class="banner-font-medium">Bio-FUE<sup>TM </sup> Hair Transplant </p>
            <p class="banner-font-big orange-text ">Most Successful treatment for Baldness</p>	
            <span class="banner-font-medium">for Men & Women</span><br />
            <a class=" btn-outline " href="#contact-us" role="button">Know More</a>
        </div>
    </div>
</div>