<!doctype html>
<html lang="en">
	
    <head>
        <?php $this->load->view('front/partials/head'); ?>
    </head>
	
    <body>
        <?php $this->load->view('front/partials/header'); ?>
        <main>
            <?php echo $contents; ?>
            <!-- /.container --> 
        </main>
        <!-- FOOTER -->
        <?php $this->load->view('front/partials/footer'); ?>
        <!-- Bootstrap core JavaScript
            ================================================== --> 
        <!-- Placed at the end of the document so the pages load faster --> 
		<script src="<?php echo cdn('assets/template/frontend/'); ?>js/jquery-3.2.1.slim.min.js"></script> 
		<script src="<?php echo cdn('assets/template/frontend/'); ?>js/jquery.js"></script>
		<script src="<?php echo cdn('assets/template/frontend/'); ?>js/bootstrap.min.js"></script> 
        <script src="<?php echo cdn('assets/template/frontend/'); ?>js/holder.min.js"></script> 
        <script src="<?php echo cdn('assets/template/frontend/'); ?>js/popper.min.js"></script> 
		<script src="<?php echo cdn('assets/template/frontend/'); ?>js/slick.js"></script> 
        <script src="<?php echo cdn('assets/template/frontend/'); ?>js/custom.js"></script> 
        <script src="<?php echo cdn('assets/template/frontend/'); ?>js/lazyload.min.js"></script>
        <script type="text/javascript">
			setTimeout(function(){$("#myModal").modal();}, 15000);
            
            (function () {
                function logElementEvent(eventName, element) {
                    console.log(Date.now(), eventName, element.getAttribute("data-src"));
                }
                ll = new LazyLoad({
                    elements_selector: '.lazy',
                    load_delay: 300,
                    threshold: 0,
                    callback_enter: function (element) {
                        logElementEvent("ENTERED", element);
                    },
                    callback_load: function (element) {
                        logElementEvent("LOADED", element);
                    },
                    callback_set: function (element) {
                        logElementEvent("SET", element);
                    },
                    callback_error: function (element) {
                        logElementEvent("ERROR", element);
                        element.src = "https://placeholdit.imgix.net/~text?txtsize=21&txt=Fallback%20image&w=220&h=280";
                    }
                });
            }());
        </script>
		<script id="dsq-count-scr" src="//akclinic.disqus.com/count.js" async></script>
    </body>
</html>