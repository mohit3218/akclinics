<style>
.slick-slide{ text-align:center;}
.slick-slide p a{ font-size:14px;}
.slick-slide h3 span{  margin:10px 0; display:block;}
</style>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12 text-center">
            <div class="row">
                <div class="section-title ">
                    <h2 class="black-text">Our Services In Bangalore</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 kd-logo ptb-1">
            <div class="regular slider">
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/1.png" alt="Anagen Phase" class="img-thumbnail lazy"> <p><a href="<?= base_url(); ?>bangalore/scalp-micro-pigmentation/" target="_blank">Scalp Micropigmentation in Bangalore</a> </p></div>
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/2.png" alt="Anagen Phase" class="img-thumbnail lazy"> <p><a href="<?= base_url(); ?>bangalore/best-hair-transplant-clinic/" target="_blank">Hair Transplant in Bangalore</a></p> </div>
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/3.png" alt="Anagen Phase" class="img-thumbnail lazy"><p> <a href="<?= base_url(); ?>bangalore/laser-hair-removal-for-men-and-women/" target="_blank">Laser Hair Removal Treatment in Bangalore</a> </p></div>
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/4.png" alt="Anagen Phase" class="img-thumbnail lazy"> <p><a href="<?= base_url(); ?>bangalore/hair-loss-treatment/" target="_blank">Hair Loss Treatment in Bangalore </a></p> </div>
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/5.png" alt="Anagen Phase" class="img-thumbnail lazy"> <p><a href="<?= base_url(); ?>bangalore/prp-hair-loss-treatment/" target="_parent">PRP Hair Treatment in Bangalore</a></p> </div>
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/6.png" alt="Anagen Phase" class="img-thumbnail lazy"> <p><a href="<?= base_url(); ?>bangalore/stretch-marks-removal-treatment/" target="_blank">Stretch Marks Removal in Bangalore</a> </p></div>
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/7.png" alt="Anagen Phase" class="img-thumbnail lazy"> <p><a href="<?= base_url(); ?>bangalore/laser-permanent-tattoo-removal/" target="_blank">Tattoo Removal in Bangalore</a></p> </div>
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/8.png" alt="Anagen Phase" class="img-thumbnail lazy"> <p><a href="<?= base_url(); ?>bangalore/laser-acne-scar-removal-treatment/" target="_blank">Acne Scar Treatment in Bangalore</a></p> </div>
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/9.png" alt="Anagen Phase" class="img-thumbnail lazy"> <p><a href="<?= base_url(); ?>bangalore/pigmentation-treatment/" target="_blank">Pigmentation Treatment in Bangalore</a></p> </div>
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/10.png" alt="Anagen Phase" class="img-thumbnail lazy"> <p><a href="<?= base_url(); ?>bangalore/laser-skin-treatment/" target="_blank">Laser Skin Treatment in Bangalore</a> </p></div>
                <div> <img data-src="<?= cdn('assets/template/uploads/'); ?>2018/10/11.png" alt="Anagen Phase" class="img-thumbnail lazy"> <p><a href="<?= base_url(); ?>bangalore/skin-whitening-lightening-treatment/" target="_blank">Skin Whitening Treatment in Bangalore</a></p> </div>
            </div>
        </div>
    </div>
</div>
