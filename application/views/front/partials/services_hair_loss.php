<div class="container mt-5 display-none">
    <div class="row">
        <div class="col-md-3"> 
            <!-- Featured Box -->
            <div class="" style="cursor: pointer;" onclick="location.href = '/contact-us';">
                <div class="circle"><i class="icofont-hospital" style="font-size:20px;"></i><span></span></div>
                <div class="featured-desc">
                    <h3>Online Consultation Form</h3>
                    <p class="text-small">Start your hair loss just by filling this form.</p>
                </div>
            </div>
        </div>
        <div class="col-md-3"> 
            <!-- Featured Box -->
            <div class="" style="cursor: pointer;" onclick="location.href = '/contact-us';">
                <div class="circle"><i class="icofont-doctor" style="font-size:20px;"></i><span></span></div>
                <div class="featured-desc">
                    <h3>Expert<br>
                        Evaluation</h3>
                    <p class="text-small">Send your photos in order to be analyzed by experts.</p>
                </div>
            </div>
        </div>
        <div class="col-md-3"> 
            <!-- Featured Box -->
            <div class="" style="cursor: pointer;" onclick="location.href = '/contact-us';">
                <div class="circle"><i class="icofont-headphone" style="font-size:20px;"></i><span></span></div>
                <div class="featured-desc">
                    <h3>We Call You<br>
                        Back</h3>
                    <p class="text-small">Leave us your number and we will call you back.</p>
                </div>
            </div>
        </div>
        <div class="col-md-3"> 
            <!-- Featured Box -->
            <div class="" style="cursor: pointer;" onclick="location.href = '/contact-us';">
                <div class="circle"><i class="icofont-question-circle" style="font-size:20px;"></i><span></span></div>
                <div class="featured-desc">
                    <h3>Frequently Asked<br>
                        Questions</h3>
                    <p class="text-small">All questions and answers for pre- and post-hair transplantation.</p>
                </div>
            </div>
        </div>
    </div>
</div>