<?php
$curret_url_slug = $this->uri->segment(1);

$about_active = $hair_transplant_active = $hair_loss_active = $hair_restoration_active = $cosmetic_surgery_active = $cosmetology_active = $results_active = $blog_active =$locations_active = $contact_us_active = '';
if($curret_url_slug == 'hair-transplant'){ $hair_transplant_active = 'active';}
else if($curret_url_slug == 'hair-loss'){ $hair_loss_active = 'active';}
else if($curret_url_slug == 'hair-restoration'){ $hair_restoration_active = 'active';}
else if($curret_url_slug == 'cosmetic-surgery'){ $cosmetic_surgery_active = 'active';}
else if($curret_url_slug == 'cosmetology'){ $cosmetology_active = 'active';}
else if($curret_url_slug == 'results'){ $results_active = 'active';}
else if($curret_url_slug == 'blog'){ $blog_active = 'active';}
else if($curret_url_slug == 'locations'){ $locations_active = 'active';}
else if($curret_url_slug == 'contact-us'){ $contact_us_active = 'active';}
else if($curret_url_slug == 'about-us'){ $about_active = 'active';}
?>


<header>
    <nav class="navbar navbar-expand-md navbar-light  fixed-top bg-light rounded"> 
    <div class="container" >   <a class="navbar-brand" href="/"><img src="<?php echo base_url('assets/template/frontend/'); ?>img/akclinics-logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item <?= $about_active;?> dropdown"> 
                    <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>about-us/" id="dropdown03" aria-haspopup="true" aria-expanded="false">About Us</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown03"> 
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant-training/">Hair Transplant Training</a> 
                    </div>
                </li>
                <li class="nav-item <?= $hair_transplant_active;?> dropdown"> 
                    <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>hair-transplant/" id="dropdown03" aria-haspopup="true" aria-expanded="false">Hair Transplant</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown03"> 	
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/hair-transplant-cost/">Hair Transplant Cost</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/fue-hair-transplant/">FUE Hair Transplant</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/fut-strip-hair-transplant/">FUT Hair Transplant</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/hair-transplant-techniques/">Hair Transplant Techniques</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/facial-hair-transplant/">Facial Hair Transplant</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/beard-transplant/">Beard Transplant</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/bio-fue/">Bio FUE</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/body-hair-transplant/">Body Hair Transplant</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/revision-hair-transplant/">Revision Hair Transplant</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-transplant/post-operative-care/">Post Operative Care</a>
                    </div>
                </li>
                <li class="nav-item  <?= $hair_loss_active;?>"> 
                    <a class="nav-link" href="<?= base_url(); ?>hair-loss/">Hair Loss</a> 
                </li>
                <li class="nav-item <?= $hair_restoration_active;?> dropdown"> 
                    <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>hair-restoration/" id="dropdown03"  aria-haspopup="true" aria-expanded="false">Hair Restoration</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown03"> 
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-gain-therapy/"> Hair Gain Therapy</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>prp-treatment/">PRP Hair Treatment</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>artificial-hair-restoration/">Artificial Hair Restoration</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>hair-restoration/scalp-micro-pigmentation-india/">Scalp Micro Pigmentation</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>mesotherapy/">Mesotherapy</a> 
                    </div>
                </li>
                <li class="nav-item <?= $cosmetic_surgery_active;?> dropdown"> 
                    <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>cosmetic-surgery/" id="dropdown03" aria-haspopup="true" aria-expanded="false"> Cosmetic Surgery</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown03"> 
                        <a class="dropdown-item" href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>gynecomastia/">Gynecomastia</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Treatment</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>liposuction/">Liposuction</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>breast-reduction/">Breast Reduction</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynaecology</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a> 
                    </div>
                </li>
                <li class="nav-item <?= $cosmetology_active;?> dropdown"> 
                    <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>cosmetology/" id="dropdown03" aria-haspopup="true" aria-expanded="false">Cosmetology</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown03"> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/laser-hair-removal-for-men-and-women/">Laser Hair Removal</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/non-surgical-ultrasonic-liposuction/">Non Surgical Liposuction</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/skin-polishing/">Skin Polishing</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/stretch-marks-treatment/">Stretch Marks Treatment</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/laser-photo-facial/">Laser Photo Facial</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/laser-tattoo-removal/">Laser Tattoo Removal</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/ultherapy-treatment/">Ultherapy Treatment</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/chemical-peels/">Chemical Peels</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/acne-treatment/">Acne Treatment</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">Pigmentation Treatment</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/laser-vaginal-rejuvenation/">Laser Vaginal Rejuvenation</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/anti-ageing-treatments/">Anti Ageing Treatments</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>cosmetology/ultracel-skin-tightening-treatment/">Ultracel Skin Tightening</a> 
                    </div>
                </li>
                <li class="nav-item <?= $results_active;?>"> 
                    <a class="nav-link" href="<?= base_url(); ?>results/">Result</a> 
                </li>
                <li class="nav-item <?= $blog_active;?>"> 
                    <a class="nav-link" href="<?= base_url(); ?>blog/">Blog</a> 
                </li>
                <li class="nav-item  <?= $locations_active;?> dropdown"> 
                    <a class="nav-link dropdown-toggle" href="<?= base_url(); ?>locations/" id="dropdown03" aria-haspopup="true" aria-expanded="false">Our Location</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown03"> 
                        <a class="dropdown-item" href="<?= base_url(); ?>locations/hair-transplant-ludhiana/">Ludhiana</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>locations/hair-transplant-delhi/">Delhi</a> 
                        <a class="dropdown-item" href="<?= base_url(); ?>bangalore/">Bangalore</a> 
                    </div>
                </li>
            </ul>
        </div>
    </div>
    </nav>
</header>
