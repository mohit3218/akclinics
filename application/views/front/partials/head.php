<meta charset="utf-8">
<meta name="viewport" content="width=device-width, minimum-scale=1,initial-scale=1, shrink-to-fit=no">
<meta name="author" content="AK Clinics">

<link rel="shortcut icon" href="<?php echo cdn('assets/template/'); ?>favicon.ico" />
<?=(isset($meta) ? meta_tags($meta) : meta_tags());?>

<!-- Bootstrap core CSS -->

<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,400,400i,500,700" rel="stylesheet">

<link rel="preload" href="<?php echo cdn('assets/template/frontend/'); ?>css/font-awesome.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="<?php echo cdn('assets/template/frontend/'); ?>css/font-awesome.min.css"></noscript>

<link href="<?php echo cdn('assets/template/frontend/'); ?>css/bootstrap.min.css" rel="stylesheet">

<link rel="preload" href="<?php echo cdn('assets/template/frontend/'); ?>css/slick.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="<?php echo cdn('assets/template/frontend/'); ?>css/slick.css"></noscript>

<link rel="preload" href="<?php echo cdn('assets/template/frontend/'); ?>css/custom.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="<?php echo cdn('assets/template/frontend/'); ?>css/custom.min.css"></noscript>

<script src="<?php echo cdn('assets/template/frontend/'); ?>js/jquery.min.js"></script> 
<script async src="<?php echo cdn('assets/template/frontend/'); ?>js/csspreload.js"></script> 

<script async src="https://www.googletagmanager.com/gtag/js?id=AW-1014342854"></script>
<style>.gsc-adBlock{display:none !important}</style>

<script type="text/javascript">
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'AW-1014342854');
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '915328878663520', {}, {"agent": "wordpress-4.9.8-1.7.9"});
fbq('track', 'PageView', {"source": "wordpress","version": "4.9.8","pluginVersion": "1.7.9"});
</script>

<noscript><img src="https://www.facebook.com/tr?id=915328878663520&ev=PageView&noscript=1" alt="fbpx" style="display:none" height="1" width="1" /></noscript>
<!--[if lt IE 9]><script src="https://cdn.akclinics.org/wp-content/themes/genesis/lib/js/html5shiv.min.js"></script><![endif]-->
