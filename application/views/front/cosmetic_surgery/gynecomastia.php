<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> 
                        <a itemprop="item" href="<?= base_url(); ?>cosmetic-surgery/"> <span itemprop="name">Cosmetic Surgery</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>gynecomastia/"> <span itemprop="name">Gynecomastia</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title">
                    <h1 class="black-text text-center">Male Breast Reduction Surgery</h1>
                </div>
                <p>Enlargement of breast in human male is called Gynecomastia because of feminine type looks of breast. In this physical abnormality and deformed state, breasts bulged and protrude outward. It primarily involves accumulation of glandular tissue, fatty tissue and excessive growth of skin. In medical term it is proliferation of ductal and stromal part of mammary glands. This means breast more often becomes bulky, voluminous, heavy, irregular and distorted. This condition may be unilateral or bilateral. This condition is also known as Man boobs or men breast enlargement. Gynecomastia removal is surgically done with gynecomastia surgery or men breast reduction surgery.</p>
                <h4>Causes of Gynecomastia</h4>
                <ul class=" margin-bottom-20">
                    <li>Low level of androgens or sex hormones such as testosterone, LH</li>
                    <li>High levels of estrogens or female hormones</li>
                    <li>Drugs such as antibiotics, antidepressants, diuretics</li>
                    <li>Use of anabolic steroids to gain large and bulky muscles, gyming</li>
                    <li>High level of prolactin, a hormone responsible for producing milk in females,</li>
                    <li>Certain narcotics such as marijuana, heroin</li>
                    <li>Genetically due to klinefelter’s syndrome</li>
                    <li>Diet loaded with fat and lipids</li>
                    <li>Hypogonadism – small testes/ unable to produce enough hormones</li>
                    <li>Rare causes – cancer of testis, breast, liver cirrhosis</li>
                    <li>Hyperthyroidism - It helps in retaining more fluid in body</li>
                </ul>
                <h4>Types of Gynecomastia</h4>
                <figure><img data-src="<?= cdn('assets/template/frontend/images/'); ?>gynecomastia-img.jpg" class="img-fluid lazy" alt="Male Breast Reduction Surgery Clinic In India"/></figure> 
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetic-surgery/">Cosmetic Surgery</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Surgery</a></li>
                        <li><a href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>liposuction/">Liposuction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-reduction/">Breast Reduction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynecology</a></li>
                        <li><a href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3  ">
    <div class="container">
        <div class="row">
            <div class="section-title text-center ">
                <h2 class="black-text">Other aspects of Gynecomastia or Male Breast</h2>
            </div>
        </div>
        <div class="row text-center ptb-1">
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Signs & Symptoms</h3>
                <p>The most important symptom is enlargement of male breast – especially the enlargement of the glandular tissue rather than fatty tissue. The same could be observed on both side or even one side. The same should be diagnosed & treated as early as possible.</p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Classification</h3>
                <p>The spectrum of gynecomastia severity has been categorized into a grading system:</p>
                <ul class="text-left">
                    <li>Grade I: Minor enlargement, no skin excess</li>
                    <li>Grade II: Moderate enlargement, no skin excess</li>
                    <li>Grade III: Moderate enlargement, skin excess</li>
                    <li>Grade IV: Marked enlargement, skin excess</li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Prognosis</h3>
                <p>Gynecomastia is generally not physcially harmful but more devastating psychological consequenses like low self-esteem or shame for sufferers. One must know that weight loss cannot reduce the glandular component so treatment is a must. In rare cases, it could also be indicator of underlying condition like testicular cancer.</p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Epidemiology</h3>
                <p>Gynecomastia or male breasts are becoming more and more prevalent in all age groups especially Senile gynecomastia is estimated to be present in 24-65% of men between the ages of fifty and eighty. Breast reduction surgeries to correct gynecomastia are becoming increasingly common. There are many approaches to the treatment of the male breasts </p>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text text-center"> Male Breast Reduction Surgery</h2>
                    <p>Our highly qualified plastic surgeons at AK Clinics routinely perform gynecomastia mastectomy or liposuction of patients suffering from this condition. AK Clinics is equipped with ultra modern equipments with all facilities and state-of-art-technology. Moreover our doctors are highly sensitive to your feelings and emotions and ensures you of masculine look through this surgery.</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Non Surgical Treatment</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Non Surgical Treatment</h3>
                    <p>Many people approach endocrinologist for the treatment but in most of the cases the size once increased can only be reduced by a surgery. Further since gynecomastia is hormonal problem so the treatment regime is generally anti hormones which have serious side effects.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Surgical Treatment – Least Invasive</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Surgical Treatment – Least Invasive</h3>
                    <p>Gynecomastia Surgery is mostly performed a least invasive method where in the patient sees almost invisible scars. Liposuction is used in conjunction for better results. But in some cases where the size is too big or the boobs are hanging because of the over weight loss, a more invasive method Infra-mammary approach in which incision is given below the breast.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Surgical Treatment – Conventional method</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Surgical Treatment – Conventional method</h3>
                    <p>In the conventional method & most commonly used an incision is made around the areola and entire gland is removed though this incision. This is used when there is no sagging or no excision is required. The nerves and blood vessels are also removed. So this method is getting less and less popular. Though in some cases this needs to chosen invariably.</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="service-area ptb-3  ">
    <div class="container">
        <div class="row">
            <div class="section-title text-center ">
                <h2 class=" black-text">Before, during & after the Gynecomastia Surgery</h2>
            </div>
        </div>
        <div class="row text-center ptb-1">
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Diagnosis</h3>
                <p>Our <a href="<?= base_url(); ?>cosmetic-surgery/">eminent cosmetic surgeons</a> identify the Gynecomastia through the physical diagnosis of breast that includes its tenderness, hardness, shape, volume and size of abnormal breast. Clinical tests such as hormonal levels, ultrasound diagnoses confirm the exact cause. This could also be related to sexual dysfunction. </p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>During the procedure</h3>
                <p>Depending upon the type of procedure decided, the surgery is performed under local or general anaesthesia. In most of the cases the surgery is performed on out patient basis which means the discharge normally happens the same day. The procedure takes 2-3 hours in total and is relatively simple with a short recovery period. All the instructions are given to make sure the recovery period is hassle free.</p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Post-op Period</h3>
                <p>One should not do any exercise involving muscular force post surgery. Our Plastic surgeons at AK Clinics will advise you regarding the exercise that can be done after few weeks of surgery beginning from very light exercises. Medicines should be taken as prescribed and follow up diagnosis and treatment is necessary for the best results.</p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Cost of surgery</h3>
                <p>The cost of cosmetic surgery depends upon the individual case. It varies from one patient to another considering the size and volume of enlarged breast and tissues involved.</p>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h2 class="black-text text-center">FAQs About Gynecomastia & its treatment</h2>
                    <p>We strive to provide highest quality at affordable cost with hygiene and imported equipments</p>
                </div>
            </div>
        </div>
        <div class="col-md-12">

            <div id="accordion" class="mt-4">
                <div class="row"> <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">What Is The Role Of Testosterone In Gynecomastia?</a> </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                <div class="card-body">Testosterone is a male sex hormone or androgen. Deficiency of testosterone hormone causes this condition in males in older ages.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">What Is Right Time Of Surgery?</a> </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body">It depends upon individual’s choice but when someone feels that he really has problem with large breast such as low self confidence, inferiority complex, shyness he should decide for breast reduction surgery.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">What Exactly Is Gynecomastia?</a> </div>
                            <div id="collapsefive" class="collapse" data-parent="#accordion">
                                <div class="card-body">When breast of man becomes large enough with looks like that of women then this physical abnormality is defined as Gynecomastia in medical terminology. The mammary glands have stroma and ductal parts. Proliferation of tissues and accumulation of fats results in this condition.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsese">Is There Any Need Of Liposuction In Breast Reduction Surgery?</a> </div>
                            <div id="collapsese" class="collapse" data-parent="#accordion">
                                <div class="card-body">In some cases where fats and lipid accumulated in tissues are very large it is very essential to remove the fat through liposuction to have more masculine breast with better results.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsenine">Is There Any Mark Or Scarring On The Breast After Surgery?</a> </div>
                            <div id="collapsenine" class="collapse " data-parent="#accordion">
                                <div class="card-body">No, incision is drawn on joint of areola of nipple and normal skin.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseelev">Is Just Liposuction Not Sufficient To Treat Gynecomastia?</a> </div>
                            <div id="collapseelev" class="collapse" data-parent="#accordion">
                                <div class="card-body">This is in very few cases where ultrasound indicates accumulation of fats in tissue but this fat can reappear there if your diet contains high fat or lipid content. But when breast reduction surgery is performed cosmetic surgeon removes tissue which store fats, along with glandular tissues and excess of skin. All this leads to very good masculine contour of the breast.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsethi">Is Breast Reduction Surgery A Major Surgery?</a> </div>
                            <div id="collapsethi" class="collapse" data-parent="#accordion">
                                <div class="card-body">No, it does not involve any vital internal organs like heart, brain, kidney, liver etc.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefif">How Can I Sure Myself That I Have Only Gynecomastia And Not Breast Cancer?</a> </div>
                            <div id="collapsefif" class="collapse" data-parent="#accordion">
                                <div class="card-body">In males breast cancer is rare and related to failure of testis. Moreover if any male has only asymmetric enlargement of breast (only one of breast is large), some fluids come out of nipple, painful nipple or there is any lump in the breast he should immediately visit his physician to clarify the exact disease and cause.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsesteen">Can You Tell Me The Basic And Very Common Causes?</a> </div>
                            <div id="collapsesteen" class="collapse" data-parent="#accordion">
                                <div class="card-body">Cause generally differs from one individual to another and some time specific to age group or profession or habit. for example bodybuilders having anabolic steroids for the growth of their muscles may develop this condition due more muscular growth of breast area. In older age males, it is generally due to lower sex hormones, habit of taking marijuana or heroin may cause man boobs, in some cases hypothyroidism which results in retention of fluids in body may be the cause. In brief surgeons after having some clinical tests and then conclude the exact cause of Gynaecomastia.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsenteen">Can Gynaecomastia Disappear Itself?</a> </div>
                            <div id="collapsenteen" class="collapse " data-parent="#accordion">
                                <div class="card-body">In newborn male babies Gynaecomastia is due influence of maternal hormones and this disappears soon. Secondly when young boys enter puberty they develop Gynaecomastia which disappears in some boys by the time but may persist in some other boys. Fat rich diet during this period of time and less physical activities may enhance Gynaecomastia considerably.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">What Is The Role Of Alcohol?</a> </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">Alcohol is also a cause of this condition.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">What Is Done In Breast Reduction Surgery?</a> </div>
                            <div id="collapsefour" class="collapse " data-parent="#accordion">
                                <div class="card-body">In breast reduction surgery, most of glandular tissue, fatty tissue and fat with excess of skin is removed surgically.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsesix">Is There Any Risk In Surgery Of Man Boobs?</a> </div>
                            <div id="collapsesix" class="collapse " data-parent="#accordion">
                                <div class="card-body">Every surgery has some inherent risk but well qualified, trained and experienced plastic surgeon is essential to perform successful surgery. Moreover at AK Clinics, we perform breast reduction surgery called mastectomy successfully and there are many such patients who are happy and much satisfied with their masculine breast.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseebi">Is There Any Age Limit For Breast Reduction Surgery?</a> </div>
                            <div id="collapseebi" class="collapse" data-parent="#accordion">
                                <div class="card-body">There is no age limit but this condition during puberty may disappears by the time. But if breast keeps on growing very fast and becomes large, bulky and voluminous one can go for mastectomy to reduce the breast and bring in almost flat and proper shape like those of normal boys.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseten">Is There Any Gland In Men’s Breast?</a> </div>
                            <div id="collapseten" class="collapse" data-parent="#accordion">
                                <div class="card-body">No, incision is drawn on joint of areola of nipple and normal skin.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsetwe">Is Breast Reduction Surgery More Painful?</a> </div>
                            <div id="collapsetwe" class="collapse" data-parent="#accordion">
                                <div class="card-body">No, this surgery is performed under general anesthesia and after that pain relieving medicines are given to patient to minimize the pain, almost painless.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefort">How Much Time Do I Need To Stay In Hospital?</a> </div>
                            <div id="collapsefort" class="collapse " data-parent="#accordion">
                                <div class="card-body">Breast reduction surgery is performed with in 1 hour and patients can go home after 1 day depending upon the case.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsesixt">Has Fatty Diet Any Role To Play?</a> </div>
                            <div id="collapsesixt" class="collapse " data-parent="#accordion">
                                <div class="card-body">Yes, at AK Clinics we have found large amount of fats during Mastectomy or breast reduction surgery of some patients.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseeteen">Can One Reduce Man Boobs With Exercise And Diet?</a> </div>
                            <div id="collapseeteen" class="collapse" data-parent="#accordion">
                                <div class="card-body">With exercises on can lose little bit of fat only not the proliferated glandular and fatty tissue. With diet you can restrict accumulation of fats and hence the size of breast but you will not be able to reduce as you expect. Mastectomy or breast reduction surgery is the only effective method with highly noticeable and visible results.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsetwot">Can Anybody Guess That Whether I Have Undergone Breast Reduction Surgery Or Not?</a> </div>
                            <div id="collapsetwot" class="collapse" data-parent="#accordion">
                                <div class="card-body">AK Clinics does not reveal personal information and respect privacy of its every patient. Just looking at your breast nobody can be sure that you have undergone breast reduction surgery.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetic_surgery_blogs'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
