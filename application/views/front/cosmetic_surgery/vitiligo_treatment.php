<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> 
                        <a itemprop="item" href="<?= base_url(); ?>cosmetic-surgery/"> <span itemprop="name">Cosmetic Surgery</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>vitiligo-treatment/"> <span itemprop="name">Vitiligo Treatment</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">White Patches in Skin</h1>
                </div>
                <p>This disease is due to abnormal Skin <a href="<?= base_url(); ?>cosmetology/pigmentation-treatment/">pigmentation</a>. In the lower layers of epidermis of our Skin, special cells called melanocytes cells form melanin pigment. The color of skin depends upon the quantity of melanin pigment produced by melanocytes. Larger the quantity of melanin pigment darker is the skin. In the diseased condition some or all of melanocytes get damaged or become non functional. Due to lack of melanin pigment, skin becomes white in affected parts. These affected parts have white spots in form of irregular patches of different sizes. <br /> <br /> White patches in skin may be symmetrical. Vitiligo also known as leucoderma or white skin disease may appear on any parts of human body and are not restricted to any particular organ. The vitiligo treatment is divided in 2 parts – 1st part to stop the disease from spreading with help of laser, PUVA and medicines & then treating the patches with melanocyte transfer or skin grafting commonly known as Vitiligo surgery. <br />
                    <br />  Also Melanin is very important pigment in skin as it protects skin from harmful rays coming out from sun which have potential to cause skin burns and skin cancer. Vitiligo is not a communicable disease. White patches generally grow in size by the time. <br /> <br />
                    This disease is found to be related with many other diseases such as <a href="<?= base_url(); ?>blog/alopecia-areata/" target="_blank">alopecia</a>, diabetes, typhoid, pernicious anemia etc. in some cases. The most disturbing and embarrassing consequences of vitiligo are lack of self confidence, inferiority complex, and many other social and psychological problems. <br />
                    <br />  Vitiligo is found in almost all races in equal proportion irrespective of age and gender. </p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetic-surgery/">Cosmetic Surgery</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia/">Gynecomastia Surgery</a></li>
                        <li><a href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>liposuction/">Liposuction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-reduction/">Breast Reduction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynecology</a></li>
                        <li><a href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12 text-center"> <div class="section-title">
                        <h2>Types of Vitiligo</h2>
                        <p>The classification based on the quantity of vitiligo have always been in doubt. In more recent times the 2 broader types have been accepted to classify Vitiligo : Segmental Vitiligo (SV) and Non-Segmental Vitiligo(NSV).</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 ptb-1">
                <div class="row">
                    <div class="col-md-6"><strong>– Segmental –</strong>
                        <ul>
                            <li>It is not symmetric but found in young ages. White patches are irregular and not on exposed and expressive parts of body.</li>
                            <li>This affects areas of skin that are in association with posterior root of spinal nerve.</li>
                            <li>This type of vitiligo spreads more rapidly if not treated in time but at the same</li>
                        </ul>
                    </div>
                    <div class="col-md-6"><strong>– Non-segmental –</strong>
                        <ul>
                            <li>It is symmetrical and more noticeable as well. If one hand has white patch the other hand also have white patch. If there is white patch around one eye then the other eye too has the white patch.</li>
                            <li>This is found on arms, eyes, feet, hands and elbows – more generalized in nature. Its classes include Generalised Vitiligo, Universal Vitiligo, Acrofacial Vitiligo, & Mucosal Vitiligo</li>
                            <li>NSV spreads less rapidly but at the same time is more difficult to treat</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">

            <div class="section-title text-center ">
                <h2 class="black-text">Causes of Vitiligo</h2>
                <p>The causes are different for different individuals. But the most common are:-</p>
            </div>

        </div>
        <div class="row text-center ptb-1">
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Genetic factors</h3>
                <p>It has been observed that individuals with family history of disease are affected with white patches. Therefore it is considered as hereditary in some cases.</p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Autoimmune disorders</h3>
                <p>Immunity is one’s strength to fight against diseases. But due to defective immunity system our body protecting cells attack the pigment making melanocytes cells. Due to destruction of melanocytes, melanin decreases considerably and it results in vitiligo.</p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Nerve damage</h3>
                <p>Nerve damage or injury to dermal tissue may also be a cause. Many studies have suggested that Segmental Vitiligo is more often caused by nervous system disorders.</p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Chemicals</h3>
                <p>Some people are allergic to certain chemicals & the cause can be the Side effects of certain harmful chemicals. In these cases again the melanocytes get destroyed and hence skin loses pigment</p>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h2 class="black-text ">Vitiligo Treatment</h2><p>There are different methods of treatment usually depends upon the part of body involved, symptoms, causes etc. Shapes of white patches, rate of growth and frequency of white patches are also considered as important factors for treatment. After a thorough and complete diagnosis our highly qualified and widely experienced dermatologist follows a systematic approach to cure the the same. Some of techniques and methods used for curing vitiligo are:-</p>
                </div>

            </div>
        </div>
        <div class="row ptb-1">
            <div class="col-md-6">
                <div id="accordion" >
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Phototherapy – Use Of UV Light</a> </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">PUVA has been used for treatment for vitiligo for very long. Though currently UVB is more popular owing to lower incidents of photo-toxic side effects. The treatments may take as long as 12-15 months to achieve results. These are normally used in conjunction with topical agents for a quicker response.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">PUVA- Use Psoralen</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">In this form of treatment psoralen is used in conjunction with the phototherapy. The psoralen is applied 30 mins before the phototherapy treatment to the affected areas.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Treatment By LASER- Excimer LASER Treatment</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">This is relatively a new form of treatment which has given good results.A 308mn excimer laser is used for 15 weeks or less and shows impressive repigmentation. he 308nm excimer laser is found effective in majority of patients achieving upto 75% repigmentation and It is found effective and even higher in higher Fitzpatrick skin types.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Medicinal – Steroid Therapy Containing Corticosteroids</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">Corticosteroids can definitely help repigment the skin especially if started around the same time as onset. The intensity & strength varies with the age of the patient. These steriods contain cortisone which is similar to hormones produced by adrenal glands. These have shown results in many patients with usage for 3 months or more.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">Melantocyte Transplantation</a> </div>
                        <div id="collapsefive" class="collapse" data-parent="#accordion">
                            <div class="card-body">Melantocyte Transplantation is the latest treatment in surgical management of Vitiligo. Even very large areas can be treatment with this method and results are better than older techniques like skin or punch grafting. In this method melanocytes are taken from healthy area of skin and transferred inform of cellular suspension to the affected ares of the skin.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <iframe style="border: 1px solid #CCC; border-width: 1px; margin-bottom: 5px; max-width: 100%;" src="//www.slideshare.net/slideshow/embed_code/key/jwgEWdNGiYRrcO" width="100%" height="355" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" allowfullscreen="allowfullscreen"> </iframe>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class=" section-title ">
                    <h2 class=" black-text">Vitiligo Treatment by Melanocyte Transplanttion</h2>
                </div>
            </div>
        </div>
        <div class="row ptb-1">
            <div class="col-md-7 col-sm-12 content-part">
                <p>In this procedure, the melanocytes and keratinocytes are taken from a healthy area of the patient. Then a suspension is prepared by using various enzymes then transplanted to the affected area. This technique has given over 95% results in various patients.</p>
                <p>The procedure need only few hours to complete and needs no hospitalisation. It requires only local anaesthesia and patient is sent home immediately after the procedure.</p> <h4>We’ve full-time team of doctors, technicians, world-class facilities to provide great results and a hassle-free experience.</h4>
            </div>
            <div class="col-md-5 col-sm-12">
                <figure><img class="img-responsive img-thumbnail center lazy" data-src="<?php echo base_url('assets/template/frontend/'); ?>img/vitiligo.png" alt="Vitiligo Treatment Clinic in Delhi, India"></figure>
            </div>
        </div>
    </div>
</div>
<div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/cosmetic_surgery_blogs'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>