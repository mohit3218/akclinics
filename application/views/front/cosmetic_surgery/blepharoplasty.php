<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> 
                        <a itemprop="item" href="<?= base_url(); ?>cosmetic-surgery/"> <span itemprop="name">Cosmetic Surgery</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>blepharoplasty/"> <span itemprop="name">Blepharoplasty</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">What is Blepharoplasty?</h1>
                    <p>The procedure done to reconstruct or repair the eyelids is known as blepharoplasty or eyelid surgery. The process usually includes the removal of fat as well as excess skin and tissue. This is the process that is used to rectify upper eyelids that might be drooping too much or high level of puffiness under the eyes. While in many cases, blepharoplasty is done to reduce the signs of ageing, in certain cases, drooping eyelids can hamper vision.</p>
                </div>
                <figure><img data-src="<?= cdn('assets/template/frontend/images/'); ?>blepharoplasty-infographic.png" class="img-fluid lazy" alt="Blepharoplasty surgery in India"/></figure> 
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetic-surgery/">Cosmetic Surgery</a></li>
                        <li class=""><a href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia/">Gynecomastia Surgery</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Surgery</a></li>
                        <li><a href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>liposuction/">Liposuction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-reduction/">Breast Reduction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynecology</a></li>
                        <li><a href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="row">
            <div class="section-title text-center ">
                <h2 class="black-text">Types of Blepharoplasty</h2>
            </div>
        </div>
        <div class="row text-center ptb-1">
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h3>Upper eyelid surgery</h3>
                <p>Used mainly to address the issue of functionality, this procedure is done mainly on the elderly. With age, the upper eyelid will lose the elasticity, which will lead to drooping. This can hamper vision and create difficulty while writing or driving and upper eyelid surgery aims at correcting the same.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h3>Lower eyelid blepharoplasty</h3>
                <p>The surgery is done on the lower eyelid and aims to reduce the bags under the eyes or improve the wrinkles around the eyes.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h3>Double eyelid surgery</h3>
                <p>Often known as the Asian blepharoplasty, the surgery is done to create a crease in the upper lid, which actually gives the appearance of a double lid.</p>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text text-center">Before, during & after the Blepharoplasty</h2>
                    
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Before the surgery</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Before the surgery</h3>
                    <p>Before your surgeon can get you started on your procedure, you will be asked several questions about your health in general. Your medical history will be looked into, in detail, and if you have or have had any serious medical condition, you will need to mention the same. If you are a smoker, you will be asked to refrain from doing so, a few weeks prior to the surgery. You might also be asked to avoid medications such as aspirin, as these could lead to increased bleeding.</p>  
                </div>
                <div class="tab">
                    <button class="tab-toggle">During the Surgery</button>
                </div>
                <div class="content">
                    <h3 class="m-3">During the Surgery</h3>
                    <p>The very first step would be to administer anaesthesia, which will ensure that the person does not feel any pain or discomfort. Certain surgeons will prefer to give sedation, which will allow the person to sleep through the procedure. Incisions will be made along the natural lines of the eyes or in order to protect the aesthetics, there could also be incisions on the inner side of the surface. Al the excess tissue and skin will be removed and then with the help of small sutures, the rest of the skin will be stitched back together.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">After the surgery</button>
                </div>
                <div class="content">
                    <h3 class="m-3">After the surgery	</h3>
                    <p>There will be swelling, bruising and redness around the surgical site for a few days and this is completely natural. You will be given very specific instructions about how to handle the surgical site. The doctor will also prescribe medications to control the pain and eye drops to avoid any infections. You could use cold compresses to help with the swelling and itching. Your eyes should be back to normal in a few weeks and your doctor will tell you about the precautions that you will need to take in the days to come.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3  bg-light-gray ">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                    <p>We strive to provide highest quality at affordable cost with hygiene and imported equipments</p>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Is There An Age Limit For Blepharoplasty?</a> </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">While there is no specific age limit for the procedure, the requirement for the same normally comes with old age.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Will Blepharoplasty Take Care Of The Wrinkles And Crows’ Feet Around My Eyes?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">No; blepharoplasty is meant to correct only the eyelids, which means that it cannot take care of wrinkles or crows’ feet. There are other procedures for such issues.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Will Blepharoplasty Take Care Of The Dark Circles Under My Eyes?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">Dark circles have nothing to do with the dark skin under the eyes, which is why treating the eyelids will have no effect on the colouring of the skin.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Will The Effects Of Blepharoplasty Be Permanent?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">While the effects might not be permanent, it will last a really long time. With age, the eyelids will start drooping again and a repeat surgery might be required.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">Will Insurance Cover My Blepharoplasty Bill?</a> </div>
                        <div id="collapsefive" class="collapse" data-parent="#accordion">
                            <div class="card-body">Drooping eyelids could hamper vision, which is why, certain insurance companies will cover the same.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>Possible Risks</h2>
                    <p>Ideally, when blepharoplasty has been conducted by an experienced surgeon, there are little to no chances of things going wrong. However, there are some complications that can be associated with this procedure, such as:</p>
                </div>
                <div class="row">
                    <ul>
                        <li>The results might not be visible immediately, because it might take some time for the results to become obvious. </li>
                        <li>There might be swelling and bruising around the eyes, and this might not fade away immediately. </li>
                        <li>There could be blurred vision or even double vision for a few days. </li>
                        <li>The procedure might have to be repeated every few years, making it an expensive investment. </li>
                        <li>The scarring might be obvious. </li>

                        <li>One common complication of blepharoplasty is a condition known as ectropion. In this condition, the lower eyelid gets pulled down and this situation would invite further the requirement of further surgery. </li>
                        <li>There could be an appearance of minute whiteheads, after the stitches have been removed. </li>
                        <li>There could be extreme dryness or irritation in the eyes, immediately after the surgery and this could continue for a few days. </li>
                        <li>Some people could also have trouble closing their eyes for a few days. </li>
                        <li>There could be a lack of symmetry in the final results. </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetic_surgery_blogs'); ?>
<div class="bg-light-gray ">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>