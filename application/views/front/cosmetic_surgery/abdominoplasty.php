<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> 
                        <a itemprop="item" href="<?= base_url(); ?>cosmetic-surgery/"> <span itemprop="name">Cosmetic Surgery</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>abdominoplasty/"> <span itemprop="name">Abdominoplasty</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Abdominoplasty – Tummy Tuck Treatment </h1>
                </div>
                <p>A flat tummy/abdomen is something almost everyone craves for, but most people are not willing to work for it. However, there are cases where the weight loss is just not possible and in such cases, the best way to a flat tummy is a tummy tuck.  <br/> Medically known as abdominoplasty surgery or tummy tuck surgery, this is a procedure by which excess fat and skin from the stomach area is removed to create a stomach that is flatter and smoother. The bulge in the tummy/abdomen could be due to age, pregnancy, immense weight fluctuations or even heredity.</p>
                <strong>Tummy Tuck Types</strong> 
                <ul>
                    <li>Traditional Tummy Tuck</li>
                    <li>Mini& Extended Tummy Tuck</li>
                    <li>Circumferential Tummy Tuck</li>
             
                </ul>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetic-surgery/">Cosmetic Surgery</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia/">Gynecomastia Surgery</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Surgery</a></li>
                        <li><a href="<?= base_url(); ?>liposuction/">Liposuction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-reduction/">Breast Reduction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynecology</a></li>
                        <li><a href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3  bg-light-gray ">
    <div class="container">   
        <div class="row">
            <div class="section-title">
                <h2 class=" black-text text-center ">Types of Abdominoplasty</h2>
            </div>
        </div>
        <div class="col-md-12">    
            <div class="row text-center">
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <h4>Traditional Tummy Tuck</h4>
                    <p>An incision is made from hip to hip hidden in undergarments and the entire belly area is opened up. The process is often combined with liposuction for best results.</p>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <h4>Mini & Extended Tummy Tuck</h4>
                    <p>When there is minimal removal of fat required, the incisions are kept below the navel. The process needs only a single incision, the navel does not have to be repositioned and the recovery time is lesser.When the basic tummy tuck is extended to the remove the ‘love handles’ too. The incision is made really low, making it practically invisible post-surgery and allows for the handling of a larger area.</p>
                </div>
              
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <h4>Circumferential Tummy Tuck</h4>
                    <p>Almost like a body lift, this is an ideal choice for people who have lost immense weight or have to undergo gastric bypass surgery. This type of surgery will help remove excess skin from not only the abdomen, but also hips, thighs and buttocks.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text text-center">Abdominoplasty</h2>
                    <p>Our highly qualified plastic surgeons at AK Clinics routinely perform Abdominoplasty-tummy tuck of patients suffering from this condition. AK Clinics is equipped with ultra modern equipments with all facilities and state-of-art-technology. Moreover our doctors are highly sensitive to your feelings and emotions and ensures you of aesthetic look through this surgery.</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle ">Before the Surgery</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Before the surgery</h3>
                    <p>Before your tummy tuck, you will be asked to avoid alcohol and smoking for a few weeks. You will also be asked to undergo extensive medical exams and tests, just to ensure that you are healthy enough for the procedure. Your regular course of medication will have to be checked, because if you are on any kind of blood thinners, you will be asked to avoid them for a few days. Medications for other medical conditions might also be altered, as they could lead to complications, during or post-surgery. Since this a proper surgery, you will be asked to get admitted the previous day or at least the morning of the surgery, so that a further set of tests and checks can be made.</p>
                </div><div class="tab">
                    <button class="tab-toggle">During the Surgery</button>
                </div>
                <div class="content">
                    <h3 class="m-3">During the Surgery</h3>
                    <p>AnaesthesiaSince it is a proper surgical procedure, anaesthesia will be required and the surgeon will choose between sedation, anaesthesia or a combination. Making the incisions – The incision will depend on the type of tummy tuck, but for a traditional one, a horizontal incision will be made between starting from the pubic area to the navel. The length will be dependent on the amount of fat that needs to be removed. The excess fat will be removed and the excess skin will be snipped off. The remaining skin will then be pulled up and a new belly button will have to be created. Closing the incisions – Once the procedures have been completed, the incisions will be closed using sutures, stitches, tapes or staples.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">After the surgery</button>
                </div>
                <div class="content">
                    <h3 class="m-3">After the surgery	</h3>
                    <p>After the procedure, dressing will be applied to the incision and you will be given instructions as to how care for the same. Drains might also be placed to allow for excess fluids to seep out without any issues and these will be removed within a few days’ time. While your stomach will seem instantly flatter, the actual results will become visible only after a few weeks. You will be advised bed rest for a while and many people have mentioned that they have trouble walking straight or sleeping horizontally. This happens because the stomach has been pulled tight and till the time the stomach muscles loosen up again, this will continue. You will however be asked to walk around as much as possible, after the first few days. Most people will be asked to wear supportive clothing, as these will assist with faster healing as well.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                    <p>We strive to provide highest quality at affordable cost with hygiene and imported equipments</p>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> 
                            <a class="card-link" data-toggle="collapse" href="#collapseOne">Can A Tummy Tuck Happen Post Pregnancy?</a> 
                        </div>
                        <div id="collapseOne" class="collapse " data-parent="#accordion">
                            <div class="card-body">Ideally, a tummy tuck should be done, if no more pregnancies are being planned, because if you do get pregnant after a tummy tuck, there is no assurance that the effects of the procedure will last.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> 
                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Is A Tummy Possible After A C-Section?</a> 
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">Absolutely! Once your stomach has returned to normalcy, you will become eligible for a tummy tuck.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> 
                            <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Will Weight Gain Affect The Tummy Tuck?</a> 
                        </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">Your tummy will look flat, but if you gain too much weight, it will start to show.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> 
                            <a class="card-link" data-toggle="collapse" href="#collapsefour">Is There An Age Limit?</a> 
                        </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">Ideally, you should be above 18 to undergo the procedure.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> 
                            <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">Is There A Lot Of Pain Post A Tummy Tuck?</a> 
                        </div>
                        <div id="collapsefive" class="collapse" data-parent="#accordion">
                            <div class="card-body">The level of pain depends on how extensive the tummy tuck has been. You can expect a fair amount of pain, which in many cases, gives way to numbness. Certain people have said that even when they scratch their stomach, they do not feel it.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsesix">Can A Tummy Tuck Be Combined With Other Cosmetic Procedures?</a> </div>
                        <div id="collapsesix" class="collapse" data-parent="#accordion">
                            <div class="card-body">Yes, a tummy tuck can be combined with breast augmentation or breast lift. Normally, when both these procedures are combined, the process is referred to as a mommy makeover.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>Possible Risks</h2>
                    <p>As is the case with any surgery, there are some risks and complications that can be associated with a tummy tuck too.</p>
                </div>
                <div class="col-md-12 ptb-1">
                    <ul>
                        <li>There could be complications related to the anaesthesia being used</li>
                        <li>There can be allergic reactions to the medical material being used during or after the surgery</li>
                        <li>Since stomach muscles and tissues are being touched, there could be the chance of the blood supply getting affected, which could lead to necrosis.</li>

                        <li>If you do develop necrosis, it could lead to massive infections, especially when not treated in time. </li>
                        <li>There could also be a blood clot in the body, which is medically known as pulmonary thromboemboli, which will also have to be treated in time.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetic_surgery_blogs'); ?>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>