<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> 
                        <a itemprop="item" href="<?= base_url(); ?>cosmetic-surgery/"> <span itemprop="name">Cosmetic Surgery</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>liposuction/"> <span itemprop="name">Liposuction</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">What is Liposuction?</h1>
                </div>
                <p>Liposuction Surgery or fat reduction surgery is cosmetic and corrective surgery which helps in body contouring as well that involves the removal of excess of unwanted fat from various body parts with the help of special medical instruments. Excess of fat is stored in special tissue called adipose tissue and inside the body. In human beings additional unused fats or lipids gets stored beneath the skin mostly around abdomen or belly or stomach, tummy, hips, thighs , buttocks, love handles, saddle bags or outer thighs and other parts of body. With advent of advancement the recovery time and liposuction cost both have reduced.</p>
                <ul class="margin-bottom-20">
                    <li>Has been successfully used for all skin types including tanned skin</li>
                    <li>Possible on the entire body e.g. facial, underarms and bikini area</li>
                    <li>Cost effective</li>
                    <li>Non-invasive and almost painless</li>
                </ul>
                <strong>How Liposuction surgery is performed?</strong>
                <p>It is performed with special sterilized instrument called cannulas inserted into fatty tissue. With the help of well defined incisions marked externally on skin of identified area and the help of suction tube, fat along with some other fluids is sucked out by vacuum pump. Medicated fluids may be injected to get more results. Mostly local anesthesia is given or applied on the skin. This procedure can be done from different parts of body simultaneously. You need not to stay in clinic overnight after the procedure. Swelling and bruising are common soon after the surgery on skin and are temporary. Swelling and bruising decrease slowly and gradually. Weight loss effects are felt immediately by the patients however the new shape with noticeable changes is visible only after few weeks of liposuction.</p>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetic-surgery/">Cosmetic Surgery</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia/">Gynecomastia Surgery</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Surgery</a></li>
                        <li><a href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-reduction/">Breast Reduction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynecology</a></li>
                        <li><a href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="row">
            <div class="section-title text-center ">
                <h2 class=" black-text">Various Techniques used for Liposuction</h2>
                <p>There are many techniques used for liposuction, we’re sharing the ones that are most commonly & safely used. Our Surgeons have paramount experience in conducting liposuction with great results.</p>
            </div>
        </div>
        <div class="row text-center ptb-1">
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h3>Suction-assisted liposuction</h3>
                <p>This is the most commonly used method of liposuction, wherein a small incision is made through which the cannula goes inside to remove fat. The cannula to & fro movement aid to breaking fat cells. This procedure can also be carried under local anaesthesia using tumescence</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h3>Laser-assisted Liposuction</h3>
                <p>In this technique lower-energy laser waves are used to melt the fat with use of fibre threaded cannula. This technique has known to cause lesses bleeding.There are many devices now available but most good surgeon still prefer Suction-assisted because of better results & lesser fibrosis.</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 ">
                <h3>Ultrasound assisted Liposuction</h3>
                <p>In this technique, ultrasound vibrations are used which burst the fat cells and hence liquifying them. There on they are removed through a micro-cannula. This is a good choice for working on more fibrous areas, like the upper back or male breast area</p>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row "><div class="col-md-12"> 
                <div class="section-title">
                    <h2 class="black-text text-center">Advantages of Liposuction</h2></div>
            </div></div>

        <div class="row ptb-1">
            <div class="col-md-6">
                <ul class="text-left">
                    <li>Proper and balanced physical appearance due to reduction in volume</li>
                    <li>Easy and comfortable locomotion due to weight loss</li>
                    <li>Very helpful in sitting, standing, walking and maintaining other postures</li>
                    <li>Reduction in stress and strain over other body parts such as stomach, liver, kidneys, bones and muscles that help them functioning normally and efficiently</li>
                    <li>Leads to tightening of skin with more attractive, younger and lighter look</li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="text-left">
                    <li>Helps in getting rid of excess of fat and ultimately reduces body weight</li>
                    <li>Exercises become a lot more easy to do and have effective high yields</li>
                    <li>Wide range exercises and sports can be enjoyed very well</li>
                    <li>Successful, happy sexual life can be achieved by losing fat from lower abdomen or Mons pubis near pelvic region in men and women.</li>
                    <li>Leads to tightening of skin with more attractive, younger and lighter look</li>
                </ul></div>
        </div>
    </div>
</div>


<div class="service-area ptb-3  bg-light-orange">
    <div class="container">
        <div class="row">
            <div class="section-title text-center ">
                <h2 class=" black-text">Other aspects of Liposuction Surgery</h2>

            </div>
        </div>
        <div class="row text-center ptb-1">
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Indications</h3>
                <ul class="text-left">
                    <li>Removal of Excess fat deposists</li>
                    <li class="icon-chk">Cases of Gynecomastia</li>
                    <li class="icon-chk">Breast Reduction, facelift</li>
                    <li class="icon-chk">Combination with abdominoplasty</li>
                  <li class="icon-chk">Body contouring</li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Side Effects</h3>
                <p>There are only few minor side effects like discomfort, annoying, and even painful. Some of them include: Bruising, Swelling, Scars, Pain Numbness, Post-operative weight gain and Limited mobility.</p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 ">
                <h3>Complication Intra-op</h3>
                <p>Liposcution is no different from any other surgery and there are certain risks beyond minor side effects. Though the patient section is very carefully done but likelihood of complication increases in large cases.</p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 ">
            <h3>Post Op</h3>
<p>Patient need to wear pressure garment for 4-6 weeks.</p>
</div>
            
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                    <p>We strive to provide highest quality at affordable cost with hygiene and imported equipments</p>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Which Parts Of Body Are Generally Associated With Liposuction?</a> </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">This Surgery is used to remove fats mostly from obese parts of body around hips, thigh, face, stomach, belly, abdomen etc.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">What Positive Impacts Can I Expect On My Health If I Go Through This Procedure?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">If you are an ideal candidate then you can enjoy lot of benefits of this fat reduction surgery such as weight loss mostly in form of fat, highly attractive shape and contour of body, no stress and burden on muscles, bones and internal organs, improved physical activities and above all pleasing and impressive personality.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">What Is Tumescent Liposuction?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">Liquid is injected under skin in subcutaneous area and it got inflated, raised and firm then plastic surgeons put a medical instrument called cannula into fatty tissue and remove the fat with the help of vacuum created by machine.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">What Is Liposuction?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">Liposuction is a cosmetic surgery. Plastic surgeons remove excess of fats present under skin (Subcutaneous) with help of cannula and suck the fat out from fatty tissue by using vacuum.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">Should I Need To Stay In Hospital For Longer Time?</a> </div>
                        <div id="collapsefive" class="collapse" data-parent="#accordion">
                            <div class="card-body">No, it is a day procedure and you need not stay overnight in the hospital.</div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsesix">Is This Surgery An Option For Obesity Treatment?</a> </div>
                        <div id="collapsesix" class="collapse" data-parent="#accordion">
                            <div class="card-body">Weight loss is natural consequent of this surgery but it is not basically for obesity. Liposuction is a cosmetic surgery meant for removal of excess of fat and maintaining a well defined contour of an individual with a balanced approach with respect to whole body. For obesity you have to care for what you eat, types of exercises, level of physical activities and your genes are also responsible for obesity. Some medicines can induce obesity and edema. Edema is a state when tissues retain excess of fluids in them and results in irregular, bulky and voluminous body.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseseven">Is There Any Scarring On The Skin?</a> </div>
                        <div id="collapseseven" class="collapse" data-parent="#accordion">
                            <div class="card-body">This surgery performed by highly efficient surgeons at AK Clinics generally leaves little scarring and in most of cases it has been done in a very smart way in line with natural skin appearance or on a part which mostly remain hidden and less expressive physically.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseeight">Is Abdominoplasty Same As Liposuction?</a> </div>
                        <div id="collapseeight" class="collapse" data-parent="#accordion">
                            <div class="card-body">No, abdominoplasty is different type of plastic surgery restricted to abdomen only where as this surgery can be performed on any part of body such as thigh, around hips, face etc.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsenine">I Have Heard That Some Special Clothes/ Dresses Are Given To Patients After Surgery, Why? What Is Their Use And For How Long These Should Be Worn?</a> </div>
                        <div id="collapsenine" class="collapse " data-parent="#accordion">
                            <div class="card-body">Yes it is true. Surgeons advise this practice to patients who has undergone this fat reduction surgery surgery because it helps in maintaining contour, protecting and supporting the body part in right position.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseten">How Much Weight Is Reduced Through This Surgery?</a> </div>
                        <div id="collapseten" class="collapse" data-parent="#accordion">
                            <div class="card-body">It is sure that Weight of every individual reduces after this surgery. But the measure of weight loss is different in different individuals. Though it is good to shed some extra weight due to fat but cosmetic surgeons at AK Clinics do more care about shape and overall aesthetic achievement. During the procedure, the symmetry of body is considered very important for balanced and attractive appearance</div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseele">How Much Time Does It Take To Perform The Surgery?</a> </div>
                        <div id="collapseele" class="collapse" data-parent="#accordion">
                            <div class="card-body">Time for procedure totally depends upon how much area of body is covered under this cosmetic surgery. Moreover the organs and location involved also matters. Generally it takes 1 – 2 hours to perform the surgery.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsetewl">Do Stretch Marks Vanish After This Surgery?</a> </div>
                        <div id="collapsetewl" class="collapse" data-parent="#accordion">
                            <div class="card-body">Stretch marks do not vanish completely after the procedure but their physical appearance decreases very much.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsethr">Can This Surgery Be A Good Choice For Cellulite?</a> </div>
                        <div id="collapsethr" class="collapse" data-parent="#accordion">
                            <div class="card-body">No, cellulite needs different treatment. Generally in cellulite after applying certain pressure there appears a dimple on skin and skin does not retrace its original position immediately because of retention of fluids in body tissue rather than fats. However a person may suffer from cellulite and obese conditions simultaneously. Only a well trained and qualified cosmetic surgeon can diagnose such conditions.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefor">Can Liposuction Be Performed Along With Other Cosmetic Surgeries Like Gynaecomastia?</a> </div>
                        <div id="collapsefor" class="collapse " data-parent="#accordion">
                            <div class="card-body">Yes, it can be performed along with other minimally invasive surgeries. Such a combination of surgeries is very much beneficial to the patients in terms of money, time and a little bit of restless or discomfort associated with the surgery. Surgeon needs not to give anaesthesia twice, so this is an advantage of combination surgery.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefif">Can I Do Some Exercises After This Surgery?</a> </div>
                        <div id="collapsefif" class="collapse" data-parent="#accordion">
                            <div class="card-body">Our surgeons will advise you and our trained staff will guide you properly because type and nature of exercise depends upon the area and location reshaped. But after few weeks of the procedure you are generally free to do most of exercises.</div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseelxe">Besides The Aesthetic Advantage, Are There Any Benefits For Carrying Liposuction Plastic Surgery?</a> </div>
                        <div id="collapseelxe" class="collapse" data-parent="#accordion">
                            <div class="card-body">Liposuction helps in getting well defined contour. Due to loss of weight, stress and undue pressure on bones and internal organs decreases. One can achieve brisk physical activity. Enhanced mobility of body allows doing more but different types of exercises. Lighter body having young, slim and fresh look gives a feeling of immense pleasure.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsetewld">Are There Some Complications In This Surgery?</a> </div>
                        <div id="collapsetewld" class="collapse" data-parent="#accordion">
                            <div class="card-body">As such there are no complications in liposuction surgery except common risks associated with any kind of other surgery. Swelling and bruising however occurs but diminishes gradually and slowly by the passage of time.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsethre">Are The Results Of Liposuction Permanent?</a> </div>
                        <div id="collapsethre" class="collapse" data-parent="#accordion">
                            <div class="card-body">Yes, As long as you control over your diet but when you take excess of fatty diet then already present small adipocytes (a type of fat storing cells) grow large by storing fats. Also it is neither in welfare of any individual to remove more of fat and fatty tissue from body nor is it possible to do so because the fat molecules are vital and essential parts of human organs too. Removing excess of fat may harm the body.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseforc">Is Liposuction A Major Surgery?</a> </div>
                        <div id="collapseforc" class="collapse " data-parent="#accordion">
                            <div class="card-body">No, it is not major but minimally invasive surgery.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefifd">Can My Skin Become More Loose After Liposuction Plastic Surgery?</a> </div>
                        <div id="collapsefifd" class="collapse" data-parent="#accordion">
                            <div class="card-body">No, our skin has capacity to adjust and aligned with new contour because of its elastic nature. Moreover some exercises pertaining to the treated part of the body helps in gaining firm shape of body.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="col-md-12 text-center">
                    <h2>Areas treated with Liposuction</h2>
                </div>
                <div class="row text-center pt-3">
                    <figure><img data-src="<?= cdn('assets/template/frontend/images/'); ?>liposuction-Male-and-Female.jpg" alt="liposuction-Male-and-Female" class="img-fluid lazy"> <p>AK Clinics is a well established clinic with all the latest and modern surgical equipments to give you a beautiful, balanced and slim look that attracts everybody. Enhanced aesthetic look brings charm and glamour to your personality.</p>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area bg-light-gray">
    <?php $this->load->view('front/partials/cosmetic_surgery_blogs'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>