<style>
    .background-one{background: #f0ebd8; opacity:0.9}
    .custom-padding {padding-top: 60px !important;padding-right: 40px !important;padding-bottom: 0px !important;padding-left: 0px !important;}
    a.slide-over{ opacity:0.8; background:#ccc;}
    a.slide-over:hover{ opacity:1;}
    .addReadMore.showlesscontent .SecSec,.addReadMore.showlesscontent .readLess {display: none;}
    .addReadMore.showmorecontent .readMore {display: none;}
    .addReadMore .readMore,.addReadMore .readLess {margin-left: 2px;cursor: pointer;}
    .addReadMoreWrapTxt.showmorecontent .SecSec,.addReadMoreWrapTxt.showmorecontent .readLess {display: block;}
</style><?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> 
                        <a itemprop="item" href="<?= base_url(); ?>cosmetic-surgery/"> <span itemprop="name">Cosmetic Surgery</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area"> 
    <div class="jumbotron">
        <div class="container text-center">
            <h1 class="black-text">Cosmetic Surgery</h1>
            <p class="ptb-1">There was once a time, when you had to live with the way you looked – whether you had a crooked nose or very small breasts, there was little you could do about it. However, with advances in science, doctors now have the power to actually change the way you look and bring it to closer to the way you want to look. At AK Clinics, our team of doctors will help rectify whatever maybe your concern – your small or large breasts, nose, man boobs, white patches or even your eyelids, we will have the perfect solution for you. The cosmetic surgery procedures that you can avail of at AK Clinics include:</p>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-orange">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="regular slider">
                <div>
                    <div class="card mb-3 box-shadow">

                        <img data-src="<?= cdn('assets/template/frontend/img/'); ?>service-image-6.jpg" class="card-img-top img-fluid lazy" alt="Gynecomastia Surgery">
                        <div class="card-body text-center"> <strong>Gynecomastia</strong>
                            <p class="card-text addReadMore showlesscontent">While breasts might be assets for women, should the same present itself in men, there could be nothing more embarrassing. However man boobs or gynecomastia is a reality and there are several men, who are struggling with the same. Step into the offices of AK Clinics and our doctors will make sure that you have a body that you are proud of! Not only will we carefully remove the breast tissue, but also make sure that there are minimal scars and the healing time is also reduced. Our skills in this procedure have helped numerous men regain their confidence.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>gynecomastia/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>service-image-5.jpg" alt="Liposuction">
                        <div class="card-body  text-center"> <strong>Liposuction</strong>
                            <p class="card-text addReadMore showlesscontent">Excess weight is normally a problem for most people and while exercise and a proper diet could help control the same, most people are unable to do it. Then, in some cases there is too much fat and cellulite that accumulates inside the body. The only method of removing the same, then becomes via liposuction and we at AK Clinics hold an expertise in the same. Using state of the art tools, we will ensure that your body goes from flabby to fit in no time. In addition, we have perfected our science so much that once the procedure has been completed, you will be left with minimal stretch marks</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>liposuction/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline ">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>vitiligo-img.jpg" alt="Vitiligo Surgery">
                        <div class="card-body  text-center"> <strong>Vitiligo Surgery</strong>
                            <p class="card-text  addReadMore showlesscontent">Everyone wants to be fair, but no one wants to have white patches on their skin. This is actually a condition known as vitiligo and there are actually scientific methods of treating the same and at AK Clinics our team of doctors is one of the best. In our surgical treatment for vitiligo, we will aim to replace the melanocytes, so that your skin goes back to normal in no time. We also offer tissue grafting, which is emerging as one of the most popular treatments for this condition. Considering factors such as age, stability of the condition and size of the patch, our doctors will be able to suggest the best course of treatment.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>vitiligo-treatment/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline ">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>breast-reduction-surgery.jpg" alt="Breast Reduction Surgery">
                        <div class="card-body  text-center"> <strong>Breast Reduction Surgery</strong>
                            <p class="card-text  addReadMore showlesscontent">While breasts might be the best assets for a woman, at times, they are simply too large to handle. Very large breasts can lead to serious health problems such as back pains, neck pains, infections in the creases that form under the breasts and bra straps cutting into the shoulder skin. If you too have been suffering from all these problems, because of large breasts, walk into AK Clinics and with our breast reduction surgery, you will be able to walk straight again! Our surgical techniques are such that your breast will be reduced to a size that is healthiest for you and you will have minimal scarring.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>breast-reduction/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline ">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>service-image-2.jpg" alt="Breast Augmentation Surgery">
                        <div class="card-body  text-center"> <strong>Breast Augmentation Surgery</strong>
                            <p class="card-text  addReadMore showlesscontent">Learn More Get Online Consultation
                                Large breasts might be a problem, but small breasts have their own set of issues. Many women are disappointed with the size of their breasts, because they feel that no clothes look good on them or they have no cleavage at all. At AK Clinics, we can increase your breast size to whatever size you wish; however, we will suggest that best size for your body shape and height too. Our aim is to make sure that while you look great, you are still healthy and at no health risks. We utilise saline as well as silicone implant, the choice of which will be dependent on an in-depth assessment.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>breast-augmentation/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline ">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>rhinoplasty-img.jpg" alt="Rhinoplasty Surgery">
                        <div class="card-body  text-center"> <strong>Rhinoplasty</strong>
                            <p class="card-text  addReadMore showlesscontent">Your nose gives definition to your face and should that be out of proportion, it would be normal for you to lose your confidence. At AK Clinics, we strive to help our patients to regain their confidence by helping them rebuild or reshape their nose. By taking cartilage from the ear or ribs, we will be able to completely rebuild your nose, ensuring that it is in proportion with the rest of the face. Even if you have trouble breathing, our rhinoplasty procedures would be of help. The reason why we have become the popular choice for many people is because our surgical procedure leaves you with minimal scars.</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>rhinoplasty/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline ">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="card mb-3 box-shadow"> 
                        <img class="card-img-top img-fluid lazy" data-src="<?= cdn('assets/template/frontend/img/'); ?>blepharoplasty-img.jpg" alt="Blepharoplasty">
                        <div class="card-body  text-center"> <strong>Blepharoplasty</strong>
                            <p class="card-text  addReadMore showlesscontent">They say eyes are the windows to the soul, but imagine eyes without their lids. Eyelids are just as important, because they actually provide essential protection to the eyes. However, not all people are born with proper eyelids and then there are also those people who lose their eyelids to accidents or even certain medical conditions. For such people, we offer blepharoplasty, by means of which we will be able to rebuild or redefine your eyelids to make sure that your eyes always have a lid to keep them safe!</p>
                            <div class="d-flex justify-content-between align-items-center card-box-btn">
                                <div class="btn-group">
                                    <a href="<?= base_url(); ?>blepharoplasty/" target="_blank">
                                        <button type="button" class="btn btn-sm btn-outline ">Read More <i class="icofont-long-arrow-right"></i></button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></div> 
    </div></div>


<div class="divider parallax layer-overlay overlay-dark-5" data-bg-img="<?= cdn('assets/template/frontend/img/'); ?>dr-profile-cover.jpg" style="background-image: url(&quot;<?php echo cdn('assets/template/frontend/'); ?>img/dr-profile-cover.jpg&quot;); ">
    <div class="container pt-0 pb-0">
        <div class="section-content">
            <div class="row">
                <div class="col-md-6 sm-height-auto">
                    <div class="background-one">
                        <h4 class="title mt-0 line-bottom line-height-2 text-black-222">BEGINNING OF THE NEW<span class="text-theme-colored"> AGE</span></h4>
                        <b class="small-separator"></b>
                        <div class="dr-aman-quote">
                            <blockquote>
                                <p> Our aspiration to become India's leading hair & skin clinic has driven us to provide best-in-class equipment & experienced doctors. This is the reason we had been consistently giving great results. We now aspire to take this to all major cities of India. </p>
                                <p><strong>Dr. Aman Dua</strong>
                                    <br>

                                    <br>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ptb-3 doctors-details-section text-white">
                    <h2>Our Complete List Of Procedures</h2>
                    <div class="row drlist">
                        <div class="col-md-6">
                            <ul class="">
                                <li><a href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty Surgery</a></li>
                                <li><a href="<?= base_url(); ?>gynecomastia/">Gynecomastia Surgery</a></li>
                                <li><a href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty Surgery</a></li>
                                <li><a href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation Surgery</a></li>
                                <li><a href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynecology</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="">
                                <li><a href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty Surgery</a></li>
                                <li><a href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Surgery</a></li>
                                <li><a href="<?= base_url(); ?>liposuction/">Liposuction Surgery</a></li>
                                <li><a href="<?= base_url(); ?>breast-reduction/">Breast Reduction Surgery</a></li>
                                <li><a href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetic_surgery_blogs'); ?>
<div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
<script>

    $(document).on('ready', function () {
        $(".regular").slick({
            dots: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
</script> 