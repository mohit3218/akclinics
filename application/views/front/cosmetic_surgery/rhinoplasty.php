<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> 
                        <a itemprop="item" href="<?= base_url(); ?>cosmetic-surgery/"> <span itemprop="name">Cosmetic Surgery</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>rhinoplasty/"> <span itemprop="name">Rhinoplasty</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">What is Rhinoplasty Treatment?</h1>
                </div>
                <p>In very simple terms, rhinoplasty or nose job is the surgery by which the shape of the nose can be changed. The surgery could be conducted for cosmetic or medical purposes, such as better breathing. The procedure can be used to improve the bone, the cartilage, the skin or the nose, or even all three of them. The rhinoplasty surgery cost in India depends on many factors – extent of the job, technique used, kind of problem like saddle nose or crooked nose, the surgeon and location.</p>
                <p>Though the costs are very affordable specially at our clinic in Delhi and Ludhiana – the broad range is around INR 75000 or USD 1300 for moderate work. Rhinoplasty Surgery can achieve the following changes:</p>
                <ul>
                    <li>Nose width or position of nostrils</li>
                    <li>Size in proportion to the face</li>
                    <li>Significantly improving crooked or saddle nose</li>
                    <li>Any asymmetry of the nose</li>
                    <li>Any visible humps or depressions of the nose</li>
                </ul>

                <h4>Why Rhinoplasty</h4>
                <p>A rhinoplasty or a nose is a cosmetic plastic surgery procedure to reshape or resize your nose. Are you a candidate? want to be the one? find few reasons why you should go for it.</p>
                <figure class="pt-1"><img data-src="<?= cdn('assets/template/frontend/images/'); ?>why-rhinoplasty.png" class="img-fluid lazy" alt="Best Rhinoplasty Clinic in India"></figure>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetic-surgery/">Cosmetic Surgery</a></li>
                        <li><a href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia/">Gynecomastia Surgery</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Surgery</a></li>
                        <li><a href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>liposuction/">Liposuction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-reduction/">Breast Reduction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynecology</a></li>
                        <li><a href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text text-center">Types of Rhinoplasty</h2>
                    <p>There are actually two types of rhinoplasty – open and closed, each has its own set of pros and cons and the decision on which need to be done for whom, depends completely on the diagnosis of the surgeon. It is the approach while making the incision that decides whether the surgery is an open one or a closed one.</p>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Open Rhinoplasty</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Open Rhinoplasty</h3>
                    <p>A small incision is made on the columella, which is the exterior part of the septum. This incision is made in addition to the normal incisions that would be made for such a procedure.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Closed Rhinoplasty</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Closed Rhinoplasty</h3>
                    <p>The entire procedure is performed inside the nose and there are no cuts or incisions on the columella.It is the mainly the columellar incision that differentiates the open and closed versions of the rhinoplasty, however, there are a few other differences in the closed version, such as:</p>
                    <ul>
                        <li>Lesser incisions, which means lesser sutures</li>
                        <li>More possibilities for technical variations</li>
                        <li>Lesser edema post-surgery</li>
                        <li>Lesser chances of causing unwanted damage to the nasal area</li>
                        <li>Lesser time in the operating room and faster recovery</li>
                        <li>Lesser scarring</li>
                    </ul>
                </div>
                <div class="tab">
                    <button class="tab-toggle">There is yet another categorisation when it comes to rhinoplasty:</button>
                </div>
                <div class="content">
                    <h3 class="m-3">There is yet another categorisation when it comes to rhinoplasty:</h3>
                    <strong>Primary</strong>
                    <p>The very first time a rhinoplasty is conducted for either better functioning, reconstruction or correction.</p>
                    <strong>Secondary</strong>
                    <p>When a second rhinoplasty has to be conducted, because the primary surgery did not have the desired results. This is also known as revision rhinoplasty. Normally, secondary rhinoplasty procedures are much more complicated, because for the first one, the actual structure of the nose would have been altered. So, in order to have the desired results, the surgeon will have to recreate the support systems from scratch. The cartilage for the grafts might have to be harvested from the rib cage or the ears.</p>
                    <strong>Nasal Reconstruction</strong>
                    <p>As the name suggests, this type of surgery is about reconstructing what might have gotten damaged. The damage could include displaced or broken bones in the nose, displaced cartilage, collapsed nasal bridges, defects that have been present since birth, autoimmune disorders, trauma or accident as well as failure in the primary surgery.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Then there is also something known as non-surgical rhinoplasty:</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Then there is also something known as non-surgical rhinoplasty:</h3>
                    <p>Non-surgical rhinoplasty is yet another medical procedure, in which fillers are used, which can be injected into the nose. These fillers can either change or simply shape the nose, without any incisions or sutures. If there are any depressed areas of the nose, these fillers are injected to create a rise. This means that this procedure can be used only to increase the size of the nose, but not reduce it. At times, the procedure is used to ease out breathing difficulties.</p>
                </div>

                <div class="tab">
                    <button class="tab-toggle">Before Rhinoplasty Surgery</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Before Rhinoplasty Surgery</h3>
                    <p>Before your surgeon can get you started on your procedure, you will be asked several questions about your health in general. Your medical history will be looked into, in detail, and if you have or have had any serious medical condition, you will need to mention the same.<br />
                        You will have to undergo a thorough medical examination to ensure that you are physically fit for the procedure. These tests will include blood tests, heart functions and sugar levels. You blood will be checked and your medications will also have to be monitored.<br />
                        If you are a smoker, you will be asked to refrain from doing so, a few weeks prior to the surgery. You might also be asked to avoid medications such as aspirin, as these could lead to increased bleeding.</p>
                </div>

                <div class="tab">
                    <button class="tab-toggle">During Rhinoplasty Surgery</button>
                </div>
                <div class="content">
                    <h3 class="m-3">During Rhinoplasty Surgery</h3>
                    <ul class="list-unstyled margin-bottom-20">
                        <li><strong>Step 1 –</strong> Anaesthesia – The patient will be administered either general anaesthesia or sedation to ensure complete comfort during the procedure.</li>
                        <li><strong>Step 2 –</strong> Incision – Irrespective of whether the surgeon is performing an open or closed rhinoplasty, there will be incisions. The only difference is where the incisions will be placed. Once the incisions have been made, the surgeon will be able to raise the nasal bones and cartilage.</li>
                        <li><strong>Step 3 –</strong> Reshaping – If the nose is too large, some of the bone or cartilage will be removed. Alternately, if the nose is too small, grafts will have to be taken from other parts of the body. Grafts can be taken from the septum, rib cage or ears.</li>
                        <li><strong>Step 4 –</strong> Correcting a deviated septum – If it is the septum that has suffered damage, then the same is straightened out.</li>
                        <li><strong>Step 5 –</strong> Closing the incision – Inside or outside, the incisions are then closed up. The nose will have been sculpted to the desired shape, and the skin will be pulled up before closure.</li>
                    </ul>
                </div>

                <div class="tab">
                    <button class="tab-toggle">After Rhinoplasty Surgery</button>
                </div>
                <div class="content">
                    <h3 class="m-3">After Rhinoplasty Surgery</h3>
                    <p>Most surgeons will place a splint or some sort of bandaging inside your nose, immediately after the surgery, to ensure that the repaired portion has sufficient support. These supports will be left inside till the time the healing is complete. Although the initial swelling will reduce in a matter of days, the actual reshaping will take time to settle in. Over a period of time, the shaping will start to become more obvious.</p>
                    <p>There are specific things that need to be kept in mind while recovering from rhinoplasty and these include:</p>

                    <ul class=" margin-bottom-20">
                        <li>You will be told how to clean and care for the site, immediately after surgery</li>
                        <li>You will need to make sure that you take all your medications on time, including the oral and topical ones</li>
                        <li>Your doctor will advise you on how to keep a look out for potential infection or trouble</li>
                        <li>You will also be advised about when to meet the doctor for regular follow-ups</li>
                        <li>It is just as important that you ask your doctor about how and when you can return to your normal routines, including exercise</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Will I Have A Perfect Nose, Post-Surgery?</a> </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">There is no guarantee that you will have a perfect nose, but yes, you can get closer to your ideal nose. As a matter of fact, a good surgeon will never promise perfect results or raise your hopes too high.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">What Happens If I Am Not Happy With The Rhinoplasty?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">In case you are not happy with the results of your first surgery, you can always opt for another surgery, wherein the focus will be only on correcting what does not seem appropriate.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Could I Have Trouble Breathing?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">Yes; there is quite a chance that you might have trouble breathing after the surgery and if the same does happen, it would be best that you meet a doctor immediately</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Will I Need To Reveal My Medical History To My Surgeon?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">Absolutely – your surgeon will need to know what medications you have been taking, including vitamins and supplements. You will also have to tell them about any surgeries that you might have had.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">When Will I Be Able To Return To A Normal Routine?</a> </div>
                        <div id="collapsefive" class="collapse" data-parent="#accordion">
                            <div class="card-body">You should be able to go home the same day or the next day, but you will not be able to resume exercise or any strenuous activity for a while. Your doctor will be able to advice you better on the same.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>Possible Risks</h2>
                    <p>As is the case with any surgery, there are a few risks with rhinoplasty too, and some of them include:</p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                            <li>Bleeding and recurring nosebleeds</li>
                            <li>Septal perforation</li>
                            <li>Infection or allergy to medical material used including anaesthesia</li>
                            <li>Uneven looking nose</li>
                            <li>Breathing trouble</li>
                            <li>Pain or swelling that does not go down</li>
                            <li>Loss of sensation in or around the nose</li>
                            <li>Scars</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area bg-light-gray ">
    <?php $this->load->view('front/partials/cosmetic_surgery_blogs'); ?>
</div>
<?php $this->load->view('front/partials/location_block'); ?>
<div class="service-area  bg-light-gray ">
    <?php $this->load->view('front/partials/doctor_contacts'); ?>
</div>