<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> 
                        <a itemprop="item" href="<?= base_url(); ?>cosmetic-surgery/"> <span itemprop="name">Cosmetic Surgery</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="3" />
                    </li>
                    <li class="disabled  anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>cosmetic-gynaecology/"> <span itemprop="name">Cosmetic Gynaecology</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text ">Cosmetic Gynecology</h1>
                </div>
                <p>For most women, being attractive is what brings them a sense of confidence and being confident makes them feel sexually powerful too. However, in certain cases, the sexual confidence dips because their genital parts are either not perfectly formed or have been damaged. There are also cases where with age, the genitalia starts to become loose. The medical method of managing most of these kinds of cases would be vaginal cosmetic surgery. The most common surgeries are vaginal tightening surgery to tighten the vaginal walls and Re-virginity Surgery for hymen replacement.</p>
                <figure class="pt-1"><img data-src="<?= cdn('assets/template/frontend/images/'); ?>vaginal-cosmetic-surgery.jpg" class="img-fluid lazy" alt="Hymenoplasty surgery clinic in delhi, India"/></figure>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetic-surgery/">Cosmetic Surgery</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia/">Gynecomastia Surgery</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Surgery</a></li>
                        <li><a href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>liposuction/">Liposuction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-reduction/">Breast Reduction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3  bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="section-title text-center">
                    <h2 class="black-text">Types of Cosmetic Gynecology</h2>
                    <p>There are several types of surgeries, under the umbrella of vaginal cosmetic surgery and some of them include:</p>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle ">Labiaplasty</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Labiaplasty</h3>
                    <p>A procedure used to reduce the inner vaginal lips or enlarge the outer lips, using fat. If there is excess skin, it would be visible as a bulge and this can often cause embarrassment while wearing tight clothes or swimwear.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Hymenoplasty</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Hymenoplasty</h3>
                    <p>If the hymen gets ruptured, the process of repairing the same is known as hymenoplasty. The hymen is actually a membrane that covers the vagina and can be ruptured due to several reasons including sexual intercourse for the first time, rough sports and horse riding.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Vaginoplasty</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Vaginoplasty</h3>
                    <p>This is a procedure that can tighten as well as strengthen the muscles and tissues of the vaginal canal. The procedure can also be used to improve the function of the vulva-vaginal body structures.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Colovaginoplasty</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Colovaginoplasty</h3>
                    <p>This method is used to remove a section of the sigmoid colon, and then be used to create a vagina. The vascular pedicle from the colon is used to create the vaginal tissue. The newly created vagina will have good depth and is the ideal choice for women who have androgen insensitivity syndrome.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Balloon vaginal surgery</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Balloon vaginal surgery</h3>
                    <p>One of the fastest methods to create a new vagina, the procedure is done laparoscopically. By inserting a foley catheter into the rectouterine pouch, the balloon is distended and traction is applied at the same time.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Clitoral hood reduction</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Clitoral hood reduction</h3>
                    <p>The basic idea behind this procedure is to reduce the size of the clitoral hood; however the same procedure is also used to improve sexual function.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Buccal mucosa surgery</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Buccal mucosa surgery</h3>
                    <p>A comparatively new procedure, this surgery allows surgeons to utilise the tissues from inside the cheek to create a new lining for the vagina. The lining is put into place using dissolvable sutures and entire labia minoras can be created using this method.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Vecchietti procedure</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Vecchietti procedure</h3>
                    <p>This procedure is used to treat a condition known as Müllerrian agenesis. A small plastic sphere is inserted against the sutures, and the threads attached to the sphere are pulled through the vaginal skin and abdomen to be attached to a traction device. The mechanism allows for the stretching of the vagina to a size that is required.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">McIndoe technique</button>
                </div>
                <div class="content">
                    <h3 class="m-3">McIndoe technique</h3>
                    <p>A space in created between the bladder and rectum, a mould covered with skin graft is inserted into the space created</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center ">
                    <h2 class="black-text">Before the Cosmetic Gynecology</h2>
                  

                </div>
            </div>
        </div>
        <div class="row text-left ptb-1">
            <div class="col-md-4 "> <h4> Before the surgery</h4>
                <p>As is with any type of surgery, you will be asked to undergo extensive examination, because the surgeon will need to not only assess the condition that you might be suffering from, but also decide the precise course of action. You will be asked to undergo some standard tests, such as blood tests and the surgeon will have to ensure that you are in good enough health to undergo the decided procedure. <br />
                    <br />
                    If you are on any kind of medications, your doctor will want to know about the same, because certain medications will either have to be stopped or replaced for the purpose of the surgery. You will also be explained, in detail, exactly what will happen during the surgery, so that you consent with full knowledge. You will also be asked to give up smoking, a month before the procedure. Since the procedures are going to be done on or inside the vagina, the menstrual cycle has an important role to play – generally, no procedures will be done with there is bleeding. </p>
            </div>
            <div class="col-md-4 "> <h4>During the surgery</h4>
                <p> The actual surgical procedure for each vaginal cosmetic surgery is different. For instance, in a hymenoplasty, once the anaesthetic has taken effect, the surgeon will, in all probability utilise lasers to repair the hymen. However, if the procedure is a Labiaplasty, then either the labia will be trimmed down to reduce the size or fat from the body will be used to plump it up.<br />
                    <br />
                    For process such as balloon vaginal surgery and the Vecchietti procedure, surgeons will go in laparoscopically, which means lesser incisions and sutures, which in turn will mean lesser scars. However, in most procedures, incisions will have to be made to remove excess tissue or skin; the remaining skin or tissue will be pulled in tightly and dissolvable sutures will be placed to allow for the healing process to start.</p>
            </div>
            <div class="col-md-4 "> <h4>After the surgery</h4>
                <p>As soon as the surgery is over, bandages and dressings will be applied on the outside. Most of the times, such procedures are over and done with, in a few hours, which means that you should be able to go home the same day. You will not be asked for bed rest for more than a few days, and you will be asked to start walking around a few days post-surgery, in order to avoid blood clots. <br />
                    <br />
                    In most cases, women are able to return to their normal routines in about a week, but sexual intercourse can be resumed only after the surgeon sees fit. If you have undergone a hymenoplasty, you can expect a little bleeding at the time of the first intercourse. In other procedures, there can be swelling, bruising, redness and even itching, most of which are completely normal. You will be advised to take your medication on time, wear slightly loose clothing and not pick up any heavy weights for a few weeks.</p>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Will It Be Painful?</a> </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">Almost all the vaginal cosmetic surgeries are done under anaesthesia or sedation, there will be some amount of pain and discomfort, once the effects wear off. There might be difficulty while sitting, urinating and the first intercourse, post-surgery might be quite painful.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Will The Effects Be Permanent?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">In most cases, they are, but factors such as age, sexual activity and indulgence in other physical activities such as sports will determine the longevity.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Will Sexual Ability Improve?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">Most women, especially those who have undergone vaginal tightening have said that they have enjoyed sexual intercourse more, post-surgery. Their partners have also spoken about greater friction, which allows them to maintain erection for longer.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Are Such Surgeries Covered By Insurance?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">Unfortunately, most of the times, vaginal surgeries are considered cosmetic, which is why insurance will not cover the same. However, if the surgery is medically required and the aim is to repair damage caused due to cancer or an accident, then perhaps the insurance company would consider covering it.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefourx">How Long Will It Take To Return To The Normal Routine?</a> </div>
                        <div id="collapsefourx" class="collapse " data-parent="#accordion">
                            <div class="card-body">In most cases, women are able to return to a normal routine in about 3 weeks, although sexual activity might have to be abstained from, for a little longer.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" id="testimonails">
                <div class="col-md-12 text-center">
                    <h2>Possible Risks</h2>
                    <p>Ideally, when cosmetic gynaecology has been conducted by an experienced surgeon, there are little to no chances of things going wrong. However, there are some complications that can be associated with this procedure, such as:</p>
                </div>
                <div class="row">
                    <div class="col-md-12 ptb-1">
                        <ul><li>Normally, a little extra fluid build-up post-surgery is normal and a drain might be placed to there could be a loss of drain the same out. However, in certain cases, there is too much fluid accumulation.</li>

                            <li>Pain that does not reduce</li>
                            <li>Changes in sensation in the genital area</li>
                            <li>Since the breasts are so close to the lungs and heart, there is always the chance that they might be damaged during surgery.</li>

                            <li>Dyspaureunia, which refers to pain during sexual intercourse</li>

                            <li >Bleeding, infection and scarring</li>
                            <li>There could be a change or complete loss of sensation in the nipples or even the entire breast</li>
                            <li>There is the possibility of damage to the nerves or the muscles in the breast area.</li>
                            <li>Adhesions in the vaginal tissues</li><li>Hypersensitivity in the clitoris</li>
                            <li>The skin around the breast could become discoloured or there could be pigmentation that does not fade away with time</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetic_surgery_blogs'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>