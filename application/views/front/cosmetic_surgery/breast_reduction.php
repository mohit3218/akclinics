<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> 
                        <a itemprop="item" href="<?= base_url(); ?>cosmetic-surgery/"> <span itemprop="name">Cosmetic Surgery</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>breast-reduction/"> <span itemprop="name">Breast Reduction</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Breast Size Reduction</h1>
                </div>
                <p>Breast reduction surgery, also known as reduction mammoplasty or breast size reduction, is a process using which some of the tissue and skin is removed from the breasts, allowing a surgeon to restructure, reshape and reduce the size of a breast. The same process can also be used to reduce the size of the areola (the dark area around the nipple).</p>
                <p>It is important to understand that breast reduction and breast lift are two different things, because in the latter breasts are only tightened and the nipple area is elevated. Also it is important to understand inherent surgery risks with the procedure but generally this is a routine procedure.</p>

                <figure><img data-src="<?= cdn('assets/template/frontend/images/'); ?>breast-reduction-infographics.png" class="img-fluid lazy" alt="Breast Reduction Clinic in Delhi, India"/></figure>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetic-surgery/">Cosmetic Surgery</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia/">Gynecomastia Surgery</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Surgery</a></li>
                        <li><a href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>liposuction/">Liposuction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation Surgery</a></li>
                        <li><a href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynecology</a></li>
                        <li><a href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title">
                    <h2 class="black-text text-center">Types of Breast Reduction</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12 ptb-1">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Traditional or ‘anchor’ breast reduction</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Traditional or ‘anchor’ breast reduction</h3>
                    <p>This is most the standard method and the primary incision is made around the nipple. The shape of the incision is almost like an anchor and hence the name. Opening up the breast will allow for easy removal of fat, tissue and excess skin. The nipple and areola will be relocated to ensure proper aesthetics.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle ">Pedicle Method</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Pedicle Method</h3>
                    <p>Another common method, wherein the breast size is reduced, but the function and sensation in the nipples as well as the breast are maintained completely.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Free Nipple Graft</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Free Nipple Graft</h3>

                    <p>The nipple is removed first and then on its new location, a skin graft is used to attach the same. The blood supply for the newly placed nipple is taken through the deep dermis.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Vertical incision breast reduction surgery</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Vertical incision breast reduction surgery</h3>
                    <p>A comparatively new technique, this is far less invasive, which means lesser scars and faster recovery. The incision made will be in the shape of a lollipop and once the excessive tissue and fat have been removed, the skin is overlapped over the incision and stitched up.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Scar-less breast reduction</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Scar-less breast reduction</h3>
                    <p>Through very small incisions, and by using liposuction, excess fat will be sucked out. The incision will be tiny and this is an ideal choice for women who do not have excessively large breasts.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Stevens Laser Bra</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Stevens Laser Bra</h3>
                    <p>This is the most recent of the methods and aims are creating an “internal bra”, instead of simply discarding the skin. An incision will be made and a laser bra will be attached to the chest, providing permanent support.</p>
                </div>

                <div class="tab">
                    <button class="tab-toggle">Breast Reduction Advantages</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Breast Reduction Advantages</h3>
                    <figure><img class="img-fluid lazy" data-src="<?= cdn('assets/template/frontend/images/'); ?>breast-reduction-advantages.png" alt="Advantages of Breast Reduction Surgery"></figure>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 ">
    <div class="container">
        <div class="row">
            <div class="section-heading text-center ">
                <h2 class="section-title black-text">Breast Reduction</h2>
                <p>Our highly qualified plastic surgeons at AK Clinics routinely perform breast reduction of patients suffering from this condition. AK Clinics is equipped with ultra modern equipments with all facilities and state-of-art-technology. Moreover our doctors are highly sensitive to your feelings and emotions and ensures you of masculine look through this surgery.</p>
            </div>
        </div>
        <div class="row text-left ptb-1">
            <div class="col-md-6 ">
                <div class="col-sm-12 ">
                    <div class="col-lg-12 ">
                        <h3>Before the Surgery</h3>
                        <p>Preparing for a breast reduction surgery goes on at several levels – your surgeon will examine you physically to make sure that there are no issues with your getting a breast reduction done. Your hip size will play an important role in determining your new breast size. The surgeon should also advice you about the possible risks and side effects of such procedures.
                        </p><p>Your doctor will ask you for your medical history and ensure that you have no serious medical conditions. The breasts will also be examined, because this will allow the surgeon to decide the best surgical approach. In certain cases, the doctor might also ask you to get an x-ray or mammogram, in addition to blood tests, pregnancy test and clotting tests.</p>
                    </div>
                    <div class="col-lg-12 pt-1 ">
                        <h3>After the Surgery</h3>
                        <p>There is bound to be some pain in the days immediately after the breast reduction surgery. However, the doctor will prescribe some pain relief medication, which should be taken as per the prescription. The pain and swelling should subside in the days to come, but the discomfort might last a little longer. As a matter of fact, the doctor will suggest that you not indulge in any strenuous physical activities for close to a month, including lifting heavy objects, stretching your arms too high and playing any vigorous sports.
                        </p><p>All this time, it is essential that you wear a supporting bra, because your breasts need to be protected. In addition, there will be scars that you need to be prepared about. While these scars will fade over time, chances of them disappearing completely are minimal.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="col-sm-12 ">
                    <h3>During the Surgery</h3>
                    <p>In simple terms, the breast is cut open, excess tissue is removed, the skin is pulled and stitched back together. In many cases, cutting the breast is not required, because if all the excess tissue is only fat, it can be removed in a liposuction like procedure.
                        <strong>There are basic four steps in a breast reduction procedure:</strong></p>
                    <ul class="text-left pt-1">
                        <li>Step 1: Anaesthesia – Patients are normally administered anaesthesia or sedated, which means that they will not feel any pain or discomfort during the procedure.</li>
                        <li>Step 2: Incision – The surgeon will make incisions in pre-determined areas, allowing them to take a closer look at the tissue.</li>
                        <li>Step 3: Removal and repositioning of tissue – All excess tissue will be removed, ensuring that the breast size is reduced. The nipple will have to be repositioned and the entire breast will be pulled up.</li>
                        <li>Step 4: Closure – The incisions will be carefully closed, while the skin is pulled tight, to give the breast a feeling of firmness and tightness.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Is A Breast Reduction Surgery Permanent?</a> </div>
                        <div id="collapseOne" class="collapse " data-parent="#accordion">
                            <div class="card-body">Breast reduction surgery is permanent, provided you keep a check on your body weight. You need to remember that if you gain weight, so will your breasts. There is the possibility that during pregnancy, there is a change in the breasts again, because of the swelling of the glands storing milk and other fluids. Women on hormonal supplements will also have trouble maintaining breast size, as well as girls who are still in their teens.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Will There Be A Lot Of Pain And Bruising?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">It is a surgery after all, which means that there is bound to be some amount of pain, but the actual procedure is always done under sedation or anaesthesia, which means that you will be numb during the surgery. Normally, it takes a little over a week for the swelling to reduce and bruising is actually dependent on the person, because while some women have a lot of bruising, others have minimal amounts. Your doctor might suggest some topical ointments to help with the swelling as well as bruising. You will also be given medications to alleviate the pain and discomfort.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">How Soon Will I Be Able To See The Results?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">In most cases, women are able to go back to their normal life, in a matter of 15 days, however, there have been cases, where women have returned to normalcy in less than a week. The reason why people take time to get back to their daily lives is the pain, or rather, discomfort that ensues for a few days, post-surgery. Your doctor will tell you that you should not bend over, lift heavy things, raise your arms too high or indulge in strenuous exercise for a minimum of three weeks.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Will My Insurance Company Cover The Procedure?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">Breast reduction is often suggested in order to alleviate back pain, bra straps cutting into the shoulder, improper posture or even constant leg pains, because of the heavy upper body. Due to such reasons, several insurance companies actually allow breast reduction surgery.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" id="testimonails">
                <div class="col-md-12 text-center">
                    <h2>Possible Risks</h2>
                    <p>Ideally, when brest reduction has been conducted by an experienced surgeon, there are little to no chances of things going wrong. However, there are some complications that can be associated with this procedure, such as:</p>
                </div>
                <div class="row ptb-1">
                    <div class="col-md-12">
                        <ul>
                            <li>Perhaps the biggest risk with such a surgery is that the woman might not be able to breastfeed in the future</li>
                            <li>There is bound to be scarring, but at time, the scars become too obvious</li>
                            <li>There is the possibility of infections or blood clots</li>
                            <li>If the anaesthesia has not been administered properly, it could lead to further complications</li>
                            <li>The inexperience of the surgeon could lead to the lack of balance between the two breasts or irregular placement of the areolae</li>

                            <li>Normally, a little extra fluid build-up post-surgery is normal and a drain might be placed to there could be a loss of drain the same out. However, in certain cases, there is too much fluid accumulation.</li>
                            <li>There could be a change or complete loss of sensation in the nipples or even the entire breast</li>
                            <li>There is the possibility of damage to the nerves or the muscles in the breast area.</li>
                            <li>Since the breasts are so close to the lungs and heart, there is always the chance that they might be damaged during surgery.</li>
                            <li>The skin around the breast could become discoloured or there could be pigmentation that does not fade away with time</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetic_surgery_blogs'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>


