<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> 
                        <a itemprop="item" href="<?= base_url(); ?>cosmetic-surgery/"> <span itemprop="name">Cosmetic Surgery</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= base_url(); ?>male-genital-surgery/"> <span itemprop="name">Male Genital Surgery</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title">
                    <h1 class="black-text text-left">Male Genital Surgery</h1>
                </div>
                <p>For centuries, men have compared their sexual prowess to their strength – their virility is what matters to them the most, and while today, the talks about the same might have become a little more subdued, the thought process is still pretty much the same. This is why, when a man finds something wrong with his penis, he feels like the world is crumbling down, around him.<br /><br />
                    There can be a range of problems that can affect the male penis, and these could be congenital or due to an accident or trauma. Developmental disorders of the genitals, curves caused by diseases, adjustment in the form of the foreskin, implants for testicles and even excretory functions of sperms can all be treated today.</p>

                <h4 class="text-muted">Male Genital Infographics</h4>
                <figure><img data-src="<?= cdn('assets/template/frontend/images/'); ?>male-genital-surgery-info.png" class="img-fluid lazy" alt="Male Genital Surgery Clinic in Delhi, India"/></figure>
            </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetic-surgery/">Cosmetic Surgery</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia/">Gynecomastia Surgery</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Surgery</a></li>
                        <li><a href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>liposuction/">Liposuction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-augmentation/">Breast Augmentation Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-reduction/">Breast Reduction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynecology</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12 ">
                <div class="section-title">
                    <h2 class="black-text text-center">Types of Male Genital</h2>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Penis Enlargement</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Penis Enlargement</h3>
                    <p>Girth can be increased with fillen or but injections.Lengthening can be done by the release of ligaments. supporting the penis or expiring the root of the penis and covering it with a skin graft.
</p>           

                    
                    <p>Also known as penile girth enhancement, there are two separate processes aimed at increasing the size of a penis. While one is meant to enlarge the penis, the other is meant to increase the length. A similar process can also be used to increase the length of a flaccid penis. A skin graft is used to stitch skin as well as tissue of the penis and this allows for the enlargement of the penis.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle ">Circumcision or Repair</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Circumcision or Repair</h3>
                    <p>If the foreskin is too tight or there is just too much of it, men can opt for circumcision. The foreskin is removed and the penis will have a cleaner appeal.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Scrotal Reduction</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Scrotal Reduction</h3>
                    <p>When there is excessive skin between the penis and the scrotum, most men prefer to have it removed. In many cases, the skin is so much in excess that it can interfere with intercourse. Scrotal reduction will remove this excessive skin and allow for not only better aesthetics, but also improved and safer intercourse.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Testicular implants</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Testicular implants</h3>
                    <p>There are several cases, when men have been born without testicles or the sperm generation is not up to the mark. For such people, the medically most suitable option happens to be implants. The surgery is reasonably simple and can be performed on anyone who has crossed puberty.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Triple augmentation</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Triple augmentation</h3>
                    <p>When lengthening, broadening and glanular enhancement are all done together, the procedure is known as a triple augmentation. Combining all three procedures in one go would offer a more unified look to the penis and be easier on the pocket too.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Penile Prosthesis</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Penile Prosthesisn</h3>
                    <p>A penile prosthesis is another treatment option for men with erectile dysfunction. These devices are either malleable (bendable) or inflatable.</p>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center ">
                    <h2 class=" black-text">Before, during & after the Male Genital Surgery</h2>
                    
       
                                           
                </div>
            </div>
        </div>
        <div class="row text-left ptb-1">
            <div class="col-md-6 ">
                <div class="col-sm-12 ">
              
                    <div class="col-lg-12 ">
                        <h3>Before the Surgery</h3>
                        <p>In the very first consultation session, the doctor will talk to you about why you want the surgery and what your goals are. You will also have to undergo an extensive examination, which will allow your doctor to gauge the exact condition, as well as the precise method of treatment. The surgeon will also require your medical history, because he or she will have to make sure that you are an ideal enough candidate for the surgery. If you are on any medications, you will have to specify the same, because some of them might either have to be stopped for a while or replaced, for the surgery to be a success.</p>
                    </div>
                    <div class="col-lg-12 pt-1 ">
                        <h3>After the Surgery</h3>
                        <p>With almost all the male genital surgeries, you will be able to return home the same day; however, you might not be able to drive yourself, which is why you will be asked to make the arrangements, in advance. There will be a little swelling, bruising, redness and itching, which will last for a few days. You might be prescribed topical creams or medications, along with medicines for pain management.
                            <br /><br />While you might not need complete bed rest, you will be asked to refrain from any strenuous physical activities. You should be able to shower within 2 days of surgery, but exercising and have sexual intercourse will have to be deferred for a minimum of two weeks. However, if you have a sedentary job, you should be able to return to it in about ten days’ time.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="col-sm-12 ">
                    <h3>During the Surgery</h3>
                    <p>While the exact procedure for each male genital surgery is different, the underlying factor is pretty much the same. The idea is to improve the penile function, the structure of the scrotum or testicles. All procedures will be conducted under the influence of anaesthesia, sedation or a combination of both. Normally, most of these procedures are done on an outpatient basis, which means that you will not be required to spend the night at the hospital.
                        <br /><br />If you are going for a penile girth enhancement, the most common approach would be the skin graft. A piece of skin will be stitched along with the fascial tissue of the penis and the size can be increased, as per the requirement of the patient. The doctor will however, inform the person about realistic sizes. If you are undergoing a scrotal reduction, the webbing will have to be carefully trimmed away and then the remaining skin will be pulled up and stitched together with dissolvable sutures. For testicular implants, the patient will have to have reached a certain stage of maturity, so that the ideal sized implants can be chosen. An incision will be made just at the top part of the scrotum and the implant will be inserted.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Will There Be A Lot Of Pain?</a> </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">It is a surgery, which means that there will be a pain; however, you will be given either a local anaesthetic or will be put under sedation, which means that you will not experience any pain, during the surgery. There might be some pain in the days that follow, but your doctor will give you medication to control the same.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Will A Penile Enlargement Improve My Sexual Performance?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">While the size and thickness of your penis can be increased, there is no real guarantee that your performance will improve. However, partners of most people who have undergone such procedures have mentioned sexual intercourse being more pleasurable.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Are Such Procedures Permanent?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">The surgeries are designed to provide long term results, however, with age and genetics, the effects of the surgery might start wearing off.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">How Expensive Will It Be?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">Since most of these are elective or cosmetic surgeries, they are not exactly light on the pocket. The exact cost of each surgery will be dependent on the actual surgery and how extensive it is and the experience of the surgeon.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefive">Will These Surgeries Get Covered By My Insurance?</a> </div>
                        <div id="collapsefive" class="collapse " data-parent="#accordion">
                            <div class="card-body">As most of the male genital surgeries are of an elective or cosmetic nature, most insurance companies will not pay for them. However, if you are going for a testicular implant, because you were born without them or lost them in a trauma, there is a chance that you might be able to get some insurance cover.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefivde">When Will I Be Able To Return To My Normal Routine?</a> </div>
                        <div id="collapsefivde" class="collapse " data-parent="#accordion">
                            <div class="card-body">Ideally, you should be able to return to a normal life within a few days, but vigorous activities, sports and sexual intercourse might have to wait for a minimum of two weeks.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>Possible Risks</h2>
                </div>
                <div class="row">
                    <div class="col-md-12"><br /> <p class="text-center">Ideally, when male genital has been conducted by an experienced surgeon, there are little to no chances of things going wrong. However, there are some complications that can be associated with this procedure, such as:</p><br />
                        <p class="text-center">It is important to remember that the penis is a very delicate organ and corrective or reconstructive surgeries could lead to loss of sensation. As a matter of fact, this is one of the most common problems with circumcision. The glan, which is on the tip of the penis, starts to lose sensation after the procedure, and since this is not a reversible procedure, the damage done will be permanent.<br /><br />
                            There can also be complications with the anaesthesia, especially if it has not been administered by an expert. There can be infections and allergic reactions, particularly to the medical material being used, such as sutures, bandages and topical medications. There could be incessant bleeding, if an important nerve gets cut during surgery or there could be an embolus, which is a blood clot.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetic_surgery_blogs'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>