<?php $this->load->view('front/partials/cosmetic_surgery_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> 
                        <a itemprop="item" href="<?= base_url(); ?>cosmetic-surgery/"> <span itemprop="name">Cosmetic Surgery</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>breast-augmentation/"> <span itemprop="name">Breast Augmentation</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-9">
                <div class="section-title-1 text-left">
                    <h1 class="black-text">Breast Enlargement</h1>
                </div>
                <p>In simple terms, breast enhancement or breast enlargement is a process by which the size of the breasts can be increased. the technique of Breast Augmentation surgery can be used to restore the volume of the breasts, post-delivery, weight loss or due to age and also improve the balance of your physical structure. The process can also be done for women, who have injured their breasts due to an accident or have had to remove them due to cancer.</p>
                <figure class="ptb-1"><img data-src="<?= cdn('assets/template/frontend/images/'); ?>breast-augmentation-infographics.png" class="img-fluid lazy" alt="Best breast enlargement surgery clinic in delhi, India"/></figure> </div>
            <div class="col-md-3">
                <div class="services-list">
                    <ul class="list mt-0">
                        <li class=""><a href="<?= base_url(); ?>cosmetic-surgery/">Cosmetic Surgery</a></li>
                        <li><a href="<?= base_url(); ?>rhinoplasty/">Rhinoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>blepharoplasty/">Blepharoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>gynecomastia/">Gynecomastia Surgery</a></li>
                        <li><a href="<?= base_url(); ?>vitiligo-treatment/">Vitiligo Surgery</a></li>
                        <li><a href="<?= base_url(); ?>abdominoplasty/">Abdominoplasty Surgery</a></li>
                        <li><a href="<?= base_url(); ?>liposuction/">Liposuction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>breast-reduction/">Breast Reduction Surgery</a></li>
                        <li><a href="<?= base_url(); ?>cosmetic-gynaecology/">Cosmetic Gynecology</a></li>
                        <li><a href="<?= base_url(); ?>male-genital-surgery/">Male Genital Surgery</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title text-center">
                    <h2 class="black-text">Types of Breast Augmentation</h2>
                    <p>The manner in which the breast augmentation or breast enhancement is done is pretty much the same, but there are a range of breast implants that can be used and some of these include:</p>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Silicone implants</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Silicone implants</h3>
                    <p>Filled with silicone gel, these are the most popular implants, mainly because they feel a lot like the original breast tissues. There is minimal chance that the implant breaks or leaks.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle ">Saline implants</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Saline implants</h3>
                    <p>As the name suggests, saline implants are filled with salt water, which has been sterilised.The benefit of saline implants is that it offers uniformity in terms of shape and firmness. </p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Smooth breast implants</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Smooth breast implants</h3>
                    <strong>Primary</strong>
                    <p>These implants provide the softest feeling, and these can actually move inside the breast implant pocket, which is why it looks more natural</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Round Breast Implants</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Round Breast Implants:</h3>
                    <p>The round breast implants tend to make the breasts appear rounder and more voluptuous. Given their shape, there is minimal chance of the implants rotating and moving out of place.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Gummy bear breast implants</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Gummy bear breast implants</h3>
                    <p>The form stable implants can maintain their shape irrespective of what happens to the implant shell. Even though the filling in these implants is also silicone gel, it is actually thicker than the traditional silicone gel filling, making them less prone to breakage.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Fat Transfer</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Fat Transfer</h3>
                    <p>Through liposuction, excess fat is removed from the body, and this fat is injected into the breasts. This is a great option for women who are not too keen on having a foreign body inside their body or need only a marginal increase in their breast size.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="service-area ptb-3">
 <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title text-center">
                    <h2 class="black-text">Breast Reduction Advantages</h2>
                    <p>The manner in which the breast augmentation or breast enhancement is done is pretty much the same, but there are a range of breast implants that can be used and some of these include:</p>
                </div>
            </div>
        </div>

<div class="row text-center  ">
<div class="col-md-12">
<figure><img class="img-fluid" src="https://akclinics.org/wp-content/themes/AKclinics/images/breast-augmentation-advantages.png" alt="breast augmentation advantages"></figure>
</div>
</div>



</div></div>








<div class="service-area ptb-3 bg-light-gray ">
    <div class="container">
        <div class="col-md-12 ">
            <div class="row text-center">
                <div class="section-title text-center">
                    <h2 class="black-text">Types of Breast Augmentation</h2>
                    <p>The manner in which the breast augmentation or breast enhancement is done is pretty much the same, but there are a range of breast implants that can be used and some of these include:</p>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="tabs">
                <div class="tab">
                    <button class="tab-toggle">Silicone implants</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Silicone implants</h3>
                    <p>Filled with silicone gel, these are the most popular implants, mainly because they feel a lot like the original breast tissues. There is minimal chance that the implant breaks or leaks.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle ">Saline implants</button>
                </div>
                <div class="content ">
                    <h3 class="m-3">Saline implants</h3>
                    <p>As the name suggests, saline implants are filled with salt water, which has been sterilised.The benefit of saline implants is that it offers uniformity in terms of shape and firmness. </p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Smooth breast implants</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Smooth breast implants</h3>
                    <strong>Primary</strong>
                    <p>These implants provide the softest feeling, and these can actually move inside the breast implant pocket, which is why it looks more natural</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Round Breast Implants</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Round Breast Implants:</h3>
                    <p>The round breast implants tend to make the breasts appear rounder and more voluptuous. Given their shape, there is minimal chance of the implants rotating and moving out of place.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Gummy bear breast implants</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Gummy bear breast implants</h3>
                    <p>The form stable implants can maintain their shape irrespective of what happens to the implant shell. Even though the filling in these implants is also silicone gel, it is actually thicker than the traditional silicone gel filling, making them less prone to breakage.</p>
                </div>
                <div class="tab">
                    <button class="tab-toggle">Fat Transfer</button>
                </div>
                <div class="content">
                    <h3 class="m-3">Fat Transfer</h3>
                    <p>Through liposuction, excess fat is removed from the body, and this fat is injected into the breasts. This is a great option for women who are not too keen on having a foreign body inside their body or need only a marginal increase in their breast size.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="row">
            <div class="section-title text-center ">
                <h2 class=" black-text">Breast Augmentation</h2>
            </div>
        </div>
        <div class="row text-left">
            <div class="col-md-6 ">
                <div class="col-sm-12 ">
                    <div class="col-lg-12 ">
                        <h3>Before the Surgery</h3>
                        <p>You will have to undergo a thorough medical examination to ensure that you are physically fit for the procedure. Your blood will be checked and your medications will also have to be monitored. If you are on blood thinners, you will be asked to avoid the same for a while. This will also be the time, when the size of the implant will be finalized on. Implant size can be mutually decided, by the doctor and patient</p>
                    </div>
                    <div class="col-lg-12 pt-1 ">
                        <h3>After the Surgery</h3>
                        <p>Immediately after the surgery, you will be kept under observation, to ensure that there is no excessive bleeding or clotting. Breasts are generally wrapped in gauze and you will be asked to wear a support bra or elastic bandage to support your breasts. While you will be allowed to return home soon, you will have to take care of the breasts, to avoid infections. The surgeon will suggest the constant wearing of support bras and returning to a normal life of exercise and activity will take a few weeks.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="col-sm-12 ">
                    <h3>During the Surgery</h3>
                    <p>In simple terms, the breast is cut open, excess tissue is removed, the skin is pulled and stitched back together. In many cases, cutting the breast is not required, because if all the excess tissue is only fat, it can be removed in a liposuction like procedure.
                        </p>
                    <ul class="text-left pt-1"><strong class="">There are basic four steps in a breast reduction procedure:</strong>
                        <li>Anaesthesia is administered either via intravenous method or in the form of general anaesthesia.</li>
                        <li>There are mainly three types of incisions that can be made – periareolar incision, inframammary incision and transaxillary incision. The first type of incision is made around the areolas, the second one underneath the breast and the final one is made in the armpits.</li>
                        <li>The implants are then placed carefully inside the breast pocket. The implant can be placed either under the pectoral muscle or behind the breast tissue.</li>
                        <li>The incision that had been made is now closed up, and the entire area will be cleaned and bandaged.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>FAQs</h2>
                </div>
                <div id="accordion" class="mt-4">
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">Will It Be All Over In One Surgery?</a> </div>
                        <div id="collapseOne" class="collapse show" data-parent="#accordion">
                            <div class="card-body">Most of the times, the implants will not last forever, because there could be leakage, breakage, change in shape or weight loss or weight gain. Factors such as pregnancy can also lead to the need of another surgery.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Can I Go To A Really Large Size In Just One Surgery?</a> </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">This is not suggested, because going up one or two sizes is ok in a single surgery, larger sizes will need a number of sessions. Each implant takes time to settle in and your body needs time to adapt to the medical device.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">Is It Possible To Try Different Sizes, Before Deciding On One?</a> </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">By using something called sizers you can get a fair idea of how you will look in the future, post-surgery. The sizers are neoprene sacks, which are filled with beads and by putting these inside your bra, you will be able to decide the correct size.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Will I Be Able To Breastfeed Later On?</a> </div>
                        <div id="collapsefour" class="collapse " data-parent="#accordion">
                            <div class="card-body">When you undergo a breast enhancement surgery, there is a chance that the minor ducts could be damaged. There is also the possibility that the areola complex gets completely disconnected from the particular portion of the gland that enables you to breastfeed.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 text-center">
                    <h2>Possible Risks</h2>
                    <p class="text-left">Ideally, when breast augmentation has been conducted by an experienced surgeon, there are little to no chances of things going wrong. However, there are some complications that can be associated with this procedure, such as:</p>
                </div>
                <div class="row pt-1">
                    <ul>
                        <li>Bleeding, infections, pain and scars</li>
                        <li>Loss of sensation in the nipples or entire breast</li>
                        <li>Complications with anaesthesia</li>

                        <li>Accumulation of fluid, Rupturing or leakage of the implant</li>
                        <li>Wrinkled breasts or incorrect placement</li>
                        <li>Formation of scar tissue around the implant</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/cosmetic_surgery_blogs'); ?>
<div class="bg-light-gray">
    <?php $this->load->view('front/partials/location_block'); ?>
</div>
<?php $this->load->view('front/partials/doctor_contacts'); ?>