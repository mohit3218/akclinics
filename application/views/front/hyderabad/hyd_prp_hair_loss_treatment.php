<?php $this->load->view('front/partials/hair_loss_restoration_banner'); ?>
<div class="header-banner-content-area light-orange">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-left m-2 text-dark" >
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>"> <span itemprop="name">Home</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li class="disabled" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>bangalore/"> <span itemprop="name">Best Skin Care Clinic in Bangalore</span></a><i class="fa fa-angle-right fa-1x text-white p-1"></i>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li class="disabled anchor-dis-li" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"> <a itemprop="item" href="<?= base_url(); ?>bangalore/prp-hair-loss-treatment/"> <span itemprop="name">PRP Hair Loss Treatment</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container ">
        <div class="row"> 
            <div class="col-md-12">
                <div class="section-title">
                    <h1 class="black-text">PRP Therapy for Hair Loss Treatment</h1>
                </div>
                <p>If your hair loss is bothering you, there are a few things that you can do – you can try all types of home remedies to boost your hair growth, you could try medications that your general physician suggests or you could do the most sensible thing – you could consider PRP treatment for hair loss in Bangalore. At AK Clinics, we offer the most ground breaking method of controlling hair loss and ensuring that you are able to enjoy a full head of hair, for much longer! This therapy is completely natural & free of any chemicals<br />
                    <br />
                <h4>What is PRP?</h4>
                When hair loss has just started, there are so many things that you can do to ensure that the rest 
                of your hair do not fall out. However, there are a rare few procedures that are as effective as PRP 
                or Platelet Rich Plasma therapy. This procedure is a great choice for people who have started losing
                hair, but want to stay away from the possibility of gaining bald spots soon. This is also the 
                procedure of choice for people who want to try out something minimally invasive and don’t want 
                to swallow many pills.
                </p>
                <br />        
                <p>Human blood is an extremely complex formulation, but it is the platelets that assist with the 
                    clotting, when there is an injury. Platelets also have growth factors, which not only ensure 
                    growth, but also assist with the healing process. So, imagine a procedure in which, these 
                    platelets, made all the more potent, were injected back into your body? That is the basic tenet 
                    of PRP!</p>
                <br />
                <p >PRP is a non-surgical, minimally invasive procedure, which is a great option for people who want 
                    to try something before a transplant. PRP treatment for hair loss works great for men as well as 
                    women and is known to show some wonderful results.
                </p>
            </div>

        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12 text-center">
            <div class="row">
                <div class=" section-title ">
                    <h2 class=" black-text">How does PRP works?</h2>
                    <p>The human blood has something called mesenchymal stem cells, which essentially contain growth 
                        factors and assist with the development of various parts of the body. For the PRP therapy to 
                        work, blood is drawn from the body and the platelets, which contain the mesenchymal stem 
                        cells, are harvested. The platelets are then treated with some other growth factors, turning 
                        the blood taken from your own body, into something of a super serum, and then the same is 
                        injected back into your body. For hair loss, this serum will be injected into your scalp, 
                        and this will help the follicles to grow better and stronger. When you come to AK Clinics 
                        for PRP treatment for hair in Bangalore, this is what the process will be like: 
                    </p>
                </div>

            </div>
        </div>

        <div class="col-md-12 text-center ptb-1 ">
            <P>When you come to AK Clinics for PRP treatment for hair in Bangalore, this is what the process will be like: </P>
            <div class="row ptb-1">
                <div class="col-md-3">
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/prp-11.png" alt="Blood Sampling-prp hair treatment in Bangalore" title="Blood Sampling-prp hair treatment in Bangalore" class="img-responsive lazy"><br>
                    <h4>Step 1</h4>
                    <p>Using a regular syringe, blood will be collected from your body, most probably about 30 to 45 minutes before the procedure. Anywhere between 40 to 60cc of blood will be collected and the same will be stored in vials, till the next step. </p>
                </div>
                <div class="col-md-3">
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/prp-12.png" alt="Separating platelets - prp hair treatment in Bangalore" title="Separating platelets - prp hair treatment in Bangalore" class="img-responsive lazy"><br>
                    <h4>Step 2 </h4>
                    <p> The vials of blood are put inside a centrifuge for a few moments and this allows for the separation of the components that make up blood. Once the centrifugation process is complete, the samples will be stored in a sterile refrigerator, to ensure that there is no contamination.</p>
                </div>
                <div class="col-md-3">
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/prp-13.png" alt="Plasma Concentration &amp; Growth Factors- PRP Therapy Bangalore" title="Plasma Concentration &amp; Growth Factors- PRP Therapy Bangalore" class="img-responsive lazy"><br>
                    <h4>Step 3 </h4>
                    <p>The plasma is divided into three – the platelet poor plasma, the platelet rich plasma and the red blood cells. The growth factors are then separate and diluted using the plasma, making the serum ready for injection.</p>
                </div>
                <div class="col-md-3">
                    <img data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/prp-14.png" alt="PRP Injection - Prp Hair Treatment Cost in Bangalore" title="PRP Injection - Prp Hair Treatment Cost in Bangalore" class="img-responsive lazy"><br>
                    <h4>Step 4</h4>
                    <p>The potent plasma serum is then carefully injected into the scalp. Once the growth factors enter the blood stream, they stimulate the stem cells in the scalp, enabling new growth and a better life for the existing hair.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs" >
                        <div class="tab">
                            <button class="tab-toggle ">How much will PRP therapy cost?</button>
                        </div>
                        <div class="content ">
                            <h3 class="m-3">How much will PRP therapy cost?</h3>
                            <p>The cost of PRP therapy depends on a range of factors, starting from the actual clinic itself, the geographical location, the equipment that is being used, the experience and expertise of the doctor performing the procedure and also the number of sessions and injections you require.
                                In case you are wondering what the PRP hair loss treatment cost in Bangalore’s AK Clinics will cost you, just get in touch with us!</p>
                        </div>
                        <div class="tab">
                            <button class="tab-toggle">Why PRP Therapy?</button>
                        </div>
                        <div class="content">
                            <h3 class="m-3">Why PRP Therapy?</h3>
                            <p>Perhaps the biggest reason why you should consider PRP is because it accentuates the natural healing process of the body. Because it is blood from your own body, the chances of infections are minimal and also the chances of rejection from the body are also incredibly low. In addition, this is a procedure that will not require a lot of time from you, because a single session will last only about one to two hours. You will be able to see hair growth from the second or third session and thereon, you will notice that your hair is becoming thicker and healthier. The hair that will grow in will be natural and real, which means that you can style them the way you want.
                                This is the procedure for people who are worried about invasive procedures, because there are no cuts, no scars and no long recovery period, because this is not a surgery!</p>
                        </div>
                        <div class="tab">
                            <button class="tab-toggle">Things to remember before a PRP therapye</button>
                        </div>
                        <div class="content">
                            <h3 class="m-3">Things to remember before a PRP therapy</h3>
                            <p>Even though there is not much precaution that you need to take before a PRP therapy session, there are a few things that you need to keep in mind:</p>
                            <ul>
                                <li>You will have to stop smoking and consuming alcohol for couple of days.</li>
                                <li>If you are on any blood thinners, you will be asked to stop the same a week beforehand</li>
                                <li>Make sure you wash your head well with an antibacterial shampoo, before the treatment</li>
                                <li>Ensure that you have a proper breakfast before arriving at AK Clinics for your PRP hair treatment in Bangalore</li>
                            </ul>
                        </div>
                        <div class="tab">
                            <button class="tab-toggle">PRP specific cells that cause hair growth include</button>
                        </div>
                        <div class="content">
                            <h3 class="m-3">PRP specific cells that cause hair growth include</h3>
                            <figure><img class="img-fluid lazy" data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/platelts.png" alt="PRP cells that cause hair growth include-prp treatment Bangalore" title="PRP cells that cause hair growth include-prp treatment Bangalore"></figure>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3  bg-light-gray">
    <div class="container">
        <div class="col-md-12 text-center">
            <div class="row">
                <div class=" section-title ">
                    <h2 class="black-text">What are the benefits of PRP?</h2>
                    <p>There is a reason why PRP is becoming so popular these days – the benefits of the procedure are too many:</p> </div>

            </div>
        </div>
        <div class="col-md-12 text-center ptb-1">

            <div class="row">
                <div class="col-md-3">
                    <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/prp-benifit-1.jpg" alt="Texture - PRP cost in bangalore" title="Texture - PRP cost in bangalore" class="rounded border  mb-2 lazy"> </figure>
                    <p>This is a minimally invasive procedure, which means that there is next to no downtime and there is a really short recovery period. In most cases, people are able to return to most of their regular activities on the very next day. </p>
                </div>
                <div class="col-md-3">
                    <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/prp-benifit-12.jpg" alt="Hair Growth - Best PRP Hair Treatment in Bangalore" title="Hair Growth - Best PRP Hair Treatment in Bangalore" class="rounded border  mb-2 lazy"> </figure>
                    <p>Because it is your own blood that is being used, the chances of infection, allergies or your body rejecting the same are almost next to none. </p>
                </div>
                <div class="col-md-3">
                    <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/prp-benifit-3.jpg" alt="Surgical Boost - PRP Therapy in Bangalore" title="Surgical Boost - PRP Therapy in Bangalore" class="rounded border  mb-2 lazy"> </figure>
                    <p>Not only will hair fall reduce drastically post procedure, you will also notice that you are now enjoying a better rate of hair growth along with better hair texture in terms of smoothness and softness. </p>
                </div>
                <div class="col-md-3">
                    <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/prp-benifit-4.jpg" alt="No Infections - PRP in Bangalore" title="No Infections - PRP in Bangalore" class="rounded border  mb-2 lazy"> </figure>
                    <p>If the procedure is being done in tandem with a hair transplant, you can be sure of a better growth and the recovery period will also reduce, as the PRP helps in faster healing. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 ">
    <div class="container">
        <div class="col-md-12 text-center">
            <div class="row">
                <div class="section-title">
                    <h2 class=" black-text">What are the side effect of PRP?</h2>
                    <p>One of the reasons why PRP treatment in Bangalore has become so popular is that there are no known side effects, as the procedure uses blood from your own body. Because there are no cuts and incisions, there are minimal chances of infections or rejection. However, as it is a medical procedure, there could be some side effects, such as:</p>   </div>
            </div>
        </div>
        <div class="col-md-12 text-center ptb-1">
            <p>There is a reason why PRP is becoming so popular these days – the benefits of the procedure are too many:</p>
            <div class="row  ptb-1">
                <div class="col-md-3">
                    <figure><img alt="Best PRP Treatment in Bangalore" title="PRP Hair Treatment in Bangalore" data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/side-effects-11.jpg" class="rounded border mb-2 lazy"> </figure>
                    <p>There could be a little pain or irritation, post procedure, but this will subside in a matter of hours. </p>
                </div>
                <div class="col-md-3">
                    <figure><img alt="Best prp hair treatment in bangalore" title="Best prp hair treatment in bangalore" data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/side-effects-21.jpg" class="rounded border mb-2 lazy"> </figure>
                    <p>For some people, there could be a little bleeding where the injections are being administered, but this too is not a thing to worry about. The bleeding will not be excessive and will subside in some time. </p>
                </div>
                <div class="col-md-3">
                    <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/prp-benifit-3.jpg" class="rounded border mb-2 lazy" alt="PRP Hair Loss Treatment Cost in Bangalore" title="PRP Hair Loss Treatment Cost in Bangalore"> </figure>
                    <p>Not only will hair fall reduce drastically post procedure, you will also notice that you are now enjoying a better rate of hair growth along with better hair texture in terms of smoothness and softness.</p>
                </div>
                <div class="col-md-3">
                    <figure><img data-src="<?= cdn('assets/template/uploads/'); ?>2017/09/prp-benifit-4.jpg" class="rounded border mb-2 lazy" alt="PRP Hair Loss Treatment Cost in Bangalore" title="PRP Hair Loss Treatment Cost in Bangalore"> </figure>
                    <p>If the procedure is being done in tandem with a hair transplant, you can be sure of a better growth and the recovery period will also reduce, as the PRP helps in faster healing.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service-area ptb-3 bg-light-gray">
    <div class="container">
        <div class="col-md-12"> 
            <div class="row">
                <div class="col-md-6">    
                    <h2 class="section black-text">PRP FAQ</h2>
                    <div id="accordion" class="mt-4">
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne">What Is The Basic Concept Behind PRP?</a> </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                <div class="card-body">A small amount of blood is taken from the body and the extremely potent platelet rich plasma is extracted from the same. The plasma is converted into a powerful serum by means of addition of growth factors. This fortified serum is then injected back into the body, promoting better hair growth, improved texture of hair and improved healing of the scalp and hair follicles.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">How Many Sessions Will Be Needed?</a> </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">The frequency will change from person to person, but in most cases, 3 to 4 sessions are more than enough and each session is spaced about a month or so apart. While, for some people, maintenance sessions might be required once or twice a year, in the subsequent years. With PRP hair treatment in Bangalore from AK Clinics, you should be able to see new growth within 4 months or so.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">How Long Does Each Session Take?</a> </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body">PRP is often called a lunch time procedure, because it takes only about an hour or so, and because there is no downtime, you can actually return to work, as soon as the procedure is over.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefour">Is The Procedure Painful?</a> </div>
                            <div id="collapsefour" class="collapse " data-parent="#accordion">
                                <div class="card-body">There is no pain or discomfort associated with PRP – because it is only a series of injections, there are a lot of people who forgo any sedation or anaesthetic. However, when you are getting your PRP treatment in Bangalore from AK Clinics, we will ensure that a topical anaesthetic gel is applied to the scalp, ensuring absolutely no pain or discomfort. As soon as the anaesthesia wears off, you are free to leave.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsefive">What Can I Expect From This Treatment?</a> </div>
                            <div id="collapsefive" class="collapse " data-parent="#accordion">
                                <div class="card-body">Not only will you notice a visible reduction in your hair fall, you will also see the growth of hair caliber and the density will also visibly improve. In addition, the texture and general health of your hair will also improve.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapsesix">Is PRP Meant For Women Too?</a> </div>
                            <div id="collapsesix" class="collapse " data-parent="#accordion">
                                <div class="card-body">Absolutely! This is not a gender specific therapy and can be done on both men and women. As a matter of fact, if you have been noticing your hair thinning or are bothered by your constant hair loss, then this is the procedure for you.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseseven">Will I Have To Stop My Regular Medications?</a> </div>
                            <div id="collapseseven" class="collapse " data-parent="#accordion">
                                <div class="card-body">While there is no need to stop regular medication, if you are on blood thinners, you might be asked to stop for a few days. We have a team of trained medical practitioners and skilled doctors, who will be able to assist you with such questions.</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseeight">Can PRP Be Combined With Hair Transplants?</a> </div>
                            <div id="collapseeight" class="collapse " data-parent="#accordion">
                                <div class="card-body">Absolutely! PRP can be done before a transplant, after one or even during the transplant procedure. Not only will it plasma serum ensure faster healing, it will also ensure that you have faster and better hair growth, along with stimulating the hair that is already present on your scalp.</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 review">
                    <h2 >PRP Results</h2>
                    <div id="carouselExampleIndicators" class="carousel slide text-white ptb-3" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="4" class=""></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item">
                                <figure class=""><img class="lazy" data-src="<?= cdn('assets/template/') ?>themes/AKclinics/images/prp/prp-result-1.png" alt="PRP hair loss treatment in bangalore" width="550" height="360" style="max-width:100%;" title="PRP hair loss treatment in bangalore"> </figure>
                            </div>
                            <div class="carousel-item">
                                <figure class=""><img class="lazy" title="PRP hair treatment cost in bangalore" src="<?= cdn('assets/template/') ?>themes/AKclinics/images/prp/prp-result-2.png" width="550" height="360" style="max-width:100%;" alt="PRP hair treatment cost in bangalore"> </figure>
                            </div>
                            <div class="carousel-item active">
                                <figure class=""><img class="lazy" src="<?= cdn('assets/template/') ?>themes/AKclinics/images/prp/prp-result-3.png" width="550" height="360" style="max-width:100%;" alt="PRP hair loss treatment cost in bangalore" title="PRP hair loss treatment cost in bangalore"></figure>
                            </div>
                            <div class="carousel-item">
                                <figure class=""><img class="lazy" src="<?= cdn('assets/template/') ?>themes/AKclinics/images/prp/prp-result-4.png" width="550" height="360" style="max-width:100%;" alt="PRP treatment in bangalore" title="PRP treatment in bangalore"> </figure>
                            </div>
                            <div class="carousel-item">
                                <figure class=""><img class="lazy" src="<?= cdn('assets/template/') ?>themes/AKclinics/images/prp/prp-result-5.png" width="550" height="360" style="max-width:100%;" alt="Cost of prp hair treatment in bangalore" title="Cost of prp hair treatment in bangalore"> </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/partials/services_bangalore'); ?>
<?php $this->load->view('front/partials/doctor_contacts'); ?>
