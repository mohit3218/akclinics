<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Manager
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="<?= base_url(); ?>admin/ratings" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="<?= base_url(); ?>admin/ratings" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Google Ratings Manager
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="<?= base_url(); ?>admin/ratings/create" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Edit Google Review Ratings
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div>
            <div class="m-portlet__head-tools">
                <a href="<?= base_url(); ?>admin/ratings" class="btn btn-focus m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                    <span>
                        <i class="flaticon-layers"></i>
                        <span>
                            Back
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('back/partials/message'); ?>

<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Googlr Review Edit Form
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <!--<form class="m-form">-->
                    <?php echo form_open("admin/ratings/edit/".$data['id'], 'class="m-form"'); ?>
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Location Name:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="name" placeholder="Enter location name"  id="name" class="form-control m-input" required="required" value="<?= $data['name']; ?>">
                                    <span class="m-form__help">
                                        Please enter location name
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Location Rating:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="rating" placeholder="Enter location rating"  id="rating" class="form-control m-input" required="required" value="<?= $data['rating']; ?>">
                                    <span class="m-form__help">
                                        Please enter location rating
                                    </span>
                                </div>
                            </div>
                        </div>
                
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">
                                Location Review:
                            </label>
                            <div class="col-lg-6">
                                <input type="text" name="review" class="form-control m-input" placeholder="Enter location review" required="required" value="<?= $data['review']; ?>" />
                                <span class="m-form__help">
                                    Please enter location review
                                </span>
                            </div>
                        </div>
                        
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <a class="btn btn-secondary m-btn m-btn--icon" id="m_reset" type="reset" href="<?= base_url('admin/ratings'); ?>">
                                            <span>
                                                <span>
                                                    Cancel
                                                </span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
               <?php echo form_close(); ?>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>