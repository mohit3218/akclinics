<?php
/**
 * Category Index View - Shows Category summary for Admin users
 * 
 * @created    22/10/2018
 * @package    AK Clinics
 * @copyright  Copyright (C) 2018
 * @author     Mohit Thakur
 */
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($search))
{
    if ($search == TRUE)
    {
        $is_active = ($search_data['is_active'] != '') ? $search_data['is_active'] : -1;
        $name = $search_data['name'];
    }
}
else
{
    $is_active = -1;
}
?>
<style>
    .m-portlet .m-portlet__body {padding: 2.2rem 1rem !important;}
    .hide,label.m-checkbox.m-checkbox--single.m-checkbox--solid.m-checkbox--brand {display: none !important;}
</style>
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Manager
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="<?= base_url(); ?>admin/auth" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="<?= base_url(); ?>admin/ratings" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Setting Manager
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="<?= base_url(); ?>admin/ratings" class="m-nav__link">
                        <span class="m-nav__link-text">
                            All Google Review ratings
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div>
            <div class="m-portlet__head-tools">
                <a href="<?= base_url(); ?>admin/ratings/create" class="btn btn-focus m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                    <span>
                        <i class="la la-user"></i>
                        <span>
                            Add New Google Review
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('back/partials/message'); ?>

<!-- END: Subheader -->
<!--<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Search Form
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <form class="m-form m-form--fit m--margin-bottom-20">
                <div class="row m--margin-bottom-20">
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                        <label>
                            Category Name:
                        </label>
                        <input type="text" class="form-control m-input" placeholder="Category name" data-col-index="0" name="name" value='<?= (isset($name)) ? $name : ''; ?>'>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                        <label>
                            Status:
                        </label>
                        <select class="form-control m-input" id="exampleSelect1" name="is_active">
                            <option value="">Please Select</option>
                            <?php 
                                foreach (StaticArray::$status_arr as $key => $value) : 
                                    if ($is_active == $key) { $sele = 'selected'; } 
                                    else { $sele = ''; }
                                    echo '<option value="' . $key . '" ' . $sele . '>' . $value . '</option>';                                                        
                                endforeach; 
                            ?>
                        </select>
                    </div>
                </div>
                <div class="m-separator m-separator--md m-separator--dashed"></div>
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-brand m-btn m-btn--icon" id="m_search" type="submit" name="search" value="search">
                            <span>
                                <i class="la la-search"></i>
                                <span>
                                    Search
                                </span>
                            </span>
                        </button>
                        &nbsp;&nbsp;
                        <a class="btn btn-secondary m-btn m-btn--icon" id="m_reset" type="reset" href="<?= base_url('admin/categories'); ?>">
                            <span>
                                <i class="la la-close"></i>
                                <span>
                                    Reset
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>-->
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Google Review Ratings Record
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                <thead>
                    <tr>
                        <th></th>
                        <th>Sr No.</th>
                        <th>Location Name </th>
                        <th>Location Rating </th>
                        <th>Location Review </th>
                        <th>Status </th>
                        <th class="hide"></th>
                        <th>Actions</th>
                        <th class="hide"> </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($review_ratings_record as $review_ratings): ?>
                    <tr class="odd gradeX">
                        <td></td>
                        <td><?php echo $review_ratings['id']; ?></td>
                        <td><?php echo $review_ratings['name']; ?></td>
                        <td><?php echo $review_ratings['rating']; ?></td>
                        <td><?php echo $review_ratings['review']; ?></td>
                        <td>
                            <?php $active_staus = ($review_ratings['is_active'] == 1) ? 'm-badge  m-badge--success m-badge--wide' : 'm-badge  m-badge--danger m-badge--wide'; ?>
                            <?php $staus = ($review_ratings['is_active'] == 1) ? 'Active' : 'In-Active'; ?>
                            <span class="<?= $active_staus; ?>"><?= $staus; ?></span>
                        </td>
                        <td class="hide"></td>
                        <td>
                            <a href="<?= base_url(); ?>admin/ratings/edit/<?= $review_ratings['id']; ?>" title="Edit" class="icon currency-color">
                                <i class=" la la-edit"></i>
                            </a>
                            <a href="<?= base_url(); ?>admin/ratings/delete/<?= $review_ratings['id']; ?>" onclick="return confirm('Do you want delete this record');" title="Delete">
                                <i class=" la la-trash"></i>
                            </a>
                        </td>
                        <td class="hide"></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>