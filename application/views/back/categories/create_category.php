<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Manager
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="<?= base_url(); ?>admin/categories" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="<?= base_url(); ?>admin/categories" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Category Manager
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="<?= base_url(); ?>admin/categories/create" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Add Category
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div>
            <div class="m-portlet__head-tools">
                <a href="<?= base_url(); ?>admin/categories" class="btn btn-focus m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                    <span>
                        <i class="flaticon-layers"></i>
                        <span>
                            Category Summary
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('back/partials/message'); ?>

<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Category Add Form
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <!--<form class="m-form">-->
                    <?php echo form_open("admin/categories/create", 'class="m-form"'); ?>
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Category Name:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="name" placeholder="Enter Category name"  id="name" class="form-control m-input" required="required">
                                    <span class="m-form__help">
                                        Please enter category name
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Slug:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="slug" placeholder="Enter slug name"  id="name" class="form-control m-input" required="required">
                                    <span class="m-form__help">
                                        Please enter slug name
                                    </span>
                                </div>
                            </div>
                        </div>
                
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">
                                Meta Title:
                            </label>
                            <div class="col-lg-6">
                                <input type="text" name="document_title" class="form-control m-input" placeholder="Enter meta title" required="required" />
                                <span class="m-form__help">
                                    Please enter document title
                                </span>
                            </div>
                        </div>


                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">
                                Meta Description:
                            </label>
                            <div class="col-lg-6">
                                <textarea type="text" name="meta_description"  id="meta_description" class="form-control m-input" placeholder="Enter meta description" required="required"></textarea>
                                <span class="m-form__help">
                                    Please enter meta description
                                </span>
                            </div>
                        </div>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">
                                Meta Keywords:
                            </label>
                            <div class="col-lg-6">
                                <input type="text" name="meta_keywords" class="form-control m-input" placeholder="Enter meta keywords"/>
                                <span class="m-form__help">
                                    Please enter meta keywords
                                </span>
                            </div>
                        </div>

                        <div class="m-form__group form-group row">
                            <label class="col-lg-4 col-form-label">
                                Robots Meta Settings:
                            </label>
                            <div class="col-lg-6">
                                <div class="m-checkbox-inline">
                                    <label class="m-checkbox">
                                        <input id="noindex" type="checkbox" name="no_index">
                                        Apply noindex to this archive?
                                        <span></span>
                                    </label>
                                </div>
                                <div class="m-checkbox-inline">
                                    <label class="m-checkbox">
                                        <input id="nofollow" type="checkbox" name="no_follow">
                                        Apply nofollow to this archive?
                                        <span></span>
                                    </label>
                                </div>
                                <div class="m-checkbox-inline">
                                    <label class="m-checkbox">
                                        <input id="noarchive" type="checkbox" name="no_archive" >
                                        Apply noarchive to this archive?
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-success">
                                            Submit
                                        </button>
                                        <a class="btn btn-secondary m-btn m-btn--icon" id="m_reset" type="reset" href="<?= base_url('admin/categories'); ?>">
                                            <span>
                                                <span>
                                                    Cancel
                                                </span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
               <?php echo form_close(); ?>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>