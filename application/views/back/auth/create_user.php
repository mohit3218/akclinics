<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Manager
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="/admin/auth" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Settings
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Add New User
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                User Add Form
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <!--<form class="m-form">-->
                    <?php echo form_open("admin/auth/create_user", 'class="m-form"'); ?>
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    First Name:
                                </label>
                                <div class="col-lg-6">
                                    <?php echo form_input($first_name); ?>
                                    <span class="m-form__help">
                                        Please enter your first name
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Last Name:
                                </label>
                                <div class="col-lg-6">
                                    <?php echo form_input($last_name); ?>
                                    <span class="m-form__help">
                                        Please enter your last name
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Company Name:
                                </label>
                                <div class="col-lg-6">
                                    <?php echo form_input($company); ?>
                                    <span class="m-form__help">
                                        Please enter your company name
                                    </span>
                                </div>
                            </div>
                            <?php if ($identity_column !== 'email') { ?>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-4 col-form-label">
                                        Identity:
                                    </label>
                                    <div class="col-lg-6">
                                        <?php echo form_error('identity'); ?>
                                        <?php echo form_input($identity); ?>
                                        <span class="m-form__help">
                                            We'll never share your email with anyone else
                                        </span>
                                    </div>
                                </div>
                            <?php } ?>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Email address:
                                </label>
                                <div class="col-lg-6">
                                    <?php echo form_input($email); ?>
                                    <span class="m-form__help">
                                        We'll never share your email with anyone else
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Contact
                                </label>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-chain"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control m-input" placeholder="Phone number">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Password:
                                </label>
                                <div class="col-lg-6">
                                   <?php echo form_input($password); ?>
                                    <span class="m-form__help">
                                        Please enter your company name
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Confirm Password:
                                </label>
                                <div class="col-lg-6">
                                   <?php echo form_input($password_confirm); ?>
                                    <span class="m-form__help">
                                        Please enter your company name
                                    </span>
                                </div>
                            </div>
<!--                            <div class="m-form__group form-group row">
                                <label class="col-lg-4 col-form-label">
                                    Communication:
                                </label>
                                <div class="col-lg-6">
                                    <div class="m-checkbox-inline">
                                        <label class="m-checkbox">
                                            <input type="checkbox">
                                            Email
                                            <span></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox">
                                            SMS
                                            <span></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox">
                                            Phone
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-success">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn btn-secondary">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
               <?php echo form_close(); ?>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>