<?php
/**
 * User Edit Form 
 * 
 * @created    06/10/2018
 * @package    AK Clinics
 * @copyright  Copyright (C) 2018
 * @author     Mohit Thakur
 */
?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="">Home</a>
        </li>
        <li>
            <span>User Manager</span>
        </li>
        <li>
            <span>Edit</span>
        </li>
    </ul>
</div>
<div class="clearfix"></div>
<div class="form__structure">
    <?php echo form_open(uri_string(), 'class="form-horizontal form-row-seperated"'); ?>
    <div class="form-body">
        
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12 form-lable-align">First Name: <span>*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo form_input($first_name); ?>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12 form-lable-align">Last Name: <span>*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo form_input($last_name); ?>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12 form-lable-align">Company Name: <span>*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo form_input($company); ?>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12 form-lable-align">Phone: </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo form_input($phone); ?>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12 form-lable-align">Password: (if changing password):</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo form_input($password); ?>
            </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12 form-lable-align">Confirm Password: (if changing password): </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo form_input($password_confirm); ?>
            </div>
        </div>
        
        <?php if ($this->ion_auth->is_admin()): ?>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12 form-lable-align">Member of groups<span>*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="mt-checkbox-inline form-mt-checkbox-inline">
                    <?php foreach ($groups as $group): ?>
                        <label class="mt-checkbox mt-checkbox-outline">
                            <?php
                            $gID = $group['id'];
                            $checked = null;
                            $item = null;
                            foreach ($currentGroups as $grp)
                            {
                                if ($gID == $grp->id)
                                {
                                    $checked = ' checked="checked"';
                                    break;
                                }
                            }
                            ?>
                            <input type="checkbox" name="groups[]" value="<?php echo $group['id']; ?>"<?php echo $checked; ?>>
                        <?php echo htmlspecialchars($group['name'], ENT_QUOTES, 'UTF-8'); ?>
                             <span></span>
                        </label>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
        <?php endif ?>
        <?php echo form_hidden('id', $user->id); ?>
        <?php echo form_hidden($csrf); ?>
        
        <div class="action-buttons">
            <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-4 col-sm-offset-4 col-xs-offset-0">
                <button type="submit" href="javascript:;" class="btn blue">Submit</button>
                <button type="reset" class="btn grey">Cancel</button>
            </div> 
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
