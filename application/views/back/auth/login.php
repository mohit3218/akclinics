<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
        <?php $this->load->view('back/partials/head'); ?>
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url(<?php echo base_url('assets/template/backend/'); ?>images/bg/bg-1.jpg);">
				<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
								<img src="<?php echo base_url('assets/template/backend/'); ?>images/logo.png">
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Sign In To Admin
								</h3>
							</div>
                            <?php echo form_open("admin/auth/login", 'class="m-login__form m-form"'); ?>
								<div class="form-group m-form__group">
									<input class="form-control m-input"   type="text" placeholder="Email" name="identity" autocomplete="off">
                                    <?php  //echo form_input($identity); ?>
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
                                    <?php  //echo form_input($password); ?>
								</div>
								<div class="row m-login__form-sub">
									<div class="col m--align-left m-login__form-left">
										<label class="m-checkbox  m-checkbox--light">
                                            <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?>
											Remember me
											<span></span>
										</label>
									</div>
									<div class="col m--align-right m-login__form-right">
										<a href="forgot_password" id="m_login_forget_password" class="m-link">
											Forget Password ?
										</a>
									</div>
								</div>
								<div class="m-login__form-action">
                                    <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">
										Sign In
									</button>
								</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end:: Page -->
        <script src="<?php echo base_url('assets/template/backend/'); ?>vendors/base/vendors.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/template/backend/'); ?>demo/default/base/scripts.bundle.js" type="text/javascript"></script>
	</body>
	<!-- end::Body -->
</html>
