<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Manager
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="<?= base_url(); ?>admin/post" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="<?= base_url(); ?>admin/post" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Post Manager
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="<?= base_url(); ?>admin/post/edit" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Edit Post
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div>
            <div class="m-portlet__head-tools">
                <a href="<?= base_url(); ?>admin/post" class="btn btn-focus m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                    <span>
                        <i class="flaticon-layers"></i>
                        <span>
                            Back
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('back/partials/message'); ?>

<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Post Edit Form
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <!--<form class="m-form">-->
                    <?php echo form_open_multipart("admin/post/edit/".$data['id'], 'class="m-form"'); ?>
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Title:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" id="title" name="title" class="form-control m-input" placeholder="Enter post title" required="required" value="<?= $data['title']; ?>"/>
                                    <input type="hidden" id="slug_name" name="slug_name" value="<?= $slug; ?>"/>
                                    <input type="hidden" id="slug" name="slug_id" value="<?= $data['slug_id']; ?>"/>
                                    <span class="m-form__help">
                                        Please enter post title
                                    </span>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Post URL Link:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" id="post_url" name="post_url" class="form-control m-input" placeholder="Enter post URL link" required="required" value="<?= $data['post_url']; ?>"/>
                                    <span class="m-form__help">
                                        Please enter post URL link
                                    </span>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Category:
                                </label>
                                <div class="col-lg-6">
                                    <select class="form-control m-select2" id="m_select2_3" name="categories_id[]" multiple="multiple" required="required">
                                        <?php
                                        foreach ($category_arr as $categories) { 
                                            $selected = '';
                                            foreach($cat_ids as $cat_id) {
                                                if($categories['id'] == $cat_id['cat_id']) {
                                                    $selected = 'selected';
                                                }
                                            }
                                        ?>
                                             <option value="<?php echo $categories['id'];?>" <?= $selected; ?>><?php echo $categories['name'];?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="m-form__help">
                                        Please select category
                                    </span>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Author:
                                </label>
                                <div class="col-lg-6">
                                    <select class="form-control m-select2" id="m_select2_2" name="post_author" required="required">
                                        <option value="">Please Select</option>
                                        <?php
                                        foreach ($users_arr as $user) { 
                                            $selected = '';
                                            if($data['post_author'] == $user['id']) {
                                                $selected = 'selected';
                                            }
                                        ?>
                                             <option value="<?php echo $user['id'];?>" <?= $selected; ?>><?php echo $user['name'];?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="m-form__help">
                                        Please select Author
                                    </span>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Content:
                                </label>
                                <div class="col-lg-12">
                                    <!--<textarea type="text" name="content" class="form-control m-input ckeditor" placeholder="Enter blog content" required="required" value='<?= $data['content']; ?>'><?= $data['content']; ?></textarea>-->
                                    <?php echo $this->ckeditor->editor("content", $data['content'] ,array('class' => 'form-control m-input', 'required' => 'required')); ?>
                                    <span class="m-form__help">
                                        Please enter post content
                                    </span>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Featured Old Image
                                </label>
                                <div></div>
                                <div class="col-lg-6 custom-file">
                                    <input type="hidden" name="old_file_name" value="<?= $data['image']?>">
                                    <a class="btn default btn-xs purple ajaxify" href="<?= base_url($data['image']); ?>" target="_blank"> <i class="flaticon-eye"></i> View </a>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Featured Image
                                </label>
                                <div></div>
                                <div class="col-lg-6 custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="image">
                                    <label class="custom-file-label" for="customFile">
                                        Choose file
                                    </label>
                                </div>
                            </div>
							
							<div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Alt:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" id="title" name="image_alt" class="form-control m-input" placeholder="Enter feature image alt tag"  value="<?= $data['image_alt']?>" />
                                    <span class="m-form__help">
                                        Please enter post title
                                    </span>
                                </div>
                            </div>
                            
                            <div class="m-form__group form-group row">
                                <label class="col-lg-4 col-form-label">
                                    Post Status:
                                </label>
                                <div class="col-lg-6">
                                    <div class="m-checkbox-inline">
                                        <?php foreach (StaticArray::$blog_post_status as $key => $blog_status) :?>
                                        <label class="m-checkbox">
                                            <?php 
                                                $checked = '';
                                                if($key == $data['post_status']){ $checked = 'checked'; } ?>
                                                <input type="checkbox" name="post_status" value="<?= $key; ?>" <?= $checked; ?> /> <?= $blog_status; ?>
                                            <span></span>
                                        </label>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Document Title:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="document_title" class="form-control m-input" placeholder="Enter document title" value="<?= $data['document_title']?>" required="required" />
                                    <span class="m-form__help">
                                        Please enter document title
                                    </span>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Meta Description:
                                </label>
                                <div class="col-lg-6">
                                    <textarea type="text" name="meta_description"  id="meta_description" class="form-control m-input" value="<?= $data['meta_description']; ?>"><?= $data['meta_description']; ?></textarea>
                                    <span class="m-form__help">
                                        Please enter meta description
                                    </span>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Meta Keywords:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="meta_keywords" class="form-control m-input" value="<?= $data['meta_keywords']?>" placeholder="Enter meta keywords"/>
                                    <span class="m-form__help">
                                        Please enter meta keywords
                                    </span>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Canonical URL:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="canonical_url" class="form-control m-input" value="<?= $data['canonical_url']?>"  placeholder="Enter canonical URL"/>
                                    <span class="m-form__help">
                                        Please enter canonical URL
                                    </span>
                                </div>
                            </div>
                            
                            <div class="m-form__group form-group row">
                                <label class="col-lg-4 col-form-label">
                                    Robots Meta Settings:
                                </label>
                                <div class="col-lg-6">
                                    <div class="m-checkbox-inline">
                                        <label class="m-checkbox">
                                            <?php 
                                                $checked = '';
                                                if($data['no_index'] == 1){ $checked = 'checked'; } ?>
                                            <input type="checkbox" id="noindex" name="no_index" <?= $checked; ?> /> Apply noindex to this post/page
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="m-checkbox-inline">
                                        <label class="m-checkbox">
                                            <?php 
                                                $checked = '';
                                                if($data['no_follow'] == 1){ $checked = 'checked'; } ?>
                                            <input type="checkbox" id="nofollow"  name="no_follow" <?= $checked; ?> /> Apply nofollow to this post/page
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="m-checkbox-inline">
                                        <label class="m-checkbox">
                                            <?php 
                                                $checked = '';
                                                if($data['no_archive'] == 1){ $checked = 'checked'; } ?>
                                            <input type="checkbox" id="noarchive"  name="no_archive" <?= $checked; ?> />Apply noarchive to this post/page
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="m-form__seperator m-form__seperator--dashed"></div>
                        <div class="m-form__section m-form__section--last">
                            <div class="m-form__heading">
                                <h3 class="m-form__heading-title">
                                    OG Info:
                                </h3>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    OG Title:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="og_title"  id="og_title" class="form-control m-input" value="<?= $data['og_title']; ?>">
                                    <span class="m-form__help">
                                        Please enter OG title
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    OG Description:
                                </label>
                                <div class="col-lg-6">
                                    <textarea type="text" name="og_description"  id="meta_description" class="form-control m-input" value="<?= $data['og_description']; ?>"><?= $data['og_description']; ?></textarea>
                                    <span class="m-form__help">
                                        Please enter OG description
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    OG Image:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="og_image"  id="og_image" class="form-control m-input" value="<?= $data['og_image']; ?>">
                                    <span class="m-form__help">
                                        Please enter OG image
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="m-form__seperator m-form__seperator--dashed"></div>
                        <div class="m-form__section m-form__section--last">
                            <div class="m-form__heading">
                                <h3 class="m-form__heading-title">
                                    Interlinking Info:
                                </h3>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Title:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" id="title" name="interlink_title" class="form-control m-input" placeholder="Enter inetlink post title" value="<?= $data['interlink_title']; ?>"/>
                                    <!--<input type="hidden" id="slug" name="slug"/>-->
                                    <span class="m-form__help">
                                        Please enter post title
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Content:
                                </label>
                                <div class="col-lg-6">
                                    <textarea type="text" id="editor1" name="interlink_content" maxlength="250" class="form-control m-input" placeholder="Enter inetlink post content" value="<?= $data['interlink_content']; ?>"><?= $data['interlink_content']; ?></textarea>
                                    <span class="m-form__help">
                                        Please enter post content
                                    </span>
                                </div>
                            </div>
                            <!--<div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Interlink Old Image
                                </label>
                                <div></div>
                                <div class="col-lg-6 custom-file">
                                    <input type="hidden" name="old_inter_file_name" value="<?= $data['interlink_image']?>">
                                    <a class="btn default btn-xs purple ajaxify" href="<?= base_url($data['interlink_image']); ?>" target="_blank"> <i class="flaticon-eye"></i> View </a>
                                </div>
                            </div>-->
                            <!--<div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Featured Image
                                </label>
                                <div></div>
                                <div class="col-lg-6 custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="interlink_image">
                                    <label class="custom-file-label" for="customFile">
                                        Choose file
                                    </label>
                                </div>
                            </div>-->
                        </div>
                        <div class="m-form__seperator m-form__seperator--dashed"></div>
                        <div class="m-form__section m-form__section--last">
                            <div class="m-form__heading">
                                <h3 class="m-form__heading-title">
                                    Post Date Update:
                                </h3>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Post Date
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="post_date" class="form-control datepicker" id="m_datepicker_1" readonly value="<?= date('m/d/Y', strtotime($data['post_date']));  ?>" placeholder="Select date"/>
                                    <span class="m-form__help">
                                        Date Format is: MM-DD-YYYY
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-success">
                                        Submit
                                    </button>
                                    <a class="btn btn-secondary m-btn m-btn--icon" id="m_reset" type="reset" href="<?= base_url('admin/post'); ?>">
                                        <span>
                                            <span>
                                                Cancel
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
               <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#title').change(function() 
{
    $(this).val($.trim($(this).val()));
    $(this).val($(this).val().replace(/\s+/g,' '));
    $('#post_url').val($(this).val().toLowerCase());
    $('#post_url').val($('#post_url').val().replace(/\W/g, ' '));
    $('#post_url').val($.trim($('#post_url').val()));
    $('#post_url').val('<?php echo base_url()?>blog/' + $('#post_url').val().replace(/\s+/g, '-'));
    
    $('#slug_name').val($(this).val().toLowerCase());
    $('#slug_name').val($('#slug_name').val().replace(/\W/g, ' '));
    $('#slug_name').val($.trim($('#slug_name').val()));
    $('#slug_name').val($('#slug_name').val().replace(/\s+/g, '-'));
});

$('#noindex','#nofollow','#noarchive').on('change', function(){
   this.value = this.checked ? 1 : 0;
}).change();
</script>