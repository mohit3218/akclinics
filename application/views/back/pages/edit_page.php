<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Manager
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="/admin/page" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Slug Manager
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Edit Page
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div>
            <div class="m-portlet__head-tools">
                <a href="<?= base_url(); ?>admin/page" class="btn btn-focus m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                    <span>
                        <i class="flaticon-layers"></i>
                        <span>
                            Back
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('back/partials/message'); ?>

<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Page Edit Form
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <!--<form class="m-form">-->
                <?php echo form_open('admin/page/edit/'.$data['id'], 'class="m-form"'); ?>
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Meta Name:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="name"  id="name" class="form-control m-input" value="<?= $data['name']; ?>">
                                    <span class="m-form__help">
                                        Please enter page name
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="exampleSelect1" class="col-lg-4 col-form-label">
                                    Slug:
                                </label>
                                <div class="col-lg-6">
                                    <select class="form-control m-input" id="exampleSelect1" name="slug_id">
                                        <?php foreach ($slugs_arr as $slug) : ?>
                                        <?php if($slug['id'] == $data['slug_id']) : ?>
                                        <option value="<?= $slug['id']; ?>" selected="selected"><?= $slug['name']; ?></option>
                                        <?php endif; ?>
                                        <option value="<?= $slug['id']; ?>"> <?= $slug['name']; ?> </option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="m-form__help">
                                        Please enter slug
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Meta Title:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="meta_title"  id="meta_title" class="form-control m-input" value="<?= $data['meta_title']; ?>">
                                    <span class="m-form__help">
                                        Please enter meta title
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Meta Keyword:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="meta_keywords"  id="meta_keywords" class="form-control m-input" value="<?= $data['meta_keywords']; ?>">
                                    <span class="m-form__help">
                                        Please enter meta keyword
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Meta Description:
                                </label>
                                <div class="col-lg-6">
                                    <textarea type="text" name="meta_description"  id="meta_description" class="form-control m-input" value="<?= $data['meta_description']; ?>"><?= $data['meta_description']; ?></textarea>
                                    <span class="m-form__help">
                                        Please enter meta description
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    OG Title:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="og_title"  id="og_title" class="form-control m-input" value="<?= $data['og_title']; ?>">
                                    <span class="m-form__help">
                                        Please enter OG title
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    OG Description:
                                </label>
                                <div class="col-lg-6">
                                    <textarea type="text" name="og_description"  id="meta_description" class="form-control m-input" value="<?= $data['og_description']; ?>"><?= $data['og_description']; ?></textarea>
                                    <span class="m-form__help">
                                        Please enter OG description
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    OG Image:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="og_image"  id="og_image" class="form-control m-input" value="<?= $data['og_image']; ?>">
                                    <span class="m-form__help">
                                        Please enter OG image
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Canonical URL:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="canonical_url"  id="og_image" class="form-control m-input" value="<?= $data['canonical_url']; ?>">
                                    <span class="m-form__help">
                                        Please enter canonical url
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-success">
                                        Submit
                                    </button>
                                    <a class="btn btn-secondary m-btn m-btn--icon" id="m_reset" type="reset" href="<?= base_url('admin/page'); ?>">
                                        <span>
                                            <span>
                                                Cancel
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
               <?php echo form_close(); ?>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>