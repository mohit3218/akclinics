<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Manager
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="<?= base_url(); ?>admin/slug" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="<?= base_url(); ?>admin/slug" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Slug Manager
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="<?= base_url(); ?>admin/slug/create" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Add Slug
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div>
            <div class="m-portlet__head-tools">
                <a href="<?= base_url(); ?>admin/slug" class="btn btn-focus m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                    <span>
                        <i class="flaticon-layers"></i>
                        <span>
                            Slug Summary
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('back/partials/message'); ?>

<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Slug Add Form
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <!--<form class="m-form">-->
                    <?php echo form_open("admin/slug/create", 'class="m-form"'); ?>
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Slug Name:
                                </label>
                                <div class="col-lg-6">
                                    <?php echo form_input($name); ?>
                                    <span class="m-form__help">
                                        Please enter Slug
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-success">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn btn-secondary">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
               <?php echo form_close(); ?>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>