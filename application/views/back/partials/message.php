<?php if ($this->session->flashdata('success')) : ?>
    <div class="m-content m-content-message-info">
        <div class="row">
            <div class="col-xl-12">
                <div class="m-portlet">
                    <div class="m-portlet__body">
                        <div class="m-section">
                            <div class="m-section__content">
                                <div class="alert alert-success col-12" role="alert">
                                    <strong>
                                        Successfully!
                                    </strong>
                                    <?= $this->session->flashdata('success'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('info')) : ?>
    <div class="m-content m-content-message-info">
        <div class="row">
            <div class="col-xl-12">
                <div class="m-portlet">
                    <div class="m-portlet__body">
                        <div class="m-section">
                            <div class="m-section__content">
                                <div class="alert alert-info col-12" role="alert">
                                    <strong>
                                        Heads up!
                                    </strong>
                                    <?= $this->session->flashdata('info'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('failure')) : ?>
    <div class="m-content m-content-message-info">
        <div class="row">
            <div class="col-xl-12">
                <div class="m-portlet">
                    <div class="m-portlet__body">
                        <div class="m-section">
                            <div class="m-section__content">
                                <div class="alert alert-danger col-12" role="alert">
                                    <strong>
                                        Oh snap!
                                    </strong>
                                    <?= $this->session->flashdata('failure'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $(".m-content-message-info").delay(3000).fadeOut(1);
    });
</script>