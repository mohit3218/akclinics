<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD -->
    <head>
        <?php $this->load->view('back/partials/head'); ?>
    </head>
    <!-- END HEAD -->
    <body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <!-- BEGIN HEADER -->
            <?php $this->load->view('back/partials/header'); ?>
            <!-- END HEADER -->
            <!-- BEGIN CONTAINER -->
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
                <!-- BEGIN SIDEBAR -->
                <?php $this->load->view('back/partials/sidebar'); ?>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="m-grid__item m-grid__item--fluid m-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                        <?php echo $contents;?>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <?php $this->load->view('back/partials/footer'); ?>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url('assets/template/backend/'); ?>vendors/base/vendors.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/template/backend/'); ?>demo/default/base/scripts.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/template/backend/'); ?>vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/template/backend/'); ?>demo/default/custom/crud/datatables/basic/basic.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/template/backend/'); ?>demo/default/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/template/backend/'); ?>demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/template/backend/'); ?>ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/template/backend/'); ?>ckfinder/ckfinder.js"></script>
    </body>
</html>