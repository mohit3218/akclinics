<?php
$menu = AdminMenu::$adminMenuConfig;
$admin = $this->uri->segment(1);
$controller = $this->uri->segment(2);
$method = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : '';
$action = !empty($method) ? '/' . $method : '';
$route = $admin . '/' . $controller . $action;
?>
<!-- BEGIN: Left Aside -->
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div 
        id="m_ver_menu" 
        class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
        m-menu-vertical="1"
        m-menu-scrollable="1" m-menu-dropdown-timeout="500"  
        style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <?php foreach($menu as $item): ?>
            <?php
                $active = '';
                foreach($item['links'] as $link)
                {
                    if ($link["route"] == $route || in_array($route, $link["other_routes"]))
                    {
                        $active = 'm-menu__item--open m-menu__item--expanded';
                    }
                }
            ?>
            <li class="m-menu__item  m-menu__item--submenu <?php  echo $active ?>" aria-haspopup="true"  m-menu-submenu-toggle="hover">
                <a  href="javascript:;" class="m-menu__link m-menu__toggle">
                    <i class="<?php echo $item['class'] ?> "></i>
                    <span class="m-menu__link-text">
                        <?php echo $item['title'] ?> 
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <?php foreach($item['links'] as $link): ?>
                        <?php $sub_active = $link["route"] == $route || in_array($route, $link["other_routes"]); ?>
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                            <span class="m-menu__link">
                                <span class="m-menu__link-text">
                                    <?php echo $item['title'] ?> 
                                </span>
                            </span>
                        </li>
                        <li class="m-menu__item <?php  echo $sub_active ? 'm-menu__item--active' : '' ?>" aria-haspopup="true" >
                            <?php $controller = (!empty($link['controller'])) ?  '/'. $link['controller'] : ''; ?>
                            <a  href="<?php echo '/admin' . $controller . '/' . $link['action'] ?>" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-layers">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    <?php echo $link['page_title'] ?> 
                                </span>
                            </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>