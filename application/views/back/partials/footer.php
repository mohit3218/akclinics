<footer class="m-grid__item		m-footer ">
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                <span class="m-footer__copyright">
                    2007 &copy; 
                    <a href="https://akclinics.org/" class="m-link">
                        AK Clinics
                    </a>
                </span>
            </div>
            <!--<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                    <div class="m-stack__item m-stack__item--middle m-brand__logo">
                        <a href="admin/slugs" class="m-brand__logo-wrapper">
                            <img alt="" src="<?php echo base_url('assets/template/backend/'); ?>images/logo.png"/>
                        </a>
                    </div>
                </ul>
            </div>-->
        </div>
    </div>
</footer>