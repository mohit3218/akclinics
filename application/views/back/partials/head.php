<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
<title>AK Clinics</title>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<script>
    WebFont.load({
        google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
        active: function () {
            sessionStorage.fonts = true;
        }
    });
</script>
<!--end::Web font -->
<!--begin::Base Styles -->
<link href="<?php echo base_url('assets/template/backend/'); ?>vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/template/backend/'); ?>demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/template/backend/'); ?>vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="<?php echo base_url('assets/template/'); ?>favicon.ico" />
<script src="<?php echo base_url('assets/template/backend/'); ?>js/jquery.min.js" type="text/javascript"></script>