<?php
/**
 * Blogs Index View - Shows Blog summary for Admin users
 * 
 * @created    22/10/2018
 * @package    AK Clinics
 * @copyright  Copyright (C) 2018
 * @author     Mohit Thakur
 */
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($search))
{
    if ($search == TRUE)
    {
        $cat_id = $search_data['categories_id'];
        $post_blog_sts = $search_data['blog_status'];
        $is_active = ($search_data['is_active'] != '') ? $search_data['is_active'] : -1;
        $title = $search_data['title'];
    }
}
else
{
    $cat_id = '';
    $is_active = -1;
}
?>
<style>
    .m-portlet .m-portlet__body {padding: 2.2rem 1rem !important;}
    .hide,label.m-checkbox.m-checkbox--single.m-checkbox--solid.m-checkbox--brand {display: none !important;}
    .blog-img-view{width: 50px;}
    .blog-img-view-modal{position: relative; left: 8%;}
    .anchor-link-modal{cursor: pointer;}
    .blog-image-modal .modal-dialog.modal-dialog-centered {width: 85% !important;max-width: 100%;background: transparent !important;}
    /*.blog-image-modal .modal-body {background-color: whitesmoke;}*/
</style>
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Manager
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Blog Manager
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Blog Summary
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div>
            <div class="m-portlet__head-tools">
                <a href="<?= base_url(); ?>admin/blogs/create" class="btn btn-focus m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                    <span>
                        <i class="la la-user"></i>
                        <span>
                            New Record
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('back/partials/message'); ?>

<!-- END: Subheader -->
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Search Form
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <form class="m-form m-form--fit m--margin-bottom-20">
                <div class="row m--margin-bottom-20">
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                        <label>
                            Blog Tirle:
                        </label>
                        <input type="text" class="form-control m-input" placeholder="Blog Title" data-col-index="0" name="title" value='<?= (isset($title)) ? $title : ''; ?>'>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                        <label>
                            Category:
                        </label>
                        <select class="form-control m-select2" id="m_select2_1" id="exampleSelect1" name="categories_id">
                            <option value="">Please Select</option>
                            <?php 
                                foreach ($category_arr as $key => $value) : 
                                    if ($cat_id == $value['id']) { $sele = 'selected'; } 
                                    else { $sele = ''; }
                                    echo '<option value="' . $value['id'] . '" ' . $sele . '>' . $value['name'] . '</option>';                                                        
                                endforeach; 
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                        <label>
                            Post Status:
                        </label>
                        <select class="form-control m-input" id="exampleSelect1" name="blog_status">
                            <option value="">Please Select</option>
                            <?php 
                                foreach (StaticArray::$blog_post_status as $key => $value) : 
                                    if ($post_blog_sts == $key) { $sele = 'selected'; } 
                                    else { $sele = ''; }
                                    echo '<option value="' . $key . '" ' . $sele . '>' . $value . '</option>';                                                        
                                endforeach; 
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                        <label>
                            Status:
                        </label>
                        <select class="form-control m-input" id="exampleSelect1" name="is_active">
                            <option value="">Please Select</option>
                            <?php 
                                foreach (StaticArray::$status_arr as $key => $value) : 
                                    if ($is_active == $key) { $sele = 'selected'; } 
                                    else { $sele = ''; }
                                    echo '<option value="' . $key . '" ' . $sele . '>' . $value . '</option>';                                                        
                                endforeach; 
                            ?>
                        </select>
                    </div>
                </div>
                <div class="m-separator m-separator--md m-separator--dashed"></div>
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-brand m-btn m-btn--icon" id="m_search" type="submit" name="search" value="search">
                            <span>
                                <i class="la la-search"></i>
                                <span>
                                    Search
                                </span>
                            </span>
                        </button>
                        &nbsp;&nbsp;
                        <a class="btn btn-secondary m-btn m-btn--icon" id="m_reset" type="reset" href="<?= base_url('admin/blogs'); ?>">
                            <span>
                                <i class="la la-close"></i>
                                <span>
                                    Reset
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Blogs Record
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                <thead>
                    <tr>
                        <th></th>
                        <th>Sr No.</th>
                        <th>Title </th>
                        <th>Category </th>
                        <!--<th>Content</th>-->
                        <th>Image</th>
                        <th>Blog Link</th>
                        <th>Post Status </th>
                        <!--<th>Post Date </th>-->
                        <th>Status </th>
                        <th>Created Date </th>
                        <th>Actions</th>
                        <th class="hide"> </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($blogs as $blog): ?>
                    <tr class="odd gradeX">
                        <td></td>
                        <td><?php echo $blog['id']; ?></td>
                        <td><?php echo $blog['title']; ?></td>
                        <td><?php echo $category_list[$blog['categories_id']]; ?></td>
                        <!--<td><?php echo $blog['content']; ?></td>-->
                        <?php if($blog['is_cron'] == 1) { ?>
                        <td>
                            <a href="javascript:;" class="anchor-link-modal" data-toggle="modal" data-cron="<?= $blog['is_cron']; ?>" data-id="<?= $blog['image']; ?>">
                                <img class="blog-img-view" src="<?php echo $blog['image']; ?>" alt="" >
                            </a>
                        </td>
                        <?php } else { ?>
                        <td>
                            <a href="javascript:;" class="anchor-link-modal" data-toggle="modal" data-cron="<?= $blog['is_cron']; ?>" data-id="<?= $blog['image']; ?>">
                                <img class="blog-img-view" src="<?php echo base_url() . '/'.$blog['image']; ?>" alt="" >
                            </a>
                        </td>
                        <?php } ?>
                        <td><?php echo $blog['blog_url']; ?></td>
                        <td>
                            <?php 
                                if($blog['blog_status'] == 1) {
                                    $active_staus = 'm-badge  m-badge--brand m-badge--wide';
                                    $blog_post_staus = 'Private';
                                }else if($blog['blog_status'] == 2) {
                                    $active_staus = 'm-badge  m-badge--success m-badge--wide'; 
                                    $blog_post_staus = 'Public';
                                }else{
                                    $active_staus = 'm-badge  m-badge--danger m-badge--wide'; 
                                    $blog_post_staus = 'Uncategorized';
                                }
                            ?>
                            <span class="<?= $active_staus; ?>"><?= $blog_post_staus; ?></span>
                        </td>
                        <!--<td><?php echo $blog['blog_post_date']; ?></td>-->
                        <td>
                            <?php $active_staus = ($blog['is_active'] == 1) ? 'm-badge  m-badge--success m-badge--wide' : 'm-badge  m-badge--danger m-badge--wide'; ?>
                            <?php $staus = ($blog['is_active'] == 1) ? 'Active' : 'In-Active'; ?>
                            <span class="<?= $active_staus; ?>"><?= $staus; ?></span>
                        </td>
                        <td><?php echo date("d-m-Y H:i:s", strtotime($blog['created_on'])); ?></td>
                        <td>
                            <a href="<?= base_url(); ?>admin/blogs/edit/<?= $blog['id']; ?>" title="Edit" class="icon currency-color">
                                <i class=" la la-edit"></i>
                            </a>
                            <a href="<?= base_url(); ?>admin/blogs/delete/<?= $blog['id']; ?>" onclick="return confirm('Do you want delete this record');" title="Delete">
                                <i class=" la la-trash"></i>
                            </a>
                        </td>
                        <td class="hide"></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
<div class="modal fade blog-image-modal modal-full-screen" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    Post Image
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <img class="blog-img-view-modal" alt="" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
   $(".anchor-link-modal").click(function(){ // Click to only happen on announce links
        var postImage = $(this).data('id');
        var cronStatus = $(this).data('cron');
        if(cronStatus == 1)
        {
            $(".blog-img-view-modal").attr("src", postImage);
        }
        else
        {
            $(".blog-img-view-modal").attr("src", '<?php echo base_url();?>' + postImage);
        }
     $('.blog-image-modal').modal('show');
   });
});
</script>