<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Manager
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="<?= base_url(); ?>admin/blogs" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="<?= base_url(); ?>admin/blogs" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Blog Manager
                        </span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="<?= base_url(); ?>admin/blogs/edit" class="m-nav__link">
                        <span class="m-nav__link-text">
                            Edit Blog
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div>
            <div class="m-portlet__head-tools">
                <a href="<?= base_url(); ?>admin/blogs" class="btn btn-focus m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                    <span>
                        <i class="flaticon-layers"></i>
                        <span>
                            Back
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('back/partials/message'); ?>

<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Blog Edit Form
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <!--<form class="m-form">-->
                    <?php echo form_open_multipart("admin/blogs/edit/".$data['id'], 'class="m-form"'); ?>
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Title:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="title" class="form-control m-input" placeholder="Enter blog title" required="required" value="<?= $data['title']; ?>"/>
                                    <span class="m-form__help">
                                        Please enter blog title
                                    </span>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Category:
                                </label>
                                <div class="col-lg-6">
                                    <select class="form-control m-select2" id="m_select2_1" name="categories_id"  required="required">
                                        <option value="">Please Select</option>
                                        <?php
                                        foreach ($category_arr as $categories) { ?>
                                             <option value="<?php echo $categories['id'];?>"<?php if($categories['id']==$data['categories_id']){ echo "selected";}?>><?php echo $categories['name'];?></option>
                                        <?php }
                                        ?>
                                    </select>
                                    <span class="m-form__help">
                                        Please select category
                                    </span>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Content:
                                </label>
                                <div class="col-lg-6">
                                    <textarea type="text" name="content" class="form-control m-input" placeholder="Enter blog content" required="required" maxlength="250" value="<?= $data['content']; ?>"><?= $data['content']; ?></textarea>
                                    <span class="m-form__help">
                                        Please enter blog content
                                    </span>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Old Image
                                </label>
                                <div></div>
                                <div class="col-lg-6 custom-file">
                                    <input type="hidden" name="old_file_name" value="<?= $data['image']?>">
                                    <a class="btn default btn-xs purple ajaxify" href="<?= base_url($data['image']); ?>" target="_blank"> <i class="flaticon-eye"></i> View </a>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    File Image
                                </label>
                                <div></div>
                                <div class="col-lg-6 custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="image">
                                    <label class="custom-file-label" for="customFile">
                                        Choose file
                                    </label>
                                </div>
                            </div>
                            
                            <div class="form-group m-form__group row">
                                <label class="col-lg-4 col-form-label">
                                    Blog URL Link:
                                </label>
                                <div class="col-lg-6">
                                    <input type="text" name="blog_url" class="form-control m-input" placeholder="Enter blog url link" required="required" value="<?= $data['blog_url']; ?>"/>
                                    <span class="m-form__help">
                                        Please enter blog url link
                                    </span>
                                </div>
                            </div>
                            
                            <div class="m-form__group form-group row">
                                <label class="col-lg-4 col-form-label">
                                    Blog Post Status:
                                </label>
                                <div class="col-lg-6">
                                    <div class="m-checkbox-inline">
                                        <?php foreach (StaticArray::$blog_post_status as $key => $blog_status) :?>
                                        <label class="m-checkbox">
                                            <?php 
                                                $checked = '';
                                                if($key == $data['blog_status']){ $checked = 'checked'; } ?>
                                                <input type="checkbox" name="blog_status" value="<?= $key; ?>" <?= $checked; ?> /> <?= $blog_status; ?>
                                            <span></span>
                                        </label>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-success">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn btn-secondary">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
               <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>