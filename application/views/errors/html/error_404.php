<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<style type="text/css">

#container {
	background: #f2f3f7;
	border: 30px solid #FFF;
	position: absolute;
	height: 100%;
	width: 100%;
	left: 0;
	top: 0
}
#container .overlay {
	position: absolute;
	background-repeat: repeat;
	opacity: 0.8;
	width: 100%;
	height: 100%
}
#container .item-title {
	position: absolute;
	z-index: 0;
	left: 0;
	padding: 0;
	top: 50vh;
	width: 100%;
	padding: 0 5%;
	-webkit-transition: all 0.3s cubic-bezier(0, 0, 0.58, 1);
	-moz-transition: all 0.3s cubic-bezier(0, 0, 0.58, 1);
	-ms-transition: all 0.3s cubic-bezier(0, 0, 0.58, 1);
	-o-transition: all 0.3s cubic-bezier(0, 0, 0.58, 1);
	transition: all 0.3s cubic-bezier(0, 0, 0.58, 1);
	-webkit-transform: translateY(-50%);
	-moz-transform: translateY(-50%);
	-ms-transform: translateY(-50%);
	-o-transform: translateY(-50%);
	transform: translateY(-50%);
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
	opacity: 1;
	visibility: visible
}
#container .item-title #message {
	max-width: 600px;
	min-height: 350px;
	margin: 0 auto
}
#container .item-title #message p:first-child {
	font-size: 110px;
	font-weight: 200;
	color: #000
}
#container .item-title .link-bottom {
	margin: 0 auto;
	max-width: 600px
}
#container .item-title .link-bottom .link-icon {
	margin-right: 10px;
	font-size: 22px
}
@media only screen and (max-width: 480px) {
#container {
	min-height: 100%;
	height: auto
}
#container .overlay {
	min-height: 100%;
	height: auto
}
#container .item-title {
	padding: 100px 5%;
	position: relative;
	top: 0;
	-webkit-transform: translateY(0);
	-moz-transform: translateY(0);
	-ms-transform: translateY(0);
	-o-transform: translateY(0);
	transform: translateY(0)
}
#container .item-title #message {
	min-height: 240px
}
#container .item-title #message p:first-child {
	font-size: 50px;
	font-weight: 200
}
}
</style>
</head>
<body>
<div id="container">
  <div class="overlay"></div>
  <div class="item-title">
    <div id="message">
      <p>ERROR 404 NOT FOUND</p>
      <p>You may have mis-typed the URL.</p>
      <p>Or the page has been removed.</p>
      <p>Actually, there is nothing to see here...</p>
      <p>Click on the links below to do something, Thanks!</p>
    </div>
    <div class="link-bottom"> <a class="link-icon" href="#"><i class="icon ion-ios-home"></i> HOME</a> <a class="link-icon" href="mailto:someone@example.com"><i class="icon ion-ios-compose"></i> WRITE US</a></div>
  </div>
</div>
<div id="containevr">
  <h1><?php echo $heading; ?></h1>
  <?php echo $message; ?> </div>
</body>
</html>