<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->library(array('ion_auth', 'form_validation','pagination'));
		$this->load->helper(array('url', 'language'));
        $this->load->model('category_model');
        $this->load->model('common_functions');
    }

    public function index($offset = 0)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			//$this->data['slugs'] = $this->slug_model->get_slugs();
            $search['search_status'] = FALSE;
            $where = '';
            $serach_string = '';
            if ($this->input->get('search'))
            {
                $serach_string = $this->input->server('QUERY_STRING');
                $config['enable_query_strings'] = TRUE;
                $config['suffix'] = '?' . $serach_string;
                $search_data = $this->input->get();
                $where = $this->category_model->categories_view_search_coundtiotn($search_data);
                if ($where != NULL)
                {
                    $where = $where;
                }
                else
                {
                    $where = '';
                }
                $this->data['search'] = TRUE;
                $this->data['search_data'] = $search_data;
            }
            $num_rows = $this->category_model->count_categories($where);
            $config['per_page'] = 100;
            $config['num_links'] = $num_rows;
            $this->pagination->initialize($config);
            $this->data['categories'] = $this->category_model->get_categories_data($config['per_page'], $offset, $search, $where);
            $this->data['totalRecords'] = $num_rows;
            
            $this->template->load('layout', 'contents' , 'back/categories/' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
    }


    public function create()
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        $this->load->helper('form');
        $this->load->library('form_validation');
 
        $this->data['title'] = 'Create a Categories';
 
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('document_title', 'Meta Title', 'required');
        $this->form_validation->set_rules('meta_description', 'Meta Description', 'required');
 
        if ($this->form_validation->run() === FALSE)
        {
             //$this->session->set_flashdata('failure', 'Unable to add slug!');
            if(validation_errors())
            {
                $this->session->set_flashdata('failure', 'Unable to add category!');
                $this->template->load('layout', 'contents' , 'back/categories/' . DIRECTORY_SEPARATOR . 'create_category', $this->data);
            }
            else
            {
                $this->template->load('layout', 'contents' , 'back/categories/' . DIRECTORY_SEPARATOR . 'create_category', $this->data);
            }
        }
        else
        {
            $this->category_model->add_categories();
            
            $this->session->set_flashdata('success', 'Category added successfully!');
			redirect("admin/categories", 'refresh');
        }
    }

    public function edit($id)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->data['title'] = 'Edit a Categorie';
        $this->data['data'] = $this->category_model->get_categories_by_id($id);

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('document_title', 'Meta Title', 'required');
        $this->form_validation->set_rules('meta_description', 'Meta Description', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            if(validation_errors())
            {
                $this->session->set_flashdata('failure', 'Unable to update category!');
                $this->template->load('layout', 'contents' , 'back/categories/' . DIRECTORY_SEPARATOR . 'edit_category', $this->data);
            }
            else
            {
                $this->template->load('layout', 'contents' , 'back/categories/' . DIRECTORY_SEPARATOR . 'edit_category', $this->data);
            }
        }
        else
        {
            $this->category_model->add_categories($id);
            $this->session->set_flashdata('success', 'Category updated successfully!');
			redirect("admin/categories", 'refresh');
        }
    }

    public function delete($id)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        if (empty($id))
        {
            show_404();
        }

        $result = $this->category_model->delete_categories($id);
        
        if($result)
        {
            $this->session->set_flashdata('success', 'Category deleted successfully!');
        }
        else
        {
           $this->session->set_flashdata('failure', 'The record cannot be deleted, as it has associated data!');
        }
        
        redirect("admin/categories", 'refresh');
    }

}
