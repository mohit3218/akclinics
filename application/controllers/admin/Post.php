<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->library(array('ion_auth', 'form_validation','pagination','ckeditor','ckfinder'));
		$this->load->helper(array('url', 'language'));
        $this->load->model('post_model');
        $this->load->model('category_model');
        $this->load->model('common_functions');
        $this->ckeditor->basePath = base_url().'assets/template/backend/ckeditor/';
        $this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../assets/template/backend/ckfinder/'); 
    }
    
    public function index($offset = 0)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			//$this->data['slugs'] = $this->slug_model->get_slugs();
            $this->data['users_list'] = $this->common_functions->users_list();
            $search['search_status'] = FALSE;
            $where = '';
            $serach_string = '';
            if ($this->input->get('search'))
            {
                $serach_string = $this->input->server('QUERY_STRING');
                $config['enable_query_strings'] = TRUE;
                $config['suffix'] = '?' . $serach_string;
                $search_data = $this->input->get();
                $where = $this->post_model->posts_view_search_coundtiotn($search_data);
                if ($where != NULL)
                {
                    $where = $where;
                }
                else
                {
                    $where = '';
                }
                $this->data['search'] = TRUE;
                $this->data['search_data'] = $search_data;
            }
            $num_rows = $this->post_model->count_posts($where);
            $config['per_page'] = 1000;
            $config['num_links'] = $num_rows;
            $this->pagination->initialize($config);
            $this->data['category_list'] = $this->category_model->get_categories_list();
            $this->data['category_arr'] = $this->category_model->get_categories_arr();
            $this->data['posts'] = $this->post_model->get_posts_data($config['per_page'], $offset, $search, $where);
            $this->data['totalRecords'] = $num_rows;
            
            $this->template->load('layout', 'contents' , 'back/posts/' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
    }
    
    public function create()
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        $this->load->helper('form');
        $this->load->library('form_validation');
 
        $this->data['title'] = 'Create a Post';
        $this->data['category_arr'] = $this->category_model->get_categories_arr();
        $this->data['users_arr'] = $this->common_functions->user_list_arr();
        //debug($this->data['users_arr']);exit;
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('content', 'Content', 'required');
        $this->form_validation->set_rules('post_url', 'Blog URL', 'required');
 
        if ($this->form_validation->run() === FALSE)
        {
             //$this->session->set_flashdata('failure', 'Unable to add slug!');
            if(validation_errors())
            {
                $this->session->set_flashdata('failure', 'Unable to add post!');
                $this->template->load('layout', 'contents' , 'back/posts/' . DIRECTORY_SEPARATOR . 'create_post', $this->data);
            }
            else
            {
                $this->template->load('layout', 'contents' , 'back/posts/' . DIRECTORY_SEPARATOR . 'create_post', $this->data);
            }
        }
        else
        {
            $this->post_model->add_posts();
            
            $this->session->set_flashdata('success', 'Post added successfully!');
			redirect("admin/post", 'refresh');
        }
    }
    
    public function edit($id, $slug_id = NULL)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->data['title'] = 'Edit a Post';
        $this->data['category_arr'] = $this->category_model->get_categories_arr();
        
        $post_cat_query = $this->db->get_where('posts_cat_ids', array('post_id' => $id));
        $post_cat_data = $post_cat_query->result_array();
        
        $this->data['cat_ids'] = $post_cat_data;
        
        $slug_query = $this->db->get_where('post_slugs', array('id' => $slug_id));
        $slud_data = $slug_query->row_array();
        $this->data['slug'] = $slud_data['name'];
        
        $this->data['users_arr'] = $this->common_functions->user_list_arr();
        $this->data['data'] = $this->post_model->get_posts_by_id($id);

        $this->form_validation->set_rules('title', 'title', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            if(validation_errors())
            {
                $this->session->set_flashdata('failure', 'Unable to add Post!');
                $this->template->load('layout', 'contents' , 'back/posts/' . DIRECTORY_SEPARATOR . 'edit_post', $this->data);
            }
            else
            {
                $this->template->load('layout', 'contents' , 'back/posts/' . DIRECTORY_SEPARATOR . 'edit_post', $this->data);
            }
        }
        else
        {
            $this->post_model->add_posts($id);
            $this->session->set_flashdata('success', 'Post updated successfully!');
			redirect("admin/post", 'refresh');
        }
    }
    
    public function delete($id)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        if (empty($id))
        {
            show_404();
        }

        $result = $this->post_model->delete_posts($id);
        
        if($result)
        {
            $this->session->set_flashdata('success', 'Post deleted successfully!');
        }
        else
        {
           $this->session->set_flashdata('failure', 'The record cannot be deleted, as it has associated data!');
        }
        
        redirect("admin/post", 'refresh');
    }
}