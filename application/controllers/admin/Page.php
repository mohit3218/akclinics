<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->library(array('ion_auth', 'form_validation', 'pagination'));
		$this->load->helper(array('url', 'language'));
        $this->load->model('page_model');
        $this->load->model('slug_model');
        $this->load->model('common_functions');
    }

    public function index($offset = 0)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			//$this->data['pages'] = $this->page_model->get_pages();
			$this->data['slug_list'] = $this->slug_model->get_slugs_list();
            
            $search['search_status'] = FALSE;
            $where = '';
            $serach_string = '';
            if ($this->input->get('search'))
            {
                $serach_string = $this->input->server('QUERY_STRING');
                $config['enable_query_strings'] = TRUE;
                $config['suffix'] = '?' . $serach_string;
                $search_data = $this->input->get();
                $where = $this->page_model->pages_view_search_coundtiotn($search_data);
                if ($where != NULL)
                {
                    $where = $where;
                }
                else
                {
                    $where = '';
                }
                $this->data['search'] = TRUE;
                $this->data['search_data'] = $search_data;
            }
            $num_rows = $this->page_model->count_pages($where);
            $config['per_page'] = 1000;
            // $config['num_links'] = $num_rows;
            $this->pagination->initialize($config);
            $this->data['pages'] = $this->page_model->get_pages($config['per_page'], $offset, $search, $where);
            $this->data['totalRecords'] = $num_rows;
            $this->data['slugs_arr'] = $this->slug_model->get_slugs();
            
            $this->template->load('layout', 'contents' , 'back/pages/' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
    }


    public function create()
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        $this->load->helper('form');
        $this->load->library('form_validation');
 
        $this->data['title'] = 'Create a page';
 
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('slug_id', 'Slug ID', 'required');
        $this->form_validation->set_rules('meta_title', 'Meta Title', 'required');
        $this->form_validation->set_rules('meta_description', 'Meta Description', 'required');
 
        $this->data['name'] = array(
				'name' => 'name',
				'id' => 'name',
                'class' => 'form-control m-input',
                'placeholder' => 'Enter Page name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('name'),
			);
        
        $this->load->model('slug_model');
        
        $this->data['slugs_arr'] = $this->slug_model->get_slugs();
        
        $this->data['meta_title'] = array(
				'name' => 'meta_title',
				'id' => 'meta_title',
                'class' => 'form-control m-input',
				'type' => 'text',
				'value' => $this->form_validation->set_value('meta_title'),
			);
        
        $this->data['meta_keywords'] = array(
				'name' => 'meta_keywords',
				'id' => 'meta_keywords',
                'class' => 'form-control m-input',
                'placeholder' => 'Enter Meta Keywords',
				'type' => 'text',
				'value' => $this->form_validation->set_value('meta_keywords'),
			);
        
        $this->data['og_title'] = array(
				'name' => 'og_title',
				'id' => 'og_title',
                'class' => 'form-control m-input',
                'placeholder' => 'Enter OG Title',
				'type' => 'text',
				'value' => $this->form_validation->set_value('og_title'),
			);
        
        $this->data['og_description'] = array(
				'name' => 'og_description',
				'id' => 'og_description',
                'class' => 'form-control m-input',
                'placeholder' => 'Enter OG Description',
				'type' => 'text',
				'value' => $this->form_validation->set_value('og_description'),
			);
        
        $this->data['og_image'] = array(
				'name' => 'og_image',
				'id' => 'og_image',
                'class' => 'form-control m-input',
                'placeholder' => 'Enter OG image',
				'type' => 'text',
				'value' => $this->form_validation->set_value('og_image'),
			);
        
        if ($this->form_validation->run() === FALSE)
        {
            if(validation_errors())
            {
                $this->session->set_flashdata('failure', 'Unable to add page!');
                $this->template->load('layout', 'contents' , 'back/pages/' . DIRECTORY_SEPARATOR . 'create_page', $this->data);
            }
            else
            {
                $this->template->load('layout', 'contents' , 'back/pages/' . DIRECTORY_SEPARATOR . 'create_page', $this->data);
            }
        }
        else
        {
            $this->page_model->add_page();
            
            $this->session->set_flashdata('success', 'Page added successfully!');
			redirect("admin/page", 'refresh');
        }
    }

    public function edit($id)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->data['title'] = 'Edit a news item';
        $this->data['data'] = $this->page_model->get_page_by_id($id);
        $this->data['slugs_arr'] = $this->slug_model->get_slugs();

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('slug_id', 'Slug ID', 'required');
        $this->form_validation->set_rules('meta_title', 'Meta Title', 'required');
        $this->form_validation->set_rules('meta_description', 'Meta Description', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->load('layout', 'contents' , 'back/pages/' . DIRECTORY_SEPARATOR . 'edit_page', $this->data);
        }
        else
        {
            $this->page_model->add_page($id);
            $this->session->set_flashdata('success', 'Page updated successfully!');
			redirect("admin/page", 'refresh');
        }
    }

    public function delete($id)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        if (empty($id))
        {
            show_404();
        }

        $this->session->set_flashdata('success', 'Page deleted successfully!');
        
        $this->page_model->delete_page($id);
        redirect("admin/page", 'refresh');
    }

}
