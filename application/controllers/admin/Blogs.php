<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->library(array('ion_auth', 'form_validation','pagination','ckeditor','ckfinder'));
		$this->load->helper(array('url', 'language'));
        $this->load->model('blog_model');
        $this->load->model('category_model');
        $this->load->model('common_functions');
    }
    
    public function index($offset = 0)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			//$this->data['slugs'] = $this->slug_model->get_slugs();
            $search['search_status'] = FALSE;
            $where = '';
            $serach_string = '';
            if ($this->input->get('search'))
            {
                $serach_string = $this->input->server('QUERY_STRING');
                $config['enable_query_strings'] = TRUE;
                $config['suffix'] = '?' . $serach_string;
                $search_data = $this->input->get();
                $where = $this->blog_model->blogs_view_search_coundtiotn($search_data);
                if ($where != NULL)
                {
                    $where = $where;
                }
                else
                {
                    $where = '';
                }
                $this->data['search'] = TRUE;
                $this->data['search_data'] = $search_data;
            }
            $num_rows = $this->blog_model->count_blogs($where);
            $config['per_page'] = 1000;
            $config['num_links'] = $num_rows;
            $this->pagination->initialize($config);
            $this->data['category_list'] = $this->category_model->get_categories_list();
            $this->data['category_arr'] = $this->category_model->get_categories_arr();
            $this->data['blogs'] = $this->blog_model->get_blogs_data($config['per_page'], $offset, $search, $where);
            $this->data['totalRecords'] = $num_rows;
            
            $this->template->load('layout', 'contents' , 'back/blogs/' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
    }
    
    public function create()
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        $this->load->helper('form');
        $this->load->library('form_validation');
 
        $this->data['title'] = 'Create a Blog';
        $this->data['category_arr'] = $this->category_model->get_categories_arr();
 
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('categories_id', 'Categories ID', 'required');
        $this->form_validation->set_rules('content', 'Content', 'required');
        $this->form_validation->set_rules('blog_url', 'Blog URL', 'required');
 
        if ($this->form_validation->run() === FALSE)
        {
             //$this->session->set_flashdata('failure', 'Unable to add slug!');
            if(validation_errors())
            {
                $this->session->set_flashdata('failure', 'Unable to add Blog!');
                $this->template->load('layout', 'contents' , 'back/blogs/' . DIRECTORY_SEPARATOR . 'create_blog', $this->data);
            }
            else
            {
                $this->template->load('layout', 'contents' , 'back/blogs/' . DIRECTORY_SEPARATOR . 'create_blog', $this->data);
            }
        }
        else
        {
            $this->blog_model->add_blogs();
            
            $this->session->set_flashdata('success', 'Blog added successfully!');
			redirect("admin/blogs", 'refresh');
        }
    }
    
    public function edit($id)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->data['title'] = 'Edit a Blog';
        $this->data['category_arr'] = $this->category_model->get_categories_arr();
        $this->data['data'] = $this->blog_model->get_blogs_by_id($id);

        $this->form_validation->set_rules('title', 'title', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            if(validation_errors())
            {
                $this->session->set_flashdata('failure', 'Unable to add Blog!');
                $this->template->load('layout', 'contents' , 'back/blogs/' . DIRECTORY_SEPARATOR . 'edit_blog', $this->data);
            }
            else
            {
                $this->template->load('layout', 'contents' , 'back/blogs/' . DIRECTORY_SEPARATOR . 'edit_blog', $this->data);
            }
        }
        else
        {
            $this->blog_model->add_blogs($id);
            $this->session->set_flashdata('success', 'Blog updated successfully!');
			redirect("admin/blogs", 'refresh');
        }
    }
    
    public function delete($id)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        if (empty($id))
        {
            show_404();
        }

        $result = $this->blog_model->delete_blogs($id);
        
        if($result)
        {
            $this->session->set_flashdata('success', 'Blog deleted successfully!');
        }
        else
        {
           $this->session->set_flashdata('failure', 'The record cannot be deleted, as it has associated data!');
        }
        
        redirect("admin/blogs", 'refresh');
    }
}

