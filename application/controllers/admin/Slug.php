<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Slug extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->library(array('ion_auth', 'form_validation','pagination'));
		$this->load->helper(array('url', 'language'));
        $this->load->model('slug_model');
        $this->load->model('common_functions');
    }

    public function index($offset = 0)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			//$this->data['slugs'] = $this->slug_model->get_slugs();
            $search['search_status'] = FALSE;
            $where = '';
            $serach_string = '';
            if ($this->input->get('search'))
            {
                $serach_string = $this->input->server('QUERY_STRING');
                $config['enable_query_strings'] = TRUE;
                $config['suffix'] = '?' . $serach_string;
                $search_data = $this->input->get();
                $where = $this->slug_model->slugs_view_search_coundtiotn($search_data);
                if ($where != NULL)
                {
                    $where = $where;
                }
                else
                {
                    $where = '';
                }
                $this->data['search'] = TRUE;
                $this->data['search_data'] = $search_data;
            }
            $num_rows = $this->slug_model->count_slugs($where);
            $config['per_page'] = 1000;
            $config['num_links'] = $num_rows;
            $this->pagination->initialize($config);
            $this->data['slugs'] = $this->slug_model->get_slugs_data($config['per_page'], $offset, $search, $where);
            $this->data['totalRecords'] = $num_rows;
            
            $this->template->load('layout', 'contents' , 'back/slugs/' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
    }


    public function create()
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        $this->load->helper('form');
        $this->load->library('form_validation');
 
        $this->data['title'] = 'Create a slug';
 
        $this->form_validation->set_rules('name', 'Name', 'required');
 
        $this->data['name'] = array(
				'name' => 'name',
				'id' => 'name',
                'class' => 'form-control m-input',
                'placeholder' => 'Enter Slug name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('name'),
			);
        
        if ($this->form_validation->run() === FALSE)
        {
             //$this->session->set_flashdata('failure', 'Unable to add slug!');
            if(validation_errors())
            {
                $this->session->set_flashdata('failure', 'Unable to add slug!');
                $this->template->load('layout', 'contents' , 'back/slugs/' . DIRECTORY_SEPARATOR . 'create_slug', $this->data);
            }
            else
            {
                $this->template->load('layout', 'contents' , 'back/slugs/' . DIRECTORY_SEPARATOR . 'create_slug', $this->data);
            }
        }
        else
        {
            $this->slug_model->add_slug();
            
            $this->session->set_flashdata('success', 'Slug added successfully!');
			redirect("admin/slug", 'refresh');
        }
    }

    public function edit($id)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->data['title'] = 'Edit a news item';
        $this->data['data'] = $this->slug_model->get_slug_by_id($id);

        $this->form_validation->set_rules('name', 'Name', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->load('layout', 'contents' , 'back/slugs/' . DIRECTORY_SEPARATOR . 'edit_slug', $this->data);
        }
        else
        {
            $this->slug_model->add_slug($id);
            $this->session->set_flashdata('success', 'Slug updated successfully!');
			redirect("admin/slug", 'refresh');
        }
    }

    public function delete($id)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        if (empty($id))
        {
            show_404();
        }

        $result = $this->slug_model->delete_slug($id);
        
        if($result)
        {
            $this->session->set_flashdata('success', 'Slug deleted successfully!');
        }
        else
        {
           $this->session->set_flashdata('failure', 'The record cannot be deleted, as it has associated data!');
        }
        
        redirect("admin/slug", 'refresh');
    }

}
