<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->library(array('ion_auth', 'form_validation','pagination'));
		$this->load->helper(array('url', 'language'));
        $this->load->model('location_model');
    }

    public function index($offset = 0)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('admin/auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			//$this->data['slugs'] = $this->slug_model->get_slugs();
            $search['search_status'] = FALSE;
            $where = '';
            $serach_string = '';
            if ($this->input->get('search'))
            {
                $serach_string = $this->input->server('QUERY_STRING');
                $config['enable_query_strings'] = TRUE;
                $config['suffix'] = '?' . $serach_string;
                $search_data = $this->input->get();
                $where = $this->location_model->locations_view_search_coundtiotn($search_data);
                if ($where != NULL)
                {
                    $where = $where;
                }
                else
                {
                    $where = '';
                }
                $this->data['search'] = TRUE;
                $this->data['search_data'] = $search_data;
            }
            $num_rows = $this->location_model->count_locations($where);
            $config['per_page'] = 100;
            $config['num_links'] = $num_rows;
            $this->pagination->initialize($config);
            $this->data['locations'] = $this->location_model->get_locations_data($config['per_page'], $offset, $search, $where);
            $this->data['totalRecords'] = $num_rows;
            
            $this->template->load('layout', 'contents' , 'back/locations/' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
    }
    
    public function create()
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        $this->load->helper('form');
        $this->load->library('form_validation');
 
        $this->data['title'] = 'Add a Location';
 
        $getUserlocation = json_decode(file_get_contents("https://ipinfo.io/"));

        $coordinates = explode(",", $getUserlocation->loc);
        $this->data['latitude'] = $coordinates[0]; // latitude
        $this->data['longitude'] = $coordinates[1]; // longitude
 
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
 
        if ($this->form_validation->run() === FALSE)
        {
            if(validation_errors())
            {
                $this->session->set_flashdata('failure', 'Unable to add Location!');
                $this->template->load('layout', 'contents' , 'back/locations/' . DIRECTORY_SEPARATOR . 'create_location', $this->data);
            }
            else
            {
                $this->template->load('layout', 'contents' , 'back/locations/' . DIRECTORY_SEPARATOR . 'create_location', $this->data);
            }
        }
        else
        {
            $this->location_model->add_locations();
            
            $this->session->set_flashdata('success', 'Location added successfully!');
			redirect("admin/location", 'refresh');
        }
    }
    
    public function edit($id)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->data['title'] = 'Edit a Location';
        $this->data['data'] = $this->location_model->get_locations_by_id($id);

        $this->form_validation->set_rules('name', 'Name', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->load('layout', 'contents' , 'back/locations/' . DIRECTORY_SEPARATOR . 'edit_location', $this->data);
        }
        else
        {
            $this->location_model->add_locations($id);
            $this->session->set_flashdata('success', 'Location updated successfully!');
			redirect("admin/location", 'refresh');
        }
    }
    
    public function delete($id)
    {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('admin/auth', 'refresh');
		}
        
        if (empty($id))
        {
            show_404();
        }

        $result = $this->location_model->delete_locations($id);
        
        if($result)
        {
            $this->session->set_flashdata('success', 'Location deleted successfully!');
        }
        else
        {
           $this->session->set_flashdata('failure', 'The record cannot be deleted, as it has associated data!');
        }
        
        redirect("admin/location", 'refresh');
    }
}