<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
    public function __construct()
	{
		parent::__construct();
        $this->load->model('home_model');
        $this->load->model('page_model');
        $this->load->model('category_model');
        $this->load->model('post_model');
        $this->load->model('ratings_model');
		$this->load->helper(array('url', 'language'));
        $this->load->library('amp/Amp_lib', ['css_prefix' => 'amp-inline'], 'amp_lib');
	}
	
	
	public function cache_page_time()
    {
        //$this->output->cache(60);
    }
    
    function paging_meta_data($meta_data)
    {
        $this->data['meta']['title'] = $meta_data['meta_title'];
        $this->data['meta']['desc'] = $meta_data['meta_description'];
        
        if(!empty($meta_data['keywords']) != '') 
        {
            $this->data['meta']['keywords'] = $meta_data['keywords'];
        }

        if(!empty($meta_data['no_index']) != '') 
        {
            $this->data['meta']['no_index'] = $meta_data['no_index'];
        }
        if(!empty($meta_data['no_follow']) != '') 
        {
            $this->data['meta']['no_follow'] = $meta_data['no_follow'];
        }
        if(!empty($meta_data['no_archive']) != '') 
        {
            $this->data['meta']['no_archive'] = $meta_data['no_archive'];
        }
        
        if(!empty($meta_data['og_title']) != '') 
        {
            $this->data['meta']['og_title'] = $meta_data['og_title'];
        }
        if(!empty($meta_data['og_description']) != '') 
        {
            $this->data['meta']['og_desc'] = $meta_data['og_description'];
        }
        if(!empty($meta_data['og_image']) != '') 
        {
            $this->data['meta']['og_image'] = $meta_data['og_image'];
        }
        if(!empty($meta_data['canonical_url']) != '') 
        {
            $this->data['meta']['canonical_url'] = $meta_data['canonical_url'];
        }
        
        if(!empty($meta_data['meta_url']) != '') 
        {
            $this->data['meta']['og_url'] = $meta_data['meta_url'];
        }
        
        return $this->data;
    }
    
    function blogs_slider_view($blogs)
    {
        if(!empty($blogs['all_blogs']))
        {
            foreach ($blogs['all_blogs'] as $k => $blog)
            {
                $this->data['all_blog'][$k]['id']    = $blog['id'];
                $this->data['all_blog'][$k]['interlink_title']    = $blog['interlink_title'];
                $this->data['all_blog'][$k]['interlink_content']    = $blog['interlink_content'];
                $this->data['all_blog'][$k]['interlink_image']    = $blog['interlink_image'];
                $this->data['all_blog'][$k]['title']    = $blog['title'];
                $this->data['all_blog'][$k]['categories_id']    = $blog['categories_id'];
                $this->data['all_blog'][$k]['post_url'] = $blog['post_url'];
                $this->data['all_blog'][$k]['post_date'] = date('M,d,Y',strtotime($blog['post_date']));
                $content   = htmlentities(trim(strip_tags($blog['content'])));
                $content   = html_entity_decode($content);
                $this->data['all_blog'][$k]['content'] = strlen($content) > 30 ? substr($content,0,150)."..." : substr($content, 0 , 150);
                $this->data['all_blog'][$k]['image']     = $blog['image'];
                $this->data['all_blog'][$k]['image_alt']     = $blog['image_alt'];
                $this->data['all_blog'][$k]['is_cron']     = $blog['is_cron'];
            }
        }
        
        if(!empty($blogs['patient_blogs']))
        {
            foreach ($blogs['patient_blogs'] as $k => $blog)
            {
                $this->data['patient_blog'][$k]['interlink_title']    = $blog['interlink_title'];
                $this->data['patient_blog'][$k]['interlink_content']    = $blog['interlink_content'];
                $this->data['patient_blog'][$k]['interlink_image']    = $blog['interlink_image'];
                $this->data['patient_blog'][$k]['title']    = $blog['title'];
                $this->data['patient_blog'][$k]['post_url'] = $blog['post_url'];
                $this->data['patient_blog'][$k]['categories_id'] = $blog['categories_id'];
                $this->data['patient_blog'][$k]['post_date'] = date('M,d,Y',strtotime($blog['post_date']));
                $content   = htmlentities(trim(strip_tags($blog['content'])));
                $content   = html_entity_decode($content);
                $this->data['patient_blog'][$k]['content'] = strlen($content) > 30 ? substr($content,0,150)."..." : substr($content, 0 , 150);
                $this->data['patient_blog'][$k]['image']     = $blog['image'];
				$this->data['patient_blog'][$k]['image_alt']     = $blog['image_alt'];
                $this->data['patient_blog'][$k]['is_cron']     = $blog['is_cron'];
            }
        }
        
        if(!empty($blogs['success_stories_blogs']))
        {
            foreach ($blogs['success_stories_blogs'] as $k => $blog)
            {
                $this->data['success_stories_blog'][$k]['interlink_title']    = $blog['interlink_title'];
                $this->data['success_stories_blog'][$k]['interlink_content']    = $blog['interlink_content'];
                $this->data['success_stories_blog'][$k]['interlink_image']    = $blog['interlink_image'];
                $this->data['success_stories_blog'][$k]['title']    = $blog['title'];
                $this->data['success_stories_blog'][$k]['post_url'] = $blog['post_url'];
                $this->data['success_stories_blog'][$k]['categories_id'] = $blog['categories_id'];
                $this->data['success_stories_blog'][$k]['post_date'] = date('M,d,Y',strtotime($blog['post_date']));
                $content   = htmlentities(trim(strip_tags($blog['content'])));
                $content   = html_entity_decode($content);
                $this->data['success_stories_blog'][$k]['content'] = strlen($content) > 30 ? substr($content,0,150)."..." : substr($content, 0 , 150);
                $this->data['success_stories_blog'][$k]['image']     = $blog['image'];
				$this->data['success_stories_blog'][$k]['image_alt']     = $blog['image_alt'];
                $this->data['success_stories_blog'][$k]['is_cron']     = $blog['is_cron'];
            }
        }
        
        return $this->data;
    }
    
    function hair_transplant_blogs_slider_view($blogs)
    {
        if(!empty($blogs['hair_loss_transplant']))
        {
            foreach ($blogs['hair_loss_transplant'] as $k => $blog)
            {
                $this->data['hair_loss_transplant'][$k]['id']    = $blog['id'];
                $this->data['hair_loss_transplant'][$k]['interlink_title']    = $blog['interlink_title'];
                $this->data['hair_loss_transplant'][$k]['interlink_content']    = $blog['interlink_content'];
                $this->data['hair_loss_transplant'][$k]['interlink_image']    = $blog['interlink_image'];
                $this->data['hair_loss_transplant'][$k]['title']    = $blog['title'];
                $this->data['hair_loss_transplant'][$k]['categories_id']    = $blog['categories_id'];
                $this->data['hair_loss_transplant'][$k]['post_url'] = $blog['post_url'];
                //$this->data['all_blog'][$k]['post_date'] = date('M,d,Y',strtotime($blog['post_date']));
                $content   = htmlentities(trim(strip_tags($blog['content'])));
                $content   = html_entity_decode($content);
                $this->data['hair_loss_transplant'][$k]['content'] = strlen($content) > 30 ? substr($content,0,150)."..." : substr($content, 0 , 150);
                $this->data['hair_loss_transplant'][$k]['image']     = $blog['image'];
				$this->data['hair_loss_transplant'][$k]['image_alt']     = $blog['image_alt'];
                $this->data['hair_loss_transplant'][$k]['is_cron']     = $blog['is_cron'];
            }
        }
        return $this->data;
    }
    
    function hair_trasplant_loss_and_restoration_blogs()
    {
        $hair_loss_transplant_conditions = array('post.is_active' => 1, 'post.is_deleted' => 0, 'post.post_status' => 2);
        
        $cat_ids = array('pc.cat_id' => CI_HAIR_LOSS_TREATMENT_ARTICLES,CI_HAIR_LOSS_ARTICLES, CI_HAIR_TRANSPLANT_ARTICLES);
        
        $limit = 6;
        
        $hair_loss_transplant = $this->home_model->get_post_blogs($hair_loss_transplant_conditions, $limit, $cat_ids);
        
        //Get patient blogs
        $blogs = array(
            'hair_loss_transplant' => $hair_loss_transplant
        );
        
        $this->data['data'] = $this->hair_transplant_blogs_slider_view($blogs);
        
        return $this->data;
    }
    
    function cosmetic_surgery_blogs_slider_view($blogs)
    {
        if(!empty($blogs['cosmetic_surgery']))
        {
            foreach ($blogs['cosmetic_surgery'] as $k => $blog)
            {
                $this->data['cosmetic_surgery'][$k]['id']    = $blog['id'];
                $this->data['cosmetic_surgery'][$k]['interlink_title']    = $blog['interlink_title'];
                $this->data['cosmetic_surgery'][$k]['interlink_content']    = $blog['interlink_content'];
                $this->data['cosmetic_surgery'][$k]['interlink_image']    = $blog['interlink_image'];
                $this->data['cosmetic_surgery'][$k]['title']    = $blog['title'];
                $this->data['cosmetic_surgery'][$k]['categories_id']    = $blog['categories_id'];
                $this->data['cosmetic_surgery'][$k]['post_url'] = $blog['post_url'];
                //$this->data['cosmetic_surgery'][$k]['post_date'] = date('M,d,Y',strtotime($blog['post_date']));
                $content   = htmlentities(trim(strip_tags($blog['content'])));
                $content   = html_entity_decode($content);
                $this->data['cosmetic_surgery'][$k]['content'] = strlen($content) > 30 ? substr($content,0,150)."..." : substr($content, 0 , 150);
                $this->data['cosmetic_surgery'][$k]['image']     = $blog['image'];
				$this->data['cosmetic_surgery'][$k]['image_alt']     = $blog['image_alt'];
                $this->data['cosmetic_surgery'][$k]['is_cron']     = $blog['is_cron'];
            }
        }
        
        return $this->data;
    }
    
    function cosmetic_surgery_blogs()
    {
        $cosmetic_surgery_conditions = array('post.is_active' => 1, 'post.is_deleted' => 0, 'pc.cat_id' => CI_COSMETIC_SURGERY_ARTICLES, 'post.post_status' => 2);
        
        $limit = 6;
        
        $cosmetic_surgery = $this->home_model->get_post_blogs($cosmetic_surgery_conditions, $limit);
        
        //Get patient blogs
        $blogs = array('cosmetic_surgery' => $cosmetic_surgery);
        
        $this->data['data'] = $this->cosmetic_surgery_blogs_slider_view($blogs);
        
        return $this->data;
    }

    function cosmetology_blogs_slider_view($blogs)
    {
        if(!empty($blogs['cosmetology']))
        {
            foreach ($blogs['cosmetology'] as $k => $blog)
            {
                $this->data['cosmetology'][$k]['id']    = $blog['id'];
                $this->data['cosmetology'][$k]['interlink_title']    = $blog['interlink_title'];
                $this->data['cosmetology'][$k]['interlink_content']    = $blog['interlink_content'];
                $this->data['cosmetology'][$k]['interlink_image']    = $blog['interlink_image'];
                $this->data['cosmetology'][$k]['title']    = $blog['title'];
                $this->data['cosmetology'][$k]['categories_id']    = $blog['categories_id'];
                $this->data['cosmetology'][$k]['post_url'] = $blog['post_url'];
                //$this->data['cosmetology'][$k]['post_date'] = date('M,d,Y',strtotime($blog['post_date']));
                $content   = htmlentities(trim(strip_tags($blog['content'])));
                $content   = html_entity_decode($content);
                $this->data['cosmetology'][$k]['content'] = strlen($content) > 30 ? substr($content,0,150)."..." : substr($content, 0 , 150);
                $this->data['cosmetology'][$k]['image']     = $blog['image'];
				$this->data['cosmetology'][$k]['image_alt']     = $blog['image_alt'];
                $this->data['cosmetology'][$k]['is_cron']     = $blog['is_cron'];
            }
        }
        
        return $this->data;
    }
    
    function cosmetology_blogs()
    {
        $cosmetology_conditions = array('post.is_active' => 1, 'post.is_deleted' => 0, 'pc.cat_id' => CI_COSMETOLOGY, 'post.post_status' => 2);
        
        $limit = 6;
        
        $cosmetology = $this->home_model->get_post_blogs($cosmetology_conditions, $limit);
        
        //Get patient blogs
        $blogs = array('cosmetology' => $cosmetology);
        
        $this->data['data'] = $this->cosmetology_blogs_slider_view($blogs);
        
        return $this->data;
    }
    
    function dr_blogs_slider_view($blogs)
    {
        if(!empty($blogs['dr_blogs']))
        {
            foreach ($blogs['dr_blogs'] as $k => $blog)
            {
                $this->data['dr_blogs'][$k]['id']    = $blog['id'];
                $this->data['dr_blogs'][$k]['interlink_title']    = $blog['interlink_title'];
                $this->data['dr_blogs'][$k]['interlink_content']    = $blog['interlink_content'];
                $this->data['dr_blogs'][$k]['interlink_image']    = $blog['interlink_image'];
                $this->data['dr_blogs'][$k]['title']    = $blog['title'];
                $this->data['dr_blogs'][$k]['categories_id']    = $blog['categories_id'];
                $this->data['dr_blogs'][$k]['post_url'] = $blog['post_url'];
                //$this->data['cosmetology'][$k]['dr_blogs'] = date('M,d,Y',strtotime($blog['post_date']));
                $content   = htmlentities(trim(strip_tags($blog['content'])));
                $content   = html_entity_decode($content);
                $this->data['dr_blogs'][$k]['content'] = strlen($content) > 30 ? substr($content,0,150)."..." : substr($content, 0 , 150);
                $this->data['dr_blogs'][$k]['image']     = $blog['image'];
				$this->data['dr_blogs'][$k]['image_alt']     = $blog['image_alt'];
                $this->data['dr_blogs'][$k]['is_cron']     = $blog['is_cron'];
            }
        }
        
        return $this->data;
    }
    
    function dr_blogs($dr_auther_id)
    {
        $cosmetology_conditions = array('post.is_active' => 1, 'post.is_deleted' => 0, 'post.post_author' => $dr_auther_id, 'post.post_status' => 2);
        
        $limit = 6;
        
        $dr_blogs = $this->home_model->get_post_blogs($cosmetology_conditions, $limit);
        
        //Get patient blogs
        $blogs = array('dr_blogs' => $dr_blogs);
        
        $this->data['data'] = $this->dr_blogs_slider_view($blogs);
        
        return $this->data;
    }
    
    public function index()
	{
		$server_req = $_SERVER['REQUEST_URI'];
		
		if($server_req != '/')
		{
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . '/'); 
            exit();
		}
		
        $this->cache_page_time();
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HOME_PAGE);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        //Set Meta data page
        $this->paging_meta_data($meta_data);
        
        //$this->load->model('category_model');
        //$category_list = $this->category_model->get_categories_list();
        
        //Get all blogs
        $all_blogs_conditions = array('post.is_active' => 1, 'post.is_deleted' => 0, 'post.post_status' => 2);
        $limit = 3;
        $all_blogs = $this->home_model->get_post_blogs($all_blogs_conditions, $limit);
        
        //Get patient blogs
        $patient_blog_conditions = array('post.is_active' => 1, 'post.is_deleted' => 0, 'pc.cat_id' => CI_PATIRNTS_ASK, 'post.post_status' => 2);
        $patient_blogs = $this->home_model->get_post_blogs($patient_blog_conditions, $limit);
        
        //Get Success stories blogs
        $success_stories_conditions = array('post.is_active' => 1, 'post.is_deleted' => 0, 'pc.cat_id' => CI_SUCCESS_STORIES, 'post.post_status' => 2);
        $success_stories_blogs = $this->home_model->get_post_blogs($success_stories_conditions, $limit);
        
        
        $blogs = array(
            'all_blogs' => $all_blogs,
            'patient_blogs' => $patient_blogs,
            'success_stories_blogs' => $success_stories_blogs,
        );
        
        $this->data['data'] = $this->blogs_slider_view($blogs);
        
        $this->front_template->load('layout', 'contents' , 'front/home/' . DIRECTORY_SEPARATOR . 'index', $this->data);
	}
    
    //Hair Transplant Functions
    public function hair_transplant()
    {
		$this->cache_page_time();
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_TRANSPLANT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'index', $this->data);
    }
    
    public function hair_transplant_in_men()
    {
		$this->cache_page_time();
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_TRANSPLANT_IN_MEN);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'hair_transplant_in_men', $this->data);
    }
    
    public function hair_transplant_in_women()
    {
		$this->cache_page_time();
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_TRANSPLANT_IN_WOMEN);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'hair_transplant_in_women', $this->data);
    }
    
    public function hair_transplant_cost()
    {
		$this->cache_page_time();
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_TRANSPLANT_COST);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'hair_transplant_cost', $this->data);
    }
    
    public function fue_hair_transplant()
    {
		$this->cache_page_time();
         //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(FUE_HAIR_TRANSPLANT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'fue_hair_transplant', $this->data);
    }
    
    public function fut_strip_hair_transplant()
    {
		$this->cache_page_time();
         //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(FUT_HAIR_TRANSPLANT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
    
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'fut_strip_hair_transplant', $this->data);
    }
    
    public function hair_transplant_techniques()
    {
		$this->cache_page_time();
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_TRANSPLANT_TECHNIQUES);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'hair_transplant_techniques', $this->data);
    }
    
    public function facial_hair_transplant()
    {
		$this->cache_page_time();
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(FACIAL_HAIR_TRANSPLANT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'facial_hair_transplant', $this->data);
    }
    
    public function beard_transplant()
    {
		$this->cache_page_time();
         //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(BEARD_TRANSPLANT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'beard_transplant', $this->data);
    }
    
    public function bio_fue()
    {
		$this->cache_page_time();
         //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(BIO_FUE);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'bio_fue', $this->data);
    }
    
    public function body_hair_transplant()
    {
		$this->cache_page_time();
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(BODY_HAIR_TRANSPLANT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'body_hair_transplant', $this->data);
    }
    
    public function revision_hair_transplant()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(REVISION_HAIR_TRANSPLANT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'revision_hair_transplant', $this->data);
    }
    
    public function post_operative_care()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(POSTOPERATIVE_CARE);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'post_operative_care', $this->data);
    }
    
    //About Us Functions
    public function about_us()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(ABOUT_US);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'index', $this->data);
    }
    
    public function hair_transplant_training()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_TRANSPLANT_TRAINING);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'hair_transplant_training', $this->data);
    }
    
    //Hair Loss Functions
    public function hair_loss()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_LOSS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_loss/' . DIRECTORY_SEPARATOR . 'index', $this->data);
    }
    
    //Hair Restoration Functions
    public function hair_restoration()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_RESTORATION);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_restoration/' . DIRECTORY_SEPARATOR . 'index', $this->data);
    }
    
    public function hair_gain_therapy()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_GAIN_THERAPY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_restoration/' . DIRECTORY_SEPARATOR . 'hair_gain_therapy', $this->data);
    }
    
    public function prp_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(PRP_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_restoration/' . DIRECTORY_SEPARATOR . 'prp_treatment', $this->data);
    }
    
    public function artificial_hair_restoration()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(ARTIFICIAL_HAIR_RESTORATION);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_restoration/' . DIRECTORY_SEPARATOR . 'artificial_hair_restoration', $this->data);
    }
    
    public function scalp_micro_pigmentation_india()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SCALP_MICRO_PIGMENTATION_INDIA);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_restoration/' . DIRECTORY_SEPARATOR . 'scalp_micro_pigmentation_india', $this->data);
    }
    
    public function mesotherapy()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(MESOTHERAPY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_restoration/' . DIRECTORY_SEPARATOR . 'mesotherapy', $this->data);
    }
    
    //Cosmetic Surgery Functions
    public function cosmetic_surgery()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(COSMETIC_SURGERY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetic_surgery_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetic_surgery/' . DIRECTORY_SEPARATOR . 'index', $this->data);
    }
    
    public function rhinoplasty()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(RHINOPLASTY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetic_surgery_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetic_surgery/' . DIRECTORY_SEPARATOR . 'rhinoplasty', $this->data);
    }
    public function blepharoplasty()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(BLEPHAROPLASTY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetic_surgery_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetic_surgery/' . DIRECTORY_SEPARATOR . 'blepharoplasty', $this->data);
    }
    public function gynecomastia()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(GYNECOMASTIA);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetic_surgery_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetic_surgery/' . DIRECTORY_SEPARATOR . 'gynecomastia', $this->data);
    }
    public function vitiligo_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(VITILIGO_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetic_surgery_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetic_surgery/' . DIRECTORY_SEPARATOR . 'vitiligo_treatment', $this->data);
    }
    public function abdominoplasty()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(ABDOMINOPLASTY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetic_surgery_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetic_surgery/' . DIRECTORY_SEPARATOR . 'abdominoplasty', $this->data);
    }
    public function liposuction()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(LIPOSUCTION);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetic_surgery_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetic_surgery/' . DIRECTORY_SEPARATOR . 'liposuction', $this->data);
    }
    public function breast_augmentation()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(BREAST_AUGMENTATION);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetic_surgery_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetic_surgery/' . DIRECTORY_SEPARATOR . 'breast_augmentation', $this->data);
    }
    public function breast_reduction()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(BREAST_REDUCTION);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetic_surgery_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetic_surgery/' . DIRECTORY_SEPARATOR . 'breast_reduction', $this->data);
    }
    public function cosmetic_gynaecology()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(COSMETIC_GYNAECOLOGY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetic_surgery_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetic_surgery/' . DIRECTORY_SEPARATOR . 'cosmetic_gynaecology', $this->data);
    }
    public function male_genital_surgery()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(MALE_GENITAL_SURGERY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetic_surgery_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetic_surgery/' . DIRECTORY_SEPARATOR . 'male_genital_surgery', $this->data);
    }
    
    //Cosmetology Functions
    public function cosmetology()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(COSMETOLOGY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'index', $this->data);
    }
    
    public function laser_hair_removal_for_men_and_women()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(LASER_HAIR_REMOVAL_FOR_MEN_AND_WOMEN);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'laser_hair_removal_for_men_and_women', $this->data);
    }
    
    public function non_surgical_ultrasonic_liposuction()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(NON_SURGICAL_ULTRASONIC_LIPOSUCTION);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'non_surgical_ultrasonic_liposuction', $this->data);
    }
    
    public function skin_polishing()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SKIN_POLISHING);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'skin_polishing', $this->data);
    }
    
    public function stretch_marks_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(STRETCH_MARKS_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'stretch_marks_treatment', $this->data);
    }
    
    public function laser_photo_facial()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(LASER_PHOTO_FACIAL);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'laser_photo_facial', $this->data);
    }
    
    public function laser_tattoo_removal()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(LASER_TATTOO_REMOVAL);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'laser_tattoo_removal', $this->data);
    }
    
    public function ultherapy_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(ULTHERAPY_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'ultherapy_treatment', $this->data);
    }
    
    public function chemical_peels()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(CHEMICAL_PEELS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'chemical_peels', $this->data);
    }
    
    public function acne_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(ACNE_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'acne_treatment', $this->data);
    }
    
    public function pigmentation_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(PIGMENTATION_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'pigmentation_treatment', $this->data);
    }
    
    public function laser_vaginal_rejuvenation()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(LASER_VAGINAL_REJUVENATION);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'laser_vaginal_rejuvenation', $this->data);
    }
    
    public function anti_ageing_treatments()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(ANTI_AGEING_TREATMENTS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'anti_ageing_treatments', $this->data);
    }
    
    public function ultracel_skin_tightening_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(ULTRACEL_SKIN_TIGHTENING_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'ultracel_skin_tightening_treatment', $this->data);
    }
    
    //Results Functions
    public function results()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(RESULTS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/results/' . DIRECTORY_SEPARATOR . 'index', $this->data);
    }
    
    public function videos()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(RESULT_VIDEOS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/results/' . DIRECTORY_SEPARATOR . 'videos', $this->data);
    }
    
    public function cosmetic_surgery_results()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(RESULTS_COSMETIC_SURGERY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/results/' . DIRECTORY_SEPARATOR . 'cosmetic_surgery_results', $this->data);
    }
    
    public function prp_therapy()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(RESULTS_PRP_THERAPY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/results/' . DIRECTORY_SEPARATOR . 'prp_therapy', $this->data);
    }
    
    //Blogs Function
    public function blog($cat_slug_data = array())
    {
        ini_set('max_execution_time', 60 * 60);
        
        $current_method = $this->uri->segment(1);
        $page_name = $this->uri->segment(2);

        $url_page_blog_no = $current_method . '/' . $page_name;
        if($url_page_blog_no === 'blog/page')
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url('/blog/')); 
            exit();
        }
        else if($url_page_blog_no === 'blog/author')
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url('/blog/')); 
            exit();
        }
        else if($url_page_blog_no === 'blog/tag')
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url('/blog/')); 
            exit();
        }
        else if($current_method === 'tag')
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url('/blog/')); 
            exit();
        }
        
        $this->data['pageNo'] = 0;
        
        $all_blogs_conditions = array('post.is_active' => 1, 'post.is_deleted' => 0, 'post.post_status' => 2);
        $limit = 12;
        
        //Get a home page meta data
        if(empty($cat_slug_data))
        {
            $meta_data = $this->page_model->get_page_list_id(BLOG);
            $meta_data['meta_url'] = base_url(uri_string()) . '/';
            $this->paging_meta_data($meta_data);
        }
        else
        {
            //Get all blogs
            $cat_id = $cat_slug_data['id'];
            $all_blogs_conditions['pc.cat_id'] = $cat_id;
            $this->data['cat_id'] = $cat_id;
            
            $meta_data = $this->category_model->get_categories_meta_data($cat_id);
            $meta_data['meta_title'] = $meta_data['document_title'];
            $meta_data['meta_url'] = base_url(uri_string()) . '/';
            $this->paging_meta_data($meta_data);
        }
        
        $all_blogs = $this->home_model->get_blogs_data($all_blogs_conditions, $limit, $offset = 0);
        
        $blogs = array('all_blogs' => $all_blogs);
        
        $this->data['data'] = $this->blogs_slider_view($blogs);
        
        $this->front_template->load('layout', 'contents' , 'front/blogs/' . DIRECTORY_SEPARATOR . 'index', $this->data);
    }
    
    function loadMoreData()
    {
        ini_set('max_execution_time', 60 * 60);
        
        //For Cat posts
        $current_method = $this->uri->segment(1);
        $cat_name = $this->uri->segment(2);
        $post_name = $this->uri->segment(3);
        $redirects_page = $current_method . '/' . $cat_name . '/' . $post_name;
        
        if($redirects_page === 'blog/patients-ask/'. $post_name)
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url("/blog/$post_name/")); 
            exit();
        }
        else if($redirects_page === 'blog/success-stories/'. $post_name)
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url("/blog/$post_name/")); 
            exit();
        }
        else if($redirects_page === 'blog/case-study/'. $post_name)
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url("/blog/$post_name/")); 
            exit();
        }
        else if($redirects_page === 'blog/hair-transplant-articles/'. $post_name)
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url("/blog/$post_name/")); 
            exit();
        }
        else if($redirects_page === 'blog/news-and-events/'. $post_name)
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url("/blog/$post_name/")); 
            exit();
        }
        else if($redirects_page === 'blog/hair-loss-articles/'. $post_name)
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url("/blog/$post_name/")); 
            exit();
        }
        else if($redirects_page === 'blog/hair-loss-treatments/'. $post_name)
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url("/blog/$post_name/")); 
            exit();
        }
        else if($redirects_page === 'blog/medical-treatment-articles/'. $post_name)
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url("/blog/$post_name/")); 
            exit();
        }
        else if($redirects_page === 'blog/cosmetic-surgery-articles/'. $post_name)
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url("/blog/$post_name/")); 
            exit();
        }
        else if($redirects_page === 'blog/styleshala/'. $post_name)
        {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location:" . base_url("/blog/$post_name/")); 
            exit();
        }
        
        // Get last post ID
        $this->data['pageNo'] = $lastID = $this->input->post('id');
        $lastPgaeNo = $lastID * 12;
        
        $cat_id = $this->input->post('cat_id');
        
        $all_blogs_conditions = array('post.is_active' => 1, 'post.is_deleted' => 0, 'post.post_status' => 2);
        if($cat_id != 0)
        {
            $all_blogs_conditions['pc.cat_id'] = $cat_id;
             $this->data['cat_id'] = $cat_id;
        }
        
        $limit = 12;
        $all_blogs = $this->home_model->get_blogs_data($all_blogs_conditions, $limit,$lastPgaeNo);
        
        $this->data['total_records'] = count($all_blogs);
        $blogs = array('all_blogs' => $all_blogs);
        // Get post rows num
        $this->data['data'] = $this->blogs_slider_view($blogs);
        // Get posts data from the database
        $this->data['postNum'] = count($all_blogs);
        $this->data['postLimit'] = 10;
        // Pass data to view
        $this->load->view('front/blogs/loadMoreData', $this->data);
    }
    
    //Locations Functions
    public function locations()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(LOCATION);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/locations/' . DIRECTORY_SEPARATOR . 'index', $this->data);
    }
    
    public function hair_transplant_ludhiana()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_TRANSPLANT_LUDHIANA);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/locations/' . DIRECTORY_SEPARATOR . 'hair_transplant_ludhiana', $this->data);
    }
    
    public function hair_transplant_delhi()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_TRANSPLANT_DELHI);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/locations/' . DIRECTORY_SEPARATOR . 'hair_transplant_delhi', $this->data);
    }
    
    public function best_hair_transplant_clinic()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(BEST_HAIR_TRANSPLANT_CLINIC);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/locations/' . DIRECTORY_SEPARATOR . 'best_hair_transplant_clinic', $this->data);
    }
    
    //Contact Us Function
    public function contact_us()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(CONTACT_US);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/contact/' . DIRECTORY_SEPARATOR . 'index', $this->data);
    }
    
    public function blog_posts()
    {
        //Get current url slug name
        $curret_url_slug = $this->uri->segment(2);

        $cat_slug_data = $this->home_model->get_blog_cat_slug_by_name($curret_url_slug);
       
        if(!empty($cat_slug_data))
        {
            $this->blog($cat_slug_data);
        }
        else
        {
            $slug_data = $this->home_model->get_post_slug_id_by_name($curret_url_slug);
			
			if(empty($slug_data))
			{
				show_404();
			}
             //Get Slug ID
            $slug_id = $slug_data['id'];
            //Get Post by current slug ID
            $current_post = $this->home_model->get_post_by_post_slug_id($slug_id);

            $useragent = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) 
            {
                $meta_data = array(
                    'meta_title' => $current_post['document_title'],
                    'meta_description' => $current_post['meta_description'],
                    'meta_keywords' => $current_post['meta_keywords'],
                    'canonical_url' => base_url(str_replace("/amp", '', uri_string())) . '/',
                );
            }
            else
            {
                $meta_data = array(
                    'meta_title' => $current_post['document_title'],
                    'meta_description' => $current_post['meta_description'],
                    'meta_keywords' => $current_post['meta_keywords'],
                    'canonical_url' => base_url(str_replace("/amp", '', uri_string())) . '/',
                    'og_title' => $current_post['og_title'],
                    'og_description' => $current_post['og_description'],
                    'og_image' => $current_post['og_image'],
                    'no_index' => $current_post['no_index'],
                    'no_follow' => $current_post['no_follow'],
                    'no_archive' => $current_post['no_archive'],
                    'meta_url' => base_url(str_replace("/amp", '', uri_string())) . '/'
                );
            }
            //Set Meta Data Current Blog Post Page
            $this->paging_meta_data($meta_data);

            $this->data['slug_name'] = $curret_url_slug;
            $this->data['blog_data'] =  $current_post;
            $this->data['title'] =  $current_post['title'];
            $this->data['post_date'] =  $current_post['post_date'];
			$users_list = $this->home_model->get_users_list();
			$this->data['post_author'] = isset($users_list[$current_post['post_author']]) ? $users_list[$current_post['post_author']] : 'AK Clinics';
            //$this->data['image'] =  $current_post['image'];
            $this->data['similar_post'] = $this->home_model->getSimilarPost();
            $current_method = $this->uri->segment(1);
            $page_name = $this->uri->segment(2);
            $amp_key = $this->uri->segment(3);

            $url_page_amp = $current_method . '/' . $page_name . '/' . $amp_key;
            $match_url_amp = 'blog/'.$page_name.'/amp';

            if($url_page_amp == $match_url_amp)
            {
               
                $this->amp_lib->createDocument($this->data['blog_data']['content']);
                $amp = $this->amp_lib->convert();
                $this->data['blog_data'] =$amp;
                $this->front_template->load('amp_layout', 'contents' , 'front/blogs/'  . 'amp_blog_post', $this->data);
            }
            else if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) 
            {
                header("Location:" . base_url($match_url_amp)); 
                exit();
            
                $this->amp_lib->createDocument($this->data['blog_data']['content']);
                $amp = $this->amp_lib->convert();
                $this->data['blog_data'] =$amp;
                $this->front_template->load('amp_layout', 'contents' , 'front/blogs/'  . 'amp_blog_post', $this->data);
            }
            else
            {
                $this->front_template->load('layout', 'contents' , 'front/blogs/'  . 'blog_post', $this->data);
            }
            
        }
    }
    
    
    public function about_our_team()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(OUR_TEAM);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'about_our_team', $this->data);
    }
    
    public function dr_kapil_dua()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(DR_KAPIL_DUA);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        //$this->dr_blogs(2);
		
        $this->data['dr_name'] = "Dr kapil Dua";
		//Get all blogs
        $all_blogs_conditions = array('post.is_active' => 1, 'post.is_deleted' => 0, 'post.post_author' => 2, 'post.post_status' => 2);
        $limit = 3;
        $all_blogs = $this->home_model->get_post_blogs($all_blogs_conditions, $limit);
        
        //Get patient blogs
        $patient_blog_conditions = array('post.is_active' => 1, 'post.is_deleted' => 0, 'post.post_author' => 2, 'pc.cat_id' => CI_CASE_STUDY, 'post.post_status' => 2);
        $patient_blogs = $this->home_model->get_post_blogs($patient_blog_conditions, $limit);
        
        //Get Success stories blogs
        $success_stories_conditions = array('post.is_active' => 1, 'post.is_deleted' => 0, 'post.post_author' => 2, 'pc.cat_id' => CI_NEWS_AND_EVENTS, 'post.post_status' => 2);
        $success_stories_blogs = $this->home_model->get_post_blogs($success_stories_conditions, $limit);
        
        
        $blogs = array(
            'all_blogs' => $all_blogs,
            'patient_blogs' => $patient_blogs,
            'success_stories_blogs' => $success_stories_blogs,
        );
        
        $this->data['data'] = $this->blogs_slider_view($blogs);
        
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'dr_kapil_dua', $this->data);
    }
    
    public function dr_aman_dua()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(DR_AMAN_DUA);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->data['dr_name'] = "Dr Aman Dua";
        $this->dr_blogs(3);
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'dr_aman_dua', $this->data);
    }
    
    public function dr_roshan_kumar()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(DR_ROSHAN_KUMAR);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'dr_roshan_kumar', $this->data);
    }
    
    public function dr_vivek_mehta()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(DR_VIVEK_MEHTA);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'dr_vivek_mehta', $this->data);
    }
    
    public function dr_shraddha_uprety()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(DR_SHRADDHA_UPRETY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'dr_shraddha_uprety', $this->data);
    }
    
    public function dr_bhawna_bhardwaj()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(DR_BHAWNA_BHARDWAJ);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'dr_bhawna_bhardwaj', $this->data);
    }
    
    public function dr_madhu_kuthial()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(DR_MADHU_KUTHIAL);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'dr_madhu_kuthial', $this->data);
    }
    
    public function dr_jigisha_jalu()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(DR_JIGISHA_JALU);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'dr_jigisha_jalu', $this->data);
    }
    
    public function privacy_policy()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(PRIVACY_POLICY);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'privacy_policy', $this->data);
    }
    
    public function hair_gain_therapy_post_op_care_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_GAIN_THERAPY_POST_OP_CARE_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'hair_gain_therapy_post_op_care_instructions', $this->data);
    }
    
    public function stretch_marks_treatment_post_op_care_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(STRETCH_MARKS_TREATMENT_POST_OP_CARE_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'stretch_marks_treatment_post_op_care_instructions', $this->data);
    }
    
    public function ultherapy_treatment_post_op_care_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(ULTHERAPY_TREATMENT_POST_OP_CARE_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'ultherapy_treatment_post_op_care_instructions', $this->data);
    }
    
    public function pigmentation_treatment_post_op_care_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(PIGMENTATION_TREATMENT_POST_OP_CARE_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'pigmentation_treatment_post_op_care_instructions', $this->data);
    }
    
    public function laser_hair_removal_post_op_care_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(LASER_HAIR_REMOVAL_POST_OP_CARE_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'laser_hair_removal_post_op_care_instructions', $this->data);
    }
    
    public function chemical_peels_post_op_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(CHEMICAL_PEELS_POST_OP_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'chemical_peels_post_op_instructions', $this->data);
    }
    
    public function acne_scar_treatment_post_op_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(ACNE_SCAR_TREATMENT_POST_OP_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'acne_scar_treatment_post_op_instructions', $this->data);
    }
    
    public function scalp_micropigmentation_post_op_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SCALP_MICROPIGMENTATION_POST_OP_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string());
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'scalp_micropigmentation_post_op_instructions', $this->data);
    }
    
    public function rhinoplasty_post_op_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(RHINOPLASTY_POST_OP_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'rhinoplasty_post_op_instructions', $this->data);
    }
    
    public function cosmedico_treatment_post_op_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(COSMEDICO_TREATMENT_POST_OP_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string());
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'cosmedico_treatment_post_op_instructions', $this->data);
    }
    
    public function laser_vaginal_rejuvenation_post_op_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(LASER_VAGINAL_REJUVENATION_POST_OP_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'laser_vaginal_rejuvenation_post_op_instructions', $this->data);
    }
    
    public function co2_laser_treatment_post_op_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(CO2_LASER_TREATMENT_POST_OP_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'co2_laser_treatment_post_op_instructions', $this->data);
    }
    
    public function vitiligo_treatment_post_op_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(VITILIGO_TREATMENT_POST_OP_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'vitiligo_treatment_post_op_instructions', $this->data);
    }
    
    public function gynecomastia_post_op_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(GYNECOMASTIA_POST_OP_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'gynecomastia_post_op_instructions', $this->data);
    }
    
    public function anti_ageing_treatments_post_op_care_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(ANTI_AGEING_TREATMENTS_POST_OP_CARE_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'anti_ageing_treatments_post_op_care_instructions', $this->data);
    }
    
    public function non_surgical_ultrasonic_liposuction_post_op_care_instructions()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(NON_SURGICAL_ULTRASONIC_LIPOSUCTION_POST_OP_CARE_INSTRUCTIONS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/po_instructions/' . DIRECTORY_SEPARATOR . 'non_surgical_ultrasonic_liposuction_post_op_care_instructions', $this->data);
    }
    
    public function sbc_bangalore()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SBC_BANGALORE);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/bangalore/' . DIRECTORY_SEPARATOR . 'index', $this->data);
    }
    
    public function sbc_hair_loss_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SBC_HAIR_LOSS_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/bangalore/' . DIRECTORY_SEPARATOR . 'sbc_hair_loss_treatment', $this->data);
    }
    
    public function sbc_best_hair_transplant_clinic()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SBC_BEST_HAIR_TRANSPLANT_CLINIC);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/bangalore/' . DIRECTORY_SEPARATOR . 'sbc_best_hair_transplant_clinic', $this->data);
    }
    
    public function sbc_laser_hair_removal_for_men_and_women()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SBC_LASER_HAIR_REMOVAL_FOR_MEN_AND_WOMEN);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/bangalore/' . DIRECTORY_SEPARATOR . 'sbc_laser_hair_removal_for_men_and_women', $this->data);
    }
    
    public function sbc_prp_hair_loss_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SBC_PRP_HAIR_LOSS_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/bangalore/' . DIRECTORY_SEPARATOR . 'sbc_prp_hair_loss_treatment', $this->data);
    }
    
    public function sbc_laser_permanent_tattoo_removal()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SBC_LASER_PERMANENT_TATTOO_REMOVAL);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/bangalore/' . DIRECTORY_SEPARATOR . 'sbc_laser_permanent_tattoo_removal', $this->data);
    }
    
    public function sbc_laser_skin_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SBC_LASER_SKIN_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/bangalore/' . DIRECTORY_SEPARATOR . 'sbc_laser_skin_treatment', $this->data);
    }
    
    public function sbc_skin_whitening_lightening_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SBC_SKIN_WHITENING_LIGHTENING_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/bangalore/' . DIRECTORY_SEPARATOR . 'sbc_skin_whitening_lightening_treatment', $this->data);
    }
    
    public function sbc_scalp_micro_pigmentation()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SBC_SCALP_MICRO_PIGMENTATION);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/bangalore/' . DIRECTORY_SEPARATOR . 'sbc_scalp_micro_pigmentation', $this->data);
    }
    
    public function sbc_laser_acne_scar_removal_treatmen()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SBC_LASER_ACNE_SCAR_REMOVAL_TREATMEN);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/bangalore/' . DIRECTORY_SEPARATOR . 'sbc_laser_acne_scar_removal_treatmen', $this->data);
    }
    
    public function sbc_pigmentation_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SBC_PIGMENTATION_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/bangalore/' . DIRECTORY_SEPARATOR . 'sbc_pigmentation_treatment', $this->data);
    }
    
    public function sbc_stretch_marks_removal_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(SBC_STRETCH_MARKS_REMOVAL_TREATMENT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/bangalore/' . DIRECTORY_SEPARATOR . 'sbc_stretch_marks_removal_treatment', $this->data);
    }
    
    public function giga_session()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(GIGA_SESSION);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'giga_session', $this->data);
    }
    
    public function bio_dht()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(BIO_DHT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'bio_dht', $this->data);
    }

    public function faqs()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(FAQS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'faqs', $this->data);
    }
    
    public function hair_loss_men()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_LOSS_MEN);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_loss/' . DIRECTORY_SEPARATOR . 'hair_loss_men', $this->data);
    }
    
    public function hair_loss_women()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_LOSS_WOMEN);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_loss/' . DIRECTORY_SEPARATOR . 'hair_loss_women', $this->data);
    }
    
    public function hair_loss_treatment_men_women()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_LOSS_TREATMENT_MEN_WOMEN);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->hair_trasplant_loss_and_restoration_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/hair_restoration/' . DIRECTORY_SEPARATOR . 'hair_loss_treatment_men_women', $this->data);
    }
    
    public function botulinum_toxin_botox()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(BOTULINUM_TOXIN_BOTOX);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'botulinum_toxin_botox', $this->data);
    }
    
    public function fillers()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(FILLERS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'fillers', $this->data);
    }
    
    public function facial_volumizers()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(FACIAL_VOLUMIZERS);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'facial_volumizers', $this->data);
    }
    
    public function fractional_laser()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(FRACTIONAL_LASER);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'fractional_laser', $this->data);
    }
    
    public function vampire_lift()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(VAMPIRE_LIFT);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->cosmetology_blogs();
        
        $this->front_template->load('layout', 'contents' , 'front/cosmetology/' . DIRECTORY_SEPARATOR . 'vampire_lift', $this->data);
    }
    
    public function demo_page()
    {
        $this->load->view('front/home/demo');
    }
    
    public function hair_transplant_on_emi()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HAIR_TRANSPLANT_ON_EMI);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hair_transplant/' . DIRECTORY_SEPARATOR . 'hair_transplant_on_emi', $this->data);
    }
    
    public function virtual_consultation()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(VIRTUAL_CONSULTATION);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hair_loss/' . DIRECTORY_SEPARATOR . 'virtual_consultation', $this->data);
    }
	
	public function dr_dinkar_sood()
	{
		//Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(DR_DINKAR_SOOD);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'dr_dinkar_sood', $this->data);
	}
	
	public function google_search()
	{
		$meta_data = $this->page_model->get_page_list_id(SEARCH);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
		$serach_string = '';
		if (isset($_GET['s']) && empty($_GET['s']))
		{
			$search_srting = $this->input->get('s');
			if ($search_srting != '') 
			{
				$this->data['search_srting'] = $search_srting;
			}
		}
		
		$this->front_template->load('layout', 'contents' , 'front/blogs/' . DIRECTORY_SEPARATOR . 'search', $this->data);
	}
    
    
    public function hyd_hyderabad()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HYD_HYDERABAD);
        //$meta_data['meta_title'] = '';
        //$meta_data['meta_description'] = '';
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hyderabad/' . DIRECTORY_SEPARATOR . 'index', $this->data);
    }
    
    public function hyd_best_hair_transplant_clinic()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HYD_BEST_HAIR_TRANSPLANT_CLINIC);
        //$meta_data['meta_title'] = '';
        //$meta_data['meta_description'] = '';
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hyderabad/' . DIRECTORY_SEPARATOR . 'hyd_best_hair_transplant_clinic', $this->data);
    }
    
    public function hyd_hair_loss_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HYD_HAIR_LOSS_TREATMENT);
        //$meta_data['meta_title'] = '';
        //$meta_data['meta_description'] = '';
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hyderabad/' . DIRECTORY_SEPARATOR . 'hyd_hair_loss_treatment', $this->data);
    }
    
    public function hyd_laser_hair_removal_for_men_and_women()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HYD_LASER_HAIR_REMOVAL_FOR_MEN_AND_WOMEN);
        //$meta_data['meta_title'] = '';
        //$meta_data['meta_description'] = '';
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hyderabad/' . DIRECTORY_SEPARATOR . 'hyd_laser_hair_removal_for_men_and_women', $this->data);
    }
    
    public function hyd_laser_acne_scar_removal_treatmen()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HYD_LASER_ACNE_SCAR_REMOVAL_TREATMEN);
        //$meta_data['meta_title'] = '';
        //$meta_data['meta_description'] = '';
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hyderabad/' . DIRECTORY_SEPARATOR . 'hyd_laser_acne_scar_removal_treatmen', $this->data);
    }
    
    public function hyd_laser_permanent_tattoo_removal()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HYD_LASER_PERMANENT_TATTOO_REMOVAL);
        //$meta_data['meta_title'] = '';
        //$meta_data['meta_description'] = '';
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hyderabad/' . DIRECTORY_SEPARATOR . 'hyd_laser_permanent_tattoo_removal', $this->data);
    }
    
    public function hyd_laser_skin_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HYD_LASER_SKIN_TREATMENT);
        //$meta_data['meta_title'] = '';
        //$meta_data['meta_description'] = '';
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hyderabad/' . DIRECTORY_SEPARATOR . 'hyd_laser_skin_treatment', $this->data);
    }
    
    public function hyd_pigmentation_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HYD_PIGMENTATION_TREATMENT);
        //$meta_data['meta_title'] = '';
        //$meta_data['meta_description'] = '';
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hyderabad/' . DIRECTORY_SEPARATOR . 'hyd_pigmentation_treatment', $this->data);
    }
    
    public function hyd_prp_hair_loss_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HYD_PRP_HAIR_LOSS_TREATMENT);
        //$meta_data['meta_title'] = '';
        //$meta_data['meta_description'] = '';
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hyderabad/' . DIRECTORY_SEPARATOR . 'hyd_prp_hair_loss_treatment', $this->data);
    }
    
    public function hyd_scalp_micro_pigmentation()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HYD_SCALP_MICRO_PIGMENTATION);
        //$meta_data['meta_title'] = '';
        //$meta_data['meta_description'] = '';
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hyderabad/' . DIRECTORY_SEPARATOR . 'hyd_scalp_micro_pigmentation', $this->data);
    }
    
    public function hyd_skin_whitening_lightening_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HYD_SKIN_WHITENING_LIGHTENING_TREATMENT);
        //$meta_data['meta_title'] = '';
        //$meta_data['meta_description'] = '';
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hyderabad/' . DIRECTORY_SEPARATOR . 'hyd_skin_whitening_lightening_treatment', $this->data);
    }
    
    public function hyd_stretch_marks_removal_treatment()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(HYD_STRETCH_MARKS_REMOVAL_TREATMENT);
        //$meta_data['meta_title'] = '';
        //$meta_data['meta_description'] = '';
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        
        $this->front_template->load('layout', 'contents' , 'front/hyderabad/' . DIRECTORY_SEPARATOR . 'hyd_stretch_marks_removal_treatment', $this->data);
    }
    
    public function dr_nirav_desai()
    {
        //Get a home page meta data
        $meta_data = $this->page_model->get_page_list_id(DR_NIRAV_DESAI);
        $meta_data['meta_url'] = base_url(uri_string()) . '/';
        $this->paging_meta_data($meta_data);
        $this->data['dr_name'] = "Dr Nirav Desai";
        $this->front_template->load('layout', 'contents' , 'front/about_us/' . DIRECTORY_SEPARATOR . 'dr_nirav_desai', $this->data);
    }
}

