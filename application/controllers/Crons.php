<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crons extends CI_Controller 
{
    public function __construct()
	{
		parent::__construct();
        $this->load->model('home_model');
        $this->load->model('blog_model');
        $this->load->model('slug_model');
		$this->load->helper(array('url', 'language'));
	}
    
	public function delete_page_and_databate_cache()
	{
		$this->output->clear_all_cache();
		$this->db->cache_delete_all();
		echo 'Page and Database Cache Deleted Sucessfully!';
		exit;
	}
	
	//Function Name
    public function all_post_blogs()
    {
        ini_set('max_execution_time', 60 * 60);
        
        $pat_args = array('numberposts' => 1000);
        $post_blogs = get_posts($pat_args);
        
        //Truncate the table
        $trun_query = 'truncate `posts`';
        $this->db->query($trun_query);
        
        //Insert WP all blogs
        $query = '';
        
        $is_active = $is_cron = 1; $is_deleted = 0;
        
        $insert_id = '';
        foreach ($post_blogs as $k => $blog)
        {
            //get_post_status($blog->ID)
            //$q = $this->db->query("SELECT * FROM `orgsite_akclinics`.`akc_postmeta` AS t1 where post_id = $blog->ID and meta_key = '_genesis_scripts' ");
            
            //Get Meta OG Title, DESC and Image
            $meta_genesis_scripts = get_post_meta($blog->ID,'_genesis_scripts');
            
            $og_title = $og_desc = $og_image = NULL;
            if(!empty($meta_genesis_scripts))
            {
                $explode_genesis_scripts = str_replace ( '<meta' , '{}<meta', $meta_genesis_scripts[0] );

                $genesis_scripts_arr = explode("{}", $explode_genesis_scripts);

                $explode_og_title = $explode_og_description = $explode_og_image = '';
                $explode_genesis_scripts = array();

                foreach ($genesis_scripts_arr as $k => $meta_genesis_data)
                {
                    if(strpos($meta_genesis_data,"og:title"))
                    {
                        $explode_genesis_scripts['og_title'] = str_replace ( 'og:title" content="' , 'og:title" content="||', $meta_genesis_data );
                        $explode_og_title = explode("||", $explode_genesis_scripts['og_title']);
                        foreach($explode_og_title as $og_final_data)
                        {
                            $og_title = substr($og_final_data, 0, strripos($og_final_data, '" '));
                        }
                    }
                    if(strpos($meta_genesis_data,"og:description"))
                    {
                        $explode_genesis_scripts['og_description'] = str_replace ( 'og:description" content="' , 'og:description" content="||', $meta_genesis_data );
                        $explode_og_description = explode("||", $explode_genesis_scripts['og_description']);
                        foreach($explode_og_description as $og_final_data)
                        {
                            $og_desc = substr($og_final_data, 0, strripos($og_final_data, '"'));
                        }
                    }
                    if(strpos($meta_genesis_data,"og:image"))
                    {
                        $explode_genesis_scripts['og_image'] = str_replace ( 'og:image" content="' , 'og:image" content="||', $meta_genesis_data );
                        $explode_og_image = explode("||", $explode_genesis_scripts['og_image']);
                        foreach($explode_og_image as $og_final_data)
                        {
                            $og_image = substr($og_final_data, 0, strripos($og_final_data, '"'));
                        }
                    }
                }
            }
            
            $post_categories_id = wp_get_post_categories($blog->ID);
            
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            //$post_content = 'Dummy';
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $meta_description[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            //Post Auther Set
            $post_author = $blog->post_author;
            if($post_author == 4){$post_author = 2;}
            else if($post_author == 3){$post_author = 3;}
            else {$post_author = 0;}
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query = "INSERT INTO `posts` (title, content, image, post_url,
						post_status, post_date, document_title, meta_description, meta_keywords, canonical_url, og_title, og_description, og_image, post_author ,no_index, no_follow, no_archive, 
                        is_active, is_cron, is_deleted, created_on ) 
                    VALUES ('".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', '".mysqli_real_escape_string($this->db->conn_id,$og_title)."', '".mysqli_real_escape_string($this->db->conn_id,$og_desc)."', '".mysqli_real_escape_string($this->db->conn_id,$og_image)."' ,$post_author ,$post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on');";
            
            $this->db->query($query);
            
            $post_insert_id = $this->db->insert_id();
            
            foreach ($post_categories_id as $k => $id)
            {
                if(BLOG_CATEGORY == $id){$post_categories_id = CI_BLOG_CATEGORY;}
                else if(HAIR_LOSS_ARTICLES == $id){$post_categories_id = CI_HAIR_LOSS_ARTICLES;}
                else if(UNCATEGORIZED == $id){$post_categories_id = CI_UNCATEGORIZED;}
                else if(PATIRNTS_ASK == $id){$post_categories_id = CI_PATIRNTS_ASK;}
                else if(SUCCESS_STORIES == $id){$post_categories_id = CI_SUCCESS_STORIES;}
                else if(CASE_STUDY == $id){$post_categories_id = CI_CASE_STUDY;}
                else if(COSMETIC_SURGERY_ARTICLES == $id){$post_categories_id = CI_COSMETIC_SURGERY_ARTICLES;}
                else if(WP_COSMETOLOGY == $id){$post_categories_id = CI_COSMETOLOGY;}
                else if(HAIR_LOSS_TREATMENT_ARTICLES == $id){$post_categories_id = CI_HAIR_LOSS_TREATMENT_ARTICLES;}
                else if(HAIR_TRANSPLANT_ARTICLES == $id){$post_categories_id = CI_HAIR_TRANSPLANT_ARTICLES;}
                else if(MEDICAL_TREATMENT_ARTICLES == $id){$post_categories_id = CI_MEDICAL_TREATMENT_ARTICLES;}
                else if(NEWS_AND_EVENTS == $id){$post_categories_id = CI_NEWS_AND_EVENTS;}
                else if(STYLESHALA == $id){$post_categories_id = CI_STYLESHALA;}
                
                $query = "INSERT INTO `posts_cat_ids` (post_id, cat_id) 
                    VALUES ($post_insert_id, $post_categories_id);";
                
                $this->db->query($query);
            }
        }
        
         echo "Post blogs Inserted successfully. </br>";
         exit;
    }
    
    public function update_posts_url()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = (POST_URL_FIND_LIVE == 1)? 'https://akclinics.org' : 'http://192.168.1.133/akc_wp';
        $string_to_replace = (POST_URL_REPLACE_LIVE == 1)? 'http://akclinics.co.in' : 'http://dev.akclinics.com';
        
        $query = "UPDATE `posts` SET post_url = REPLACE(post_url,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Post URL updated according to site URL";
        exit;
    }
    
    public function create_slug_crons_data()
    {
        ini_set('max_execution_time', 60 * 60);
        
        $all_posts = $this->home_model->get_post_url_and_id();
        //debug($all_posts);exit;
        $query = 'INSERT IGNORE INTO `post_slugs` (name, is_active, is_deleted, created_on, created_by) VALUES ';
        
        $is_active = 1;$is_deleted = 0;
        foreach ($all_posts as $k => $data)
        {
            $slug = substr($data['post_url'], strpos($data['post_url'], "blog") + 5);
            $slug_name = trim($slug,'/');
            
            $post_date = $data['post_date'];
            
            $query .= "('".mysqli_real_escape_string($this->db->conn_id,$slug_name)."',$is_active, $is_deleted, '$post_date', 1),";
        }
        
        $query = trim($query,',');
        
        $this->db->query($query);
        
        echo "Post URL slug created successfully!";
        exit;
        
    }
    
    public function update_slug_id_cron_data_posts_table()
    {
        ini_set('max_execution_time', 60 * 60);
        $all_posts = $this->home_model->get_post_url_and_id();
        $query = '';
        foreach($all_posts as $k => $data)
        {
            $query = "UPDATE `posts` Set `slug_id` = (select `id` from `post_slugs`
                        where
                            `name`='" .trim(substr($data['post_url'], strpos($data['post_url'], "blog") + 5), '/') . "' LIMIT 1)
                        where
                    id = " . $data['id'] . ';';
            
            $this->db->query($query);
        }
        
        echo "Slug id updated post table successfully!";
        exit;
    }
    
    public function update_posts_canonical_url()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = (POST_URL_FIND_LIVE == 1)? 'https://akclinics.org' : 'http://192.168.1.133/akc_wp';
        $string_to_replace = (POST_URL_REPLACE_LIVE == 1)? 'http://akclinics.co.in' : 'http://dev.akclinics.com';
        
        $query = "UPDATE `posts` SET canonical_url = REPLACE(canonical_url,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Posts table canonical url updated successfully!";
        exit;
    }
    
    public function update_posts_images_path()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = (POST_URL_FIND_LIVE == 1)? 'https://akclinics.org/wp-content' : 'http://192.168.1.133/akc_wp/wp-content';
        $string_to_replace = (POST_URL_REPLACE_LIVE == 1)? 'assets/template' : 'assets/template';
        
        $query = "UPDATE `posts` SET image = REPLACE(image,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Posts table images path updated sucessfully";
        exit;
    }
    
    public function update_posts_content_anchor_tag_site_path()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = (POST_URL_FIND_LIVE == 1)? 'href="https://akclinics.org' : 'href="http://192.168.1.133/akc_wp';
        $string_to_replace = (POST_URL_REPLACE_LIVE == 1)? 'href="http://akclinics.co.in' : 'href="http://dev.akclinics.com';
        
        $query = "UPDATE `posts` SET content = REPLACE(content,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Posts table contents site path updated sucessfully";
        exit;
    }
    
    public function update_posts_content_img_src_site_path()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = (POST_URL_FIND_LIVE == 1)? 'src="https://akclinics.org/wp-content' : 'src="http://192.168.1.133/akc_wp/wp-content';
        $string_to_replace = (POST_URL_REPLACE_LIVE == 1)? 'src="http://akclinics.co.in/assets/template' : 'src="http://dev.akclinics.com/assets/template';
        
        $query = "UPDATE `posts` SET content = REPLACE(content,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Posts table contents img src path updated sucessfully";
        exit;
    }
    
    public function update_posts_content_class_img_name()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = (POST_URL_FIND_LIVE == 1)? 'class="img-responsive"' : 'class="img-responsive"';
        $string_to_replace = (POST_URL_REPLACE_LIVE == 1)? 'class="img-fluid"' : 'class="img-fluid"';
        
        $query = "UPDATE `posts` SET content = REPLACE(content,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Posts table contents img src path updated sucessfully";
        exit;
    }
    
    public function update_posts_content_class_col()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = (POST_URL_FIND_LIVE == 1)? 'class="col-md-12"' : 'class="col-md-12"';
        $string_to_replace = (POST_URL_REPLACE_LIVE == 1)? 'class="row"' : 'class="row"';
        
        $query = "UPDATE `posts` SET content = REPLACE(content,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Posts table contents img src path updated sucessfully";
        exit;
    }
    
	public function insert_post_comments_data()
	{
		ini_set('max_execution_time', 60 * 60);
		
		$pat_args = array('numberposts' => 1000);
        $post_blogs = get_posts($pat_args);
		
		foreach ($post_blogs as $k => $blog)
        {
			$comments = get_comments("post_id=$blog->ID");
			
			foreach($comments as $comment) 
			{
				$query = "INSERT INTO `post_comments` (id, comment_ID,  comment_post_ID, comment_author, 
							comment_author_email,comment_author_url,comment_author_IP,
							comment_date,comment_date_gmt,comment_content,comment_karma,comment_approved,
							comment_agent,comment_type,comment_parent) 
                    VALUES ($comment->comment_ID,$comment->comment_ID,$comment->comment_post_ID,'".mysqli_real_escape_string($this->db->conn_id,$comment->comment_author)."','".mysqli_real_escape_string($this->db->conn_id,$comment->comment_author_email)."','".mysqli_real_escape_string($this->db->conn_id,$comment->comment_author_url)."','$comment->comment_author_IP','$comment->comment_date','$comment->comment_date_gmt','".mysqli_real_escape_string($this->db->conn_id,$comment->comment_content)."','$comment->comment_karma','$comment->comment_approved','$comment->comment_agent','$comment->comment_type','$comment->comment_parent');";
			
				$this->db->query($query);
			}
		}
		
		echo "Posts comment data inserted sucessfully";
        exit;
	}

	





	public function live_update_posts_content_img_src_site_path()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = (POST_URL_FIND_LIVE == 1)? 'src="https://akclinics.org/wp-content' : 'src="http://192.168.1.133/akc_wp/wp-content';
        $string_to_replace = (POST_URL_REPLACE_LIVE == 1)? 'src="https://akclinics.org/assets/template' : 'src="http://dev.akclinics.com/assets/template';
        
        $query = "UPDATE `posts` SET content = REPLACE(content,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Posts table contents img src site path updated sucessfully";
        exit;
    }
    
    public function live_update_posts_content_img_cdn_path()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = (POST_URL_FIND_LIVE == 1)? 'https://cdn.akclinics.org/wp-content' : 'http://192.168.1.133/akc_wp/wp-content';
        $string_to_replace = (POST_URL_REPLACE_LIVE == 1)? 'https://akclinics.org/assets/template' : 'http://dev.akclinics.com/assets/template';
        
         $query = "UPDATE `posts` SET content = REPLACE(content,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Posts table contents img src cdn path updated sucessfully";exit;
    }
    
    public function live_update_posts_og_img_cdn_path()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = (POST_URL_FIND_LIVE == 1)? 'https://cdn.akclinics.org/wp-content' : 'http://192.168.1.133/akc_wp/wp-content';
        $string_to_replace = (POST_URL_REPLACE_LIVE == 1)? 'https://akclinics.org/assets/template' : 'http://dev.akclinics.com/assets/template';
        
         $query = "UPDATE `posts` SET og_image = REPLACE(og_image,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
       echo "Posts table contents og img src cdn 1 path updated sucessfully";exit;
    }
    
    public function live_update_posts_og_img_cdn_path_update()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = (POST_URL_FIND_LIVE == 1)? 'https://akclinics.org/wp-content' : 'http://192.168.1.133/akc_wp/wp-content';
        $string_to_replace = (POST_URL_REPLACE_LIVE == 1)? 'https://akclinics.org/assets/template' : 'http://dev.akclinics.com/assets/template';
        
         $query = "UPDATE `posts` SET og_image = REPLACE(og_image,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Posts table contents og img src cdn 2 path updated sucessfully";exit;
    }
    
    
    public function live_update_page_og_img_cdn_path()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = (POST_URL_FIND_LIVE == 1)? 'https://cdn.akclinics.org/wp-content' : 'http://192.168.1.133/akc_wp/wp-content';
        $string_to_replace = (POST_URL_REPLACE_LIVE == 1)? 'https://akclinics.org/assets/template' : 'http://dev.akclinics.com/assets/template';
        
         $query = "UPDATE `pages` SET og_image = REPLACE(og_image,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
       echo "Page table contents og img src cdn 1 path updated sucessfully";exit;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function live_update_posts_content_img_src_site_path_in_cdn()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = 'src="https://akclinics.org/assets/template';
        $string_to_replace = 'src="https://cdn.akclinics.org/assets/template';
        
        $query = "UPDATE `posts` SET content = REPLACE(content,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Posts table contents img src site path updated sucessfully";
        exit;
    }
    
    public function live_update_posts_content_img_cdn_path_in_cdn()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = 'https://akclinics.org/assets/template';
        $string_to_replace = 'https://cdn.akclinics.org/assets/template';
        
         $query = "UPDATE `posts` SET content = REPLACE(content,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Posts table contents img src cdn path updated sucessfully";exit;
    }
    
    public function live_update_posts_og_img_cdn_path_update_in_cdn()
    {
        ini_set('max_execution_time', 60 * 60);
        $string_find = 'https://akclinics.org/assets/template';
        $string_to_replace = 'https://cdn.akclinics.org/assets/template';
        
         $query = "UPDATE `posts` SET og_image = REPLACE(og_image,'$string_find','$string_to_replace');";
        
        $this->db->query($query);
        
        echo "Posts table contents og img src cdn 2 path updated sucessfully";exit;
    }
        //all_wp_blog_patient_success_and_case_study_add
    //cosmetic_cosmetology_blogs
    //hair_loss_blogs
    //hair_loss_treat_hair_loss_art_medical_treat
    //news_styleshala_uncate
    
    public function all_wp_blog_patient_success_and_case_study_add()
    {
        ini_set('max_execution_time', 60 * 60);
        $pat_args = array( 'category' => PATIRNTS_ASK, 'numberposts' => 1000);
        $succ_args = array( 'category' => SUCCESS_STORIES, 'numberposts' => 1000);
        $case_args = array( 'category' => CASE_STUDY, 'numberposts' => 1000);
        
        $pat_blog = get_posts($pat_args);
        $succ_blog = get_posts($succ_args);
        $case_blog = get_posts($case_args);

        //Truncate the table
        $trun_query = 'truncate `posts`';
        $this->db->query($trun_query);
        
        //Insert WP all blogs
        $query = 'INSERT INTO `posts` (categories_id, title, content, image, post_url,
						post_status, post_date, document_title, meta_description, meta_keywords, canonical_url, no_index, no_follow, no_archive, 
                        is_active, is_cron, is_deleted, created_on ) 
                    VALUES ';
        
        $is_active = $is_cron = 1; $is_deleted = 0;
        foreach ($pat_blog as $k => $blog)
        {
            $post_categories_id = CI_PATIRNTS_ASK;
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            //$post_content = 'Dummy';
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $document_title[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query .= "($post_categories_id, '".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', $post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on'),";
        }
        
        foreach ($succ_blog as $k => $blog)
        {
            $post_categories_id = CI_SUCCESS_STORIES;
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : '';
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $document_title[0] : '';
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query .= "($post_categories_id, '".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', $post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on'),";
        }
        
        foreach ($case_blog as $k => $blog)
        {
            $post_categories_id = CI_CASE_STUDY;
            $post_title = $blog->post_title;
            $post_content = trim($blog->post_content);
            //$post_content = 'Dummy';
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $document_title[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query .= "($post_categories_id, '".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', $post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on'),";
        }
        
        $query = trim($query,',');
        $this->db->query($query);
        
        echo "Patient blogs Inserted successfully. </br>";
        echo "Success stories blogs Inserted successfully. </br>";
        echo "Case study blogs Inserted successfully. </br>";
    }
    
    public function cosmetic_cosmetology_blogs()
    {
        ini_set('max_execution_time', 60 * 60);
        
        $cos_sur_args = array( 'category' => COSMETIC_SURGERY_ARTICLES, 'numberposts' => 1000);
        $cosm_args = array( 'category' => WP_COSMETOLOGY, 'numberposts' => 1000);

        $cos_sur_blog = get_posts($cos_sur_args);
        $cosm_blog = get_posts($cosm_args);
        
        //Insert WP all blogs
        $query = 'INSERT INTO `posts` (categories_id, title, content, image, post_url,
						post_status, post_date, document_title, meta_description, meta_keywords, canonical_url, no_index, no_follow, no_archive, 
                        is_active, is_cron, is_deleted, created_on ) 
                    VALUES ';
        
        $is_active = $is_cron = 1; $is_deleted = 0;
        foreach ($cos_sur_blog as $k => $blog)
        {
            $post_categories_id = CI_COSMETIC_SURGERY_ARTICLES;
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $document_title[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query .= "($post_categories_id, '".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', $post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on'),";
        }
        
       foreach ($cosm_blog as $k => $blog)
        {
            $post_categories_id = CI_COSMETOLOGY;
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $document_title[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query .= "($post_categories_id, '".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', $post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on'),";
        }
        
        $query = trim($query,',');
        $this->db->query($query);
        
        echo "Cosmetic Surgery Articles blogs Inserted successfully. </br>";
        echo "Cosmetology stories blogs Inserted successfully. </br>";
    }
    
    public function hair_loss_blogs()
    {
        ini_set('max_execution_time', 60 * 60);
        
        $hair_loss_args = array( 'category' => HAIR_LOSS_ARTICLES, 'numberposts' => 1000);
        
        $hair_loss_blog = get_posts($hair_loss_args);
        
        //Insert WP all blogs
        $query = 'INSERT INTO `posts` (categories_id, title, content, image, post_url,
						post_status, post_date, document_title, meta_description, meta_keywords, canonical_url, no_index, no_follow, no_archive, 
                        is_active, is_cron, is_deleted, created_on ) 
                    VALUES ';
        
        $is_active = $is_cron = 1; $is_deleted = 0;
        foreach ($hair_loss_blog as $k => $blog)
        {
            $post_categories_id = CI_HAIR_LOSS_ARTICLES;
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $document_title[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query .= "($post_categories_id, '".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', $post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on'),";
        }
        
        $query = trim($query,',');
        $this->db->query($query);
        
        echo "Hair Loss Articles blogs Inserted successfully. </br>";
    }

    public function hair_loss_treat_hair_loss_art_medical_treat()
    {
        ini_set('max_execution_time', 60 * 60);
        
        $hair_loss_treat_args = array( 'category' => HAIR_LOSS_TREATMENT_ARTICLES, 'numberposts' => 1000);
        $hair_tra_art_args = array( 'category' => HAIR_TRANSPLANT_ARTICLES, 'numberposts' => 1000);
        $medi_treat_args = array( 'category' => MEDICAL_TREATMENT_ARTICLES, 'numberposts' => 1000);
        
        $hair_loss_treat_blog = get_posts($hair_loss_treat_args);
        $hair_tra_art_blog = get_posts($hair_tra_art_args);
        $medi_treat_blog = get_posts($medi_treat_args);
        
        //Insert WP all blogs
        $query = 'INSERT INTO `posts` (categories_id, title, content, image, post_url,
						post_status, post_date, document_title, meta_description, meta_keywords, canonical_url, no_index, no_follow, no_archive, 
                        is_active, is_cron, is_deleted, created_on ) 
                    VALUES ';
        
        $is_active = $is_cron = 1; $is_deleted = 0;
        foreach ($hair_loss_treat_blog as $k => $blog)
        {
            $post_categories_id = CI_HAIR_LOSS_TREATMENT_ARTICLES;
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $document_title[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query .= "($post_categories_id, '".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', $post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on'),";
        }
        
        foreach ($hair_tra_art_blog as $k => $blog)
        {
            $post_categories_id = CI_HAIR_TRANSPLANT_ARTICLES;
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $document_title[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query .= "($post_categories_id, '".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', $post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on'),";
        }
        
        foreach ($medi_treat_blog as $k => $blog)
        {
            $post_categories_id = CI_MEDICAL_TREATMENT_ARTICLES;
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $document_title[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query .= "($post_categories_id, '".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', $post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on'),";
        }
        
        $query = trim($query,',');
        $this->db->query($query);
        
        echo "Hair Loss Treatment Articles blogs Inserted successfully. </br>";
        echo "Hair Transplant Articles blogs Inserted successfully. </br>";
        echo "Medical Treatment Articles blogs Inserted successfully. </br>";
        
    }
    
    public function news_styleshala_uncate()
    {
        ini_set('max_execution_time', 60 * 60);
        
        $news_eve_args = array( 'category' => NEWS_AND_EVENTS, 'numberposts' => 1000);
        $style_args = array( 'category' => STYLESHALA, 'numberposts' => 1000);
        $uncat_args = array( 'category' => UNCATEGORIZED, 'numberposts' => 1000);
        
        $news_eve_blog = get_posts($news_eve_args);
        $style_blog = get_posts($style_args);
        $uncat_blog = get_posts($uncat_args);
         
        //Insert WP all blogs
        $query = 'INSERT INTO `posts` (categories_id, title, content, image, post_url,
						post_status, post_date, document_title, meta_description, meta_keywords, canonical_url, no_index, no_follow, no_archive, 
                        is_active, is_cron, is_deleted, created_on ) 
                    VALUES ';
        
        $is_active = $is_cron = 1; $is_deleted = 0;
        foreach ($news_eve_blog as $k => $blog)
        {
            $post_categories_id = CI_NEWS_AND_EVENTS;
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $document_title[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query .= "($post_categories_id, '".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', $post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on'),";
        }
        
        foreach ($style_blog as $k => $blog)
        {
            $post_categories_id = CI_STYLESHALA;
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $document_title[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query .= "($post_categories_id, '".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', $post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on'),";
        }
        
        foreach ($uncat_blog as $k => $blog)
        {
            $post_categories_id = CI_UNCATEGORIZED;
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $document_title[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query .= "($post_categories_id, '".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', $post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on'),";
        }
        
        $query = trim($query,',');
        $this->db->query($query);
        
        echo "News and Events blogs Inserted successfully. </br>";
        echo "Styleshala blogs Inserted successfully. </br>";
        echo "Uncategorized blogs Inserted successfully. </br>";
    }
	
	
	    //Function Name
    public function all_post_blogs_test()
    {
        ini_set('max_execution_time', 60 * 60);
        
        $pat_args = array('numberposts' => 1000);
        $post_blogs = get_posts($pat_args);
        
        //Truncate the table
        $trun_query = 'truncate `posts`';
        $this->db->query($trun_query);
        
        //Insert WP all blogs
        $query = '';
        
        $is_active = $is_cron = 1; $is_deleted = 0;
        
        $insert_id = '';
        foreach ($post_blogs as $k => $blog)
        {
            //get_post_status($blog->ID)
            //$q = $this->db->query("SELECT * FROM `orgsite_akclinics`.`akc_postmeta` AS t1 where post_id = $blog->ID and meta_key = '_genesis_scripts' ");
            
            //Get Meta OG Title, DESC and Image
            $meta_genesis_scripts = get_post_meta($blog->ID,'_genesis_scripts');
            
            $og_title = $og_desc = $og_image = NULL;
            if(!empty($meta_genesis_scripts))
            {
                $explode_genesis_scripts = str_replace ( '<meta' , '{}<meta', $meta_genesis_scripts[0] );

                $genesis_scripts_arr = explode("{}", $explode_genesis_scripts);

                $explode_og_title = $explode_og_description = $explode_og_image = '';
                $explode_genesis_scripts = array();

                foreach ($genesis_scripts_arr as $k => $meta_genesis_data)
                {
                    if(strpos($meta_genesis_data,"og:title"))
                    {
                        $explode_genesis_scripts['og_title'] = str_replace ( 'og:title" content="' , 'og:title" content="||', $meta_genesis_data );
                        $explode_og_title = explode("||", $explode_genesis_scripts['og_title']);
                        foreach($explode_og_title as $og_final_data)
                        {
                            $og_title = substr($og_final_data, 0, strripos($og_final_data, '" '));
                        }
                    }
                    if(strpos($meta_genesis_data,"og:description"))
                    {
                        $explode_genesis_scripts['og_description'] = str_replace ( 'og:description" content="' , 'og:description" content="||', $meta_genesis_data );
                        $explode_og_description = explode("||", $explode_genesis_scripts['og_description']);
                        foreach($explode_og_description as $og_final_data)
                        {
                            $og_desc = substr($og_final_data, 0, strripos($og_final_data, '"'));
                        }
                    }
                    if(strpos($meta_genesis_data,"og:image"))
                    {
                        $explode_genesis_scripts['og_image'] = str_replace ( 'og:image" content="' , 'og:image" content="||', $meta_genesis_data );
                        $explode_og_image = explode("||", $explode_genesis_scripts['og_image']);
                        foreach($explode_og_image as $og_final_data)
                        {
                            $og_image = substr($og_final_data, 0, strripos($og_final_data, '"'));
                        }
                    }
                }
            }
            
            $post_categories_id = wp_get_post_categories($blog->ID);
            
            $post_title = $blog->post_title;
            $post_content = $blog->post_content;
            //$post_content = 'Dummy';
            $post_image = get_the_post_thumbnail_url($blog->ID);
            $post_guid = get_permalink( $blog->ID );
            $post_status = $blog->post_status;
            if($post_status == 'publish') { $post_status = 2; } else { $post_status = 1; }
            $post_date = $created_on  = $blog->post_date;
            $document_title = get_post_meta($blog->ID,'_genesis_title');
            $post_document_title = isset($document_title[0]) ? $document_title[0] : NULL;
            $meta_description = get_post_meta($blog->ID,'_genesis_description');
            $post_meta_description = isset($meta_description[0]) ? $meta_description[0] : NULL;
            
            $meta_keywords = get_post_meta($blog->ID,'_genesis_keywords');
            $post_meta_keywords = isset($meta_keywords[0]) ? $meta_keywords[0] : NULL;
            
            $meta_canonical_uri = get_post_meta($blog->ID,'_genesis_canonical_uri');
            $post_meta_canonical_uri = isset($meta_canonical_uri[0]) ? $meta_canonical_uri[0] : NULL;
            
            //Post Auther Set
            $post_author = $blog->post_author;
            /*if($post_author == 4){$post_author = 2;}
            else if($post_author == 3){$post_author = 3;}
            else {$post_author = 0;}*/
            
            $meta_no_index = get_post_meta($blog->ID,'_genesis_noindex');
            $post_meta_no_index = isset($meta_no_index[0]) ? $meta_no_index[0] : 0;
            
            $meta_no_follow = get_post_meta($blog->ID,'_genesis_nofollow');
            $post_meta_no_follow = isset($meta_no_follow[0]) ? $meta_no_follow[0] : 0;
            
            $meta_no_archive = get_post_meta($blog->ID,'_genesis_noarchive');
            $post_no_archive = isset($meta_no_archive[0]) ? $meta_no_archive[0] : 0;
            
            $query = "INSERT INTO `posts` (wp_post_id,title, content, image, post_url,
						post_status, post_date, document_title, meta_description, meta_keywords, canonical_url, og_title, og_description, og_image, post_author,wp_post_author_id ,no_index, no_follow, no_archive, 
                        is_active, is_cron, is_deleted, created_on ) 
                    VALUES ($blog->ID,'".mysqli_real_escape_string($this->db->conn_id,$post_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_content)."', '$post_image', '$post_guid', '$post_status', '$post_date', '".mysqli_real_escape_string($this->db->conn_id,$post_document_title)."', '".mysqli_real_escape_string($this->db->conn_id,$post_meta_description)."', '$post_meta_keywords', '$post_meta_canonical_uri', '".mysqli_real_escape_string($this->db->conn_id,$og_title)."', '".mysqli_real_escape_string($this->db->conn_id,$og_desc)."', '".mysqli_real_escape_string($this->db->conn_id,$og_image)."' ,$post_author, $post_author ,$post_meta_no_index, $post_meta_no_follow, $post_no_archive ,$is_active, $is_cron, $is_deleted, '$created_on');";
            
            $this->db->query($query);
            
            $post_insert_id = $this->db->insert_id();
            
            foreach ($post_categories_id as $k => $id)
            {
                if(BLOG_CATEGORY == $id){$post_categories_id = CI_BLOG_CATEGORY;}
                else if(HAIR_LOSS_ARTICLES == $id){$post_categories_id = CI_HAIR_LOSS_ARTICLES;}
                else if(UNCATEGORIZED == $id){$post_categories_id = CI_UNCATEGORIZED;}
                else if(PATIRNTS_ASK == $id){$post_categories_id = CI_PATIRNTS_ASK;}
                else if(SUCCESS_STORIES == $id){$post_categories_id = CI_SUCCESS_STORIES;}
                else if(CASE_STUDY == $id){$post_categories_id = CI_CASE_STUDY;}
                else if(COSMETIC_SURGERY_ARTICLES == $id){$post_categories_id = CI_COSMETIC_SURGERY_ARTICLES;}
                else if(WP_COSMETOLOGY == $id){$post_categories_id = CI_COSMETOLOGY;}
                else if(HAIR_LOSS_TREATMENT_ARTICLES == $id){$post_categories_id = CI_HAIR_LOSS_TREATMENT_ARTICLES;}
                else if(HAIR_TRANSPLANT_ARTICLES == $id){$post_categories_id = CI_HAIR_TRANSPLANT_ARTICLES;}
                else if(MEDICAL_TREATMENT_ARTICLES == $id){$post_categories_id = CI_MEDICAL_TREATMENT_ARTICLES;}
                else if(NEWS_AND_EVENTS == $id){$post_categories_id = CI_NEWS_AND_EVENTS;}
                else if(STYLESHALA == $id){$post_categories_id = CI_STYLESHALA;}
                
                $query = "INSERT INTO `posts_cat_ids` (post_id, cat_id) 
                    VALUES ($post_insert_id, $post_categories_id);";
                
                $this->db->query($query);
            }
        }
        
         echo "Post blogs Inserted successfully. </br>";
         exit;
    }
}