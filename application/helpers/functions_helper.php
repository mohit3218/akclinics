<?php

function debug($data)
{
    $bt = debug_backtrace();
    $caller = array_shift($bt);
    
    echo "<pre>";
    echo "<b>" . $caller["file"] . " : " . $caller["line"] . "</b><br/>";
    print_r($data);
    echo "</pre>";
}

function obj_to_array($obj) 
{
    $arr = array();
    if (gettype($obj) == "object") 
    {
        $arr = obj_to_array(get_object_vars($obj));
    } 
    else if (gettype($obj) == "array") 
    {
        foreach ($obj as $k => $v) 
        {
            $arr[$k] = obj_to_array($v);
        }
    }
    else 
    {
        $arr = $obj;
    }

    return $arr;
}