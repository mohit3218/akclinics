<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('meta_tags'))
{
    function meta_tags($meta)
    {
        $html = '';

        //uses default set in seo_config.php

        $html .= '<title>' . $meta['title'] .'</title>' . "\r\n";
        //$html .= '<meta name="title" content="' . $meta['title'] . '"/>' . "\r\n";
        $html .= '<meta name="description" content="' . $meta['desc'] . '"/>' . "\r\n\r\n";
        
        if(!empty($meta['keywords']))
        {
            $html .= '<meta name="keywords" content="' . $meta['keywords'] . '"/>' . "\r\n\r\n";
        }
        
        if(!empty($meta['canonical_url']))
        {   
            $html .= '<link rel="canonical" href="' . $meta['canonical_url'] . '"/>' . "\r\n\r\n";
        }
        
        if(!empty($meta['og_title']) && !empty($meta['og_desc']) && !empty($meta['og_image']))
        {
            $html .= '<meta property="article:publisher" content="https://www.facebook.com/akclinics"/>' . "\r\n";
            $html .= '<meta property="og:type" content="article"/>' . "\r\n";
            $html .= '<meta property="og:site_name" content="akclinics.org"/>' . "\r\n";
            $html .= '<meta property="og:title" content="' . $meta['og_title'] . '"/>' . "\r\n";
            $html .= '<meta name="og:description" content="' . $meta['og_desc'] . '"/>' . "\r\n";
            $html .= '<meta name="og:url" content="' . $meta['og_url'] . '"/>' . "\r\n";
            $html .= '<meta name="og:image" content="' . $meta['og_image'] . '"/>' . "\r\n\r\n\r\n";
            
            $html .= '<meta property="twitter:card" content="summary_large_image"/>' . "\r\n";
            $html .= '<meta name="twitter:url" content="' . $meta['og_url'] . '"/>';
            $html .= '<meta property="twitter:site" content="akclinics.org"/>' . "\r\n";
            $html .= '<meta property="twitter:title" content="' . $meta['og_title'] . '"/>' . "\r\n";
            $html .= '<meta name="twitter:description" content="' . $meta['og_desc'] . '"/>' . "\r\n";
            $html .= '<meta name="twitter:image" content="' . $meta['og_image'] . '"/>' . "\r\n";
            $html .= '<meta name="twitter:creator" content="@akclinics"/>' . "\r\n";
        }
        
        
        if (!empty($meta['no_index']) && !empty($meta['no_follow']) && $meta['no_index'] == 0 && $meta['no_follow'] == 0)
        {
            $html .= '<meta name="robots" content="index,follow"/>' . "\r\n";
        }
        else if (!empty($meta['no_index']) && !empty($meta['no_follow']) && $meta['no_index'] == 1 && $meta['no_follow'] == 0)
        {
            $html .= '<meta name="robots" content="follow"/>' . "\r\n";
        }
        else if (!empty($meta['no_index']) && !empty($meta['no_follow']) &&$meta['no_index'] == 0 && $meta['no_follow'] == 1)
        {
            $html .= '<meta name="robots" content="index"/>' . "\r\n";
        }
        
        echo $html;
    }

}
