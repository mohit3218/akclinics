<?php
/**
 * StaticArray
 * 
 * @created    20/10/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */
Class StaticArray
{
   public static $status_arr = array(
       1 => "Active",
       0 => "In-Active"
   );
   
    public static $blog_post_status = array(
       1 => "Private",
       2 => "Public"
   );
    
   public static $enquire_types = array(
       1 => "Hair Transplant",
       2 => "Cosmetic Surgery",
       3 => "Cosmetology"
   );
}
