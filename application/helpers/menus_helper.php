<?php
Class AdminMenu
{
    public static $adminMenuConfig = array(
        array(
            'title' => 'Page Manager',
            'class' => 'm-menu__link-icon la la-external-link',
            'links' => array(
                array(
                    'text' => 'All Slugs',
                    'controller' => '',
                    'action' => 'slug',
                    'route' => 'admin/slug',
                     "other_routes" => [],
                    'page_title' => 'All Slugs'
                ),
                array(
                    'text' => 'All Pages',
                    'controller' => '',
                    'action' => 'page',
                    'route' => 'admin/page',
                    "other_routes" => [],
                    'page_title' => 'All Pages'
                ),
            )
        ),
        array(
            'title' => 'Blogs Manager',
            'class' => 'm-menu__link-icon la la-external-link',
            'links' => array(
                array(
                    'text' => 'All Categories',
                    'controller' => '',
                    'action' => 'categories',
                    'route' => 'admin/categories',
                    "other_routes" => [],
                    'page_title' => 'All Categories'
                ),
                array(
                    'text' => 'All Post',
                    'controller' => '',
                    'action' => 'post',
                    'route' => 'admin/post',
                    "other_routes" => [],
                    'page_title' => 'All Posts'
                ),
            )
        ),
        array(
            'title' => 'Settings',
            'class' => 'm-menu__link-icon la la-user',
            'links' => array(
                array(
                    'text' => 'Summary',
                    'controller' => '',
                    'action' => 'auth',
                    'route' => 'admin/auth',
                    "other_routes" => [],
                    'page_title' => 'All User'
                ),
                array(
                    'text' => 'Google Review Ratings',
                    'controller' => '',
                    'action' => 'ratings',
                    'route' => 'admin/ratings',
                    "other_routes" => [],
                    'page_title' => 'Google Review Ratings'
                ),
            )
        ),
    );
}
