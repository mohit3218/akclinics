<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//Hair Transplant routes
$route['default_controller'] = 'Home/index';
$route['demo-page'] = 'home/demo_page';
$route['hair-transplant'] = 'home/hair_transplant';
$route['hair-transplant/hair-transplant-in-men'] = 'home/hair_transplant_in_men';
$route['hair-transplant/hair-transplant-in-women'] = 'home/hair_transplant_in_women';
$route['hair-transplant/hair-transplant-cost'] = 'home/hair_transplant_cost';
$route['hair-transplant/fue-hair-transplant'] = 'home/fue_hair_transplant';
$route['hair-transplant/fut-strip-hair-transplant'] = 'home/fut_strip_hair_transplant';

$route['hair-transplant/hair-transplant-techniques'] = 'home/hair_transplant_techniques';
$route['hair-transplant/facial-hair-transplant'] = 'home/facial_hair_transplant';
$route['hair-transplant/beard-transplant'] = 'home/beard_transplant';
$route['hair-transplant/bio-fue'] = 'home/bio_fue';
$route['hair-transplant/body-hair-transplant'] = 'home/body_hair_transplant';
$route['hair-transplant/revision-hair-transplant'] = 'home/revision_hair_transplant';
$route['hair-transplant/post-operative-care'] = 'home/post_operative_care';
$route['hair-transplant/giga-session'] = 'home/giga_session';
$route['hair-transplant/bio-dht'] = 'home/bio_dht';
$route['hair-transplant/faqs'] = 'home/faqs';
$route['hair-transplant-on-emi'] = 'home/hair_transplant_on_emi';


//About Us Routes
$route['about-us'] = 'home/about_us';
$route['hair-transplant-training'] = 'home/hair_transplant_training';
$route['about-us/our-team'] = 'home/about_our_team';
$route['about-us/our-team/dr-kapil-dua'] = 'home/dr_kapil_dua';
$route['about-us/our-team/dr-aman-dua'] = 'home/dr_aman_dua';
$route['about-us/our-team/dr-roshan-kumar'] = 'home/dr_roshan_kumar';
$route['about-us/our-team/dr-vivek-mehta'] = 'home/dr_vivek_mehta';
$route['about-us/our-team/dr-shraddha-uprety'] = 'home/dr_shraddha_uprety';
$route['about-us/our-team/dr-bhawna-bhardwaj'] = 'home/dr_bhawna_bhardwaj';
$route['about-us/our-team/dr-madhu-kuthial'] = 'home/dr_madhu_kuthial';
$route['about-us/our-team/dr-jigisha-jalu'] = 'home/dr_jigisha_jalu';
$route['about-us/our-team/dr-dinkar-sood'] = 'home/dr_dinkar_sood';
$route['about-us/our-team/dr-nirav-desai'] = 'home/dr_nirav_desai';

//Hair Loss Roues
$route['hair-loss'] = 'home/hair_loss';
$route['hair-loss/hair-loss-men'] = 'home/hair_loss_men';
$route['hair-loss/hair-loss-women'] = 'home/hair_loss_women';
$route['virtual-consultation'] = 'home/virtual_consultation';
//Hair Restoration Roues
$route['hair-restoration'] = 'home/hair_restoration';
$route['hair-gain-therapy'] = 'home/hair_gain_therapy';
$route['prp-treatment'] = 'home/prp_treatment';
$route['hair-loss-treatment-men-women'] = 'home/hair_loss_treatment_men_women';
$route['artificial-hair-restoration'] = 'home/artificial_hair_restoration';
$route['hair-restoration/scalp-micro-pigmentation-india'] = 'home/scalp_micro_pigmentation_india';
$route['mesotherapy'] = 'home/mesotherapy';

//Cosmetic Surgery Routes
$route['cosmetic-surgery'] = 'home/cosmetic_surgery';
$route['rhinoplasty'] = 'home/rhinoplasty';
$route['blepharoplasty'] = 'home/blepharoplasty';
$route['gynecomastia'] = 'home/gynecomastia';
$route['vitiligo-treatment'] = 'home/vitiligo_treatment';
$route['abdominoplasty'] = 'home/abdominoplasty';
$route['liposuction'] = 'home/liposuction';
$route['breast-augmentation'] = 'home/breast_augmentation';
$route['breast-reduction'] = 'home/breast_reduction';
$route['cosmetic-gynaecology'] = 'home/cosmetic_gynaecology';
$route['male-genital-surgery'] = 'home/male_genital_surgery';

//Cosmetology Surgery Routes
$route['cosmetology'] = 'home/cosmetology';
$route['cosmetology/laser-hair-removal-for-men-and-women'] = 'home/laser_hair_removal_for_men_and_women';
$route['cosmetology/non-surgical-ultrasonic-liposuction'] = 'home/non_surgical_ultrasonic_liposuction';
$route['cosmetology/skin-polishing'] = 'home/skin_polishing';
$route['cosmetology/stretch-marks-treatment'] = 'home/stretch_marks_treatment';
$route['cosmetology/laser-photo-facial'] = 'home/laser_photo_facial';
$route['cosmetology/laser-tattoo-removal'] = 'home/laser_tattoo_removal';
$route['cosmetology/ultherapy-treatment'] = 'home/ultherapy_treatment';
$route['cosmetology/chemical-peels'] = 'home/chemical_peels';
$route['cosmetology/acne-treatment'] = 'home/acne_treatment';
$route['cosmetology/pigmentation-treatment'] = 'home/pigmentation_treatment';
$route['cosmetology/laser-vaginal-rejuvenation'] = 'home/laser_vaginal_rejuvenation';
$route['cosmetology/anti-ageing-treatments'] = 'home/anti_ageing_treatments';
$route['cosmetology/ultracel-skin-tightening-treatment'] = 'home/ultracel_skin_tightening_treatment';
$route['cosmetology/anti-ageing-treatments/botulinum-toxin-botox'] = 'home/botulinum_toxin_botox';
$route['cosmetology/anti-ageing-treatments/fillers'] = 'home/fillers';
$route['cosmetology/anti-ageing-treatments/facial-volumizers'] = 'home/facial_volumizers';
$route['cosmetology/anti-ageing-treatments/fractional-laser'] = 'home/fractional_laser';
$route['cosmetology/anti-ageing-treatments/vampire-lift'] = 'home/vampire_lift';

//Results Routes
$route['results'] = 'home/results';
$route['results/videos'] = 'home/videos';
$route['results/cosmetic-surgery-results'] = 'home/cosmetic_surgery_results';
$route['results/prp-therapy'] = 'home/prp_therapy';
//Blog Routes
$route['blog'] = 'home/blog';
$route['blog/page/(:any)'] = 'home/blog';
$route['blog/author/(:any)'] = 'home/blog';
$route['blog/tag/(:any)'] = 'home/blog';
$route['tag/(:any)'] = 'home/blog';
$route['blog/patients-ask/(:any)'] = 'home/loadMoreData';
$route['blog/success-stories/(:any)'] = 'home/loadMoreData';
$route['blog/case-study/(:any)'] = 'home/loadMoreData';
$route['blog/hair-transplant-articles/(:any)'] = 'home/loadMoreData';
$route['blog/news-and-events/(:any)'] = 'home/loadMoreData';
$route['blog/hair-loss-articles/(:any)'] = 'home/loadMoreData';
$route['blog/hair-loss-treatments/(:any)'] = 'home/loadMoreData';
$route['blog/medical-treatment-articles/(:any)'] = 'home/loadMoreData';
$route['blog/cosmetic-surgery-articles/(:any)'] = 'home/loadMoreData';
$route['blog/styleshala/(:any)'] = 'home/loadMoreData';
$route['search'] = 'home/google_search';
//Locations Routes
$route['locations'] = 'home/locations';
$route['locations/hair-transplant-ludhiana'] = 'home/hair_transplant_ludhiana';
$route['locations/hair-transplant-delhi'] = 'home/hair_transplant_delhi';
//$route['locations/best-hair-transplant-clinic'] = 'home/best_hair_transplant_clinic';
//Contact US Route
$route['contact-us'] = 'home/contact_us';
//Post any route
$route['blog/(:any)'] = 'home/blog_posts';
$route['blog/(:any)/amp'] = 'home/blog_posts';
//Post operations routes
$route['privacy-policy'] = 'home/privacy_policy';
$route['hair-gain-therapy-post-op-care-instructions'] = 'home/hair_gain_therapy_post_op_care_instructions';
$route['stretch-marks-treatment-post-op-care-instructions'] = 'home/stretch_marks_treatment_post_op_care_instructions';
$route['ultherapy-treatment-post-op-care-instructions'] = 'home/ultherapy_treatment_post_op_care_instructions';
$route['pigmentation-treatment-post-op-care-instructions'] = 'home/pigmentation_treatment_post_op_care_instructions';
$route['laser-hair-removal-post-op-care-instructions'] = 'home/laser_hair_removal_post_op_care_instructions';
$route['chemical-peels-post-op-instructions'] = 'home/chemical_peels_post_op_instructions';
$route['acne-scar-treatment-post-op-instructions'] = 'home/acne_scar_treatment_post_op_instructions';
$route['scalp-micropigmentation-post-op-instructions'] = 'home/scalp_micropigmentation_post_op_instructions';
$route['rhinoplasty-post-op-instructions'] = 'home/rhinoplasty_post_op_instructions';
$route['cosmedico-treatment-post-op-instructions'] = 'home/cosmedico_treatment_post_op_instructions';
$route['laser-vaginal-rejuvenation-post-op-instructions'] = 'home/laser_vaginal_rejuvenation_post_op_instructions';
$route['co2-laser-treatment-post-op-instructions'] = 'home/co2_laser_treatment_post_op_instructions';
$route['vitiligo-treatment-post-op-instructions'] = 'home/vitiligo_treatment_post_op_instructions';
$route['gynecomastia-post-op-instructions'] = 'home/gynecomastia_post_op_instructions';
$route['anti-ageing-treatments-post-op-care-instructions'] = 'home/anti_ageing_treatments_post_op_care_instructions';
$route['non-surgical-ultrasonic-liposuction-post-op-care-instructions'] = 'home/non_surgical_ultrasonic_liposuction_post_op_care_instructions';
//Bangalore routes
$route['bangalore'] = 'home/sbc_bangalore';
$route['bangalore/hair-loss-treatment'] = 'home/sbc_hair_loss_treatment';
$route['bangalore/best-hair-transplant-clinic'] = 'home/sbc_best_hair_transplant_clinic';
$route['bangalore/laser-hair-removal-for-men-and-women'] = 'home/sbc_laser_hair_removal_for_men_and_women';
$route['bangalore/prp-hair-loss-treatment'] = 'home/sbc_prp_hair_loss_treatment';
$route['bangalore/laser-permanent-tattoo-removal'] = 'home/sbc_laser_permanent_tattoo_removal';
$route['bangalore/laser-skin-treatment'] = 'home/sbc_laser_skin_treatment';
$route['bangalore/skin-whitening-lightening-treatment'] = 'home/sbc_skin_whitening_lightening_treatment';
$route['bangalore/scalp-micro-pigmentation'] = 'home/sbc_scalp_micro_pigmentation';
$route['bangalore/laser-acne-scar-removal-treatment'] = 'home/sbc_laser_acne_scar_removal_treatmen';
$route['bangalore/pigmentation-treatment'] = 'home/sbc_pigmentation_treatment';
$route['bangalore/stretch-marks-removal-treatment'] = 'home/sbc_stretch_marks_removal_treatment';

//Page does not exist route
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Admin Panel Route
//Login Route
$route['s3cure-admin-panel'] = 'admin/Auth/index';
//Slug Manager Route
$route['slug'] = 'slug';
$route['slug/create'] = 'slug/create_slug';
$route['slug/edit/(:any)'] = 'slug/edit_slug/$1';
$route['slug/delete/(:any)'] = 'slug/delete/$1';
//Page Manager Route
$route['page'] = 'page';
$route['page/create'] = 'page/create_page';
$route['page/edit/(:any)'] = 'page/edit_page/$1';
$route['page/delete/(:any)'] = 'page/delete/$1';
//Hyderabad routes
$route['hyderabad'] = 'home/hyd_hyderabad';
$route['hyderabad/hair-loss-treatment'] = 'home/hyd_hair_loss_treatment';
$route['hyderabad/best-hair-transplant-clinic'] = 'home/hyd_best_hair_transplant_clinic';
$route['hyderabad/laser-hair-removal-for-men-and-women'] = 'home/hyd_laser_hair_removal_for_men_and_women';
$route['hyderabad/prp-hair-loss-treatment'] = 'home/hyd_prp_hair_loss_treatment';
$route['hyderabad/laser-permanent-tattoo-removal'] = 'home/hyd_laser_permanent_tattoo_removal';
$route['hyderabad/laser-skin-treatment'] = 'home/hyd_laser_skin_treatment';
$route['hyderabad/skin-whitening-lightening-treatment'] = 'home/hyd_skin_whitening_lightening_treatment';
$route['hyderabad/scalp-micro-pigmentation'] = 'home/hyd_scalp_micro_pigmentation';
$route['hyderabad/laser-acne-scar-removal-treatment'] = 'home/hyd_laser_acne_scar_removal_treatmen';
$route['hyderabad/pigmentation-treatment'] = 'home/hyd_pigmentation_treatment';
$route['hyderabad/stretch-marks-removal-treatment'] = 'home/hyd_stretch_marks_removal_treatment';
