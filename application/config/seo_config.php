<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['seo_title']  = 'Best Hair Transplant, Dermatology, Cosmetic Surgery in India';
$config['seo_desc']   = 'AK Clinics offers Best Hair Transplantation, Hair Loss Treatment, Dermatology &amp; Cosmetic Surgery in Ludhiana, New Delhi, Bangalore India at nominal cost. The clinics are lead by internationally renowned surgeon Dr Kapil Dua, best hair transplant surgeon in India.';
$config['seo_keywords']   = 'AK Clinics offers Best Hair Transplantation, Hair Loss Treatment, Dermatology &amp; Cosmetic Surgery in Ludhiana, New Delhi, Bangalore India at nominal cost. The clinics are lead by internationally renowned surgeon Dr Kapil Dua, best hair transplant surgeon in India.';
$config['seo_robot']  = true;