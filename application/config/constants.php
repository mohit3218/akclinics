<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
//CDN SITE_URL_IMAGE
define('CDN_IMGAE_URL', 'dev.akclinics.com');
//define('CDN_IMGAE_URL', 'akclinics.co.in');
//define('CDN_IMGAE_URL', 'akclinics.org');
//define('CDN_IMGAE_URL', 'cdn.akclinics.org');
//Gernal
define('POST_URL_FIND_LIVE', 1);
define('POST_URL_REPLACE_LIVE', 1);
//Blog category ID Wordpress
define('BLOG_CATEGORY', 26);
define('PATIRNTS_ASK', 7);
define('SUCCESS_STORIES', 25);
define('CASE_STUDY', 6);
define('COSMETIC_SURGERY_ARTICLES', 393);
define('WP_COSMETOLOGY', 414);
define('HAIR_LOSS_ARTICLES', 16);
define('HAIR_LOSS_TREATMENT_ARTICLES', 211);
define('HAIR_TRANSPLANT_ARTICLES', 19);
define('MEDICAL_TREATMENT_ARTICLES', 27);
define('NEWS_AND_EVENTS', 47);
define('STYLESHALA', 436);
define('UNCATEGORIZED', 1);

//Blog category ID CI
define('CI_UNCATEGORIZED', 1);
define('CI_CASE_STUDY', 2);
define('CI_PATIRNTS_ASK', 3);
define('CI_HAIR_LOSS_ARTICLES', 4);
define('CI_HAIR_TRANSPLANT_ARTICLES', 5);
define('CI_SUCCESS_STORIES', 6);
define('CI_BLOG_CATEGORY', 7);
define('CI_MEDICAL_TREATMENT_ARTICLES', 8);
define('CI_NEWS_AND_EVENTS', 9);
define('CI_HAIR_LOSS_TREATMENT_ARTICLES', 10);
define('CI_COSMETIC_SURGERY_ARTICLES', 11);
define('CI_COSMETOLOGY', 12);
define('CI_STYLESHALA', 13);

//Pages Id
define('SITE_NAME', ' | AK Clinics');
//Hair Transplant pages ID
define('HOME_PAGE', 1);
define('HAIR_TRANSPLANT', 2);
define('HAIR_TRANSPLANT_COST', 3);
define('FUE_HAIR_TRANSPLANT', 4);
define('FUT_HAIR_TRANSPLANT', 5);
define('HAIR_TRANSPLANT_TECHNIQUES', 6);
define('FACIAL_HAIR_TRANSPLANT', 7);
define('BEARD_TRANSPLANT', 8);
define('BIO_FUE', 9);
define('BODY_HAIR_TRANSPLANT', 10);
define('REVISION_HAIR_TRANSPLANT', 11);
define('POSTOPERATIVE_CARE', 12);
define('GIGA_SESSION', 95);
define('BIO_DHT', 96);
define('FAQS', 97);
//Hair Loss pages ID
define('HAIR_LOSS', 13);
define('HAIR_LOSS_MEN', 98);
define('HAIR_LOSS_WOMEN', 99);
define('VIRTUAL_CONSULTATION', 109);
//Hair Restoration pages ID
define('HAIR_RESTORATION', 14);
define('HAIR_GAIN_THERAPY', 15);
define('PRP_TREATMENT', 16);
define('HAIR_LOSS_TREATMENT_MEN_WOMEN', 100);
define('ARTIFICIAL_HAIR_RESTORATION', 17);
define('SCALP_MICRO_PIGMENTATION_INDIA', 18);
define('MESOTHERAPY', 19);
//About Us pages ID
define('ABOUT_US', 20);
define('HAIR_TRANSPLANT_TRAINING', 21);
define('OUR_TEAM', 57);
define('DR_KAPIL_DUA', 58);
define('DR_AMAN_DUA', 59);
define('DR_ROSHAN_KUMAR', 60);
define('DR_VIVEK_MEHTA', 61);
define('DR_SHRADDHA_UPRETY', 62);
define('DR_BHAWNA_BHARDWAJ', 63);
define('DR_MADHU_KUTHIAL', 64);
define('DR_JIGISHA_JALU', 65);
define('DR_DINKAR_SOOD', 110);
define('SEARCH', 111);
define('DR_NIRAV_DESAI', 124);
//Cosmetic Surgery pages ID
define('COSMETIC_SURGERY', 22);
define('RHINOPLASTY', 23);
define('BLEPHAROPLASTY', 24);
define('GYNECOMASTIA', 25);
define('VITILIGO_TREATMENT', 26);
define('ABDOMINOPLASTY', 27);
define('LIPOSUCTION', 28);
define('BREAST_AUGMENTATION', 29);
define('BREAST_REDUCTION', 30);
define('COSMETIC_GYNAECOLOGY', 31);
define('MALE_GENITAL_SURGERY', 32);
//Cosmetology pages ID
define('COSMETOLOGY', 33);
define('LASER_HAIR_REMOVAL_FOR_MEN_AND_WOMEN', 34);
define('NON_SURGICAL_ULTRASONIC_LIPOSUCTION', 35);
define('SKIN_POLISHING', 36);
define('STRETCH_MARKS_TREATMENT', 37);
define('LASER_PHOTO_FACIAL', 38);
define('LASER_TATTOO_REMOVAL', 39);
define('ULTHERAPY_TREATMENT', 40);
define('CHEMICAL_PEELS', 41);
define('ACNE_TREATMENT', 42);
define('PIGMENTATION_TREATMENT', 43);
define('LASER_VAGINAL_REJUVENATION', 44);
define('ANTI_AGEING_TREATMENTS', 45);
define('ULTRACEL_SKIN_TIGHTENING_TREATMENT', 46);
define('BOTULINUM_TOXIN_BOTOX', 101);
define('FILLERS', 102);
define('FACIAL_VOLUMIZERS', 103);
define('FRACTIONAL_LASER', 104);
define('VAMPIRE_LIFT', 105);
define('HAIR_TRANSPLANT_IN_MEN', 106);
define('HAIR_TRANSPLANT_IN_WOMEN', 107);
define('HAIR_TRANSPLANT_ON_EMI', 108);
//Results Pages ID
define('RESULTS', 47);
define('RESULT_VIDEOS', 48);
define('RESULTS_COSMETIC_SURGERY', 49);
define('RESULTS_PRP_THERAPY', 50);
//Blog Pages ID
define('BLOG', 51);
//Locations Pages ID
define('LOCATION', 52);
define('HAIR_TRANSPLANT_LUDHIANA', 53);
define('HAIR_TRANSPLANT_DELHI', 54);
define('BEST_HAIR_TRANSPLANT_CLINIC', 55);
//Contact Us Pages ID
define('CONTACT_US', 56);
//POST Opertaions Pages ID
define('PRIVACY_POLICY', 66);
define('HAIR_GAIN_THERAPY_POST_OP_CARE_INSTRUCTIONS', 67);
define('STRETCH_MARKS_TREATMENT_POST_OP_CARE_INSTRUCTIONS', 68);
define('ULTHERAPY_TREATMENT_POST_OP_CARE_INSTRUCTIONS', 69);
define('PIGMENTATION_TREATMENT_POST_OP_CARE_INSTRUCTIONS', 70);
define('LASER_HAIR_REMOVAL_POST_OP_CARE_INSTRUCTIONS', 71);
define('CHEMICAL_PEELS_POST_OP_INSTRUCTIONS', 72);
define('ACNE_SCAR_TREATMENT_POST_OP_INSTRUCTIONS', 73);
define('SCALP_MICROPIGMENTATION_POST_OP_INSTRUCTIONS', 74);
define('RHINOPLASTY_POST_OP_INSTRUCTIONS', 75);
define('COSMEDICO_TREATMENT_POST_OP_INSTRUCTIONS', 76);
define('LASER_VAGINAL_REJUVENATION_POST_OP_INSTRUCTIONS', 77);
define('CO2_LASER_TREATMENT_POST_OP_INSTRUCTIONS', 78);
define('VITILIGO_TREATMENT_POST_OP_INSTRUCTIONS', 79);
define('GYNECOMASTIA_POST_OP_INSTRUCTIONS', 80);
define('ANTI_AGEING_TREATMENTS_POST_OP_CARE_INSTRUCTIONS', 81);
define('NON_SURGICAL_ULTRASONIC_LIPOSUCTION_POST_OP_CARE_INSTRUCTIONS', 82);
//Bangalore Pages ID
define('SBC_BANGALORE', 83);
define('SBC_HAIR_LOSS_TREATMENT', 84);
define('SBC_BEST_HAIR_TRANSPLANT_CLINIC', 85);
define('SBC_LASER_HAIR_REMOVAL_FOR_MEN_AND_WOMEN', 86);
define('SBC_PRP_HAIR_LOSS_TREATMENT', 87);
define('SBC_LASER_PERMANENT_TATTOO_REMOVAL', 88);
define('SBC_LASER_SKIN_TREATMENT', 89);
define('SBC_SKIN_WHITENING_LIGHTENING_TREATMENT', 90);
define('SBC_SCALP_MICRO_PIGMENTATION', 91);
define('SBC_LASER_ACNE_SCAR_REMOVAL_TREATMEN', 92);
define('SBC_PIGMENTATION_TREATMENT', 93);
define('SBC_STRETCH_MARKS_REMOVAL_TREATMENT', 94);
//Hyderabad Pages ID
define('HYD_HYDERABAD', 112);
define('HYD_BEST_HAIR_TRANSPLANT_CLINIC', 113);
define('HYD_HAIR_LOSS_TREATMENT', 114);
define('HYD_LASER_HAIR_REMOVAL_FOR_MEN_AND_WOMEN', 115);
define('HYD_LASER_ACNE_SCAR_REMOVAL_TREATMEN', 116);
define('HYD_LASER_PERMANENT_TATTOO_REMOVAL', 117);
define('HYD_LASER_SKIN_TREATMENT', 118);
define('HYD_PIGMENTATION_TREATMENT', 119);
define('HYD_PRP_HAIR_LOSS_TREATMENT', 120);
define('HYD_SCALP_MICRO_PIGMENTATION', 121);
define('HYD_SKIN_WHITENING_LIGHTENING_TREATMENT', 122);
define('HYD_STRETCH_MARKS_REMOVAL_TREATMENT', 123);